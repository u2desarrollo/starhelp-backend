<?php

use App\Entities\Promotion;
use App\Entities\Tax;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Entities\User::class, function (Faker $faker) {
    return [

        'email' => $faker->unique()->safeEmail,
        'password' => 'secret', // secret
        'remember_token' => str_random(10),
    ];
});
$factory->define(Tax::class, function (Faker $faker) {
    return [
        'code' => $faker->randomDigit,
        'description' => $faker->text,
        'percentage' => $faker->randomDigit, // secret
        'base' => $faker->randomDigit,
        'minimum_base' => $faker->randomDigit,
    ];
});

$factory->define(Promotion::class, function (Faker $faker) {
    return [
        'promotion_type_id' => 1,
        'line_id' => 11,
        'description' => $faker->name, // secret
        'start_date' => $faker->date,
        'end_date' => $faker->date,
        'minimum_gross_value' => 1,
        'only_available' => false,
        'discount' => 1, // secret
        'pay_soon' => 1,
        'cash_payment' => 1,
        'gift_type' => 1,
        'image' => $faker->name,

    ];
});
