<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Price::class, function (Faker $faker) {
	return [
		'product_id' => \App\Entities\Product::inRandomOrder()->first()->id,
		'price_list_id' => $faker->randomElement([269, 270, 869]),
		'price' => rand(10000, 1000000),
		'include_iva' => false
	];
});
