<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $branchoffice_warehouses = DB::table('branchoffice_warehouses')->get();

        $products = DB::table('products')->get();

        $count = 0;

        try {

            DB::beginTransaction();

            DB::table('inventories')->truncate();

            foreach ($products as $product) {

                $value = random_int(15000, 500000);

                foreach ($branchoffice_warehouses as $branchoffice_warehouse) {

                    $insert = [
                        'branchoffice_warehouse_id' => $branchoffice_warehouse->id,
                        'product_id' => $product->id,
                        'stock' => random_int(1, 500),
                        'stock_values' => $value,
                        'average_cost' => $value
                    ];

                    DB::table('inventories')->insert($insert);

                    echo ++$count . "\n";
                }

            }

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();

        }

    }
}
