<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mpios = [
            [
                'city_code' => '001',
                'department_id' => '05',
                'city' => 'MEDELLIN',

            ],
            [
                'city_code' => '002',
                'department_id' => '05',
                'city' => 'ABEJORRAL',

            ],
            [
                'city_code' => '004',
                'department_id' => '05',
                'city' => 'ABRIAQUI',

            ],
            [
                'city_code' => '021',
                'department_id' => '05',
                'city' => 'ALEJANDRIA',

            ],
            [
                'city_code' => '030',
                'department_id' => '05',
                'city' => 'AMAGA',

            ],
            [
                'city_code' => '031',
                'department_id' => '05',
                'city' => 'AMALFI',

            ],
            [
                'city_code' => '034',
                'department_id' => '05',
                'city' => 'ANDES',

            ],
            [
                'city_code' => '036',
                'department_id' => '05',
                'city' => 'ANGELOPOLIS',

            ],
            [
                'city_code' => '038',
                'department_id' => '05',
                'city' => 'ANGOSTURA',

            ],
            [
                'city_code' => '040',
                'department_id' => '05',
                'city' => 'ANORI',

            ],
            [
                'city_code' => '042',
                'department_id' => '05',
                'city' => 'ANTIOQUIA',

            ],
            [
                'city_code' => '044',
                'department_id' => '05',
                'city' => 'ANZA',

            ],
            [
                'city_code' => '045',
                'department_id' => '05',
                'city' => 'APARTADO',

            ],
            [
                'city_code' => '051',
                'department_id' => '05',
                'city' => 'ARBOLETES',

            ],
            [
                'city_code' => '055',
                'department_id' => '05',
                'city' => 'ARGELIA',

            ],
            [
                'city_code' => '059',
                'department_id' => '05',
                'city' => 'ARMENIA',

            ],
            [
                'city_code' => '079',
                'department_id' => '05',
                'city' => 'BARBOSA',

            ],
            [
                'city_code' => '086',
                'department_id' => '05',
                'city' => 'BELMIRA',

            ],
            [
                'city_code' => '088',
                'department_id' => '05',
                'city' => 'BELLO',

            ],
            [
                'city_code' => '091',
                'department_id' => '05',
                'city' => 'BETANIA',

            ],
            [
                'city_code' => '093',
                'department_id' => '05',
                'city' => 'BETULIA',

            ],
            [
                'city_code' => '101',
                'department_id' => '05',
                'city' => 'BOLIVAR',

            ],
            [
                'city_code' => '107',
                'department_id' => '05',
                'city' => 'BRICEÑO',

            ],
            [
                'city_code' => '113',
                'department_id' => '05',
                'city' => 'BURITICA',

            ],
            [
                'city_code' => '120',
                'department_id' => '05',
                'city' => 'CACERES',

            ],
            [
                'city_code' => '125',
                'department_id' => '05',
                'city' => 'CAICEDO',

            ],
            [
                'city_code' => '129',
                'department_id' => '05',
                'city' => 'CALDAS',

            ],
            [
                'city_code' => '134',
                'department_id' => '05',
                'city' => 'CAMPAMENTO',

            ],
            [
                'city_code' => '138',
                'department_id' => '05',
                'city' => 'CAÑASGORDAS',

            ],
            [
                'city_code' => '142',
                'department_id' => '05',
                'city' => 'CARACOLI',

            ],
            [
                'city_code' => '145',
                'department_id' => '05',
                'city' => 'CARAMANTA',

            ],
            [
                'city_code' => '147',
                'department_id' => '05',
                'city' => 'CAREPA',

            ],
            [
                'city_code' => '148',
                'department_id' => '05',
                'city' => 'CARMEN DE VIBORAL',

            ],
            [
                'city_code' => '150',
                'department_id' => '05',
                'city' => 'CAROLINA',

            ],
            [
                'city_code' => '154',
                'department_id' => '05',
                'city' => 'CAUCASIA',

            ],
            [
                'city_code' => '172',
                'department_id' => '05',
                'city' => 'CHIGORODO',

            ],
            [
                'city_code' => '190',
                'department_id' => '05',
                'city' => 'CISNEROS',

            ],
            [
                'city_code' => '197',
                'department_id' => '05',
                'city' => 'COCORNA',

            ],
            [
                'city_code' => '206',
                'department_id' => '05',
                'city' => 'CONCEPCIÓN',

            ],
            [
                'city_code' => '209',
                'department_id' => '05',
                'city' => 'CONCORDIA',

            ],
            [
                'city_code' => '212',
                'department_id' => '05',
                'city' => 'COPACABANA',

            ],
            [
                'city_code' => '234',
                'department_id' => '05',
                'city' => 'DABEIBA',

            ],
            [
                'city_code' => '237',
                'department_id' => '05',
                'city' => 'DON MATIAS',

            ],
            [
                'city_code' => '240',
                'department_id' => '05',
                'city' => 'EBEJICO',

            ],
            [
                'city_code' => '250',
                'department_id' => '05',
                'city' => 'EL BAGRE',

            ],
            [
                'city_code' => '264',
                'department_id' => '05',
                'city' => 'ENTRERRIOS',

            ],
            [
                'city_code' => '266',
                'department_id' => '05',
                'city' => 'ENVIGADO',

            ],
            [
                'city_code' => '282',
                'department_id' => '05',
                'city' => 'FREDONIA',

            ],
            [
                'city_code' => '284',
                'department_id' => '05',
                'city' => 'FRONTINO',

            ],
            [
                'city_code' => '306',
                'department_id' => '05',
                'city' => 'GIRALDO',

            ],
            [
                'city_code' => '308',
                'department_id' => '05',
                'city' => 'GIRARDOTA',

            ],
            [
                'city_code' => '310',
                'department_id' => '05',
                'city' => 'GOMEZ PLATA',

            ],
            [
                'city_code' => '313',
                'department_id' => '05',
                'city' => 'GRANADA',

            ],
            [
                'city_code' => '315',
                'department_id' => '05',
                'city' => 'GUADALUPE',

            ],
            [
                'city_code' => '318',
                'department_id' => '05',
                'city' => 'GUARNE',

            ],
            [
                'city_code' => '321',
                'department_id' => '05',
                'city' => 'GUATAPE',

            ],
            [
                'city_code' => '347',
                'department_id' => '05',
                'city' => 'HELICONIA',

            ],
            [
                'city_code' => '353',
                'department_id' => '05',
                'city' => 'HISPANIA',

            ],
            [
                'city_code' => '360',
                'department_id' => '05',
                'city' => 'ITAGUI',

            ],
            [
                'city_code' => '361',
                'department_id' => '05',
                'city' => 'ITUANGO',

            ],
            [
                'city_code' => '364',
                'department_id' => '05',
                'city' => 'JARDIN',

            ],
            [
                'city_code' => '368',
                'department_id' => '05',
                'city' => 'JERICO',

            ],
            [
                'city_code' => '376',
                'department_id' => '05',
                'city' => 'LA CEJA',

            ],
            [
                'city_code' => '380',
                'department_id' => '05',
                'city' => 'LA ESTRELLA',

            ],
            [
                'city_code' => '390',
                'department_id' => '05',
                'city' => 'LA PINTADA',

            ],
            [
                'city_code' => '400',
                'department_id' => '05',
                'city' => 'LA UNION',

            ],
            [
                'city_code' => '411',
                'department_id' => '05',
                'city' => 'LIBORINA',

            ],
            [
                'city_code' => '425',
                'department_id' => '05',
                'city' => 'MACEO',

            ],
            [
                'city_code' => '440',
                'department_id' => '05',
                'city' => 'MARINILLA',

            ],
            [
                'city_code' => '467',
                'department_id' => '05',
                'city' => 'MONTEBELLO',

            ],
            [
                'city_code' => '475',
                'department_id' => '05',
                'city' => 'MURINDO',

            ],
            [
                'city_code' => '480',
                'department_id' => '05',
                'city' => 'MUTATA',

            ],
            [
                'city_code' => '483',
                'department_id' => '05',
                'city' => 'NARIÑO',

            ],
            [
                'city_code' => '490',
                'department_id' => '05',
                'city' => 'NECOCLI',

            ],
            [
                'city_code' => '495',
                'department_id' => '05',
                'city' => 'NECHI',

            ],
            [
                'city_code' => '501',
                'department_id' => '05',
                'city' => 'OLAYA',

            ],
            [
                'city_code' => '541',
                'department_id' => '05',
                'city' => 'PEÑOL',

            ],
            [
                'city_code' => '543',
                'department_id' => '05',
                'city' => 'PEQUE',

            ],
            [
                'city_code' => '576',
                'department_id' => '05',
                'city' => 'PUEBLORRICO',

            ],
            [
                'city_code' => '585',
                'department_id' => '05',
                'city' => 'PUERTO NARE (LA MAGDALENA)',

            ],
            [
                'city_code' => '591',
                'department_id' => '05',
                'city' => 'PUERTO TRIUNFO',

            ],
            [
                'city_code' => '604',
                'department_id' => '05',
                'city' => 'REMEDIOS',

            ],
            [
                'city_code' => '607',
                'department_id' => '05',
                'city' => 'RETIRO',

            ],
            [
                'city_code' => '615',
                'department_id' => '05',
                'city' => 'RIONEGRO',

            ],
            [
                'city_code' => '628',
                'department_id' => '05',
                'city' => 'SABANALARGA',

            ],
            [
                'city_code' => '631',
                'department_id' => '05',
                'city' => 'SABANETA',

            ],
            [
                'city_code' => '642',
                'department_id' => '05',
                'city' => 'SALGAR',

            ],
            [
                'city_code' => '647',
                'department_id' => '05',
                'city' => 'SAN ANDRES',

            ],
            [
                'city_code' => '649',
                'department_id' => '05',
                'city' => 'SAN CARLOS',

            ],
            [
                'city_code' => '652',
                'department_id' => '05',
                'city' => 'SAN FRANCISCO',

            ],
            [
                'city_code' => '656',
                'department_id' => '05',
                'city' => 'SAN JERONIMO',

            ],
            [
                'city_code' => '658',
                'department_id' => '05',
                'city' => 'SAN JOSE DE LA MONTAÑA',

            ],
            [
                'city_code' => '659',
                'department_id' => '05',
                'city' => 'SAN JUAN DE URABA',

            ],
            [
                'city_code' => '660',
                'department_id' => '05',
                'city' => 'SAN LUIS',

            ],
            [
                'city_code' => '664',
                'department_id' => '05',
                'city' => 'SAN PEDRO',

            ],
            [
                'city_code' => '665',
                'department_id' => '05',
                'city' => 'SAN PEDRO DE URABA',

            ],
            [
                'city_code' => '667',
                'department_id' => '05',
                'city' => 'SAN RAFAEL',

            ],
            [
                'city_code' => '670',
                'department_id' => '05',
                'city' => 'SAN ROQUE',

            ],
            [
                'city_code' => '674',
                'department_id' => '05',
                'city' => 'SAN VICENTE',

            ],
            [
                'city_code' => '679',
                'department_id' => '05',
                'city' => 'SANTA BARBARA',

            ],
            [
                'city_code' => '686',
                'department_id' => '05',
                'city' => 'SANTA ROSA DE OSOS',

            ],
            [
                'city_code' => '690',
                'department_id' => '05',
                'city' => 'SANTO DOMINGO',

            ],
            [
                'city_code' => '697',
                'department_id' => '05',
                'city' => 'SANTUARIO',

            ],
            [
                'city_code' => '736',
                'department_id' => '05',
                'city' => 'SEGOVIA',

            ],
            [
                'city_code' => '756',
                'department_id' => '05',
                'city' => 'SONSON',

            ],
            [
                'city_code' => '761',
                'department_id' => '05',
                'city' => 'SOPETRAN',

            ],
            [
                'city_code' => '789',
                'department_id' => '05',
                'city' => 'TAMESIS',

            ],
            [
                'city_code' => '790',
                'department_id' => '05',
                'city' => 'TARAZA',

            ],
            [
                'city_code' => '792',
                'department_id' => '05',
                'city' => 'TARSO',

            ],
            [
                'city_code' => '809',
                'department_id' => '05',
                'city' => 'TITIRIBI',

            ],
            [
                'city_code' => '819',
                'department_id' => '05',
                'city' => 'TOLEDO',

            ],
            [
                'city_code' => '837',
                'department_id' => '05',
                'city' => 'TURBO',

            ],
            [
                'city_code' => '824',
                'department_id' => '05',
                'city' => 'URAMITA',

            ],
            [
                'city_code' => '847',
                'department_id' => '05',
                'city' => 'URRAO',

            ],
            [
                'city_code' => '854',
                'department_id' => '05',
                'city' => 'VALDIVIA',

            ],
            [
                'city_code' => '856',
                'department_id' => '05',
                'city' => 'VALPARAISO',

            ],
            [
                'city_code' => '858',
                'department_id' => '05',
                'city' => 'VEGACHI',

            ],
            [
                'city_code' => '861',
                'department_id' => '05',
                'city' => 'VENECIA',

            ],
            [
                'city_code' => '873',
                'department_id' => '05',
                'city' => 'VIGIA DEL FUERTE',

            ],
            [
                'city_code' => '885',
                'department_id' => '05',
                'city' => 'YALI',

            ],
            [
                'city_code' => '887',
                'department_id' => '05',
                'city' => 'YARUMAL',

            ],
            [
                'city_code' => '890',
                'department_id' => '05',
                'city' => 'YOLOMBO',

            ],
            [
                'city_code' => '893',
                'department_id' => '05',
                'city' => 'YONDO',

            ],
            [
                'city_code' => '895',
                'department_id' => '05',
                'city' => 'ZARAGOZA',

            ],
            [
                'city_code' => '001',
                'department_id' => '08',
                'city' => 'BARRANQUILLA',

            ],
            [
                'city_code' => '078',
                'department_id' => '08',
                'city' => 'BARANOA',

            ],
            [
                'city_code' => '137',
                'department_id' => '08',
                'city' => 'CAMPO DE LA CRUZ',

            ],
            [
                'city_code' => '141',
                'department_id' => '08',
                'city' => 'CANDELARIA',

            ],
            [
                'city_code' => '296',
                'department_id' => '08',
                'city' => 'GALAPA',

            ],
            [
                'city_code' => '372',
                'department_id' => '08',
                'city' => 'JUAN DE ACOSTA',

            ],
            [
                'city_code' => '421',
                'department_id' => '08',
                'city' => 'LURUACO',

            ],
            [
                'city_code' => '433',
                'department_id' => '08',
                'city' => 'MALAMBO',

            ],
            [
                'city_code' => '436',
                'department_id' => '08',
                'city' => 'MANATI',

            ],
            [
                'city_code' => '520',
                'department_id' => '08',
                'city' => 'PALMAR DE VARELA',

            ],
            [
                'city_code' => '549',
                'department_id' => '08',
                'city' => 'PIOJO',

            ],
            [
                'city_code' => '558',
                'department_id' => '08',
                'city' => 'POLO NUEVO',

            ],
            [
                'city_code' => '560',
                'department_id' => '08',
                'city' => 'PONEDERA',

            ],
            [
                'city_code' => '573',
                'department_id' => '08',
                'city' => 'PUERTO COLOMBIA',

            ],
            [
                'city_code' => '606',
                'department_id' => '08',
                'city' => 'REPELON',

            ],
            [
                'city_code' => '634',
                'department_id' => '08',
                'city' => 'SABANAGRANDE',

            ],
            [
                'city_code' => '638',
                'department_id' => '08',
                'city' => 'SABANALARGA',

            ],
            [
                'city_code' => '675',
                'department_id' => '08',
                'city' => 'SANTA LUCIA',

            ],
            [
                'city_code' => '685',
                'department_id' => '08',
                'city' => 'SANTO TOMAS',

            ],
            [
                'city_code' => '758',
                'department_id' => '08',
                'city' => 'SOLEDAD',

            ],
            [
                'city_code' => '770',
                'department_id' => '08',
                'city' => 'SUAN',

            ],
            [
                'city_code' => '832',
                'department_id' => '08',
                'city' => 'TUBARA',

            ],
            [
                'city_code' => '849',
                'department_id' => '08',
                'city' => 'USIACURI',

            ],
            [
                'city_code' => '001',
                'department_id' => '11',
                'city' => 'BOGOTÁ D.C',

            ],
            [
                'city_code' => '001',
                'department_id' => '13',
                'city' => 'CARTAGENA D.T',

            ],
            [
                'city_code' => '006',
                'department_id' => '13',
                'city' => 'ACHI',

            ],
            [
                'city_code' => '030',
                'department_id' => '13',
                'city' => 'ALTOS DEL ROSARIO',

            ],
            [
                'city_code' => '042',
                'department_id' => '13',
                'city' => 'ARENAL',

            ],
            [
                'city_code' => '052',
                'department_id' => '13',
                'city' => 'ARJONA',

            ],
            [
                'city_code' => '062',
                'department_id' => '13',
                'city' => 'ARROYOHONDO',

            ],
            [
                'city_code' => '074',
                'department_id' => '13',
                'city' => 'BARRANCO DE LOBA',

            ],
            [
                'city_code' => '140',
                'department_id' => '13',
                'city' => 'CALAMAR',

            ],
            [
                'city_code' => '160',
                'department_id' => '13',
                'city' => 'CANTAGALLO',

            ],
            [
                'city_code' => '188',
                'department_id' => '13',
                'city' => 'CICUCO',

            ],
            [
                'city_code' => '212',
                'department_id' => '13',
                'city' => 'CORDOBA',

            ],
            [
                'city_code' => '222',
                'department_id' => '13',
                'city' => 'CLEMENCIA',

            ],
            [
                'city_code' => '244',
                'department_id' => '13',
                'city' => 'EL CARMEN DE BOLIVAR',

            ],
            [
                'city_code' => '248',
                'department_id' => '13',
                'city' => 'EL GUAMO',

            ],
            [
                'city_code' => '268',
                'department_id' => '13',
                'city' => 'EL PEÑON',

            ],
            [
                'city_code' => '300',
                'department_id' => '13',
                'city' => 'HATILLO DE LOBA',

            ],
            [
                'city_code' => '430',
                'department_id' => '13',
                'city' => 'MAGANGUE',

            ],
            [
                'city_code' => '433',
                'department_id' => '13',
                'city' => 'MAHATES',

            ],
            [
                'city_code' => '440',
                'department_id' => '13',
                'city' => 'MAGARITA',

            ],
            [
                'city_code' => '442',
                'department_id' => '13',
                'city' => 'MARIA LA BAJA',

            ],
            [
                'city_code' => '458',
                'department_id' => '13',
                'city' => 'MONTECRISTO',

            ],
            [
                'city_code' => '468',
                'department_id' => '13',
                'city' => 'MONPOS',

            ],
            [
                'city_code' => '473',
                'department_id' => '13',
                'city' => 'MORALES',

            ],
            [
                'city_code' => '549',
                'department_id' => '13',
                'city' => 'PINILLOS',

            ],
            [
                'city_code' => '580',
                'department_id' => '13',
                'city' => 'REGIDOR',

            ],
            [
                'city_code' => '600',
                'department_id' => '13',
                'city' => 'RIO VIEJO',

            ],
            [
                'city_code' => '620',
                'department_id' => '13',
                'city' => 'SAN CRISTOBAL',

            ],
            [
                'city_code' => '647',
                'department_id' => '13',
                'city' => 'SAN ESTANISLAO',

            ],
            [
                'city_code' => '650',
                'department_id' => '13',
                'city' => 'SAN FERNANDO',

            ],
            [
                'city_code' => '654',
                'department_id' => '13',
                'city' => 'SAN JACINTO',

            ],
            [
                'city_code' => '655',
                'department_id' => '13',
                'city' => 'SAN JACINTO DEL CAUCA',

            ],
            [
                'city_code' => '657',
                'department_id' => '13',
                'city' => 'SAN JUAN DE NEPOMUCENO',

            ],
            [
                'city_code' => '667',
                'department_id' => '13',
                'city' => 'SAN MARTIN DE LOBA',

            ],
            [
                'city_code' => '670',
                'department_id' => '13',
                'city' => 'SAN PABLO',

            ],
            [
                'city_code' => '673',
                'department_id' => '13',
                'city' => 'SANTA CATALINA',

            ],
            [
                'city_code' => '683',
                'department_id' => '13',
                'city' => 'SANTA ROSA',

            ],
            [
                'city_code' => '688',
                'department_id' => '13',
                'city' => 'SANTA ROSA DEL SUR',

            ],
            [
                'city_code' => '744',
                'department_id' => '13',
                'city' => 'SIMITI',

            ],
            [
                'city_code' => '760',
                'department_id' => '13',
                'city' => 'SOPLAVIENTO',

            ],
            [
                'city_code' => '780',
                'department_id' => '13',
                'city' => 'TALAIGUA NUEVO',

            ],
            [
                'city_code' => '810',
                'department_id' => '13',
                'city' => 'TIQUISIO',

            ],
            [
                'city_code' => '836',
                'department_id' => '13',
                'city' => 'TURBACO',

            ],
            [
                'city_code' => '838',
                'department_id' => '13',
                'city' => 'TURBANA',

            ],
            [
                'city_code' => '873',
                'department_id' => '13',
                'city' => 'VILLANUEVA',

            ],
            [
                'city_code' => '894',
                'department_id' => '13',
                'city' => 'ZAMBRANO',

            ],
            [
                'city_code' => '001',
                'department_id' => '15',
                'city' => 'TUNJA',

            ],
            [
                'city_code' => '022',
                'department_id' => '15',
                'city' => 'ALMEIDA',

            ],
            [
                'city_code' => '047',
                'department_id' => '15',
                'city' => 'AQUITANIA',

            ],
            [
                'city_code' => '051',
                'department_id' => '15',
                'city' => 'ARCABUCO',

            ],
            [
                'city_code' => '087',
                'department_id' => '15',
                'city' => 'BELEN',

            ],
            [
                'city_code' => '090',
                'department_id' => '15',
                'city' => 'BERBEO',

            ],
            [
                'city_code' => '092',
                'department_id' => '15',
                'city' => 'BETEITIVA',

            ],
            [
                'city_code' => '097',
                'department_id' => '15',
                'city' => 'BOAVITA',

            ],
            [
                'city_code' => '104',
                'department_id' => '15',
                'city' => 'BOYACA',

            ],
            [
                'city_code' => '106',
                'department_id' => '15',
                'city' => 'BRICEÑO',

            ],
            [
                'city_code' => '109',
                'department_id' => '15',
                'city' => 'BUENAVISTA',

            ],
            [
                'city_code' => '114',
                'department_id' => '15',
                'city' => 'BUSBANZA',

            ],
            [
                'city_code' => '131',
                'department_id' => '15',
                'city' => 'CALDAS',

            ],
            [
                'city_code' => '135',
                'department_id' => '15',
                'city' => 'CAMPOHERMOSO',

            ],
            [
                'city_code' => '162',
                'department_id' => '15',
                'city' => 'CERINZA',

            ],
            [
                'city_code' => '172',
                'department_id' => '15',
                'city' => 'CHINAVITA',

            ],
            [
                'city_code' => '176',
                'department_id' => '15',
                'city' => 'CHIQUINQUIRA',

            ],
            [
                'city_code' => '180',
                'department_id' => '15',
                'city' => 'CHISCAS',

            ],
            [
                'city_code' => '183',
                'department_id' => '15',
                'city' => 'CHITA',

            ],
            [
                'city_code' => '185',
                'department_id' => '15',
                'city' => 'CHITARAQUE',

            ],
            [
                'city_code' => '187',
                'department_id' => '15',
                'city' => 'CHIVATA',

            ],
            [
                'city_code' => '189',
                'department_id' => '15',
                'city' => 'CIENAGA',

            ],
            [
                'city_code' => '204',
                'department_id' => '15',
                'city' => 'COMBITA',

            ],
            [
                'city_code' => '212',
                'department_id' => '15',
                'city' => 'COPER',

            ],
            [
                'city_code' => '215',
                'department_id' => '15',
                'city' => 'CORRALES',

            ],
            [
                'city_code' => '218',
                'department_id' => '15',
                'city' => 'COVARACHIA',

            ],
            [
                'city_code' => '223',
                'department_id' => '15',
                'city' => 'CUBARA',

            ],
            [
                'city_code' => '224',
                'department_id' => '15',
                'city' => 'CUCAITA',

            ],
            [
                'city_code' => '226',
                'department_id' => '15',
                'city' => 'CUITIVA',

            ],
            [
                'city_code' => '232',
                'department_id' => '15',
                'city' => 'CHIQUIZA',

            ],
            [
                'city_code' => '236',
                'department_id' => '15',
                'city' => 'CHIVOR',

            ],
            [
                'city_code' => '238',
                'department_id' => '15',
                'city' => 'DUITAMA',

            ],
            [
                'city_code' => '244',
                'department_id' => '15',
                'city' => 'EL COCUY',

            ],
            [
                'city_code' => '248',
                'department_id' => '15',
                'city' => 'EL ESPINO',

            ],
            [
                'city_code' => '272',
                'department_id' => '15',
                'city' => 'FIRAVITOBA',

            ],
            [
                'city_code' => '276',
                'department_id' => '15',
                'city' => 'FLORESTA',

            ],
            [
                'city_code' => '293',
                'department_id' => '15',
                'city' => 'GACHANTIVA',

            ],
            [
                'city_code' => '296',
                'department_id' => '15',
                'city' => 'GAMEZA',

            ],
            [
                'city_code' => '299',
                'department_id' => '15',
                'city' => 'GARAGOA',

            ],
            [
                'city_code' => '317',
                'department_id' => '15',
                'city' => 'GUACAMAYAS',

            ],
            [
                'city_code' => '322',
                'department_id' => '15',
                'city' => 'GUATEQUE',

            ],
            [
                'city_code' => '325',
                'department_id' => '15',
                'city' => 'GUAYATA',

            ],
            [
                'city_code' => '332',
                'department_id' => '15',
                'city' => 'GUICAN',

            ],
            [
                'city_code' => '362',
                'department_id' => '15',
                'city' => 'IZA',

            ],
            [
                'city_code' => '367',
                'department_id' => '15',
                'city' => 'JENESANO',

            ],
            [
                'city_code' => '368',
                'department_id' => '15',
                'city' => 'JERICO',

            ],
            [
                'city_code' => '377',
                'department_id' => '15',
                'city' => 'LABRANZAGRANDE',

            ],
            [
                'city_code' => '380',
                'department_id' => '15',
                'city' => 'LA CAPILLA',

            ],
            [
                'city_code' => '401',
                'department_id' => '15',
                'city' => 'LA VICTORIA',

            ],
            [
                'city_code' => '403',
                'department_id' => '15',
                'city' => 'LA UVITA',

            ],
            [
                'city_code' => '407',
                'department_id' => '15',
                'city' => 'VILLA DE LEIVA',

            ],
            [
                'city_code' => '425',
                'department_id' => '15',
                'city' => 'MACANAL',

            ],
            [
                'city_code' => '442',
                'department_id' => '15',
                'city' => 'MARIPI',

            ],
            [
                'city_code' => '455',
                'department_id' => '15',
                'city' => 'MIRAFLORES',

            ],
            [
                'city_code' => '464',
                'department_id' => '15',
                'city' => 'MONGUA',

            ],
            [
                'city_code' => '466',
                'department_id' => '15',
                'city' => 'MONGUI',

            ],
            [
                'city_code' => '469',
                'department_id' => '15',
                'city' => 'MONIQUIRA',

            ],
            [
                'city_code' => '476',
                'department_id' => '15',
                'city' => 'MOTAVITA',

            ],
            [
                'city_code' => '480',
                'department_id' => '15',
                'city' => 'MUZO',

            ],
            [
                'city_code' => '491',
                'department_id' => '15',
                'city' => 'NOBSA',

            ],
            [
                'city_code' => '494',
                'department_id' => '15',
                'city' => 'NUEVO COLON',

            ],
            [
                'city_code' => '500',
                'department_id' => '15',
                'city' => 'OICATA',

            ],
            [
                'city_code' => '507',
                'department_id' => '15',
                'city' => 'OTANCHE',

            ],
            [
                'city_code' => '511',
                'department_id' => '15',
                'city' => 'PACHAVITA',

            ],
            [
                'city_code' => '514',
                'department_id' => '15',
                'city' => 'PAEZ',

            ],
            [
                'city_code' => '516',
                'department_id' => '15',
                'city' => 'PAIPA',

            ],
            [
                'city_code' => '518',
                'department_id' => '15',
                'city' => 'PAJARITO',

            ],
            [
                'city_code' => '522',
                'department_id' => '15',
                'city' => 'PANQUEBA',

            ],
            [
                'city_code' => '531',
                'department_id' => '15',
                'city' => 'PAUNA',

            ],
            [
                'city_code' => '533',
                'department_id' => '15',
                'city' => 'PAYA',

            ],
            [
                'city_code' => '537',
                'department_id' => '15',
                'city' => 'PAZ DEL RIO',

            ],
            [
                'city_code' => '542',
                'department_id' => '15',
                'city' => 'PESCA',

            ],
            [
                'city_code' => '550',
                'department_id' => '15',
                'city' => 'PISBA',

            ],
            [
                'city_code' => '572',
                'department_id' => '15',
                'city' => 'PUERTO BOYACA',

            ],
            [
                'city_code' => '580',
                'department_id' => '15',
                'city' => 'QUIPAMA',

            ],
            [
                'city_code' => '599',
                'department_id' => '15',
                'city' => 'RAMIRIQUI',

            ],
            [
                'city_code' => '600',
                'department_id' => '15',
                'city' => 'RAQUIRA',

            ],
            [
                'city_code' => '621',
                'department_id' => '15',
                'city' => 'RONDON',

            ],
            [
                'city_code' => '632',
                'department_id' => '15',
                'city' => 'SABOYA',

            ],
            [
                'city_code' => '638',
                'department_id' => '15',
                'city' => 'SACHICA',

            ],
            [
                'city_code' => '646',
                'department_id' => '15',
                'city' => 'SAMACA',

            ],
            [
                'city_code' => '660',
                'department_id' => '15',
                'city' => 'SAN EDUARDO',

            ],
            [
                'city_code' => '664',
                'department_id' => '15',
                'city' => 'SAN JOSE DE PARE',

            ],
            [
                'city_code' => '667',
                'department_id' => '15',
                'city' => 'SAN LUIS DE GACENO',

            ],
            [
                'city_code' => '673',
                'department_id' => '15',
                'city' => 'SAN MATEO',

            ],
            [
                'city_code' => '676',
                'department_id' => '15',
                'city' => 'SAN MIGUEL DE SEMA',

            ],
            [
                'city_code' => '681',
                'department_id' => '15',
                'city' => 'SAN PABLO DE BORBUR',

            ],
            [
                'city_code' => '686',
                'department_id' => '15',
                'city' => 'SANTANA',

            ],
            [
                'city_code' => '690',
                'department_id' => '15',
                'city' => 'SANTA MARIA',

            ],
            [
                'city_code' => '693',
                'department_id' => '15',
                'city' => 'SANTA ROSA DE VITERBO',

            ],
            [
                'city_code' => '696',
                'department_id' => '15',
                'city' => 'SANTA SOFIA',

            ],
            [
                'city_code' => '720',
                'department_id' => '15',
                'city' => 'SATIVANORTE',

            ],
            [
                'city_code' => '723',
                'department_id' => '15',
                'city' => 'SATIVASUR',

            ],
            [
                'city_code' => '740',
                'department_id' => '15',
                'city' => 'SIACHOQUE',

            ],
            [
                'city_code' => '753',
                'department_id' => '15',
                'city' => 'SOATA',

            ],
            [
                'city_code' => '755',
                'department_id' => '15',
                'city' => 'SOCOTA',

            ],
            [
                'city_code' => '757',
                'department_id' => '15',
                'city' => 'SOCHA',

            ],
            [
                'city_code' => '759',
                'department_id' => '15',
                'city' => 'SOGAMOSO',

            ],
            [
                'city_code' => '761',
                'department_id' => '15',
                'city' => 'SOMONDOCO',

            ],
            [
                'city_code' => '762',
                'department_id' => '15',
                'city' => 'SORA',

            ],
            [
                'city_code' => '763',
                'department_id' => '15',
                'city' => 'SOTAQUIRA',

            ],
            [
                'city_code' => '764',
                'department_id' => '15',
                'city' => 'SORACA',

            ],
            [
                'city_code' => '774',
                'department_id' => '15',
                'city' => 'SUSACON',

            ],
            [
                'city_code' => '776',
                'department_id' => '15',
                'city' => 'SUTAMARCHAN',

            ],
            [
                'city_code' => '778',
                'department_id' => '15',
                'city' => 'SUTATENZA',

            ],
            [
                'city_code' => '790',
                'department_id' => '15',
                'city' => 'TASCO',

            ],
            [
                'city_code' => '798',
                'department_id' => '15',
                'city' => 'TENZA',

            ],
            [
                'city_code' => '804',
                'department_id' => '15',
                'city' => 'TIBANA',

            ],
            [
                'city_code' => '806',
                'department_id' => '15',
                'city' => 'TIBASOSA',

            ],
            [
                'city_code' => '808',
                'department_id' => '15',
                'city' => 'TINJACA',

            ],
            [
                'city_code' => '810',
                'department_id' => '15',
                'city' => 'TIPACOQUE',

            ],
            [
                'city_code' => '814',
                'department_id' => '15',
                'city' => 'TOCA',

            ],
            [
                'city_code' => '816',
                'department_id' => '15',
                'city' => 'TOGUI',

            ],
            [
                'city_code' => '820',
                'department_id' => '15',
                'city' => 'TOPAGA',

            ],
            [
                'city_code' => '822',
                'department_id' => '15',
                'city' => 'TOTA',

            ],
            [
                'city_code' => '832',
                'department_id' => '15',
                'city' => 'TUNUNGUA',

            ],
            [
                'city_code' => '835',
                'department_id' => '15',
                'city' => 'TURMEQUE',

            ],
            [
                'city_code' => '837',
                'department_id' => '15',
                'city' => 'TUTA',

            ],
            [
                'city_code' => '839',
                'department_id' => '15',
                'city' => 'TUTASA',

            ],
            [
                'city_code' => '842',
                'department_id' => '15',
                'city' => 'UMBITA',

            ],
            [
                'city_code' => '861',
                'department_id' => '15',
                'city' => 'VENTAQUEMADA',

            ],
            [
                'city_code' => '879',
                'department_id' => '15',
                'city' => 'VIRACACHA',

            ],
            [
                'city_code' => '897',
                'department_id' => '15',
                'city' => 'ZETAQUIRA',

            ],
            [
                'city_code' => '001',
                'department_id' => '17',
                'city' => 'MANIZALES',

            ],
            [
                'city_code' => '013',
                'department_id' => '17',
                'city' => 'AGUADAS',

            ],
            [
                'city_code' => '042',
                'department_id' => '17',
                'city' => 'ANSERNA',

            ],
            [
                'city_code' => '050',
                'department_id' => '17',
                'city' => 'ARANZAZU',

            ],
            [
                'city_code' => '088',
                'department_id' => '17',
                'city' => 'BELALCAZAR',

            ],
            [
                'city_code' => '174',
                'department_id' => '17',
                'city' => 'CHINCHINA',

            ],
            [
                'city_code' => '272',
                'department_id' => '17',
                'city' => 'FILADELFIA',

            ],
            [
                'city_code' => '380',
                'department_id' => '17',
                'city' => 'LA DORADA',

            ],
            [
                'city_code' => '388',
                'department_id' => '17',
                'city' => 'LA MERCED',

            ],
            [
                'city_code' => '433',
                'department_id' => '17',
                'city' => 'MANZANARES',

            ],
            [
                'city_code' => '442',
                'department_id' => '17',
                'city' => 'MARMATO',

            ],
            [
                'city_code' => '444',
                'department_id' => '17',
                'city' => 'MARQUETALIA',

            ],
            [
                'city_code' => '446',
                'department_id' => '17',
                'city' => 'MARULANDA',

            ],
            [
                'city_code' => '486',
                'department_id' => '17',
                'city' => 'NEIRA',

            ],
            [
                'city_code' => '495',
                'department_id' => '17',
                'city' => 'NORCASIA',

            ],
            [
                'city_code' => '513',
                'department_id' => '17',
                'city' => 'PACORA',

            ],
            [
                'city_code' => '524',
                'department_id' => '17',
                'city' => 'PALESTINA',

            ],
            [
                'city_code' => '541',
                'department_id' => '17',
                'city' => 'PENSILVANIA',

            ],
            [
                'city_code' => '614',
                'department_id' => '17',
                'city' => 'RIOSUCIO',

            ],
            [
                'city_code' => '616',
                'department_id' => '17',
                'city' => 'RISARALDA',

            ],
            [
                'city_code' => '653',
                'department_id' => '17',
                'city' => 'SALAMINA',

            ],
            [
                'city_code' => '662',
                'department_id' => '17',
                'city' => 'SAMANA',

            ],
            [
                'city_code' => '665',
                'department_id' => '17',
                'city' => 'SAN JOSE',

            ],
            [
                'city_code' => '777',
                'department_id' => '17',
                'city' => 'SUPIA',

            ],
            [
                'city_code' => '867',
                'department_id' => '17',
                'city' => 'VICTORIA',

            ],
            [
                'city_code' => '873',
                'department_id' => '17',
                'city' => 'VILLAMARIA',

            ],
            [
                'city_code' => '877',
                'department_id' => '17',
                'city' => 'VITERBO',

            ],
            [
                'city_code' => '001',
                'department_id' => '18',
                'city' => 'FLORENCIA',

            ],
            [
                'city_code' => '029',
                'department_id' => '18',
                'city' => 'ALBANIA',

            ],
            [
                'city_code' => '094',
                'department_id' => '18',
                'city' => 'BELEN DE LOS ANDAQUIES',

            ],
            [
                'city_code' => '150',
                'department_id' => '18',
                'city' => 'CARTAGENA DEL CHAIRA',

            ],
            [
                'city_code' => '205',
                'department_id' => '18',
                'city' => 'CURILLO',

            ],
            [
                'city_code' => '247',
                'department_id' => '18',
                'city' => 'EL DONCELLO',

            ],
            [
                'city_code' => '256',
                'department_id' => '18',
                'city' => 'EL PAUJIL',

            ],
            [
                'city_code' => '410',
                'department_id' => '18',
                'city' => 'LA MONTAÑITA',

            ],
            [
                'city_code' => '460',
                'department_id' => '18',
                'city' => 'MILAN',

            ],
            [
                'city_code' => '479',
                'department_id' => '18',
                'city' => 'MORELIA',

            ],
            [
                'city_code' => '592',
                'department_id' => '18',
                'city' => 'PUERTO RICO',

            ],
            [
                'city_code' => '610',
                'department_id' => '18',
                'city' => 'SAN JOSE DE FRAGUA',

            ],
            [
                'city_code' => '753',
                'department_id' => '18',
                'city' => 'SAN VICENTE DEL CAGUAN',

            ],
            [
                'city_code' => '756',
                'department_id' => '18',
                'city' => 'SOLANO',

            ],
            [
                'city_code' => '785',
                'department_id' => '18',
                'city' => 'SOLITA',

            ],
            [
                'city_code' => '860',
                'department_id' => '18',
                'city' => 'VALPARAISO',

            ],
            [
                'city_code' => '001',
                'department_id' => '19',
                'city' => 'POPAYAN',

            ],
            [
                'city_code' => '022',
                'department_id' => '19',
                'city' => 'ALMAGUER',

            ],
            [
                'city_code' => '050',
                'department_id' => '19',
                'city' => 'ARGELIA',

            ],
            [
                'city_code' => '075',
                'department_id' => '19',
                'city' => 'BALBOA',

            ],
            [
                'city_code' => '100',
                'department_id' => '19',
                'city' => 'BOLIVAR',

            ],
            [
                'city_code' => '110',
                'department_id' => '19',
                'city' => 'BUENOS AIRES',

            ],
            [
                'city_code' => '130',
                'department_id' => '19',
                'city' => 'CAJIBIO',

            ],
            [
                'city_code' => '137',
                'department_id' => '19',
                'city' => 'CALDONO',

            ],
            [
                'city_code' => '142',
                'department_id' => '19',
                'city' => 'CALOTO',

            ],
            [
                'city_code' => '212',
                'department_id' => '19',
                'city' => 'CORINTO',

            ],
            [
                'city_code' => '256',
                'department_id' => '19',
                'city' => 'EL TAMBO',

            ],
            [
                'city_code' => '290',
                'department_id' => '19',
                'city' => 'FLORENCIA',

            ],
            [
                'city_code' => '318',
                'department_id' => '19',
                'city' => 'GUAPI',

            ],
            [
                'city_code' => '355',
                'department_id' => '19',
                'city' => 'INZA',

            ],
            [
                'city_code' => '364',
                'department_id' => '19',
                'city' => 'JAMBALO',

            ],
            [
                'city_code' => '392',
                'department_id' => '19',
                'city' => 'LA SIERRA',

            ],
            [
                'city_code' => '397',
                'department_id' => '19',
                'city' => 'LA VEGA',

            ],
            [
                'city_code' => '418',
                'department_id' => '19',
                'city' => 'LOPEZ',

            ],
            [
                'city_code' => '450',
                'department_id' => '19',
                'city' => 'MERCADERES',

            ],
            [
                'city_code' => '455',
                'department_id' => '19',
                'city' => 'MIRANDA',

            ],
            [
                'city_code' => '473',
                'department_id' => '19',
                'city' => 'MORALES',

            ],
            [
                'city_code' => '513',
                'department_id' => '19',
                'city' => 'PADILLA',

            ],
            [
                'city_code' => '517',
                'department_id' => '19',
                'city' => 'PAEZ (BELALCAZAR)',

            ],
            [
                'city_code' => '532',
                'department_id' => '19',
                'city' => 'PATIA (EL BORDO)',

            ],
            [
                'city_code' => '533',
                'department_id' => '19',
                'city' => 'PIAMONTE',

            ],
            [
                'city_code' => '548',
                'department_id' => '19',
                'city' => 'PIENDAMO',

            ],
            [
                'city_code' => '573',
                'department_id' => '19',
                'city' => 'PUERTO TEJADA',

            ],
            [
                'city_code' => '585',
                'department_id' => '19',
                'city' => 'PURACE (COCONUCO)',

            ],
            [
                'city_code' => '622',
                'department_id' => '19',
                'city' => 'ROSAS',

            ],
            [
                'city_code' => '693',
                'department_id' => '19',
                'city' => 'SAN SEBASTIAN',

            ],
            [
                'city_code' => '698',
                'department_id' => '19',
                'city' => 'SANTANDER DE QUILICHAO',

            ],
            [
                'city_code' => '701',
                'department_id' => '19',
                'city' => 'SANTA ROSA',

            ],
            [
                'city_code' => '743',
                'department_id' => '19',
                'city' => 'SILVIA',

            ],
            [
                'city_code' => '760',
                'department_id' => '19',
                'city' => 'SOTARA (PAISPAMBA)',

            ],
            [
                'city_code' => '780',
                'department_id' => '19',
                'city' => 'SUAREZ',

            ],
            [
                'city_code' => '807',
                'department_id' => '19',
                'city' => 'TIMBIO',

            ],
            [
                'city_code' => '809',
                'department_id' => '19',
                'city' => 'TIMBIQUI',

            ],
            [
                'city_code' => '821',
                'department_id' => '19',
                'city' => 'TORIBIO',

            ],
            [
                'city_code' => '824',
                'department_id' => '19',
                'city' => 'TOTORO',

            ],
            [
                'city_code' => '845',
                'department_id' => '19',
                'city' => 'VILLARICA',

            ],
            [
                'city_code' => '001',
                'department_id' => '20',
                'city' => 'VALLEDUPAR',

            ],
            [
                'city_code' => '011',
                'department_id' => '20',
                'city' => 'AGUACHICA',

            ],
            [
                'city_code' => '013',
                'department_id' => '20',
                'city' => 'AGUSTIN CODAZZI',

            ],
            [
                'city_code' => '032',
                'department_id' => '20',
                'city' => 'ASTREA',

            ],
            [
                'city_code' => '045',
                'department_id' => '20',
                'city' => 'BECERRIL',

            ],
            [
                'city_code' => '060',
                'department_id' => '20',
                'city' => 'BOSCONIA',

            ],
            [
                'city_code' => '175',
                'department_id' => '20',
                'city' => 'CHIMICHAGUA',

            ],
            [
                'city_code' => '178',
                'department_id' => '20',
                'city' => 'CHIRIGUANA',

            ],
            [
                'city_code' => '228',
                'department_id' => '20',
                'city' => 'CURUMANI',

            ],
            [
                'city_code' => '238',
                'department_id' => '20',
                'city' => 'EL COPEY',

            ],
            [
                'city_code' => '250',
                'department_id' => '20',
                'city' => 'EL PASO',

            ],
            [
                'city_code' => '295',
                'department_id' => '20',
                'city' => 'GAMARRA',

            ],
            [
                'city_code' => '310',
                'department_id' => '20',
                'city' => 'GONZALEZ',

            ],
            [
                'city_code' => '383',
                'department_id' => '20',
                'city' => 'LA GLORIA',

            ],
            [
                'city_code' => '400',
                'department_id' => '20',
                'city' => 'LA JAGUA IBIRICO',

            ],
            [
                'city_code' => '443',
                'department_id' => '20',
                'city' => 'MANAURE',

            ],
            [
                'city_code' => '517',
                'department_id' => '20',
                'city' => 'PAILITAS',

            ],
            [
                'city_code' => '550',
                'department_id' => '20',
                'city' => 'PELAYA',

            ],
            [
                'city_code' => '570',
                'department_id' => '20',
                'city' => 'PUEBLO BELLO',

            ],
            [
                'city_code' => '614',
                'department_id' => '20',
                'city' => 'RIO DE ORO',

            ],
            [
                'city_code' => '621',
                'department_id' => '20',
                'city' => 'LA PAZ',

            ],
            [
                'city_code' => '710',
                'department_id' => '20',
                'city' => 'SAN ALBERTO',

            ],
            [
                'city_code' => '750',
                'department_id' => '20',
                'city' => 'SAN DIEGO',

            ],
            [
                'city_code' => '770',
                'department_id' => '20',
                'city' => 'SAN MARTIN',

            ],
            [
                'city_code' => '787',
                'department_id' => '20',
                'city' => 'TAMALAMEQUE',

            ],
            [
                'city_code' => '001',
                'department_id' => '23',
                'city' => 'MONTERIA',

            ],
            [
                'city_code' => '068',
                'department_id' => '23',
                'city' => 'AYAPEL',

            ],
            [
                'city_code' => '079',
                'department_id' => '23',
                'city' => 'BUENAVISTA',

            ],
            [
                'city_code' => '090',
                'department_id' => '23',
                'city' => 'CANALETE',

            ],
            [
                'city_code' => '162',
                'department_id' => '23',
                'city' => 'CERETE',

            ],
            [
                'city_code' => '168',
                'department_id' => '23',
                'city' => 'CHIMA',

            ],
            [
                'city_code' => '182',
                'department_id' => '23',
                'city' => 'CHINU',

            ],
            [
                'city_code' => '189',
                'department_id' => '23',
                'city' => 'CIENAGA DE ORO',

            ],
            [
                'city_code' => '300',
                'department_id' => '23',
                'city' => 'COTORRA',

            ],
            [
                'city_code' => '350',
                'department_id' => '23',
                'city' => 'LA APARTADA',

            ],
            [
                'city_code' => '417',
                'department_id' => '23',
                'city' => 'LORICA',

            ],
            [
                'city_code' => '419',
                'department_id' => '23',
                'city' => 'LOS CORDOBAS',

            ],
            [
                'city_code' => '464',
                'department_id' => '23',
                'city' => 'MOMIL',

            ],
            [
                'city_code' => '466',
                'department_id' => '23',
                'city' => 'MONTELIBANO',

            ],
            [
                'city_code' => '500',
                'department_id' => '23',
                'city' => 'MOÑITOS',

            ],
            [
                'city_code' => '555',
                'department_id' => '23',
                'city' => 'PLANETA RICA',

            ],
            [
                'city_code' => '570',
                'department_id' => '23',
                'city' => 'PUEBLO NUEVO',

            ],
            [
                'city_code' => '574',
                'department_id' => '23',
                'city' => 'PUERTO ESCONDIDO ',

            ],
            [
                'city_code' => '580 ',
                'department_id' => '23',
                'city' => 'PUERTO LIBERTADOR',

            ],
            [
                'city_code' => '586 ',
                'department_id' => '23',
                'city' => 'PURISIMA ',

            ],
            [
                'city_code' => '660',
                'department_id' => '23',
                'city' => 'SAHAGUN',

            ],
            [
                'city_code' => '670 ',
                'department_id' => '23',
                'city' => 'SAN ANDRES SOTAVENTO',

            ],
            [
                'city_code' => '672 ',
                'department_id' => '23',
                'city' => 'SAN ANTERO',

            ],
            [
                'city_code' => '675',
                'department_id' => '23',
                'city' => 'SAN BERNARDO DEL VIENTO ',

            ],
            [
                'city_code' => '678',
                'department_id' => '23',
                'city' => 'SAN CARLOS ',

            ],
            [
                'city_code' => '686 ',
                'department_id' => '23',
                'city' => 'SAN PELAYO',

            ],
            [
                'city_code' => '807',
                'department_id' => '23',
                'city' => 'TIERRALTA',

            ],
            [
                'city_code' => '855',
                'department_id' => '23',
                'city' => 'VALENCIA',

            ],
            [
                'city_code' => '001',
                'department_id' => '25',
                'city' => 'AGUA DE DIOS',

            ],
            [
                'city_code' => '019',
                'department_id' => '25',
                'city' => 'ALBAN',

            ],
            [
                'city_code' => '035',
                'department_id' => '25',
                'city' => 'ANAPOIMA',

            ],
            [
                'city_code' => '040',
                'department_id' => '25',
                'city' => 'ANOLAIMA',

            ],
            [
                'city_code' => '053',
                'department_id' => '25',
                'city' => 'ARBELAEZ',

            ],
            [
                'city_code' => '086',
                'department_id' => '25',
                'city' => 'BELTRAN',

            ],
            [
                'city_code' => '095',
                'department_id' => '25',
                'city' => 'BITUIMA',

            ],
            [
                'city_code' => '099',
                'department_id' => '25',
                'city' => 'BOJACA',

            ],
            [
                'city_code' => '120',
                'department_id' => '25',
                'city' => 'CABRERA',

            ],
            [
                'city_code' => '123',
                'department_id' => '25',
                'city' => 'CACHIPAY',

            ],
            [
                'city_code' => '126',
                'department_id' => '25',
                'city' => 'CAJICA',

            ],
            [
                'city_code' => '148',
                'department_id' => '25',
                'city' => 'CAPARRAPI',

            ],
            [
                'city_code' => '151',
                'department_id' => '25',
                'city' => 'CAQUEZA',

            ],
            [
                'city_code' => '154',
                'department_id' => '25',
                'city' => 'CARMEN DE CARUPA',

            ],
            [
                'city_code' => '168',
                'department_id' => '25',
                'city' => 'CHAGUANI',

            ],
            [
                'city_code' => '175',
                'department_id' => '25',
                'city' => 'CHIA',

            ],
            [
                'city_code' => '178',
                'department_id' => '25',
                'city' => 'CHIPAQUE',

            ],
            [
                'city_code' => '181',
                'department_id' => '25',
                'city' => 'CHOACHI',

            ],
            [
                'city_code' => '183',
                'department_id' => '25',
                'city' => 'CHOCONTA',

            ],
            [
                'city_code' => '200',
                'department_id' => '25',
                'city' => 'COGUA',

            ],
            [
                'city_code' => '214',
                'department_id' => '25',
                'city' => 'COTA',

            ],
            [
                'city_code' => '224',
                'department_id' => '25',
                'city' => 'CUCUNUBA',

            ],
            [
                'city_code' => '245',
                'department_id' => '25',
                'city' => 'EL COLEGIO',

            ],
            [
                'city_code' => '258',
                'department_id' => '25',
                'city' => 'EL PEÑON',

            ],
            [
                'city_code' => '260',
                'department_id' => '25',
                'city' => 'EL ROSAL',

            ],
            [
                'city_code' => '269',
                'department_id' => '25',
                'city' => 'FACATATIVA',

            ],
            [
                'city_code' => '279',
                'department_id' => '25',
                'city' => 'FOMEQUE',

            ],
            [
                'city_code' => '281',
                'department_id' => '25',
                'city' => 'FOSCA',

            ],
            [
                'city_code' => '286',
                'department_id' => '25',
                'city' => 'FUNZA',

            ],
            [
                'city_code' => '288',
                'department_id' => '25',
                'city' => 'FUQUENE',

            ],
            [
                'city_code' => '290',
                'department_id' => '25',
                'city' => 'FUSAGASUGA',

            ],
            [
                'city_code' => '293',
                'department_id' => '25',
                'city' => 'GACHALA',

            ],
            [
                'city_code' => '295',
                'department_id' => '25',
                'city' => 'GACHANCIPA',

            ],
            [
                'city_code' => '297',
                'department_id' => '25',
                'city' => 'GACHETA',

            ],
            [
                'city_code' => '299',
                'department_id' => '25',
                'city' => 'GAMA',

            ],
            [
                'city_code' => '307',
                'department_id' => '25',
                'city' => 'GIRARDOT',

            ],
            [
                'city_code' => '312',
                'department_id' => '25',
                'city' => 'GRANADA',

            ],
            [
                'city_code' => '317',
                'department_id' => '25',
                'city' => 'GUACHETA',

            ],
            [
                'city_code' => '320',
                'department_id' => '25',
                'city' => 'GUADUAS',

            ],
            [
                'city_code' => '322',
                'department_id' => '25',
                'city' => 'GUASCA',

            ],
            [
                'city_code' => '324',
                'department_id' => '25',
                'city' => 'GUATAQUI',

            ],
            [
                'city_code' => '326',
                'department_id' => '25',
                'city' => 'GUATAVITA',

            ],
            [
                'city_code' => '328',
                'department_id' => '25',
                'city' => 'GUAYABAL DE SIQUIMA',

            ],
            [
                'city_code' => '335',
                'department_id' => '25',
                'city' => 'GUAYABETAL',

            ],
            [
                'city_code' => '339',
                'department_id' => '25',
                'city' => 'GUTIERREZ',

            ],
            [
                'city_code' => '368',
                'department_id' => '25',
                'city' => 'JERUSALEN',

            ],
            [
                'city_code' => '372',
                'department_id' => '25',
                'city' => 'JUNIN',

            ],
            [
                'city_code' => '377',
                'department_id' => '25',
                'city' => 'LA CALERA',

            ],
            [
                'city_code' => '386',
                'department_id' => '25',
                'city' => 'LA MESA',

            ],
            [
                'city_code' => '394',
                'department_id' => '25',
                'city' => 'LA PALMA',

            ],
            [
                'city_code' => '398',
                'department_id' => '25',
                'city' => 'LA PEÑA',

            ],
            [
                'city_code' => '402',
                'department_id' => '25',
                'city' => 'LA VEGA',

            ],
            [
                'city_code' => '407',
                'department_id' => '25',
                'city' => 'LENGUAZAQUE',

            ],
            [
                'city_code' => '426',
                'department_id' => '25',
                'city' => 'MACHETA',

            ],
            [
                'city_code' => '430',
                'department_id' => '25',
                'city' => 'MADRID',

            ],
            [
                'city_code' => '436',
                'department_id' => '25',
                'city' => 'MANTA',

            ],
            [
                'city_code' => '438',
                'department_id' => '25',
                'city' => 'MEDINA',

            ],
            [
                'city_code' => '473',
                'department_id' => '25',
                'city' => 'MOSQUERA',

            ],
            [
                'city_code' => '483',
                'department_id' => '25',
                'city' => 'NARIÑO',

            ],
            [
                'city_code' => '486',
                'department_id' => '25',
                'city' => 'NEMOCON',

            ],
            [
                'city_code' => '488',
                'department_id' => '25',
                'city' => 'NILO',

            ],
            [
                'city_code' => '489',
                'department_id' => '25',
                'city' => 'NIMAIMA',

            ],
            [
                'city_code' => '491',
                'department_id' => '25',
                'city' => 'NOCAIMA',

            ],
            [
                'city_code' => '506',
                'department_id' => '25',
                'city' => 'VENECIA',

            ],
            [
                'city_code' => '513',
                'department_id' => '25',
                'city' => 'PACHO',

            ],
            [
                'city_code' => '518',
                'department_id' => '25',
                'city' => 'PAIME',

            ],
            [
                'city_code' => '524',
                'department_id' => '25',
                'city' => 'PANDI',

            ],
            [
                'city_code' => '530',
                'department_id' => '25',
                'city' => 'PARATEBUENO',

            ],
            [
                'city_code' => '535',
                'department_id' => '25',
                'city' => 'PASCA',

            ],
            [
                'city_code' => '572',
                'department_id' => '25',
                'city' => 'PURTO SALGAR',

            ],
            [
                'city_code' => '580',
                'department_id' => '25',
                'city' => 'PULI',

            ],
            [
                'city_code' => '592',
                'department_id' => '25',
                'city' => 'QUEBRADANEGRA',

            ],
            [
                'city_code' => '594',
                'department_id' => '25',
                'city' => 'QUETAME',

            ],
            [
                'city_code' => '596',
                'department_id' => '25',
                'city' => 'QUIPILE',

            ],
            [
                'city_code' => '599',
                'department_id' => '25',
                'city' => 'APULO',

            ],
            [
                'city_code' => '612',
                'department_id' => '25',
                'city' => 'RICAURTE',

            ],
            [
                'city_code' => '645',
                'department_id' => '25',
                'city' => 'SAN ANTONIO DEL TEQUENDAMA',

            ],
            [
                'city_code' => '649',
                'department_id' => '25',
                'city' => 'SAN BERNARDO',

            ],
            [
                'city_code' => '653',
                'department_id' => '25',
                'city' => 'SAN CAYETANO',

            ],
            [
                'city_code' => '658',
                'department_id' => '25',
                'city' => 'SAN FRANCISCO',

            ],
            [
                'city_code' => '662',
                'department_id' => '25',
                'city' => 'SAN JUAN DE RIOSECO',

            ],
            [
                'city_code' => '718',
                'department_id' => '25',
                'city' => 'SASAIMA',

            ],
            [
                'city_code' => '736',
                'department_id' => '25',
                'city' => 'SESQUILE',

            ],
            [
                'city_code' => '740',
                'department_id' => '25',
                'city' => 'SIBATE',

            ],
            [
                'city_code' => '743',
                'department_id' => '25',
                'city' => 'SILVANIA',

            ],
            [
                'city_code' => '745',
                'department_id' => '25',
                'city' => 'SIMIJACA',

            ],
            [
                'city_code' => '754',
                'department_id' => '25',
                'city' => 'SOACHA',

            ],
            [
                'city_code' => '758',
                'department_id' => '25',
                'city' => 'SOPO',

            ],
            [
                'city_code' => '769',
                'department_id' => '25',
                'city' => 'SUBACHOQUE',

            ],
            [
                'city_code' => '772',
                'department_id' => '25',
                'city' => 'SUESCA',

            ],
            [
                'city_code' => '777',
                'department_id' => '25',
                'city' => 'SUPATA',

            ],
            [
                'city_code' => '779',
                'department_id' => '25',
                'city' => 'SUSA',

            ],
            [
                'city_code' => '781',
                'department_id' => '25',
                'city' => 'SUTATAUSA',

            ],
            [
                'city_code' => '785',
                'department_id' => '25',
                'city' => 'TABIO',

            ],
            [
                'city_code' => '793',
                'department_id' => '25',
                'city' => 'TAUSA',

            ],
            [
                'city_code' => '797',
                'department_id' => '25',
                'city' => 'TENA',

            ],
            [
                'city_code' => '799',
                'department_id' => '25',
                'city' => 'TENJO',

            ],
            [
                'city_code' => '805',
                'department_id' => '25',
                'city' => 'TIBACUY',

            ],
            [
                'city_code' => '807',
                'department_id' => '25',
                'city' => 'TIBIRITA',

            ],
            [
                'city_code' => '815',
                'department_id' => '25',
                'city' => 'TOCAIMA',

            ],
            [
                'city_code' => '823',
                'department_id' => '25',
                'city' => 'TOPAIPI',

            ],
            [
                'city_code' => '839',
                'department_id' => '25',
                'city' => 'UBALA',

            ],
            [
                'city_code' => '841',
                'department_id' => '25',
                'city' => 'UBAQUE',

            ],
            [
                'city_code' => '843',
                'department_id' => '25',
                'city' => 'UBATE',

            ],
            [
                'city_code' => '845',
                'department_id' => '25',
                'city' => 'UNE',

            ],
            [
                'city_code' => '851',
                'department_id' => '25',
                'city' => 'UTICA',

            ],
            [
                'city_code' => '862',
                'department_id' => '25',
                'city' => 'VERGARA',

            ],
            [
                'city_code' => '867',
                'department_id' => '25',
                'city' => 'VIANI',

            ],
            [
                'city_code' => '871',
                'department_id' => '25',
                'city' => 'VILLAGOMEZ',

            ],
            [
                'city_code' => '873',
                'department_id' => '25',
                'city' => 'VILLAPINZON',

            ],
            [
                'city_code' => '875',
                'department_id' => '25',
                'city' => 'VILLETA',

            ],
            [
                'city_code' => '878',
                'department_id' => '25',
                'city' => 'VIOTA',

            ],
            [
                'city_code' => '885',
                'department_id' => '25',
                'city' => 'YACOPI',

            ],
            [
                'city_code' => '898',
                'department_id' => '25',
                'city' => 'ZIPACON',

            ],
            [
                'city_code' => '899',
                'department_id' => '25',
                'city' => 'ZIPAQUIRA',

            ],
            [
                'city_code' => '001',
                'department_id' => '27',
                'city' => 'QUIBDO',

            ],
            [
                'city_code' => '006',
                'department_id' => '27',
                'city' => 'ACANDI',

            ],
            [
                'city_code' => '025',
                'department_id' => '27',
                'city' => 'ALTO BAUDO',

            ],
            [
                'city_code' => '050',
                'department_id' => '27',
                'city' => 'ATRATO',

            ],
            [
                'city_code' => '073',
                'department_id' => '27',
                'city' => 'BAGADO',

            ],
            [
                'city_code' => '075',
                'department_id' => '27',
                'city' => 'BAHIA SOLANO',

            ],
            [
                'city_code' => '077',
                'department_id' => '27',
                'city' => 'BAJO BAUDO',

            ],
            [
                'city_code' => '099',
                'department_id' => '27',
                'city' => 'BOJAYA',

            ],
            [
                'city_code' => '135',
                'department_id' => '27',
                'city' => 'CANTON DE SAN PABLO',

            ],
            [
                'city_code' => '205',
                'department_id' => '27',
                'city' => 'CONDOTO',

            ],
            [
                'city_code' => '245',
                'department_id' => '27',
                'city' => 'EL CARMEN DE ATRATO',

            ],
            [
                'city_code' => '250',
                'department_id' => '27',
                'city' => 'LITORAL DEL BAJO SAN JUAN',

            ],
            [
                'city_code' => '361',
                'department_id' => '27',
                'city' => 'ISTMINA',

            ],
            [
                'city_code' => '372',
                'department_id' => '27',
                'city' => 'JURADO',

            ],
            [
                'city_code' => '413',
                'department_id' => '27',
                'city' => 'LLORO',

            ],
            [
                'city_code' => '425',
                'department_id' => '27',
                'city' => 'MEDIO ATRATO',

            ],
            [
                'city_code' => '430',
                'department_id' => '27',
                'city' => 'MEDIO BAUDO',

            ],
            [
                'city_code' => '491',
                'department_id' => '27',
                'city' => 'NOVITA',

            ],
            [
                'city_code' => '495',
                'department_id' => '27',
                'city' => 'NUQUI',

            ],
            [
                'city_code' => '600',
                'department_id' => '27',
                'city' => 'RIOQUITO',

            ],
            [
                'city_code' => '615',
                'department_id' => '27',
                'city' => 'RIOSUCIO',

            ],
            [
                'city_code' => '660',
                'department_id' => '27',
                'city' => 'SAN JOSE DEL PALMAR',

            ],
            [
                'city_code' => '745',
                'department_id' => '27',
                'city' => 'SIPI',

            ],
            [
                'city_code' => '787',
                'department_id' => '27',
                'city' => 'TADO',

            ],
            [
                'city_code' => '800',
                'department_id' => '27',
                'city' => 'UNGUIA',

            ],
            [
                'city_code' => '810',
                'department_id' => '27',
                'city' => 'UNION PANAMERICANA',

            ],
            [
                'city_code' => '001',
                'department_id' => '41',
                'city' => 'NEIVA',

            ],
            [
                'city_code' => '006',
                'department_id' => '41',
                'city' => 'ACEVEDO',

            ],
            [
                'city_code' => '013',
                'department_id' => '41',
                'city' => 'AGRADO',

            ],
            [
                'city_code' => '016',
                'department_id' => '41',
                'city' => 'AIPE',

            ],
            [
                'city_code' => '020',
                'department_id' => '41',
                'city' => 'ALGECIRAS',

            ],
            [
                'city_code' => '026',
                'department_id' => '41',
                'city' => 'ALTAMIRA',

            ],
            [
                'city_code' => '078',
                'department_id' => '41',
                'city' => 'BARAYA',

            ],
            [
                'city_code' => '132',
                'department_id' => '41',
                'city' => 'CAMPOALEGRE',

            ],
            [
                'city_code' => '206',
                'department_id' => '41',
                'city' => 'COLOMBIA',

            ],
            [
                'city_code' => '244',
                'department_id' => '41',
                'city' => 'ELIAS',

            ],
            [
                'city_code' => '298',
                'department_id' => '41',
                'city' => 'GARZON',

            ],
            [
                'city_code' => '306',
                'department_id' => '41',
                'city' => 'GIGANTE',

            ],
            [
                'city_code' => '319',
                'department_id' => '05',
                'city' => 'GUADALUPE',

            ],
            [
                'city_code' => '349',
                'department_id' => '41',
                'city' => 'HOBO',

            ],
            [
                'city_code' => '357',
                'department_id' => '41',
                'city' => 'IQUIRA',

            ],
            [
                'city_code' => '359',
                'department_id' => '41',
                'city' => 'ISNOS',

            ],
            [
                'city_code' => '378',
                'department_id' => '41',
                'city' => 'LA ARGENTINA',

            ],
            [
                'city_code' => '396',
                'department_id' => '41',
                'city' => 'LA PLATA',

            ],
            [
                'city_code' => '483',
                'department_id' => '41',
                'city' => 'NATAGA',

            ],
            [
                'city_code' => '503',
                'department_id' => '41',
                'city' => 'OPORAPA',

            ],
            [
                'city_code' => '518',
                'department_id' => '41',
                'city' => 'PAICOL',

            ],
            [
                'city_code' => '524',
                'department_id' => '41',
                'city' => 'PALERMO',

            ],
            [
                'city_code' => '530',
                'department_id' => '41',
                'city' => 'PALESTINA',

            ],
            [
                'city_code' => '548',
                'department_id' => '41',
                'city' => 'PITAL',

            ],
            [
                'city_code' => '551',
                'department_id' => '41',
                'city' => 'PITALITO',

            ],
            [
                'city_code' => '615',
                'department_id' => '41',
                'city' => 'RIVERA',

            ],
            [
                'city_code' => '660',
                'department_id' => '41',
                'city' => 'SALADOBLANCO',

            ],
            [
                'city_code' => '668',
                'department_id' => '41',
                'city' => 'SAN AGUSTIN',

            ],
            [
                'city_code' => '676',
                'department_id' => '41',
                'city' => 'SANTA MARIA',

            ],
            [
                'city_code' => '770',
                'department_id' => '41',
                'city' => 'SUAZA',

            ],
            [
                'city_code' => '791',
                'department_id' => '41',
                'city' => 'TARQUI',

            ],
            [
                'city_code' => '797',
                'department_id' => '41',
                'city' => 'TESALIA',

            ],
            [
                'city_code' => '799',
                'department_id' => '41',
                'city' => 'TELLO',

            ],
            [
                'city_code' => '801',
                'department_id' => '41',
                'city' => 'TERUEL',

            ],
            [
                'city_code' => '807',
                'department_id' => '41',
                'city' => 'TIMANA',

            ],
            [
                'city_code' => '872',
                'department_id' => '41',
                'city' => 'VILLAVIEJA',

            ],
            [
                'city_code' => '885',
                'department_id' => '41',
                'city' => 'YAGUARA',

            ],
            [
                'city_code' => '001',
                'department_id' => '44',
                'city' => 'RIOACHA',

            ],
            [
                'city_code' => '078',
                'department_id' => '44',
                'city' => 'BARRANCAS',

            ],
            [
                'city_code' => '090',
                'department_id' => '44',
                'city' => 'DIBULLA',

            ],
            [
                'city_code' => '098',
                'department_id' => '44',
                'city' => 'DISTRACCION',

            ],
            [
                'city_code' => '110',
                'department_id' => '44',
                'city' => 'EL MOLINO',

            ],
            [
                'city_code' => '279',
                'department_id' => '44',
                'city' => 'FONSECA',

            ],
            [
                'city_code' => '378',
                'department_id' => '44',
                'city' => 'HATONUEVO',

            ],
            [
                'city_code' => '420',
                'department_id' => '44',
                'city' => 'LA JAGUA DEL PILAR',

            ],
            [
                'city_code' => '430',
                'department_id' => '44',
                'city' => 'MAICAO',

            ],
            [
                'city_code' => '560',
                'department_id' => '44',
                'city' => 'MANAURE',

            ],
            [
                'city_code' => '650',
                'department_id' => '44',
                'city' => 'SAN JUAN DEL CESAR',

            ],
            [
                'city_code' => '847',
                'department_id' => '44',
                'city' => 'URIBIA',

            ],
            [
                'city_code' => '855',
                'department_id' => '44',
                'city' => 'URUMITA',

            ],
            [
                'city_code' => '874',
                'department_id' => '44',
                'city' => 'VILANUEVA',

            ],
            [
                'city_code' => '001',
                'department_id' => '47',
                'city' => 'SANTA MARTA',

            ],
            [
                'city_code' => '030',
                'department_id' => '47',
                'city' => 'ALGARROBO',

            ],
            [
                'city_code' => '053',
                'department_id' => '47',
                'city' => 'ARACATACA',

            ],
            [
                'city_code' => '058',
                'department_id' => '47',
                'city' => 'ARIGUANI',

            ],
            [
                'city_code' => '161',
                'department_id' => '47',
                'city' => 'CERRO SAN ANTONIO',

            ],
            [
                'city_code' => '170',
                'department_id' => '47',
                'city' => 'CHIVOLO',

            ],
            [
                'city_code' => '189',
                'department_id' => '47',
                'city' => 'CIENAGA',

            ],
            [
                'city_code' => '205',
                'department_id' => '47',
                'city' => 'CONCORDIA',

            ],
            [
                'city_code' => '245',
                'department_id' => '47',
                'city' => 'EL BANCO',

            ],
            [
                'city_code' => '258',
                'department_id' => '47',
                'city' => 'EL PIÑON',

            ],
            [
                'city_code' => '268',
                'department_id' => '47',
                'city' => 'EL RETEN',

            ],
            [
                'city_code' => '288',
                'department_id' => '47',
                'city' => 'FUNDACION',

            ],
            [
                'city_code' => '318',
                'department_id' => '47',
                'city' => 'GUAMAL',

            ],
            [
                'city_code' => '541',
                'department_id' => '47',
                'city' => 'PEDRAZA',

            ],
            [
                'city_code' => '545',
                'department_id' => '47',
                'city' => 'PIJIÑO',

            ],
            [
                'city_code' => '551',
                'department_id' => '47',
                'city' => 'PIVIJAY',

            ],
            [
                'city_code' => '555',
                'department_id' => '47',
                'city' => 'PLATO',

            ],
            [
                'city_code' => '570',
                'department_id' => '47',
                'city' => 'PUEBLOVIEJO',

            ],
            [
                'city_code' => '605',
                'department_id' => '47',
                'city' => 'REMOLINO',

            ],
            [
                'city_code' => '660',
                'department_id' => '47',
                'city' => 'SABANAS DE SAN ANGEL',

            ],
            [
                'city_code' => '675',
                'department_id' => '47',
                'city' => 'SALAMINA',

            ],
            [
                'city_code' => '692',
                'department_id' => '47',
                'city' => 'SAN SEBASTIAN DE BUENAVISTA',

            ],
            [
                'city_code' => '703',
                'department_id' => '47',
                'city' => 'SAN ZENON',

            ],
            [
                'city_code' => '707',
                'department_id' => '47',
                'city' => 'SANTA ANA',

            ],
            [
                'city_code' => '745',
                'department_id' => '47',
                'city' => 'SITIONUEVO',

            ],
            [
                'city_code' => '798',
                'department_id' => '47',
                'city' => 'TENERIFE',

            ],
            [
                'city_code' => '001',
                'department_id' => '50',
                'city' => 'VILLAVICENCIO',

            ],
            [
                'city_code' => '006',
                'department_id' => '50',
                'city' => 'ACACIAS',

            ],
            [
                'city_code' => '110',
                'department_id' => '50',
                'city' => 'BARRANCA DE UPIA',

            ],
            [
                'city_code' => '124',
                'department_id' => '50',
                'city' => 'CARABUYARO',

            ],
            [
                'city_code' => '150',
                'department_id' => '50',
                'city' => 'CASTILLA LA NUEVA',

            ],
            [
                'city_code' => '223',
                'department_id' => '50',
                'city' => 'SAN LUIS DE CUBARRAL',

            ],
            [
                'city_code' => '226',
                'department_id' => '50',
                'city' => 'CUMARAL',

            ],
            [
                'city_code' => '245',
                'department_id' => '50',
                'city' => 'EL CALVARIO',

            ],
            [
                'city_code' => '251',
                'department_id' => '50',
                'city' => 'EL CASTILLO',

            ],
            [
                'city_code' => '270',
                'department_id' => '50',
                'city' => 'EL DORADO',

            ],
            [
                'city_code' => '287',
                'department_id' => '50',
                'city' => 'FUENTE DE ORO',

            ],
            [
                'city_code' => '313',
                'department_id' => '50',
                'city' => 'GRANADA',

            ],
            [
                'city_code' => '318',
                'department_id' => '50',
                'city' => 'GUAMAL',

            ],
            [
                'city_code' => '325',
                'department_id' => '50',
                'city' => 'MAPIRIPAN',

            ],
            [
                'city_code' => '330',
                'department_id' => '50',
                'city' => 'MESETAS',

            ],
            [
                'city_code' => '350',
                'department_id' => '50',
                'city' => 'LA MACARENA',

            ],
            [
                'city_code' => '370',
                'department_id' => '50',
                'city' => 'LA URIBE',

            ],
            [
                'city_code' => '400',
                'department_id' => '50',
                'city' => 'LEJANIAS',

            ],
            [
                'city_code' => '450',
                'department_id' => '50',
                'city' => 'PUERTO CONCORDIA',

            ],
            [
                'city_code' => '568',
                'department_id' => '50',
                'city' => 'PUERTO GAITAN',

            ],
            [
                'city_code' => '573',
                'department_id' => '50',
                'city' => 'PUERTO LOPEZ',

            ],
            [
                'city_code' => '577',
                'department_id' => '50',
                'city' => 'PUERTO LLERAS',

            ],
            [
                'city_code' => '590',
                'department_id' => '50',
                'city' => 'PUERTO RICO',

            ],
            [
                'city_code' => '606',
                'department_id' => '50',
                'city' => 'RESTREPO',

            ],
            [
                'city_code' => '680',
                'department_id' => '50',
                'city' => 'SAN CARLOS DE GUAROA',

            ],
            [
                'city_code' => '683',
                'department_id' => '50',
                'city' => 'SAN JUAN DE ARAMA',

            ],
            [
                'city_code' => '686',
                'department_id' => '50',
                'city' => 'SAN JUANITO',

            ],
            [
                'city_code' => '689',
                'department_id' => '50',
                'city' => 'SAN MARTIN',

            ],
            [
                'city_code' => '711',
                'department_id' => '50',
                'city' => 'VISTA HERMOSA',

            ],
            [
                'city_code' => '001',
                'department_id' => '52',
                'city' => 'SAN JUAN DE PASTO',

            ],
            [
                'city_code' => '019',
                'department_id' => '52',
                'city' => 'ALBAN',

            ],
            [
                'city_code' => '022',
                'department_id' => '52',
                'city' => 'ALDANA',

            ],
            [
                'city_code' => '036',
                'department_id' => '52',
                'city' => 'ANCUYA',

            ],
            [
                'city_code' => '051',
                'department_id' => '52',
                'city' => 'ARBOLEDA',

            ],
            [
                'city_code' => '079',
                'department_id' => '52',
                'city' => 'BARBACOAS',

            ],
            [
                'city_code' => '083',
                'department_id' => '52',
                'city' => 'BELEN',

            ],
            [
                'city_code' => '110',
                'department_id' => '52',
                'city' => 'BUESACO',

            ],
            [
                'city_code' => '203',
                'department_id' => '52',
                'city' => 'COLON',

            ],
            [
                'city_code' => '207',
                'department_id' => '52',
                'city' => 'CONSACA',

            ],
            [
                'city_code' => '210',
                'department_id' => '52',
                'city' => 'CONTADERO',

            ],
            [
                'city_code' => '215',
                'department_id' => '52',
                'city' => 'CORDOBA',

            ],
            [
                'city_code' => '224',
                'department_id' => '52',
                'city' => 'CUASPUD',

            ],
            [
                'city_code' => '227',
                'department_id' => '52',
                'city' => 'CUMBAL',

            ],
            [
                'city_code' => '233',
                'department_id' => '52',
                'city' => 'CUMBITARA',

            ],
            [
                'city_code' => '240',
                'department_id' => '52',
                'city' => 'CHACHAGUI',

            ],
            [
                'city_code' => '250',
                'department_id' => '52',
                'city' => 'EL CHARCO',

            ],
            [
                'city_code' => '254',
                'department_id' => '52',
                'city' => 'EL PEÑOL',

            ],
            [
                'city_code' => '256',
                'department_id' => '52',
                'city' => 'EL ROSARIO',

            ],
            [
                'city_code' => '258',
                'department_id' => '52',
                'city' => 'EL TABLON',

            ],
            [
                'city_code' => '260',
                'department_id' => '52',
                'city' => 'EL TAMBO',

            ],
            [
                'city_code' => '287',
                'department_id' => '52',
                'city' => 'FUNES',

            ],
            [
                'city_code' => '317',
                'department_id' => '52',
                'city' => 'GUACHUCAL',

            ],
            [
                'city_code' => '320',
                'department_id' => '52',
                'city' => 'GUAITARILLA',

            ],
            [
                'city_code' => '323',
                'department_id' => '52',
                'city' => 'GUALMATAN',

            ],
            [
                'city_code' => '352',
                'department_id' => '52',
                'city' => 'ILES',

            ],
            [
                'city_code' => '354',
                'department_id' => '52',
                'city' => 'IMUES',

            ],
            [
                'city_code' => '356',
                'department_id' => '52',
                'city' => 'IPIALES',

            ],
            [
                'city_code' => '378',
                'department_id' => '52',
                'city' => 'LA CRUZ',

            ],
            [
                'city_code' => '381',
                'department_id' => '52',
                'city' => 'LA FLORIDA',

            ],
            [
                'city_code' => '385',
                'department_id' => '52',
                'city' => 'LA LLANADA',

            ],
            [
                'city_code' => '390',
                'department_id' => '52',
                'city' => 'LA TOLA',

            ],
            [
                'city_code' => '399',
                'department_id' => '52',
                'city' => 'LA UNION',

            ],
            [
                'city_code' => '405',
                'department_id' => '52',
                'city' => 'LEIVA',

            ],
            [
                'city_code' => '411',
                'department_id' => '52',
                'city' => 'LINARES',

            ],
            [
                'city_code' => '418',
                'department_id' => '52',
                'city' => 'LOS ANDES',

            ],
            [
                'city_code' => '427',
                'department_id' => '52',
                'city' => 'MAGUI',

            ],
            [
                'city_code' => '435',
                'department_id' => '52',
                'city' => 'MALLAMA',

            ],
            [
                'city_code' => '473',
                'department_id' => '52',
                'city' => 'MOSQUERA',

            ],
            [
                'city_code' => '490',
                'department_id' => '52',
                'city' => 'OLAYA HERRERA',

            ],
            [
                'city_code' => '506',
                'department_id' => '52',
                'city' => 'OSPINA',

            ],
            [
                'city_code' => '520',
                'department_id' => '52',
                'city' => 'FRANCISCO PIZARRO',

            ],
            [
                'city_code' => '540',
                'department_id' => '52',
                'city' => 'POLICARPA',

            ],
            [
                'city_code' => '560',
                'department_id' => '52',
                'city' => 'POTOSI',

            ],
            [
                'city_code' => '565',
                'department_id' => '52',
                'city' => 'PROVIDENCIA',

            ],
            [
                'city_code' => '573',
                'department_id' => '52',
                'city' => 'PUERRES',

            ],
            [
                'city_code' => '585',
                'department_id' => '52',
                'city' => 'PUPIALES',

            ],
            [
                'city_code' => '612',
                'department_id' => '52',
                'city' => 'RICAURTE',

            ],
            [
                'city_code' => '621',
                'department_id' => '52',
                'city' => 'ROBERTO PAYAN',

            ],
            [
                'city_code' => '678',
                'department_id' => '52',
                'city' => 'SAMANIEGO',

            ],
            [
                'city_code' => '683',
                'department_id' => '52',
                'city' => 'SANDONA',

            ],
            [
                'city_code' => '685',
                'department_id' => '52',
                'city' => 'SAN BERNARDO',

            ],
            [
                'city_code' => '687',
                'department_id' => '52',
                'city' => 'SAN LORENZO',

            ],
            [
                'city_code' => '693',
                'department_id' => '52',
                'city' => 'SAN PABLO',

            ],
            [
                'city_code' => '694',
                'department_id' => '52',
                'city' => 'SAN PEDRO DE CARTAGO',

            ],
            [
                'city_code' => '696',
                'department_id' => '52',
                'city' => 'SANTA BARBARA',

            ],
            [
                'city_code' => '699',
                'department_id' => '52',
                'city' => 'SANTA CRUZ',

            ],
            [
                'city_code' => '720',
                'department_id' => '52',
                'city' => 'SAPUYES',

            ],
            [
                'city_code' => '786',
                'department_id' => '52',
                'city' => 'TAMINANGO',

            ],
            [
                'city_code' => '788',
                'department_id' => '52',
                'city' => 'TANGUA',

            ],
            [
                'city_code' => '835',
                'department_id' => '52',
                'city' => 'TUMACO',

            ],
            [
                'city_code' => '838',
                'department_id' => '52',
                'city' => 'TUQUERRES',

            ],
            [
                'city_code' => '885',
                'department_id' => '52',
                'city' => 'YACUANQUER',

            ],
            [
                'city_code' => '001',
                'department_id' => '54',
                'city' => 'CUCUTA',

            ],
            [
                'city_code' => '003',
                'department_id' => '54',
                'city' => 'ABREGO',

            ],
            [
                'city_code' => '051',
                'department_id' => '54',
                'city' => 'ARBOLEDAS',

            ],
            [
                'city_code' => '099',
                'department_id' => '54',
                'city' => 'BOCHALEMA',

            ],
            [
                'city_code' => '109',
                'department_id' => '54',
                'city' => 'BUCARASICA',

            ],
            [
                'city_code' => '125',
                'department_id' => '54',
                'city' => 'CACOTA',

            ],
            [
                'city_code' => '128',
                'department_id' => '54',
                'city' => 'CACHIRA',

            ],
            [
                'city_code' => '172',
                'department_id' => '54',
                'city' => 'CHINACOTA',

            ],
            [
                'city_code' => '174',
                'department_id' => '54',
                'city' => 'CHITAGA',

            ],
            [
                'city_code' => '206',
                'department_id' => '54',
                'city' => 'CONVENCION',

            ],
            [
                'city_code' => '223',
                'department_id' => '54',
                'city' => 'CUCUTILLA',

            ],
            [
                'city_code' => '239',
                'department_id' => '54',
                'city' => 'DURANIA',

            ],
            [
                'city_code' => '245',
                'department_id' => '54',
                'city' => 'EL CARMEN',

            ],
            [
                'city_code' => '250',
                'department_id' => '54',
                'city' => 'EL TARRA',

            ],
            [
                'city_code' => '261',
                'department_id' => '54',
                'city' => 'ZULIA',

            ],
            [
                'city_code' => '313',
                'department_id' => '54',
                'city' => 'GRAMALOTE',

            ],
            [
                'city_code' => '344',
                'department_id' => '54',
                'city' => 'HACARI',

            ],
            [
                'city_code' => '347',
                'department_id' => '54',
                'city' => 'HERRAN',

            ],
            [
                'city_code' => '377',
                'department_id' => '54',
                'city' => 'LABATECA',

            ],
            [
                'city_code' => '385',
                'department_id' => '54',
                'city' => 'LA ESPERANZA',

            ],
            [
                'city_code' => '398',
                'department_id' => '54',
                'city' => 'LA PLAYA',

            ],
            [
                'city_code' => '405',
                'department_id' => '54',
                'city' => 'LOS PATIOS',

            ],
            [
                'city_code' => '418',
                'department_id' => '54',
                'city' => 'LOURDES',

            ],
            [
                'city_code' => '480',
                'department_id' => '54',
                'city' => 'MUTISCUA',

            ],
            [
                'city_code' => '498',
                'department_id' => '54',
                'city' => 'OCAÑA',

            ],
            [
                'city_code' => '518',
                'department_id' => '54',
                'city' => 'PAMPLONA',

            ],
            [
                'city_code' => '520',
                'department_id' => '54',
                'city' => 'PAMPLONITA',

            ],
            [
                'city_code' => '553',
                'department_id' => '54',
                'city' => 'PUERTO SANTANDER',

            ],
            [
                'city_code' => '599',
                'department_id' => '54',
                'city' => 'RAGONVALIA',

            ],
            [
                'city_code' => '660',
                'department_id' => '54',
                'city' => 'SALAZAR',

            ],
            [
                'city_code' => '670',
                'department_id' => '54',
                'city' => 'SAN CALIXTO',

            ],
            [
                'city_code' => '673',
                'department_id' => '54',
                'city' => 'SAN CAYETANO',

            ],
            [
                'city_code' => '680',
                'department_id' => '54',
                'city' => 'SANTIAGO',

            ],
            [
                'city_code' => '720',
                'department_id' => '54',
                'city' => 'SARDINATA',

            ],
            [
                'city_code' => '743',
                'department_id' => '54',
                'city' => 'SILOS',

            ],
            [
                'city_code' => '800',
                'department_id' => '54',
                'city' => 'TEORAMA',

            ],
            [
                'city_code' => '810',
                'department_id' => '54',
                'city' => 'TIBU',

            ],
            [
                'city_code' => '820',
                'department_id' => '54',
                'city' => 'TOLEDO',

            ],
            [
                'city_code' => '871',
                'department_id' => '54',
                'city' => 'VILLACARO',

            ],
            [
                'city_code' => '874',
                'department_id' => '54',
                'city' => 'VILLA DEL ROSARIO',

            ],
            [
                'city_code' => '001',
                'department_id' => '63',
                'city' => 'ARMENIA',

            ],
            [
                'city_code' => '111',
                'department_id' => '63',
                'city' => 'BUENAVISTA',

            ],
            [
                'city_code' => '130',
                'department_id' => '63',
                'city' => 'CALARCA',

            ],
            [
                'city_code' => '190',
                'department_id' => '63',
                'city' => 'CIRCASIA',

            ],
            [
                'city_code' => '212',
                'department_id' => '63',
                'city' => 'CORDOBA',

            ],
            [
                'city_code' => '272',
                'department_id' => '63',
                'city' => 'FILANDIA',

            ],
            [
                'city_code' => '302',
                'department_id' => '63',
                'city' => 'GENOVA',

            ],
            [
                'city_code' => '401',
                'department_id' => '63',
                'city' => 'LA TEBAIDA',

            ],
            [
                'city_code' => '470',
                'department_id' => '63',
                'city' => 'MONTENEGRO',

            ],
            [
                'city_code' => '548',
                'department_id' => '63',
                'city' => 'PIJAO',

            ],
            [
                'city_code' => '594',
                'department_id' => '63',
                'city' => 'QUIMBAYA',

            ],
            [
                'city_code' => '690',
                'department_id' => '63',
                'city' => 'SALENTO',

            ],
            [
                'city_code' => '001',
                'department_id' => '66',
                'city' => 'PEREIRA',

            ],
            [
                'city_code' => '045',
                'department_id' => '66',
                'city' => 'APIA',

            ],
            [
                'city_code' => '075',
                'department_id' => '66',
                'city' => 'BALBOA',

            ],
            [
                'city_code' => '088',
                'department_id' => '66',
                'city' => 'BELEN DE UMBRIA',

            ],
            [
                'city_code' => '170',
                'department_id' => '66',
                'city' => 'DOS QUEBRADAS',

            ],
            [
                'city_code' => '318',
                'department_id' => '66',
                'city' => 'GUATICA',

            ],
            [
                'city_code' => '383',
                'department_id' => '66',
                'city' => 'LA CELIA',

            ],
            [
                'city_code' => '400',
                'department_id' => '66',
                'city' => 'LA VIRGINIA',

            ],
            [
                'city_code' => '440',
                'department_id' => '66',
                'city' => 'MARSELLA',

            ],
            [
                'city_code' => '456',
                'department_id' => '66',
                'city' => 'MISTRATO',

            ],
            [
                'city_code' => '572',
                'department_id' => '66',
                'city' => 'PUEBLO RICO',

            ],
            [
                'city_code' => '594',
                'department_id' => '66',
                'city' => 'QUINCHIA',

            ],
            [
                'city_code' => '682',
                'department_id' => '66',
                'city' => 'SANTA ROSA DE CABAL',

            ],
            [
                'city_code' => '687',
                'department_id' => '66',
                'city' => 'SANTUARIO',

            ],
            [
                'city_code' => '001',
                'department_id' => '68',
                'city' => 'BUCARAMANGA',

            ],
            [
                'city_code' => '013',
                'department_id' => '68',
                'city' => 'AGUADA',

            ],
            [
                'city_code' => '020',
                'department_id' => '68',
                'city' => 'ALBANIA',

            ],
            [
                'city_code' => '051',
                'department_id' => '68',
                'city' => 'ARATOCA',

            ],
            [
                'city_code' => '077',
                'department_id' => '68',
                'city' => 'BARBOSA',

            ],
            [
                'city_code' => '079',
                'department_id' => '68',
                'city' => 'BARICHARA',

            ],
            [
                'city_code' => '081',
                'department_id' => '68',
                'city' => 'BARRANCABERMEJA',

            ],
            [
                'city_code' => '092',
                'department_id' => '68',
                'city' => 'BETULIA',

            ],
            [
                'city_code' => '101',
                'department_id' => '68',
                'city' => 'BOLIVR',

            ],
            [
                'city_code' => '121',
                'department_id' => '68',
                'city' => 'CABRERA',

            ],
            [
                'city_code' => '132',
                'department_id' => '68',
                'city' => 'CALIFORNIA',

            ],
            [
                'city_code' => '147',
                'department_id' => '68',
                'city' => 'CAPITANEJO',

            ],
            [
                'city_code' => '152',
                'department_id' => '68',
                'city' => 'CARCASI',

            ],
            [
                'city_code' => '160',
                'department_id' => '68',
                'city' => 'CEPITA',

            ],
            [
                'city_code' => '162',
                'department_id' => '68',
                'city' => 'CERRITO',

            ],
            [
                'city_code' => '167',
                'department_id' => '68',
                'city' => 'CHARALA',

            ],
            [
                'city_code' => '169',
                'department_id' => '68',
                'city' => 'CHARTA',

            ],
            [
                'city_code' => '176',
                'department_id' => '68',
                'city' => 'CHIMA',

            ],
            [
                'city_code' => '179',
                'department_id' => '68',
                'city' => 'CHIPATA',

            ],
            [
                'city_code' => '190',
                'department_id' => '68',
                'city' => 'CIMITARRA',

            ],
            [
                'city_code' => '207',
                'department_id' => '68',
                'city' => 'CONCEPCION',

            ],
            [
                'city_code' => '209',
                'department_id' => '68',
                'city' => 'CONFINES',

            ],
            [
                'city_code' => '211',
                'department_id' => '68',
                'city' => 'CONTRATACION',

            ],
            [
                'city_code' => '217',
                'department_id' => '68',
                'city' => 'COROMORO',

            ],
            [
                'city_code' => '229',
                'department_id' => '68',
                'city' => 'CURITI',

            ],
            [
                'city_code' => '235',
                'department_id' => '68',
                'city' => 'EL CARMEN DE CHUCURY',

            ],
            [
                'city_code' => '245',
                'department_id' => '68',
                'city' => 'EL GUACAMAYO',

            ],
            [
                'city_code' => '250',
                'department_id' => '68',
                'city' => 'EL PEÑON',

            ],
            [
                'city_code' => '255',
                'department_id' => '68',
                'city' => 'EL PLAYON',

            ],
            [
                'city_code' => '264',
                'department_id' => '68',
                'city' => 'ENCINO',

            ],
            [
                'city_code' => '266',
                'department_id' => '68',
                'city' => 'ENCISO',

            ],
            [
                'city_code' => '271',
                'department_id' => '68',
                'city' => 'FLORIAN',

            ],
            [
                'city_code' => '276',
                'department_id' => '68',
                'city' => 'FLORIDABLANCA',

            ],
            [
                'city_code' => '296',
                'department_id' => '68',
                'city' => 'GALAN',

            ],
            [
                'city_code' => '298',
                'department_id' => '68',
                'city' => 'GAMBITA',

            ],
            [
                'city_code' => '307',
                'department_id' => '68',
                'city' => 'GIRON',

            ],
            [
                'city_code' => '318',
                'department_id' => '68',
                'city' => 'GUACA',

            ],
            [
                'city_code' => '320',
                'department_id' => '68',
                'city' => 'GUADALUPE',

            ],
            [
                'city_code' => '322',
                'department_id' => '68',
                'city' => 'GUAPOTA',

            ],
            [
                'city_code' => '324',
                'department_id' => '68',
                'city' => 'GUAVATA',

            ],
            [
                'city_code' => '327',
                'department_id' => '68',
                'city' => 'GUEPSA',

            ],
            [
                'city_code' => '344',
                'department_id' => '68',
                'city' => 'HATO',

            ],
            [
                'city_code' => '368',
                'department_id' => '68',
                'city' => 'JESUS MARIA',

            ],
            [
                'city_code' => '370',
                'department_id' => '68',
                'city' => 'JORDAN',

            ],
            [
                'city_code' => '377',
                'department_id' => '68',
                'city' => 'LA BELLEZA',

            ],
            [
                'city_code' => '385',
                'department_id' => '68',
                'city' => 'LANDAZURI',

            ],
            [
                'city_code' => '397',
                'department_id' => '68',
                'city' => 'LA PAZ',

            ],
            [
                'city_code' => '406',
                'department_id' => '68',
                'city' => 'LEBRIJA',

            ],
            [
                'city_code' => '418',
                'department_id' => '68',
                'city' => 'LOS SANTOS',

            ],
            [
                'city_code' => '425',
                'department_id' => '68',
                'city' => 'MACARAVITA',

            ],
            [
                'city_code' => '432',
                'department_id' => '68',
                'city' => 'MALAGA',

            ],
            [
                'city_code' => '444',
                'department_id' => '68',
                'city' => 'MATANZA',

            ],
            [
                'city_code' => '464',
                'department_id' => '68',
                'city' => 'MOGOTES',

            ],
            [
                'city_code' => '468',
                'department_id' => '68',
                'city' => 'MOLAGAVITA',

            ],
            [
                'city_code' => '498',
                'department_id' => '68',
                'city' => 'OCAMONTE',

            ],
            [
                'city_code' => '500',
                'department_id' => '68',
                'city' => 'ONZAGA',

            ],
            [
                'city_code' => '522',
                'department_id' => '68',
                'city' => 'PALMAR',

            ],
            [
                'city_code' => '524',
                'department_id' => '68',
                'city' => 'PALMAS DEL SOCORRO',

            ],
            [
                'city_code' => '533',
                'department_id' => '68',
                'city' => 'PARAMO',

            ],
            [
                'city_code' => '547',
                'department_id' => '68',
                'city' => 'PIEDECUESTA',

            ],
            [
                'city_code' => '549',
                'department_id' => '68',
                'city' => 'PINCHOTE',

            ],
            [
                'city_code' => '572',
                'department_id' => '68',
                'city' => 'PUENTE NACIONAL',

            ],
            [
                'city_code' => '573',
                'department_id' => '68',
                'city' => 'PUERTO PARRA',

            ],
            [
                'city_code' => '575',
                'department_id' => '68',
                'city' => 'PUERTO WILCHES',

            ],
            [
                'city_code' => '615',
                'department_id' => '68',
                'city' => 'RIONEGRO',

            ],
            [
                'city_code' => '655',
                'department_id' => '68',
                'city' => 'SABANA DE TORRES',

            ],
            [
                'city_code' => '669',
                'department_id' => '68',
                'city' => 'SAN ANDRES',

            ],
            [
                'city_code' => '673',
                'department_id' => '68',
                'city' => 'SAN BENITO',

            ],
            [
                'city_code' => '679',
                'department_id' => '68',
                'city' => 'SAN GIL',

            ],
            [
                'city_code' => '682',
                'department_id' => '68',
                'city' => 'SAN JOAQUIN',

            ],
            [
                'city_code' => '684',
                'department_id' => '68',
                'city' => 'SAN JOSE DE MIRANDA',

            ],
            [
                'city_code' => '686',
                'department_id' => '68',
                'city' => 'SAN MIGUEL',

            ],
            [
                'city_code' => '689',
                'department_id' => '68',
                'city' => 'SAN VICENTE DE CHUCURI',

            ],
            [
                'city_code' => '705',
                'department_id' => '68',
                'city' => 'SANTA BARBARA',

            ],
            [
                'city_code' => '720',
                'department_id' => '68',
                'city' => 'SANTA HELENA DEL OPON',

            ],
            [
                'city_code' => '745',
                'department_id' => '68',
                'city' => 'SIMACOTA',

            ],
            [
                'city_code' => '755',
                'department_id' => '68',
                'city' => 'SOCORRO',

            ],
            [
                'city_code' => '770',
                'department_id' => '68',
                'city' => 'SUAITA',

            ],
            [
                'city_code' => '773',
                'department_id' => '68',
                'city' => 'SUCRE',

            ],
            [
                'city_code' => '780',
                'department_id' => '68',
                'city' => 'SURATA',

            ],
            [
                'city_code' => '820',
                'department_id' => '68',
                'city' => 'TONA',

            ],
            [
                'city_code' => '855',
                'department_id' => '68',
                'city' => 'VALLE SAN JOSE',

            ],
            [
                'city_code' => '861',
                'department_id' => '68',
                'city' => 'VELEZ',

            ],
            [
                'city_code' => '867',
                'department_id' => '68',
                'city' => 'VETAS',

            ],
            [
                'city_code' => '872',
                'department_id' => '68',
                'city' => 'VILLANUEVA',

            ],
            [
                'city_code' => '895',
                'department_id' => '68',
                'city' => 'ZAPATOCA',

            ],
            [
                'city_code' => '001',
                'department_id' => '70',
                'city' => 'SINCELEJO',

            ],
            [
                'city_code' => '110',
                'department_id' => '70',
                'city' => 'BUENAVISTA',

            ],
            [
                'city_code' => '124',
                'department_id' => '70',
                'city' => 'CAMINITO',

            ],
            [
                'city_code' => '204',
                'department_id' => '70',
                'city' => 'COLOSO',

            ],
            [
                'city_code' => '215',
                'department_id' => '70',
                'city' => 'COROZAL',

            ],
            [
                'city_code' => '230',
                'department_id' => '70',
                'city' => 'CHALAN',

            ],
            [
                'city_code' => '235',
                'department_id' => '70',
                'city' => 'GALERAS',

            ],
            [
                'city_code' => '265',
                'department_id' => '70',
                'city' => 'GUARANDA',

            ],
            [
                'city_code' => '400',
                'department_id' => '70',
                'city' => 'LA UNION',

            ],
            [
                'city_code' => '418',
                'department_id' => '70',
                'city' => 'LOS PALMITOS',

            ],
            [
                'city_code' => '429',
                'department_id' => '70',
                'city' => 'MAJAGUAL',

            ],
            [
                'city_code' => '473',
                'department_id' => '70',
                'city' => 'MORROA',

            ],
            [
                'city_code' => '508',
                'department_id' => '70',
                'city' => 'OVEJAS',

            ],
            [
                'city_code' => '523',
                'department_id' => '70',
                'city' => 'PALMITO',

            ],
            [
                'city_code' => '670',
                'department_id' => '70',
                'city' => 'SAMPUES',

            ],
            [
                'city_code' => '678',
                'department_id' => '70',
                'city' => 'SAN BENITO ABAD',

            ],
            [
                'city_code' => '702',
                'department_id' => '70',
                'city' => 'SAN JUAN DE BETULIA',

            ],
            [
                'city_code' => '708',
                'department_id' => '70',
                'city' => 'SAN MARCOS',

            ],
            [
                'city_code' => '713',
                'department_id' => '70',
                'city' => 'SAN ONOFRE',

            ],
            [
                'city_code' => '717',
                'department_id' => '70',
                'city' => 'SAN PEDRO',

            ],
            [
                'city_code' => '742',
                'department_id' => '70',
                'city' => 'SINCE',

            ],
            [
                'city_code' => '771',
                'department_id' => '70',
                'city' => 'SUCRE',

            ],
            [
                'city_code' => '820',
                'department_id' => '70',
                'city' => 'TOLU',

            ],
            [
                'city_code' => '823',
                'department_id' => '70',
                'city' => 'TOLUVIEJO',

            ],
            [
                'city_code' => '001',
                'department_id' => '73',
                'city' => 'IBAGUE',

            ],
            [
                'city_code' => '024',
                'department_id' => '73',
                'city' => 'ALPUJARRA',

            ],
            [
                'city_code' => '026',
                'department_id' => '73',
                'city' => 'ALVARADO',

            ],
            [
                'city_code' => '030',
                'department_id' => '73',
                'city' => 'AMBALEMA',

            ],
            [
                'city_code' => '043',
                'department_id' => '73',
                'city' => 'ANZOATEGUI',

            ],
            [
                'city_code' => '055',
                'department_id' => '73',
                'city' => 'ARMERO GUAYABAL',

            ],
            [
                'city_code' => '067',
                'department_id' => '73',
                'city' => 'ATACO',

            ],
            [
                'city_code' => '124',
                'department_id' => '73',
                'city' => 'CAJAMARCA',

            ],
            [
                'city_code' => '148',
                'department_id' => '73',
                'city' => 'CARMEN APICALA',

            ],
            [
                'city_code' => '152',
                'department_id' => '73',
                'city' => 'CASABIANCA',

            ],
            [
                'city_code' => '168',
                'department_id' => '73',
                'city' => 'CHAPARRAL',

            ],
            [
                'city_code' => '200',
                'department_id' => '73',
                'city' => 'COELLO',

            ],
            [
                'city_code' => '217',
                'department_id' => '73',
                'city' => 'COYAIMA',

            ],
            [
                'city_code' => '226',
                'department_id' => '73',
                'city' => 'CUNDAY',

            ],
            [
                'city_code' => '236',
                'department_id' => '73',
                'city' => 'DOLORES',

            ],
            [
                'city_code' => '268',
                'department_id' => '73',
                'city' => 'ESPINAL',

            ],
            [
                'city_code' => '270',
                'department_id' => '73',
                'city' => 'FALAN',

            ],
            [
                'city_code' => '275',
                'department_id' => '73',
                'city' => 'FLANDES',

            ],
            [
                'city_code' => '283',
                'department_id' => '73',
                'city' => 'FRESNO',

            ],
            [
                'city_code' => '319',
                'department_id' => '73',
                'city' => 'GUAMO',

            ],
            [
                'city_code' => '347',
                'department_id' => '73',
                'city' => 'HERVEO',

            ],
            [
                'city_code' => '349',
                'department_id' => '73',
                'city' => 'HONDA',

            ],
            [
                'city_code' => '352',
                'department_id' => '73',
                'city' => 'ICONONZO',

            ],
            [
                'city_code' => '408',
                'department_id' => '73',
                'city' => 'LERIDA',

            ],
            [
                'city_code' => '411',
                'department_id' => '73',
                'city' => 'LIBANO',

            ],
            [
                'city_code' => '443',
                'department_id' => '73',
                'city' => 'MARIQUITA',

            ],
            [
                'city_code' => '449',
                'department_id' => '73',
                'city' => 'MELGAR',

            ],
            [
                'city_code' => '461',
                'department_id' => '73',
                'city' => 'MURILLO',

            ],
            [
                'city_code' => '483',
                'department_id' => '73',
                'city' => 'NATAGAIMA',

            ],
            [
                'city_code' => '504',
                'department_id' => '73',
                'city' => 'ORTEGA',

            ],
            [
                'city_code' => '520',
                'department_id' => '73',
                'city' => 'PALOCABILDO',

            ],
            [
                'city_code' => '547',
                'department_id' => '73',
                'city' => 'PIEDRAS',

            ],
            [
                'city_code' => '555',
                'department_id' => '73',
                'city' => 'PLANADAS',

            ],
            [
                'city_code' => '563',
                'department_id' => '73',
                'city' => 'PRADO',

            ],
            [
                'city_code' => '585',
                'department_id' => '73',
                'city' => 'PURIFICACION',

            ],
            [
                'city_code' => '616',
                'department_id' => '73',
                'city' => 'RIOBLANCO',

            ],
            [
                'city_code' => '622',
                'department_id' => '73',
                'city' => 'RONCESVALLES',

            ],
            [
                'city_code' => '624',
                'department_id' => '73',
                'city' => 'ROVIRA',

            ],
            [
                'city_code' => '671',
                'department_id' => '73',
                'city' => 'SALDAÑA',

            ],
            [
                'city_code' => '675',
                'department_id' => '73',
                'city' => 'SAN ANTONIO',

            ],
            [
                'city_code' => '678',
                'department_id' => '73',
                'city' => 'SAN LUIS',

            ],
            [
                'city_code' => '686',
                'department_id' => '73',
                'city' => 'SANTA ISABEL',

            ],
            [
                'city_code' => '770',
                'department_id' => '73',
                'city' => 'SUAREZ',

            ],
            [
                'city_code' => '854',
                'department_id' => '73',
                'city' => 'VALLE DE SAN JUAN',

            ],
            [
                'city_code' => '861',
                'department_id' => '73',
                'city' => 'VENADILLO',

            ],
            [
                'city_code' => '870',
                'department_id' => '73',
                'city' => 'VILLAHERMOSA',

            ],
            [
                'city_code' => '873',
                'department_id' => '73',
                'city' => 'VILLARICA',

            ],
            [
                'city_code' => '001',
                'department_id' => '76',
                'city' => 'SANTIAGO DE CALI',

            ],
            [
                'city_code' => '020',
                'department_id' => '76',
                'city' => 'ALCALA',

            ],
            [
                'city_code' => '036',
                'department_id' => '76',
                'city' => 'ANDALUCIA',

            ],
            [
                'city_code' => '041',
                'department_id' => '76',
                'city' => 'ANSERMANUEVO',

            ],
            [
                'city_code' => '054',
                'department_id' => '76',
                'city' => 'ARGELIA',

            ],
            [
                'city_code' => '100',
                'department_id' => '76',
                'city' => 'BOLIVAR',

            ],
            [
                'city_code' => '109',
                'department_id' => '76',
                'city' => 'BUENAVENTURA',

            ],
            [
                'city_code' => '111',
                'department_id' => '76',
                'city' => 'BUGA',

            ],
            [
                'city_code' => '113',
                'department_id' => '76',
                'city' => 'BUGALAGRANDE',

            ],
            [
                'city_code' => '122',
                'department_id' => '76',
                'city' => 'CAICEDONIA',

            ],
            [
                'city_code' => '126',
                'department_id' => '76',
                'city' => 'CALIMA',

            ],
            [
                'city_code' => '130',
                'department_id' => '76',
                'city' => 'CANDELARIA',

            ],
            [
                'city_code' => '147',
                'department_id' => '76',
                'city' => 'CARTAGO',

            ],
            [
                'city_code' => '233',
                'department_id' => '76',
                'city' => 'DAGUA',

            ],
            [
                'city_code' => '243',
                'department_id' => '76',
                'city' => 'EL AGUILA',

            ],
            [
                'city_code' => '246',
                'department_id' => '76',
                'city' => 'EL CAIRO',

            ],
            [
                'city_code' => '248',
                'department_id' => '76',
                'city' => 'EL CERRITO',

            ],
            [
                'city_code' => '250',
                'department_id' => '76',
                'city' => 'EL DOVIO',

            ],
            [
                'city_code' => '275',
                'department_id' => '76',
                'city' => 'FLORIDA',

            ],
            [
                'city_code' => '306',
                'department_id' => '76',
                'city' => 'GINEBRA',

            ],
            [
                'city_code' => '318',
                'department_id' => '76',
                'city' => 'GUACARI',

            ],
            [
                'city_code' => '364',
                'department_id' => '76',
                'city' => 'JAMUNDI',

            ],
            [
                'city_code' => '377',
                'department_id' => '76',
                'city' => 'LA CUMBRE',

            ],
            [
                'city_code' => '400',
                'department_id' => '76',
                'city' => 'LA UNION',

            ],
            [
                'city_code' => '403',
                'department_id' => '76',
                'city' => 'LA VICTORIA',

            ],
            [
                'city_code' => '497',
                'department_id' => '76',
                'city' => 'OBANDO',

            ],
            [
                'city_code' => '520',
                'department_id' => '76',
                'city' => 'PALMIRA',

            ],
            [
                'city_code' => '563',
                'department_id' => '76',
                'city' => 'PRADERA',

            ],
            [
                'city_code' => '606',
                'department_id' => '76',
                'city' => 'RESTREPO',

            ],
            [
                'city_code' => '616',
                'department_id' => '76',
                'city' => 'RIOFRIO',

            ],
            [
                'city_code' => '622',
                'department_id' => '76',
                'city' => 'ROLDANILLO',

            ],
            [
                'city_code' => '670',
                'department_id' => '76',
                'city' => 'SAN PEDRO',

            ],
            [
                'city_code' => '736',
                'department_id' => '76',
                'city' => 'SEVILLA',

            ],
            [
                'city_code' => '823',
                'department_id' => '76',
                'city' => 'TORO',

            ],
            [
                'city_code' => '828',
                'department_id' => '76',
                'city' => 'TRUJILLO',

            ],
            [
                'city_code' => '834',
                'department_id' => '76',
                'city' => 'TULUA',

            ],
            [
                'city_code' => '845',
                'department_id' => '76',
                'city' => 'ULLOA',

            ],
            [
                'city_code' => '863',
                'department_id' => '76',
                'city' => 'VERSALLES',

            ],
            [
                'city_code' => '869',
                'department_id' => '76',
                'city' => 'VIJES',

            ],
            [
                'city_code' => '890',
                'department_id' => '76',
                'city' => 'YOTOCO',

            ],
            [
                'city_code' => '892',
                'department_id' => '76',
                'city' => 'YUMBO',

            ],
            [
                'city_code' => '895',
                'department_id' => '76',
                'city' => 'ZARZAL',

            ],
            [
                'city_code' => '001',
                'department_id' => '81',
                'city' => 'ARAUCA',

            ],
            [
                'city_code' => '065',
                'department_id' => '81',
                'city' => 'ARAUQUITA',

            ],
            [
                'city_code' => '220',
                'department_id' => '81',
                'city' => 'CRAVO NORTE',

            ],
            [
                'city_code' => '300',
                'department_id' => '81',
                'city' => 'FORTUL',

            ],
            [
                'city_code' => '591',
                'department_id' => '81',
                'city' => 'PUERTO RONDON',

            ],
            [
                'city_code' => '736',
                'department_id' => '81',
                'city' => 'SARAVENA',

            ],
            [
                'city_code' => '794',
                'department_id' => '81',
                'city' => 'TAME',

            ],
            [
                'city_code' => '001',
                'department_id' => '85',
                'city' => 'YOPAL',

            ],
            [
                'city_code' => '010',
                'department_id' => '85',
                'city' => 'AGUAZUL',

            ],
            [
                'city_code' => '015',
                'department_id' => '85',
                'city' => 'CHAMEZA',

            ],
            [
                'city_code' => '125',
                'department_id' => '85',
                'city' => 'HATO COROZAL',

            ],
            [
                'city_code' => '136',
                'department_id' => '85',
                'city' => 'LA SALINA',

            ],
            [
                'city_code' => '139',
                'department_id' => '85',
                'city' => 'MANI',

            ],
            [
                'city_code' => '162',
                'department_id' => '85',
                'city' => 'MONTERREY',

            ],
            [
                'city_code' => '225',
                'department_id' => '85',
                'city' => 'NUNCHIA',

            ],
            [
                'city_code' => '230',
                'department_id' => '85',
                'city' => 'OROCUE',

            ],
            [
                'city_code' => '250',
                'department_id' => '85',
                'city' => 'PAZ DE ARIPORO',

            ],
            [
                'city_code' => '263',
                'department_id' => '85',
                'city' => 'PORE',

            ],
            [
                'city_code' => '279',
                'department_id' => '85',
                'city' => 'RECETOR',

            ],
            [
                'city_code' => '300',
                'department_id' => '85',
                'city' => 'SABANALARGA',

            ],
            [
                'city_code' => '315',
                'department_id' => '85',
                'city' => 'SACAMA',

            ],
            [
                'city_code' => '325',
                'department_id' => '85',
                'city' => 'SAN LUIS DE PALENQUE',

            ],
            [
                'city_code' => '400',
                'department_id' => '85',
                'city' => 'TAMARA',

            ],
            [
                'city_code' => '410',
                'department_id' => '85',
                'city' => 'TAURAMENA',

            ],
            [
                'city_code' => '430',
                'department_id' => '85',
                'city' => 'TRINIDAD',

            ],
            [
                'city_code' => '440',
                'department_id' => '85',
                'city' => 'VILLANUEVA',

            ],
            [
                'city_code' => '001',
                'department_id' => '86',
                'city' => 'MOCOA',

            ],
            [
                'city_code' => '219',
                'department_id' => '86',
                'city' => 'COLON',

            ],
            [
                'city_code' => '320',
                'department_id' => '86',
                'city' => 'ORITO',

            ],
            [
                'city_code' => '568',
                'department_id' => '86',
                'city' => 'PUERTO ASIS',

            ],
            [
                'city_code' => '569',
                'department_id' => '86',
                'city' => 'PUERTO CAICEDO',

            ],
            [
                'city_code' => '571',
                'department_id' => '86',
                'city' => 'PUERTO GUZMAN',

            ],
            [
                'city_code' => '573',
                'department_id' => '86',
                'city' => 'PUERTO LEGUIZAMO',

            ],
            [
                'city_code' => '749',
                'department_id' => '86',
                'city' => 'SIBUNDOY',

            ],
            [
                'city_code' => '755',
                'department_id' => '86',
                'city' => 'SAN FRANCISCO',

            ],
            [
                'city_code' => '757',
                'department_id' => '86',
                'city' => 'SAN MIGUEL',

            ],
            [
                'city_code' => '760',
                'department_id' => '86',
                'city' => 'SANTIAGO',

            ],
            [
                'city_code' => '865',
                'department_id' => '86',
                'city' => 'LA HORMIGA',

            ],
            [
                'city_code' => '885',
                'department_id' => '86',
                'city' => 'VILLAGARZON',

            ],
            [
                'city_code' => '001',
                'department_id' => '88',
                'city' => 'SAN ANDRES',

            ],
            [
                'city_code' => '564',
                'department_id' => '88',
                'city' => 'PROVIDENCIA',

            ],
            [
                'city_code' => '001',
                'department_id' => '91',
                'city' => 'LETICIA',

            ],
            [
                'city_code' => '263',
                'department_id' => '91',
                'city' => 'EL ENCANTO',

            ],
            [
                'city_code' => '405',
                'department_id' => '91',
                'city' => 'LA CHORRERA',

            ],
            [
                'city_code' => '407',
                'department_id' => '91',
                'city' => 'LA PEDRERA',

            ],
            [
                'city_code' => '430',
                'department_id' => '91',
                'city' => 'LA VICTORIA',

            ],
            [
                'city_code' => '460',
                'department_id' => '91',
                'city' => 'MIRITI-PARANA',

            ],
            [
                'city_code' => '530',
                'department_id' => '91',
                'city' => 'PUERTO ALEGRIA',

            ],
            [
                'city_code' => '536',
                'department_id' => '91',
                'city' => 'PUERTO ARICA',

            ],
            [
                'city_code' => '540',
                'department_id' => '91',
                'city' => 'PUERTO NARIÑO',

            ],
            [
                'city_code' => '669',
                'department_id' => '91',
                'city' => 'PUERTO SANTANDER',

            ],
            [
                'city_code' => '798',
                'department_id' => '91',
                'city' => 'TARAPACA',

            ],
            [
                'city_code' => '001',
                'department_id' => '94',
                'city' => 'PUERTO  INIRIDA',

            ],
            [
                'city_code' => '343',
                'department_id' => '94',
                'city' => 'BARRANCO MINAS',

            ],
            [
                'city_code' => '883',
                'department_id' => '94',
                'city' => 'SAN FELIPE',

            ],
            [
                'city_code' => '884',
                'department_id' => '94',
                'city' => 'PUERTO COLOMBIA',

            ],
            [
                'city_code' => '885',
                'department_id' => '94',
                'city' => 'LA GUADALUPE',

            ],
            [
                'city_code' => '886',
                'department_id' => '94',
                'city' => 'CACAHUAL',

            ],
            [
                'city_code' => '887',
                'department_id' => '94',
                'city' => 'PANA PANA',

            ],
            [
                'city_code' => '888',
                'department_id' => '94',
                'city' => 'MORICHAL',

            ],
            [
                'city_code' => '001',
                'department_id' => '95',
                'city' => 'SAN JOSE DEL GUAVIARE',

            ],
            [
                'city_code' => '015',
                'department_id' => '95',
                'city' => 'CALAMAR',

            ],
            [
                'city_code' => '025',
                'department_id' => '95',
                'city' => 'EL RETORNO',

            ],
            [
                'city_code' => '200',
                'department_id' => '95',
                'city' => 'MIRAFLORES',

            ],
            [
                'city_code' => '001',
                'department_id' => '97',
                'city' => 'MITU',

            ],
            [
                'city_code' => '161',
                'department_id' => '97',
                'city' => 'CARURU',

            ],
            [
                'city_code' => '511',
                'department_id' => '97',
                'city' => 'PACOA',

            ],
            [
                'city_code' => '666',
                'department_id' => '97',
                'city' => 'TARAIRA',

            ],
            [
                'city_code' => '777',
                'department_id' => '97',
                'city' => 'PAPUNAUA',

            ],
            [
                'city_code' => '889',
                'department_id' => '97',
                'city' => 'YAVARATE',

            ],
            [
                'city_code' => '001',
                'department_id' => '99',
                'city' => 'PUERTO CARREÑO',

            ],
            [
                'city_code' => '524',
                'department_id' => '99',
                'city' => 'LA PRIMAVERA',

            ],
            [
                'city_code' => '572',
                'department_id' => '99',
                'city' => 'SANTA RITA',

            ],
            [
                'city_code' => '666',
                'department_id' => '99',
                'city' => 'SANTA ROSALIA',

            ],
            [
                'city_code' => '760',
                'department_id' => '99',
                'city' => 'SAN JOSE DE OCUNE',

            ],
            [
                'city_code' => '773',
                'department_id' => '99',
                'city' => 'CUMARIBO',

            ]
        ];
    DB::table('cities')->insert($mpios);
    }
}
