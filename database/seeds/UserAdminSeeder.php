<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'devU2',
          'email' => 'desarrollo@u2.com.co',
          'password' => bcrypt('secret'),
      ]);
    }
}
