<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class BalanceAccountsSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = DB::table('accounts')->get();

        //print_r($accounts);

        $count = 0;

        try {

            DB::beginTransaction();

            DB::table('balances_for_accounts')->truncate();
            $year_month = ['202101', '202102', '202103','202104','202105','201912','201911','201910','201909'];

            foreach ($accounts as $account) {

                foreach ($year_month as $yearmonth){

                    echo $account->id."Hola<br>";
                    $insert = [
                        'year_month' => $yearmonth,
                        'account_id' =>$account->id,
                        'original_balance' =>random_int(120000,91389098),
                        'initial_balance_month' =>random_int(120000,91389098),
                        'debits_month' =>random_int(120000,9389098),
                        'credits_month' =>random_int(120000,9189098),
                        'final_balance' =>random_int(120000,9189098),
                        'created_at' =>'now()',
                        'updated_at' =>'now()',
                    ];

                    print_r($insert);

                    DB::table('balances_for_accounts')->insert($insert);
                   
                }
                
                echo ++$count . "\n";


            }

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();

        }

    }
}