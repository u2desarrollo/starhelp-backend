<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceTableForPriceListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = DB::table('products')->get();

        echo count($products);

        $count = 0;

        try {

            DB::beginTransaction();

            foreach ($products as $product) {

                $price = DB::table('prices')->where(['product_id' => $product->id, 'price_list_id' => 924])->first();

                if (!$price) {

                    $insert = [
                        'product_id' => $product->id,
                        'price_list_id' => 924,
                        'price' => 199000,
                        'include_iva' => false
                    ];

                    DB::table('prices')->insert($insert);

                    echo ++$count . "\n";
                }

            }

            DB::commit();

        } catch (\Exception $e) {

            echo $e->getMessage();

            DB::rollBack();

        }
    }
}
