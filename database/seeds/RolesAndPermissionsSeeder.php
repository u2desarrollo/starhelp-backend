<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit banners']);
        Permission::create(['name' => 'delete banners']);
        Permission::create(['name' => 'create banners']);
        Permission::create(['name' => 'list banners']);
        Permission::create(['name' => 'create order']);
        Permission::create(['name' => 'access portal']);

        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'vendedor']);
        $role->givePermissionTo('create order', 'access portal');

        $role = Role::create(['name' => 'cliente']);
        $role->givePermissionTo('create order', 'access portal');

        // or may be done by chaining
        $role = Role::create(['name' => 'admin'])
            ->givePermissionTo(['edit banners', 'delete banners', 'create banners', 'list banners']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
