<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricesContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branchoffices = DB::table('branchoffices')->whereNull('deleted_at')->get();

        $contacts = DB::table('contacts')->where('id', 758)->where('is_provider', true)->get();

        $products = DB::table('products')->get();

        $count = 0;

        try {

            DB::beginTransaction();

            DB::table('price_contacts')->truncate();

            foreach ($products as $product) {

                $value = random_int(15000, 500000);

                foreach ($branchoffices as $branchoffice) {

                    foreach ($contacts as $contact) {

                        $insert = [
                            'branchoffice_id' => $branchoffice->id,
                            'product_id' => $product->id,
                            'contact_id' => $contact->id,
                            'price' => $value,
                            'previous_price' => $value,
                            'user_id' => 44
                        ];

                        DB::table('price_contacts')->insert($insert);

                        echo ++$count . "\n";
                    }

                }

            }

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();

        }
    }
}
