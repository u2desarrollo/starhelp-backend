<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = DB::table('products')->get();

        $branchoffices = DB::table('branchoffices')->get();

        $count = 0;

        try {

            DB::beginTransaction();

            DB::table('prices')->truncate();

            foreach ($products as $product) {

                foreach ($branchoffices as $branchoffice) {

                    $insert = [
                        'product_id' => $product->id,
                        'price_list_id' => 1,
                        'price' => random_int(15000, 500000),
                        'include_iva' => false,
                        'branchoffice_id' => $branchoffice->id
                    ];

                    DB::table('prices')->insert($insert);

                    echo ++$count . "\n";

                }

            }

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();

        }

    }
}
