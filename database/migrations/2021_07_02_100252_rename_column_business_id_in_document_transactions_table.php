<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnBusinessIdInDocumentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->renameColumn('business_id', 'projects_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->renameColumn('projects_id', 'business_id');
        });
    }
}
