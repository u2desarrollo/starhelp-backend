<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryRoleIdToPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('permissions', 'category_role_id')){
            Schema::table('permissions', function (Blueprint $table) {
                $table->unsignedInteger('category_role_id')->nullable();
    
                $table->foreign('category_role_id')->references('id')->on('parameters');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('category_role_id');
        });
    }
}
