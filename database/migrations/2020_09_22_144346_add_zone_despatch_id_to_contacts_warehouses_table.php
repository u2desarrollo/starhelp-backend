<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZoneDespatchIdToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->integer('zone_despatch_id')->nullable()->comment('Zona de despacho');
            $table->foreign('zone_despatch_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('zone_despatch_id');
        });
    }
}
