<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            //$table->dropColumn('iva_percentage');
            //$table->dropColumn('consumption_tax_percentage');
            //$table->renameColumn('unit_value', 'unit_value_before_taxes');
            //$table->integer('unit_value_after_taxes')->nullable()->comment('Valor unitario despues de impuestos');
            //$table->renameColumn('technician', 'technician­_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_products');
    }
}