<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalancesForContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balances_for_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year_month', 7)->comment('Año mes');
            $table->unsignedInteger('account_id')->comment('Id de la cuenta');
            $table->unsignedInteger('contact_id')->comment('Id del tercero');
            $table->bigInteger('original_balance')->default(0)->comment('Saldo original');
            $table->bigInteger('initial_balance_month')->default(0)->comment('Saldo inicial mes');
            $table->bigInteger('debits_month')->default(0)->comment('Débitos mes');
            $table->bigInteger('credits_month')->default(0)->comment('Créditos mes');
            $table->bigInteger('final_balance')->default(0)->comment('Saldo final');

            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('contact_id')->references('id')->on('contacts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balances_for_contacts');
    }
}
