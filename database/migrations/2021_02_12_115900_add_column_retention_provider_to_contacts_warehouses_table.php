<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRetentionProviderToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            # ret_fue
            $table->float('ret_fue_prod_vr_limit_provider')->nullable();
            $table->float('ret_fue_prod_percentage_provider')->nullable();

            $table->float('ret_fue_serv_vr_limit_provider')->nullable();
            $table->float('ret_fue_serv_percentage_provider')->nullable();


            # ret_iva
            $table->float('ret_iva_prod_vr_limit_provider')->nullable();
            $table->float('ret_iva_prod_percentage_provider')->nullable();

            $table->float('ret_iva_serv_vr_limit_provider')->nullable();
            $table->float('ret_iva_serv_percentage_provider')->nullable();


            # ret_ica
            $table->float('ret_ica_prod_vr_limit_provider')->nullable();
            $table->float('ret_ica_prod_percentage_provider')->nullable();

            $table->float('ret_ica_serv_vr_limit_provider')->nullable();
            $table->float('ret_ica_serv_percentage_provider')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('ret_fue_prod_vr_limit_provider');
            $table->dropColumn('ret_fue_prod_percentage_provider');
            $table->dropColumn('ret_fue_serv_vr_limit_provider');
            $table->dropColumn('ret_fue_serv_percentage_provider');
            $table->dropColumn('ret_iva_prod_vr_limit_provider');
            $table->dropColumn('ret_iva_prod_percentage_provider');
            $table->dropColumn('ret_iva_serv_vr_limit_provider');
            $table->dropColumn('ret_iva_serv_percentage_provider');
            $table->dropColumn('ret_ica_prod_vr_limit_provider');
            $table->dropColumn('ret_ica_prod_percentage_provider');
            $table->dropColumn('ret_ica_serv_vr_limit_provider');
            $table->dropColumn('ret_ica_serv_percentage_provider');
        });
    }
}
