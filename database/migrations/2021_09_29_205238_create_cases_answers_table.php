<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('case_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('content');
            $table->boolean('private')->default(false);
            $table->boolean('solution')->default(false);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('case_id')->references('id')->on('cases');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases_answers');
    }
}
