<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCodeParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters', function (Blueprint $table) {
            //   lo que hago es reescribir los campos de la tabla recuerda que ya fue creadamente anteriormente

            $table->string('code_parameter', 10)->comment('Código del parámetro')->change();

                   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters', function (Blueprint $table) {
            //   lo que hago es reescribir los campos de la tabla recuerda que ya fue creadamente anteriormente
            $table->dropColumn('code_parameter');

                   });
    }
}
