<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_transactions', function (Blueprint $table) {
           
            
            
            
            $table->increments('id');
            $table->integer('document_id')->unsigned()->comment('Consecutivo = CONCOM ');
            $table->integer('number_order_recording')->unsigned()->comment('No Orden Grabacion dentro del compr.');
            $table->integer('branchoffice_id')->unsigned()->nullable()->comment('Sede (PV)');
            $table->integer('vouchertype_id')->unsigned()->comment('Tipo comprobante');
            $table->dateTime('date_vouchertype')->comment('Fecha comprobante');
            $table->dateTime('number_vouchertype')->comment('Numero comprobante');
            $table->integer('account_id')->unsigned()->comment('Cuenta Auxiliar');
            $table->integer('contact_id')->unsigned()->comment('Codigo Tercero');
            $table->integer('document_number')->unsigned()->nullable()->comment('No.Document (Factura)');
			$table->char('prefix', 5)->nullable()->comment('Prefijo (para facturas)');
            $table->integer('quota_number')->unsigned()->nullable()->comment('Numero de cuota');
            $table->string('type_bank_document')->nullable()->comment('');
            $table->integer('bank_document_number')->unsigned()->nullable()->comment('Numero Documento bancario');
            $table->integer('cost_center_1')->nullable()->unsigned()->comment('Centro costo 1');
            $table->integer('cost_center_2')->nullable()->unsigned()->comment('Centro costo 2');
            $table->integer('cost_center_3')->nullable()->unsigned()->comment('Centro costo 3');
            $table->integer('business_id')->nullable()->unsigned()->comment('Codigo del Negocio');
            $table->string('concept')->nullable()->comment('Codigo Concepto');
            $table->dateTime('document_date')->nullable()->comment('Fecha emision documento (Factura)');
            $table->dateTime('due_date')->nullable()->comment('Fecha vencimiento');
            $table->integer('pay_day')->nullable()->unsigned()->comment('Dias de plazo');
            $table->integer('seller_id')->nullable()->unsigned()->comment('Codigo Vendedor');
            $table->string('detail')->nullable()->comment('Detalle de la partida');
            $table->string('automatic detail')->nullable()->comment('Detalle automatico');
            $table->text('observations')->nullable()->comment('Observacion');
            $table->integer('operation_value')->nullable()->unsigned()->comment('Valor de la partida');
            $table->integer('transactions_type')->nullable()->unsigned()->comment('Tipo transaccion D-Debito  C-Credito');
            $table->integer('base_value')->nullable()->unsigned()->comment('Valor base');
            $table->integer('base_document_id')->nullable()->unsigned()->comment('ORIGEN: No.Pedido, No.Orden Produccion,');


            $table->timestamps();
            $table->softDeletes();


            $table->foreign('document_id')->references('id')->on('documents');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('vouchertype_id')->references('id')->on('voucherstypes');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('cost_center_1')->references('id')->on('parameters');
            $table->foreign('cost_center_2')->references('id')->on('parameters');
            $table->foreign('cost_center_3')->references('id')->on('parameters');
            $table->foreign('base_document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_transactions');
    }
}
