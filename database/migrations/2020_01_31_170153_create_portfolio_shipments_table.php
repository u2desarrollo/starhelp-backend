<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expiration_days_from')->nullable()->comment('dias vencimiento desde');
            $table->integer('expiration_days_until')->nullable()->comment('dias vencimiento hasta');
            $table->integer('frecuency')->nullable()->comment('frecuencia');
            $table->time('time')->nullable()->comment('hora');
            $table->boolean('pre_legal_charge')->nullable()->comment('cobro pre juridico');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_shipments');
    }
}
