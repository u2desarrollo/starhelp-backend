<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistanceToleranceToContactsEmployeesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_employees', function (Blueprint $table) {
			$table->integer('distance_tolerance')->default(200)->comment('Tolerancia de distancia en metros');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_employees', function (Blueprint $table) {
			$table->dropColumn('distance_tolerance');
		});
	}
}