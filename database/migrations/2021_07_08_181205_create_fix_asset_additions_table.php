<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixAssetAdditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fix_asset_additions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fix_asset_id')->unsigned();
            $table->string('plate_number')->comment('Numero de placa');
            $table->text('description')->nullable()->comment('Descripcion principal');
            $table->float('value');
            $table->date('date_addition')->nullable();

            $table->foreign('fix_asset_id')->references('id')->on('fix_assets');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fix_asset_additions');
    }
}
