<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasBackorderTableDocumentsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->integer('backorder_quantity')->nullable();
            $table->float('backorder_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('backorder_quantity');
            $table->dropColumn('backorder_value');
        });
    }
}
