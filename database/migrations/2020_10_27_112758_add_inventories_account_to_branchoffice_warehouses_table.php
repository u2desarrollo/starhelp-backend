<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoriesAccountToBranchofficeWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branchoffice_warehouses', function (Blueprint $table) {
            $table->integer('inventories_account')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branchoffice_warehouses', function (Blueprint $table) {
            $table->dropColumn('inventories_account');
        });
    }
}
