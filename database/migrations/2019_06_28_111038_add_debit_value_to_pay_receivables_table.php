<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDebitValueToPayReceivablesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pay_receivables', function (Blueprint $table) {
			$table->double('debit_value')->nullable()->after('pay_way')->comment('Valor débito');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pay_receivables', function (Blueprint $table) {
			$table->dropColumn('debit_value');
		});
	}
}