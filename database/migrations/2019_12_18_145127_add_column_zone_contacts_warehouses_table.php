<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnZoneContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // contacts_warehouses
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->bigInteger('zone_id')->unsigned()->nullable();
            $table->foreign('zone_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropForeign('documents_user_id_authorizes_foreign');
            $table->dropForeign('documents_status_id_authorized_foreign');

            $table->dropColumn('contacts_warehouses_zone_id_foreign');
        });
    }
}
