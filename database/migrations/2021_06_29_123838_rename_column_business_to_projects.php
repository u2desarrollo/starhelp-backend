<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnBusinessToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->renameColumn('manage_businesses_stage', 'manage_projects_stage');
            $table->renameColumn('manage_businesses', 'manage_projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->renameColumn('manage_projects_stage', 'manage_businesses_stage');
            $table->renameColumn('manage_projects', 'manage_businesses');
        });
    }
}
