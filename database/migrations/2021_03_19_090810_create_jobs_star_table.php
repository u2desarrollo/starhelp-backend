<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsStarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_star', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_creation_id')->comment('ID del usuario que creo el JOB');
            $table->string('name')->nullable()->comment('Nombre del JOB');
            $table->string('description')->nullable()->comment('Descripcion del JOB');
            $table->boolean('state')->default(true)->comment('Estado del JOB');
            $table->dateTime('closing_date')->nullable()->comment('Fecha del cierre del JOB');
            $table->dateTime('limit_date')->nullable()->comment('Fecha limite del JOB');
            $table->integer('alert_type')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_creation_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_star');
    }
}
