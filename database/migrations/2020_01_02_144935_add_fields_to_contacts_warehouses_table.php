<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->integer('city_id')->unsigned()->nullable();
            $table->boolean('retention_source')->nullable()->comment('Determina si el cliente hace retención en la fuente');
            $table->double('percentage_retention_source')->nullable()->comment('Porcentaje de retencion en la fuente del proveedor');
            $table->boolean('retention_iva')->nullable()->comment('Determina si se le factura con IVA al cliente');
            $table->double('percentage_retention_iva')->nullable()->comment('Porcentaje de retención de IVA del proveedor');
            $table->boolean('retention_ica')->nullable()->comment('Determina si se le factura con ICA al cliente');
            $table->double('percentage_retention_ica')->nullable();

            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('city_id');
            $table->dropColumn('retention_source');
            $table->dropColumn('percentage_retention_source');
            $table->dropColumn('retention_iva');
            $table->dropColumn('percentage_retention_iva');
            $table->dropColumn('retention_ica');
            $table->dropColumn('percentage_retention_ica');
        });
    }
}
