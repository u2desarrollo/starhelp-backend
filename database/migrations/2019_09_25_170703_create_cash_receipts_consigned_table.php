<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashReceiptsConsignedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_receipts_consigned', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consignment_id')->unsigned()->comment('Id consignaión');
            $table->integer('document_id')->unsigned()->comment('Recibo de caja');
            $table->timestamps();

            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_receipts_consigned');
    }
}
