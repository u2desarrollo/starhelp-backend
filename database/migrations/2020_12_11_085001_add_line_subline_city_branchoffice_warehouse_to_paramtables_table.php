<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLineSublineCityBranchofficeWarehouseToParamtablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paramtables', function (Blueprint $table) {
            $table->boolean('line')->nullable()->default(false);
            $table->boolean('subline')->nullable()->default(false);
            $table->boolean('city')->nullable()->default(false);
            $table->boolean('branchoffice_warehouses')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paramtables', function (Blueprint $table) {
            $table->dropColumn('line');
            $table->dropColumn('subline');
            $table->dropColumn('city');
            $table->dropColumn('branchoffice_warehouses');
        });
    }
}
