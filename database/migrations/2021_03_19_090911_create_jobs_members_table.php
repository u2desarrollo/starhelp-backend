<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jobs_star_id')->comment('ID del JOB');
            $table->unsignedInteger('user_id')->comment('ID del usuario que es miembro del JOB');
            $table->unsignedInteger('user_inviting_id')->comment('ID del usuario quien invito al miembro del JOB');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('jobs_star_id')->references('id')->on('jobs_star');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_inviting_id')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_members');
    }
}
