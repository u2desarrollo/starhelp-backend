<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMotiveFromVisitsCustomersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('visits_customers', function (Blueprint $table) {
			$table->dropColumn('motive');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('visits_customers', function (Blueprint $table) {
			$table->json('motive')->nullable()->comment('Motivo');
		});
	}
}