<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestsStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests_state', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('service_requests_types_id');
            $table->bigInteger('model_id')->nullable();
            $table->bigInteger('service_requests_state_id')->nullable();

            $table->string('name');
            $table->integer('order')->unsigned();
            $table->boolean('view_customer')->default(false);
            $table->boolean('view_seller')->default(false);
            

            $table->softDeletes();
            $table->timestamps();


            // Relacion
            $table->foreign('service_requests_types_id')->references('id')->on('service_requests_types');
            $table->foreign('model_id')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests_types');
    }
}
