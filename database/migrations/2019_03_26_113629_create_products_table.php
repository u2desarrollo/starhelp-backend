<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');

            $table->string('code', 100);
            $table->string('barcode', 60)->nullable();
            $table->string('description', 255);
            $table->string('long_description', 1000);
            $table->integer('line_id')->unsigned();
            $table->integer('subline_id')->unsigned()->nullable();
            $table->integer('brand_id')->unsigned();
            $table->integer('subbrand_id')->unsigned()->nullable();
            $table->integer('owner')->unsigned()->nullable();
            $table->integer('maker')->unsigned()->nullable();
            $table->integer('type')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->boolean('inventory_management')->default(1);
            $table->boolean('purchase')->default(1);
            $table->boolean('sale')->default(1);
            $table->boolean('kit')->default(0);
            $table->boolean('state')->default(1);
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('condition')->nullable();
            $table->boolean('has_lot')->default(0)->comment('Maneja lote');
            $table->boolean('has_serial')->default(0)->comment('Maneja serial');
            $table->boolean('high_price')->default(0)->comment('Producto de alto costo');
            $table->boolean('high_risk')->default(0)->comment('Producto de alto riesgo');
            $table->integer('products_risk_id')->unsigned()->nullable()->comment('Llave foranea de la tabla de riesgos');
            $table->integer('rotation_type')->nullable();
            $table->boolean('cooled')->default(0)->comment('Refrigerado');
            $table->boolean('reusable')->default(0)->comment('Reutilizable');
            $table->string('production_formula', 20)->nullable();
            $table->string('tariff_position', 20)->nullable()->comment('Posición arancelaria');
            $table->boolean('export_product')->default(0);
            $table->boolean('pos')->default(0);
            $table->boolean('lock_buy')->default(0);
            $table->string('lock_buy_observations', 150)->nullable();
            $table->boolean('lock_sale')->default(0);
            $table->string('lock_sale_observations', 150)->nullable();
            $table->double('percentage_iva_buy')->nullable();
            $table->double('percentage_tax_consumer_buy')->nullable();
            $table->double('percentage_iva_sale')->nullable();
            $table->double('percentage_tax_consumer_sale')->nullable();
            $table->integer('balance_units')->nullable();
            $table->integer('balance_value')->nullable();
            $table->double('average_cost')->nullable();
            $table->date('start_date_inventory')->nullable();
            $table->date('exp_date_certified')->nullable();
            $table->boolean('modify_description_buy')->default(0);
            $table->boolean('modify_price_buy')->default(0);
            $table->string('observation_buy', 150)->nullable();
            $table->string('conformity_certificate', 150)->nullable();
            $table->string('presentation_packaging_buy', 15)->nullable();
            $table->integer('presentation_quantity_buy')->nullable();
            $table->string('presentation_unit_buy', 15)->nullable();
            $table->string('presentation_assembled_buy', 15)->nullable();
            $table->integer('minimum_quantity_buy')->nullable();
            $table->boolean('local_buy')->default(0);
            $table->boolean('calculate_price_buy')->default(0);
            $table->boolean('automatic_reposition')->default(0);
            $table->integer('contact_id')->unsigned()->nullable();
            $table->boolean('point_of_sale_deployment')->default(0);
            $table->boolean('amazon_deployment')->default(0);
            $table->boolean('free_market_deployment')->default(0);
            $table->boolean('modify_description_sale')->default(0);
            $table->boolean('modify_price_sale')->default(0);
            $table->string('observation_sale', 150)->nullable();
            $table->boolean('allow_invoices_sales_zeros')->default(0);
            $table->string('presentation_packaging_sale', 15)->nullable();
            $table->integer('presentation_quantity_sale')->nullable();
            $table->string('presentation_unit_sale', 15)->nullable();
            $table->string('presentation_assembled_sale', 15)->nullable();
            $table->integer('minimum_quantity_sale')->nullable();
            $table->double('commision_percentage')->nullable();
            $table->double('profit_margin_percentage')->nullable();
            $table->double('quantity_available')->default(0);
            $table->boolean('entry_third_party')->default(0);
            $table->boolean('reduction_management')->default(0);
            $table->boolean('controlled_sale')->default(0);
            $table->boolean('product_of_continuous_use')->default(0);
            $table->integer('erp_id')->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->boolean('exclude_sales_report')->comment('excluir informe de ventas')->nullable();

            $table->index('subline_id', 'fk_products_lines_idx');
            $table->index('subbrand_id', 'fk_products_subbrands1_idx');
            $table->index('subcategory_id', 'fk_products_subcategory1_idx');
            $table->index('product_id', 'fk_products_products1_idx');
            $table->index('products_risk_id', 'fk_products_products_risks1_idx');
            $table->index('contact_id', 'fk_products_contacts1_idx');
            $table->index('line_id', 'fk_products_lines1_idx');
            $table->index('category_id', 'fk_products_categories1_idx');
            $table->index('brand_id', 'fk_products_brands1_idx');

            $table->foreign('subline_id')->references('id')->on('sublines');
            $table->foreign('subbrand_id')->references('id')->on('subbrands');
            $table->foreign('subcategory_id')->references('id')->on('subcategories');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('products_risk_id')->references('id')->on('parameters');
            $table->foreign('type')->references('id')->on('parameters');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('user_id')->references('id')->on('users');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}