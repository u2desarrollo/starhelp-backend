<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchofficeWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branchoffice_warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branchoffice_id')->unsigned()->comment('Codigo de la sede');
            $table->string('warehouse_code')->comment('Codigo de la Bodega');
            $table->string('warehouse_description')->comment('Descripcion de la Bodega');
            $table->boolean('update_inventory')->nullable()->comment('Actualiza el inventario de la bodega');
            $table->boolean('exits_no_exist')->nullable()->comment('Salidas sin existencias');
            $table->boolean('negative_balances')->nullable()->comment('Saldos negativos');
            $table->integer('warehouse_status')->nullable()->comment('Estado de la bodega 1-Activo, 3-Inventario fisico, 9-Inactivo');
            $table->date('last_date_physical_inventory')->nullable()->comment('Fecha Ultimo inventario Fisico');
            $table->string('number_physical_inventory')->nullable()->comment('Numero de inventario fisico');
            $table->timestamps();

            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branchoffice_warehouses');
    }
}
