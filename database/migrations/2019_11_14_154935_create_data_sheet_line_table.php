<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSheetLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_sheet_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('data_sheet_id')->unsigned();
            $table->bigInteger('line_id')->unsigned();
            $table->integer('required')->default('0');

            $table->foreign('data_sheet_id')->references('id')->on('data_sheet');
            $table->foreign('line_id')->references('id')->on('lines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_sheet_line');
    }
}
