<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarehouseIdToDocumentsProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->integer('warehouse_id')->unsigned()->nullable();

			$table->index('warehouse_id', 'fk_warehouse_documents_products_idx');
			$table->foreign('warehouse_id')->references('id')->on('branchoffice_warehouses');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->dropColumn('warehouse_id');
		});
	}
}