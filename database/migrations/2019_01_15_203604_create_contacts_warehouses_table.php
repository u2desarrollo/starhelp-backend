<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_warehouses', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->string('code', 10);
            $table->string('description', 100);
            $table->string('address', 70)->nullable();
            $table->string('picture', 200)->nullable();
            $table->string('latitude', 45)->nullable();
            $table->string('length', 45)->nullable();
            $table->string('telephone', 30)->nullable();
            $table->string('email', 60)->nullable();
            $table->longText('observation')->nullable();
            $table->boolean('state')->default('1');
            $table->timestamps();
            $table->softDeletes();

            $table->index('contact_id', 'fk_contactswarehouses_contacts1_idx');

            $table->foreign('contact_id')->references('id')->on('contacts');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_warehouses');
    }
}
