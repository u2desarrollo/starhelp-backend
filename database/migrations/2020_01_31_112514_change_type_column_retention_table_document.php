<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeColumnRetentionTableDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
             # ret_fue
            $table->float('ret_fue_prod_vr_limit')->change();
            $table->float('ret_fue_prod_percentage')->change();
            $table->float('ret_fue_prod_total')->change();

            $table->float('ret_fue_serv_vr_limit')->change();
            $table->float('ret_fue_serv_percentage')->change();
            $table->float('ret_fue_serv_total')->change();

            $table->float('ret_fue_total')->change();


            # ret_iva
            $table->float('ret_iva_prod_vr_limit')->change();
            $table->float('ret_iva_prod_percentage')->change();
            $table->float('ret_iva_prod_total')->change();

            $table->float('ret_iva_serv_vr_limit')->change();
            $table->float('ret_iva_serv_percentage')->change();
            $table->float('ret_iva_serv_total')->change();

            $table->float('ret_iva_total')->change();


            # ret_ica
            $table->float('ret_ica_prod_vr_limit')->change();
            $table->float('ret_ica_prod_percentage')->change();
            $table->float('ret_ica_prod_total')->change();

            $table->float('ret_ica_serv_vr_limit')->change();
            $table->float('ret_ica_serv_percentage')->change();
            $table->float('ret_ica_serv_total')->change();

            $table->float('ret_ica_total')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            //
        });
    }
}
