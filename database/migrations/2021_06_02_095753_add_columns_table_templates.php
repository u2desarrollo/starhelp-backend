<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->boolean('select_import')->comment('Liquida Costos Import')->nullable()->default(false);
            $table->boolean('select_validate_wms')->comment('Valida Oper WMS')->nullable()->default(false);
            $table->boolean('quantity_decimals')->comment('Cantidad Con Decimales')->nullable()->default(false);
            $table->integer('document_name')->comment('Nombre documento :1-SI,2-NO,9-OB')->nullable()->default(2);
            $table->integer('days_number')->comment('Nombre dias Cantidad Entero')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->dropColumn('select_import');
            $table->dropColumn('select_validate_wms');
            $table->dropColumn('quantity_decimals');
            $table->dropColumn('document_name');
            $table->dropColumn('days_number');
        });
    }
}
