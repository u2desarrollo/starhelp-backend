<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('rotation_classification')->comment('Clasificacion Rotacion')->nullable();
            $table->string('supplyer_code')->comment('Codigo proveedor')->nullable();
            $table->string('units_per_pallet')->comment('Unidades por pallet')->nullable();
            $table->string('high')->comment('Alto')->nullable();
            $table->string('long')->comment('Largo')->nullable();
            $table->string('width')->comment('Ancho')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('rotation_classification');
            $table->dropColumn('supplyer_code');
            $table->dropColumn('units_per_pallet');
            $table->dropColumn('high');
            $table->dropColumn('long');
            $table->dropColumn('width');
        });
    }
}
