<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 10)->comment('Código del vehículo - Tabla Facecolda');
            $table->unsignedInteger('brand_vehicle_id')->comment('Marca del vehículo - Tabla de parámetros');
            $table->unsignedInteger('class_vehicle_id')->comment('Clase del vehículo - Tabla de parámetros');
            $table->string('vehicle_type', 180)->comment('Tipo de vehículo - Este el el resultado de la concatenación de la referencia 1, 2, y 3 del listado de Facecolda');
            $table->integer('weight')->comment('Peso del vehículo');
            $table->integer('service')->comment('Servicio del vehículo, 1 - Publico, 2 - Particular');
            $table->integer('power')->comment('Potencia del vehículo');
            $table->string('box_type', 2)->nullable()->comment('Tipo de caja del vehículo, AT, MP, TP');
            $table->integer('cylinder_capacity')->comment('Cilindraje del vehículo');
            $table->integer('passenger_capacity')->comment('Capacidad de pasajeros del vehículo');
            $table->string('nationality', 4)->comment('Nacionalidad del vehículo');
            $table->string('fuel', 4)->comment('Combustible del vehículo');
            $table->integer('doors')->comment('Cantidad de puertas del vehículo');
            $table->string('transmission', 5)->comment('Transmisión del vehículo');
            $table->timestamps();

            $table->foreign('brand_vehicle_id')->references('id')->on('parameters');
            $table->foreign('class_vehicle_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_types');
    }
}
