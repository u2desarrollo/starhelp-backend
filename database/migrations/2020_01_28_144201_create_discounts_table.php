<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter')->unsigned()->comment('categoria de cliente');
            $table->integer('line_id')->unsigned()->comment('Linea');
            $table->integer('brand_id')->unsigned()->comment('Marca')->nullable();
            $table->integer('percentage')->unsigned()->comments('porcentaje');

            $table->timestamps();

            $table->foreign('parameter')->references('id')->on('parameters');
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
