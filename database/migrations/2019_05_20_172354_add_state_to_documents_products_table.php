<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToDocumentsProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->integer('state')->unsigned()->nullable();

			$table->index('state', 'fk_parameters_documents_products_idx');
			$table->foreign('state')->references('id')->on('parameters');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->dropColumn('state');
		});
	}
}