<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jobs_star_id')->comment('ID del JOB');
            $table->string('name')->comment('Ruta del archivo en el servidor');
            $table->string('type')->comment('Tipo de extension del archivo');
            $table->string('path_document')->comment('Ruta del archivo en el servidor');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('jobs_star_id')->references('id')->on('jobs_star');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_docs');
    }
}
