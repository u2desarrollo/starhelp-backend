<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsInformationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents_informations', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('document_id')->unsigned();

			$table->string('mac_address')->nullable();
			$table->integer('money')->unsigned()->nullable();
			$table->string('trm')->comment('Tasa de cambio')->nullable();

			$table->integer('branchoffice_dispatch')->comment('Sede de despacho')->nullable()->unsigned();
			$table->integer('delivery_establishment')->comment('Establecimiento de entrega')->nullable()->unsigned();

			$table->string('delivery_place')->comment('Lugar de entrega')->nullable();
			$table->date('delivery_date')->comment('Fecha de entrega')->nullable();
			$table->integer('delivery_days')->comment('Días de entrega')->nullable();
			$table->string('delivery_observation')->comment('Observación de entrega')->nullable();

			$table->boolean('home_delivery')->comment('Venta a domicilio')->nullable();
			$table->integer('domiciliary')->comment('Domiciliario')->nullable()->unsigned();
			$table->integer('protractor')->comment('Transportadora')->nullable()->unsigned();
			$table->string('guide_number')->comment('Número de guía')->nullable();
			$table->string('registration_tag')->comment('Placa')->nullable();
			$table->integer('order_type')->comment('Tipo de orden')->nullable()->unsigned();

			$table->integer('cost_center')->comment('Centro de costos')->nullable()->unsigned();
			$table->integer('bussiness_code')->comment('Código de negocio')->nullable()->unsigned();

			$table->integer('price_list')->comment('Lista de precios')->nullable()->unsigned();
			$table->boolean('include_tax')->comment('Incluye impuestos')->nullable();

			$table->integer('additional_contact')->comment('Tercero adicional')->nullable()->unsigned();
			$table->integer('additional_branchoffice')->comment('Sede adicional')->nullable()->unsigned();
			$table->integer('additional_warehouse')->comment('Bodega adicional')->nullable()->unsigned();

			$table->date('since')->nullable();
			$table->date('until')->nullable();

			$table->boolean('incorporated')->default(0)->comment('Documento incorporado')->nullable();
			$table->boolean('reversed')->default(0)->comment('Documento reversado')->nullable();
			$table->boolean('modified')->default(0)->comment('Documento modificado')->nullable();
			$table->boolean('anulled')->default(0)->comment('Documento anulado')->nullable();

			$table->string('special_bearings')->comment('Marcación especial')->nullable();
			$table->boolean('printed')->default(0)->comment('Documento impreso')->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->index('document_id', 'fk_document_documents_informations_idx');
			$table->index('money', 'fk_money_documents_informations_idx');
			$table->index('branchoffice_dispatch', 'fk_branchoffice_dispatch_documents_informations_idx');
			$table->index('delivery_establishment', 'fk_delivery_establishment_documents_informations_idx');
			$table->index('domiciliary', 'fk_domiciliary_documents_informations_idx');
			$table->index('protractor', 'fk_protractor_documents_informations_idx');
			$table->index('order_type', 'fk_order_type_documents_informations_idx');
			$table->index('cost_center', 'fk_cost_center_documents_informations_idx');
			// $table->index('bussiness_code', 'fk_bussiness_code_documents_informations_idx');
			$table->index('price_list', 'fk_price_list_documents_informations_idx');
			$table->index('additional_contact', 'fk_additional_contact_documents_informations_idx');
			$table->index('additional_branchoffice', 'fk_additional_branchoffice_documents_informations_idx');
			$table->index('additional_warehouse', 'fk_additional_warehouse_documents_informations_idx');

			$table->foreign('document_id')->references('id')->on('documents');
			$table->foreign('money')->references('id')->on('parameters');
			$table->foreign('branchoffice_dispatch')->references('id')->on('branchoffices');
			$table->foreign('delivery_establishment')->references('id')->on('contacts_warehouses');
			$table->foreign('domiciliary')->references('id')->on('contacts');
			$table->foreign('protractor')->references('id')->on('contacts');
			$table->foreign('order_type')->references('id')->on('parameters');
			$table->foreign('cost_center')->references('id')->on('parameters');
			// $table->foreign('bussiness_code')->references('id')->on('');
			$table->foreign('price_list')->references('id')->on('parameters');
			$table->foreign('additional_contact')->references('id')->on('contacts');
			$table->foreign('additional_branchoffice')->references('id')->on('branchoffices');
			$table->foreign('additional_warehouse')->references('id')->on('branchoffice_warehouses');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('documents_informations');
	}
}