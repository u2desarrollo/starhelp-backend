<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginDocumentToVoucherstypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucherstypes', function (Blueprint $table) {
            $table->boolean('origin_document')->default(false)->comment('Determina si los documentos con este tipo de comprobante son documentos de origen o no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucherstypes', function (Blueprint $table) {
            $table->dropColumn('origin_document');
        });
    }
}
