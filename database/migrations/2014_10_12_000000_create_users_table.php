<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo', 255);
            $table->rememberToken();
            $table->integer('subscriber_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->integer('branchoffice_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('branchoffice_warehouse_id')->unsigned();
            $table->integer('contact_warehouse_id')->unsigned();
            $table->integer('clerk_id')->unsigned();
            $table->boolean('password_changed')->default(false);
            $table->string('type', 1);
            $table->string('signature')->nullable()->comment('foto de firma');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
