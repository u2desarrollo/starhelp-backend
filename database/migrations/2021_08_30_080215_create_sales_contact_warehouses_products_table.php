<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesContactWarehousesProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_contact_warehouses_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('contact_warehouse_id')->comment('Id de la sucursal del cliente');
            $table->unsignedInteger('product_id')->comment('Id del producto');

            $table->date('last_sale_date')->comment('Fecha de la ultima venta');
            $table->integer('last_sale_days')->default(0)->comment('Dias desde la ultima venta');
            $table->integer('last_sale_quantity')->default(0)->comment('Cantidad de la ultima venta');
            $table->integer('last_sale_unit_value')->default(0)->comment('Valor unitario de la ultima venta');

            $table->date('date_one')->nullable()->comment('Fecha 1');
            $table->date('date_two')->nullable()->comment('Fecha 2');
            $table->date('date_three')->nullable()->comment('Fecha 3');

            $table->integer('days_sale')->nullable()->comment('Se calculan los dias entre F1y F2 y los dias entre F2 y F3. Se escoge el menor valor y se restan 3 dias.');

            $table->boolean('suggested')->default(false)->comment('Sugerido para pedido');

            $table->foreign('contact_warehouse_id')->references('id')->on('contacts_warehouses');
            $table->foreign('product_id')->references('id')->on('products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_sales_for_contact_warehouse');
    }
}
