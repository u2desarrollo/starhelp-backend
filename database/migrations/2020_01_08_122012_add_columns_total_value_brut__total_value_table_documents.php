<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTotalValueBrutTotalValueTableDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->bigInteger('total_value_brut')->nullable()->unsigned();
            $table->bigInteger('total_value')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('total_value_brut', 'total_value')){
            Schema::table('documents', function (Blueprint $table) {
                $table->dropColumn('total_value_brut');
                $table->dropColumn('total_value');
            });
        }
    }
}
