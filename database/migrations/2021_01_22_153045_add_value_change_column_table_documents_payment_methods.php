<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValueChangeColumnTableDocumentsPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_payment_methods', function (Blueprint $table) {
            $table->float('value_change')->nullable()->comment('Valor del cambio cuando el metodo de pago es efectivo.');
            $table->float('total_value')->nullable()->comment('Valor total del metodo de pago es efectivo menos el cambio.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_payment_methods', function (Blueprint $table) {
            $table->dropColumn('value_change');
            $table->dropColumn('total_value');
        });
    }
}
