<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropLineIdFromContactsWarehousesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			// $table->dropIndex('fk_line_contacts_warehouses_idx');
			// $table->foreign('line_id');
			$table->dropColumn('line_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->integer('line_id')->nullable()->unsigned();

			// $table->index('line_id', 'fk_line_contacts_warehouses_idx');
			// $table->foreign('line_id')->references('id')->on('lines');
		});
	}
}