<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_operation', function (Blueprint $table) {
            $table->increments('id')->comment('id unico por elemento');
            $table->string("code")->comment('codigo ');
            $table->string("description")->comment('descripcion');
            $table->integer("inventory_movement")->comment('inventario de movimientos');
            $table->integer("affectation")->comment('afectacion');
            $table->integer('inventory_cost')->comment('costo de inventario');
            $table->integer('parameter_id')->nullable()->comment('foranea a la tabla de parameters');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('parameter_id')->references('id')->on('parameters');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types_operation');
    }
}
