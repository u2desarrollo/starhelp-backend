<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubbrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subbrands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->unsigned()->comment('id de la marca');
            $table->string('subbrand_code')->comment('codigo de la submarca');
            $table->string('description')->comment('descripción de la submarca');
            $table->string('image')->comment('imagen de la submarca')->nullable();
            $table->boolean('lock_buy')->comment('Bloqueo Compra si/no')->nullable();
            $table->boolean('block_sale')->comment('Bloqueo Venta si/no')->nullable();
            $table->string('data_sheet')->comment('ficha tecnica')->nullable();
            $table->double('margin_cost_percentage')->comment('porcentaje margen costo')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subbrands');
    }
}
