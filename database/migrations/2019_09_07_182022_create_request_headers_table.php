<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->integer('contacts_warehouse_id')->unsigned();
            $table->timestamp('request_date');
            $table->boolean('route_to_pickup')->comment('en ruta para recoger');
            $table->integer('request_type');
            $table->integer('maintenance_type')->nullable();
            $table->integer('status_request')->nullable();
            $table->integer('units')->nullable();
            $table->string('direc_observation')->nullable();
            $table->string('customer_observation')->nullable();
            $table->string('provider_observation')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->index('contact_id', 'fk_request_headers_contacts_idx');
            $table->index('contacts_warehouse_id', 'fk_request_headers_contacts_warehouses_idx');

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('contacts_warehouse_id')->references('id')->on('contacts_warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_headers');
    }
}
