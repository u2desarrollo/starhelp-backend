<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamtablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paramtables', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('subscriber_id')->unsigned()->nullable()->comment('Id del suscriptor');
            $table->string('code_table', 5)->comment('Código de la tabla');
            $table->string('name_table', 45)->comment('Nombre de la tabla');
            $table->string('desc_code_parameter', 45)->nullable()->comment('Label del código del parámetro');
            $table->string('desc_name_parameter', 45)->nullable()->comment('Label del nombre del parámetro');
            $table->string('desc_alphanum_data_first', 45)->nullable()->comment('Descripción del primer dato alfanumérico');
            $table->string('desc_alphanum_data_second', 45)->nullable()->comment('Descripción del segundo dato alfanumérico');
            $table->string('desc_alphanum_data_third', 45)->nullable()->comment('Descripción del tercer dato alfanumérico');
            $table->string('desc_numeric_data_first', 45)->nullable()->comment('Descripción del primer dato numérico');
            $table->string('desc_numeric_data_second', 45)->nullable()->comment('Descripción del segundo dato numérico');
            $table->string('desc_numeric_data_third', 45)->nullable()->comment('Descripción del tercer dato numérico');
            $table->string('desc_date_data_first', 45)->nullable()->comment('Descripción del primer dato de fecha');
            $table->string('desc_date_data_second', 45)->nullable()->comment('Descripción del segundo dato de fecha');
            $table->string('desc_date_data_third', 45)->nullable()->comment('Descripción del tercer dato de fecha');
            $table->string('observation', 255)->nullable()->comment('');
            $table->boolean('protected')->default(true)->comment('');
            $table->string('desc_image', 255)->nullable();
            $table->softDeletes();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paramtables');
    }
}
