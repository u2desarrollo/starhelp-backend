<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_providers', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned()->comment('Llave foranea que relaciona con la tabla principal de contactos');
            $table->integer('category_id')->unsigned();
            $table->integer('seller_id')->nullable();
            $table->integer('point_of_sale_id')->nullable()->comment('Punto de venta');
            $table->double('percentage_commision')->nullable()->comment('Porcentaje de comisión del proveedor o empleado');
            $table->string('contract_number', 45)->nullable()->comment('Número de contrato de proveedor o empleado');
            $table->integer('price_list_id')->nullable()->comment('Código de lista de precios del cliente o proveedor');
            $table->integer('priority')->nullable()->comment('Prioridad cliente o proveedor');
            $table->double('percentage_discount')->nullable()->comment('Porcentaje de descuento de Cliente o Proveedor');
            $table->integer('credit_quota')->nullable()->comment('Cupo de credito de proveedor');
            $table->date('expiration_date_credit_quota')->nullable()->comment('Fecha de vencimiento del cupo de credito o Proveedor');
            $table->integer('days_soon_payment_1')->nullable()->comment('Días de pronto pago 1');
            $table->double('percentage_soon_payment_1')->nullable()->comment('Porcentaje de pronto pago 1');
            $table->integer('days_soon_payment_2')->nullable()->comment('Días de pronto pago 2');
            $table->double('percentage_soon_payment_2')->nullable()->comment('Porcentaje de pronto pago 2');
            $table->integer('days_soon_payment_3')->nullable()->comment('Días de pronto pago 3');
            $table->double('percentage_soon_payment_3')->nullable()->comment('Porcentaje de pronto pago 3');
            $table->integer('replacement_days')->nullable()->comment('Días de reposición proveedor');
            $table->boolean('electronic_order')->nullable()->comment('Pedidos electronico proveedor');
            $table->boolean('update_shopping_list')->nullable()->comment('Actualiza listas de compras');
            $table->boolean('calculate_sale_prices')->nullable()->comment('Calcular precios de venta');
            $table->date('last_purchase')->nullable()->comment('Ultima fecha de compra de cliente o proveedor');
            $table->string('observation', 255)->nullable()->comment('Observaciones del cliente');
            $table->boolean('blocked')->default('0')->comment('Determina si el cliente o proveedor esta bloqueado');
            $table->string('logo', 255)->nullable()->comment('Imagen del cliente');
            $table->boolean('retention_source')->nullable()->comment('Determina si el cliente hace retención en la fuente');
            $table->double('percentage_retention_source')->nullable()->comment('Porcentaje de retencion en la fuente del proveedor');
            $table->boolean('retention_iva')->nullable()->comment('Determina si se le factura con IVA al cliente');
            $table->double('percentage_retention_iva')->nullable()->comment('Porcentaje de retención de IVA del proveedor');
            $table->boolean('retention_ica')->nullable()->comment('Determina si se le factura con ICA al cliente');
            $table->double('percentage_retention_ica')->nullable();
            $table->integer('code_ica_id')->unsigned()->nullable();
            $table->integer('payment_bank_id')->nullable()->comment('Llave foranea que determina el código del banco de pago del proveedor');
            $table->integer('account_type')->nullable()->comment('Tipo de cuenta bancaria del proveedor');
            $table->string('account_number', 45)->nullable()->comment('Número de cuenta del proveedor');
            $table->timestamps();
            $table->softDeletes();

            $table->index('contact_id', 'fk_contacts_providers_contacts1_idx');
            $table->index('category_id', 'fk_contacts_providers_parameters1_idx');
            $table->index('code_ica_id', 'fk_contacts_providers_parameters2_idx');

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('category_id')->references('id')->on('parameters');
            $table->foreign('code_ica_id')->references('id')->on('parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_providers');
    }
}
