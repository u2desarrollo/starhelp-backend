<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaValuesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('criteria_values', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('relation_id');
			$table->integer('parameter_id')->unsigned();
			$table->integer('criteria_type_id')->unsigned();

			$table->index('parameter_id','fk_values_criteria_parameters1_idx');
			$table->index('criteria_type_id','fk_values_criteria_criteria_types1_idx');

			$table->foreign('parameter_id')->references('id')->on('parameters');
			$table->foreign('criteria_type_id')->references('id')->on('criteria_types');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('criteria_values');
	}
}