<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCenterCostAcoounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn(['cost_center_2', 'cost_center_3', 'cost_center_1']);
        });

        Schema::table('accounts', function (Blueprint $table) {
            $table->boolean('cost_center_1')->nullable()->comment('Captura Centro de costo 1');
            $table->boolean('cost_center_2')->nullable()->comment('Captura Centro de costo 2');
            $table->boolean('cost_center_3')->nullable()->comment('Captura Centro de costo 3');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            //
        });
    }
}
