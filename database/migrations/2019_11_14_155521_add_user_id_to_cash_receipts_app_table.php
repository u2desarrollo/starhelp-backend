<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToCashReceiptsAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_receipts_app', function (Blueprint $table) {
            $table->integer('contact_id')->unsigned()->nullable()->comment('llave foranea con contacts');
            $table->integer('consecutive')->nullable()->comment('consecutivo');
            $table->foreign('contact_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_receipts_app');
    }
    }

