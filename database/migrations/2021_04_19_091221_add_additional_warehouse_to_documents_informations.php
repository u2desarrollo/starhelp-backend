<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalWarehouseToDocumentsInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->integer('additional_contact_warehouse_id')->unsigned()->nullable()->comment('sucursal adicional');
            $table->foreign('additional_contact_warehouse_id')->references('id')->on('contacts_warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            //
        });
    }
}
