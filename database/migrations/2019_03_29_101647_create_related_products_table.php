<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatedProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('related_products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->integer('product_related_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->index('product_id', 'fk_product_id_idx');
			$table->index('product_related_id', 'fk_product_related_id_idx');

			$table->foreign('product_id')->references('id')->on('products');
			$table->foreign('product_related_id')->references('id')->on('products');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('related_products');
	}
}