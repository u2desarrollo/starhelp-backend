<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRetentionAccoundingServToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->integer('ret_fue_serv_account_id')->unsigned()->nullable();
            $table->integer('ret_iva_serv_account_id')->unsigned()->nullable();
            $table->integer('ret_ica_serv_account_id')->unsigned()->nullable();
            $table->integer('ret_fue_serv_provider_account_id')->unsigned()->nullable();
            $table->integer('ret_iva_serv_provider_account_id')->unsigned()->nullable();
            $table->integer('ret_ica_serv_provider_account_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('ret_fue_serv_account_id');
            $table->dropColumn('ret_iva_serv_account_id');
            $table->dropColumn('ret_ica_serv_account_id');
            $table->dropColumn('ret_fue_serv_provider_account_id');
            $table->dropColumn('ret_iva_serv_provider_account_id');
            $table->dropColumn('ret_ica_serv_provider_account_id');
        });
    }
}
