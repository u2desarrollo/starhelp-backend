<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTypeColumnDocumentsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->float('physical_unit')->change();
            $table->float('total_value_brut')->change();
            $table->float('total_value')->change();
            $table->float('inventory_cost_value')->change();

            $table->float('stock_after_operation')->change();
            $table->float('stock_value_after_operation')->change();
            $table->float('stock_company_after_operation')->change();
            $table->float('stock_company_value_after_operation')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->bigInteger('physical_unit')->change();
            //$table->bigInteger('unit_list_value')->change();
            $table->bigInteger('total_value_brut')->change();
            $table->bigInteger('total_value')->change();
            $table->bigInteger('inventory_cost_value')->change();

            //
            $table->bigInteger('stock_after_operation')->change();
            $table->bigInteger('stock_value_after_operation')->change();
            $table->bigInteger('stock_company_after_operation')->change();
            $table->bigInteger('stock_company_value_after_operation')->change();
        });
    }
}
