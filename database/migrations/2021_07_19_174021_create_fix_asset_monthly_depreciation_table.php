<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixAssetMonthlyDepreciationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fix_asset_monthly_depreciation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fix_asset_id')->unsigned();

            $table->string('year_month');
            $table->float('value_additions')->nullable();
            $table->float('historical_cost')->nullable();
            $table->float('balance_to_depreciated')->nullable();
            $table->float('depreciate_value_month')->nullable();
            $table->float('accumulated_depreciation_value')->nullable();
            $table->integer('months_accumulated')->unsigned();
            $table->boolean('initial_recording')->default(false);

            $table->foreign('fix_asset_id')->references('id')->on('fix_assets');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fix_asset_monthly_depreciation');
    }
}
