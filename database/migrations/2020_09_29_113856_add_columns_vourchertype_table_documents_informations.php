<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsVourchertypeTableDocumentsInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->string('vouchers_type_prefix')->nullable();
            $table->integer('vouchers_type_range_from')->nullable();
            $table->integer('vouchers_type_range_up')->nullable();
            $table->string('vouchers_type_resolution')->nullable();
            $table->boolean('vouchers_type_electronic_bill')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->dropColumn('vouchers_type_prefix');
            $table->dropColumn('vouchers_type_range_from');
            $table->dropColumn('vouchers_type_range_up');
            $table->dropColumn('vouchers_type_resolution');
            $table->dropColumn('vouchers_type_electronic_bill');
        });
    }
}
