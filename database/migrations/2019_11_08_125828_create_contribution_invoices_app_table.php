<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionInvoicesAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribution_invoices_app', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_receipts_app_id')->unsigned();
            $table->integer('document_id')->unsigned()->nullable();
            $table->integer('total_paid')->comment('total pagado');
            $table->integer('value_discount_prompt_payment')->comment('valor decuento pronto pago');
            $table->integer('invoice_days')->comment('dias transcurridos a partir de la fecha de creacion de la factura');
            

            //  ***   Retenciones
            $table->integer('rete_fte')->comment('retencion fuente');
            $table->integer('rete_iva')->comment('retencion iva');
            $table->integer('rete_ica')->comment('retencion ica');

            // saldo factura a la hora de operacion
            $table->integer('invoice_document')->nullable()->comment('Saldo factura operada');

            // ** 
            $table->timestamps();


            $table->foreign('cash_receipts_app_id')->references('id')->on('cash_receipts_app');
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contribution_invoices_app');
    }
}
