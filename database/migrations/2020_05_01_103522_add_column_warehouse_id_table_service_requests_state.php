<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWarehouseIdTableServiceRequestsState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_requests_state', function (Blueprint $table) {
            $table->integer('branchoffice_warehouse_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_requests_state', function (Blueprint $table) {
            $table->dropColumn('branchoffice_warehouse_id');
            
        });
    }
}
