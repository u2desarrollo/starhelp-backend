<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffectsDocumentIdToDocumentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->unsignedInteger('affects_document_id')->comment('Id del documento que afecta');

            $table->foreign('affects_document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->dropColumn('affects_document_id');
        });
    }
}
