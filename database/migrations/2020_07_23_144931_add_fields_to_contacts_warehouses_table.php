<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->date('creation_date')->nullable();
            $table->string('other_telephones', 200)->nullable();
            $table->unsignedInteger('payment_type_id')->nullable();

            $table->foreign('payment_type_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('creation_date');
            $table->dropColumn('other_telephones');
            $table->dropColumn('payment_type_id');
        });
    }
}
