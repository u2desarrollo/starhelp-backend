<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->char('id', 2)->unique()->index()->comment('Id del Departamento');
            $table->string('department', 30)->comment('Nombre del departamento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.     *
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
