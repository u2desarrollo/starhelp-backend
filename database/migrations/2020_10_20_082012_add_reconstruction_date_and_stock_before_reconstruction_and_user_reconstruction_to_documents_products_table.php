<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReconstructionDateAndStockBeforeReconstructionAndUserReconstructionToDocumentsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dateTime('reconstruction_date')->nullable();
            $table->integer('user_reconstruction')->nullable();
            $table->integer('stock_before_reconstruction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('reconstruction_date');
            $table->dropColumn('user_reconstruction');
            $table->dropColumn('stock_before_reconstruction');
            
        });
    }
}
