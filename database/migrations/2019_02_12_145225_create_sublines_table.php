<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSublinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sublines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_id')->unsigned()->comment('id de la linea');
            $table->string('subline_code')->comment('codigo de la sublinea');
            $table->string('subline_description')->comment('descripcion de la sublinea');
            $table->string('short_description')->comment('descripcion corta de la sublinea')->nullable();
            $table->integer('lock_buy')->comment('bloqueo compra subli 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('block_sale')->comment('bloqueo venta subli 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->string('margin_cost')->comment('Margen sobre precio venta para establecer el costo (servicios)')->nullable();
            $table->integer('calculates_sale_price')->comment('calcula precio venta subli 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();

            //CONTABILIDAD
            $table->integer('inventories_account')->comment('cuenta inventarios')->nullable();
            $table->integer('account_cost')->comment('cuenta costo')->nullable();
            $table->integer('sales_account')->comment('cuenta ventas')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Indice
            $table->unique(['line_id', 'subline_code'], 'line_subline_index');
            // Llaves foraneas
            $table->foreign('line_id')->references('id')->on('lines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sublines');
    }
}
