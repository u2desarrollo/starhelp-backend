<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTableDocumentTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('document_transactions');
        Schema::create('document_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year_month', 6)->comment('año mes');
            $table->integer('document_id')->nullable();
            $table->integer('branchoffice_id')->comment('Sede (PV)')->nullable();
            $table->integer('vouchertype_id')->comment('Tipo comprobante')->nullable();
            $table->integer('account_id')->comment('Cuenta Auxiliar');
            $table->integer('contact_id')->comment('Codigo Tercero');
            $table->integer('quota_number')->comment('Numero de cuota')->nullable();
            $table->string('consecutive')->comment('Consecutivo (Factura)')->nullable();
            $table->date('document_date')->comment('Fecha comprobante')->nullable();
            $table->integer('seller_id')->comment('Codigo Vendedor')->nullable();
            $table->date('issue_date')->comment('Fecha emision (Factura)')->nullable();
            $table->integer('pay_day')->comment('Dias de plazo')->nullable();
            $table->date('due_date')->comment('Fecha vencimiento')->nullable();
            $table->integer('concept')->comment('Codigo Concepto')->nullable();
            $table->longtext('detail')->comment('Detalle de la partida')->nullable();
            $table->longtext('refer_detail')->comment('Detalle Referencia')->nullable();
            $table->longtext('observations')->comment('Observacion')->nullable();
            $table->string('type_bank_document')->nullable();
            $table->integer('bank_document_number')->comment('Numero Documento bancario')->nullable();
            $table->integer('cost_center_1')->comment('Centro costo 1')->nullable();
            $table->integer('cost_center_2')->comment('Centro costo 2')->nullable();
            $table->integer('cost_center_3')->comment('Centro costo 3')->nullable();
            $table->integer('business_id')->comment('id del Negocio')->nullable();
            $table->integer('number_business')->comment('Codigo del Negocio')->nullable();
            $table->integer('payment_method')->nullable();
            $table->integer('crossing_document_id')->comment('id documento cruce')->nullable();
            $table->integer('crossing_vouchertype_id')->comment('tipo comprobante doc cruce')->nullable();
            $table->string('crossing_prefix')->comment('Prefijo doc cruce')->nullable();
            $table->integer('crossing_consecutive')->comment('Numero doc cruce')->nullable();
            $table->date('crossing_due_date_document')->comment('Fecha Venc doc cruce')->nullable();
            $table->bigInteger('operation_value')->comment('Valor de la partida')->nullable();
            $table->string('transactions_type')->comment('Tipo transaccion D-Debito  C-Credito');
            $table->bigInteger('base_value')->comment('Valor base')->nullable();
            $table->integer('base_document_id')->comment('ORIGEN: No.Pedido, No.Orden Produccion,')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('document_id')->references('id')->on('documents');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('vouchertype_id')->references('id')->on('voucherstypes');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('cost_center_1')->references('id')->on('parameters');
            $table->foreign('cost_center_2')->references('id')->on('parameters');
            $table->foreign('cost_center_3')->references('id')->on('parameters');
            $table->foreign('base_document_id')->references('id')->on('documents');

            $table->index(['year_month', 'contact_id', 'account_id', 'crossing_document_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
