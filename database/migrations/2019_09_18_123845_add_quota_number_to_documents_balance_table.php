<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuotaNumberToDocumentsBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_balance', function (Blueprint $table) {
            //
            //$table->dropColumn('accounting_account');
            //$table->dropColumn('quota_number');
            $table->bigInteger("quota_number")->nullable()->comment('Número de cuota');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_balance', function (Blueprint $table) {
            //
            $table->dropColumn("quota_number");

        });
    }
}
