<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBaseServRetentionTableDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->float('ret_fue_serv_base_value')->nullable();
            $table->float('ret_iva_serv_base_value')->nullable();
            $table->float('ret_ica_serv_base_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('ret_fue_serv_base_value');
            $table->dropColumn('ret_iva_serv_base_value');
            $table->dropColumn('ret_ica_serv_base_value');
        });
    }
}
