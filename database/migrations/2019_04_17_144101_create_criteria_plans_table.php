<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaPlansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('criteria_plans', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code', 6);
			$table->string('name', 45);
			$table->integer('param_table_id')->unsigned();
			$table->integer('criteria_type_id')->unsigned();

			$table->index('param_table_id','fk_plan_criteria_param_tables1_idx');
			$table->index('criteria_type_id','fk_plan_criteria_criteria_types1_idx');

			$table->foreign('param_table_id')->references('id')->on('paramtables');
			$table->foreign('criteria_type_id')->references('id')->on('criteria_types');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('criteria_plans');
	}
}