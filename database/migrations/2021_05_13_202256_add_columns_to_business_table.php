<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business', function (Blueprint $table) {
            $table->string('code')->nullable();
            $table->integer('contact_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->string('observation')->nullable();
            $table->string('name')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('contact_id');
            $table->dropColumn('type_id');
            $table->dropColumn('status');
            $table->dropColumn('observation');
        });
    }
}
