<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactLineTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_line', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('contact_id')->unsigned();
			$table->integer('line_id')->unsigned();
			$table->timestamps();

			$table->index('contact_id', 'fk_contact_contact_line_idx');
			$table->index('line_id', 'fk_line_contact_line_idx');

			$table->foreign('contact_id')->references('id')->on('contacts');
			$table->foreign('line_id')->references('id')->on('lines');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contact_line');
	}
}