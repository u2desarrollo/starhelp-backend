<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerIdToDocumentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function (Blueprint $table) {
			$table->integer('seller_id')->unsigned()->nullable()->comment('Vendedor');

			$table->index('seller_id', 'fk_seller_documents');
			$table->foreign('seller_id')->references('id')->on('contacts');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function (Blueprint $table) {
			$table->dropColumn('seller_id');
		});
	}
}