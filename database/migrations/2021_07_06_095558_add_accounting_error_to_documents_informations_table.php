<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountingErrorToDocumentsInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->boolean('accounting_error')->nullable()->default(false)->comment('Determina si el hubo algun error al contabilizar el documento');
            $table->text('accounting_description')->nullable()->comment('Descripcion de los errores contables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->dropColumn('accounting_error');
            $table->dropColumn('accounting_description');
        });
    }
}
