<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents_products', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('document_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->string('lot')->nullable();
			$table->string('serial_number')->nullable();

			$table->integer('seller')->comment('Vendedor')->nullable()->unsigned();
			$table->integer('technician')->comment('Técnico')->nullable()->unsigned();
			$table->date('lot_due_date')->comment('Fecha de vencimiento del lote')->nullable();

			$table->integer('line_id')->comment('Línea')->nullable()->unsigned();
			$table->integer('subline_id')->comment('Sublínea')->nullable()->unsigned();
			$table->integer('brand_id')->comment('Marca')->nullable()->unsigned();
			$table->integer('subbrand_id')->comment('Submarca')->nullable()->unsigned();

			$table->text('description')->comment('Descripción del producto')->nullable();
			$table->string('observation')->comment('Observación')->nullable();
			$table->boolean('applies_inventory')->comment('Aplica inventario')->nullable();

			$table->integer('quantity')->comment('Cantidad de operación')->nullable();
			$table->integer('bonus_quantity')->comment('Cantidad de bonificación')->nullable();
			$table->integer('missing_quantity')->comment('Cantidad faltante')->nullable();

			$table->double('unit_list_value')->comment('Valor de lista unitario')->nullable();
			$table->double('bonus_value')->comment('Valor bonificado')->nullable();
			$table->double('unit_value')->comment('Valor unitario')->nullable();
			$table->boolean('inventory_adjustment')->comment('Ajuste de inventario')->nullable();
			$table->double('iva_percentage')->comment('Porcentaje de IVA')->nullable();
			$table->double('consumption_tax_percentage')->comment('Porcentaje de impuesto al consumo')->nullable();

			$table->double('discount_value')->comment('Valor de descuento sobre el valor bruto')->nullable();

			$table->double('inventory_value')->comment('Valor de costo para afectar inventario')->nullable();
			$table->double('freight_value')->comment('Valor de flete')->nullable();
			$table->double('insurance_value')->comment('Valor de seguros')->nullable();

			$table->integer('unit_warehouse_balance')->comment('Saldo de unidades en bodega - después de la operación')->nullable();
			$table->double('value_warehouse_balance')->comment('Saldo de valores - después de la operación')->nullable();

			$table->integer('unit_balance')->comment('Saldo de unidades en compañía - después de la operación')->nullable();
			$table->double('value_balance')->comment('Saldo de valores en compañía - después de la operación')->nullable();

			$table->timestamps();
			$table->softDeletes();


			$table->index('document_id', 'fk_document_documents_products_idx');
			$table->index('product_id', 'fk_product_documents_products_idx');
			$table->index('seller', 'fk_seller_documents_products_idx');
			$table->index('technician', 'fk_technician_documents_products_idx');
			$table->index('line_id', 'fk_line_documents_products_idx');
			$table->index('subline_id', 'fk_subline_documents_products_idx');
			$table->index('brand_id', 'fk_brand_documents_products_idx');
			$table->index('subbrand_id', 'fk_subbrand_documents_products_idx');

			$table->foreign('document_id')->references('id')->on('documents');
			$table->foreign('product_id')->references('id')->on('products');
			$table->foreign('seller')->references('id')->on('contacts');
			$table->foreign('technician')->references('id')->on('contacts');
			$table->foreign('line_id')->references('id')->on('lines');
			$table->foreign('subline_id')->references('id')->on('sublines');
			$table->foreign('brand_id')->references('id')->on('brands');
			$table->foreign('subbrand_id')->references('id')->on('subbrands');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('documents_products');
	}
}