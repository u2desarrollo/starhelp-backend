<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDocumentsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->integer('type_difference')->nullable();
            $table->integer('difference_quantity')->nullable();
            $table->float('difference_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('type_difference');
            $table->dropColumn('difference_quantity');
            $table->dropColumn('difference_value');
        });
    }
}
