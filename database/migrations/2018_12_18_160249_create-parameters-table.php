<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('paramtable_id')->unsigned()->comment('Id de la tabla de parámetros');
            $table->string('code_parameter', 5)->comment('Código del parámetro');
            $table->string('name_parameter', 45)->comment('Nombre del parametro');
            $table->string('alphanum_data_first', 60)->nullable()->comment('Valor del primer dato alfanumérico');
            $table->string('alphanum_data_second', 60)->nullable()->comment('Valor del segundo dato alfanumérico');
            $table->string('alphanum_data_third', 60)->nullable()->comment('Valor del tercer dato alfanumérico');
            $table->double('numeric_data_first')->nullable()->comment('Valor del primer dato numérico');
            $table->double('numeric_data_second')->nullable()->comment('Valor del segundo dato numérico');
            $table->double('numeric_data_third')->nullable()->comment('Valor del tercer dato numérico');
            $table->date('date_data_first')->nullable()->comment('Valor del primer dato de fecha');
            $table->date('date_data_second')->nullable()->comment('Valor del segundo dato de fecha');
            $table->date('date_data_third')->nullable()->comment('Valor del tercer dato de fecha');
            $table->string('image', 255)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('paramtable_id')->references('id')->on('paramtables')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paramtables');
    }
}
