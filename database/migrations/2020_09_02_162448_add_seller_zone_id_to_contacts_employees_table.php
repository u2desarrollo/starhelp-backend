<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerZoneIdToContactsEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_employees', function (Blueprint $table) {
            $table->unsignedInteger('seller_zone_id')->nullable()->comment('Zona de vendedor');
            $table->foreign('seller_zone_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_employees', function (Blueprint $table) {
            //
        });
    }
}
