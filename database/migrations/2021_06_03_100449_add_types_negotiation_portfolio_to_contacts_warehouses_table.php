<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesNegotiationPortfolioToContactsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->integer('types_negotiation_portfolio')->nullable()->comment('Tipo de Negociación en cartera');
            $table->foreign('types_negotiation_portfolio')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_warehouses', function (Blueprint $table) {
            $table->dropColumn('types_negotiation_portfolio');
        });
    }
}
