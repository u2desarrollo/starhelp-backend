<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('accounts');
        Schema::create('accounts', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('code', 255)->unique()->comment('Codigo asociado a la Cuenta');
            $table->string('name', 255)->comment('Nombre asignado a la Cuenta');
            $table->string('short_name', 255)->nullable()->comment('Nombre corto');
            $table->text('description')->nullable()->comment('Descripcion del contenido y proposito de la Cta');
            $table->integer('debit_credit')->unsigned()->comment('Saldo normalmentes Débito o Crédito');
            $table->integer('group')->unsigned()->comment(' Activo, Pasivo, Patrimonio, Ingresos, Gastos, etc');
            $table->integer('level')->unsigned()->comment('Auxiliar. Cuenta, Submayor, Mayor, Grupo');
            $table->integer('account_totalize')->unsigned()->comment('Cuenta superior sobre la cual totaliza ');
            $table->integer('category')->unsigned()->nullable()->comment('Categoria de Cuenta');
            $table->integer('branchoffice_id')->unsigned()->nullable()->comment('Llave foranea de branchoffices');
            $table->integer('contact_id')->unsigned()->nullable()->comment('Tercero asociado a la Cta');
            $table->boolean('manage_contact_balances')->nullable()->comment('Maneja saldos de terceros');
            $table->boolean('is_customer')->nullable()->comment('Determina si es un cliente');
			$table->boolean('is_provider')->nullable()->comment('Determina si es un proveedor');
			$table->boolean('is_employee')->nullable()->comment('Determina si es un empleado');
            $table->boolean('resume_beginning_year')->nullable()->comment('Resume saldos en el tercero asociado a la Cta al inicio año');
            $table->integer('wallet_type')->unsigned()->nullable()->comment('Cartera clientes, Cartera proveedores, Otros');
            $table->string('document_description', 255)->nullable()->comment('Descripcion del socumento: Factura, Pagare, etc');
            $table->boolean('quota_balances')->nullable()->comment('Maneja saldos de Cuotas en cada factura');
            $table->integer('cost_center_2')->nullable()->unsigned()->comment('Captura Centro de costo 2');
            $table->integer('cost_center_3')->nullable()->unsigned()->comment('Captura Centro de costo 3');
            $table->integer('cost_center_1')->nullable()->unsigned()->comment('Captura Centro de costo 1');
            $table->boolean('manage_businesses')->nullable()->comment('Ejm. Importación, construcción, etc');
            $table->boolean('manage_businesses_stage')->nullable()->comment('Etapas asociadas a los negocios');
            $table->boolean('manage_bank_document')->nullable()->comment('Consignacion, cheque, Nota-Db, etc ');
            $table->boolean('manage_way_to_pay')->nullable()->comment('Captura Forma de pago: Efectivo, Tarjeta-Cr Bancolombia..');
            $table->integer('tax_account')->nullable()->unsigned()->comment('Iva, Ica, Ret-Fuente, Ret-Iva, Ret-Ica');
            $table->boolean('Active_account')->default(true)->comment('');
            $table->dateTime('account_created_at')->nullable()->comment('');
            $table->dateTime('account_cancellation_date')->nullable()->comment('');
            $table->string('pevious_account', 255)->nullable()->comment('Codigo de la cuenta en sistema anterior. Sirve para convertir a la contab U2.');
            
            
            
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('contact_id')->references('id')->on('contacts');

            // tablas de parametros
            $table->foreign('group')->references('id')->on('parameters');
            $table->foreign('level')->references('id')->on('parameters');
            $table->foreign('category')->references('id')->on('parameters');
            $table->foreign('wallet_type')->references('id')->on('parameters');
            $table->foreign('cost_center_2')->references('id')->on('parameters');
            $table->foreign('cost_center_3')->references('id')->on('parameters');
            $table->foreign('cost_center_1')->references('id')->on('parameters');
            
            // relación a taxes
            $table->foreign('tax_account')->references('id')->on('taxes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
