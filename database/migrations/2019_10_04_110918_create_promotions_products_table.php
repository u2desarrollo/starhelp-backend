<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promotion_id')->comment('llave foranea con la tabla de promociones');
            $table->integer('product_id')->comment('llave foranea relacion con tabla productos');
            $table->integer('minimum_amount')->nullable()->comment('cantidad minima');

            $table->double('discount')->nullable()->comment('descuento');
            $table->double('pay_soon')->nullable()->comment('pronto pago');
            $table->double('cash_payment')->nullable()->comment('pago contado');
            $table->integer('discount_value')->nullable()->comment('valor del descuento');

            $table->timestamps();


            $table->foreign('promotion_id')->references('id')->on('promotions');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions_products');
    }
}
