<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_header_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('quantity')->nullable();
            $table->string('serial')->nullable();
            $table->date('certificate_expiration_date')->nullable();
            $table->string('fault_description')->nullable();
            $table->string('photo')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->index('request_header_id', 'fk_request_products_request_headers_idx');
            $table->index('product_id', 'fk_request_products_products_idx');

            $table->foreign('request_header_id')->references('id')->on('request_headers');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_products');
    }
}
