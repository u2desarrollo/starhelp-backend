<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsProductsTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_products_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_product_id')->unsigned()->comment('Id de documents_products (Detalle documento)');
            $table->integer('tax_id')->unsigned()->comment('Id de tax (Impuesto)');
            $table->bigInteger('base_value')->comment('Valor base');
            $table->double('tax_percentage')->comment('Porcentaje del impuesto');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('document_product_id')->references('id')->on('documents_products');
            $table->foreign('tax_id')->references('id')->on('taxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_products_taxes');
    }
}
