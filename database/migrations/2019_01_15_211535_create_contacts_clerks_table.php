<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsClerksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_clerks', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->integer('identification_type')->nullable()->unsigned()->comment('Tipo de identificación');
            $table->string('identification', 20)->nullable()->comment('Número de identificación');
            $table->string('name', 150)->comment('Nombre');
            $table->string('surname', 60)->comment('Apellido');
            $table->integer('gender')->nullable()->comment('Genero');
            $table->date('birthdate')->nullable()->comment('Fecha de cumpleaños');
            $table->integer('city_id')->nullable()->unsigned()->comment('LLave foranea de la tabla Cities');
            $table->string('address', 100)->nullable()->comment('Dirección comercial');
            $table->string('stratum', 1)->nullable()->comment('Estrato socioeconomico');
            $table->string('main_telephone', 30)->nullable()->comment('Número de teléfono principal');
            $table->string('secondary_telephone', 30)->nullable()->comment('Número de teléfono secundario');
            $table->string('cell_phone', 20)->nullable()->comment('Número de celular');
            $table->string('email', 60)->nullable()->comment('Correo electrónico');
            $table->string('position', 80)->nullable()->comment('Correo electrónico');
            $table->string('observation', 255)->nullable()->comment('Correo electrónico');
            $table->timestamps();
            $table->softDeletes();

            $table->index('contact_id','fk_contacts_clerks_contacts1_idx');

            $table->foreign('contact_id')->references('id')->on('contacts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_clerks');
    }
}
