<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('license_id')->unsigned()->comment('Id de la licencia');
            $table->string('identification', 20)->comment('Numero de la identificación - Parametro');
            $table->integer('check_digit')->comment('Digito de Verificación')->nullable();
            $table->integer('identification_type')->comment('Tipo de identificación')->unsigned();
            $table->string('name', 150)->comment('Nombre de la Empresa');
            $table->string('short_name', 45)->comment('Nombre corto de la Empresa')->nullable();
            $table->string('email', 60)->comment('Email')->nullable();
            $table->string('web_page', 60)->comment('Pagina web')->nullable();
            $table->integer('class_person')->comment('Tipo de Persona - Parametro')->nullable()->unsigned();
            $table->integer('taxpayer_type')->comment('Tipo de retención - Parametro')->nullable()->unsigned();
            $table->integer('tax_regimen')->comment('Regimen - Parametro')->nullable()->unsigned();
            $table->string('id_legal_representative', 20)->comment('Documento Representante Legal')->nullable();
            $table->string('name_legal_representative', 60)->comment('Nombre Representante Legal')->nullable();
            $table->string('logo')->comment('Logo del suscriptor');
            $table->integer('company_type')->comment('Tipo de empresa - Parametro')->nullable()->unsigned();
            $table->boolean('state')->comment('estado del suscriptor')->nullable();
            $table->timestamp('firmDate')->comment('fecha en firma')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Relacion
            $table->foreign('license_id')->references('id')->on('licenses');
            $table->foreign('class_person')->references('id')->on('parameters');
            $table->foreign('taxpayer_type')->references('id')->on('parameters');
            $table->foreign('tax_regimen')->references('id')->on('parameters');
            $table->foreign('company_type')->references('id')->on('parameters');
            $table->foreign('identification_type')->references('id')->on('parameters');
        });

       ///*  */ DB::statement("ALTER TABLE licenses COMMENT 'Suscriptor propietario de la o las licencias' ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
