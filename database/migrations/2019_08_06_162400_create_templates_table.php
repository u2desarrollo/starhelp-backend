<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->comment('Codigo del modelo');
            $table->string('description')->comment('Descripción del modelo');
            // Documento
            $table->integer('operationType_id')->comment('Tipo de operacion')->nullable();
            $table->integer('voucherType_id')->comment('Tipo de comprobante')->nullable();
            $table->integer('consecutive_handling')->comment('Manejo de Consecutvio 1-individual x sede, 2-general para todas sedes')->nullable();
            $table->integer('return_model_id')->comment('modelo devolucion')->nullable();
            $table->boolean('select_branchoffice')->comment('selecciona sede SI/NO')->nullable();
            $table->boolean('select_warehouse')->comment('selecciona bodega SI/NO')->nullable();
            $table->integer('select_cost_center')->comment('selecciona centro de costo:1-SI,2-NO,9-OB')->nullable();
            $table->integer('seller_mode')->comment('1-Vendedores x sede,2-Todos los vendedores')->nullable();
            $table->integer('currency')->comment('moneda')->nullable();
            $table->string('observation_tittle_1')->comment('titulo observacion 1')->nullable();
            $table->string('observation_tittle_2')->comment('titulo observacion 2')->nullable();
            $table->string('footnotes')->comment('Notas pie de pagina')->nullable();
            $table->integer('user_id')->comment('requiere autorizacion')->nullable();
            $table->integer('select_conveyor')->comment('selecciona transportadora:1-SI,2-NO,9-OB')->nullable();
            $table->integer('capt_delivery_address')->comment('Captura Dirección:1-SI,2-NO,9-OB')->nullable();
            $table->integer('capt_license')->comment('Captura Placa:1-SI,2-NO,9-OB')->nullable();
            $table->string('final_function')->comment('Función Final')->nullable();
            $table->string('control_stocks')->nullable()->comment('controlar existencias');
            $table->string('sales_price_control')->nullable()->comment('control precio venta');
            // Tercero
            //buscador tercero
            $table->boolean('contact_capture')->comment('Captura Tercero SI/NO')->nullable();
            $table->integer('contact_type')->comment('tipo de tercero:1-Cliente,2-Proveedor,3-Empleado')->nullable();
            $table->integer('contact_category')->comment('Categoria de tercero')->nullable();
            $table->integer('contact_id')->comment('Tercero')->nullable();
            //Información Tercero
            $table->integer('basic_contact_information')->comment('Datos Basicos Tercero:1-No visualiza,2-visualiza')->nullable();
            $table->boolean('financial_info')->comment('Ver informacion financiera SI/NO')->nullable();
            $table->boolean('button_360')->comment('Ver boton 360 SI/NO')->nullable();
            $table->integer('send_contact_email')->comment('Envio e-mail tercero 1-NO  2-Simpre que tenga e-mail  3-Obligatorio')->nullable();
            // Doc Base / Cruce
            //doc base
            $table->integer('doc_base_management')->comment('Manejo Doc-Base 1-Si  2-no 9-Obligatorio')->nullable();
            $table->integer('voucher_type_id')->comment('tipo de comprobante')->nullable();
            $table->integer('model_id')->comment('modelo')->nullable();
            $table->boolean('see_backorder')->comment('ver backorder SI/NO')->nullable();
            //doc cruce
            $table->integer('doc_cruce_management')->comment('Manejo Doc-cruce 1-Si  2-no 9-Obligatorio')->nullable();
            $table->string('tittle')->comment('titulo')->nullable();
            $table->boolean('see_seller_doc')->comment('Ver Vendedor SI/NO')->nullable();
            $table->integer('valid_exist')->comment('Manejo Valida Si existe 1-Sin Validacion  2-Advierte si existe   3-Bloquea si existe')->nullable();
            $table->boolean('value_control')->comment('control valores si/no')->nullable();
            $table->boolean('quantity_control')->comment('control cantidades si/no')->nullable();
            $table->integer('crossing_document_dates')->comment('Fecha de Emision, Dias de plazo y Fecha de Vencimiento 1-Si  2-no 9-Obligatorio')->nullable();
            //Productos
            $table->boolean('add_products')->comment('agregar productos SI/NO')->nullable();
            $table->integer('price_list_id')->comment('Lista de Precios')->nullable();
            $table->boolean('add_promotions_button')->comment('Boton Agregar Promociones SI/NO')->nullable();
            $table->boolean('recalc_iva_less_cost')->comment('Recalc-IVA Venta menor al costo SI/NO')->nullable();
            $table->boolean('recalc_iva_base')->comment('Base Recalculo IVA SI/NO')->nullable();
            $table->boolean('up_excel_button')->comment('Boton Subir desde Excel SI/NO')->nullable();
            $table->boolean('down_excel_button')->comment('Boton Bajar Plantilla Excel SI/NO')->nullable();
            //Buscador Productos
            $table->integer('line_id')->comment('Lineas Productos permitidas')->nullable();
            $table->boolean('see_location')->comment('ver ubicacion SI/NO')->nullable();
            //Columnas Productos
            $table->boolean('modify_prices')->comment('editar precios SI/NO')->nullable();
            $table->boolean('see_discount')->comment('ver descuentos SI/NO')->nullable();
            $table->boolean('edit_discount')->comment('editar descuentos SI/NO')->nullable();
            $table->boolean('see_bonus_amount')->comment('ver cantidad bonificada SI/NO')->nullable();
            $table->boolean('edit_bonus_amount')->comment('edita cantidad bonificada SI/NO')->nullable();
            $table->boolean('see_vrunit_base')->comment('Ver Vr-Unit Base / Negoc SI/NO')->nullable();
            $table->boolean('see_cant_backorder')->comment('ver cantidad backorder SI/NO')->nullable();
            $table->integer('backorder_quantity_control')->comment('control cant backorder:1.Sin validacion 2.Igual a BO 3.Admite Parcial de BO. No mayor a BO')->nullable();
            $table->boolean('see_physical_cant')->comment('ver cant fisica SI/NO')->nullable();
            $table->boolean('edit_physical_cant')->comment('editar cant fisica SI/NO')->nullable();
            $table->boolean('see_lots')->comment('ver lotes SI/NO')->nullable();
            $table->boolean('edit_lots')->comment('editar lotes SI/NO')->nullable();
            $table->boolean('edit_unit_value')->comment('editar valor unitario SI/NO')->nullable();
            $table->boolean('see_applied_promotion')->comment('ver promocion aplicada SI/NO')->nullable();
            $table->boolean('see_seller')->comment('ver vendedor SI/NO')->nullable();
            $table->boolean('edit_seller')->comment('editar vendedor SI/NO')->nullable();
            // news
            $table->integer('sale_price_control')->nullable()->comment('control precio venta');
            $table->integer('stock_control')->nullable()->comment('control existencias');

            $table->boolean('see_product_observation')->comment('ver observaciones por producto SI/NO')->nullable();
            $table->boolean('edit_product_observation')->comment('editar observaciones por producto SI/NO')->nullable();
            $table->boolean('hide_value')->comment('oculta valores SI/NO')->nullable();
            $table->boolean('cents_values')->comment('valores con centavos SI/NO')->nullable();
            $table->boolean('hands_free_operation')->comment('manejo manos libres SI/NO')->nullable();
            //Totales Documento
            $table->boolean('discount_foot_bill')->comment('Descuento Pie Factura SI/NO')->nullable();
            $table->integer('retention_management')->comment('manejo retencion: 1-no, 2-reten manual, 3-reten auto')->nullable();
            $table->integer('handle_payment_methods')->comment('Manejo Formas de pago:1-SI,2-NO,9-OB')->nullable();
            $table->integer('payment_methods_allowed_id')->comment('Formas de pago permitidas')->nullable();
            $table->boolean('purchase_difference_management')->comment('Manejo de Diferencias en Compras SI/NO')->nullable();

            // impresión
            $table->boolean('print')->comment('imprime SI/NO')->nullable();
            $table->integer('print_template')->comment('Plantilla de impresion')->nullable();
            $table->string('print_tittle')->comment('titulo impresion')->nullable();
            $table->integer('product_order')->comment('1-Ord de Registro  2-Ord Alfabetico  3-Ord Marca/Alfabetico')->nullable();
            $table->boolean('total_letters')->comment('total en letras SI/NO')->nullable();
            $table->boolean('cash_drawer_opens')->comment('abre cajon monedero SI/NO')->nullable();
            $table->integer('TRM_value')->comment('trm')->nullable();
            $table->integer('minimal_price')->comment('precio minimo')->nullable();
            $table->integer('import_number')->comment('numero de importacion')->nullable();
            $table->integer('branchoffice_voucherType_id')->comment('sede tipo de comprobante')->nullable();
            $table->integer('branchoffice_change')->comment('1-bodega de la misma sede, 2-todas las bodegas')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('operationType_id')->references('id')->on('types_operation');
            $table->foreign('voucherType_id')->references('id')->on('voucherstypes');
            $table->foreign('voucher_type_id')->references('id')->on('voucherstypes');
            $table->foreign('currency')->references('id')->on('parameters');
            $table->foreign('contact_category')->references('id')->on('parameters');
            $table->foreign('price_list_id')->references('id')->on('parameters');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('return_model_id')->references('id')->on('templates');
            $table->foreign('model_id')->references('id')->on('templates');
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('branchoffice_vouchertype_id')->references('id')->on('branchoffices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
