<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashReceiptsAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // table de recibos de caja
        Schema::create('cash_receipts_app', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consignment_id')->unsigned()->comment('llave foranea de la tabla consignaciones con posibilidad de ser nulo')->nullable();
            $table->string('id_erp')->unsigned()->comment('id del erp  con posibilidad de ser nulo')->nullable();
            $table->integer('warehouse_id')->unsigned()->comment('llave foranea con tabla contact_warehouses');
            $table->timestamp('creation_date')->comment('fecha de creacion')->nullable();
            $table->string('observations')->nullable()->comment('obcervaciones');
            $table->string('invoice_link')->nullable()->comment('url del recibo');
            $table->string('payment_applied_erp')->nullable()->comment('recibo de caja consignado y aplicado en erp');
            
            
            $table->softDeletes();
            $table->timestamps();



            $table->foreign('consignment_id')->references('id')->on('consignment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_receipts_app');
    }
}
