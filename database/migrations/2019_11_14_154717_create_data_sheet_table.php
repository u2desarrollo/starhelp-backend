<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_sheet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('internal_name');
            $table->string('visible_name');
            $table->string('type_value');
            $table->bigInteger('paramtable_id')->unsigned()->nullable();
            $table->text('tooltip');
            $table->timestamps();

            $table->foreign('paramtable_id')->references('id')->on('paramtables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_sheet');
    }
}