<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateFnGetSalesContactWarehousesProductsFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $function = 'CREATE OR REPLACE FUNCTION public.fn_get_sales_contact_warehouses_products()
                         RETURNS TABLE(contact_warehouse_id integer, product_id integer, last_sale_date date, last_sale_quantity integer, last_sale_unit_value bigint, new boolean)
                         LANGUAGE plpgsql
                        AS $function$
                            begin
                                return query
                                select
                                    distinct
                                    d.warehouse_id as contact_warehouse_id,
                                    dp.product_id,
                                    cast(d.document_date as date) as last_sale_date,
                                    dp.quantity as last_sale_quantity,
                                    cast(dp.unit_value_before_taxes as bigint) as last_sale_unit_value,
                                    p.created_at > (current_date - 30) as new
                                from documents_products dp
                                    inner join documents d on d.id = dp.document_id and d.deleted_at is null and d.in_progress = false
                                    inner join voucherstypes v on v.id = d.vouchertype_id
                                    inner join documents dv on dv.thread = d.id and d.id != dv.id and d.document_date::date != dv.document_date::date
                                    inner join products p on p.id = dp.product_id
                                where
                                        v.code_voucher_type  = \'205\'
                                    and dp.deleted_at is null
                                    and d.document_date >= current_date
                                order by d.document_date asc;
                            END;
                        $function$;';

        DB::unprepared("DROP FUNCTION IF EXISTS public.fn_get_sales_contact_warehouses_products();");
        DB::unprepared($function);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION IF EXISTS public.fn_get_sales_contact_warehouses_products()");
    }
}
