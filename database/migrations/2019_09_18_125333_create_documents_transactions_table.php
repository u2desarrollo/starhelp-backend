<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_code_puc')->nullable()->comment('Codigo Cuenta puc');
            $table->integer('contact_code')->nullable()->comment('Codigo tercero');

            $table->string('voucher_type')->nullable()->comment('Tipo Comprobante');   //voucher tYpe
            $table->integer('voucher_number')->nullable()->comment('Numero comprobante');
            $table->string('voucher_prefix')->nullable()->comment('Prefijo comprobante');
            $table->date('voucher_date')->nullable()->comment('Fecha comprobante');


            $table->string('document_number')->nullable()->comment('Numero de documento');
            $table->string('document_prefix')->nullable()->comment('prefijo factura');
            $table->integer('quota_number')->nullable()->comment('Numero de cuota');
            $table->date('document_date')->nullable()->comment('Fecha factura');
            $table->integer('term_days_document')->nullable()->comment('Numero dias plazo');
            $table->date('expiration_date_document')->nullable()->comment('Fecha vencimiento Documento');

            $table->string('year_month')->nullable()->comment('Año mes  // periodo contable');
            $table->string('detail')->nullable()->comment('Detalle');
            $table->string('description')->nullable()->comment('Descripción');
            $table->string('cost_center')->nullable()->comment('centro de costo');
            $table->string('type_bank_document')->comment('Tipo documento banco')->nullable();
            $table->integer('number_document')->comment('Número documento ')->nullable();
            $table->string('Production_order_number')->comment('Numero orden produccion ')->nullable();
            $table->string('cod_expense_concept')->comment('Cod concepto Gasto')->nullable();
            $table->boolean('consignment')->comment('Consignacion')->nullable();
            $table->string('identifier_1')->comment('identificador 1')->nullable();
            $table->string('identifier_2')->comment('identificador 2')->nullable();
            $table->string('identifier_3')->comment('identificador 3')->nullable();


            $table->integer('document_id')->unsigned()->comment('Id del documento');
            $table->integer('transaction_value')->nullable()->comment('valor de la transaccion');
            $table->string('transaction_type')->comment('tipo de transaccion ,Indicativos  (debito= D, credito=C)')->nullable();
            $table->integer('base_value')->nullable()->comment('valor base');
            $table->string('erp_id')->comment('id del erp')->nullable();
            $table->timestamps();
            $table->integer('payment_applied_erp')->nullable()->comment('pago en proceso');
            $table->integer('update_or_create')->nullable()->comment('saber que registros elimino');
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_transactions');
    }
}
