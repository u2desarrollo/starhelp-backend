<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayReceivablesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pay_receivables', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('documents_balance_id')->unsigned()->comment('Cuenta por cobrar');
			$table->integer('vouchertype_id')->unsigned()->nullable()->comment('Tipo de comprobante');
			$table->integer('voucher_number')->nullable()->comment('Número de comprobante');
			$table->date('pay_date')->comment('Fecha de pago');
			$table->integer('pay_way')->unsigned()->nullable()->comment('Forma de pago');
			$table->double('applied_value')->nullable()->comment('Valor aplicado');
			$table->timestamps();

			$table->index('documents_balance_id', 'fk_receivable_pay_receivables_idx');
			$table->index('vouchertype_id', 'fk_vouchertype_pay_receivables_idx');
			$table->index('pay_way', 'fk_parameters_pay_receivables_idx');

			$table->foreign('documents_balance_id')->references('id')->on('documents_balance');
			$table->foreign('vouchertype_id')->references('id')->on('voucherstypes');
			$table->foreign('pay_way')->references('id')->on('parameters');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pay_receivables');
	}
}
