<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeManageBussinessAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            Schema::table('accounts', function (Blueprint $table) {
                $table->dropColumn(['manage_businesses']);
            });
    
            Schema::table('accounts', function (Blueprint $table) {
                $table->integer('manage_businesses')->nullable()->comment('Ejm. Importación, construcción, etc... 0. NO, 1. SI, 3. OB');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->boolean('manage_businesses')->nullable()->comment('Ejm. Importación, construcción, etc');
        });
    }
}
