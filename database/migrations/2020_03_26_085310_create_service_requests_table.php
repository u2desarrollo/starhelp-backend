<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('service_requests_types_id');
            $table->bigInteger('product_id');
            $table->bigInteger('contact_id');
            $table->bigInteger('seller_id');

            $table->timestamp('date')->default(Carbon::now());

            $table->softDeletes();
            $table->timestamps();

            // Relacion
            $table->foreign('service_requests_types_id')->references('id')->on('service_requests_types');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('seller_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests');
    }
}
