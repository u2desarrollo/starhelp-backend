<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods_app', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_receipts_app_id')->comment('llave foranea de la tabla de recibos de caja');
            $table->integer('parameter_id')->unsigned()->comment('llave foranea con la tabla de parametros referenciando el metodo de pago cheque o efectivo');
            $table->integer('total_paid')->comment('total pagado');
            $table->string('bank')->nullable()->comment('banco');
            $table->string('check_number')->nullable()->comment('numero cheque');
            $table->timestamps();


            $table->foreign('cash_receipts_app_id')->references('id')->on('cash_receipts_app');
            $table->foreign('parameter_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods_app');
    }
}
