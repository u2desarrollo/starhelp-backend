<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableTemplateAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_accounting', function (Blueprint $table) {
            $table->boolean('departure_square')->nullable()->default(false);
            $table->string('debit_credit', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_accounting', function (Blueprint $table) {
            $table->dropColumn('departure_square');
            $table->dropColumn('debit_credit');
        });
    }
}
