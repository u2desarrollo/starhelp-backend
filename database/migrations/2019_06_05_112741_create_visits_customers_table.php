<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsCustomersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visits_customers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('seller_id')->unsigned()->comment('Vendedor');
			$table->integer('contact_id')->unsigned()->comment('Cliente');
			$table->integer('contact_warehouse_id')->unsigned()->nullable()->comment('Bodega');
			$table->integer('visit_type')->unsigned()->comment('Tipo de visita');
			$table->string('latitude_checkin')->comment('Latitud de checkin');
			$table->string('longitude_checkin')->comment('Longitud de checkin');
			$table->string('latitude_checkout')->nullable()->comment('Latitud de checkout');
			$table->string('longitude_checkout')->nullable()->comment('Longitud de checkout');
			$table->json('motive')->nullable()->comment('Motivo de visita');
			$table->tinyInteger('test')->default(0)->comment('Prueba');
			$table->integer('distance')->nullable()->comment('Distancia en metros');
			$table->dateTime('checkin_date')->nullable()->comment('Fecha de checkin');
			$table->dateTime('checkout_date')->nullable()->comment('Fecha de checkout');
			$table->string('observation')->nullable()->comment('Observaciones');
			$table->string('photo')->nullable()->comment('Foto');
			$table->string('checkout_ok')->default('NO');
			$table->timestamps();

			$table->index('seller_id', 'fk_seller_visits_customers_idx');
			$table->index('contact_id', 'fk_contact_visits_customers_idx');
			$table->index('contact_warehouse_id', 'fk_contact_warehouse_visits_customers_idx');
			$table->index('visit_type', 'fk_visit_type_visits_customers_idx');

			$table->foreign('seller_id')->references('id')->on('contacts');
			$table->foreign('contact_id')->references('id')->on('contacts');
			$table->foreign('contact_warehouse_id')->references('id')->on('contacts_warehouses');
			$table->foreign('visit_type')->references('id')->on('parameters');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('visits_customers');
	}
}