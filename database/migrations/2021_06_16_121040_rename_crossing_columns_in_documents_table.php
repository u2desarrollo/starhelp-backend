<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCrossingColumnsInDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->renameColumn('crossing_date_document', 'affects_date_document');
            $table->renameColumn('crossing_number_document', 'affects_number_document');
            $table->renameColumn('crossing_prefix', 'affects_prefix_document');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->renameColumn('affects_date_document', 'crossing_date_document');
            $table->renameColumn('affects_number_document', 'crossing_number_document');
            $table->renameColumn('affects_prefix_document', 'crossing_prefix');
        });
    }
}
