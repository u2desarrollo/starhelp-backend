<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentWaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_ways', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id')->unsigned()->comment('Recibo de caja');
            $table->integer('pay_way_id')->unsigned()->comment('Formas de pago');
            $table->double('value')->unsigned()->default(0)->comment('Valor');
            $table->string('bank')->nullable()->comment('Entidad bancaria');
            $table->string('bonus_number')->nullable()->comment('Número de cheque');
            $table->integer('user_id')->unsigned()->nullable()->comment('Usuario');
            $table->timestamps();

            $table->foreign('document_id')->references('id')->on('documents');
            $table->foreign('pay_way_id')->references('id')->on('parameters');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_ways');
    }
}
