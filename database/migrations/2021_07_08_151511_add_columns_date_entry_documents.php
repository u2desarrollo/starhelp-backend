<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDateEntryDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->date('date_entry')->comment('Fecha de Entrada Documentos @hom669')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->dropColumn('date_entry');
        });
    }
}
