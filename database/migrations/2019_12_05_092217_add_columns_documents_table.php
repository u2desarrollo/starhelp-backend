<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('status_id_authorized')->unsigned()->nullable();
            $table->bigInteger('max_value_authorized')->unsigned()->nullable();
            $table->integer('user_id_authorizes')->unsigned()->nullable();
            $table->date('date_authorized')->nullable();
            $table->text('observation_authorized')->nullable();

            $table->foreign('user_id_authorizes')->references('id')->on('users');
            $table->foreign('status_id_authorized')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropForeign('documents_user_id_authorizes_foreign');
            $table->dropForeign('documents_status_id_authorized_foreign');

            $table->dropColumn('status_id_authorized');
            $table->dropColumn('max_value_authorized');
            $table->dropColumn('user_id_authorizes');
            $table->dropColumn('date_authorized');
            $table->dropColumn('observation_authorized');
        });
    }
}