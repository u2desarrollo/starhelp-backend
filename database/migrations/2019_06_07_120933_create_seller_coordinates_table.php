<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerCoordinatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_coordinates', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('seller_id')->unsigned()->comment('Vendedor');
			$table->string('latitude')->comment('Latitud');
			$table->string('longitude')->comment('Longitud');
			$table->dateTime('datetime')->comment('Fecha y hora');
			$table->timestamps();

			$table->index('seller_id', 'fk_seller_seller_coordinates_idx');

			$table->foreign('seller_id')->references('id')->on('contacts');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('seller_coordinates');
	}
}