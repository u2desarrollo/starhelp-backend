<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->boolean('document_save_finish')->nullable();
            $table->boolean('capture_vehicle_data')->nullable();
            $table->boolean('technical_capture')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
           $table->dropColumn('document_save_finish');
           $table->dropColumn('capture_vehicle_data');
           $table->dropColumn('technical_capture');
        });
    }
}
