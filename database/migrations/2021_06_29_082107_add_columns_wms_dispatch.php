<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsWmsDispatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_informations', function (Blueprint $table) {

            $table->string('name_user_wms')->comment('Nombre de usuario que realizo el consumo en WMS @hom669')->nullable();
            $table->timestamp('date_hour_consume')->comment('Fecha y Hora del consumo en WMS @hom669')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_informations', function (Blueprint $table) {
            $table->dropColumn('name_user_wms');
            $table->dropColumn('date_hour_consume');
        });
    }
}
