<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsSoonPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts_soon_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_id')->unsigned();
            $table->integer('subline_id')->nullable()->unsigned();
            $table->integer('days_up')->unsigned();
            $table->integer('percentage')->unsigned();
            $table->integer('branchoffice_id')->unsigned();   
            $table->integer('brand_id')->unsigned();   
            
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('subline_id')->references('id')->on('sublines');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('brand_id')->references('id')->on('branchoffices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts_soon_payment');
    }
}
