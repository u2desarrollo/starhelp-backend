<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactsWarehousesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->integer('line_id')->nullable()->unsigned();
			$table->integer('seller_id')->nullable()->unsigned();
			$table->integer('price_list_id')->nullable()->unsigned();
			$table->double('credit_quota')->nullable();

			$table->index('line_id', 'fk_line_contacts_warehouses_idx');
			$table->index('seller_id', 'fk_seller_contacts_warehouses_idx');
			$table->index('price_list_id', 'fk_price_list_contacts_warehouses_idx');

			$table->foreign('line_id')->references('id')->on('lines');
			$table->foreign('seller_id')->references('id')->on('contacts');
			$table->foreign('price_list_id')->references('id')->on('parameters');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			// $table->dropIndex('fk_line_contacts_warehouses_idx');
			// $table->dropIndex('fk_seller_contacts_warehouses_idx');
			// $table->dropIndex('fk_price_list_contacts_warehouses_idx');

			// $table->foreign('line_id');
			// $table->foreign('seller_id');
			// $table->foreign('price_list_id');

			$table->dropColumn('line_id');
			$table->dropColumn('seller_id');
			$table->dropColumn('price_list_id');
			$table->dropColumn('credit_quota');
		});
	}
}