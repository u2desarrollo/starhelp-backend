<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryCashRegisterSquareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_cash_register_square', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('branchoffice_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->dateTime('dateFrom')->nullable();
            $table->dateTime('dateTo')->nullable();
            $table->text('note')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_cash_register_square');
    }
}
