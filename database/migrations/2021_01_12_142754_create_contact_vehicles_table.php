<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vehicle_type_id')->comment('Id del tipo de vehículo');
            $table->unsignedInteger('contact_id')->comment('Id del tipo del tercero asociado al vehículo');
            $table->string('year', 4)->comment('Año modelo del vehículo');
            $table->string('license_plate', 10)->comment('Placa del vehículo');
            $table->string('serial_number', 20)->nullable()->comment('Número de serial del vehículo');
            $table->integer('average_daily_kilometers')->nullable()->comment('Promedio de kilometros diarios del vehículo');
            $table->string('width', 20)->nullable()->comment('Ancho de la llanta del vehículo');
            $table->string('rin', 20)->nullable()->comment('Rin de la llanta del vehículo');
            $table->string('profile', 20)->nullable()->comment('Perfil de la llanta del vehículo');
            $table->string('observations', 150)->nullable()->comment('Observaciones');
            $table->timestamps();

            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types');
            $table->foreign('contact_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_vehicles');
    }
}
