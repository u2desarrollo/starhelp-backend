<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTrmTableDocumentsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->integer('total_value_us')->unsigned()->nullable();
            $table->integer('total_value_brut_us')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations. 
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('total_value_us');
            $table->dropColumn('total_value_brut_us');
        });
    }
}
