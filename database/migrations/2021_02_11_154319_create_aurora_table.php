<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuroraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aurora', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('type')->unsigned()->comment('Tipo');
            $table->integer('option')->unsigned()->comment('Opción');
            $table->integer('frequency')->unsigned()->comment('frecuencia');
            $table->integer('period')->unsigned()->nullable()->comment('periodo');
            $table->string('hour')->comment('Hora');
            $table->boolean('active')->unsigned()->comment('Activo');
            

            $table->softDeletes();
            $table->timestamps();
            $table->foreign('option')->references('id')->on('parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aurora');
    }
}
