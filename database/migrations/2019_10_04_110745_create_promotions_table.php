<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id')->comment('Identificador unico');

            $table->integer('promotion_type_id')->comment('tipo de promoción');
            $table->integer('line_id')->nullable()->comment('llave foranea relacion con tabla lineas');;
            $table->integer('subline_id')->nullable()->comment('llave foranea con la tabla de sublineas');
            $table->integer('brand_id')->nullable()->comment('llave foranea relacion con tabla marca');
            $table->integer('subbrand_id')->nullable()->comment('llave foranea relacion con tabla submarca');
            $table->string('description')->comment('descripcion');
            $table->date('start_date')->comment('fecha de inicio');
            $table->date('end_date')->comment('fecha de fin');
            $table->integer('minimum_gross_value')->nullable()->comment('valor minimo bruto');
            $table->boolean('only_available')->nullable()->comment('solo aplica para productos disponibles???');
            $table->double('discount')->nullable()->comment('descuento');
            $table->double('pay_soon')->nullable()->comment('pronto pago');
            $table->double('cash_payment')->nullable()->comment('pago contado');
            $table->integer('gift_type')->nullable()->comment('tipo de obsequio');
            $table->integer('specific_product_id')->nullable()->comment('id producto especifico');
            $table->integer('branchoffice_id')->nullable()->comment('id de la sucursal de colsaisa');
            $table->string('image')->nullable()->comment('imagen');


            //foraneas
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('subline_id')->references('id')->on('sublines');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('subbrand_id')->references('id')->on('subbrands');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
