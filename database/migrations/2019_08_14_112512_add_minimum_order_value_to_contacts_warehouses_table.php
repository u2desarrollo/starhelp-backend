<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumOrderValueToContactsWarehousesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('contacts_warehouses', function (Blueprint $table) {
      $table->integer('minimum_order_value')->default(0)->comment('Valor minimo del pedido');
    });
  }
  
  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('contacts_warehouses', function (Blueprint $table) {
      $table->dropColumn('minimum_order_value');
    });
  }
}
