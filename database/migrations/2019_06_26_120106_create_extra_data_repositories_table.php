<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateExtraDataRepositoriesTable.
 */
class CreateExtraDataRepositoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        //Schema::dropIfExists('extra_data_repositories');
		Schema::create('extra_data_repositories', function(Blueprint $table) {
            $table->increments('id');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //Schema::dropIfExists('extra_data_repositories');
		Schema::drop('extra_data_repositories');
	}
}
