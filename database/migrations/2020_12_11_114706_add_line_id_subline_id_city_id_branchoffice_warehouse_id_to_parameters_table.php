<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLineIdSublineIdCityIdBranchofficeWarehouseIdToParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->integer('line_id')->unsigned()->nullable()->comment('linea');
            $table->integer('subline_id')->unsigned()->nullable()->comment('sub linea');
            $table->integer('city_id')->unsigned()->nullable()->comment('ciudad');
            $table->integer('branchoffice_warehouses_id')->unsigned()->nullable()->comment('bodega');


            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('subline_id')->references('id')->on('sublines');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('branchoffice_warehouses_id')->references('id')->on('branchoffice_warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->dropColumn('line_id');
            $table->dropColumn('subline_id');
            $table->dropColumn('city_id');
            $table->dropColumn('branchoffice_warehouses_id');
        });
    }
}
