<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersB2bTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners_b2b', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('line_id')->unsigned();
			$table->integer('parameter_id')->unsigned();
			$table->string('name');
			$table->string('big_banner');
			$table->string('small_banner');
			$table->date('since');
			$table->date('until');
			$table->boolean('state');
			$table->timestamps();
			$table->softDeletes();

			$table->index('line_id', 'fk_line_id_idx');
			$table->index('parameter_id', 'fk_parameter_id_idx');

			$table->foreign('line_id')->references('id')->on('lines');
			$table->foreign('parameter_id')->references('id')->on('parameters');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('banners_b2b');
	}
}