<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {

            $table->integer('branchoffice_id')->unsigned()->nullable()->comment('Llave foranea de la tabla branchoffices');
            $table->integer('branchoffice_warehouse_id')->unsigned()->nullable()->comment('Llave foranea de la tabla branchoffice_warehouses');
            $table->string('observation_two')->nullable()->comment('Observación dos');

            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('branchoffice_warehouse_id')->references('id')->on('branchoffice_warehouses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            //
        });
    }
}
