<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderMinimumAmmountToContactsEmployeesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_employees', function (Blueprint $table) {
			$table->integer('order_minimum_ammount')->nullable()->comment('Cantidad mínima de pedido');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_employees', function (Blueprint $table) {
			$table->dropColumn('order_minimum_ammount');
		});
	}
}