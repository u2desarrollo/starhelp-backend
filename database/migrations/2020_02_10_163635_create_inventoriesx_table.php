<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventoriesx', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branchoffice_warehouse_id')->unsigned()->comment('Llave foranea de branchoffices_warehouses');
            $table->integer('product_id')->unsigned()->comment('Llave foranea de products');
            $table->double('stock')->comment('Saldo en unidades');
            $table->double('stock_values')->comment('Saldo en valores');
            $table->double('average_cost')->comment('Costo promedio');
            $table->timestamps();

            $table->foreign('branchoffice_warehouse_id')->references('id')->on('branchoffice_warehouses');
            $table->foreign('product_id')->references('id')->on('products');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoriesx');
    }
}
