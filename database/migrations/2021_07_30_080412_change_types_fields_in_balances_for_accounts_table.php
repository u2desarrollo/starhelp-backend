<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypesFieldsInBalancesForAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balances_for_accounts', function (Blueprint $table) {
            $table->float('original_balance')->default(0)->comment('Saldo original')->change();
            $table->float('initial_balance_month')->default(0)->comment('Saldo inicial mes')->change();
            $table->float('debits_month')->default(0)->comment('Débitos mes')->change();
            $table->float('credits_month')->default(0)->comment('Créditos mes')->change();
            $table->float('final_balance')->default(0)->comment('Saldo final')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balances_for_accounts', function (Blueprint $table) {
            //
        });
    }
}
