<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendPointToContactsWarehousesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->string('send_point')->nullable()->comment('Punto de envío');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->dropColumn('send_point');
		});
	}
}