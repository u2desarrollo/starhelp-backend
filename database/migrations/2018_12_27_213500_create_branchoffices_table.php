<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchofficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branchoffices', function (Blueprint $table) {
            $table->increments('id')->comment('Llave primaria');
            $table->integer('subscriber_id')->unsigned()->comment('LLave foranea tabla subscribers');
            $table->string('name', 45)->comment('Nombre de la sucursal');
            $table->string('address', 70)->comment('Dirección de la sucursal');
            $table->string('telephone', 45)->nullable()->comment('Teléfono de la sucursal');
            $table->string('fax', 45)->nullable()->comment('Faxde la sucursal');
            $table->boolean('main')->default(false)->comment('Determina si es la sucursal principal del suscriptor');
            $table->boolean('branchoffice_type')->comment('Determina si es oficina administrativa o punto de venta');
            $table->integer('city_id')->comment('Llave foranea de la tabla cities')->unsigned();
            $table->string('latitude', 45)->nullable()->comment('Latitud de geolocalización');
            $table->string('length', 45)->nullable()->comment('Longitud de geolocalización');
            $table->integer('minimum_order_amount')->nullable()->comment('monto minimo pedido ');
            $table->string('email')->nullable()->comment('correo');

            $table->foreign('subscriber_id')->references('id')->on('subscribers');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branchoffices');
    }
}
