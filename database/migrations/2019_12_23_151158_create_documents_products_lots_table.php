<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsProductsLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_products_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('documents_products_id')->unsigned();
            $table->string('num_lot');
            $table->date('expiration_date')->nullable();
            $table->timestamps();

            $table->foreign('documents_products_id')->references('id')->on('documents_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_products_lots');
    }
}
