<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuVideosTutorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_videos_tutorial', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('menu_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->text('note')->nullable();
            $table->text('title');
            $table->text('url');

            $table->foreign('menu_id')->references('id')->on('menus');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_videos_tutorial');
    }
}
