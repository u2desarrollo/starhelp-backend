<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsBalanceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents_balance', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('document_id')->unsigned()->comment('Factura');
            $table->string('year_month');
            $table->integer('Account_code')->nullable()->comment('Codigo cuenta');
            $table->integer('contact_code')->nullable()->comment('Codigo tercero');
            $table->integer('document_code')->nullable()->comment('Codigo documento');
			$table->double('invoice_balance')->nullable()->comment('Saldo cierre mes');
			
			$table->timestamp('document_date')->nullable()->comment('fecha docuemnto saldo');
			$table->integer('update_or_create')->nullable()->comment('saber que registros elimino');
			$table->timestamps();



			$table->foreign('document_id')->references('id')->on('documents');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('documents_balance');
	}
}
