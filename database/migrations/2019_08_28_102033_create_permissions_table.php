<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePermissionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('permissions');

		Schema::create('permissions', function (Blueprint $table) {
			$table->increments('id');
			$table->string('permission')->comment('Permiso');
            $table->integer('created_by')->comment('Usuario que crea el permiso');
            $table->integer('module')->comment('1 para administracion starcommerce , 2 para b2b');
			$table->timestamps();

			$table->foreign('created_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('permissions');
	}
}
