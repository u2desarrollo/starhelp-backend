<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErpIdToDocumentsBalanceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents_balance', function (Blueprint $table) {
            $table->string('erp_id')->nullable();
            $table->unique(['document_id', 'erp_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents_balance', function (Blueprint $table) {
			$table->dropColumn('erp_id');
		});
	}
}
