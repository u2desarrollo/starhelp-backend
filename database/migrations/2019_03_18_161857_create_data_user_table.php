<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_user', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('Llave foranea que contiene el id del usuario');
            $table->integer('datable_id')->unsigned()->comment('Llave foranea que contiene el id del funcionario o del tercero (empleado)');
            $table->string('datable_type')->comment('Contiene el modelo al que esta relacionado el usuario');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_user');
    }
}
