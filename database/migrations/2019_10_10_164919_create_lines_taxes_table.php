<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTaxesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lines_taxes', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('line_id')->unsigned();
			$table->integer('tax_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('line_id')->references('id')->on('lines');
			$table->foreign('tax_id')->references('id')->on('taxes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lines_taxes');
	}
}
