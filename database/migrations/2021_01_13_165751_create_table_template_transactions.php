<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTemplateTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('template_id')->unsigned()->comment('ID del modelo');

            $table->string('order')->nullable();
            $table->string('description')->nullable();
            $table->integer('account_aux_id')->unsigned()->nullable();
            $table->integer('account_root_id')->unsigned()->nullable();
            $table->string('db_cr')->nullable();
            $table->boolean('cap_contact')->nullable()->default(false);
            $table->integer('default_contact_id')->unsigned()->nullable();
            $table->boolean('create_contact')->nullable()->default(false);
            $table->string('default_detail')->nullable();
            $table->boolean('square_match')->nullable()->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('account_aux_id')->references('id')->on('accounts');
            $table->foreign('account_root_id')->references('id')->on('accounts');
            $table->foreign('default_contact_id')->references('id')->on('contacts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_transactions');
    }
}
