<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsFixAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fix_assets', function (Blueprint $table) {
            $table->renameColumn('balance_account_id', 'account_accumulated_depreciation_id');
            $table->renameColumn('result_account_id', 'account_depreciation_expense_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fix_assets', function (Blueprint $table) {
            $table->renameColumn('account_accumulated_depreciation_id', 'balance_account_id');
            $table->renameColumn('account_depreciation_expense_id', 'result_account_id');
        });
    }
}
