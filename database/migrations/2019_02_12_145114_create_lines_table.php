<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscriber_id')->unsigned()->comment('Id del suscriptor');
            $table->string('line_code')->comment('Codigo de la Linea');
            $table->string('line_description')->comment('Descripcion de la Linea');
            $table->string('short_description')->nullable()->comment('Descripcion corta de la Linea');
            $table->boolean('lock_buy')->comment('Bloqueo Compra si/no')->nullable();
            $table->boolean('block_sale')->comment('Bloqueo Venta si/no')->nullable();
            $table->string('margin_cost')->comment('Margen sobre precio venta para establecer el costo (servicios)')->nullable();
            $table->boolean('calculates_sale_price')->comment('calcula precio venta si/no')->nullable();

            //CONTABILIDAD
            $table->integer('inventories_account')->comment('cuenta inventarios')->nullable();
            $table->integer('account_cost')->comment('cuenta_costo')->nullable();
            $table->integer('sales_account')->comment('cuenta_ventas')->nullable();

            // MANEJO DE DATOS (PRODUCTOS)
            $table->integer('barcode')->comment('codigo_barras 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('brand')->comment('marca 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('sub_brand')->comment('submarca 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('line')->comment('linea 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('subline')->comment('sublinea 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('category')->comment('categoria 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('subcategory')->comment('subcategoria 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('lot')->comment('lote 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('serial')->comment('serial 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('type')->comment('tipo 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('Photos')->comment('fotos 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('owner')->comment('propietario 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('maker')->comment('fabricante 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('registration_invima')->comment('registro invima 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('exp_date_invima')->comment('fecha vencimiento invima 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('generic')->comment('generico 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('year')->comment('año 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('conformity_certificate')->comment('certificado conformidad 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('exp_date_certified')->comment('fecha vencimiento certificado 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('weight')->comment('peso 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('volume')->comment('volumen 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('height')->comment('talla 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('color')->comment('color 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('old_product_code')->comment('codigo producto antiguo 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('new_product_code')->comment('codigo producto nuevo 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('commission_portion')->comment('porcion comision 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('mg_of_utility')->comment('mg de utilidad 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('contact_income')->comment('ingreso para terceros 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('reduction_management')->comment('manejo reduccion 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('high_price')->comment('alto costo 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('high_risk')->comment('alto riesgo 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('refrigerated')->comment('refrigerado 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('type_rotation')->comment('tipo rotacion 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('handling_formula')->comment('manejo formula 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('width')->comment('ancho 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('outline')->comment('perfil 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('rin')->comment('rin 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('utqg')->comment('utqg 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('load_index')->comment('indice carga 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('speed_index')->comment('indice velocidad 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('cum_code')->comment('codigo cum 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('sgsss_code')->comment('codigo sgsss 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('application_unit')->comment('unidad de aplicacion 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('number_applications')->comment('numero aplicaciones 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('pharmacy_code')->comment('codigo indicacion farma 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('concentration')->comment('concentracion 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('controlled_sale')->comment('venta controlada 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('belongs_to_pos')->comment('pertenece a pos 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('data_sheet')->comment('Ficha Tecnica 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('data_sheet_link')->comment('Link ficha tecnica 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('video_link')->comment('Link a video 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();

            // CARACTERISTICA COMPRA
            $table->integer('purchase_present_packaging')->comment('compra presente empaque 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->string('purchase_quantity_present')->comment('compra cantidad presente')->nullable();
            $table->integer('local_purchase')->comment('compra local 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('observation_buy')->comment('observacion compra 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();

            // CARACTERISTICA VENTA
            $table->integer('present_sale_packaging')->comment('venta present empaque 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('sale_quantity_packaging')->comment('venta cant present')->nullable();
            $table->integer('observation_of_sale')->comment('observacion de venta 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('channel_point_sale')->comment('canal punto venta 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('channel_amazon')->comment('canal amazon 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('channel_b2c')->comment('canal b2c 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->integer('channel_b2b')->comment('canal b2b 1-SI, 2-NO, 9-OBLIGATORIO')->nullable();
            $table->string('prod_desc_config')->comment('configuracion de descripcion de producto')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Llaves foraneas
            $table->foreign('subscriber_id')->references('id')->on('subscribers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lines');
    }
}
