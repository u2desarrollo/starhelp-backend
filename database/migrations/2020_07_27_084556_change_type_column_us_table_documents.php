<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeColumnUsTableDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->float('discount_value_us')->change();
            $table->float('trm_value')->change();
            $table->float('total_value_us')->change();
            $table->float('total_value_brut_us')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('discount_value_us')->change();
            $table->integer('trm_value')->change();
            $table->integer('total_value_us')->change();
            $table->integer('total_value_brut_us')->change();
        });
    }
}
