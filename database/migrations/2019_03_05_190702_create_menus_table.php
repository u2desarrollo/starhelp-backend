<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('module_id')->unsigned();
            $table->integer('level')->unsigned()->default(1);
            $table->integer('order');
            $table->string('description', 60);
            $table->string('url', 60)->nullable();
            $table->string('icon', 30);
            $table->integer('menu_id')->unsigned()->nullable();
            $table->integer('permission_id')->unsigned()->nullable();

            $table->index('module_id','fk_menu_modules1_idx');
            $table->index('menu_id','fk_menus_menus1_idx');
            $table->index('permission_id','fk_menus_permissions1_idx');

            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('menu_id')->references('id')->on('menus');
            $table->foreign('permission_id')->references('id')->on('permissions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
