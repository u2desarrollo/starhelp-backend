<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsInteractionsDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_interactions_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jobs_interactions_id')->comment('ID de la Interaccion del JOB');
            $table->string('name')->comment('Nombre original del archivo');
            $table->string('type')->comment('Tipo de extension del archivo');
            $table->string('path_document')->comment('Ruta del archivo en el servidor');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('jobs_interactions_id')->references('id')->on('jobs_interactions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_interaction');
    }
}
