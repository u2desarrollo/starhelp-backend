<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->increments('id')->comment('Identificador unico por elemento');
            $table->string('code')->comment('Código del impuesto');
            $table->string('description')->comment('Descripción');
            $table->double('percentage',3,4)->comment('Porcentaje');
            $table->integer('base')->nullable()->comment('Base');
            $table->integer('minimum_base')->nullable()->comment('Minimo como base');
            $table->text('sales_account')->nullable()->comment('Cuenta de ventas');
            $table->text('sales_return_account')->nullable()->comment('Cuenta devolución ventas');
            $table->text('purchase_account')->nullable()->comment('Cuenta de compras');
            $table->text('purchase_return_account')->nullable()->comment('Cuenta devolución de compras');
            $table->boolean('status')->nullable()->comment('Estado');
            $table->boolean('settlement_by_product')->nullable()->comment('liquidacion por producto');
            $table->string('condition')->nullable()->comment('Condición');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
