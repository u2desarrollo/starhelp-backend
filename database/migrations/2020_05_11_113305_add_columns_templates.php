<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->integer('handling_other_parameters_state')->nullable()->default(2);
            $table->string('handling_other_parameters_description')->nullable();
            $table->integer('handling_other_parameters_param_table_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->dropColumn('handling_other_parameters_state');
            $table->dropColumn('handling_other_parameters_description');
            $table->dropColumn('handling_other_parameters_param_table_id');
        });
    }
}
