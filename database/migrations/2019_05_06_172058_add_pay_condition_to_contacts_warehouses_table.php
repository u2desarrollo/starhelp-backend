<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayConditionToContactsWarehousesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->string('pay_condition')->nullable()->comment('Condición de pago');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts_warehouses', function (Blueprint $table) {
			$table->dropColumn('pay_condition');
		});
	}
}