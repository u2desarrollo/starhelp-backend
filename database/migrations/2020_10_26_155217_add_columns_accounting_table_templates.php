<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsAccountingTableTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->boolean('accounting_generate')->nullable()->default(false);
            // $table->integer('accounting_concept')->unsigned()->nullable();
            // $table->integer('accounting_according')->unsigned()->nullable();
            // $table->integer('accounting_account')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->dropColumn('accounting_generate');
            // $table->dropColumn('accounting_concept');
            // $table->dropColumn('accounting_according');
            // $table->dropColumn('accounting_account');
        });
    }
}
