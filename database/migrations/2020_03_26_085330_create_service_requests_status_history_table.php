<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateServiceRequestsStatusHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests_status_history', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('service_requests_id');
            $table->bigInteger('service_requests_state_id');
            $table->bigInteger('user_id');

            $table->text('observation')->nullable();
            $table->timestamp('date')->default(Carbon::now());

            $table->softDeletes();
            $table->timestamps();

            // Relacion
            $table->foreign('service_requests_id')->references('id')->on('service_requests');
            $table->foreign('service_requests_state_id')->references('id')->on('service_requests_state');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests_status_history');
    }
}
