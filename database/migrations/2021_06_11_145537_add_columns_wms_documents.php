<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsWmsDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {

            $table->boolean('update_wms')->comment('Consumo de WMS')->nullable()->default(false);
            $table->timestamp('date_update_wms')->comment('Fecha de Envio WMS')->nullable();
            $table->timestamp('date_update_wms_ultime')->comment('Fecha de Ultima Actualizacion WMS')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('update_wms');
            $table->dropColumn('date_update_wms');
            $table->dropColumn('date_update_wms_ultime');
        });
    }
}
