<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableDocumentsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->bigInteger('physical_unit')->unsigned()->nullable();
            //$table->bigInteger('unit_list_value')->unsigned()->nullable(); //ya existe
            $table->bigInteger('total_value_brut')->unsigned()->nullable();
            $table->bigInteger('total_value')->unsigned()->nullable();
            $table->bigInteger('inventory_cost_value')->unsigned()->nullable();

            //
            $table->bigInteger('stock_after_operation')->unsigned()->nullable();
            $table->bigInteger('stock_value_after_operation')->unsigned()->nullable();
            $table->bigInteger('stock_company_after_operation')->unsigned()->nullable();
            $table->bigInteger('stock_company_value_after_operation')->unsigned()->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('physical_unit');
            $table->dropColumn('unit_list_value');
            $table->dropColumn('total_value_brut');
            $table->dropColumn('total_value');
            $table->dropColumn('inventory_cost_value');

            //
            $table->dropColumn('stock_after_operation');
            $table->dropColumn('stock_value_after_operation');
            $table->dropColumn('stock_company_after_operation');
            $table->dropColumn('stock_company_value_after_operation');
        });
    }
}
