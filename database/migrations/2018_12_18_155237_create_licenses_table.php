<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('id autoincremental');
            $table->string('license_code')->comment('Codigo de licencia de uso')->unique();
            $table->date('start_date')->comment('Fecha de inicio de la licencia');
            $table->date('end_date')->comment('Fecha final de la licencia');
            $table->boolean('trial_license')->comment('Licencia de prueba si/no');
            $table->integer('maximum_users')->comment('Maxima cantidad de usuarios por licencia');
            $table->softDeletes();
            $table->timestamps();
        });

        // DB::statement("ALTER TABLE licenses COMMENT 'Numero de Licencia de Uso' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}
