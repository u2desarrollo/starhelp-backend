<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_others', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('contact_id')->comment('Id del tercero');
            $table->unsignedInteger('category_id')->comment('Id de la categoría, tabla de parámetros');
            $table->string('observations', 255)->nullable()->comment('Observaciones');
            $table->timestamps();

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('category_id')->references('id')->on('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_others');
    }
}
