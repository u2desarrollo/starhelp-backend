<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand_code')->comment('codigo de la marca');
            $table->string('description')->comment('descripción de la marca');
            $table->string('image')->comment('imagen de la marca')->nullable();
            $table->integer('provider_id')->unsigned()->comment('proveedores')->nullable();
            $table->integer('maker_id')->unsigned()->comment('fabricantes')->nullable();
            $table->boolean('lock_buy')->comment('Bloqueo Compra si/no')->nullable();
            $table->boolean('block_sale')->comment('Bloqueo Venta si/no')->nullable();
            $table->string('thumbnails')->comment('miniatura de la imagen')->nullable();
            $table->boolean('deploy_sales_force_app')->nullable()->comment('despliega aplicacion fuerza de venta');
            $table->boolean('deploy_b2c')->nullable()->comment('despliega b2c');
            $table->boolean('deploy_b2b')->nullable()->comment('despliega b2b');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('contacts');
            $table->foreign('maker_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
