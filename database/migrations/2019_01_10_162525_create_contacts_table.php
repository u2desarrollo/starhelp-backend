<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('subscriber_id')->unsigned()->comment('id del suscriptor');
			$table->integer('identification_type')->unsigned()->nullable()->comment('Tipo de identificación');
			$table->string('identification', 20)->nullable()->comment('Número de identificación');
			$table->string('check_digit', 1)->nullable()->comment('Digito de verificación');
			$table->string('name', 150)->comment('Nombre');
			$table->string('surname', 60)->nullable()->comment('Apellido');
			$table->date('birthdate')->nullable()->comment('Fecha de cumpleaños');
			$table->integer('gender')->nullable()->comment('Genero');
			$table->integer('city_id')->unsigned()->comment('LLave foranea de la tabla Cities');
			$table->string('address', 100)->nullable()->comment('Dirección comercial');
			$table->string('stratum', 1)->nullable()->comment('Estrato socioeconomico');
			$table->string('code_ciiu', 45)->nullable()->comment('Código CIIU');
			$table->string('main_telephone', 30)->nullable()->comment('Número de teléfono principal');
			$table->string('secondary_telephone', 30)->nullable()->comment('Número de teléfono secundario');
			$table->string('fax', 30)->nullable()->comment('Fax');
			$table->string('cell_phone', 20)->nullable()->comment('Número de celular');
			$table->string('email', 60)->nullable()->comment('Correo electrónico');
			$table->string('email_electronic_invoice', 60)->nullable()->comment('Correo electrónico para factura electrónica');
			$table->string('web_page', 200)->nullable()->comment('Dirección de la página web');
			$table->string('latitude', 45)->nullable()->comment('Latitud');
			$table->string('length', 45)->nullable()->comment('Longitud');
			$table->boolean('is_customer')->default('0')->comment('Determina si es un cliente');
			$table->boolean('is_provider')->default('0')->comment('Determina si es un proveedor');
			$table->boolean('is_employee')->default('0')->comment('Determina si es un empleado');
			$table->integer('class_person')->unsigned()->comment('LLave foranea de parameters que determina la clase de persona');
			$table->integer('taxpayer_type')->unsigned()->comment('LLave foranea de parameters que determina el tipo de contribuyente');
			$table->integer('tax_regime')->unsigned()->comment('LLave foranea de parameters que determina el regimen contributivo');
			$table->boolean('declarant')->default('0')->comment('Determina si es declarante');
			$table->boolean('self_retainer')->default('0')->comment('Determina si es autoretenedor');
			$table->timestamps();
			$table->softDeletes();
			$table->string('physical_file_number')->nullable()->comment('numero expediente fisico');
			$table->integer('pay_days')->nullable()->comment('dias de pago');
			$table->integer('ERP_code')->nullable()->comment('codigo erp');

			$table->index('city_id','fk_entities_cities1_idx');
			$table->index('subscriber_id','fk_entities_subscribers1_idx');
			$table->index('class_person','fk_entities_parameters3_idx');
			$table->index('taxpayer_type','fk_entities_parameters1_idx');
			$table->index('tax_regime','fk_entities_parameters4_idx');
			$table->index('identification_type','fk_entities_parameters13_idx');

			$table->foreign('city_id')->references('id')->on('cities');
			$table->foreign('subscriber_id')->references('id')->on('subscribers');
			$table->foreign('class_person')->references('id')->on('parameters');
			$table->foreign('taxpayer_type')->references('id')->on('parameters');
			$table->foreign('tax_regime')->references('id')->on('parameters');
			$table->foreign('identification_type')->references('id')->on('parameters');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contacts');
	}
}