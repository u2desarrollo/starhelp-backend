<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jobs_star_id')->comment('ID del JOB');
            $table->unsignedInteger('user_id')->comment('ID del usuario que interactuo en el JOB');
            $table->string('description')->nullable()->comment('Descripcion de la interaccion');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('jobs_star_id')->references('id')->on('jobs_star');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_interactions');
    }
}
