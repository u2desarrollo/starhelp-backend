<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_employees', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned()->comment('Llave foranea que relaciona con la tabla principal de contactos');
            $table->integer('category_id')->unsigned()->comment('Categoría de empleado');
            $table->integer('cost_center_id')->unsigned()->nullable()->comment('Centro de costos');
            $table->string('contract_number', 45)->nullable()->comment('Numero de contrato');
            $table->double('percentage_commision')->nullable()->comment('Porcentaje de comisión');
            $table->integer('position_id')->unsigned()->nullable()->comment('Cargo');
            $table->string('observation', 150)->nullable()->comment('Observación');
            $table->string('photo', 80)->nullable()->comment('Foto');
            $table->timestamps();
            $table->softDeletes();

            $table->index('category_id', 'fk_contacts_employees_parameters1_idx');
            $table->index('cost_center_id', 'fk_contacts_employees_parameters2_idx');
            $table->index('position_id', 'fk_contacts_employees_parameters3_idx');
            $table->index('contact_id', 'fk_contacts_employees_contacts1_idx');

            $table->foreign('category_id')->references('id')->on('parameters');
            $table->foreign('cost_center_id')->references('id')->on('parameters');
            $table->foreign('position_id')->references('id')->on('parameters');
            $table->foreign('contact_id')->references('id')->on('contacts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_employees');
    }
}
