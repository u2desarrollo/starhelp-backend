<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTrmTableDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('trm_value')->unsigned()->nullable();
            $table->integer('total_value_us')->unsigned()->nullable();
            $table->integer('total_value_brut_us')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('trm_value');
            $table->dropColumn('total_value_us');
            $table->dropColumn('total_value_brut_us');
        });
    }
}
