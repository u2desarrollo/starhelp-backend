<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTemplateAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('template_accounting');

        Schema::create('template_accounting', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('model_id')->unsigned()->nullable()->comment('Llave foranea del modelo.');
            $table->integer('concept_id')->unsigned()->nullable()->comment('Llave foranea del concepto de la contabilidad a la tabla de parametros.');
            $table->integer('according_id')->unsigned()->nullable()->comment('Llave foranea del segun de la contabilidad a la tabla de parametros.');
            $table->integer('account_id')->unsigned()->nullable()->comment('Llave foranea de la cuenta de la contabilidad a la tabla de accounts.');
            
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('model_id')->references('id')->on('templates');
            // $table->foreign('concept_id')->references('id')->on('parameters');
            // $table->foreign('according_id')->references('id')->on('parameters');
            // $table->foreign('account_id')->references('id')->on('accounts');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_accounting');
    }
}
