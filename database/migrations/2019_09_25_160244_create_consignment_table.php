<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consignment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->comment('Id banco');
            $table->string('consignment_number')->nullable()->comment('Número consignación');
            $table->string('id_erp')->unsigned()->comment('id del erp  con posibilidad de ser nulo')->nullable();
            $table->string('photo_consignment')->nullable()->comment('Foto consignación');;
            $table->integer('user_id')->unsigned()->comment('Usuario');
            $table->dateTime('date_time')->comment('Feha hora');
            $table->timestamps();
            $table->foreign('bank_id')->references('id')->on('parameters');
            $table->foreign('contact_id')->references('id')->on('contacts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consignment');
    }
}
