<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('line_id')->unsigned()->nullable();
            $table->integer('subline_id')->unsigned()->nullable();

            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('subline_id')->references('id')->on('sublines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('line_id');
            $table->dropColumn('subline_id');
        });
    }
}