<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherstypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucherstypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branchoffice_id')->unsigned()->comment('Llave foranea de branchoffices');
            $table->string('code_voucher_type', 10)->comment('Código del tipo de comprobante');
            $table->string('name_voucher_type', 255)->comment('Nombre del tipo de comprobante');
            $table->string('short_name', 60)->comment('Nombre corto');
            $table->string('prefix', 10)->comment('Prefijo');
            $table->integer('consecutive_number')->comment('Número consecutivo');
            $table->boolean('electronic_bill')->comment('Factura electrónica');
            $table->string('resolution')->comment('Resolución');
            $table->enum('affectation', [1, 2, 3])->comment('Afectación [1:Inventarios, 2:Contabilidad, 3:Inventarios y Contabilidad]');
            $table->boolean('state')->comment('Estado');
            $table->integer('range_from')->nullable()->comment('rango desde');
            $table->integer('range_up')->nullable()->comment('rango hasta');
            $table->timestamps();

            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucherstypes');
    }
}
