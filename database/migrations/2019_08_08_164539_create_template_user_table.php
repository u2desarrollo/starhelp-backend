<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();

			$table->index('template_id', 'fk_template_user_idx');
			$table->index('user_id', 'fk_user_template_idx');

			$table->foreign('template_id')->references('id')->on('templates');
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_user');
    }
}
