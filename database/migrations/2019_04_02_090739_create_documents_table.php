<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('branchoffice_id')->unsigned()->comment('Punto de venta')->nullable();
			$table->integer('vouchertype_id')->unsigned()->comment('Tipo de comprobante');
			$table->integer('consecutive')->comment('Número de consecutivo');
			$table->char('prefix', 5)->comment('Prefijo de facturación');

			$table->integer('warehouse_id')->unsigned()->comment('Bodega')->nullable();
			$table->date('voucher_date')->comment('Fecha de comprobante');
			$table->integer('contact_id')->unsigned()->comment('Código del tercero');
			$table->string('inventory_operation')->comment('Operación de inventario');
			$table->string('model_code')->comment('Código del modelo de captura')->nullable();

			$table->integer('thread')->unsigned()->comment('Hilo conductor')->nullable();
			$table->integer('base_document')->unsigned()->comment('Documento base')->nullable();
			$table->string('erp_id')->comment('Código del ERP')->nullable();

			$table->string('cufe')->comment('Código CUFE DIAN para facturación electrónica')->nullable();
			$table->integer('cufe_state')->unsigned()->nullable();

			$table->integer('document_state')->comment('Estado del documento')->unsigned();
			$table->date('document_last_state')->comment('Fecha del último estado');

			$table->string('erp_state')->comment('Estado del ERP')->nullable();
			$table->string('erp_last_state')->comment('Fecha del último estado del ERP')->nullable();

			$table->integer('city')->unsigned()->comment('Ciudad')->nullable();
			$table->string('zone')->comment('Zona')->nullable();

			$table->string('observation')->comment('Detalle u observación')->nullable();

			$table->char('crossing_prefix', 5)->comment('Prefijo de cruce')->nullable();
			$table->integer('crossing_number_document')->unsigned()->comment('Número del documento de cruce')->nullable();

			$table->date('issue_date')->comment('Fecha de emisión');
			$table->integer('term_days')->comment('Días de plazo')->nullable();
			$table->date('due_date')->comment('Fecha de vencimiento')->nullable();

			$table->double('insurance_value')->comment('Valor de seguros')->nullable();
			$table->double('freight_value')->comment('Valor de fletes')->nullable();
			$table->double('other_value')->comment('Otros valores')->nullable();
			$table->double('discount_value')->comment('Valor de descuento')->nullable();

			$table->boolean('in_progress')->default(0)->comment('Documento en proceso');

			$table->timestamps();
			$table->softDeletes();


			$table->index('branchoffice_id', 'fk_branchoffice_documents_idx');
			$table->index('vouchertype_id', 'fk_vouchertype_documents_idx');
			$table->index('warehouse_id', 'fk_warehouse_documents_idx');
			$table->index('contact_id', 'fk_contact_documents_idx');
			$table->index('thread', 'fk_thread_documents_idx');
			$table->index('base_document', 'fk_base_document_documents_idx');
			$table->index('cufe_state', 'fk_cufe_state_documents_idx');
			$table->index('document_state', 'fk_document_state_documents_idx');
			$table->index('city', 'fk_city_documents_idx');
			$table->index('crossing_number_document', 'fk_crossing_number_document_documents_idx');

			$table->foreign('branchoffice_id')->references('id')->on('branchoffices');
			$table->foreign('vouchertype_id')->references('id')->on('voucherstypes');
			$table->foreign('warehouse_id')->references('id')->on('contacts_warehouses');
			$table->foreign('contact_id')->references('id')->on('contacts');
			$table->foreign('thread')->references('id')->on('documents');
			$table->foreign('base_document')->references('id')->on('documents');
			$table->foreign('cufe_state')->references('id')->on('parameters');
			$table->foreign('document_state')->references('id')->on('parameters');
			$table->foreign('city')->references('id')->on('cities');
			$table->foreign('crossing_number_document')->references('id')->on('documents');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('documents');
	}
}
