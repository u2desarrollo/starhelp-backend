<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('price_list_id')->unsigned()->nullable()->comment('forania a la tabla de parameters haciendo referencia a lista de precios');
            $table->foreign('price_list_id')->references('id')->on('parameters');   // dijo jhon
            $table->boolean('price_taxes_included')->nullable()->comment(' impuestos incluidos SI/NO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
