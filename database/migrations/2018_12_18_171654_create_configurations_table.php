configu<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('subscriber_id')->unsigned()->comment('Id del suscriptor');
            $table->integer('product_digits')->nullable()->comment('Digitos del producto');
            $table->boolean('seller_management')->nullable()->comment('Manejo de Vendedores si/no');
            $table->boolean('brand_management')->nullable()->comment('Manejo de Marcas si/no');
            $table->boolean('line_management')->nullable()->comment('Manejo de Lineas si/no');
            $table->boolean('category_management')->nullable()->comment('Manejo de categorias si/no');
            $table->string('line_description', 45)->nullable()->comment('Descripcion de Linea');
            $table->string('subline_description', 45)->nullable()->comment('Descripcion de sublinea');
            $table->string('category_description', 45)->nullable()->comment('Descripcion de categoria');
            $table->string('subcategory_description', 45)->nullable()->comment('Descripcion de subcategoria');
            $table->string('type_description', 45)->nullable()->comment('Descripcion de tipo');
            $table->boolean('recalculate_sales_price')->nullable()->comment('Recalcular precios de venta');
            $table->double('percentage_minimum_variation', 8,2 )->nullable()->comment('Porcentaje minimo de variacion');
            $table->double('rounding_factor', 8,2 )->nullable()->comment('Factor de Redondeo');
            $table->integer('type_voucher_inventory')->nullable()->comment('Tipo de vales de inventario');
            $table->integer('account_entry')->nullable()->comment('Cuenta de entrada');
            $table->integer('account_exit')->nullable()->comment('Cuenta de salida');
            $table->integer('consecutive_product_code')->nullable()->comment('Codigo consecutivo del producto');
            $table->integer('consecutive_transfer')->nullable()->comment('Codigo consecutivo de transferencia');
            $table->integer('consecutive_sales_orders')->nullable()->comment('Codigo consecutivo de orden de salida');
            $table->integer('consecutive_purchase_orders')->nullable()->comment('Codigo consecutivo de orden de compra');
            $table->integer('folios_electronic_invoice')->nullable()->comment('Folios factura electronica');
            $table->date('last_date_close')->nullable()->comment('Ultimo cierre');
            $table->date('firm_date')->nullable()->comment('fecha_firma');
            $table->integer('base_retention_source_products')->nullable()->comment('Base Retencion Productos');
            $table->double('percentage_retention_source_products', 8,2)->nullable()->comment('Porcentaje retencion productos');
            $table->integer('base_retention_source_services')->nullable()->comment('Base Retencion serivicios');
            $table->double('percentage_retention_source_services', 8,2)->nullable()->comment('porcentaje Retencion servicios');
            $table->double('percentage_reteiva', 8,2)->nullable()->comment('Porcentaje retencion iva');
            $table->string('code_activity_ica')->nullable()->comment('Codigo actividad ica');
            $table->string('code_ciiu')->nullable()->comment('Codigo CIIU');
            $table->double('percentage_reteica', 8,2)->nullable()->comment('Porcentaje reteica');
            $table->integer('bill_to_pay')->nullable()->comment('Cuentas por Pagar');
            $table->integer('receivable')->nullable()->comment('Cuentas por cobrar');
            $table->integer('account_heritage_utility')->nullable()->comment('Utilidad Cuentas');
            $table->integer('account_heritage_lost')->nullable()->comment('Perdidas Cuentas');
            $table->integer('account_pyg_result_exercise')->nullable()->comment('resultado cuenta ejercicio pyg');
            $table->boolean('monthly_utility_transfer_or_lost')->nullable()->comment('Transferencia de utilidades mensual o perdida');
            $table->boolean('annual_utility_transfer_or_lost')->nullable()->comment('Transferencia de utilidades anual o perdida');
            $table->integer('type_voucher_result_exercise')->nullable()->comment('Tipo de documento resultado ejercicio');
            $table->string('first_logo')->nullable()->comment('logo 1');
            $table->timestamps();

            // Relacion
            $table->foreign('subscriber_id')->references('id')->on('subscribers');
        });
       //  DB::statement("ALTER TABLE licenses COMMENT 'Configuracion del suscriptor' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
