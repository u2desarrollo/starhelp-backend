<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOrdenConcept extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_accounting', function (Blueprint $table) {
            $table->integer('order_accounting')->comment('Orden de Modelos Contabilizacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_accounting', function (Blueprint $table) {
            $table->dropColumn('order_accounting');
        });
    }
}
