<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTechnnicalDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_technnical_data', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id')->unsigned();
            $table->string('link_manufacturer_one', 255)->nullable();
            $table->string('link_manufacturer_two', 255)->nullable();
            $table->string('weight', 45)->nullable();
            $table->string('volume', 45)->nullable();
            $table->string('size', 45)->nullable();
            $table->string('color', 45)->nullable();
            $table->string('year_model', 5)->nullable();
            $table->string('series', 10)->nullable();
            $table->string('link_data_sheet', 60)->nullable();
            $table->string('data_sheet', 60)->nullable();
            $table->string('products_technical_datacol', 45)->nullable();
            $table->string('link_video', 80)->nullable();
            $table->boolean('generic')->nullable();
            $table->boolean('used')->nullable();
            $table->string('invima_registry', 45)->nullable();
            $table->date('expiration_date_invima')->nullable();
            $table->string('width', 45)->nullable();
            $table->string('profile', 45)->nullable();
            $table->string('rin', 45)->nullable();
            $table->string('load_index', 45)->nullable();
            $table->string('velocity_index', 45)->nullable();
            $table->string('utqg', 45)->nullable();
            $table->boolean('runflat')->nullable();
            $table->boolean('lt')->nullable();
            $table->string('code_cum', 45)->nullable();
            $table->string('code_secretary_of_health', 45)->nullable();
            $table->string('aplication_unit', 45)->nullable();
            $table->string('number_aplications', 45)->nullable();
            $table->string('pharmacological_indication_code', 10)->nullable();
            $table->string('concentration', 20)->nullable();
            $table->string('pharmaceutical_form', 15)->nullable();
            $table->string('form_unit', 10)->nullable();
            $table->integer('number_of_doses')->nullable();
            $table->string('ium_code', 20)->nullable();

            $table->index('product_id','fk_products_technical_data_products1_idx');

            $table->foreign('product_id')->references('id')->on('products');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_technnical_data');
    }
}
