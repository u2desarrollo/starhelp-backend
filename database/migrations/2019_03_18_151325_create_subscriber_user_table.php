<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriberUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriber_user', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('Llave foranea que contiene el id del usuario');
            $table->integer('subscriber_id')->unsigned()->comment('Llave foranea que contiene el id del suscriptor');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('subscriber_id')->references('id')->on('subscribers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriber_user');
    }
}
