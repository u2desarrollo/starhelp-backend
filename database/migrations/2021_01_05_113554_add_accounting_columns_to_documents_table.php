<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountingColumnsToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->float('accounting_debit_value')->nullable();
            $table->float('accounting_credit_value')->nullable();
            $table->integer('accounting_user_last_modification')->nullable();
            $table->dateTime('accounting_date_last_modification')->nullable();
            $table->integer('center_cost_id')->nullable();
            $table->integer('business_id')->nullable();
            
            $table->foreign('accounting_user_last_modification')->references('id')->on('users');
            $table->foreign('center_cost_id')->references('id')->on('parameters');
            $table->foreign('business_id')->references('id')->on('business');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('accounting_debit_value');
            $table->dropColumn('accounting_credit_value');
            $table->dropColumn('accounting_user_last_modification');
            $table->dropColumn('accounting_date_last_modification');
            $table->dropColumn('center_cost_id');
            $table->dropColumn('business_id');
        });
    }
}
