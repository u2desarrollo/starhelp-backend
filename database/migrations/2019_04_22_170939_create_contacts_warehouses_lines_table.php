<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsWarehousesLinesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts_warehouses_lines', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('contact_warehouse_id')->unsigned();
			$table->integer('line_id')->unsigned();
			$table->timestamps();

			$table->index('contact_warehouse_id', 'fk_contact_warehouse_warehouse_line_idx');
			$table->index('line_id', 'fk_line_warehouse_line_idx');

			$table->foreign('contact_warehouse_id')->references('id')->on('contacts_warehouses');
			$table->foreign('line_id')->references('id')->on('lines');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contacts_warehouses_lines');
	}
}