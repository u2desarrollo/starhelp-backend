<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFixedAssetsIdToDocumentTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->integer('fix_asset_id')->unsigned()->nullable()->comment('activo fijo');
            $table->foreign('fix_asset_id')->references('id')->on('fix_assets');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            //
        });
    }
}
