<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromotionIdToDocumentsProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->integer('promotion_id')->unsigned()->nullable()->comment('Id de la promoción');
			$table->foreign('promotion_id')->references('id')->on('promotions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->dropColumn('promotion_id');
		});
	}
}
