<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToVisitsCustomersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('visits_customers', function (Blueprint $table) {
			$table->boolean('sales_management')->default(false)->comment('Gestión de ventas');
			$table->boolean('collection')->default(false)->comment('Cobranza');
			$table->boolean('training')->default(false)->comment('Capacitación');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('visits_customers', function (Blueprint $table) {
			$table->dropColumn('sales_management');
			$table->dropColumn('collection');
			$table->dropColumn('training');
		});
	}
}