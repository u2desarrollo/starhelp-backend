<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fix_assets', function (Blueprint $table) {
            $table->increments('id');

            // Principal
            $table->integer('consecut')->comment('consecutivo del registro, debe comenzar en 1000');
            $table->string('plate_number')->comment('Numero de placa');
            $table->string('previous_code')->nullable()->comment('Codigo en sistema anterior');
            $table->integer('active_group_id')->nullable()->comment('Tabla Parametro C-015');
            $table->text('description')->comment('Descripcion principal');

            // Datos Adquicision
            $table->text('long_description')->nullable()->comment('Descripcion de la adquicision');
            $table->dateTime('purchase_date')->nullable()->comment('Fecha de compra');
            $table->dateTime('date_regist')->nullable()->comment('Fecha de registro en el sistema');
            $table->string('year_month_start_caus')->nullable()->comment('Año-mes de inicio de Causacion');
            $table->string('year_month_end_caus')->nullable()->comment('Año-mes fin de Causacion');
            $table->string('year_month_last_caus')->nullable()->comment('Año-mes ult de Causacion');
            $table->string('months_to_caus')->nullable()->comment('Meses a causar (Vida util)');
            $table->integer('provider_id')->unsigned()->nullable()->comment('Contacts (proveedores)');
            $table->string('invoice_prefix')->nullable()->comment('Prefijo factura');
            $table->integer('invoice_number')->unsigned()->nullable()->comment('Numero factura');
            $table->integer('invoice_document_id')->unsigned()->nullable()->comment('Documento Factura');

            // Valores Historicos
            $table->float('purchase_value')->unsigned()->nullable()->comment('valor de compra');
            $table->float('iva_value')->unsigned()->nullable()->comment('Valor IVA');
            $table->float('addition_value')->unsigned()->nullable()->comment('Valor adiciones');
            $table->float('historical_cost_value')->unsigned()->nullable()->comment('Valor costo historico');
            $table->float('salvage_value')->unsigned()->nullable()->comment('Valor salvamento');
            $table->float('previous_caus_value')->unsigned()->nullable()->comment('Valor causado (fuera del sistema)');
            $table->float('value_for_causing')->unsigned()->nullable()->comment('Valor por causar');
            $table->float('monthly_deprec_value')->unsigned()->nullable()->comment('Valor depreciacion mes');
            $table->boolean('caus_status')->nullable()->comment('1-Deprecia 0-No deprecia');
            
            // Informacion Cuentas
            $table->integer('record_account_id')->unsigned()->nullable()->comment('Cuenta registro');
            $table->integer('balance_account_id')->unsigned()->nullable()->comment('Cuenta balance');
            $table->integer('result_account_id')->unsigned()->nullable()->comment('Cuenta resultado');
            
            // Informacion General
            $table->string('brand')->nullable()->comment('Marca');
            $table->string('model')->nullable()->comment('Modelo');
            $table->string('serial_number')->nullable()->comment('No Serie');
            $table->string('engine_number')->nullable()->comment('No Motor');
            $table->text('deeds')->nullable()->comment('Escrituras');
            $table->string('quantity')->nullable()->comment('Cantidad');
            $table->integer('class_id')->unsigned()->nullable()->comment('Tabla Parametro C-016');

            // Ubicacion
            $table->integer('city_id')->unsigned()->nullable()->comment('Ciudades');
            $table->integer('branchoffice_id')->unsigned()->nullable()->comment('Sedes');
            $table->string('address')->unsigned()->nullable()->comment('Direccion');
            $table->string('location')->unsigned()->nullable()->comment('Lugar');
            $table->integer('cost_center_id')->unsigned()->nullable()->comment('Tabla Parametro 038');
            $table->integer('contact_responsible_id')->unsigned()->nullable()->comment('Contacto responsable');
            $table->dateTime('delivery_date')->nullable()->comment('fecha_entrega');
            $table->string('delivery_doc')->unsigned()->nullable()->comment('Acta de entrega');
            
            // Datos dado de baja
            $table->dateTime('derecognized_date')->nullable()->comment('Fecha dado de baja');
            $table->integer('contact_sale_id')->unsigned()->nullable()->comment('Contacto Venta');
            $table->float('value_derecognized')->unsigned()->nullable()->comment('Valor dado de baja');
            $table->integer('derecognized_type_id')->unsigned()->nullable()->comment('Tipo dado de baja');
            $table->text('derecognized_observation')->unsigned()->nullable()->comment('Observacion dado de baja');
            $table->integer('derecognized_document_id')->unsigned()->nullable()->comment('Documento dado de baja');

            // Mantenimiento
            $table->integer('maintenance_provider_id')->unsigned()->nullable()->comment('Contacts / Proveedores');
            $table->text('contract_description')->unsigned()->nullable()->comment('Descripcion del contrato');
            $table->dateTime('contract_expiration_date')->nullable()->comment('Fecha vencimiento contrato');
            $table->dateTime('last_maintenance_date')->nullable()->comment('Fecha ultimo mantenimiento');
            $table->text('spare_parts')->unsigned()->nullable()->comment('Insumos Repuestos');
            $table->text('maintenance_notes')->unsigned()->nullable()->comment('Notas Mantenimiento');
            
            // Seguro
            $table->string('policy_number')->nullable()->comment('No poliza');
            $table->integer('policy_provider_id')->unsigned()->nullable()->comment('Proveedor Poliza');
            $table->dateTime('policy_expiration_date')->nullable()->comment('Fecha venc Poliza');
            $table->text('coverages')->unsigned()->nullable()->comment('Amparos');
            $table->float('premium_value')->unsigned()->nullable()->comment('Valor prima');
            $table->float('insured_value')->unsigned()->nullable()->comment('Valor Asegurado');
            $table->text('policy_observation')->nullable()->comment('Observacion Poliza');
            
            // Avaluos
            $table->dateTime('date_last_commercial_appraisal')->nullable()->comment('Fecha ultimo avaluo comercial');
            $table->float('value_last_commercial_appraisal')->unsigned()->nullable()->comment('Valor ultimo avaluo comercial');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('active_group_id')->references('id')->on('parameters');
            $table->foreign('provider_id')->references('id')->on('contacts');
            $table->foreign('invoice_document_id')->references('id')->on('documents');
            $table->foreign('record_account_id')->references('id')->on('accounts');
            $table->foreign('balance_account_id')->references('id')->on('accounts');
            $table->foreign('result_account_id')->references('id')->on('accounts');
            $table->foreign('class_id')->references('id')->on('parameters');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('cost_center_id')->references('id')->on('parameters');
            $table->foreign('contact_responsible_id')->references('id')->on('contacts');
            $table->foreign('contact_sale_id')->references('id')->on('contacts');
            $table->foreign('derecognized_type_id')->references('id')->on('parameters');
            $table->foreign('derecognized_document_id')->references('id')->on('documents');
            $table->foreign('maintenance_provider_id')->references('id')->on('contacts');
            $table->foreign('policy_provider_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fix_assets');
    }
}
