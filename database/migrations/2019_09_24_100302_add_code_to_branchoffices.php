<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToBranchoffices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branchoffices', function (Blueprint $table) {
 //   lo que hago es reescribir los campos de la tabla recuerda que ya fue creadamente anteriormente
            $table->string("code")->nullable()->comment('codigo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branchoffices', function (Blueprint $table) {
            //  cuando haga rollback me elimina la columna anteriormente creada
            $table->dropColumn("code");

        });
    }
}
