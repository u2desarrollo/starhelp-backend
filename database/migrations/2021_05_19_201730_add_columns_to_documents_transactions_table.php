<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToDocumentsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->integer('transaction_reference_id')->unsigned()->nullable();
            $table->integer('optional_sequence')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_transactions', function (Blueprint $table) {
            $table->dropColumn('transaction_reference_id');
            $table->dropColumn('optional_sequence');
        });
    }
}
