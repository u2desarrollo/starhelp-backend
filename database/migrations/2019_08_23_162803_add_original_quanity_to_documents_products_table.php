<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginalQuanityToDocumentsProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->integer('original_quantity')->default(0)->comment('Cantidad original');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents_products', function (Blueprint $table) {
			$table->dropColumn('original_quantity');
		});
	}
}