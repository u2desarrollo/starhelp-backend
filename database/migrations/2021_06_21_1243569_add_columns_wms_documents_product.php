<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsWmsDocumentsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_products', function (Blueprint $table) {

            $table->integer('quantity_wms')->comment('Cantidad Enviada por WMS @hom669')->nullable()->default(false);
            $table->text('lot_wms')->comment('Numero de Lote o Lotes enviados por WMS y guardado como json')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_products', function (Blueprint $table) {
            $table->dropColumn('quantity_wms');
            $table->dropColumn('lot_wms');
        });
    }
}
