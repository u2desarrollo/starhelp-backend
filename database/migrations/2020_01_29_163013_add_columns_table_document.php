<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            
            # ret_fue
            $table->bigInteger('ret_fue_prod_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_fue_prod_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_fue_prod_total')->unsigned()->nullable();

            $table->bigInteger('ret_fue_serv_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_fue_serv_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_fue_serv_total')->unsigned()->nullable();

            $table->bigInteger('ret_fue_total')->unsigned()->nullable();


            # ret_iva
            $table->bigInteger('ret_iva_prod_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_iva_prod_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_iva_prod_total')->unsigned()->nullable();

            $table->bigInteger('ret_iva_serv_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_iva_serv_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_iva_serv_total')->unsigned()->nullable();

            $table->bigInteger('ret_iva_total')->unsigned()->nullable();


            # ret_ica
            $table->bigInteger('ret_ica_prod_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_ica_prod_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_ica_prod_total')->unsigned()->nullable();

            $table->bigInteger('ret_ica_serv_vr_limit')->unsigned()->nullable();
            $table->bigInteger('ret_ica_serv_percentage')->unsigned()->nullable();
            $table->bigInteger('ret_ica_serv_total')->unsigned()->nullable();

            $table->bigInteger('ret_ica_total')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            
            # ret_fue
            $table->dropColumn('ret_fue_prod_vr_limit');
            $table->dropColumn('ret_fue_prod_percentage');
            $table->dropColumn('ret_fue_prod_total');

            $table->dropColumn('ret_fue_serv_vr_limit');
            $table->dropColumn('ret_fue_serv_percentage');
            $table->dropColumn('ret_fue_serv_total');

            $table->dropColumn('ret_fue_total');


            # ret_iva
            $table->dropColumn('ret_iva_prod_vr_limit');
            $table->dropColumn('ret_iva_prod_percentage');
            $table->dropColumn('ret_iva_prod_total');

            $table->dropColumn('ret_iva_serv_vr_limit');
            $table->dropColumn('ret_iva_serv_percentage');
            $table->dropColumn('ret_iva_serv_total');

            $table->dropColumn('ret_iva_total');


            # ret_ica
            $table->dropColumn('ret_ica_prod_vr_limit');
            $table->dropColumn('ret_ica_prod_percentage');
            $table->dropColumn('ret_ica_prod_total');

            $table->dropColumn('ret_ica_serv_vr_limit');
            $table->dropColumn('ret_ica_serv_percentage');
            $table->dropColumn('ret_ica_serv_total');

            $table->dropColumn('ret_ica_total');


        });
    }
}
