<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prices', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->integer('price_list_id')->unsigned()->comment('Lista de precios');
			$table->integer('branchoffice_id')->unsigned()->nullable();
			$table->double('price');
			$table->double('previous_price')->comment('Precio de venta anterior')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->index('product_id', 'fk_product_id_prices_lists_idx');
			$table->index('price_list_id', 'fk_parameter_id_prices_lists_idx');
			$table->index('branchoffice_id', 'fk_branchoffice_prices_lists_idx');

			$table->foreign('product_id')->references('id')->on('products');
			$table->foreign('price_list_id')->references('id')->on('parameters');
			$table->foreign('branchoffice_id')->references('id')->on('branchoffices');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('prices');
	}
}
