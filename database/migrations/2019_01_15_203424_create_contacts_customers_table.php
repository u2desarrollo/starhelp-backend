<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_customers', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned()->comment('Llave foranea que relaciona con la tabla principal de contactos');
            $table->integer('category_id')->unsigned()->comment('Categoría del cliente');
            $table->integer('seller_id_1')->nullable()->comment('Código del vendedor 1');
            $table->integer('seller_id_2')->nullable()->comment('Código del vendedor 2');
            $table->integer('callcenter advisor_id')->nullable()->comment('Asesor callcenter');
            $table->integer('point_of_sale_id')->nullable()->comment('Punto de venta');
            $table->boolean('electronic_invoice_shipping')->nullable()->default('0')->comment('Envío de factura electrónica');
            $table->boolean('b2b_portal_access')->nullable()->default('0')->comment('Acceso a portal B2B');
            $table->integer('priority')->nullable()->comment('Prioridad');
            $table->integer('price_list_id')->nullable()->comment('Lista de precios');
            $table->integer('zone_id')->unsigned()->nullable()->comment('Zona');
            $table->double('percentage_discount')->nullable()->default('0')->comment('Porcentaje de descuento');
            $table->integer('credit_quota')->nullable()->default('0')->comment('Cupo de credito');
            $table->date('expiration_date_credit_quota')->nullable()->comment('Fecha de vencimiento del credito');
            $table->integer('days_soon_payment_1')->nullable()->comment('Días de pronto pago 1');
            $table->double('percentage_soon_payment_1')->nullable()->comment('Porcentaje de pronto pago 1');
            $table->integer('days_soon_payment_2')->nullable()->comment('Días de pronto pago 2');
            $table->double('percentage_soon_payment_2')->nullable()->comment('Porcentaje de pronto pago 2');
            $table->integer('days_soon_payment_3')->nullable()->comment('Días de pronto pago 3');
            $table->double('percentage_soon_payment_3')->nullable()->comment('Porcentaje de pronto pago 3');
            $table->date('last_purchase')->nullable()->comment('Ultima fecha de compra de cliente o proveedor');
            $table->boolean('blocked')->default('0')->comment('Determina si el cliente o proveedor esta bloqueado');
            $table->string('observation', 255)->nullable()->comment('Observaciones del cliente');
		    $table->string('logo', 255)->nullable()->comment('Imagen del cliente');
		    $table->boolean('retention_source')->nullable()->comment('Determina si el cliente hace retención en la fuente');
		    $table->boolean('retention_iva')->nullable()->comment('Determina si se le factura con IVA al cliente');
		    $table->boolean('retention_ica')->nullable()->comment('Determina si se le factura con ICA al cliente');
		    $table->date('date_financial_statement')->nullable()->comment('Fecha de estados financieros');
		    $table->integer('assets')->nullable()->comment('Activos');
		    $table->integer('liabilities')->nullable()->comment('Pasivos');
		    $table->integer('heritage')->nullable()->comment('Patrimonio');
            $table->integer('code_ica_id')->unsigned()->nullable();
            $table->integer('aut_rep_cent_riesgo')->unsigned()->nullable()->comment('autoriza reporte central de riesgo');
		    $table->timestamps();
            $table->softDeletes();
            

		    $table->index('category_id', 'fk_contacts_customers_parameters1_idx');
		    $table->index('contact_id', 'fk_contacts_customers_contacts1_idx');
		    $table->index('zone_id', 'fk_contacts_customers_parameters2_idx');
		    $table->index('code_ica_id', 'fk_contacts_customers_parameters3_idx');

		    $table->foreign('category_id')->references('id')->on('parameters');
		    $table->foreign('contact_id')->references('id')->on('contacts');
		    $table->foreign('zone_id')->references('id')->on('parameters');
		    $table->foreign('code_ica_id')->references('id')->on('parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_customers');
    }
}
