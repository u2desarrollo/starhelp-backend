<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('branchoffice_id')->unsigned()->nullable();
            $table->integer('contact_id')->unsigned()->nullable();
            $table->double('price');
            $table->double('previous_price')->comment('Precio de venta anterior')->nullable();
            $table->integer('user_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->index('product_id', 'fk_product_id_price_contacts_idx');
            $table->index('branchoffice_id', 'fk_branchoffice_price_contacts_idx');
            $table->index('contact_id', 'fk_contact_id_price_contacts_idx');
            $table->index('user_id','fk_user_id_price_contacts_idx');

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('branchoffice_id')->references('id')->on('branchoffices');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_contacts');
    }
}
