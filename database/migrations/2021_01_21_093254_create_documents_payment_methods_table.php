<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('document_id')->unsigned()->nullable()->comment('Id del documento');
            $table->integer('payment_method_id')->unsigned()->nullable()->comment('Id tabla de parametros');
            $table->string('authorization_number')->nullable()->comment('Numero de autorizacion');
            $table->string('check_number')->nullable()->comment('Numero de cheque');
            $table->string('entity')->nullable()->comment('Entidad');
            $table->float('value')->unsigned()->nullable()->comment('Valor del metodo de pago');

            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('document_id')->references('id')->on('documents');
            $table->foreign('payment_method_id')->references('id')->on('parameters');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_payment_methods');
    }
}
