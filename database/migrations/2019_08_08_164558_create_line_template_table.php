<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_template', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->unsigned();
			$table->integer('line_id')->unsigned();
			$table->timestamps();

			$table->index('template_id', 'fk_template_line_idx');
			$table->index('line_id', 'fk_line_template_idx');

			$table->foreign('template_id')->references('id')->on('templates');
			$table->foreign('line_id')->references('id')->on('lines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_template');
    }
}
