<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_documents', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->integer('document_type_id')->unsigned();
            $table->string('path', 60);
            $table->string('name', 60);
            $table->string('observation', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('contact_id', 'fk_contacts_documents_contacts1_idx');
            $table->index('document_type_id', 'fk_contacts_documents_parameters1_idx');

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('document_type_id')->references('id')->on('parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_documents');
    }
}
