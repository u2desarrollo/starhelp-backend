<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DataBaseBackupNotification extends Notification
{
    use Queueable;

    protected $fileName;

    /**
     * Create a new notification instance.
     *
     * @param $fileName
     */
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('https://s3.console.aws.amazon.com/s3/buckets/s3-backup-databases/dbcolsaisa/?region=sa-east-1&tab=overview');
        return (new MailMessage)
            ->greeting('Hola')
            ->subject('Backup Base de datos Colsaisa')
            ->line('El backup de la base de datos de Colsaisa se ha generado exitosamente con nombre ' . $this->fileName)
            ->action('Ir', url($url))
            ->line("Puede verificar su existencia en el servicio S3 de Amazon Web Services");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
