<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserInvitation extends Notification implements ShouldQueue
{
    use Queueable;

    protected $password;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/#/login');
        return (new MailMessage)
            ->greeting('Hola')
            ->subject('Invitación Propartes B2B')
            ->line('Esta es una invitación para que ingrese al portal B2B de Propartes')
            ->action('Aceptar invitación', url($url))
            ->line("Se le ha asignado la siguiente contraseña y deberá cambiarla en el primer inicio de sesión: \n". $this->password);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
