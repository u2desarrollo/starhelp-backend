<?php

namespace App\Repositories;

use App\Entities\Aurora;
use App\Entities\Parameter;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentProduct;
use App\Entities\Document;
use App\Entities\DocumentTransactions;

/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class AuroraRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Aurora::class;
    }

    public function findAll($request){
        $process = Aurora::orderBy('id')->with(['option_'])->get();

        foreach ($process as $key => $value) {
            $hour = explode(':', $value->hour);
            $value->Hour_input= $hour[0];
            $value->minutes=$hour[1];
            
            $value->options_select= Parameter::select('id', 'paramtable_id', 'code_parameter', 'name_parameter')
            ->where('paramtable_id', 108)
            ->where('code_parameter', 'ilike', $value->type == 1 ? 'P%' : 'A%')
            ->get();
            $value->email=json_decode($value->email);
            $value->manage_hour= $value->frequency == 1 || $value->frequency == 2 ? false : true;
            $value->manage_days= $value->frequency == 11 ? true : false;
            
        }
    	return $process;

    }
   
}
