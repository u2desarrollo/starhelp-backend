<?php

namespace App\Repositories;

use App\Entities\Document;
use App\Entities\PaymentWay;
use App\Entities\User;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentWayRepository
 * @package App\Repositories
 * @version June 10, 2019, 2:58 pm UTC
 *
 * @method PaymentWay findWithoutFail($id, $columns = ['*'])
 * @method PaymentWay find($id, $columns = ['*'])
 * @method PaymentWay first($columns = ['*'])
*/
class PaymentWayRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'document_id',
        'pay_way_id',
        'value',
        'bank',
        'bonus_number',
        'user_id'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return PaymentWay::class;
	}

	public function create(array $payment_ways) {
      // este metodo puede presentar problemas por
      // que no se sabe si llegue un array o un object estar pendiente
        $created = [];
        $user=User::where('contact_id',$payment_ways['iduser'])->first();
        foreach ($payment_ways['formasPago'] as $payment) {
   if($payment->tipo=='Efectivo'){
    $payWay=849;
   }elseif($payment->tipo=='Cheque'){
    $payWay=850;
   }
// $consecutive=Document::selectRaw('max(consecutive)')->where('voucher_type',7)->get();
// $consecutive+=1;
//   $newDocument=Document::create([
//       'vouchertype_id'=>7,
//       'consecutive'=>$consecutive,
//       'prefix'=>''
//       'voucher_date'=>''
//       'contact_id'=>''
//       'inventory_operation'=>''
//       'document_state'=>''
//       'document_last_state'=>''
//       'issue_date'=>''
//   ]);


            $create = PaymentWay::create([
                'document_id' => (int)$payment->id,
                'pay_way_id' =>$payWay,
                'value' => (int)$payment->valor,
                'bank' => $payment->banco,
                'bonus_number' => (int)$payment->numeroCheque,
                'user_id' => $user->id
            ]);


            //empujar al array el valor del nuevo id
            array_push($created, ['id' => $create->id]);

        }

        return $created;
	}
}
