<?php

namespace App\Repositories;

use App\Entities\BranchOffice;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BranchOfficeRepository
 * @package App\Repositories
 * @version December 27, 2018, 9:50 pm UTC
 *
 * @method BranchOffice findWithoutFail($id, $columns = ['*'])
 * @method BranchOffice find($id, $columns = ['*'])
 * @method BranchOffice first($columns = ['*'])
 */
class BranchOfficeRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'id',
		'subscriber_id',
		'name',
		'address',
		'telephone',
		'fax',
		'main',
		'city_id',
		'latitude',
		'length',
		'branchoffice_type',
		'city'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return BranchOffice::class;
	}

	/**
	 * Método para obtener el listado de vouchersTypes de acuerdo a los parámetros recibidos
	 * @param $request contiene los parámetros para filtrar
	 * @return $vourchesType contiene una collección de de vouchersTypes
	 */
	public function findAll($request)
	{
		if ($request->paginate == 'true') {
			$branchOffice = BranchOffice::whereHas('city', function ($query) use ($request) {
				$query->where('city', 'ILIKE', '%' . $request->filter . '%');
			})
			->orWhere(function ($query) use ($request) {
				if (is_numeric($request->filter)) {
					$query->where('id', '=', $request->filter);
				} else {
					return false;
				}
			})
			->orwhere('name', 'ILIKE', '%' . $request->filter . '%')
			->orWhere('address', 'ILIKE', '%' . $request->filter . '%')
			->with(['warehouses', 'city'])
			->orderBy($request->sortBy, $request->sort)
			->paginate($request->perPage);
		} else {
			if (isset($request['subscriber_id'])) {
				$branchOffice = BranchOffice::where('subscriber_id', $request['subscriber_id'])->get();
			} else {
				$branchOffice = BranchOffice::with([
					'warehouses' => function ($query) {
						$query->where('warehouse_code', '!=', 'TR');
					}, 
					'city'
				])->get();
			}
		}
		return $branchOffice;
	}

	public function findForId($id) {
		$branchOffice = BranchOffice::where('id', $id)->with(['warehouses', 'city'])->first();
		return $branchOffice;
	}

	public function getBranchs() {
		$branchs= BranchOffice::select("id","name")->orderByRaw('id ASC')->get();
		return $branchs;
	}
}