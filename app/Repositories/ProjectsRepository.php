<?php

namespace App\Repositories;

use App\Entities\Account;
use App\Entities\Parameter;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\Projects;
use App\Entities\Document;
use App\Entities\DocumentTransactions;

class ProjectsRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Projects::class;
    }

    public function findAll($request)
    {
        if ($request->paginate == 'true') {
	        $accounts = Projects::
                with([
                    'type',
                    'contact'
                ])
                ->where(function($query) use ($request){
                    $query->where('description', 'ILIKE', '%' . $request->filter . '%');
                    // $query->orWhere('name', 'ILIKE', '%' . $request->filter . '%');
                })
	            ->paginate($request->perPage);
    	}else{
            $accounts = Projects::with([
                'type',
                'contact'
            ])->get();
    	}

    	return $accounts;
    }

    public function activeProjects($request)
    {
        $accounts = Projects::with([
            'type',
            'contact'
        ])->where('status', 1)
        ->get();
    	return $accounts;
    }

    public function findById($id)
    {
        $Projects = Projects::with([
            'type',
            'contact'
        ])->find($id);

    	return $Projects;
    }

    public function update($id, $request)
    {
        Projects::find($id)->update($request);
        return Projects::find($id);
    }

}
