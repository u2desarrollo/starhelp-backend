<?php

namespace App\Repositories;

use App\Entities\TypesOperation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypesOperationRepository
 * @package App\Repositories
 * @version August 5, 2019, 3:03 pm UTC
 *
 * @method TypesOperation findWithoutFail($id, $columns = ['*'])
 * @method TypesOperation find($id, $columns = ['*'])
 * @method TypesOperation first($columns = ['*'])
 */
class TypesOperationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'description',
        'oper_inv',
        'affectation',
        'inventory_cost',
        'iva_operation',
        'control_existence_outputs',
        'sales_price_control',
        'control_price_list'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypesOperation::class;
    }
    public function findAll($request)
    {
        $paginate = $request->paginate == 'true' ? true : false;

        if ($paginate) {
            $operations = TypesOperation::with(['parameter'])
                ->where('code', 'ILIKE', '%' . $request->filter . '%')
                ->orwhere('description', 'ILIKE', '%' . $request->filter . '%')
                ->orderBy('code')
                ->paginate($request->perPage);
        } else {
            $operations = TypesOperation::orderBy('code')->get();
        }

        return $operations;
    }

    public function findById($id)
    {
        $typesOperation = TypesOperation::findOrFail($id);
        return $typesOperation;
    }
}
