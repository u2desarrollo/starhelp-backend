<?php

namespace App\Repositories;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\City;
use App\Entities\User;
use App\Entities\ContactVehicle;
use App\Entities\ContactWarehouse;
use App\Traits\CollectionsTrait;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Excel;
use PhpParser\Node\Stmt\Foreach_;

/**
 * Class ContactRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:25 pm UTC
 *
 * @method Contact findWithoutFail($id, $columns = ['*'])
 * @method Contact find($id, $columns = ['*'])
 * @method Contact first($columns = ['*'])
 */
class ContactRepository extends BaseRepository
{
    use CollectionsTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subscribers_id',
        'identification_type',
        'identification',
        'check_digit',
        'name',
        'surname',
        'birthdate',
        'gender',
        'city_id',
        'address',
        'stratum',
        'code_ciiu',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'web_page',
        'latitude',
        'length',
        'is_customer',
        'is_provider',
        'is_employee',
        'class_person',
        'taxpayer_type',
        'tax_regime',
        'declarant',
        'self_retainer'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contact::class;
    }


    /**
     * Method to obtain the list of contacts according to the parameters received
     * @param $request Contains the parameters to filter
     * @return  $contacts a collection of contacts
     */
    public function findAll($request)
    {
        $input = $request->all();

        $paginate = $request->paginate == 'true' ? true : false;
        $query = isset($input['query']) ? $input['query'] : null;
        $is_customer = $request->is_customer == "true" ? true : false;
        $is_provider = $request->is_provider == "true" ? true : false;
        $is_employee = $request->is_employee == "true" ? true : false;

        $filter_customer = $request->filter_customer;
        $filter_provider = $request->filter_provider;
        $filter_employee = $request->filter_employee;

        $type = $request->type;
        $filter = $request->filter;
        $subscribers = $request->subscribers;

        $typeuser = $request->typeuser;

        $user = Auth::user();

        if ($user->hasRole('Vendedor')) {
            $seller_id = $user->contact_id;
        }

        $contacts = Contact::select('id', 'identification_type', 'identification', 'name', 'surname', 'address', 'main_telephone', 'cell_phone', 'is_customer', 'is_provider', 'is_employee', 'is_other', 'subscriber_id', 'direct_invoice_blocking')
            ->with([
                'provider:id,contact_id',
                'customer:id,contact_id,category_id',
                'employee:id,contact_id',
                'city',
                'subscriber',
                'warehouses:id,contact_id,description,seller_id',
                'warehouses.seller:id,identification,name,surname'
            ]);

        if ($request->has('query')) {
            $contacts->whereRaw('concat(identification, \' \', name, \' \', surname, \' \', email) ilike ?', ['%' . $query . '%']);
        }

        if ($is_customer) {
            $contacts->where('is_customer', 1);
            if ($filter_customer && is_array($filter_customer)) {
                $contacts->whereHas('customer', function ($query) use ($filter_customer) {
                    return $query->whereIn('category_id', $filter_customer);
                });
            }
        }

        if ($is_provider) {
            $contacts->where('is_provider', 1);

            if ($filter_provider && is_array($filter_provider)) {
                $contacts->whereHas('customer', function ($query) use ($filter_provider) {
                    return $query->whereIn('category_id', $filter_provider);
                });
            }
        }

        if ($is_employee) {
            $contacts->where('is_employee', 1);

            if ($filter_employee && is_array($filter_employee)) {
                $contacts->whereHas('customer', function ($query) use ($filter_employee) {
                    return $query->whereIn('category_id', $filter_employee);
                });
            }
        }

        if (isset($seller_id)){
            $contacts->whereHas('warehouses', function($warehouses) use ($seller_id) {
                $warehouses->where('seller_id', $seller_id);
            });
        }

        // Trae imformacion
        if ($paginate) {
            return  $contacts = $contacts->paginate($request->perPage);
        }

        // Cliente prospecto
        if ($is_customer) {
            $contacts->whereHas('customer', function ($query) {
                $query->where('category_id', '!=', '24473');
                $query->orWhere('category_id', null);
            });
        }

        if ($type) {
            $contacts->where($type, 'true');
            $contacts->where(function ($query) use ($filter) {
                $query->where('name', 'ILIKE', '%' . $filter . '%')
                    ->orWhere('surname', 'ILIKE', '%' . $filter . '%')
                    ->orWhere('identification', 'ILIKE', '%' . $filter . '%');
            });

            // $contacts = $contacts->get();
        }

        if ($typeuser) {
            $contacts->where($typeuser, 'true');
            $contacts->where(function ($q) use ($query) {
                return $q->where('name', 'ILIKE', '%' . $query . '%')
                    ->orWhere('surname', 'ILIKE', '%' . $query . '%');
            });
            //  $contacts = $contacts->get();
        }

        if ($subscribers) {
            $contacts->whereIn('subscriber_id', $subscribers);
            //$contacts = $contacts->get();

        }

        return $contacts->get();
    }

    public function getContactForType($request)
    {
        return Contact::with([
            'subscriber',
            'warehouses',
            'clerks'
        ])
            ->where(function ($query) use ($request) {
                return $query->where('name', 'ILIKE', '%' . $request->get('query') . '%')
                    ->orWhere('surname', 'ILIKE', '%' . $request->get('query') . '%')
                    ->orWhere('identification', 'ILIKE', '%' . $request->get('query') . '%');
            })
            ->when($request->id != '', function ($query) use ($request) {
                $query->where('id', $request->id);
            })
            ->withTrashed()
            ->get();
    }


    public function findForId($id)
    {
        try {
            $contact = Contact::with([
                'provider',
                'customer',
                'employee',
                'contactOther',
                'city',
                'subscriber',
                'clerks',
                'warehouses' => function ($query) {
                    $query->with([
                        'seller',
                        'contact',
                        'retFueAccount',
                        'retIvaAccount',
                        'retIcaAccount',
                        'retFueProviderAccount',
                        'retIvaProviderAccount',
                        'retIcaProviderAccount',
                        'retFueServAccount',
                        'retIcaServAccount',
                        'retIvaServAccount',
                        'retFueServProviderAccount',
                        'retIcaServProviderAccount',
                        'retIvaServProviderAccount'
                    ]);
                },
                'identificationt',
                'classPersonp',
                'taxPayerTypep',
                'taxRegimep'
            ])->withTrashed()->find($id);
            // $contact = Contact::where('id', $id)->with(['provider', 'customer', 'employee', 'city', 'subscriber', 'clerks', 'warehouses', 'identificationt', 'classPersonp', 'taxPayerTypep', 'taxRegimep'])->withTrashed()->first();
            $contact->lines = $contact->lines()->pluck('lines.id')->toArray();

            return $contact;
        } catch (\Exception  $e) {
            Log::error($e->getMessage());
        }
    }

    public function findCustomersByType($request)
    {
        $user = $request->id;
        $type = $request->employee_type;

        $customers = Contact::whereHas('customer', function ($query) use ($user, $type) {
            if ($type == 266) {
                // Traer solo los clientes asignados
                $query->where('seller_id_1', $user);
            } elseif ($type == 267 || $type == 268) {
                // Traer solo los clientes de la empresa en la que trabaja el usuario logueado
                return true;
            }
        })
            ->where('identification_type', 1)
            ->with(['customer', 'lines'])
            ->get();

        // SELECT * FROM contacts
        // INNER JOIN contacts_customers cc ON cc.contact_id = contacts.id
        // WHERE cc.seller_id = $user AND contacts.id IN (SELECT * FROM contacts_employees WHERE category_id = $type);

        return $customers;
    }

    public function findCustomersBySellerId($seller)
    {
        $superAdmin=true;
        $user=User::where('contact_id',$seller)->with('getRoles')->first();
        if($user){
            $superAdmin=$user->getRoles[0]->role=='superadmin'?true:false;

        }

        // este es el de propartes
        $warehouses = Contact::select(
            'contacts.name AS tercer_nom_ter',
            'contacts.tradename',
            'contacts.identification',
            'cw.address AS clibod_direccion',
            'cw.telephone AS clibod_telefono',
            'cw.branchoffice_id',
            'cw.contact_id',
            'cw.credit_quota',
            'cities.city'
        )
            ->selectRaw("CAST(contacts.id AS varchar) AS tercer_cod_ter")
            ->selectRaw("CAST(cw.id AS varchar) AS clibod_cod_bodega")
            ->selectRaw("CONCAT(contacts.identification,' - ',cw.code,' - ', cw.description ) AS clibod_descripcion")
            //->selectRaw("COALESCE(cw.latitude, '0') AS clibod_latitud")
            //->selectRaw("CASE WHEN cw.latitude = '-' THEN '0' WHEN cw.latitude is null THEN '0' ELSE cw.latitude END AS clibod_latitud")
            ->selectRaw("CASE WHEN cw.latitude = '-' OR cw.latitude is null THEN '0' ELSE cw.latitude END AS clibod_latitud")
            //->selectRaw("COALESCE(cw.length, '0') AS clibod_longitud")
            ->selectRaw("CASE WHEN cw.length = '-' OR cw.length is null THEN '0' ELSE cw.length END AS clibod_longitud")
            ->join('contacts_warehouses AS cw', 'cw.contact_id', '=', 'contacts.id')

            //    posibilidad de quitar
            ->when($superAdmin==false, function($query) use ($seller){
                $query->where('cw.seller_id', $seller);
            })

            ->leftJoin('cities', 'cw.city_id', '=', 'cities.id')
            ->whereNull('cw.deleted_at')
            ->orderby('contacts.name', 'ASC')->get();

        foreach ($warehouses as $key => $value) {
            $part = explode('/', $value->tradename);
            $cityDirection = $value->city != '' && $value->city != null ? $value->city : '';
            if ($value->clibod_direccion != '' && $value->clibod_direccion != null) {
                $cityDirection .= $cityDirection != null ? '-' . $value->clibod_direccion : ' ' . $value->clibod_direccion;
            }


            if ($value->tradename != '' || $value->tradename != null) {

                $value->clibod_descripcion .= count($part) == 2 ? ' /' . $part[1] . ' ' . $cityDirection
                    : ' /' . $part[0] . ' ' . $cityDirection;
            } else {
                $value->clibod_descripcion .= ' ' . $cityDirection;
            }
        }
        if (count($warehouses) > 0) {
            return $warehouses;
        }
        return null;
    }

    public function sellersStatistics()
    {
        //listar todos los vendedores y calcular las visitas y las ventas que ha hecho
        $sellers = Contact::select('contacts.name AS seller')
            ->with(['user' => function ($query) {
                $query->hasRole('vendedor');
            }])
            ->join('users AS u', 'u.contact_id', '=', 'contacts.id')
            ->join('model_has_roles AS mr', 'mr.model_id', '=', 'u.id')
            //->join('roles AS r', 'r.id', '=', 'mr.role_id')
            ->join('roles AS r', function ($join) {
                $join->on('r.id', '=', 'mr.role_id');
                $join->on('r.name', '=', DB::raw("'vendedor'"));
            })
            /*->where('r.name', 'vendedor')*/
            ->get();

        return $sellers;
    }

    /**
     * consulta terceros por categoria y tipo
     */
    public function findByCategory($request)
    {

        $contacts = Contact::select('id', 'name', 'surname', 'is_customer', 'is_provider', 'is_employee')
            ->when(($request->type == 5),
                function ($query) use ($request) {
                    $query->whereHas('customer', function ($query2) use ($request) {

                        $query2->where('category_id', $request->category);
                    });
                }
            )
            ->when(($request->type == 7),
                function ($query) use ($request) {
                    $query->whereHas('provider', function ($query2) use ($request) {

                        $query2->where('category_id', $request->category);
                    });
                }
            )
            ->when(($request->type == 8),
                function ($query) use ($request) {
                    $query->whereHas('employee', function ($query2) use ($request) {
                        $query2->where('category_id', $request->category);
                    });
                }
            )
            ->with(['provider', 'customer', 'employee'])
            ->where('name', 'ILIKE', '%' . $request->filter . '%')
            ->get();

        return $contacts;
    }


    public function uploadExcel($request)
    {
        try {
            if ($request->file('terceros')) {
                $file = $request->file('terceros');

                if ($file->isValid()) {
                    $registros = Excel::load($file->getRealPath(), function ($reader) {
                        DB::beginTransaction();

                        foreach ($reader->toArray() as $key => $row) {
                            $identification_type = (isset($row['d']) && strlen(trim($row['d'])) > 0 ? 1 : 2);
                            $identification = (isset($row['codigo']) && strlen(trim($row['codigo'])) > 0 ? trim($row['codigo']) : "");
                            $check_digit = (isset($row['d']) && strlen(trim($row['d'])) > 0 ? trim($row['d']) : "");
                            $name = (isset($row['nombre']) && strlen(trim($row['nombre'])) > 0 ? trim($row['nombre']) : "");
                            $address = (isset($row['direccion']) && strlen(trim($row['direccion'])) > 0 ? trim($row['direccion']) : ".");
                            $telephone = (isset($row['telefonos']) && strlen(trim($row['telefonos'])) > 0 ? explode(' ', trim($row['telefonos'])) : []);
                            $main_telephone = (isset($telephone[0]) && strlen(trim($telephone[0])) > 0 ? trim($telephone[0]) : "0");
                            $cell_phone = (isset($telephone[1]) && strlen(trim($telephone[1])) > 0 ? trim($telephone[1]) : "0");
                            $city_id = "148";

                            if (isset($row['ciudad'])) {
                                $ciudad = explode('-', trim($row['ciudad']));

                                if (isset($ciudad[0]) && isset($ciudad[1])) {
                                    $cities = City::where('city', 'ILIKE', '%' . $ciudad[0] . '%')
                                        ->whereHas('department', function ($department) use ($ciudad) {
                                            $department->where('department', 'ILIKE', '%' . $ciudad[1] . '%');
                                        })
                                        ->first();

                                    $city_id = ($cities ? $cities->id : "148");
                                } elseif (isset($ciudad[0]) && !isset($ciudad[1])) {
                                    $cities = City::where('city', 'ILIKE', '%' . $ciudad[0] . '%')
                                        ->first();
                                }
                            }

                            $nuevo = Contact::where('identification', $identification)->first();

                            $datos = [
                                'subscriber_id' => 1,
                                'identification_type' => $identification_type,
                                'identification' => $identification,
                                'check_digit' => $check_digit,
                                'name' => $this->slug($name),
                                'surname' => ".",
                                'address' => $this->slug($address),
                                'main_telephone' => $main_telephone,
                                'cell_phone' => $cell_phone,
                                'city_id' => $city_id,
                                'is_customer' => 1,
                                'is_provider' => 0,
                                'is_employee' => 0,
                                'class_person' => 8,
                                'taxpayer_type' => 13,
                                'tax_regime' => 11,
                                'declarant' => 0,
                                'self_retainer' => 0,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];

                            if (!$nuevo) {
                                $contact = Contact::create($datos);

                                if ($contact) {
                                    $datos_cliente = [
                                        'category_id' => 18,
                                        'blocked' => 0,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    ];

                                    $contact->customer()->create($datos_cliente);

                                    DB::commit();
                                }
                            }
                        }
                    })->toArray();

                    if (count($registros) > 0) {
                        return response()->json(['message' => "Contacts imported successfully"], 200);
                    } else {
                        throw new \Exception("There were no imported contacts", 400);
                    }
                } else {
                    throw new \Exception("The requested file is not valid", 400);
                }
            } else {
                throw new \Exception("The requested file does not exist", 404);
            }
        } catch (\Exception  $e) {
            $codigo = (in_array($e->getCode(), [400, 200, 404, 500]) ? $e->getCode() : 401);

            return response()->json(['error' => $e->getMessage()], $codigo);
        }
    }

    public function slug($text, $strict = false)
    {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.\s]+~u', '-', $text);

        // trim
        $text = trim($text, '-');
        setlocale(LC_CTYPE, 'en_GB.utf8');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', ' ', $text);
        if (empty($text)) {
            return 'empty_$';
        }
        if ($strict) {
            $text = str_replace(".", " ", $text);
        }
        return mb_strtoupper($text, 'UTF-8');
    }

    public function getContacts()
    {
        $contacts = Contact::all("id", "identification", "name", "address", "main_telephone", "code_ciiu")->take(5000);
        return $contacts;
    }

    public function getContactsSelect()
    {
        $contacts = Contact::select("id", "name")->take(100)->get();
        return $contacts;
    }


    public function createProspectiveCustomer($request, $seller)
    {

        $request['latitud'] = isset($request['latitud']) ? $request['latitud'] : '0';
        $request['longitud'] = isset($request['longitud']) ? $request['longitud'] : '0';
        $contact = Contact::create([
            'subscriber_id' => 1,
            'identification_type' => $request['identification_type'],
            'identification' => $request['identification'],
            'check_digit' => $request['check_digit'] == '' || $request['check_digit'] == '' ? 0 : $request['check_digit'],
            'name' => $request['contact_name'],
            'surname' => $request['surname'],
            'city_id' => $request['city_id'],
            'address' => $request['address'],
            'main_telephone' => $request['main_telephone'] ? $request['main_telephone'] : null,
            'cell_phone' => $request['cell_phone'],
            'email' => $request['email'],
            'latitude' => $request['latitud'],
            'length' => $request['longitud'],
            'is_customer' => true,
            'class_person' => 5,
            'taxpayer_type' => 14,
            'tax_regime' => 11,
            'observations' => $request['observations'],
            'tradename' => $request['tradename'] == '' ? null : $request['tradename'],
            'erp_id'=>1
        ]);

        $contact_customer = DB::table('contacts_customers')->insert([
            'contact_id' => $contact->id,
            'category_id' => 24473,
            'seller_id_1' => $seller->id,
            'b2b_portal_access' => true
        ]);

        $contact_clerk = DB::table('contacts_clerks')->insert([
            'contact_id' => $contact->id,
            'identification_type' => $request['identification_type'],
            'name' => $request['contact_name'],
            'surname' => $request['surname'],
            'gender' => $request['gender'] ? $request['gender'] : null,
            'city_id' => $request['city_id'],
            'main_telephone' => $request['main_telephone'] ? $request['main_telephone'] : null,
            'cell_phone' => $request['cell_phone'],
            'email' => $request['email'],
            'observation' => $request['observations'] ? $request['observations'] : null,

        ]);

        $contact_warehouse = DB::table('contacts_warehouses')->insertGetId([
            'contact_id' => $contact->id,
            'code' => '001',
            'description' => $request['name'],
            'address' => $request['address'],
            'latitude' => $request['latitud'],
            'length' => $request['longitud'],
            'telephone' => $request['main_telephone'] ? $request['main_telephone'] : null,
            'email' => $request['email'],
            'observation' => $request['observations'] ? $request['observations'] : null,
            'seller_id' => $seller->contact_id,
            'price_list_id' => 271,
            'picture' => $request['photo'] ? $request['photo'] : null,
            'branchoffice_id' => $seller->branchoffice_id,
            'erp_id'=>1
        ]);

        /*$contact_warehouse_line = DB::table('contacts_warehouses_lines')->insert([
			'contact_warehouse_id' => $contact_warehouse,
			'line_id' => $request['lines']
		]);*/

        return $contact_warehouse;
        // guardar timestamps en todas las tablas
    }

    /**
     * Valida que el tercero no tenga documentos asociados para eliminarlo
     * @param $id
     * @return bool[]
     */
    public function delete($id)
    {
        $contact = Contact::with('allDocuments')->find($id);

        if ($contact) {
            if ($contact->allDocuments->count() > 0) {
                return ['status' => false, 'message' => 'No es posible eliminar debido a que el tercero tiene documentos asociados'];
            }
        } else {
            return ['status' => false, 'message' => 'No existe el tercero'];
        }

        $contact->delete();

        return ['status' => true];

    }

    /**
     * @param array $request
     * @return
     */
    public function storeBasicAsCustomer(array $request)
    {
        try{
            DB::beginTransaction();

            $dataContactWarehouse = [
                'code' => '000',
                'description' => $request['name'] . ' ' . $request['surname'],
                'city_id' => $request['city_id'],
                'address' =>(isset($request['address']) ? $request['address'] : null),
                'telephone' => $request['main_telephone'],
                'email' => $request['email'],
                'cell_phone' => $request['cell_phone'],
                'branchoffice_id' => $request['branchoffice_id'],
                'creation_date' => date('Y-m-d')
            ];

            $contactWarehouse = new ContactWarehouse($dataContactWarehouse);


            $contact = Contact::where('identification', $request['identification'])->first();

            if ($contact){
                $contact->update($request);

                $warehouse = $contact->warehouses->firstWhere('code', '000');

                if (!$warehouse){
                    $contact->warehouses()->save($contactWarehouse);
                }

                if (isset($request['vehicle']) && isset($request['vehicle']['vehicle_type_id']) && !empty($request['vehicle']['vehicle_type_id'])) {
                    $vehicle = $contact->contactVehicles->firstWhere('license_plate', $request['vehicle']['license_plate']);

                    if (!$vehicle) {
                        $contactVehicle = new ContactVehicle($request['vehicle']);
                        $contact->contactVehicles()->save($contactVehicle);
                    } else {
                        $vehicle->update($request['vehicle']);
                    }
                }

            }else{
                $contact = Contact::create($request);

                $contact->warehouses()->save($contactWarehouse);

                if ($contact->is_customer){
                    $contactCustomer = new ContactCustomer();
                    $contact->customer()->save($contactCustomer);
                    if (isset($request['vehicle']) && isset($request['vehicle']['vehicle_type_id']) && !empty($request['vehicle']['vehicle_type_id'])){
                        $contactVehicle = new ContactVehicle($request['vehicle']);
                        $contact->contactVehicles()->save($contactVehicle);
                    }
                    $contact->customer;
                }else if ($contact->is_provider){
                    $contactProvider = new ContactProvider();
                    $contact->provider()->save($contactProvider);
                    $contact->provider;
                }else if ($contact->id_employee){
                    $contactEmployee = new ContactEmployee();
                    $contact->employee()->save($contactEmployee);
                    $contact->employee;
                }
            }

            $contact->warehouses;
            $contact->city;
            $contact->contactVehicles;

            DB::commit();
            return $contact;
        }catch(\Exception $exception){
            Log::error($exception);
            DB::rollBack();
        }
    }

    /**
     * Consultar los terceros transportadores
     * @return mixed Contiene los terceros transportadores
     * @author Jhon García
     */
	public function getConveyors()
	{
	    return Contact::select('id', 'name', 'surname')
            ->where('is_provider', true)
            ->whereHas('provider', function($provider){
                $provider->whereHas('category', function ($category) {
                    $category->where('code_parameter', '03');
                });
            })
            ->get();
	}
}
