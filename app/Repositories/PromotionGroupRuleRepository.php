<?php

namespace App\Repositories;

use App\Entities\PromotionGroupRule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromotionGroupRuleRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:35 pm UTC
 *
 * @method PromotionGroupRule findWithoutFail($id, $columns = ['*'])
 * @method PromotionGroupRule find($id, $columns = ['*'])
 * @method PromotionGroupRule first($columns = ['*'])
*/
class PromotionGroupRuleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion_id',
        'percentage',
        'erp_id',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromotionGroupRule::class;
    }
}
