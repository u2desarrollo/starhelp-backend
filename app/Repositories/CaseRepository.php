<?php

namespace App\Repositories;

use App\Entities\CaseTable;
use InfyOm\Generator\Common\BaseRepository;

class CaseRepository extends BaseRepository
{

    public function model()
    {
        return CaseTable::class;
    }

    public function findAll($request)
    {
        // if (!$request->company_id) {
        //     return [
        //         'code' => '400',
        //         'data' => 'Faltan parametros'
        //     ];
        // }

        $paginate = $request->paginate && $request->paginate === 'true' ? true : false;
        $perPage  = $request->perPage ? $request->perPage : 50;
        $filter   = $request->filter ? $request->filter : null;

        $company_id     = $request->company_id     ? $request->company_id     : null;
        $area_id        = $request->area_id        ? $request->area_id        : null;
        $responsiblesIds   = $request->responsiblesIds   ? $request->responsiblesIds   : null;
        $state_id       = $request->state_id       ? $request->state_id       : null;
        
        $cases = CaseTable::with([
            'user' => function ($query) {
                $query->select([
                    'id',
                    'name',
                    'surname',
                    'email',
                    'area_id',
                    'company_id'
                ]);
            },
            'area',
            'company' => function ($query) {
                $query->with([
                    'responsibles' => function ($query) {
                        $query->select([
                            'name',
                            'surname',
                            'email',
                            'area_id',
                        ]);
                    },
                ]);
            },
            'priority',
            'state'
        ])
        ->when($filter, function ($query) use ($filter) {
            $arrayFilters = explode(' ', $filter);

            if (count($arrayFilters) > 0) {

                foreach ($arrayFilters as $key => $value) {
                    $query->where(function ($query) use ($value) {
                        $query->where('title', 'ILIKE', '%'.$value.'%');
                        $query->orWhere('content', 'ILIKE', '%'.$value.'%');
                    });
                }

            }
        })
        ->when($company_id, function ($query) use ($company_id) {
            $query->where('company_id', $company_id);
        })
        ->when($area_id, function ($query) use ($area_id) {
            $query->where('area_id', $area_id);
        })
        ->when($responsiblesIds, function ($query) use ($responsiblesIds) {
            $query->whereHas('company', function ($query) use ($responsiblesIds) {
                $query->whereHas('responsibles', function ($query) use ($responsiblesIds) {
                    $query->whereIn('users.id', $responsiblesIds);
                });
            });
        })
        ->when($state_id, function ($query) use ($state_id) {
            $query->where('state_id', $state_id);
        })
        ->orderBy('created_at', 'ASC');

        return [
            'code' => '200',
            'data' => $paginate ? 
            $cases->paginate($perPage) :
            $cases->get()
        ];
    }

    public function findById($id, $request)
    {
        $case = CaseTable::with([
            'answers'  => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select([
                            'id',
                            'name',
                            'surname',
                            'email',
                            'area_id',
                            'company_id'
                        ]);
                    },
                ]);
                $query->orderBy('created_at');
            },
            'user' => function ($query) {
                $query->select([
                    'id',
                    'name',
                    'surname',
                    'email',
                    'area_id',
                    'company_id'
                ]);
            },
            'area',
            'company' => function ($query) {
                $query->with([
                    'responsibles' => function ($query) {
                        $query->select([
                            'name',
                            'surname',
                            'email',
                            'area_id',
                        ]);
                    },
                ]);
            },
            'priority',
            'state'
        ])
        ->find($id);

        return [
            'data' => $case,
            'code' => 200
        ];
    }

}
