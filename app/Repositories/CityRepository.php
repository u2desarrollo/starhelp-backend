<?php

namespace App\Repositories;

use App\Entities\City;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CityRepository
 * @package App\Repositories
 * @version December 27, 2018, 9:50 pm UTC
 *
 * @method City findWithoutFail($id, $columns = ['*'])
 * @method City find($id, $columns = ['*'])
 * @method City first($columns = ['*'])
*/
class CityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'department_id',
        'city'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return City::class;
    }

    public function cities_departaments($request)
    {
        $filter = $request->filter;
        return City :: where('city', 'ILIKE', '%' . $filter . '%')
                        ->get();
    }
}
