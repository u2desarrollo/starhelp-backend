<?php

namespace App\Repositories;

use App\Entities\CriteriaPlan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CriteriaPlanRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:40 pm UTC
 *
 * @method CriteriaPlan findWithoutFail($id, $columns = ['*'])
 * @method CriteriaPlan find($id, $columns = ['*'])
 * @method CriteriaPlan first($columns = ['*'])
*/
class CriteriaPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'param_table_id',
        'criteria_type_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CriteriaPlan::class;
    }
}
