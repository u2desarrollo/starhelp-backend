<?php

namespace App\Repositories;

use App\Entities\Categorie;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategorieRepository
 * @package App\Repositories
 * @version March 6, 2019, 9:48 pm UTC
 *
 * @method Categorie findWithoutFail($id, $columns = ['*'])
 * @method Categorie find($id, $columns = ['*'])
 * @method Categorie first($columns = ['*'])
*/
class CategorieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'categorie_code',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Categorie::class;
    }

    /**
     * Método para filtrar brands de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $brands colección de brands
     */
    public function findAll($request) {
        if ($request->line) {
            $categories = Categorie::whereHas('products', function ($query) use ($request){
                return $query->with(['line' => function ($line) use ($request){
                    $line->where('line_code', $request->line);
                }])
                ->when(isset($request->filter_brands) && is_array($request->filter_brands),
                    function ($query) use ($request) {
                        return $query->whereIn('brand_id', $request->filter_brands);
                    }
                )->when(isset($request->filter_groups) && is_array($request->filter_groups),
                    function ($query) use ($request) {
                        return $query->whereIn('subline_id', $request->filter_groups);
                    }
                );
            })
            ->orderBy('description','Asc')
            ->get();
        } else if($request->paginate == 'true') {
            $categories = Categorie::where('categorie_code', 'ILIKE', '%' . $request->filter . '%')
                    ->orWhere('description', 'ILIKE', '%' . $request->filter . '%')
                    ->orderBy($request->sortBy, $request->sort)
                    ->paginate($request->perPage);
        } else {
            $categories = Categorie::orderBy('description','Asc')->get();
        }

        return $categories;
    }

    public function findForId($id){
        $categorie = Categorie::where('id', $id)->first();
        return $categorie;
    }
}
