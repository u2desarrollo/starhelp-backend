<?php

namespace App\Repositories;

use App\Traits\CacheTrait;
use App\Traits\CollectionsTrait;
use Excel;
use App\Entities\Product;
use App\Entities\Brand;
use App\Entities\Line;
use App\Entities\Subline;
use App\Entities\Categorie;
use App\Entities\ParamTable;
use App\Entities\Parameter;
use App\Entities\Promotion;
use App\Entities\Contact;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\BranchOffice;
use App\Entities\Template;
use App\Entities\Inventory;
use App\Entities\Subscriber;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductRepository
 *
 * @package App\Repositories
 */
class ProductRepository extends BaseRepository
{
    use CollectionsTrait;
    use CacheTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'barcode',
        'description',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'line',
        'category_id',
        'subcategory_id',
        'inventory_management',
        'purchase',
        'sale',
        'kit',
        'state',
        'product_id',
        'condition',
        'has_lot',
        'has_serial',
        'high_price',
        'products_risk_id',
        'rotation_type',
        'cooled',
        'reusable',
        'production_formula',
        'tariff_position',
        'export_product',
        'pos',
        'lock_buy',
        'lock_buy_observations',
        'lock_sale',
        'lock_sale_observations',
        'percentage_iva_buy',
        'percentage_tax_consumer_buy',
        'percentage_iva_sale',
        'percentage_tax_consumer_sale',
        'balance_units',
        'balance_value',
        'average_cost',
        'start_date_inventory',
        'modify_description_buy',
        'modify_price_buy',
        'observation_buy',
        'presentation_packaging_buy',
        'presentation_quantity_buy',
        'presentation_unit_buy',
        'presentation_assembled_buy',
        'minimum_quantity_buy',
        'local_buy',
        'calculate_price_buy',
        'automatic_reposition',
        'contact_id',
        'point_of_sale_deployment',
        'amazon_deployment',
        'free_market_deployment',
        'modify_description_sale',
        'modify_price_sale',
        'observation_sale',
        'allow_invoices_sales_zeros',
        'presentation_packaging_sale',
        'presentation_quantity_sale',
        'presentation_unit_sale',
        'presentation_assembled_sale',
        'minimum_quantity_sale',
        'commision_percentage',
        'profit_margin_percentage',
        'entry_third_party',
        'controlled_sale',
        'product_of_continuous_use',
        'price',
        'quantity_available',
        'cost_center'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }

    /**
     * Consultar los productos modificados desde una fecha especifica
     *
     * @param Request $request Contiene los filtros a aplicar en la consulta
     * @return mixed Contiene una colección con los productos obtenidos de la base de datos
     * @author Jhon García
     */
    public function findAllSimple(Request $request)
    {
        $products = Product::select(
            'id',
            'long_description',
            'code',
            'description',
            'category_id',
            'subline_id',
            'brand_id',
            'subbrand_id',
            'line_id',
            'update_in_app',
            'created_at',
            'updated_at',
            'state'
        )
            ->with([
                'price',
                'productsTechnnicalData',
                'data_sheets:id,product_id,value',
                'brand:id,brand_code,description,image',
                'category:id,categorie_code,description',
                'subline:id,subline_code,subline_description',
                'subbrand:id,brand_id,subbrand_code,description,image',
                'line.taxes:code,description,percentage_sale,percentage_purchase',
                'line'                => function ($query) {
                    $query->select("id", "line_description", "line_code");
                    $query->with([
                        'taxes' => function ($query_two) {
                            $query_two->select('taxes.id', 'code', 'description', 'percentage_sale as percentage');
                        }
                    ]);
                },
                'productsAttachments' => function ($products_attachments) {
                    $products_attachments->select('product_id', 'url')
                        ->orderBy('type', 'DESC')
                        ->where('type', 'i');
                },
            ])
            ->where(function ($query) {
                $query->where('lock_sale', false);
                $query->orWhere('lock_sale', null);
            })
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })
            ->where(function ($query) {
                $query->orWhere('state', true);
                $query->orWhere('state', null);
            });

        if ($request->date_from) {
            $products->where('updated_at', '>=', $request->date_from);
        }

        return $products->orderBy('description', 'ASC')->get();
    }

    public function findAll(Request $request, $perPage = 15)
    {
        // Variables
        $paginate = $request->paginate == 'true';
        $reference = $request->reference;
        $filter = $request->filter;
        $filter_lines = $request->filter_lines;
        $filter_brands = $request->filter_brands;
        $filter_sublines = $request->filter_sublines;
        $filter_subbrands = $request->filter_subbrands;
        $only_promotions = $request->only_promotions == 'true';
        $branchoffice_id = $request->branchoffice_id;

        $branchoffice = BranchOffice::find($request->branchoffice_id);
        $model = Template::find($request->model_id);

        $branchoffice_wharehouse_id = null;
        $branchoffice_wharehouse_ = null;

        if ($branchoffice_id) {
            $branchoffice_wharehouse_ = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->where('warehouse_code', '01')->first();
            if ($branchoffice_wharehouse_) {
                $branchoffice_wharehouse_id = $branchoffice_wharehouse_->id;
            }
        }

        $products = Product::select('id', 'long_description', 'code', 'description', 'category_id', 'subline_id', 'brand_id', 'subbrand_id', 'line_id', 'quantity_available', 'lock_sale', 'percentage_iva_sale', 'manage_inventory', 'provider', 'parameter_status', 'state')
            ->with([
                'category:id,categorie_code,description',
                'line',
                'subline',
                'data_sheets',
                'productsPromotions',
                'productsAttachments',
                'productAttachmentMain',
                'productsTechnnicalData',
                'brand:id,brand_code,description,image',
                'subbrand:id,brand_id,subbrand_code,description,image',
                'price:id,product_id,price_list_id,branchoffice_id,price',
                'line.taxes:code,description,percentage_sale,percentage_purchase',
                'prices:id,branchoffice_id,include_iva,product_id,price,price_list_id',
                'inventory'            => function ($query) use ($branchoffice_wharehouse_id, $branchoffice_id, $branchoffice_wharehouse_) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available');

                    // Inventario desde bogota
                    $branchofficeModifyInventory = [288, 286, 307];

                    if (in_array($branchoffice_id, $branchofficeModifyInventory)) {
                        $query->where('branchoffice_warehouse_id', 279);
                    } else if ($branchoffice_id && $branchoffice_wharehouse_) {
                        $query->where('branchoffice_warehouse_id', $branchoffice_wharehouse_id);
                    }
                },
                'productHasPromotions' => function ($query) use ($branchoffice_id) {
                    $query->where('branchoffice_id', $branchoffice_id);
                }
            ])
            ->when($only_promotions, function ($query) use ($branchoffice_id) {
                $query->whereHas('productHasPromotions', function ($query) use ($branchoffice_id) {
                    $query->where('branchoffice_id', $branchoffice_id);
                });
            })
            ->where(function ($query) {
                $query->where('parameter_status', "!=", 24620);
                $query->orwhereNull('parameter_status');
            })
            ->where(function ($query) {
                $query->where('lock_sale', false);
                $query->orWhere('lock_sale', null);
            })
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })
            ->where(function ($query) {
                $query->orWhere('state', true);
                $query->orWhere('state', null);
            });

        if ($reference && $reference != '') {
            $products->when(function ($query) use ($reference) {
                return $query->where('code', 'ILIKE', '%' . $reference . '%');
            });
        } else {

            // Filtro de la linea
            if ($filter_lines && is_array($filter_lines)) {
                $products->whereIn('line_id', $filter_lines);
            }

            // Filtro de la marca
            if ($filter_brands && is_array($filter_brands)) {
                $products->whereIn('brand_id', $filter_brands);
            }

            // Filtro de la sub linea
            if ($filter_sublines && is_array($filter_sublines)) {
                $products->whereIn('subline_id', $filter_sublines);
            }

            // Filtro de la sub marca
            if ($filter_subbrands && is_array($request->filter_subbrands)) {
                $products->whereIn('subbrand_id', $filter_subbrands);
            }

            // Filtros de texto
            if ($filter && !empty($filter)) {

                $arrayFilters = explode(' ', $filter);

                if (count($arrayFilters) > 0) {

                    foreach ($arrayFilters as $key => $value) {
                        $products->where(function ($query) use ($value) {

                            // Filtro datasheets
                            $query->orWhereHas('data_sheets', function ($query) use ($value) {
                                $query->where('value', 'ilike', '%' . $value . '%');
                            });

                            // Filtro CODE
                            $query->orWhere('code', 'ilike', '%' . $value . '%');

                            // Filtro Description
                            $query->orWhere('description', 'ILIKE', '%' . $value . '%');

                        });
                    }

                }

            }

        }

        if ($paginate) {
            $products = $products->orderBy('line_id', 'ASC')->orderBy('description', 'ASC')->paginate($perPage);
        } else {
            $products = $products->orderBy('description', 'ASC')->get();
        }

        // Validamos si existe modelo
        if ($model) {
            $price_list_id = $model->price_list_id;
        } else {
            $price_list_id = Parameter::where('paramtable_id', 14)->where('code_parameter', '001')->first();
            $price_list_id = $price_list_id ? $price_list_id->id : null;
        }

        foreach ($products as $key => $product) {

            // Traemos la lista de precio segun el modelo
            $listPrices = $product->prices->where('price_list_id', $price_list_id)->toArray();

            // Traemos la lista de precios segun la sede
            $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', $branchoffice->id)->values()->toArray();

            // Se valida si hay lista de precios en la sede, si no se trae la global el cual tiene como "null" el campo "branchoffice_id"
            if (!$listPriceBranchoffice) {
                $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', null)->toArray();
            }

            // Guardamos la informacion
            $products[$key]['price'] = isset($listPriceBranchoffice[0]) ? $listPriceBranchoffice[0] : ['price' => 0];
        }

        return $products;
    }

    public function related($id, Request $request)
    {
        $related_products = DB::table('related_products')->where('product_id', $id)->pluck('product_related_id');

        $products = Product::select('id', 'long_description', 'code', 'description', 'category_id', 'subline_id', 'brand_id', 'line_id', 'quantity_available', 'lock_sale')
            ->with([
                'category',
                'line',
                'subline',
                'brand',
                'price' => function ($query) use ($request) {
                    $query->where('price_list_id', $request->price_list_id);
                },
                'productsAttachments',
                'productsTechnnicalData',
                //             'promotionPrice' => function ($subquery) use ($request){
                //                 $subquery->where('contact_warehouse_id', $request->warehouse);
                //             }
            ])
            ->where('lock_sale', false)
            ->whereIn('id', $related_products)
            ->whereHas('price', function ($query) use ($request) {
                $query->where('price_list_id', $request->price_list_id);
            })
            ->get();

        return $products;
    }

    public function finishItem(array $cart)
    {
        $products = DB::table('documents_products')->where('document_id', $cart['id'])->get();

        foreach ($products as $product) {
            // Obtener la cantidad disponible del producto
            $current = Product::where('id', $product->product_id)->first();

            // Restar la cantidad hecha en el pedido
            $available = (int)$current->quantity_available - $product->quantity;

            // Guardar el producto con disponibilidad
            $update = Product::where('id', $product->product_id)->update([
                'quantity_available' => $available
            ]);
        }

        if ($update) {
            return $current;
        }

        return new \App\Entities\Product;
    }


    public function uploadExcel($request)
    {
        try {
            if ($request->file('productos')) {
                $file = $request->file('productos');
                $registros = [];

                if ($file->isValid()) {

                    Excel::load($file->getRealPath(), function ($reader) use ($registros) {
                        DB::beginTransaction();

                        foreach ($reader->toArray() as $key => $row) {
                            $codigo = (isset($row['codigo']) && strlen(trim($row['codigo'])) > 0 ? trim($row['codigo']) : "");
                            $descripcion = (isset($row['descripcion']) && strlen(trim($row['descripcion'])) > 0 ? trim($row['descripcion']) : "");
                            $ma = (isset($row['ma']) && strlen(trim($row['ma'])) > 0 ? trim($row['ma']) : null);
                            $li = (isset($row['li']) && strlen(trim($row['li'])) > 0 ? trim($row['li']) : null);
                            $sl = (isset($row['sl']) && strlen(trim($row['sl'])) > 0 ? trim($row['sl']) : null);
                            $subline = (isset($row['sublinea']) && strlen(trim($row['sublinea'])) > 0 ? trim($row['sublinea']) : null);

                            $brand_id = 1;
                            $line_id = 1;
                            $subline_id = 1;
                            $category_id = 1;

                            if ($ma) {
                                $marca = Brand::where('brand_code', $ma)->first();

                                if (!$marca) {
                                    $marca = Brand::create([
                                        'brand_code'  => $ma,
                                        'description' => (isset($row['marca']) && strlen(trim($row['marca'])) > 0 ? trim($row['marca']) : ""),
                                    ]);

                                    if ($marca) {
                                        $brand_id = $marca->id;
                                    }
                                } else {
                                    $brand_id = $marca->id;
                                }
                            }

                            if ($li) {
                                $linea = Line::where('line_code', $li)->first();

                                if (!$linea) {
                                    $linea = Line::create([
                                        'subscriber_id'    => 1,
                                        'line_code'        => $li,
                                        'line_description' => (isset($row['linea']) && strlen(trim($row['linea'])) > 0 ? trim($row['linea']) : ""),
                                    ]);

                                    if ($linea) {
                                        $line_id = $linea->id;
                                    }
                                } else {
                                    $line_id = $linea->id;
                                }

                                if ($sl) {
                                    $sublinea = Subline::where('subline_code', $sl)
                                        ->where('line_id', $line_id)
                                        ->first();

                                    if (!$sublinea) {
                                        $sublinea = Subline::create([
                                            'line_id'             => $line_id,
                                            'subline_code'        => $sl,
                                            'subline_description' => (isset($row['sublinea']) && strlen(trim($row['sublinea'])) > 0 ? trim($row['sublinea']) : ""),
                                        ]);

                                        if ($sublinea) {
                                            $subline_id = $sublinea->id;
                                        }
                                    } else {
                                        $subline_id = $sublinea->id;
                                    }
                                }
                            }

                            if ($subline) {
                                $categoria = Categorie::where('description', $subline)->first();

                                if (!$categoria) {
                                    $categoria = Categorie::create([
                                        'categorie_code' => $sl,
                                        'description'    => $subline,
                                    ]);

                                    if ($categoria) {
                                        $category_id = $categoria->id;
                                    }
                                } else {
                                    $category_id = $categoria->id;
                                }
                            }

                            $nuevo = Product::where('code', $codigo)->first();

                            $datos = [
                                'code'               => $codigo,
                                'description'        => $this->slug($descripcion),
                                'line_id'            => $line_id,
                                'brand_id'           => $brand_id,
                                'subline_id'         => $subline_id,
                                'category_id'        => $category_id,
                                'quantity_available' => rand(pow(10, 2 - 1), pow(10, 2) - 1),
                                'created_at'         => date('Y-m-d H:i:s'),
                                'updated_at'         => date('Y-m-d H:i:s'),
                            ];

                            if (!$nuevo) {
                                $product = Product::create($datos);

                                if ($product) {
                                    $registros[count($registros)] = $product->id;
                                }

                                DB::commit();
                            }
                        }
                    });

                    if (count($registros) > 0) {
                        return response()->json(['message' => "Products imported successfully"], 200);
                    } else {
                        throw new \Exception("The requested file is not valid", 400);
                    }
                } else {
                    throw new \Exception("The requested file is not valid", 400);
                }
            } else {
                throw new \Exception("The requested file does not exist", 404);
            }
        } catch (\Exception  $e) {
            $codigo = (in_array($e->getCode(), [400, 200, 404, 500]) ? $e->getCode() : 401);

            return response()->json(['error' => $e->getMessage()], $codigo);
        }
    }

    public function findForCode($request)
    {
        $product = Product::where('code', 'ILIKE', $request)
            ->with([
                'category',
                'line',
                'subline',
                'brand',
                'productsAttachments',
                'productsTechnnicalData'
            ])
            ->first();
        return $product;
    }

    public function getProductsGrid($request)
    {
        // Traemos el modelo #CAMBIOMODELOPORDEFECTO
        $model = Template::find($request->model_id);

        // Validamos si el modelo ya tiene una sede asignada
        if ($model) {
            if (!empty($model->branchoffice_id)) {
                $request->merge(['branchoffice_id' => $model->branchoffice_id]);
            }

            if (!empty($model->branchoffice_warehouse_id)) {
                $branchOfficeWarehouse = BranchOfficeWarehouse::where('branchoffice_id', $request->branchoffice_id)
                    ->where('warehouse_code', $model->branchOfficeWarehouse->warehouse_code)
                    ->first();
                if ($branchOfficeWarehouse) {
                    $request->merge(['branchoffice_warehouse_id' => $branchOfficeWarehouse->id]);
                }
            }
        }

        if ($request->branchoffice_warehouse_id) {
            $branchoffice_warehouse = BranchOfficeWarehouse::find($request->branchoffice_warehouse_id);
        }

        $branchoffice_warehouse_id = $request->branchoffice_warehouse_id;
        if ($request->branchoffice_warehouse_code) {
            $branchoffice_warehouse_id = BranchOfficeWarehouse::where('warehouse_code', $request->branchoffice_warehouse_code)->where('branchoffice_id', $request->branchoffice_id)->first()->id;
        }


        if ($request->branchoffice_id) {
            $branchoffice = BranchOffice::find($request->branchoffice_id);
        } else {
            $branchoffice = BranchOffice::find($branchoffice_warehouse->branchoffice_id);
        }

        $line = Line::where('line_description', 'BATERIAS')->first();

        // POR FAVOR HACER MODIFICACIONES EN ESTA CONSULTA
        $products = Product::select("id", "code", "description", "line_id", "subline_id", "brand_id", "quantity_available as quantity", 'manage_inventory', 'state')
        ->when(!$request->validate_exhausted == 'false', function($validation){
            $validation->where(function($query){
                $query->where('parameter_status',null)->
                orWhere('parameter_status','!=',24620);
            });
        })
        ->with([
            'prices' => function($query) use ($request){
                $query->where('branchoffice_id',null);
                $query->orWhere('branchoffice_id',$request->branchoffice);
            },
            'category',
            'line',
            'subline',
            'brand',
            'priceContact' => function ($subquery) use ($request) {
                $subquery->where('branchoffice_id', $request->branchoffice_id)
                    ->where('contact_id', $request->contact_id);
            },
            'inventory' => function ($subquery) use ($branchoffice_warehouse_id) {
                $subquery->where('branchoffice_warehouse_id', $branchoffice_warehouse_id);
                $subquery->with([
                    'branchofficeWarehouse'=>function ($q2){
                        $q2->select('id', 'warehouse_code', 'warehouse_description', 'branchoffice_id');
                        $q2->with(['branchoffice'=>function($q3){
                            $q3->select('id', 'code', 'name');
                        }]);
                    }
                ]);
            }
        ])
        ->where(function ($query) {
            $query->orWhere('state', true);
            $query->orWhere('state', null);
        });
        // ->where(function($query) {
        //     $query->where('parameter_status', "!=", 24620);
        //     $query->orwhereNull('parameter_status');
        // });

        if ($request->code_line_filter) {
            $products->whereHas('line', function ($query) use ($request) {
                $query->where('line_code', $request->code_line_filter);
            });
        }

        // Validamos si hay filtro de busqueda por texto.
        if ($request->filter) {

            $filter_description = !empty($request->filter) ? explode(' ', $request->filter) : [];

            foreach ($filter_description as $key => $value) {
                $products->where(function ($query) use ($value) {
                    $query->orWhere('description', 'ILIKE', '%' . $value . '%');
                    $query->orWhere('code', 'ILIKE', '%' . $value . '%');
                });
            }

        }

        // Validamos el tipo de respuesta
        if ($request->typeRequest) {
            switch ($request->typeRequest) {
                case '1':
                case '2':
                    $products->where('line_id', $line->id);
                    break;
                case '3':
                    $products->where('line_id', '!=', $line->id);
                    break;

            }
        }

        $products = $products->paginate($request->perPage);

        // Validamos si existe modelo
        if ($model) {
            $price_list_id = $model->price_list_id;
        } else {
            $price_list_id = Parameter::where('paramtable_id', 14)->where('code_parameter', '001')->first();
            $price_list_id = $price_list_id ? $price_list_id->id : null;
        }


        foreach ($products as $key => $product) {
            $product->stock = count($product->inventory) > 0 ? $product->inventory[0]->stock : 0;
            $product->branchoffice = $branchoffice->code . '-' . $branchoffice->name;

            //disponibilidad bodega disponible
            $bW = BranchOfficeWarehouse:: where('branchoffice_id', $branchoffice_warehouse->branchoffice_id)
                ->where('warehouse_code', '01')->first();

            $inventory_availability = Inventory:: where('product_id', $product->id)
                ->where('branchoffice_warehouse_id', $bW->id)
                ->first();


            if ($inventory_availability) {
                $product->availability_branchOffice_warehouse_available = $inventory_availability->available;
            } else {
                $product->availability_branchOffice_warehouse_available = 0;
            }

            // lista de precio COSTO PROMEDIO
            if ($price_list_id == '24463') {
                $inventory = Inventory::where('product_id', $product->id)
                    ->where('branchoffice_warehouse_id', $branchoffice_warehouse->id)
                    ->first();

                if ($inventory) {
                    $product->price = [
                        'price' => $inventory->average_cost
                    ];
                } else {
                    $product->price = [
                        'price' => 0
                    ];
                }

                continue;
            }

            // Traemos la lista de precio segun el modelo
            $listPrices = $product->prices->where('price_list_id', $price_list_id)->toArray();

            // Traemos la lista de precios segun la sede
            $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', $branchoffice->id)->toArray();

            // Se valida si hay lista de precios en la sede, si no se trae la global el cual tiene como "null" el campo "branchoffice_id"
            if (!$listPriceBranchoffice) {
                $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', null)->toArray();
            }

            // Guardamos la informacion
            $product->price = isset($listPriceBranchoffice[0]) ? $listPriceBranchoffice[0] : [
                'price' => [
                    'price' => 0
                ]
            ];
        }

        // Traemos la informacion
        return $products;
    }

    /**
     * @param $id
     * @param $request
     * @param bool $includeValidationForBranchOffice
     * @return mixed
     */
    public function findForId($id, $request, $includeValidationForBranchOffice = false)
    {
        if (isset($request->branchoffice_warehouse_id)) {
            $branchoffice_warehouse = BranchOfficeWarehouse::find($request->branchoffice_warehouse_id);
        }

        if (isset($request->branchoffice_id)) {
            $branchoffice = BranchOffice::find($request->branchoffice_id);
        } else {
            $branchoffice = BranchOffice::find($branchoffice_warehouse->branchoffice_id);
        }

        $model = $this->getModelForIdFromCache($request->model_id);

        $product = Product::select('id', 'long_description', 'code', 'description', 'category_id', 'subline_id', 'brand_id', 'subbrand_id', 'line_id', 'quantity_available', 'lock_sale', 'percentage_iva_sale', 'manage_inventory', 'provider', 'weight', 'exclude_sales_report')
            ->with([
                'category',
                'prices',
                'line.taxes',
                'subline',
                'brand:id,brand_code,description,image',
                'subbrand:id,brand_id,subbrand_code,description,image',
                'productsAttachments',
                'productsTechnnicalData',
                'productsPromotions',
                'price:id,product_id,price_list_id,branchoffice_id,price' ,
                'inventory' => function ($query) use ($includeValidationForBranchOffice, $request) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available')
                        ->when($includeValidationForBranchOffice, function ($validation) use ($request) {
                            $validation->whereHas('branchofficeWarehouse', function ($branchofficeWarehouse) use ($request) {
                                $branchofficeWarehouse->where(['branchoffice_id' => $request->branchoffice_id, 'warehouse_code' => '01']);
                            });
                        });
                },
            ])
            ->find($id);

        // Validamos si existe modelo
        if ($model) {
            $price_list_id = $model->price_list_id;
        } else {
            $price_list_id = Parameter::where('paramtable_id', 14)->where('code_parameter', '001')->first();
            $price_list_id = $price_list_id ? $price_list_id->id : null;
        }

        // lista de precio COSTO PROMEDIO
        if ($price_list_id == '24463') {
            $inventory = Inventory::where('product_id', $product->id)
                ->where('branchoffice_warehouse_id', $branchoffice_warehouse->id)
                ->first();

            if ($inventory) {
                $product->price->price = $inventory->average_cost;
            } else {
                $product->price->price = 0;
            }

        } else {
            if (!isset($product->prices)) {
                $product->price = [
                    'price' => 0
                ];
            } else {
                // Traemos la liosta de precio segun el modelo
                $listPrices = $product->prices->where('price_list_id', $price_list_id)->toArray();

                // Traemos la lista de precios segun la sede
                $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', $branchoffice->id)->toArray();

                // Se valida si hay lista de precios en la sede, si no se trae la global el cual tiene como "null" el campo "branchoffice_id"
                if (!$listPriceBranchoffice) {
                    $listPriceBranchoffice = collect($listPrices)->where('branchoffice_id', null)->toArray();
                }

                // Guardamos la informacion
                $product->price = isset($listPriceBranchoffice[0]) ? $listPriceBranchoffice[0] : [
                    'price' => [
                        'price' => 0
                    ]
                ];
            }
        }

        return $product;
    }

    public function findForIdSimple($id, $request)
    {
        $product = Product::select(
            'id',
            'long_description',
            'code',
            'description',
            'category_id',
            'subline_id',
            'brand_id',
            'subbrand_id', 'line_id',
            'quantity_available',
            'lock_sale',
            'percentage_iva_sale',
            'manage_inventory',
            'provider',
            'weight',
            'exclude_sales_report',
            'modify_description_sale',
            'alternate_code',
            'old_product_code',
            'certificate_of_conformity',
            'rotation_classification',
            'supplyer_code',
            'previous_code_system',
            'short_description',
            'state'
        )
            ->with([
                'category',
                'line'     => function ($query) {
                    $query->with([
                        'taxes'
                    ]);
                },
                'subline',
                'brand'    => function ($query) {
                    $query->select('id', 'brand_code', 'description', 'image');
                },
                'subbrand' => function ($query) {
                    $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                },
                'productsAttachments',
                'productsTechnnicalData',
            ])
            // ->where(function($query){
            //     $query->where('parameter_status', '!=', '24620');
            //     $query->OrwhereNull('parameter_status');
            // })
            ->find($id);

        return $product;
    }

    public function slug($text, $strict = false)
    {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.\s]+~u', '-', $text);

        // trim
        $text = trim($text, '-');
        setlocale(LC_CTYPE, 'en_GB.utf8');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', ' ', $text);
        if (empty($text)) {
            return 'empty_$';
        }
        if ($strict) {
            $text = str_replace(".", " ", $text);
        }
        return mb_strtoupper($text, 'UTF-8');
    }

    /**
     * Obtiene productos a partir de un texto y criterios de clasificación.
     *
     * @return array
     * @author Daniel Beltrán
     */
    public function productByClasification($request)
    {

        // Declaramos variables
        $search = $request->search;
        $line_id = $request->line_id;
        $subline_id = $request->subline_id;
        $brand_id = $request->brand_id;
        $subbrand_id = $request->subbrand_id;
        $branchoffice_id = $request->branchoffice_id;

        $branchoffice_wharehouse_id = null;

        if ($branchoffice_id) {
            $branchoffice_wharehouse_ = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)
                ->where('warehouse_code', '01')
                ->first();

            if ($branchoffice_wharehouse_) {
                $branchoffice_wharehouse_id = $branchoffice_wharehouse_->id;
            }
        }

        // Traemos la promocion
        $promotion = Promotion::find($request->promotion_id);

        // Realizamos la consulta de productos
        $products = Product::select(
            'id',
            'code',
            'description',
            'quantity_available',
            'line_id',
            'subline_id',
            'brand_id',
            'subbrand_id'
        )
            ->where(function ($query) {
                $query->orWhere('state', true);
                $query->orWhere('state', null);
            })
            ->with([
                'price',
                'line'      => function ($query) {
                    $query->with([
                        'taxes'
                    ]);
                },
                'inventory' => function ($query) use ($branchoffice_wharehouse_id) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available');
                    if ($branchoffice_wharehouse_id) {
                        $query->where('branchoffice_warehouse_id', $branchoffice_wharehouse_id);
                    }
                },
            ]);

        // Validamos filtros
        if ($line_id) {
            $products->where('line_id', $line_id);
        }

        if ($subline_id) {
            $products->where('subline_id', $subline_id);
        }

        if ($brand_id) {
            $products->where('brand_id', $brand_id);
        }

        if ($subbrand_id) {
            $products->where('subbrand_id', $subbrand_id);
        }

        if ($promotion && $promotion->only_available) {
            $products->whereHas('inventory', function ($query) {
                $query->where('available', '>', '0');
            });
        }

        // Realizamos la busqueda flexible
        if ($search) {
            $arraySearch = explode(' ', $search);

            // Validar el contador
            if (count($arraySearch) > 0) {
                foreach ($arraySearch as $key => $value) {
                    $products->where(function ($query) use ($value) {

                        // Filtro datasheets
                        $query->orWhereHas('data_sheets', function ($query) use ($value) {
                            $query->where('value', 'ilike', '%' . $value . '%');
                        });

                        // Filtro CODE
                        $query->orWhere('code', 'ilike', '%' . $value . '%');

                        // Filtro Description
                        $query->orWhere('description', 'ILIKE', '%' . $value . '%');

                    });
                }
            }
        }

        $products = $products->get();
        return $products;
    }

    public function getDataReport()
    {
        return Product::select('id', 'code', 'description', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'state', 'manage_inventory')
            ->with([
                'line:id,line_description',
                'subline:id,subline_description',
                'brand:id,description',
                'subbrand:id,description',
                'price:product_id,price',
                'productsAttachments:id,product_id'
            ])->get();
    }

    public function findAllProducts($request)
    {
        // Variables
        $branchoffice_id = $request->branchoffice_id;

        if ($branchoffice_id) {
            $branchoffice_wharehouse_ = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)
                ->where('warehouse_code', '01')
                ->first();

            if ($branchoffice_wharehouse_) {
                $branchoffice_wharehouse_id = $branchoffice_wharehouse_->id;
            }
        }

        return Product::select('id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id')
            ->with([
                'inventory' => function ($query) use ($branchoffice_wharehouse_id, $branchoffice_id, $branchoffice_wharehouse_) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available');
                    if ($branchoffice_id && $branchoffice_wharehouse_) {
                        $query->where('branchoffice_warehouse_id', $branchoffice_wharehouse_id);
                    }
                }
            ])
            ->where('lock_sale', false)
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })->get();
    }

    public function findProductForId($product_id, $request)
    {
        // Variables
        $branchoffice_id = $request->branchoffice_id;

        if ($branchoffice_id) {
            $branchoffice_wharehouse_ = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)
                ->where('warehouse_code', '01')
                ->first();

            if ($branchoffice_wharehouse_) {
                $branchoffice_wharehouse_id = $branchoffice_wharehouse_->id;
            }
        }

        return Product::select('id', 'long_description', 'code', 'description', 'category_id', 'subline_id', 'brand_id', 'subbrand_id', 'line_id', 'quantity_available', 'lock_sale', 'percentage_iva_sale', 'manage_inventory', 'provider')
            ->with([
                'category:id,categorie_code,description',
                'line',
                'subline',
                'data_sheets',
                'productsPromotions',
                'productsAttachments',
                'productsTechnnicalData',
                'brand:id,brand_code,description,image',
                'subbrand:id,brand_id,subbrand_code,description,image',
                'price:id,product_id,price_list_id,branchoffice_id,price',
                'line.taxes:code,description,percentage_sale,percentage_purchase',
                'prices:id,branchoffice_id,include_iva,product_id,price,price_list_id',
                'inventory'            => function ($query) use ($branchoffice_wharehouse_id, $branchoffice_id, $branchoffice_wharehouse_) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available');
                    if ($branchoffice_id && $branchoffice_wharehouse_) {
                        $query->where('branchoffice_warehouse_id', $branchoffice_wharehouse_id);
                    }
                },
                'productHasPromotions' => function ($query) use ($branchoffice_id) {
                    $query->where('branchoffice_id', $branchoffice_id);
                }
            ])
            ->where('id', $product_id)
            ->first()->toArray();
    }

    public function searchProductByCode($request)
    {
        return Product:: select('id', 'code', 'line_id', 'subline_id', 'brand_id', 'description', 'manage_inventory')
            ->whereIN('code', $request->list_code_products)
            ->with(['inventory' => function ($query) use ($request) {
                $query->where('branchoffice_warehouse_id', $request->branchoffice_warehouse_id);
            }])
            ->get();
    }

    public function repositionProducts(Request $request)
    {
        $branchoffice_id = $request->branchoffice_id;

        $branchoffice_wharehouse_id = null;
        $branchoffice_wharehouse_ = null;

        if ($branchoffice_id) {
            $branchoffice_wharehouse_ = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)
                ->where('warehouse_code', '01')
                ->first();

            if ($branchoffice_wharehouse_) {
                $branchoffice_wharehouse_id = $branchoffice_wharehouse_->id;
            }
        }

        $request->filter_date = !empty($request->filter_date) ? $request->filter_date : now()->addYear(-1)->format('Y-m-d');

        $queryBrands = "";

        if (!empty($request->filter_brands)) {
            $queryBrands = " AND dp.brand_id IN (" . implode(",", $request->filter_brands) . ")";
        }

        $query = "SELECT DISTINCT ON (dp.product_id) dp.product_id, p.code, dp.id, MAX(d.document_date) document_date
			FROM documents_products dp
			INNER JOIN products p ON p.id = dp.product_id
			INNER JOIN documents d ON d.id = dp.document_id
			WHERE d.warehouse_id = ? AND d.\"operationType_id\" = 5 AND d.document_date >= ? $queryBrands
			GROUP BY dp.product_id, dp.id, p.code
			ORDER BY dp.product_id, p.code, dp.id DESC";

        $distincts = DB::select($query, [$request->warehouse, $request->filter_date]);

        $ids = collect($distincts)->pluck('id');

        return Product::select('products.id', 'products.long_description', 'products.code', 'products.description', 'products.category_id', 'products.subline_id', 'products.brand_id', 'products.subbrand_id', 'products.line_id', 'products.quantity_available', 'products.lock_sale', 'products.percentage_iva_sale', 'products.manage_inventory', 'provider', 'd.document_date')
            ->join('documents_products AS dp', 'dp.product_id', '=', 'products.id')
            ->join('documents AS d', 'd.id', '=', 'dp.document_id')
            ->with([
                'line',
                'brand',
                'productsAttachments',
                'productsTechnnicalData',
                'brand:id,brand_code,description,image',
                'price:id,product_id,price_list_id,branchoffice_id,price',
                'line.taxes:code,description,percentage_sale,percentage_purchase',
                'prices:id,branchoffice_id,include_iva,product_id,price,price_list_id',
                'inventory'         => function ($query) use ($branchoffice_wharehouse_id, $branchoffice_id, $branchoffice_wharehouse_) {
                    $query->select('id', 'branchoffice_warehouse_id', 'product_id', 'stock', 'stock_values', 'available');
                    if ($branchoffice_id && $branchoffice_wharehouse_) {
                        $query->where('branchoffice_warehouse_id', $branchoffice_wharehouse_id);
                    }
                },
                'documentsProducts' => function ($documentsProducts) use ($ids) {
                    $documentsProducts->whereIn('id', $ids)->with(['document:id,vouchertype_id,warehouse_id,document_date']);
                }
            ])
            ->where(function ($query) {
                $query->where('lock_sale', false);
                $query->orWhere('lock_sale', null);
            })
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })
            ->whereHas('documentsProducts', function ($documentsProducts) use ($ids) {
                $documentsProducts->whereIn('id', $ids)->whereHas('document', function ($document) {
                    $document->where('in_progress', false);
                });
            })
            ->whereIn('dp.id', $ids)
            ->orderBy('document_date', 'ASC')
            ->paginate(15);
    }

    /**
     * Este metodo retorna la Informacion que se enviara al WEBSERVICE SOAP de WMS.
     *
     * @param product Id del producto si es uno a uno el consumo
     * @param action Accion que realizara el consumo 'A' Agregar o 'M' Modificar
     * En el caso que sea Masivo no es necesario ni enviar $product ni $action
     * @return Products Collection con toda la informacion consultada en BD necesaria para llenar array_productos para enviar a WMS
     * @author Haider Oviedo @Hom669
     */
    public function getProductsWms($id_producto = '', $action = 'A')
    {


        $products = Product::distinct('products.id')->select("products.id as id_product",
            "products.code",
            "products.provider",
            "products.barcode",
            "products.description",
            "products.presentation_packaging_buy",
            "lines.line_description",
            "lines.line_code",
            "lines_taxes.tax_id",
            "taxes.code as codigo_iva",
            "taxes.description as taxes_name",
            "taxes.percentage_sale",
            "products.width",
            "products.weight",
            "products.high",
            "products.rotation_classification",
            "products.alternate_code",
            "products.update_wsm",
            "products.date_update_wsm",
            "prices.price",
            "data_sheet_products.value",
            "lines.line_code_wms",
            "products.old_product_code")
            ->selectRaw("(CASE WHEN products.state = true THEN 'S' ELSE 'N' END) AS state")
            ->selectRaw("(CASE WHEN lines.tipe = 2 THEN 'S' ELSE 'N' END) AS type")
            ->join('lines', 'lines.id', '=', 'products.line_id')
            ->leftJoin('sublines', 'products.subline_id', '=', 'sublines.id')
            ->leftJoin('prices', 'prices.product_id', '=', 'products.id')
            ->leftJoin('data_sheet_products', 'data_sheet_products.product_id', '=', 'products.id')
            ->leftJoin('lines_taxes', 'lines_taxes.line_id', '=', 'lines.id')
            ->leftJoin('taxes', 'taxes.id', '=', 'lines_taxes.id')
            ->leftJoin('parameters', 'prices.price_list_id', '=', 'parameters.id')
            ->whereNull('products.deleted_at');
        //Traer Lista de Precios para monto valor producto valor_mercado
        //->where('parameters.code_parameter','=', '001');
        //Traer Solo Valor con Iva Codigo 10
        //->where('taxes.code','=', '10');

        //Verificar si la accion es para actualizar o Agregar @hom669
        if ($action == 'A') {
            $products->where('products.update_wsm', '=', false);
        } else {
            $products->where('products.update_wsm', '=', true);
        }

        // Consulta Unico Producto @hom669
        if (!empty($id_producto)) {
            $products->where('products.id', '=', $id_producto);
        }

        return $products->orderBy('products.description', 'ASC')->limit(200)->get();
    }

    public function getProviderWms($id_provider)
    {


        $provider = Subscriber::where('id', '=', $id_provider)->select('identification')->first();

        $provider_info = Contact::where('identification', '=', $provider->identification);

        return $provider_info->get();
    }

    /**
     * Este metodo actualiza la update_wsm(Consumido Po WMS) y date_update_wsm(Fecha de Consumo) para no ser tenido en cuenta posteriormente.
     *
     * @param id_producto Id del producto si es uno a uno el consumo
     * @return Products Collection con toda la informacion del producto actualizado.
     * @author Haider Oviedo @Hom669
     */
    public function updateProductsWms($id_producto = '', $action = 'A')
    {

        $current = Product::where('id', $id_producto)->first();

        // Actualiza Estado a True y Fecha Actual del Consumo de WMS para no ser tenido en cuenta posteriormente.
        if ($action == 'A') {
            $update = Product::where('id', $id_producto)->update([
                'update_wsm'      => true,
                'date_update_wsm' => now(),
            ]);
        } else {
            $update = Product::where('id', $id_producto)->update([
                'update_wsm'             => true,
                'date_update_wms_ultime' => now(),
            ]);
        }

        $products = Product::distinct('products.id')->select("products.id as id_product",
            "products.code",
            "products.provider",
            "products.barcode",
            "products.description",
            "products.presentation_packaging_buy",
            "lines.line_description",
            "lines.line_code",
            "lines_taxes.tax_id",
            "taxes.code as codigo_iva",
            "taxes.description as taxes_name",
            "taxes.percentage_sale",
            "products.width",
            "products.weight",
            "products.high",
            "products.rotation_classification",
            "products.alternate_code",
            "products.update_wsm")
            ->selectRaw("(CASE WHEN products.state = true THEN 'S' ELSE 'N' END) AS state")
            ->selectRaw("(CASE WHEN lines.tipe = 2 THEN 'S' ELSE 'N' END) AS type")
            ->join('lines', 'lines.id', '=', 'products.line_id')
            ->leftJoin('sublines', 'products.subline_id', '=', 'sublines.id')
            ->join('lines_taxes', 'lines_taxes.line_id', '=', 'lines.id')
            ->join('taxes', 'taxes.id', '=', 'lines_taxes.id')
            ->whereNull('products.deleted_at')
            ->where('taxes.code', '=', '10');
        if (!empty($id_producto)) {
            $products->where('products.id', '=', $id_producto);
        }

        return $products->orderBy('products.description', 'ASC')->get();
    }


    /**
     * Consultar información del inventario de productos para la app.
     *
     * @param Request $request Contiene los filtros a aplicar en la consulta
     * @return JsonResponse Contiene los productos con información de inventarios
     * @author Jhon García
     */
    public function appListInventoryProduct($request)
    {
        //tabla de parametro lista de precio
        $price_list = ParamTable::select('id', 'code_table')->where('code_table', 'LDP')->first();

        // parametro precio minimo de venta
        $minumum_price_list = Parameter:: select('id', 'paramtable_id', 'code_parameter')
            ->where('paramtable_id', $price_list->id)
            ->where('code_parameter', '002')
            ->first();

        return Product::select('id', 'id as product_id', 'code as product_code', 'description as product_description')
            ->whereHas('inventory.branchofficeWarehouse', function ($query) {
                $query->where('warehouse_code', '01');
            })
            ->with([
                'minimum_price' => function ($minimum_price) use ($minumum_price_list) {
                    $minimum_price->select('id', 'product_id', 'price_list_id', 'price')
                        ->where('price_list_id', $minumum_price_list->id);
                },
                'inventory'     => function ($inventory) {
                    $inventory->select(
                        'inventories.id',
                        'bw.id as branchoffice_warehouse_id',
                        'bw.warehouse_description as branchoffice_warehouse',
                        'b.id as branchoffice_id',
                        'b.name as branchoffice',
                        'product_id',
                        'stock',
                        'stock_values',
                        'average_cost',
                        'inventories.created_at',
                        'inventories.updated_at',
                        'location',
                        'available'
                    )
                        ->join('branchoffice_warehouses as bw', 'bw.id', '=', 'inventories.branchoffice_warehouse_id')
                        ->join('branchoffices as b', 'b.id', '=', 'bw.branchoffice_id')
                        ->where('bw.warehouse_code', '01');
                }
            ])
            ->where(function ($query) {
                $query->where('lock_sale', false);
                $query->orWhere('lock_sale', null);
            })
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })
            ->where(function ($query) {
                $query->orWhere('state', true);
                $query->orWhere('state', null);
            })
            ->when($request->date_from, function ($date_from) use ($request) {
                $date_from->where('updated_at', '>=', $request->date_from);
            })
            ->orderBy('product_description', 'ASC')
            ->get()
            ->toArray();
    }
}
