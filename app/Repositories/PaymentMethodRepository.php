<?php

namespace App\Repositories;

use App\Entities\PaymentMethod;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentMethodRepository
 * @package App\Repositories
 * @version August 15, 2019, 7:21 pm UTC
 *
 * @method PaymentMethod findWithoutFail($id, $columns = ['*'])
 * @method PaymentMethod find($id, $columns = ['*'])
 * @method PaymentMethod first($columns = ['*'])
*/
class PaymentMethodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'description',
        'parameter_id',
        'contact_id',
        'accounting_account',
        'commission_percentage',
        'authorization_number',
        'check_number',
        'expiration_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentMethod::class;
    }
    public function findAll($request){
        $payment=PaymentMethod::paginate($request->perPage);
        return $payment;
    }
}
