<?php

namespace App\Repositories;

use App\Entities\Discount;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LineRepository
 * @package App\Repositories
 * @version February 12, 2019, 4:29 pm UTC
 *
 * @method Line findWithoutFail($id, $columns = ['*'])
 * @method Line find($id, $columns = ['*'])
 * @method Line first($columns = ['*'])
*/
class DiscountRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parameter',
        'line_id',
        'brand_id',
        'percentage'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Discount::class;
    }

    public function createDiscount($request)
    {
        $data = [
            'parameter' => $request->client,
            'line_id'    => $request->line,
            'brand_id'   => $request->brand,
            'percentage' => $request->percentage
        ];
        
        return Discount :: create($data);
    }

    public function searchDiscounts()
    {
        return Discount :: with(['line','brand','parameter'])
        ->get();
    }

    public function searchDiscount($id)
    {
        return Discount :: where('id', $id) 
                        ->with(['line','brand'])
                        ->first();
    }

    public function updateDiscount($request)
    {
        $discount = Discount ::  where('id', $request->id)->first();
        $discount->parameter = $request->client;
        $discount->line_id = $request->line;
        $discount->brand_id = $request->brand;
        $discount->percentage = $request->percentage;
        $discount->save();
        return $discount;
    }

    public function deleteDiscount($id)
    {
        $discount = Discount ::  where('id', $id)->first();
        $discount->delete();

    }

}
