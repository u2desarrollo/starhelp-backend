<?php

namespace App\Repositories;

use App\Entities\ContactWarehouse;
use App\Entities\RequestHeader;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RequestHeaderRepository
 * @package App\Repositories
 * @version September 1, 2019, 3:50 am UTC
 *
 * @method RequestHeader findWithoutFail($id, $columns = ['*'])
 * @method RequestHeader find($id, $columns = ['*'])document
 * @method RequestHeader first($columns = ['*'])
 */
class RequestHeaderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'contacts_warehouse_id',
        'request_date',
        'request_type',
        'maintenance_type',
        'units',
        'direc_observation',
        'customer_observation',
        'provider_observation'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestHeader::class;
    }
    //Filtro para busqueda con fechas
    public function getRequestHeaders($request)
    {

        $requestHeader = RequestHeader::with(['contact', 'contactsWarehouse.seller', 'requestProducts'])
            ->when($request->date != null, function ($query) use ($request) {
                $query->whereBetWeen('request_date', [$request->date[0], $request->date[1]]);
            })
            ->when($request->requestType != null, function ($query) use ($request) {
                $query->where('request_type', $request->requestType);
            })
            ->when($request->status_request == 'true', function ($query) use ($request) {
                $query->where('status_request', 10);
            })
            ->when($request->status_request == 'false', function ($query) use ($request) {
                $query->where(function ($subquery) {
                    $subquery->where('status_request', '!=', 10);
                    $subquery->orWhereNull('status_request');
                });
            })
            ->when($request->contactB2B != null, function ($query) use ($request) {
                $query->where('contacts_warehouse_id', $request->contactB2B);
            })
            ->when(($request->warehouse != null && $request->warehouse !=''), function ($query) use ($request) {
                $query->whereIn('contacts_warehouse_id', $request->warehouse);
            })
            ->paginate($request->perPage);

        return $requestHeader;
    }


    public function findBiId($id)
    {
        $requestHeaders = RequestHeader::where('id', $id)
            ->with('requestProducts.product', 'contactsWarehouse')->first();

        return $requestHeaders;
    }


    public function update(array $request, $id)
    {
        $requestHeaders = RequestHeader::findOrfail($id);
        $requestHeaders->status_request = $request['status_request'];
        $requestHeaders->route_to_pickup = $request['route_to_pickup'];
        $requestHeaders->maintenance_type = $request['maintenance_type'];
        $requestHeaders->provider_observation = $request['provider_observation'];
        $requestHeaders->save();
        return $requestHeaders;
    }
}
