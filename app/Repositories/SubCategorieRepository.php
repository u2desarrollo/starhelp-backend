<?php

namespace App\Repositories;

use App\Entities\SubCategorie;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubCategorieRepository
 * @package App\Repositories
 * @version March 6, 2019, 9:49 pm UTC
 *
 * @method SubCategorie findWithoutFail($id, $columns = ['*'])
 * @method SubCategorie find($id, $columns = ['*'])
 * @method SubCategorie first($columns = ['*'])
*/
class SubCategorieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'categorie_id',
        'subcategorie_code',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubCategorie::class;
    }
    public function findAll($request){
        if ($request->filter_lines) {
            $subcategories = SubCategorie::select('*')
            ->where('categorie_id', $request->filter_categorie)
            ->orderBy('description','Asc')
            ->get();
        } else {
            $subcategories = SubCategorie::orderBy('description','Asc')->get();
        }
        return $subcategories;
    }
}
