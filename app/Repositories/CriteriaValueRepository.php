<?php

namespace App\Repositories;

use App\Entities\CriteriaValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CriteriaValueRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:41 pm UTC
 *
 * @method CriteriaValue findWithoutFail($id, $columns = ['*'])
 * @method CriteriaValue find($id, $columns = ['*'])
 * @method CriteriaValue first($columns = ['*'])
*/
class CriteriaValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'relation_id',
        'parameter_id',
        'criteria_type_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CriteriaValue::class;
    }
}
