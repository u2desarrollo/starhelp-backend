<?php

namespace App\Repositories;

use App\Entities\FixAssetAddition;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class FixAssetAdditionRepository
 * @package App\Repositories
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class FixAssetAdditionRepository extends BaseRepository
{

    public function model()
    {
        return FixAssetAddition::class;
    }

    public function findAll($request)
    {
        $pagination = $request->pagination ? (boolean) $request->pagination : false;
        $filterText = $request->filterText;
        $perPage    = $request->perPage;

        $fixAssetAdditions = FixAssetAddition::with([
            'fixAsset'
        ])
        ->where(function ($query) use ($filterText) {
            if ($filterText) {
                $query->orWhere(DB::raw('CAST(value AS TEXT)'), 'ILIKE', '%' . $filterText . '%');
                $query->orWhere('plate_number', 'ILIKE', '%' . $filterText . '%');
                $query->orWhere('description', 'ILIKE', '%' . $filterText . '%');
            }
        });

        return $pagination 
        ? $fixAssetAdditions->paginate($perPage)
        : $fixAssetAdditions->get();        
    }

    public function findById($id)
    {
        return FixAssetAddition::with(['fixAsset'])->find($id);     
    }

    public function update($data, $id)
    {
        FixAssetAddition::find($id)->update($data);
        return FixAssetAddition::find($id);
    }
}
