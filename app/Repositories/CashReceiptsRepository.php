<?php

namespace App\Repositories;

use App\Entities\CashReceiptsApp;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Common\BaseRepository;
use Tymon\JWTAuth\Facades\JWTAuth;

class CashReceiptsRepository
{

    public function __construct()
    {
    }

    // este metodo solo trae los recibos de caja de cierto vendedor ' el que inicie sesion'
    // con sus respectivas facturas a las que trata
    public static function getPaymentWithoutApplying()
    {
        $user = JWTAuth::parseToken()->toUser();
        $cashR = CashReceiptsApp::where('contact_id', $user->contact_id)->
        where('payment_applied_erp', 0)->orWhere('payment_applied_erp', null)
            ->with('contributionInvoicesApp')->get();
        return json_decode($cashR);
    }

    /**
     * Obtiene de la base de datos un listado de recibos de caja filtrados por un rango de fecha y descripción
     *
     * @param Request $request Contiene los posibles filtros a aplicar en la consulta
     * @return mixed $cashReceipts Contiene el listado de recibos de caja obtenido de base de datos
     * @author Jhon García
     */
    public function getCashReceiptsApp($request)
    {

        $user=Auth::user();

        if ($user->hasRole('Vendedor')) {
            $request->seller = $user->contact_id;
        }

        $cashReceipts = CashReceiptsApp::select('id', 'creation_date', 'consecutive', 'observations', 'warehouse_id', 'consignment_id')
            ->with([
                'warehouse:id,contact_id',
                'warehouse.contact:id,identification,name,surname',
                'paymentMethodsApp:id,cash_receipts_app_id,total_paid,parameter_id',
                'paymentMethodsApp.parameter:id,name_parameter',
                'contributionInvoicesApp:id,cash_receipts_app_id,document_id,total_paid,value_discount_prompt_payment',
                'contributionInvoicesApp.document:id,prefix,consecutive',
                'contributionInvoicesApp.document.receivable:id,document_id,invoice_balance'
            ])
            ->whereBetween('creation_date', [substr($request->filter_date[0], 0, 10) . ' 00:00:00', substr($request->filter_date[1], 0, 10) . ' 23:59:59'])
            ->whereHas('warehouse', function ($warehouse) use ($request) {
                $warehouse->whereHas('contact', function ($contact) use ($request) {
                    $contact->whereRaw('concat(identification, \' \', name, \' \', surname) ilike ?', ['%' . $request->customer . '%'])
                    ->when($request->seller,function($query) use ($request){
                        $query->where('seller_id',$request->seller);
                    });
                });
            })
            ->when(isset($request->filter_document) && $request->filter_document != '', function ($contributionInvoicesApp) use ($request){
                $contributionInvoicesApp->whereHas('contributionInvoicesApp', function($document) use ($request){
                    $document->whereHas('document', function($subquery) use ($request){
                        $subquery->where('consecutive', $request->filter_document);
                    });
                });
            })
            ->orderBy('creation_date', 'DESC')
            ->paginate(30);

        return $cashReceipts;
    }

    public function searchcollectionVisits($visits)
    {
        foreach ($visits as $key => $visit) {
            $collections_visit = CashReceiptsApp :: where ('contact_id', $visit->seller_id)
            ->where('warehouse_id', $visit->contact_warehouse_id)
            ->whereDate('creation_date', substr($visit->checkin_date, 0, 10))
            ->with(['paymentMethodsApp'])
            ->get();

            $total_collection= 0;
            foreach ($collections_visit as $key => $collection_visit) {
                foreach ($collection_visit->paymentMethodsApp as $key => $paymentMethodsApp) {
                    $total_collection += $paymentMethodsApp->total_paid;
                }
            }
            $visit->collections_visit= '$ ' . number_format($total_collection, 0, '', '.');
            $visit->collections_visit_wf= $total_collection;
            $visit->total_collections_visit = count($collections_visit);
        }
        return $visits;
    }
}
