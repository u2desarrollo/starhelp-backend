<?php

namespace App\Repositories;

use App\Entities\PriceContact;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PriceContactRepository
 * @package App\Repositories
 * @version August 13, 2019, 5:54 pm UTC
 *
 * @method PriceContact findWithoutFail($id, $columns = ['*'])
 * @method PriceContact find($id, $columns = ['*'])
 * @method PriceContact first($columns = ['*'])
*/
class PriceContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'branchoffice_id',
        'contact_id',
        'price',
        'previous_price',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PriceContact::class;
    }
    public function getPriceContact($request){
        $price_contact=PriceContact::with(['product','branchOffice','contact','user'])
        ->where('branchoffice_id',$request->branch)
        ->whereHas('contact',function($query) use($request){
            $query->where('name',$request->contact);
        })->get();
        return $price_contact;
    }
}
