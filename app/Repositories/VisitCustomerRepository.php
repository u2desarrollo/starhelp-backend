<?php

namespace App\Repositories;

use App\Entities\ContactWarehouse;
use App\Entities\VisitCustomer;
use App\Http\Requests\ApiRequests\UpdateVisitCustomerAPIRequest;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class VisitCustomerRepository
 *
 * @package App\Repositories
 * @version June 5, 2019, 5:22 pm UTC
 * @method VisitCustomer findWithoutFail($id, $columns = ['*'])
 * @method VisitCustomer find($id, $columns = ['*'])
 * @method VisitCustomer first($columns = ['*'])
 */
class VisitCustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seller_id',
        'contact_id',
        'contact_warehouse_id',
        'visit_type',
        'latitude_checkin',
        'longitude_checkin',
        'latitude_checkout',
        'longitude_checkout',
        'test',
        'distance',
        'checkin_date',
        'checkout_date',
        'observation',
        'photo',
        'checkout_ok',
        'sales_management',
        'collection',
        'training'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitCustomer::class;
    }

    public function findSellers($request)
    {
        $sellers = VisitCustomer::get();
        /*where('seller_id', 'ILIKE', '%' . $request->filter . '%')
            ->orderBy($request->sortBy, $request->sort)
            ->paginate($request->perPage);*/
        return $sellers;
    }

    public function create(array $visit)
    {
        // original de propartes
        $create = VisitCustomer::create([
            'seller_id'            => $visit['iduser'],
            'contact_id'           => $visit['cod_ter'],
            'contact_warehouse_id' => $visit['cod_bodega'],
            'visit_type'           => 847,
            'distance'             => isset($visit['distancia']) ? $visit['distancia'] : 1,
            'latitude_checkin'     => $visit['latitud_checkin'],
            'longitude_checkin'    => $visit['longitud_checkin'],
            'sales_management'     => $visit['gestion'],
            'collection'           => $visit['cobranza'],
            'training'             => $visit['capacitacion'],
            'checkin_date'         => date('Y-m-d H:i:s')
        ]);

        $contact_warehouse = ContactWarehouse::find($visit['cod_bodega']);

        if ($contact_warehouse && (empty($contact_warehouse->latitude) || empty($contact_warehouse->length))) {
            $contact_warehouse->latitude = $visit['latitud_checkin'];
            $contact_warehouse->length = $visit['longitud_checkin'];
            $contact_warehouse->save();
        }

        $created = ['segven_cod_segven' => $create->id];

        return $created;
    }

    public function checkout($visit, $id)
    {
        $update = VisitCustomer::findOrFail($id);

        $update->update([
            'latitude_checkout'  => $visit['latitud_checkout'],
            'longitude_checkout' => $visit['longitud_checkout'],
            'observation'        => isset($visit['observaciones']) ? $visit['observaciones'] : '',
            'checkout_ok'        => $visit['checkout_ok'],
            'checkout_date'      => date('Y-m-d H:i:s')
        ]);


        $updated = ['segven_cod_usersd' => $update->seller_id, 'segven_fec_evento' => $update->checkout_date];

        return $updated;
    }

    public function getMarkerCoordinate($request)
    {
        $fullData = [];
        $i = 0;
        $visitCustomers = VisitCustomer::select("id", "seller_id", "contact_warehouse_id", "contact_id", "latitude_checkin", "longitude_checkin", "checkin_date")
            ->where('latitude_checkin', '<>', 0)
            ->where('longitude_checkin', '<>', 0)
            ->where('seller_id', $request['id'])
            ->whereBetween('checkin_date', [$request['date'] . " 00:00", $request['date'] . " 23:59"])
            ->with('contactsWarehouse', 'contact')
            ->get();
        foreach ($visitCustomers as $visitCustomer) {
            $lat = (float)$visitCustomer->latitude_checkin;
            $lng = (float)$visitCustomer->longitude_checkin;
            $coordinates = [
                'lat' => $lat,
                'lng' => $lng
            ];
            if ($i == 0) {

                $arreglo = [
                    'label'    => "{$i}",
                    'position' => $coordinates,
                    'icon'     => "gmap",
                ];
            } else {
                $arreglo = [
                    'label'    => "{$i}",
                    'infoText' => $visitCustomer->contactsWarehouse->description . '
                        ' . $visitCustomer->checkin_date,
                    'position' => $coordinates,
                ];
            }
            $fullData[$i] = new \ArrayObject($arreglo);
            $i += 1;
        }
        return $fullData;

    }

    public function searchVisitsSeller($id, $date)
    {
        $visits = VisitCustomer::where('seller_id', $id)
            ->whereDate('checkin_date', $date)
            ->where('checkout_date', '<>', null)
            ->with(['contact'])
            ->orderBy('checkin_date')
            ->get();
        return $visits;
    }
}
