<?php

namespace App\Repositories;

use App\Entities\TemplateAccounting;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentProduct;
use App\Entities\Document;

/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class TemplateAccountingRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TemplateAccounting::class;
    }


    public function createAccount($data)
    {
        return TemplateAccounting::insert($data);
    }

    /**
     * Método para filtrar cuentas de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $cuentas colección de cuentas
     * @author santaigo torres
     */
    public function findAll($request){
    	if ($request->paginate == 'true') {

	        // $accounts = Account::where('code', 'ILIKE', '%' . $request->filter . '%')
            //     ->orWhere('name', 'ILIKE', '%' . $request->filter . '%')
            //     ->with( 'parameter_level',
            //             'parameter_group'
            //     )
	        //     ->orderBy($request->sortBy, $request->sort)
	        //     ->paginate($request->perPage);
    	}else{
    		$accounts = TemplateAccounting::orderBy('id', 'ASC')->get();
    	}

    	return $accounts;

    }

    public function create($data)
    {
        return TemplateAccounting::create([
            'model_id' => $data['model_id'],
            'concept_id' => $data['concept_id'],
            'according_id' => $data['according_id'],
            'account_id' => $data['account_id'],
            'departure_square' => $data['departure_square'],
            'debit_credit' => $data['debit_credit'],
            'order_accounting' => $data['order_accounting'],
        ]);
    }
}
