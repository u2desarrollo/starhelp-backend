<?php

namespace App\Repositories;

use App\Entities\CustomerPromotion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CustomerPromotionRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:38 pm UTC
 *
 * @method CustomerPromotion findWithoutFail($id, $columns = ['*'])
 * @method CustomerPromotion find($id, $columns = ['*'])
 * @method CustomerPromotion first($columns = ['*'])
*/
class CustomerPromotionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_customer_id',
        'contact_warehouse_id',
        'promotion_group_rule_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerPromotion::class;
    }
}
