<?php

namespace App\Repositories;

use App\Entities\Module;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ModuleRepository
 * @package App\Repositories
 * @version March 6, 2019, 2:00 pm UTC
 *
 * @method Module findWithoutFail($id, $columns = ['*'])
 * @method Module find($id, $columns = ['*'])
 * @method Module first($columns = ['*'])
*/
class ModuleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Module::class;
    }
}
