<?php

namespace App\Repositories;

//Service

//Entities
use App\Entities\Line;
use App\Entities\Product;
use App\Entities\DataSheet;
use App\Entities\DataSheetLine;
use App\Entities\DataSheetProduct;

//Others
use Response;
use InfyOm\Generator\Common\BaseRepository;


/**
 * Class DataSheetRepository
 * @package App\Repositories
 */
class DataSheetRepository extends BaseRepository
{


    /**
     * Configure the Model
     **/
    public function model()
    {
        return DataSheet::class;
    }

    public function findForId($id)
    {
        return DataSheet::find($id);
    }

    public function findForLineId($line_id)
    {
        $line = Line::find($line_id);

        $data = $line->dataSheets;
        foreach ($data as $key => $value) {
            $value->type_value = $value->type_value_name ? $value->type_value_name->name_parameter :null;
            $value->required = $value->pivot->required;
            $value->order = $value->pivot->order;
        }
        return $data;
    }

    public function findForInternalName($internalName)
    {
        $dataSheet = DataSheet::where('internal_name', $internalName)->get();
        return $dataSheet;
    }

    public function findForInternalNameArray($ArrayinternalName)
    {
        $dataSheet = DataSheet::whereIn('internal_name', $ArrayinternalName)->select('id','internal_name')->get();
        return $dataSheet;
    }

    public function setAplication($row, $product)
    {
        // este array sirve para tener una relacion entre el data_sheet y el row del data sheel
        // key de $keys_row es el internal name
        // value es la key del $row
        $keys_row = [
            'APLICACI_N'    => 'Aplicacin',
            'VOLUMEN:'      => 'volumetria',
            'PESO'          => 'Peso ',
            'LARGO:'        => 'Largo ',
            'ALTO:'         => 'Alto ',
            'ANCHO:'        => 'Ancho',
            'UNDS_PALLET'   => 'unds por pallet'
        ];
        // ficha de datos
        $data_shett_product = [];
        // consultamos las fichas de datos
        $data_sheets = DataSheet::select('id', 'internal_name')->whereIn('internal_name', array_keys($keys_row))->get();
        foreach ($data_sheets as $key => $data_sheet) {
            $value = '';
            // verificamos si la ficha de datos ya pertenece a la linea
            $data_sheet_line = DataSheetLine :: where('data_sheet_id', $data_sheet->id)->where('line_id', $product->line_id)->first();
            if (is_null($data_sheet_line)) {
                // creamos la ficha de datos en la linea si aun no pertenece
                $data_sheet_line = DataSheetLine::create([
                    'line_id' => $product->line_id,
                    'data_sheet_id' => $data_sheet->id
                ]);
            }
            
            if (isset($row[$keys_row[$data_sheet->internal_name]])) {

                $value = trim($row[$keys_row[$data_sheet->internal_name]]) != ' ' && trim($row[$keys_row[$data_sheet->internal_name]]) != 'N/A' && trim($row[$keys_row[$data_sheet->internal_name]]) != '#N/A'? utf8_decode(trim($row[$keys_row[$data_sheet->internal_name]])) : '';

                // si la llave es ANCHO ALTO o LARGO se debe convertir el valor a cm
                if ($data_sheet->internal_name == 'ANCHO:' || $data_sheet->internal_name == 'ALTO:' || $data_sheet->internal_name == 'LARGO:') {
                    $value = $value != '' && $value != '#N/A' && $value != '#N/D' ? str_replace(',', '.', $value) / 10 : '';
                }

                $data_shett_product[]= [
                    "value" => $value,
                    "data_sheet_id" => $data_sheet->id
                ];
            }

        }

        return DataSheetProduct :: updateOrCreate(
            [
                'product_id'=>  $product->id
            ],
            [
                'product_id'=>  $product->id,
                'value'     =>  $data_shett_product
            ]
        );

        

    }
}