<?php

namespace App\Repositories;

use App\Entities\Subscriber;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubscriberRepository
 * @package App\Repositories
 * @version December 18, 2018, 7:54 pm UTC
 *
 * @method Subscriber findWithoutFail($id, $columns = ['*'])
 * @method Subscriber find($id, $columns = ['*'])
 * @method Subscriber first($columns = ['*'])
*/
class SubscriberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'license_id',
        'identification',
        'check_digit',
        'identification_type',
        'name',
        'short_name',
        'email',
        'web_page',
        'class_person',
        'taxpayer_type',
        'tax_regimen',
        'id_legal_representative',
        'name_legal_representative',
        'company_type',
        'logo',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subscriber::class;
    }

    /**
     * Método para obtener el listado de contacts de acuerdo a los parámetros recibidos
     * @param $request contiene los parámetros para filtrar
     * @return $contacts contiene una collección de de contacts
     */
    public function findAll($request)
    {
        if ($request->paginate == 'true') {
            $subscriber = Subscriber::where('name', 'ILIKE', '%' . $request->filter . '%' )
                ->orWhere(function($query) use ($request){
                    if(is_numeric($request->filter)){
                        $query->where('id', '=', $request->filter );
                    } else {
                        return false;
                    }
                })
                ->orWhere('identification', 'ILIKE', '%' . $request->filter . '%')
                ->orderBy($request->sortBy, $request->sort)
                ->paginate($request->perPage);
        } else {
            $subscriber = Subscriber::all();
        }
        return $subscriber;
    }

    public function findForId($id){

        $subscriber = Subscriber::where('id', $id)
            ->with(['configuration','branchOffice', 'document'])
            ->whereHas('branchOffice', function ($query) use ($id) {
                $query->where('main', '=', 1);
            })
            ->first();

        return $subscriber;

    }


}
