<?php

namespace App\Repositories;

use App\Entities\LineTax;
use InfyOm\Generator\Common\BaseRepository;

class LineTaxRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'line_id',
		'tax_id'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return LineTax::class;
	}

	public function findForLineId($request) {
		$taxes = LineTax::where('line_id', $request->id)
		->with([
			'tax'
		])->get();

		return $taxes;
	}

	public function create(array $request) {
		
		$tax = LineTax::updateOrCreate([
			'line_id' => $request['line_id'],
			'tax_id' => $request['tax']['id']
		], $request);
		return $tax;
	}
}
