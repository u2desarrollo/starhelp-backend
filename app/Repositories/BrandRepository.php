<?php

namespace App\Repositories;

use App\Entities\Brand;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BrandRepository
 * @package App\Repositories
 * @version March 4, 2019, 7:42 pm UTC
 *
 * @method Brand findWithoutFail($id, $columns = ['*'])
 * @method Brand find($id, $columns = ['*'])
 * @method Brand first($columns = ['*'])
*/
class BrandRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'brand_code',
        'description',
        'image',
        'provider_id',
        'maker_id',
        'lock_buy',
        'block_sale',
        'thumbnails',
        'deploy_sales_force_app',
        'deploy_b2c',
        'deploy_b2b',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Brand::class;
    }

    /**
     * Método para filtrar brands de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $brands colección de brands
     */
    public function findAll($request){
        $lines = $request->lines;

        if ($request->paginate == 'true') {
            $brands = Brand::where('brand_code', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere('description', 'ILIKE', '%' . $request->filter . '%')
                ->orderBy($request->sortBy, $request->sort)
                ->paginate($request->perPage);
        } else {
            $brands = Brand::orderBy('description','ASC');

            if ($lines && is_array($lines)) { 
                $brands->whereHas('products', function($products) use ($lines){
                    $products->whereHas('line', function($query) use ($lines){
                        $query->whereIn('id', $lines);                        
                    });
                });
            } 

            $brands = $brands->orderBy('description','ASC')->get();
            
        }

        return $brands;
    }

    public function findForId($id){
        $brands = Brand::where('id', $id)->first();
        return $brands;
    }
}
