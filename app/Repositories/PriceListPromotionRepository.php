<?php

namespace App\Repositories;

use App\Entities\PriceListPromotion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PriceListPromotionRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:40 pm UTC
 *
 * @method PriceListPromotion findWithoutFail($id, $columns = ['*'])
 * @method PriceListPromotion find($id, $columns = ['*'])
 * @method PriceListPromotion first($columns = ['*'])
*/
class PriceListPromotionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parameter_id',
        'promotion_group_rule_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PriceListPromotion::class;
    }
}
