<?php

namespace App\Repositories;

use App\Entities\JobStar;
use App\Entities\JobInteraction;

use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use Tymon\JWTAuth\Facades\JWTAuth;

class JobStarRepository extends BaseRepository
{

    public function model()
    {
        return JobStar::class;
    }

    /**
     * Método para traer los registros segun los filtros
     * 
     * @param  $request contiene los parametros del request
     * @return $jobStars Collection
     * @author Kevin Galindo
     */
    public function findAll($request) {

        $paginate   = $request->paginate;
        $filterText = $request->filter;
        $perPage    = $request->perPage ? $request->perPage : 10;
    
        $jobStars = JobStar::
        when($filterText, function ($query) use ($filterText) {
            $query->where('name', 'ilike', '%'.$filterText.'%');
            $query->orWhere('description', 'ilike', '%'.$filterText.'%');
        })
        ->with([
            'userCreation' => function ($query) {
                $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
            },
            'files',
            'members' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id', 'username');
                    },
                ]);
            },
            'interactions' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id', 'username');
                    },
                ]);
            },
        ])
        ->where('state', true);
        
        if ($request->paginate === 'true') {
            return $jobStars->paginate($request->perPage);
        } 

        return $jobStars->get();
        
    }

    /**
     * Método para traer los registros segun los filtros
     * y el usuario.
     * 
     * @param  $request contiene los parametros del request
     * @return $jobStars Collection
     * @author Kevin Galindo
     */
    public function findAllByUser($request) {
    
        $user = JWTAuth::parseToken()->toUser();

        $jobStars = JobStar::
        with([
            'userCreation' => function ($query) {
                $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
            },
            'files',
            'members' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
                    },
                ]);
            },
            'interactions' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id', 'username');
                    },
                ]);
            },
        ])
        ->where(function ($query) {
            $query->orWhere('state', true);
            $query->orWhereDay('closing_date', date('d'));
        })
        ->whereHas('members', function ($query) use ($user) {
           $query->where('user_id', $user->id);
        })
        ->get();

        return $jobStars;        
    }

    /**
     * 
     * @param  $request contiene los parametros del request
     * @return $jobStars Collection
     * @author Kevin Galindo
     */
    public function findInteractionJobStar($id) {
    
        $jobInteraction = JobInteraction::with([
            'files',
            'user' => function ($query) {
                $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
            },
            'jobStar'
        ])->find($id);

        return $jobInteraction;        
    }

    /**
     * 
     * @param  $request contiene los parametros del request
     * @return $jobStars Collection
     * @author Kevin Galindo
     */
    public function createInteractionJobStar($data) {
    
        $jobInteraction = JobInteraction::create($data);
        return $jobInteraction;        
    }

    /**
     * Método para traer un registros segun el id
     * 
     * @param  $id
     * @return $jobStar JobStar
     * @author Kevin Galindo
     */
    public function findById($id) {
        $jobStar = JobStar::
        with([
            'userCreation' => function ($query) {
                $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
            },
            'files',
            'members' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id');
                    },
                ]);
            },
            'interactions' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->select('id', 'name', 'email', 'surname', 'photo', 'contact_id', 'username');
                    },
                    'files'
                ]);
            },
        ])
        ->where('state', true)
        ->find($id);

        return $jobStar;
    }
    
    /**
     * Método para cambiar el estado a cerrado 
     * de un registro segun el id
     * 
     * @param  $id
     * @return $jobStar JobStar
     * @author Kevin Galindo
     */
    public function close($id) {
        $jobStar = $this->findById($id);

        if ($jobStar) {
            $jobStar->state = false;
            $jobStar->closing_date = date('Y-m-d H:i:s');
            $jobStar->save();
        }

        return $jobStar;
    }

    /**
     * Metodo para guardar los integrantes de forma masiva.
     * 
     * @param  $id del JobStar
     * @param  $data informacion del
     * @return $jobStar JobStar
     * @author Kevin Galindo
     */
    public function addMembers($id, $data) {
        $jobStar = $this->findById($id);

        if ($jobStar) {
            $jobStar->members()->sync($data);
            $jobStar = $this->findById($id);
            return [
                'data' => $jobStar->members,
                'code' => 200
            ];
        }

        return [
            'data' => 'No hay registro.',
            'code' => 404
        ];
    }
}
