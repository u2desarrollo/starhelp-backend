<?php

namespace App\Repositories;

use App\Entities\Company;
use InfyOm\Generator\Common\BaseRepository;

class CompanyRepository extends BaseRepository
{

    public function model()
    {
        return Company::class;
    }

    public function findAll($request)
    {
        $paginate = $request->paginate && $request->paginate === 'true' ? true : false;
        $perPage  = $request->perPage ? $request->perPage : 50;
        $filter   = $request->filter ? $request->filter : null;
        
        $cases = Company::with(['areas', 'responsibles'])
        ->when($filter, function ($query) use ($filter) {
            $arrayFilters = explode(' ', $filter);
            if (count($arrayFilters) > 0) {
                foreach ($arrayFilters as $key => $value) {
                    $query->where(function ($query) use ($value) {
                        $query->where('name', 'ILIKE', '%'.$value.'%');
                    });
                }
            }
        })
        ->orderBy('created_at', 'ASC');

        return [
            'code' => '200',
            'data' => $paginate ? 
            $cases->paginate($perPage) :
            $cases->get()
        ];
    }

}
