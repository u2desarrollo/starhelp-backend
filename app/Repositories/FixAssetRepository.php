<?php

namespace App\Repositories;

use App\Entities\FixAsset;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class FixAssetRepository
 * @package App\Repositories
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class FixAssetRepository extends BaseRepository
{

    public function model()
    {
        return FixAsset::class;
    }

    public function findAll($request)
    {
        $pagination = $request->pagination ? (boolean) $request->pagination : false;
        $filterText = $request->filterText;
        $perPage    = $request->perPage;

        $fixAssets = FixAsset::with([
            'activeGroup',
            'provider',
            'invoiceDocument',
            'recordAccount',
            'accountAccumulatedDepreciation',
            'accountDepreciationExpense',
            'paramClass',
            'city',
            'branchoffice',
            'costCenter',
            'contactResponsible',
            'contactSale',
            'derecognizedType',
            'derecognizedDocument',
            'maintenanceProvider',
            'policyProvider',
            'additions',
        ])
        ->where(function ($query) use ($filterText) {
            if ($filterText) {
                $query->orWhere(DB::raw('CAST(consecut AS TEXT)'), 'ILIKE', '%' . $filterText . '%');
                $query->orWhere('plate_number', 'ILIKE', '%' . $filterText . '%');
                $query->orWhere('description', 'ILIKE', '%' . $filterText . '%');
            }
        })
        ->orderBy('description', 'ASC');

        return $pagination 
        ? $fixAssets->paginate($perPage)
        : $fixAssets->get();        
    }

    public function findById($id)
    {
        return FixAsset::with([
            'activeGroup',
            'provider',
            'invoiceDocument',
            'recordAccount',
            'accountAccumulatedDepreciation',
            'accountDepreciationExpense',
            'paramClass',
            'city',
            'branchoffice',
            'costCenter',
            'contactResponsible',
            'contactSale',
            'derecognizedType',
            'derecognizedDocument',
            'maintenanceProvider',
            'policyProvider',
            'additions',
        ])
        ->find($id);     
    }

    public function getLastConsecutiveSuggested()
    {
        $consecutive = FixAsset::max('consecut');

        return [
            'consecut' => $consecutive ? ++$consecutive : 1000
        ];
    }

    public function update($data, $id)
    {
        FixAsset::find($id)->update($data);
        return FixAsset::find($id);
    }
}
