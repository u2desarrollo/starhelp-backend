<?php

namespace App\Repositories;

use App\Entities\ContactDocument;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactDocumentRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:27 pm UTC
 *
 * @method ContactDocument findWithoutFail($id, $columns = ['*'])
 * @method ContactDocument find($id, $columns = ['*'])
 * @method ContactDocument first($columns = ['*'])
*/
class ContactDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'document_type_id',
        'path',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactDocument::class;
    }
}
