<?php

namespace App\Repositories;

use App\Entities\Account;
use App\Entities\Parameter;
use App\Entities\BalancesForAccount;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentTransactions;
use App\Entities\Document;
use Carbon\Carbon;

class BalanceForAccountRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocumentTransactions::class;
    }

    public function findAll($request)
    {
    }

    public function balanceForAccount($request)
    {
        
        $balance_for_accounts = Account::
        selectRaw("parameters.name_parameter as level,accounts.code,accounts.name,fn_previous_balance(accounts.id,balances_for_accounts.year_month),(CASE WHEN balances_for_accounts.debits_month is null THEN 0 ELSE balances_for_accounts.debits_month END) AS debits_month,(CASE WHEN balances_for_accounts.credits_month is null THEN 0 ELSE balances_for_accounts.credits_month END) AS credits_month,(CASE WHEN balances_for_accounts.final_balance is null THEN 0 ELSE balances_for_accounts.final_balance END) AS final_balance,accounts.debit_credit")
        ->leftJoin('balances_for_accounts', 'accounts.id', '=', 'balances_for_accounts.account_id')
        ->join('parameters', 'parameters.id', '=', 'accounts.level');
         if(!empty($request->year_month)){
            $balance_for_accounts->whereRaw('(year_month = '."'".$request->year_month."'".' AND year_month is not null)');
        }
        if(!empty($request->level)){
            $balance_for_accounts->whereRaw('(accounts.level = '.$request->level."'");
        }
        if(!empty($request->account_from) && !empty($request->account_up)){
            $balance_for_accounts->whereRaw('(accounts.code >='."'". $request->account_from."'".' AND accounts.code <='."'".$request->account_up."')");
        }
        if(!empty($request->filter_text) && !empty($request->filter_text)){
            $balance_for_accounts->whereRaw('(accounts.code LIKE '."'%". $request->filter_text."%'".' OR accounts.name LIKE '."'%".$request->filter_text."%' OR parameters.name_parameter LIKE '%".$request->filter_text."%')");
        }

        

        $balance_for_accounts->orderBy('accounts.code');
        return $balance_for_accounts->get();


    }

    public function stateFinanze($request)
    {
        if(isset($request->filter_text)){
            $accounts = Account::select('id','code','name')->where("code","ilike","%".$request->filter_text."%")->orWhere("name","ilike","%".$request->filter_text."%")->orderBy('code')->get();
        }else{
            $accounts = Account::select('id','code','name')->orderBy('code')->get();
        }

        $month_act = date("m");

        $balance_for_accounts = BalancesForAccount::select('final_balance','year_month','account_id')->selectRaw('substr(year_month,5,6) as mes')->whereRaw('substr(year_month,1,4) ='."'".$request->year."'")->whereRaw('substr(year_month,5,6) <='."'".$month_act."'")->get();

        $accounts->map(function ($account) use($balance_for_accounts) {
 
            $account->cuentasformes = $balance_for_accounts->where("account_id","=",$account->id); 
            
        });

        return $accounts;


    }

    


}
