<?php

namespace App\Repositories;

use App\Entities\Line;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LineRepository
 * @package App\Repositories
 * @version February 12, 2019, 4:29 pm UTC
 *
 * @method Line findWithoutFail($id, $columns = ['*'])
 * @method Line find($id, $columns = ['*'])
 * @method Line first($columns = ['*'])
*/
class LineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subscriber_id',
        'line_code',
        'line_description',
        'short_description',
        'lock_buy',
        'block_sale',
        'margin_cost',
        'calculates_sale_price',
        'inventories_account',
        'account_cost',
        'sales_account',
        'barcode',
        'brand',
        'sub_brand',
        'line',
        'subline',
        'category',
        'subcategory',
        'lot',
        'serial',
        'type',
        'Photos',
        'owner',
        'maker',
        'registration_invima',
        'exp_date_invima',
        'generic',
        'year',
        'conformity_certificate',
        'exp_date_certified',
        'weight',
        'volume',
        'height',
        'color',
        'old_product_code',
        'new_product_code',
        'commission_portion',
        'mg_of_utility',
        'contact_income',
        'reduction_management',
        'high_price',
        'high_risk',
        'refrigerated',
        'type_rotation',
        'handling_formula',
        'width',
        'outline',
        'rin',
        'utqg',
        'load_index',
        'speed_index',
        'cum_code',
        'sgsss_code',
        'application_unit',
        'number_applications',
        'pharmacy_code',
        'concentration',
        'controlled_sale',
        'belongs_to_pos',
        'purchase_present_packaging',
        'purchase_quantity_present',
        'local_purchase',
        'observation_buy',
        'present_sale_packaging',
        'sale_quantity_packaging',
        'observation_of_sale',
        'channel_point_sale',
        'channel_amazon',
        'channel_b2c',
        'channel_b2b',
        'prod_desc_config',
        'tipe',
        'data_sheet',
        'data_sheet_link',
        'video_link',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Line::class;
    }

    /**
     * Método para filtrar lines de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $lines colección de lines
     */
    public function findAll($request){
    	if ($request->paginate == 'true') {

	        $lines = Line::where('line_code', 'ILIKE', '%' . $request->filter . '%')
	            ->orWhere('line_description', 'ILIKE', '%' . $request->filter . '%')
	            ->orWhere('short_description', 'ILIKE', '%' . $request->filter . '%')
	            ->orderBy($request->sortBy, $request->sort)
	            ->paginate($request->perPage);
    	}else{
    		$lines = Line::orderBy('line_description', 'Asc')->get();
    	}

    	return $lines;

    }

    public function findForId($id){
        $lines = Line::where('id', $id)
        ->with([
            "inventorieAccount",
            "costAccount",
            "saleAccount",
            "devolSaleAccount"
        ])
        ->first();
        return $lines;
    }

    public function create(array $request)
    {
        unset($request['act_prod_conf']);

        $line=Line::UpdateOrCreate($request,$request);
        return  $line;
    }

    /**
     * funcion para obtener o crear la linea
     * @santiago Torres
     */
    public function getLine($row)
    {

        $line = Line :: where('line_description', trim($row["Linea "]))->first();
        
        if (is_null($line)) {
            // creacion de linea

            // consultamos el codigo mas grande
            $max_code = Line :: max('line_code');
            // sumamos 10
            $max_code += '010';
            // le concatenamos un 0 ya que la suma retorna un entero
            $new_code = strlen($max_code) == 2 ?  '0'.$max_code : $max_code ;


            $line = Line :: create([
                'subscriber_id' => 3,
                'line_code' => $new_code,
                'line_description'=>trim($row["Linea "])
            ]);
        }
        return $line;
    }
}
