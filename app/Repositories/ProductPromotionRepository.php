<?php

namespace App\Repositories;

use App\Entities\ProductPromotion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductPromotionRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:39 pm UTC
 *
 * @method ProductPromotion findWithoutFail($id, $columns = ['*'])
 * @method ProductPromotion find($id, $columns = ['*'])
 * @method ProductPromotion first($columns = ['*'])
*/
class ProductPromotionRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'promotions_id',
		'product_id',
		'mimnimum_cant',
		'discount',
		'pay_soon',
		'cash_payment',
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return ProductPromotion::class;
	}

	// pendiente por establecer metyodos
}
