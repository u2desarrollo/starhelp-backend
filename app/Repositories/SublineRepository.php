<?php

namespace App\Repositories;

use App\Entities\Subline;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SublineRepository
 * @package App\Repositories
 * @version February 12, 2019, 4:29 pm UTC
 *
 * @method Subline findWithoutFail($id, $columns = ['*'])
 * @method Subline find($id, $columns = ['*'])
 * @method Subline first($columns = ['*'])
*/
class SublineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'line_id',
        'subline_code',
        'subline_description',
        'short_description',
        'lock_buy',
        'block_sale',
        'margin_cost',
        'calculates_sale_price',
        'inventories_account',
        'account_cost',
        'sales_account'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subline::class;
    }

    public function findAll($request){

        $lines = $request->lines;

        if ($request->all()) {

            if (!is_array($lines)) {
                $lines = [$lines];
            }

           // $sublines = Subline::where('line_id', $request->line)
            $sublines = Subline:://select('*')
            with([
                "inventorieAccount",
                "costAccount",
                "saleAccount",
                "devolSaleAccount"
            ])
            //->where('line_id', $request->filter_lines)
            //->
            ->whereIn('line_id', $lines)
           /*  ->when(isset($request->filter_brands) && is_array($request->filter_brands),
                function ($query) use ($request) {
                    return $query->whereHas('product', function ($query) use ($request){
                        $query->whereIn('brand_id', $request->filter_brands);
                    });
                }
            ) */
           /*  ->with([
                'line' => function ($line) use ($request){
                    $line->where('line_code', $request->line);
                    // ->orWhere('line_id', $request->line);
                }
            ]) */
           /*  ->whereHas('line', function ($line) use ($request){
                $line->where('line_code', $request->line);
                //->orWhere('line_', $request->line);
            }) */
            ->orderBy('subline_description','ASC')
            ->get();
        } else {
            $sublines = Subline::orderBy('subline_description','ASC')->get();
        }

        return $sublines;
    }
  public function create(array $request)
  {
    $sublines = new Subline();
    $sublines->fill($request);
    $sublines->save();
    return $sublines;
  }

    /**
     * obtener sublinea para carga masiva de productos colrecambios
     * @author Santiago Torres
     */
    public function getSubLine($row, $line)
    {
        $subline = Subline :: where('subline_description', utf8_decode(trim($row["Sub-linea "])))->first();
        if (is_null($subline)) {
            // creacion de sublinea

            // consultamos el codigo mas grande
            $max_code = Subline :: max('subline_code');
            // sumamos 10
            $max_code += '5';
            // le concatenamos un 0 ya que la suma retorna un entero
            $new_code = $max_code < 10 ? '00'.$max_code : '0'.$max_code;

            $subline = Subline :: create([
                'line_id'               => $line->id,
                'subline_code'          => $new_code,
                'subline_description'   => utf8_decode(trim($row["Sub-linea "]))
            ]);
        }
        return $subline;
    }
}
