<?php

namespace App\Repositories;

use App\Entities\ProductTechnnicalData;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductTechnnicalDataRepository
 * @package App\Repositories
 * @version March 26, 2019, 5:56 pm UTC
 *
 * @method ProductTechnnicalData findWithoutFail($id, $columns = ['*'])
 * @method ProductTechnnicalData find($id, $columns = ['*'])
 * @method ProductTechnnicalData first($columns = ['*'])
*/
class ProductTechnnicalDataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'link_manufacturer_one',
        'link_manufacturer_two',
        'weight',
        'volume',
        'size',
        'color',
        'year_model',
        'series',
        'link_data_sheet',
        'products_technical_datacol',
        'link_video',
        'generic',
        'used',
        'invima_registry',
        'expiration_date_invima',
        'width',
        'profile',
        'rin',
        'load_index',
        'velocity_index',
        'utqg',
        'runflat',
        'lt',
        'code_cum',
        'code_secretary_of_health',
        'aplication_unit',
        'number_aplications',
        'pharmacological_indication_code',
        'concentration',
        'pharmaceutical_form',
        'form_unit',
        'number_of_doses',
        'ium_code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductTechnnicalData::class;
    }
}
