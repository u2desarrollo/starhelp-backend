<?php

namespace App\Repositories;

use App\Entities\PayReceivable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PayReceivableRepository
 * @package App\Repositories
 * @version May 30, 2019, 9:42 pm UTC
 *
 * @method PayReceivable findWithoutFail($id, $columns = ['*'])
 * @method PayReceivable find($id, $columns = ['*'])
 * @method PayReceivable first($columns = ['*'])
*/
class PayReceivableRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'documents_balance_id',
		'vouchertype_id',
		'voucher_number',
		'pay_date',
		'pay_way',
		'applied_value'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return PayReceivable::class;
	}
}
