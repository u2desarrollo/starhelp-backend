<?php

namespace App\Repositories;

use App\Entities\Contact;
use App\Entities\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use InfyOm\Generator\Common\BaseRepository;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserRepository
 *
 * @package App\Repositories
 * @version February 14, 2019, 9:49 pm UTC
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function findAll($request)
    {
        if (isset($request->paginate) && $request->paginate == 'true') {
            $users = User::whereRaw('concat(name, \' \', surname, \' \', email) ilike ?', ['%' . $request->filter . '%'])
                ->withTrashed()
                ->orderBy($request->sortBy, $request->sort)
                ->paginate($request->perPage);
        } else {
            $users = User::where('subscriber_id', $request->subscriber_id)
                ->orderBy('name', 'Asc')
                ->get();
        }

        return $users;
    }

    /**
     * Method to create a user and assign modules and subscribers
     *
     * @param array $request
     * @return mixed
     * @throws \Exception
     */
    public function create(array $request)
    {
        try {
            DB::beginTransaction();

            $user = User::create($request);
            $user->subscribers()->attach($request['subscribers']);
            $user->modules()->attach($request['modules']);
            $role = Role::where('role', '=', $request['role'])->first();
            DB::table('role_user')->insert(['user_id' => $user->id, 'role_id' => $role->id]);

            DB::commit();
            return $user;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception("Error: " . $e->getMessage(), 500);
        }
    }

    /**
     * Method to update a user and synchronize modules and subscribers
     *
     * @param array $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(array $request, $id)
    {
        try {
            DB::beginTransaction();

            $user = User::where('id', $id)->withTrashed()->first();
            $user->fill(Arr::except($request, ['deleted_at', 'session_id', 'contact_warehouse_id', 'clerk_id']))->save();
            $user->subscribers()->sync($request['subscribers']);
            $user->modules()->sync($request['modules']);

            DB::table('role_user')->where('user_id', $user->id)->delete();

            $role = Role::where('role', '=', $request['role'])->first();

            DB::table('role_user')->insert(['user_id' => $user->id, 'role_id' => $role->id]);

            if ($request['deleted_at'] == 1) {
                $user->delete();
            } else if ($request['deleted_at'] == 0) {
                $user->restore();
            }

            DB::commit();
            return $this->findForId($id);

        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception("Error: " . $e->getMessage(), 500);
        }
    }

    /**
     * Metodo para consultar un usuario por Id
     *
     * @param $id
     * @return mixed
     * @throws FileNotFoundException
     */
    public function findForId($id)
    {
        $user = User::where('id', $id)->with(['getRoles', 'contact' => function ($contact) {
            $contact->withTrashed();
        }])
            ->withTrashed()->first();

        $user->role = count($user->getRoles) ? $user->getRoles[0]->role : '';
        $user->modules = $user->modules()->pluck('modules.id')->toArray();
        $user->subscribers = $user->subscribers()->pluck('subscribers.id')->toArray();

        if ($user->signature && Storage::disk('local')->exists('public/signature/' . $user->signature)){
            $user->signature = 'data:image/png;base64,' . base64_encode(Storage::disk('local')->get('public/signature/' . $user->signature));
        }else{
            $user->signature = null;
        }

        return $user;
    }

    // public function sellersStatistics() {
    // 	$users = User::select('*')->with(['contact', 'contactWarehouse'])->get();

    // 	$sellers = [];

    // 	$users->filter(function ($user) use ($sellers){
    //         $u = $user->hasRole('vendedor');
    //         if ($u){
    //         	return $u;
    //         }
    //     });

    // 	return $sellers;
    // }

    public function sellersStatistics($request)
    {
        $authUser = User::where('id', Auth::user()->id)->
        with('getRoles')->first();
        if ($authUser->getRoles[0]->role == 'Vendedor') {
            $authUser = $authUser->contact_id;
        } else {
            $authUser = null;
        }
        $filter = $request->all();

        if ($request->filter_date) {
            $fecha_desde = $request->filter_date[0] . ' 00:00:00';
            $fecha_hasta = $request->filter_date[1] . ' 23:59:59';
            $filter['fecha_desde'] = new Carbon($fecha_desde);
            $filter['fecha_hasta'] = new Carbon($fecha_hasta);
        } else {
            $fecha_hasta = date('Y-m-d 23:59:59');
            $filter['fecha_desde'] = new DateTime();
            $filter['fecha_desde'] = $filter['fecha_desde']->modify('first day of this month');
            $filter['fecha_desde'] = $filter['fecha_desde']->format('Y-m-d') . ' 00:00:00';
            $filter['fecha_hasta'] = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_hasta)->format('Y-m-d H:i:s');
        }

        $sellers = Contact::select('id', 'identification', 'name', 'surname', 'address', 'latitude', 'length')
            ->whereHas('users', function ($user) {
                $user->whereHas('getRoles', function ($roles) {
                    $roles->where('role', 'Vendedor');
                });
            })
            ->when($authUser != null, function ($query) use ($authUser) {
                $query->where('id', $authUser);
            })
            ->with([
                'warehouses:id,contact_id,description,address,latitude,length',
                'visits'            => function ($query) use ($filter) {
                    $query->select('id', 'seller_id', 'checkin_date', 'checkout_date', 'contact_id', 'contact_warehouse_id', 'visit_type', 'latitude_checkin', 'longitude_checkin', 'distance', 'observation', 'sales_management', 'collection', 'training', 'checkin_date');
                    $query->with([
                        'parameter:id,name_parameter',
                        'contact:id,name,surname,address,identification',
                        'contact.customer:id,contact_id,category_id',
                        'contactsWarehouse:id,contact_id,description,address,latitude,length',
                    ]);
                    $query->where('checkin_date', '>=', $filter['fecha_desde'])
                        ->where('checkin_date', '<=', $filter['fecha_hasta'])
                        ->where('checkout_date', '<>', null)
                        ->orderBy('checkin_date', 'DESC');
                },
                'ordersForSeller'   => function ($query) use ($filter) {
                    $query->select('seller_id')
                        ->selectRaw('sum(total_value_brut) as total_value_brut')
                        ->selectRaw('count(*) as total_orders')
                        ->where('consecutive', '!=', 0)
                        ->where('in_progress', false)
                        ->where('document_date', '>=', $filter['fecha_desde'])
                        ->where('document_date', '<=', $filter['fecha_hasta'])
                        ->groupBy('seller_id');
                },
                'invoicesForSeller' => function ($query) use ($filter) {
                    $query->select('seller_id')
                        ->selectRaw('sum(total_value_brut) as total_value_brut')
                        ->where('consecutive', '!=', 0)
                        ->where('in_progress', false)
                        ->where('document_date', '>=', $filter['fecha_desde'])
                        ->where('document_date', '<=', $filter['fecha_hasta'])
                        ->groupBy('seller_id');
                },
                'returnsForSeller'  => function ($query) use ($filter) {
                    $query->select('seller_id')
                        ->selectRaw('sum(total_value_brut) as total_value_brut')
                        ->where('consecutive', '!=', 0)
                        ->where('in_progress', false)
                        ->where('document_date', '>=', $filter['fecha_desde'])
                        ->where('document_date', '<=', $filter['fecha_hasta'])
                        ->groupBy('seller_id');
                }
            ])
            ->orderBy('name', 'ASC')
            ->get();

        Carbon::setLocale('es');

        $sellers->map(function ($seller) {

            $num_orders = (isset($seller->ordersForSeller[0]) ? $seller->ordersForSeller[0]->total_orders : 0);
            $seller->num_visits = $seller->visits->count();
            $seller->count_invoices = $seller->invoicesForSeller->count();

            $seller->orders_value = $seller->ordersForSeller->sum('total_value_brut');
            $seller->sells_value = $seller->invoicesForSeller->sum('total_value_brut') + $seller->returnsForSeller->sum('total_value_brut');

            $seller->num_orders = $num_orders;
            $seller->effectiveness = $seller->visits->count() > 0 ? $num_orders / $seller->visits->count() * 100 : 0;
            $seller->effectiveness = round($seller->effectiveness);
        });

        return $sellers;
    }

    /**
     * Traer el nombre de usuario.
     *
     * @return $users User
     * @author Kevin Galindo
     */
    public function getUsersUsername()
    {
        $users = User::select([
            'id',
            'name',
            'surname',
            'username',
            'photo'
        ])->get();

        return $users;
    }
}
