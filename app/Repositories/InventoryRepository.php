<?php

namespace App\Repositories;

use App\Entities\Inventory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentProduct;
use App\Entities\Product;
use App\Entities\Document;
use App\Entities\BranchOffice;
use App\Entities\BranchOfficeWarehouse;

use Illuminate\Support\Facades\Log;
/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class InventoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'inventory_cost',
        'iva_operation',
        'control_existence_outputs',
        'sales_price_control',
        'control_price_list'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inventory::class;
    }

    public function getExistenceForProductAndBranchofficeWarehouse($product_id, $branchoffice_warehouse_id)
    {

        return Inventory::where(['branchoffice_warehouse_id' => $branchoffice_warehouse_id, 'product_id' => $product_id])->first();

    }

    /**
     * @param $document
     */
    public function updateBalances($document)
    {

        $affectation = $document['vouchertype']['template']['operation_type']['affectation'];
        $inventory_cost = $document['vouchertype']['template']['operation_type']['inventory_cost'];

        foreach ($document['documents_products'] as $dp) {

            $inventory = Inventory::where([
                'branchoffice_warehouse_id' => $document['branchoffice_warehouse_id'],
                'product_id' => $dp['product_id']
            ])->first();

            if ($inventory) {
                $multiplier = $affectation == 1 ? 1 : -1;
                if ($affectation != 3) {
                    $inventory->stock += $dp['quantity'] * $multiplier;
                    switch ($inventory_cost) {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            $inventory->stock_values += $inventory->average_cost * $dp['quantity'] * $multiplier;
                            break;
                    }
                }
                $inventory->save();
            }

        }

    }

    /*public function findAll($request){
    	if ($request->paginate == 'true') {

	        $lines = Inventory::where('inventory_cost', 'ILIKE', '%' . $request->filter . '%')
	            ->orWhere('iva_operation', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere('control_existence_outputs', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere('sales_price_control', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere('control_price_list', 'ILIKE', '%' . $request->filter . '%')
	            ->orderBy($request->sortBy, $request->sort)
	            ->paginate($request->perPage);
    	}else{
    		$lines = Line::all();
    	}

    	return $lines;

    }*/

    public function getProducts($branchoffice_warehouse_id)
    {
        $list = Inventory:: where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
                            ->with(['product' => function ($q){
                                $q->select("id", "code", "description", "line_id", "subline_id", "brand_id", "quantity_available as quantity");
                                $q->with(['line',
                                'subline',
                                'brand',
                                'price',]);
                            }])
                            ->get();
        return $list;
    }


    /**
     * Finalizar o actualizar el inventario
     * @author Santiago torres
     * @param $documentProduct id del documentProduct
     */

    public function finalize($documentProduct, $validate_stock = true)
    {
        // Log::error('==============================================================================comienza funcion actualiza inventario==============================================================================');
        // buscamos el document product
        $dc = DocumentProduct :: where ('id', $documentProduct)->first();
        // buscamos el document product
        $documentProduct = DocumentProduct :: where('id', $documentProduct)
                                            ->select("branchoffice_warehouse_id","document_id", "product_id", "quantity", "unit_value_before_taxes", "operationType_id", "total_value_brut", "manage_inventory", "fixed_cost")
                                            ->with(['document' => function($q){
                                                $q->select("id", "branchoffice_warehouse_id", "vouchertype_id" , "thread", 'model_id');
                                                $q->with(['branchofficeWarehouse']);
                                                $q->with(["vouchertype" => function ($q2){
                                                    $q2->with(['template' => function ($q3){
                                                        $q3->with('operationType');
                                                    } ]);
                                                }]);
                                            },
                                            'operationType',
                                            'product' => function($q){
                                                $q->with(['line']);
                                            }
                                            ])
                                            ->wherehas('product', function($q){
                                                $q->wherehas('line' , function($q){
                                                    $q->where('inventory_update', true);
                                                    $q->where('service', false);
                                                });
                                            })
                                            ->first();
        // buscamos el inventario del producto en la bodega que dice el document product
        $validation = Inventory :: where('product_id', $documentProduct->product_id)
                    -> where('branchoffice_warehouse_id',$documentProduct->branchoffice_warehouse_id)
                    ->first();


                    // Log::info('document Product');
                    // Log::info($dc->id);
                    // Log::info('document Product cantidad');
                    // Log::info($dc->quantity);
                    // Log::info('document Product bodega');
                    // Log::info($dc->branchoffice_warehouse_id);
                    // Log::info('document Product producto');
                    // Log::info($dc->product_id);
        //validamos si maneja inventario
        if ($documentProduct->manage_inventory) {

            // Log::info('document Product costo de la operacion');
            // Log::info($documentProduct->operationType->inventory_cost);

            // Log::info('valores de document product para calcular el costo');
            // Log::info('total_value_brut');
            // Log::info($dc->total_value_brut);

            if ($validation) {
                // Log::info('costo promedio actual');
                // Log::info($validation->average_cost);


                // Log::info('stock de inventario antes de proceso');
                // Log::info($validation->stock);

                // Log::info('stock_values de inventario antes de proceso');
                // Log::info($validation->stock_values);
            }else{
                // Log::info('NO hay costo promedio actual');
            }
            // Log::info('===============================================');
            // calculamos costo promedio si el document product no tiene costo fijo
            if (!$documentProduct->fixed_cost) {
                switch ($documentProduct->operationType->inventory_cost) {
                    case '2' :
                        // costo se toma con el valor bruto del document product
                        $dc->inventory_cost_value = $dc->total_value_brut;
                    break;
                    case '3':
                        // costo se toma costo promedio * cantidad
                        if (isset($validation->average_cost)){
                            $dc->inventory_cost_value = $dc->quantity * floatval($validation->average_cost);
                        } else {
                            $dc->inventory_cost_value = 0;
                        }
                    break;
                    case '4':
                        // costo se toma del documento base
                        $base = Document ::where('id', $documentProduct->document->thread)->first();
                        $documentProductBase = DocumentProduct :: where('document_id', $base->id)->where('product_id', $documentProduct->product_id)->first();
                        if ($dc->quantity == $documentProductBase->quantity) {
                            $dc->inventory_cost_value =  $documentProductBase->inventory_cost_value;
                        }else {
                            $dc->inventory_cost_value =   $documentProductBase->inventory_cost_value > 0 ? $documentProductBase->inventory_cost_value/$documentProductBase->quantity * $documentProduct->quantity: 0 * $documentProduct->quantity;
                        }
                    break;
                }
                if ($documentProduct->document->model_id == 36 || $documentProduct->document->model_id == 37 ) {
                    if (isset($validation->average_cost)){
                        if ($dc->total_value != $validation->average_cost) {
                            $dc->inventory_cost_value = $dc->total_value;
                            $dc->fixed_cost = true;
                        }
                    }else{
                        $dc->inventory_cost_value = $dc->total_value;
                    }
                }
            }

            // Log::info('costo de la operación');
            // Log::info($dc->inventory_cost_value);

            // Log::info('afectacion 1 = entrada ; 2 = salida');
            // Log::info($documentProduct->operationType->affectation);


            // Log::info('cantidad de documents products');
            // Log::info($documentProduct->quantity);

            // Log::info('costo de la operacion ');
            // Log::info($dc->inventory_cost_value);

            //  validamos si es entrada o salida para sumar o restar saldos del inventario
            if ($documentProduct->operationType->affectation == 1) {
                //  seteamos los valores stock stock values y avarage cost
                if ($validation){
                    // seteamos saldo en unidades
                    $stock = $validation->stock + $documentProduct->quantity;
                    // seteamos saldo en valores
                    $stock_values = $validation->stock_values + $dc->inventory_cost_value;
                    // seteamos disponibilidad
                    $available = $validation->available;

                    // afecta disponiobilidad si es bodega disponible
                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {
                        $available+=$documentProduct->quantity;
                    }

                    // seteamos costo promedio
                    if ($validation->stock != 0) {
                        $average_cost = number_format($validation->stock_values/$validation->stock, 2, '.', '');
                    }
                    else{
                        //si el stock es 0 se deja el costo promedio
                        $average_cost = $validation->average_cost;
                    }
                }else{
                    // seteamos disponibilidad y validamos si es bodega disponible
                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {
                        $available =$documentProduct->quantity;
                    }else{
                        $available = 0;
                    }
                    // seteamos saldo en unidades
                    $stock = $documentProduct->quantity;
                    // seteamos saldo en valores
                    $stock_values = $dc->inventory_cost_value;
                    // seteamos costo promedio
                    $average_cost = number_format($stock_values/$stock, 2, '.', '');
                }

            }elseif ($documentProduct->operationType->affectation == 2) {

                $validation = Inventory :: where('product_id', $documentProduct->product_id)
                -> where('branchoffice_warehouse_id',$documentProduct->branchoffice_warehouse_id)
                // ->sharedLock()
                ->lockForUpdate()
                ->first();



                if ($validation){
                    // validamos si hay unidades suficientes para hacer la operación
                    if ($validation->stock < $documentProduct->quantity && $validate_stock) {

                        throw new \Exception("No hay Disponibilidad para el producto ".$documentProduct->product->code.'-'.$documentProduct->product->description);

                    }
                    // seteamos saldo en unidades
                    $stock = $validation->stock - $documentProduct->quantity;
                    // seteamos saldo en valores
                    $stock_values = $validation->stock_values - $dc->inventory_cost_value;
                    // seteamos disponibilidad
                    $available = $validation->available;

                    // afecta disponibilidad si es bodega disponible
                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {
                        $available-=$documentProduct->quantity;
                    }


                    // seteamos costo promedio
                    if($validation->stock != 0){
                        $average_cost = number_format($validation->stock_values/$validation->stock, 2, '.', '');
                    }else{
                        //si el stock es 0 se deja el costo promedio
                        $average_cost = $validation->average_cost;
                        // $average_cost = 0;
                    }

                }else{
                    // afecta disponibilidad si es bodega disponible
                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {

                        $available = $documentProduct->quantity;
                                $available = $documentProduct->quantity;
                        $available = $documentProduct->quantity;
                    }else{
                        $available = 0;
                    }

                    // seteamos saldo en unidades
                    $stock = 0-$documentProduct->quantity;
                    // seteamos saldo en valores
                    $stock_values = 0-$dc->inventory_cost_value;
                    // seteamos costo promedio
                    if ($stock!=0) {
                        $average_cost = number_format($stock_values/$stock, 2, '.', '');
                    }else{
                        $average_cost = 0;
                    }
                }
            }


            if (isset($stock)) {
                // Log::info('info que va para inventory ');
                // Log::info('stock ');
                // Log::info($stock);

                // Log::info('stock_values ');
                // Log::info($stock_values);

                // Log::info('average_cost ');
                // Log::info($average_cost);
                // Actualizamos inventario
                //almacenamos la informacion de saldos en unidades calores y costo promedio en inventory
                $r =Inventory::updateOrCreate(
                    [
                        'product_id' => $documentProduct->product_id,
                        'branchoffice_warehouse_id' => $documentProduct->branchoffice_warehouse_id
                    ],
                    [
                        'branchoffice_warehouse_id' => $documentProduct->branchoffice_warehouse_id,
                        'product_id' => $documentProduct->product_id,
                        'stock' => $stock,
                        'stock_values' => $stock_values,
                        'average_cost' => $average_cost,
                        'available' => $available
                        ]
                );

                if ($r->stock != 0) {
                    $r->average_cost = number_format($r->stock_values/$r->stock, 2, '.', '');
                    // dd(number_format($r->stock_values/$r->stock, 2, '.', ''));
                    $r->save();
                }else{
                    // $r->average_cost = 0;
                    // $r->save();
                }

                // se almacena los valores de saldos en unidades y valores en documents products
                $dc->stock_after_operation = $stock;
                $dc->stock_value_after_operation = $stock_values;
                $dc->save();

                // Log::info('stock_after_operation ');
                // Log::info($dc->stock_after_operation);

                // Log::info('stock_value_after_operation ');
                // Log::info($dc->stock_value_after_operation);
                // Log::info('==============================================================================finaliza funcion actualiza inventario==============================================================================');
                    // return ['data' => $r,
                    //         'status' => true,
                    //         'message' => 'se ha actualizado el inventario'];
            }
        }

    }

    /**
     *
     *
     */

    public function updateInventoryFromInventoryx($data){
        try {

            $Inventory = Inventory::updateOrCreate(
                [
                    'product_id' => $data["product_id"],
                    'branchoffice_warehouse_id' =>$data["branchoffice_warehouse_id"]
                ],
                [
                    'branchoffice_warehouse_id' => $data["branchoffice_warehouse_id"],
                    'product_id' => $data["product_id"],
                    'stock' => $data["stock"],
                    'stock_values' => $data["stock_values"],
                    'average_cost' => $data["average_cost"]
                ]
            );

            return $Inventory;
        } catch (\Exception $e) {
            Log::error('Error creando el documento del documento base ' . $document_id . ': ' . $e->getLine());
            //throw $th; Log::error('Error creando el documento del documento base ' . $document_id . ': ' . $e->getLine());
        }
    }

    public function updateLocation($request)
    {
        $inventory = Inventory :: where('branchoffice_warehouse_id' ,$request->branchoffice_warehouse)
                                ->where('product_id', $request->product)
                                ->first();

        // $validation = Inventory ::where('branchoffice_warehouse_id' ,$request->branchoffice_warehouse)
        //                         ->where('location',strtoupper($request->location))
        //                         ->first();

        // if ($validation) {
        //     return ['data' => [],
        //             'status' => false,
        //             'message' => 'Ya hay un producto en esa ubicación'];
        // }else{

        if ($inventory) {
            $inventory->location =  strtoupper($request->location);
            $inventory->save();
        }else{
            $data= [
                'branchoffice_warehouse_id'  => $request->branchoffice_warehouse,
                'product_id'                 => $request->product,
                'location'                   => strtoupper($request->location),
                'stock'                      => 0,
                'average_cost'               => 0,
                'stock_values'               => 0,
            ];
            $inventory = Inventory :: create($data);
        }

            return ['data' => $inventory,
                    'status' => true,
                    'message' => 'se ha actualizado la localización'];
        // }

    }

    /**
     * @author Santiago Torres
     *
     * disponibilidad de un producto
     *
     * @param product
     */

    public function availability($request)
    {
        if ($request->branchoffice_warehouse_id) {
            $inventoryAvailability = Inventory :: where('product_id', $request->product_id)->where('branchoffice_warehouse_id', $request->branchoffice_warehouse_id)->first();
        }elseif ($request->branchoffice_id && $request->servicerequeststate) {
            if ($request->servicerequeststate == 10 ||$request->servicerequeststate == 11) {
                $branchoffice_warehouse_id = BranchOfficeWarehouse :: where('branchoffice_id', $request->branchoffice_id)->where('warehouse_code', '02')->first()->id;
            }else{
                $branchoffice_warehouse_id = BranchOfficeWarehouse :: where('branchoffice_id', $request->branchoffice_id)->where('warehouse_code', '01')->first()->id;
            }

            $inventoryAvailability = Inventory :: where('product_id', $request->product_id)->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)->first();
        }

        return $inventoryAvailability;
    }

    /*
    *diponibilidad de unproducto en todas las bodegas
    *
    */
    public function branchOfficesAvailability($product_id)
    {
        $product = Product::find($product_id);

        $inventories =  Inventory::where('product_id', $product_id)
                        ->whereHas('branchofficeWarehouse', function ($query) {
                            $query->where('warehouse_code', '01');
                        })
                        ->get();

        $branchoffices = BranchOffice::whereHas('warehouses', function ($query) {
                            $query->where('warehouse_code', '01');
                        })
                        ->get();

        $arrayBranchoffice = [];

        // Se arma la estructura del arreglo
        foreach ($branchoffices as $key => $branchoffice) {

            $arrayBranchoffice[] = [
                'id'=>$branchoffice->id,
                'product_id' => $product_id,
                'branchoffice_id' => $branchoffice->id,
                'branchoffice' => $branchoffice->name,
                'branchoffice_code' => $branchoffice->code,
                'branchoffice_stock' => 0,
                'branchoffice_available' => 0
            ];
        }

        // Asignamos los valores del stock y la disponibilidad
        foreach ($inventories as $key => $inventory) {
            $key_array = array_search(
                $inventory->branchofficeWarehouse->branchoffice->id,
                array_column($arrayBranchoffice, 'branchoffice_id')
            );

            // Agregamos el producto al array.
            if ($key_array !== false) {
                $arrayBranchoffice[$key_array]['branchoffice_stock'] = $inventory->stock;
                $arrayBranchoffice[$key_array]['branchoffice_available'] = $inventory->available;
            }

        }

        $collectBranchoffice = collect($arrayBranchoffice);


        $ailabilitytotals = [
            'id'=>$product->id,
            'product' => $product->description,
            'product_id' => $product->id,
            'total_stock' => $collectBranchoffice->sum('branchoffice_stock'),
            'total_available' => $collectBranchoffice->sum('branchoffice_available'),
        ];

        return [$ailabilitytotals, $collectBranchoffice->sortBy('branchoffice_code')->values()->all()];


        $ailabilityquery =  Inventory :: where('product_id', $product_id)
        ->with([
            'product'=>function($query){
                $query->select('id', 'code', 'description');
            },
            'branchofficeWarehouse'=>function($query){
                $query->select('id', 'warehouse_code', 'warehouse_description', 'branchoffice_id');
                $query->with('branchoffice');
            }
        ])
        // ->orderBy('branchofficeWarehouse.branchoffice.code', 'ASC')
        ->get()
        ->groupBy('branchofficeWarehouse.branchoffice.code');

        $ailabilityquery = $ailabilityquery->values()->toArray();
        //declaro variables
        $ailability = array();
        $ailabilitytotals=[
            'product' => '',
            'total_stock' => 0,
            'total_available' => 0
        ];
        $branchoffices_ids = [];

        //se recorre el resultado de la consulta para agruoar el stock y disponibilidad por sede
        foreach (array_keys($ailabilityquery) as $key => $branchoffice) {
            //se setea el titulo que ira en el front
            $ailabilitytotals['product'] = $ailabilityquery[$branchoffice][0]['product']['code'].'-'. $ailabilityquery[$branchoffice][0]['product']['description'];
            $ailabilitytotals['product_id'] = $ailabilityquery[$branchoffice][0]['product_id'];
            $ailability[$branchoffice]['branchoffice_stock'] = 0;
            $ailability[$branchoffice]['branchoffice_available'] = 0;

            //se guarda el id de la bodega para despues omitirlo
            $branchoffices_ids[]= $ailabilityquery[$branchoffice][0]['branchoffice_warehouse']['branchoffice']['id'];
            foreach ($ailabilityquery[$branchoffice] as $key => $value) {
                // se suman los valores de stock y disponibilidad
                $ailability[$branchoffice]['product_id']=$value["product_id"];
                $ailability[$branchoffice]['branchoffice_id']=$value['branchoffice_warehouse']['branchoffice']['id'];
                $ailability[$branchoffice]['branchoffice']=$value['branchoffice_warehouse']['branchoffice']['name'];
                $ailability[$branchoffice]['branchoffice_code']=$value['branchoffice_warehouse']['branchoffice']['code'];
                $ailability[$branchoffice]['branchoffice_stock']+= $value['stock'];
                $ailability[$branchoffice]['branchoffice_available']+= $value['available'];
            }

            //se calcula el total de todas las sedes
            $ailabilitytotals['total_stock']+=$ailability[$branchoffice]['branchoffice_stock'];
            $ailabilitytotals['total_available']+=$ailability[$branchoffice]['branchoffice_available'];
        }

        // se consultan las bodegas que no tienen el stock y disponiblidad
        $branchoffices = BranchOffice ::whereNotIn('id', $branchoffices_ids)->get();

        foreach ($branchoffices as $key => $branchoffice) {
            $newailability = ['branchoffice'              => $branchoffice->name,
                        'branchoffice_code'         => $branchoffice->code,
                        'branchoffice_id'         => $branchoffice->id,
                        'branchoffice_stock'        => '0',
                        'branchoffice_available'    => '0',
                        //'product_id'                => $ailabilitytotals['product_id']
                        ];
            $ailability[]= $newailability;
        }

        //ordenar por sede
        $collection = collect($ailability);
        $sorted  = $collection->sortBy('branchoffice_code');

        return [$ailabilitytotals,$sorted->values()->all()];
    }

    /**
     * @param $request
     * @return array
     * @author Jhon García
     */
    public function getDataReport($request)
    {
        $parameters = [
            $request->branchoffice_id == 'null' ? null : $request->branchoffice_id,
            $request->branchoffice_warehouse_id == 'null' ? null : $request->branchoffice_warehouse_id,
            $request->line_id == 'null' ? null : $request->line_id,
            $request->brand_id == 'null' ? null : $request->brand_id
        ];

        if (substr($request->date, 0,10) == date('Y-m-d')){
            $data = DB::select('select * from public.fn_inventory_report(?, ?, ?, ?) where unit_balance > 0;', $parameters);
        }else{
            $parameters[] = substr($request->date, 0,10) . ' 23:59:59';
            $data = DB::select('select * from public.fn_inventory_report(?, ?, ?, ?, ?) where unit_balance > 0;', $parameters);
        }

        return $data;

    }

    /**
    * Cuando no se le envia un producto al reconstructor los valores de strock, stock values y avarage_cost deben quedar en 0
    *
    * @author Santiago Torres
    */
    public function updateAllValuesTo0()
    {
        $inventory = Inventory::select('id')->get();

        $response =[];
        foreach ($inventory as $key => $id) {
            $response[]=Inventory ::
                where('id', $id->id)
                ->update([
                'stock'=>0,
                'stock_values'=>0,
                'average_cost'=>0,
            ]);
        }
        return $response;
    }

     /**
    * Cuando se le envia un producto sin fecha al reconstructor los valores de strock, stock values y avarage_cost deben quedar en 0
    *
    * @author Santiago Torres
    */
    public function updateValuesTo0($product_id)
    {
        $inventory = Inventory::select('id')->where('product_id',$product_id)->get();

        $response =[];
        foreach ($inventory as $key => $id) {
            $response[]=Inventory ::
                where('id', $id->id)
                ->update([
                'stock'=>0,
                'stock_values'=>0,
            ]);
        }
        return $response;
    }


    public function searchdupli()
    {
        set_time_limit(0);
        $products = Inventory :: select('product_id')->distinct()->get();
        $branchoffice_warehouses = Inventory :: select('branchoffice_warehouse_id')->distinct()->get();

        $duplicados = [];
        foreach ($products as $key => $product) {
            foreach ($branchoffice_warehouses as $key => $branchoffice_warehouse) {
                $validate = Inventory :: where('product_id', $product->product_id)->where('branchoffice_warehouse_id',$branchoffice_warehouse->branchoffice_warehouse_id)->get();
                if (count($validate) >= 2 ) {
                    $duplicados []= $validate;
                }
            }
        }
        return $duplicados;
    }

    /**
     * @return array
     * @author Jhon García
     */
    public function getDataForRepositioningAssistant()
    {
        return DB::select('select * from public.fn_repositioning_assistant();');
    }

    /**
     * Este metodo retorna el costo de un producto
     * segun su ID y la sucursal
     * 
     * @author Kevin Galindo
     */
    public function getAverageCostByProduct($product_id, $branchoffice_warehouse_id)
    {
        $inventory = Inventory::where('product_id', $product_id)
        ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
        ->first();

        return $inventory ? $inventory->average_cost : 0;
    }


    public function setMinimunStock($row, $product)
    {
        $branchoffice = BranchOffice :: where('code', '010')->first();
        $branchoffice_warehouse = BranchOfficeWarehouse :: where('branchoffice_id', $branchoffice->id)->where('warehouse_code', '01')->first(); 

        $inventory = Inventory :: where('branchoffice_warehouse_id',$branchoffice_warehouse->id)->where('product_id', $product->id)->first();
        if (is_null($inventory)) {
            $inventory = Inventory ::create([
                'branchoffice_warehouse_id'=>$branchoffice_warehouse->id,
                'product_id'=> $product->id,
                'stock'=> 0,
                'stock_values'=>0,
                'average_cost'=>0,
                'minimum_stock'=> $row["Stock Minimo"]
            ]);
        }else{
            $inventory->minimum_stock = $row["Stock Minimo"];
            $inventory->save();
        }
    }

}
