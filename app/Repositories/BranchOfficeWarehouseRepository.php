<?php

namespace App\Repositories;

use App\Entities\BranchOfficeWarehouse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BranchOfficeWarehouseRepository
 * @package App\Repositories
 * @version January 4, 2019, 9:44 pm UTC
 *
 * @method BranchOfficeWarehouse findWithoutFail($id, $columns = ['*'])
 * @method BranchOfficeWarehouse find($id, $columns = ['*'])
 * @method BranchOfficeWarehouse first($columns = ['*'])
 */
class BranchOfficeWarehouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'branchoffice_id',
        'warehouse_code',
        'warehouse_description',
        'update_inventory',
        'exits_no_exist',
        'negative_balances',
        'warehouse_status',
        'last_date_physical_inventory',
        'number_physical_inventory'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BranchOfficeWarehouse::class;
    }

    public function findForBranchOffice($request)
    {
        if ($request->paginate == 'true') {
            $warehouses = [];
        } else {
            if (isset($request['branchoffice_id']) && $request['branchoffice_id'] > 0) {
                $warehouses = BranchOfficeWarehouse::where('branchoffice_id', $request['branchoffice_id'])
                    ->orderBy('warehouse_code', 'asc')
                    ->get();
                if (isset($request['code']) && is_array($request['code']) ) {
                    $warehouses = BranchOfficeWarehouse::where('branchoffice_id', $request['branchoffice_id'])
                    ->whereIn('warehouse_code', $request['code'])
                    ->orderBy('warehouse_code', 'asc')
                    ->get();
                }
            } else {
                $warehouses = BranchOfficeWarehouse::whereHas('branchoffice', function ($branchoffice) {
                    $branchoffice->where('main', true);
                })
                    ->orderBy('warehouse_code', 'asc')
                    ->get();
//				$warehouses = BranchOfficeWarehouse::all();
            }
        }

        return $warehouses;
    }

    public function getBranchsWare()
    {
        $branchs_ware = BranchOfficeWarehouse::with('branchoffice')->select("id", "branchoffice_id", "warehouse_description")->get();
        return $branchs_ware;
    }

    public function allBranchOfficeWarehouse()
    {
        return BranchOfficeWarehouse::select('id AS branchoffice_warehouse_id')->orderBy('branchoffice_warehouse_id')->get();
    }
}
