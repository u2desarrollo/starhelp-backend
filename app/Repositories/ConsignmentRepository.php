<?php

namespace App\Repositories;

use App\Entities\Consignment;
use Illuminate\Http\Request;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ConsignmentRepository
 * @package App\Repositories
 * @version June 10, 2019, 2:58 pm UTC
 *
 * @method Consignment findWithoutFail($id, $columns = ['*'])
 * @method Consignment find($id, $columns = ['*'])
 * @method Consignment first($columns = ['*'])
*/
class ConsignmentRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [

		'bank_id',
        'consignment_number',
        'photo_consignment',
        'user_id',
        'date_time'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Consignment::class;
	}

	public function create(array $consignment) {
        $create = Consignment::create([
            'bank_id' => $consignment['banco'],
            'consignment_number' => $consignment['numeroConsignacion'],
            'photo_consignment' => $consignment['nombreFoto'],
            'user_id' => $consignment['iduser'],
            'date_time' => date('Y-m-d H:i:s')
        ]);
        $created = ['id' => $create->id];
        return $created;
    }
}
