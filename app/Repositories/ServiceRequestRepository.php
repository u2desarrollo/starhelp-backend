<?php

namespace App\Repositories;

use App\Entities\ServiceRequest;
use App\Entities\ServiceRequestState;
use App\Entities\ServiceRequestStatusHistory;
use App\Entities\ServiceRequestType;
use App\Entities\ServiceRequestsProduct;

use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use Carbon\Carbon;

use DB;

class ServiceRequestRepository extends BaseRepository
{

    public function model()
    {
        return ServiceRequest::class;
    }

    /**
     * Este metodo se encarga de retornar la informacion de 
     * las solicitades de servicio.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function getServiceRequest($request)
    {
        //$close = $request->close == "true" ? true : false;
        //$rejected = $request->rejected == "true" ? true : false;

        $dateRange = $request->dateRange;
        $branchoffice_id = $request->branchoffice_id;

        $service_requests_types_id = $request->service_requests_types_id;
        $service_requests_state_id = $request->service_requests_state_id;

        $serviceRequest = ServiceRequest::
        where('branchoffice_id', $branchoffice_id)
        ->with([
            'type' => function ($query) {
                $query->with([
                    'states'
                ]);
            },
            'statusHistory' => function ($query) {
                $query->with([
                    'user',
                    'state',
                    'document'
                ]);
            },
            'contact' => function ($query) {
                $query->with([
                    'warehouses',
                ]);
            },
            'contactsWarehouse',
            'seller',
            'product'=>function($q2){
                $q2->with('price');
            }
        ]);

        //Filtros
        if ($dateRange && is_array($dateRange)) {
            $serviceRequest->whereBetWeen('updated_at', [$dateRange[0]." 00:00:00" , $dateRange[1]." 23:59:59"]);
        }

        if ($service_requests_types_id) {
            $serviceRequest->where('service_requests_types_id', $service_requests_types_id);
        }

        if ($service_requests_state_id) {
            $serviceRequest->where('actual_state_id', $service_requests_state_id);
        }

        $serviceRequest = $serviceRequest->get();

        foreach ($serviceRequest as $key => $item) {
            
            $item->get = true;
            $lastState = $item->statusHistory->last();

            if ($lastState->state->name == 'Cerrada') {

                // Validar que sea del dia actual
                $now = Carbon::now();
                if ($now->diffInDays($lastState->date) !== 0) {
                    $item->get = false;
                }

            }  
            // Rechazadas
            else if ($lastState->state->name == 'Rechazada') {
                if ( $rejected === true ) {
                    $item->get = true;
                } else {
                    $item->get = false;
                }
            }            
        }


        $arrayItem = $serviceRequest->filter(function($item) {
            return $item->get === true;
        });

        // ->whereDoesntHave('statusHistory', function ($query) {
        //     $query->whereHas('state', function ($query){
        //         $query->where(function($query) {
        //             $query->orWhere('name', 'Cerrada');
        //             $query->orWhere('name', 'Rechazada');
        //         });
        //     });
        // })
        //->paginate($request->perPage);

        return $arrayItem;
    }

    /**
     * Este metodo se encarga de retornar un registro
     * de una solicitades de servicio segun su id.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function findById($request, $id)
    {
        $serviceRequest = ServiceRequest::with([
            'type' => function ($query) {
                $query->with([
                    'states'
                ]);
            },
            'statusHistory' => function ($query) {
                $query->with([
                    'user',
                    'document',
                    'state'=>function($query){
                        $query->with([
                            'template'
                        ]);
                    }
                ]);
            },
            'contact',
            'seller',
            'product'=> function ($query){
                $query->with(['price', 'inventory']);
            },
            'contactsWarehouse'
        ])->find($id);

        return $serviceRequest;
    }

    /**
     * Este metodo se encarga de retornar el siguiente estado
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function getNextstateServiceRequest($serviceRequest)
    {
        // Traemos los estados que ya haya tenido
        $idsStatusHistory = $serviceRequest->statusHistory->map(function ($item, $key) { return $item->service_requests_state_id; });

        // Traemos el ultimo registro del historico
        $lastStatusHistory = $serviceRequest->statusHistory->sortBy('id')->last();

        // Traemos el tipo de solicitud
        $service_requests_types_id = $serviceRequest->service_requests_types_id;

        // Traemos el valor que identifica si es una sub clase
        $service_requests_state_id = $lastStatusHistory->state->service_requests_state_id;

        // Calculamos el siguiente paso
        $nextOrder = $lastStatusHistory->state->order + 1;

        // Traemos el siguiente paso
        $nextServiceRequestState = ServiceRequestState::
          where('service_requests_types_id', $service_requests_types_id)
        ->where('order', $nextOrder)
        ->with(['template'])
        ->where('service_requests_state_id', $service_requests_state_id)
        ->whereNotIn('id', $idsStatusHistory->toArray())
        ->get();

        // En caso de no haber otro paso se valida si hay algun subpaso
        if ($nextServiceRequestState->count() == 0) {
            $nextServiceRequestState = ServiceRequestState::
              where('service_requests_types_id', $service_requests_types_id)
            ->where('order', 1)
            ->with(['template'])
            ->where('service_requests_state_id', $lastStatusHistory->service_requests_state_id)
            ->whereNotIn('id', $idsStatusHistory->toArray())
            ->get();
        }

        // Retornamos la informacion
        return $nextServiceRequestState->toArray();
    }

    /**
     * 
     * Este metodo se encarga de crear un registro 
     * en el historico de estados de la solicitud de servicio.
     * 
     * @author Kevin Galindo
     */
    public function addStateHistoryServiceRequest($data)
    {
        try {
            $res = ServiceRequestStatusHistory::create($data);

            $res = ServiceRequest::find($data['service_requests_id'])->update([
                'updated_at' => $data['date'],
                'product_id' => $data['product_id'],
                'unit_value' => isset($data['unit_value']) ? $data['unit_value'] : null,
                'actual_state_id' => $data['service_requests_state_id']
            ]);

            return [
                'code' => 201,
                'data' => $res
            ];
        } catch (\Exception $e) {
            return [
                'code' => 500,
                'data' => $e->getMessage() . " " . $e->getLine()
            ];
        }
        
    }

    /**
     * Este metodo se encarga de crear una solicitud de servicio
     * con su estado inicial.
     * 
     * @author santiago torres | kevin galindo
     */
    public function store($data, $request)
    {
        try {
            
            DB::beginTransaction();

            // Creamos el registro de la solicitud de servicio.
            $data['date'] = Carbon::now();
            $serviceRequest = ServiceRequest::create($data);

            // Traemos el ID del estado inicial segun el tipo de solicitud de servicio
            $state = ServiceRequestState::where('service_requests_types_id', $request->type)
                    ->where('order', 0)
                    ->first()
                    ->id;

            // Organizamos la informacion y creamos el registro en el historico de estados.
            $dataHistory = [
                'service_requests_id' => $serviceRequest->id,
                'service_requests_state_id'=> $state,
                'user_id'=> $request->user,
                'observation' => '',
                'date' => Carbon::now()
            ];

            $serviceRequestStatusHistory = ServiceRequestStatusHistory::create($dataHistory);

            // Actualizamos el estado actual
            $serviceRequest->actual_state_id = $state;
            $serviceRequest->save();

            // Aceptamos los cambios
            DB::commit();

            // Rertornamos la informacion
            return [
                'code' => 201,
                'data' => [$serviceRequest, $serviceRequestStatusHistory]
            ];
        } catch (\Exception $e) {

            // En caso de error removemos los cambios.
            DB::rollBack();

            // Rertornamos el error.
            return [
                'code' => 500,
                'data' => $e->getMessage() . " " . $e->getLine()
            ];
        }
    }

    /**
     * Este metodo se encarga de traer el estado
     * de cerrado.
     * 
     * @author Kevin Galindo
     */
    public function getCloseStatusServiceRequest($service_requests_types_id)
    {
        $data = ServiceRequestState::where('service_requests_types_id', $service_requests_types_id)
        ->where('name', 'ilike', 'CERRADA')        
        ->first();

        return $data;
    }

    /**
     * 
     * este metodo guarda la informacion de los productos
     * 
     * @author Kevin Galindo 
     */
    public function saveServiceRequestsProduct($product)
    {
        return ServiceRequestsProduct::updateOrCreate([
            'product_id' => $product['product_id'],
            'service_requests_id' => $product['service_requests_id'],
            'service_requests_state_id' => $product['service_requests_state_id'],
        ], $product);
    }
    
}
