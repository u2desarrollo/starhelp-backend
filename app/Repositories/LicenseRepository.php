<?php

namespace App\Repositories;

use App\Entities\License;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LicenseRepository
 * @package App\Repositories
 * @version December 18, 2018, 7:53 pm UTC
 *
 * @method License findWithoutFail($id, $columns = ['*'])
 * @method License find($id, $columns = ['*'])
 * @method License first($columns = ['*'])
*/
class LicenseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'license_code',
        'start_date',
        'end_date',
        'trial_license',
        'maximum_users'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return License::class;
    }

    /**
     * Método para obtener el listado de vouchersTypes de acuerdo a los parámetros recibidos
     * @param $request contiene los parámetros para filtrar
     * @return $vourchesType contiene una collección de de vouchersTypes
     */
    public function findAll($request)
    {
        if ($request->paginate == 'true') {

            $license = License::where('license_code', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere(function($query) use ($request){
                    $anio = false;
                    $fecha = explode("-", trim($request->filter));
                    if(isset($fecha[2])){
                        if(checkdate($fecha[1], $fecha[2], $fecha[0])){
                            $query->whereDate('start_date', trim($request->filter))
                            ->orWhereDate('end_date',  trim($request->filter));
                        } else {
                            $anio = is_int((int)$request->filter);
                            $code = is_string($request->filter);
                            $boolean = is_bool($request->filter);
                        }
                    }else{
                        $anio = is_int((int)$request->filter);
                        $code = is_string($request->filter);

                        $tipo_lic = $request->filter;
                        if($tipo_lic == 'prueba'){
                            $query->where('trial_license', '=', 'true');
                            return;
                        } else if($tipo_lic == 'permanente'){
                            $query->where('trial_license', '=', 'false');
                            return;
                        }
                    }
                    if($anio){
                        $query->orwhereYear('start_date',  $request->filter)
                        ->orWhereYear('end_date', $request->filter)
                        ->orWhere('maximum_users', '=', $request->filter)
                        ->orWhereMonth('start_date',  $request->filter)
                        ->orWhereMonth('end_date',  $request->filter)
                        ->orWhereDay('start_date',  $request->filter)
                        ->orWhereDay('end_date',  $request->filter);
                    } else {
                        return false;
                    }

                })

                ->orderBy($request->sortBy, $request->sort)
                ->paginate($request->perPage);
        }else{
            $license = License::all();
        }

        return $license;
    }
    public function findForId($id){

        $license = License::where('id', $id)->first();

        return $license;

    }

}
