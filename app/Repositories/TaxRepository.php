<?php

namespace App\Repositories;

use App\Entities\Tax;
use App\Traits\CollectionsTrait;
use Doctrine\DBAL\Types\DecimalType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TaxRepository
 * @package App\Repositories
 * @version September 9, 2019, 8:28 pm UTC
 *
 * @method Tax findWithoutFail($id, $columns = ['*'])
 * @method Tax find($id, $columns = ['*'])
 * @method Tax first($columns = ['*'])
 */
class TaxRepository extends BaseRepository
{
    /**
     * @var array
     */

    use CollectionsTrait;


    protected $fieldSearchable = [
        'code',
        'description',
        'percentage_sale',
        'percentage_purchase',
        'base',
        'minimum_base',
        'sales_account',
        'sales_return_account',
        'purchase_account',
        'purchase_return_account',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tax::class;
    }

    public function allPaginated($request)
    {
        $taxes = Tax::where('code', 'ILIKE', '%' . $request->filter . '%')
            ->orWhere('description', 'ILIKE', '%' . $request->filter . '%')->orderBy('code','desc')
            ->paginate($request->perPage);
        return $taxes;
    }
}
