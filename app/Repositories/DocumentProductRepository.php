<?php

namespace App\Repositories;

use App\Entities\Document;
use App\Entities\DocumentProduct;
use App\Entities\DocumentTransactions;
use App\Entities\DocumentProductTax;
use App\Entities\DocumentInformation;
use App\Entities\Inventory;
use App\Entities\Price;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\Contact;
use App\Traits\CacheTrait;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;
/**
 * Class DocumentProductRepository
 *
 * @package App\Repositories
 * @version April 25, 2019, 3:25 pm UTC
 * @method DocumentProduct findWithoutFail($id, $columns = ['*'])
 * @method DocumentProduct find($id, $columns = ['*'])
 * @method DocumentProduct first($columns = ['*'])
 */
class DocumentProductRepository extends BaseRepository
{
    use CacheTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'document_id',
        'product_id',
        'lot',
        'serial_number',
        'seller',
        'technician',
        'lot_due_date',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'description',
        'observation',
        'applies_inventory',
        'quantity',
        'quantity_available',
        'bonus_quantity',
        'missing_quantity',
        'unit_list_value',
        'bonus_value',
        'unit_value_before_taxes',
        'inventory_adjustment',
        'iva_percentage',
        'consumption_tax_percentage',
        'discount_value',
        'inventory_value',
        'freight_value',
        'insurance_value',
        'unit_warehouse_balance',
        'value_warehouse_balance',
        'unit_balance',
        'value_balance',
        'warehouse_id',
        'original_quantity',
        'unit_value_after_taxes',
        'promotion_id',
        'is_benefit',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocumentProduct::class;
    }

    /**
     * @param $id
     * @param null $request
     * @return mixed
     */
    public function getDocumentProductForId($id, $request = null)
    {
        if (isset($request->documentProduct) && !empty($request->documentProduct)) {
            $document_product = $request->documentProduct;
        } else {
            $document_product = DocumentProduct::find($id);
        }

        $documentProduct = DocumentProduct::where('id', $id)
            ->with(
                [
                    'product' => function ($product) use ($document_product) {
                        $product->with(
                            [
                                'line.taxes',
                                'subline',
                                'brand',
                                'subbrand',
                                'price',
                                'inventory' => function ($inventory) use ($document_product) {
                                    $inventory->where('branchoffice_warehouse_id', $document_product->branchoffice_warehouse_id);
                                },
                            ]
                        );
                    },
                    'seller',
                    'documentProductTaxes.tax',
                    'promotion',
                    'document',
                    'account',
                ]
            )
            ->first();

        if (!empty($request)) {
            $this->addMinimalPriceDocumentProducts($documentProduct, $request);
        }

        return $documentProduct;
    }

    public function getProductsImport($document_id, $request)
    {

        $documents_products = DocumentProduct::where('document_id', $document_id)
        ->with([
            'document',
            'product', 
            'product.line', 
            'baseDocumentProduct'=>function($query){
                $query->with(['document','product']);
            }
        ])
        ->orderBy('id')
        ->get();

        $expenses = DocumentTransactions :: where('projects_id', $request->projects_id)
        ->with([
            'account',
            'account.line',
            'document',
            'document.vouchertype',
            'document.user'
        ])
        ->whereHas('document' , function($query){
            $query->where('in_progress', false);
        })
        ->whereNotNull('projects_id')
        ->get();

        // listamos las lineas de las cuentas no listamos null
        $accounts_lines = $expenses->where('account.line_id', !null)->pluck('account.line_id')->unique()->all();

        //calculamos es vr bruto del en COP de cada producto
        foreach ($documents_products as $key => $documents_product) {
            $documents_product->total_value_brut=$documents_product->total_value_brut_us * $documents_product->baseDocumentProduct->document->trm_value;
        }

        // total vr bruto de los productos de la/s factura/s
        $total_value_brut = $documents_products/*->whereNotIn('product.line_id', $accounts_lines)*/->sum('total_value_brut');
        $total_expenses = $expenses->where('account.line_id', null)->sum('operation_value');

        //total gastos por linea
        $total_expenses_lines = [];
        foreach ($expenses as $key => $expense) {
            if (!is_null($expense->account->line_id)) {
                if (isset($total_expenses_lines[$expense->account->line_id])) {
                    $total_expenses_lines[$expense->account->line_id] +=  $expense->operation_value;
                }else{
                    $total_expenses_lines[$expense->account->line_id] =  $expense->operation_value;
                }
            }
        }


        //total bruto por linea
        $total_value_brut_by_line = [];
        foreach ($documents_products as $key => $documents_product) {
            if (isset($total_value_brut_by_line[$documents_product->product->line_id])) {
                $total_value_brut_by_line[$documents_product->product->line_id] += $documents_product->total_value_brut;
            }else{
                $total_value_brut_by_line[$documents_product->product->line_id] = $documents_product->total_value_brut;
            }
        }

        // calculamos la participacion por linea o por vr bruto y lo aplicamos al valor de los gastos
        foreach ($documents_products as $key => $documents_product) {
            
            //calcular vr de gastos
            $documents_product->participation_percentage = $total_value_brut == 0 ? 0 : $documents_product->total_value_brut*100/$total_value_brut;
            $documents_product->vr_expenses = floor($documents_product->participation_percentage * $total_expenses/100);
            $documents_product->participation_percentage = number_format($documents_product->participation_percentage, 2, ',', '.');

            if (in_array($documents_product->product->line_id, $accounts_lines) ) {
                //participacion por linea
                $documents_product->participation_percentage_line = 
                $total_value_brut_by_line[$documents_product->product->line_id] == 0 ? 0 : $documents_product->total_value_brut*100/$total_value_brut_by_line[$documents_product->product->line_id];
                $documents_product->vr_expenses = floor($documents_product->participation_percentage_line * $total_expenses_lines[$documents_product->product->line_id]/100);
                $documents_product->participation_percentage_line = number_format($documents_product->participation_percentage_line, 2, ',', '.');
            }

        }

        // calculamos si hay diferencia entre el total gastos de los productos y el total de las trasaccioens
        $total_expenses_documents_product = $documents_products->sum('vr_expenses');
        $difference = floor($total_expenses + array_sum($total_expenses_lines) - $total_expenses_documents_product);

        // repartimos la diferencia para que el total gastos de productos sea al total de las transacciones
        while ($difference > 0) {
            foreach ($documents_products as $key => $documents_product) {
                if ($difference == 0) continue;
                $documents_product->vr_expenses++;
                $difference--;
            }
        }

        foreach ($documents_products as $key => $documents_product) {
            $documents_product->total_value = round($documents_product->vr_expenses + $documents_product->total_value_brut);
            $documents_product->unit_value_before_taxes = $documents_product->quantity == 0 ? 0 : $documents_product->total_value_brut / $documents_product->quantity;
        }

        return (object)[
            'expenses' => $expenses,
            'documents_product_import'=>$documents_products
        ];
    }


    /**
     * @param array $product
     * @param null $document_product_id
     * @return LengthAwarePaginator|Collection|mixed
     */
    public function create(array $product, $document_product_id = null)
    {
        if ($document_product_id) {
            $document_product = DocumentProduct::find($document_product_id);
            if ($document_product){
                $document_product->update($product);
            }
        } else {
            $repeated_product = DocumentProduct::select('id', 'product_repeat_number')
                ->where('document_id', $product['document_id'])
                ->where('product_id', $product['product_id'])
                ->get();

            if ($repeated_product->count() > 0) {
                $max_product_repeat_number = $repeated_product->max('product_repeat_number');
                $product['product_repeat_number'] = $max_product_repeat_number + 1;
            } else {
                $product['product_repeat_number'] = 0;
            }
            $document_product = DocumentProduct::create($product);
        }

        return $document_product;
    }

    public function createTaxes($taxesDocumentProduct)
    {
        DocumentProductTax::updateOrCreate(
            Arr::only($taxesDocumentProduct, ['document_product_id', 'tax_id']),
            $taxesDocumentProduct
        );
    }

    public function validateMovements($id)
    {
        $documents_products = DocumentProduct::select('id')
            ->where('product_id', $id)
            ->wherehas('document', function ($query) {
                $query->where('in_progress', false);
            })
            ->with(['document' => function ($query) {
                $query->where('in_progress', false);
            }])
            ->first();

        if (!is_null($documents_products)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * consulta para kardex
     *
     * @param $request filtros para la consulta
     * @author Santiago Torres
     */
    public function searchKardex($request)
    {
        $fecha_icicio = $request->date[0] == "null" ? '2020-07-01 00:00:00' : $request->date[0] . ' 00:00:00';
        $fecha_fin = $request->date[1] == "null" ? date('Y-m-d H:i:s') : $request->date[1] . ' 23:59:59';
        $kardex = Document::whereBetWeen('document_date', [$fecha_icicio, $fecha_fin])
            ->where('in_progress', false)
            ->with(['documentsProducts' => function ($query) use ($request) {
                $query->where('product_id', $request->product);
                $query->where('manage_inventory', true);
                // $query->where('quantity', '<>', 0);
                $query->where('branchoffice_warehouse_id', $request->bodega_salida);
                $query->with(['operationType' => function ($query) {
                    $query->where('affectation', '<>', 3);
                }]);
                $query->whereHas('operationType', function ($query) {
                    $query->where('affectation', '<>', 3);
                });
                $query->with(['document' => function ($query) use ($request) {
                    if ($request->contact_id) {
                        $query->where('contact_id', $request->contact_id);
                    }
                    $query->with(['vouchertype']);
                    $query->with('branchofficeWarehouse');
                    $query->with('branchoffice');
                    $query->with('document');
                }]);
                $query->orderBy('id', 'desc');
            }])
            ->whereHas('documentsProducts', function ($query) use ($request) {
                $query->where('product_id', $request->product);
                $query->where('manage_inventory', true);
                // $query->where('quantity', '<>', 0);
                $query->where('branchoffice_warehouse_id', $request->bodega_salida);
                $query->with(['operationType' => function ($query) {
                    $query->where('affectation', '<>', 3);
                },
                    'template',
                ]);
                $query->whereHas('operationType', function ($query) {
                    $query->where('affectation', '<>', 3);
                });
                $query->with(['document' => function ($query) use ($request) {
                    if ($request->contact_id) {
                        $query->where('contact_id', $request->contact_id);
                    }
                    $query->with(['vouchertype']);
                    $query->with('branchofficeWarehouse');
                    $query->with('document');
                }]);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('document_date', 'ASC')
            ->get();

        $fullKardex = [];

        foreach ($kardex as $key => $doc) {
            foreach ($doc->documentsProducts as $key => $value) {
                $set_product = true;
                if ($value->quantity == 0) {
                    // if ($doc->operationType_id != 1 && $doc->operationType_id != 2) { //solo salen de las devoluciones
                    if (!$doc->template->credit_note) { // los documentos que el modelo tenga nota credito
                        $set_product = false;
                    }

                    if ($doc->template->code == '262') {
                        $set_product = true;
                    }
                }

                if ($set_product) {
                    $fecha = explode(" ", $value->document->document_date);
                    $value->fecha = $fecha[0];
                    $value->hora = $fecha[1];
                    if ($value->quantity != 0) {
                        $value->Vr_Un_Costo = number_format($value->inventory_cost_value / $value->quantity, 2, '.', '');
                    } else {
                        $value->Vr_Un_Costo = 0;
                    }

                    if ($value->stock_after_operation != 0) {
                        $value->Cto_Prom = number_format($value->stock_value_after_operation / $value->stock_after_operation, 2, '.', '');
                    } else {
                        $inv = Inventory::where('product_id', $value->product_id)->where('branchoffice_warehouse_id', $value->document->branchoffice_warehouse_id)->first();
                        $value->Cto_Prom = 0;
                    }
                    $value->operation = $this->operationKardex($value);
                    $value->base_document = is_null($value->document->document) ? null :  (is_null($value->document->document->prefix) || empty($value->document->document->prefix) ? $value->document->document->consecutive :  $value->document->document->prefix . ' - ' . $value->document->document->consecutive);
                    $value->crossig_document = is_null($value->document->crossing_prefix) ? $value->document->crossing_prefix .' - ' . $value->document->crossing_number_document : null ;


                    $value->document_number = is_null($value->document->prefix) || empty($value->document->prefix) ? $value->document->consecutive :  $value->document->prefix . ' - ' . $value->document->consecutive;
                    $value->entrace = $value->operationType->affectation == '1' ? $value->quantity : null;
                    $value->exits = $value->operationType->affectation == '2' ? $value->quantity : null;

                    $fullKardex[] = $value;
                }
            }
            $value->document->prefix = trim($value->document->prefix);
            if ($value->document->document != null) {
                $value->document->document->prefix = trim($value->document->document->prefix);
            }
        }
        return $fullKardex;
        
    }

    public function operationKardex($value)
    {
        $operation = null;

        $operation = $value->document->vouchertype->short_name;

        if (!is_null($value->operationType)) {
            if ($value->operationType->code == '240') {
                $operation = 'Sal Tras';
            }elseif ($value->operationType->code == '140') {
                $operation = 'Ent Tras';
            }
        }


        return $operation;
    }

    /**
     * obtiene los docuementsProducts para recontruir la información
     *
     * @author Santiago Torres
     */
    public function searchDocumentsProductsReconstructor($request)
    {
        return DocumentProduct::select('id', 'document_id')
            ->where('product_id', $request->product)
            ->where('id', '>=', $request->document_product)
            ->orderBy('id', 'ASC')
            ->get();

    }

    /**
     * Obtiene el ultimo documentproduct según el id
     *
     * @author Santiago Torres
     */
    public function getStockAndStockValues($request, $branchoffice_warehouse_id, $product_id)
    {
        //return $branchoffice_warehouse_id;//
        $documentProdu = DocumentProduct::select('stock_after_operation', 'stock_value_after_operation')
        // ->where('id', '<' ,$request->document_product)
            ->where('stock_after_operation', '<>', null)
            ->whereHas('document', function ($query) use ($request, $branchoffice_warehouse_id) {
                $query->where('branchoffice_warehouse_id', $branchoffice_warehouse_id);
                // $query->orderby('created_at','DESC');
            });

        if ($product_id) {
            $documentProdu->where('product_id', $product_id);
        }

        if ($request->document_product) {
            $documentProdu->where('id', '<', $request->document_product);
        }

        $documentProdu->orderby('created_at', 'DESC');
        return $documentProdu->first();
    }

    /**
     *obtiene los productos para reconstruir el inventrio
     *
     * @author santiago torres
     */

    public function getProductFromDocuments()
    {
        return DocumentProduct::select('product_id')->distinct()->get();
    }

    public function updateCost($id, $new_cost)
    {
        return DocumentProduct::where('id', $id)
            ->update(['inventory_cost_value' => $new_cost]);

    }

    public function getInfoForOperationsReport($request)
    {
        if ($request->detailed_by_product == 1) {
            $parameters = [
                $request->date[0] . ' 00:00:00',
                $request->date[1] . ' 23:59:59',
                '{' . implode(", ", $request->type_operations) . '}',
                !is_null($request->branchoffice_warehouse) ? $request->branchoffice_warehouse : null,
                !is_null($request->template) ? $request->template : null,
                !is_null($request->filter_lines) ? '{' . implode(", ", $request->filter_lines) . '}' : null,
                !is_null($request->filter_sublines) ? '{' . implode(", ", $request->filter_sublines) . '}' : null,
                !is_null($request->filter_brands) ? '{' . implode(", ", $request->filter_brands) . '}' : null,
                !is_null($request->filter_subbrands) ? '{' . implode(", ", $request->filter_subbrands) . '}' : null,
                !is_null($request->product) ? '{' . implode(", ", $request->product) . '}' : null,
                !is_null($request->contact) ? '{' . implode(", ", $request->contact) . '}' : null,
                !is_null($request->city) ? '{' . implode(", ", $request->city) . '}' : null,
                !is_null($request->seller) ? '{' . implode(", ", $request->seller) . '}' : null,
                !is_null($request->f_exclude_lines) ? '{' . implode(", ", $request->f_exclude_lines) . '}' : null,
                !is_null($request->f_exclude_template) ? '{' . implode(", ", $request->f_exclude_template) . '}' : null,
            ];

            $documentProducts = DB::select("select * from public.fn_oper_report(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", $parameters);
        } elseif ($request->detailed_by_product == 2) {
            $parameters = [
                $request->date[0] . ' 00:00:00',
                $request->date[1] . ' 23:59:59',
                '{' . implode(", ", $request->type_operations) . '}',
                !is_null($request->branchoffice_warehouse) ? $request->branchoffice_warehouse : null,
                !is_null($request->template) ? $request->template : null,
                !is_null($request->filter_lines) ? '{' . implode(", ", $request->filter_lines) . '}' : null,
                !is_null($request->filter_sublines) ? '{' . implode(", ", $request->filter_sublines) . '}' : null,
                !is_null($request->filter_brands) ? '{' . implode(", ", $request->filter_brands) . '}' : null,
                !is_null($request->filter_subbrands) ? '{' . implode(", ", $request->filter_subbrands) . '}' : null,
                !is_null($request->product) ? '{' . implode(", ", $request->product) . '}' : null,
                !is_null($request->contact) ? '{' . implode(", ", $request->contact) . '}' : null,
                !is_null($request->city) ? '{' . implode(", ", $request->city) . '}' : null,
                !is_null($request->seller) ? '{' . implode(", ", $request->seller) . '}' : null,
                !is_null($request->f_exclude_lines) ? '{' . implode(", ", $request->f_exclude_lines) . '}' : null,
            ];

            $documentProducts = DB::select("select * from public.fn_operation_report_document(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", $parameters);
        }

        return $documentProducts;
    }

    public function getReturns($id)
    {
        return Document::whereIn('thread', $id)
            ->where('in_progress', false)
            ->where('model_id', 21)
            ->select('id', 'operationType_id', 'branchoffice_id', 'contact_id', 'seller_id', 'vouchertype_id')
            ->with([
                'vouchertype' => function ($query) {
                    $query->select('name_voucher_type', 'id');
                },
                'operationType' => function ($query) {
                    $query->select('id', 'code', 'description');
                },
                'branchoffice' => function ($query) {
                    $query->select('name', 'id');
                },
                'contact' => function ($query) {
                    $query->select('identification', "name", 'id', 'city_id', 'surname');
                    $query->with(['city' => function ($query) {
                        $query->select('id', 'city');
                    }]);
                },
                'seller' => function ($query) {
                    $query->select('identification', "name", 'id', 'surname');
                },
                'documentsProducts' => function ($query) {
                    $query->select('id', 'document_id', 'product_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'unit_value_before_taxes', 'quantity', 'total_value_brut', 'inventory_cost_value', 'operationType_id', 'created_at');
                    $query->with([
                        'product' => function ($query) {
                            $query->select('id', 'code', 'description', 'line_id', 'subline_id', 'brand_id', 'subbrand_id');
                            $query->with([
                                'line',
                                'subline',
                                'brand',
                                'subbrand',
                            ]);
                        },
                        'line' => function ($query) {
                            $query->select('id', 'line_description');
                        },
                        'subline' => function ($query) {
                            $query->select('id', 'subline_description');
                        },
                        'brand' => function ($query) {
                            $query->select('id', 'description');
                        },
                        'subbrand' => function ($query) {
                            $query->select('id', 'description');
                        },
                        'documentProductTaxes' => function ($query) {
                            $query->select('id', 'tax_percentage', 'document_product_id', 'tax_id');
                            $query->where('tax_id', 1);
                        },
                        'operationType' => function ($query) {
                            $query->select('id', 'code', 'description');
                        },
                    ]);
                },
            ])
            ->get();
    }

    public function listKardex($kardex)
    {
        $data = [];
        $sald = 0;
        $entra = 0;
        foreach ($kardex as $key => $value) {
            $oper = '';
            if ($value->document->vouchertype_id == 13) {
                if ($value->operationType->affectation == 2) {
                    $oper = 'Sal Tras';
                }
                if ($value->operationType->affectation == 1) {
                    $oper = 'Ent Tras';
                }
            } else {
                $oper = $value->document->getDocumentProductForIdvouchertype->getDocumentProductForIdshort_name;
            }
            $data[] = [
                'fecha' => $value->fecha,
                'Docum' => $value->document->prefix ? $value->document->prefix . '-' . $value->document->consecutive : null,
                'operac' => $oper,
                'vr_Unitario' => $value->unit_value_before_taxes,
                'vr_Bruto' => $value->total_value_brut,
                'vr_Un_Costo' => $value->Vr_Un_Costo,
                'total_costo' => $value->inventory_cost_value,
                'entradas' => $value->operationType->affectation == 1 ? $value->quantity : '',
                'salidas' => $value->operationType->affectation == 2 ? $value->quantity : '',
                'saldo_un' => $value->stock_after_operation,
                'saldo_Vr' => $value->stock_value_after_operation,
                'cto_prom' => $value->Cto_Prom,
                'doc_base' => $value->document->document ? ($value->document->document->prefix ? $value->document->document->prefix . '-' . $value->document->document->consecutive : $value->document->document->consecutive) : '',
                'cruce' => $value->document->affects_prefix_document && $value->document->affects_number_document ? $value->document->affects_prefix_document . '-' . $value->document->affects_number_document : '',
            ];

            if ($value->operationType->affectation == 1) {
                $entra += $value->quantity;
            }

            if ($value->operationType->affectation == 2) {
                $sald += $value->quantity;
            }

        }
        $inventory = Inventory::where('product_id', $kardex[0]->product_id)->where('branchoffice_warehouse_id', $kardex[0]->document->branchoffice_warehouse_id)->first();
        $data[] = [
            'fecha' => '',
            'Docum' => '',
            'operac' => '',
            'vr_Unitario' => '',
            'vr_Bruto' => '',
            'vr_Un_Costo' => '',
            'total_costo' => '',
            'entradas' => $entra,
            'salidas' => $sald,
            'saldo_un' => $inventory->stock,
            'saldo_Vr' => $inventory->stock_values,
            'cto_prom' => $inventory->average_cost,
            'doc_base' => '',
            'cruce' => '',
        ];

        return $data;
    }

    public function getProductsFromDocument($document_id)
    {
        return DocumentProduct::select('product_id')
            ->where('document_id', $document_id)->distinct()->withTrashed()->get();
    }

    /**
     * consulta para obtener los saldos anteriores al documento
     *
     * @author Santiago Torres
     */
    public function getBeforeBalancesDocument($product_id, $date, $branchoffice_warehouse_id)
    {
        $kardex = DocumentProduct::where('product_id', $product_id)
            ->where('manage_inventory', true)
            ->where('stock_after_operation', '<>', null)
            ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->with(['operationType' => function ($query) {
                $query->where('affectation', '<>', 3);
            }])
            ->whereHas('operationType', function ($query) {
                $query->where('affectation', '<>', 3);
            })
            ->with(['document' => function ($query) use ($date, $branchoffice_warehouse_id) {
                $query->where('in_progress', false);
                //$query->where('branchoffice_warehouse_id', $branchoffice_warehouse_id);
                $query->where('document_date', '<', $date);
            }])
            ->whereHas('document', function ($query) use ($date, $branchoffice_warehouse_id) {
                $query->where('in_progress', false);
                //$query->where('branchoffice_warehouse_id', $branchoffice_warehouse_id);
                $query->where('document_date', '<', $date);
            })
            ->get()->sortBy('document.id')->sortBy('document.document_date')->values()->last();

        return $kardex;
    }

    public function searcZeroReturns()
    {
        return DocumentProduct::where('inventory_cost_value', null)
            ->where('operationType_id', 20)
            ->with(['document' => function ($query) {
                $query->where('in_progress', false);
            }])
            ->whereHas('document', function ($query) {
                $query->where('in_progress', false);
            })
            ->get();
    }

    public function SearchCostProduct($product_id, $affect)
    {
        return Document::select('id', 'in_progress', 'document_date', 'branchoffice_warehouse_id')
            ->where('in_progress', false)
            ->with(['documentsProducts' => function ($query) use ($product_id, $affect) {
                $query->select('id', 'document_id', 'operationType_id', 'product_id', 'inventory_cost_value');
                $query->where('product_id', $product_id);
                $query->where('inventory_cost_value', '<>', 0);
                $query->with(['operationType' => function ($query) use ($affect) {
                    $query->select('id', 'affectation');
                    $query->where('affectation', 1);
                }]);
                $query->whereHas('operationType', function ($query) use ($affect) {
                    $query->select('id', 'affectation');
                    $query->where('affectation', 1);
                });
            }])
            ->whereHas('documentsProducts', function ($query) use ($product_id, $affect) {
                $query->select('id', 'document_id', 'operationType_id', 'product_id', 'inventory_cost_value');
                $query->where('product_id', $product_id);
                $query->where('inventory_cost_value', '<>', 0);
                $query->with(['operationType' => function ($query) use ($affect) {
                    $query->select('id', 'affectation');
                    $query->where('affectation', 1);
                }]);
                $query->whereHas('operationType', function ($query) use ($affect) {
                    $query->select('id', 'affectation');
                    $query->where('affectation', 1);
                });
            })
            ->orderBy('document_date')
            ->first();
        // ->get()->sortBy('document.document_date')->values()->last();
    }

    public function setCost($d_p_id, $cost)
    {

        $dp = DocumentProduct::where('id', $d_p_id)->first();
        $dp->inventory_cost_value = $cost;
        $dp->fixed_cost = true;
        $dp->save();

        return $dp;
    }

    public function addMinimalPriceDocumentProducts($documentProduct, $request)
    {
        if (is_array($request)) {
            if (isset($request['model_id']) && !empty($request['model_id'])) {
                $model = $this->getModelForIdFromCache($request['model_id']);
            }
        } else {
            if (isset($request->model_id) && !empty($request->model_id)) {
                $model = $this->getModelForIdFromCache($request->model_id);
            }
        }

        if (!empty($model->minimal_price)) {

            $branchoffice_warehouse_id = $documentProduct->branchoffice_warehouse_id ? $documentProduct->branchoffice_warehouse_id : $request->branchoffice_warehouse_id;

            $priceListId = $model->minimal_price;

            // lista de precio COSTO PROMEDIO
            if ($priceListId == '24463') {
                $inventory = Inventory::where('product_id', $documentProduct->product->id)
                    ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
                    ->first();

                if ($inventory) {
                    $documentProduct->minimalPrice = [
                        'price' => round($inventory->average_cost),
                        'product_id' => $documentProduct->product->id,
                        'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                        'branchoffice_id' => null,
                    ];
                } else {
                    $documentProduct->minimalPrice = [
                        'price' => 0,
                        'product_id' => $documentProduct->product->id,
                        'branchoffice_id' => null,
                    ];
                }

            } else {
                // Validamos por SEDE
                $minimalPrice = Price::where('product_id', $documentProduct->product_id)
                    ->where('price_list_id', $priceListId)
                    ->where(function ($branchoffice) use ($documentProduct) {
                        $branchoffice->where('branchoffice_id', $documentProduct->document->branchoffice_id)
                            ->orWhereNull('branchoffice_id');
                    })
                    ->orderBy('branchoffice_id', 'ASC')
                    ->get();

                $documentProduct->minimalPrice = $minimalPrice->first();
            }
        }

        return $documentProduct;
    }

    public function setInfoRebuild($id, $user_rebuild, $rebuild_date, $stock_before_rebuild)
    {
        DocumentProduct::where('id', $id)
            ->update(['reconstruction_date' => $rebuild_date,
                'user_reconstruction' => $user_rebuild,
                'stock_before_reconstruction' => $stock_before_rebuild]);
    }

    public function getDocumentExistWms($id_document)
    {
        $documentProduct = DocumentProduct::where("document_id", "=", $id_document)->count();
        return $documentProduct;
    }

    public function getDocumentProductExistWms($id_document,$id_document_product,$code_product)
    {
        $documentProduct = DocumentProduct::join('products', 'products.id', '=', 'documents_products.product_id')
                                            ->select("products.id", "products.code")
                                            ->where("documents_products.id","=",$id_document_product)
                                            ->first();
        //dd($documentProduct);
        return $documentProduct;
    }

    public function updateDocumentProductExistWms($id_document_product, $quantity_total, $lot)
    {
        $documentUpdateProduct = DocumentProduct::where(["documents_products.id" => $id_document_product])
            ->update(['quantity_wms' => $quantity_total,
                      'lot_wms' => $lot
                    ]);
        $documentProduct = DocumentProduct::join('products', 'products.id', '=', 'documents_products.product_id')
                                            ->select("documents_products.document_id as id_document","products.id as id_product",
                                            "products.code as code_product", "documents_products.quantity_wms", "documents_products.lot_wms")
                                            ->where([
                                                     "documents_products.id" => $id_document_product
                                                    ])
                                            ->first();

        return $documentProduct;
    }

    public function updateDocumentsWms($id_document)
    {
        $documentUpdate = Document::where(["documents.id" => $id_document])
            ->update(['wms_reception_dispatch' => true
                    ]);
        return $documentUpdate;
    }

    public function updateDocumentsInformationWms($id_document, $nit_protractor, $guide_number,$name_user_wms)
    {
        $id_protractor = Contact::where("identification","=",$nit_protractor)->select('id')->first();
        $protractor = $id_protractor->id;

        $documentUpdateInformation = DocumentInformation::where(["documents_informations.document_id" => $id_document])
            ->update(['protractor' => $protractor,
                      'guide_number' => $guide_number,
                      'name_user_wms' => $name_user_wms,
                      'date_hour_consume' => now()
                    ]);

        return $documentUpdateInformation;
    }

}
