<?php

namespace App\Repositories;

use App\Entities\RequestProduct;
use App\Entities\Parameter;
use App\Entities\RequestHeader;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RequestProductRepository
 * @package App\Repositories
 * @version September 1, 2019, 3:53 am UTC
 *
 * @method RequestProduct findWithoutFail($id, $columns = ['*'])
 * @method RequestProduct find($id, $columns = ['*'])
 * @method RequestProduct first($columns = ['*'])
*/
class RequestProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'request_header_id',
        'product_id',
        'quantity',
        'serial',
        'certificate_expiration_date',
        'fault_description',
        'photo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestProduct::class;
    }
    public function getRequestType(){
        return Parameter::select('id','name_parameter')->where('paramtable_id',70)->get();
    }
    //Funcion para crear la solicitd de servicio
    public function createData($request){
      try {
        $requestHeader= new RequestHeader();
        $requestHeader->contact_id=$request->contact_id;
        $requestHeader->contacts_warehouse_id=$request->contacts_warehouse_id;
        $requestHeader->request_date=$request->request_date;
        $requestHeader->request_type=$request->request_type;
        $requestHeader->maintenance_type=$request->maintenance_type;
        //Calculo si request_type es igual a RecoleccionChatarra
        if($request->request_type==4){
            //Si cumple le asigno a units el valor obtenido en el formulario
            $requestHeader->units=$request->units;
        }else{
            // Si no cumplo agrego a units el tamaño del arreglo de products
            $requestHeader->units=sizeof($request->request_products);
        }
        $requestHeader->direc_observation=$request->direc_observation;
        $requestHeader->customer_observation=$request->customer_observation;
        $requestHeader->provider_observation=$request->provider_observation;
        $requestHeader->request_date=date('Y-m-d');
        $requestHeader->save();
        if($request->request_type!=4){
           // Recorro el arreglo de producst para hacer el insert respectivo al tamaño del arreglo
            foreach($request->request_products as $product){
                $requestProduct= new RequestProduct();
                $requestProduct->request_header_id=$requestHeader->id;
                $requestProduct->product_id=$product['id'];
                $requestProduct->quantity=$product['quantity'];
                $requestProduct->serial=$product['serial'];
                $requestProduct->certificate_expiration_date=$product['certificate_expiration_date'];
                $requestProduct->fault_description=$product['fault_description'];
                $requestProduct->save();
            }
         }
        return $requestHeader;
      } catch (\Exception $th) {
          return $th;
      }
        // return $request;
    }
}
