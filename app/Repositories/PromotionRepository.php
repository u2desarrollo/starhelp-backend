<?php

namespace App\Repositories;

use App\Entities\Line;
use App\Entities\Product;
use App\Entities\ProductPromotion;
use App\Entities\Promotion;
// use App\Traits\CollectionsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use InfyOm\Generator\Common\BaseRepository;
use App\Traits\CollectionsTrait;
use App\Traits\LoadImageTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File as FileFacades;

/**
 * Class PromotionRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:32 pm UTC
 *
 * @method Promotion findWithoutFail($id, $columns = ['*'])
 * @method Promotion find($id, $columns = ['*'])
 * @method Promotion first($columns = ['*'])
 */
class PromotionRepository extends BaseRepository
{

    use CollectionsTrait, LoadImageTrait;
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion_type_id',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'description',
        'start_date',
        'end_date',
        'minimum_gross_value',
        'only_available',
        'discount',
        'pay_soon',
        'cash_payment',
        'gift_type',
        'specific_product_id',
        'image', 'minimum_amount',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promotion::class;
    }


    public function deleteProductPromotions($idPromotion)
    {
        $productPromotion = ProductPromotion::where('promotion_id', $idPromotion)->get();
        if (count($productPromotion) > 0) {
            foreach ($productPromotion as $key) {
                $key->delete();
            }
        }
    }

    public function findAll(Request $request)
    {
        $promotions = Promotion::select('promotions.id', 'start_date', 'end_date', 'promotions.description', 'promotion_type_id', 'b.name as name_branchoffice')
            ->when($request->filter != null, function ($query) use ($request) {
                $query->when($request->filter == 'Proxima', function ($query) use ($request) {
                    $query->where('start_date', '>', now())->where('end_date', '>', now());
                });

                $query->when($request->filter == 'Vigente', function ($query) use ($request) {
                    $query->where('start_date', '<=', now())->where('end_date', '>=', now());
                });

                $query->when($request->filter == 'Vencida', function ($query) use ($request) {
                    $query->where('start_date', '<', now())->where('end_date', '<=', now());
                });
            })
            ->when($request->branchoffice && !empty($request->branchoffice), function ($query) use ($request) {
                $query->where('branchoffice_id', $request->branchoffice);
            })
            ->when($request->line && !empty($request->line), function ($query) use ($request) {
                $query->where(function ($subquery) use ($request) {
                    $subquery->where('line_id', $request->line)
                        ->orWhereHas('productsPromotions', function ($productPromotion) use ($request) {
                            $productPromotion->whereHas('product', function ($query) use ($request) {
                                $query->where('line_id', $request->line);
                            });
                        });
                });
            })
            ->when($request->brand && !empty($request->brand), function ($query) use ($request) {
                $query->where('brand_id', $request->brand);
            })
            ->join('branchoffices AS b', 'b.id', '=', 'promotions.branchoffice_id')
            ->with([
                'totalBrutDocumentsProducts',
                'totalInventoryCostDocumentsProducts',
                'totalQuantityDocumentsProducts',
                'totalApplicationsInvoicesDocumentsProducts',
                'totalApplicationsDevolutionsDocumentsProducts'
            ])
            //->orderBy('description','asc')
            ->orderByRaw('b.name,promotions.start_date  asc')
            ->paginate($request->perPage);


        // select('promotions.*')
        // ->selectRaw("case when start_date > now() AND end_date > now() then 'Proxima'when start_date <= now() AND end_date >= now() then 'Vigente'when start_date < now() AND end_date <= now() then 'Vencida'end as state")
        // ->when(isset($request->filter) && $request->filter != '', function ($subquery) use ($request){
        // 	$subquery->whereRaw("(case when start_date > now() AND end_date > now() then 'Proxima'when start_date <= now() AND end_date >= now() then 'Vigente'when start_date < now() AND end_date <= now() then 'Vencida'end) = ?", [$request->filter]);
        // })
        return $promotions;
    }

    public function findForId($id, $request)
    {
        try {
            $promotion = Promotion::/* select('id', 'promotion_type_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id',
             'description', 'minimum_gross_value', 'only_available', 'discount', 'pay_soon', 'cash_payment', 'gift_type', 'specific_product_id',
             'minimum_amount', 'branchoffice_id', 'image') */select(
                'id',
                'promotion_type_id',
                'line_id',
                'subline_id',
                'brand_id',
                'subbrand_id',
                'start_date',
                'end_date',
                'description',
                'minimum_gross_value',
                'only_available',
                'discount',
                'pay_soon',
                'cash_payment',
                'gift_type',
                'specific_product_id',
                'minimum_amount',
                'branchoffice_id',
                'image',
                'title',
                'description2'
            )


                ->where('id', $id)
                ->with([
                    'line' => function ($query) {
                        $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                    },
                    'subline',
                    'brand' => function ($query) {
                        $query->select('id', 'brand_code', 'description', 'image');
                    },
                    'subbrand' => function ($query) {
                        $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                    },
                    'productsPromotions' => function ($query) use ($request) {
                        $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                        $query->with([
                            'product' => function ($query) use ($request) {
                                $query->select('id', 'code', 'description', 'type', 'line_id');
                                $query->with([
                                    /*'line' => function ($query) use($request){
                                    $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                                    $query->with([
                                        'taxes' => function ($querys) {
                                            $querys->select('code', 'description', 'percentage_sale','percentage_purchase');
                                        },
                                    ]);
                                },*/
                                    'price' => function ($query) use ($request) {
                                        $query->select('id', 'product_id', 'price_list_id', 'branchoffice_id', 'price')->where('branchoffice_id', $request->branchoffice_storage);
                                    },
                                    'line' => function ($query) {
                                        $query->with('taxes');
                                    },
                                ]);
                            },
                        ])
                        ->whereHas('product', function ($query){
                            $query->where(function ($query) {
                                $query->orWhere('state', true);
                                $query->orWhere('state', null);
                            });
                        });
                    },
                    'specificProduct' => function ($query) {
                        $query->with([
                            'price' => function ($query) {
                                $query->select('id', 'product_id', 'price_list_id', 'branchoffice_id', 'price');
                            },
                            'line' => function ($query) {
                                $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                                $query->with([
                                    'taxes'
                                ]);
                            }
                        ]);
                    },
                    'branchoffice' => function ($query) {
                        $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                    }
                ])
                ->first();




            return $promotion;
        } catch (\Throwable $th) {
            //dd($th->getMessage());
            return $th->getMessage();
        }
    }



    public function createPromotion($request)
    {

        $image = $request->file('image');
        $data = json_decode($request->products_promotions);
        if ($image) {
            //filefacades crea la imagen
            Storage::disk('local')->put('/promotions/' . $image->getClientOriginalName(), FileFacades::get($image));
        }

        $newPromotion = new Promotion();
        $date = explode(',', $request->date);
        $input = $request->toArray();
        $input['start_date'] = $date[0];
        $input['end_date'] = $date[1];

        $newPromotion->fill(Arr::only($input, ['promotion_type_id', 'description', 'start_date', 'end_date', 'only_available', 'only_available', 'branchoffice_id', 'description2', 'title']));
        if ($image) {
            $newPromotion->image = $image->getClientOriginalName();
        } elseif (!$image && $request->image2) {
            $newPromotion->image = $request->image2;
        } elseif (!$image && !$request->image2) {
            $newPromotion->image = null;
        }
        $newPromotion->save();


        if ($request->promotion_type_id == 1) {

            foreach ($data as $key) {

                $productPromotion = new ProductPromotion();
                $productPromotion->product_id = $key->product_id;
                $productPromotion->minimum_amount = $key->minimum_amount;
                $productPromotion->discount = $key->discount;
                $productPromotion->pay_soon = $key->pay_soon;
                $productPromotion->cash_payment = $key->cash_payment;
                $newPromotion->productsPromotions()->save($productPromotion);
            }


            return $newPromotion;
        } elseif ($request->promotion_type_id == 3) {

            $newPromotion->fill(Arr::only($input, ['minimum_gross_value', 'minimum_amount', 'pay_soon', 'cash_payment', 'gift_type', 'specific_product_id']));
            $newPromotion->save();

            foreach ($data as $key) {
                $productPromotion = new ProductPromotion();
                $productPromotion->product_id = $key->product_id;
                $productPromotion->minimum_amount = $key->minimum_amount;
                $productPromotion->discount = $key->discount;
                $productPromotion->discount_value = $key->discount_value;
                $newPromotion->productsPromotions()->save($productPromotion);
            }

            return $newPromotion;
        } elseif ($request->promotion_type_id == 2) {

            $newPromotion->fill(Arr::only($input, ['minimum_gross_value', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'gift_type', 'specific_product_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id']));
            $newPromotion->save();
            return $newPromotion;
        }
    }

    public function updatePromotion($request, $id)
    {


        $selectedPromotion = Promotion::where('id', $id)->first();
        $date = explode(',', $request->date);
        $input = $request->toArray();
        $input['start_date'] = $date[0];
        $input['end_date'] = $date[1];
        $selectedPromotion->fill(Arr::only($input, ['promotion_type_id', 'description', 'start_date', 'end_date', 'only_available', 'branchoffice_id']));

        if (is_object($request->image)) {

            $image = $request->file('image');
            Storage::disk('local')->delete('/promotions/' . $selectedPromotion->image);
            $selectedPromotion->image = $image->getClientOriginalName();
            //filefacades crea la imagen
            Storage::disk('local')->put('/promotions/' . $image->getClientOriginalName(), FileFacades::get($image));
        } elseif (is_null($request->image)) {

            Storage::disk('local')->delete('/promotions/' . $selectedPromotion->image);
            $selectedPromotion->image = null;
        }


        $selectedPromotion->save();
        $data = json_decode($request['products_promotions']);

        if ($request['promotion_type_id'] == 1) {
            $this->deleteProductPromotions($request['id']);
            foreach ($data as $key  => $value) {

                $productPromotion = new ProductPromotion();

                $productPromotion->product_id = $value->product_id;
                $productPromotion->minimum_amount = $value->minimum_amount;
                $productPromotion->discount = $value->discount;
                $productPromotion->pay_soon = $value->pay_soon;
                $productPromotion->cash_payment = $value->cash_payment;

                $selectedPromotion->productsPromotions()->save($productPromotion);
            }


            return $selectedPromotion;
        } elseif ($request['promotion_type_id'] == 3) {

            $this->deleteProductPromotions($request['id']);
            $selectedPromotion->fill(Arr::only($request->toArray(), ['minimum_gross_value', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'gift_type', 'specific_product_id']));
            $selectedPromotion->save();

            foreach ($data as $key => $value) {
                $productPromotion = new ProductPromotion();
                $productPromotion->product_id = $data[$key]->product_id;
                $productPromotion->minimum_amount = $data[$key]->minimum_amount;
                $productPromotion->discount = $data[$key]->discount;
                $productPromotion->discount_value = $value->discount_value;
                $selectedPromotion->productsPromotions()->save($productPromotion);
            }

            return $selectedPromotion;
        } elseif ($request['promotion_type_id'] == 2) {
            $selectedPromotion->fill(Arr::only($request->toArray(), [
                'minimum_gross_value', 'minimum_amount', 'discount',
                'pay_soon', 'cash_payment', 'gift_type', 'line_id', 'subline_id', 'brand_id', 'subbrand_id',
                'specific_product_id'
            ]));
            $selectedPromotion->save();

            return $selectedPromotion;
        }
    }


    public function getProductByParam($param)
    {
        $products = Product::where('description', 'ILIKE', '%' . $param . '%')->orWhere('code', 'ILIKE', '%' . $param . '%')->get();
        return  $products;
    }


    public function deletePromotion($id)
    {
        $promotion = Promotion::findOrFail($id);
        if ($promotion->productsPromotions->count() > 0) {
            foreach ($promotion->productsPromotions as $key => $productPromotion) {
                $productPromotion->delete();
            }
        }
        $promotion->delete();
        return $promotion;
    }

    /**
     * Obtiene las promociones activas a la fecha.
     *
     * @author Daniel Beltrán
     *
     * @return array
     */
    public function activePromotions($request)
    {
        //Variables
        $line_id     = $request->line_id;
        $subline_id  = $request->subline_id;
        $brand_id    = $request->brand_id;
        $subbrand_id = $request->subbrand_id;
        $today = date('Y-m-d');

        $promotions = Promotion::whereRaw('? BETWEEN start_date AND end_date', [$today])
            ->with([
                'productsPromotions' => function ($query) use ($request) {
                    $query->select(DB::raw('id, promotion_id, product_id, minimum_amount, discount, pay_soon, cash_payment, discount_value, 0 as quantity'));
                    $query->with([
                        'product' => function ($subquery) use ($request) {
                            $subquery->select('id', 'code', 'description', 'quantity_available');
                            $subquery->with([
                                'price'
                            ]);
                        }
                    ]);
                },
                'line:id,line_code,line_description',
                'subline',
                'brand',
                'subbrand'
            ]);


        if ($line_id) {
            $promotions->where('line_id', $line_id);
        }

        if ($subline_id) {
            $promotions->where('subline_id', $subline_id);
        }

        if ($brand_id) {
            $promotions->where('brand_id', $brand_id);
        }

        if ($subbrand_id) {
            $promotions->where('subbrand_id', $subbrand_id);
        }

        $promotions = $promotions->orderBy('start_date', 'DESC')->get();

        return $promotions;
    }

    public function findByIdForViewAndEdit($id, $request)
    {
        try {
            $promotion = Promotion::/* select('id', 'promotion_type_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id',
             'description', 'minimum_gross_value', 'only_available', 'discount', 'pay_soon', 'cash_payment', 'gift_type', 'specific_product_id',
             'minimum_amount', 'branchoffice_id', 'image') */select(
                'id',
                'promotion_type_id',
                'line_id',
                'subline_id',
                'brand_id',
                'subbrand_id',
                'start_date',
                'end_date',
                'description',
                'minimum_gross_value',
                'only_available',
                'discount',
                'pay_soon',
                'cash_payment',
                'gift_type',
                'specific_product_id',
                'minimum_amount',
                'branchoffice_id',
                'image'
            )


                ->where('id', $id)
                ->with([
                    'line' => function ($query) {
                        $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                    },
                    'subline',
                    'brand' => function ($query) {
                        $query->select('id', 'brand_code', 'description', 'image');
                    },
                    'subbrand' => function ($query) {
                        $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                    },
                    'productsPromotions' => function ($query) use ($request) {
                        $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                        $query->with([
                            'product' => function ($query) use ($request) {
                                // $query->select('id', 'code', 'description', 'type');
                                $query->with([
                                    'line' => function ($query) use ($request) {
                                        $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                                        $query->with([
                                            'taxes' => function ($querys) {
                                                $querys->select('code', 'description', 'percentage_sale', 'percentage_purchase');
                                            },
                                        ]);
                                    },
                                    'price' => function ($query) use ($request) {
                                        $query->select('id', 'product_id', 'price_list_id', 'branchoffice_id', 'price'); //->where('branchoffice_id',$request->branchoffice_storage);
                                    },
                                ]);
                            },
                        ]);
                    },
                    'specificProduct' => function ($query) {
                        $query->with([
                            'price' => function ($query) {
                                $query->select('id', 'product_id', 'price_list_id', 'branchoffice_id', 'price');
                            },
                            'line' => function ($query) {
                                $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                                $query->with([
                                    'taxes'
                                ]);
                            }
                        ]);
                    },
                    'branchoffice' => function ($query) {
                        $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                    }
                ])
                ->first();
            $promotion->image2 = $promotion->image;
            $base_64 = null;
            if ($promotion->image) {
                if (Storage::disk('local')->exists('/promotions/' . $promotion->image)) {
                    $base_64 = base64_encode(Storage::disk('local')->get('/promotions/' . $promotion->image));
                }
                $promotion->image =  $base_64 ? 'data:image/png;base64,' . $base_64 : null;
            }



            return $promotion;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
