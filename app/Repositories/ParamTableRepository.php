<?php

namespace App\Repositories;

use App\Entities\ParamTable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ParamTableRepository
 * @package App\Repositories
 * @version December 18, 2018, 4:34 pm UTC
 *
 * @method ParamTable findWithoutFail($id, $columns = ['*'])
 * @method ParamTable find($id, $columns = ['*'])
 * @method ParamTable first($columns = ['*'])
 */
class ParamTableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subscriber_id',
        'code_table',
        'name_table',
        'desc_alphanum_data_first',
        'desc_alphanum_data_second',
        'desc_alphanum_data_third',
        'desc_numeric_data_first',
        'desc_numeric_data_second',
        'desc_numeric_data_third',
        'desc_date_data_first',
        'desc_date_data_second',
        'desc_date_data_third',
        'observation',
        'protected',
        'desc_image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ParamTable::class;
    }

    /**
     * Método para filtrar paramTables de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $paramTables colección de paramTables
     */
    public function findAll($request)
    {
        $search = isset($request->search) ? $request->search : null;
        $perPage = isset($request->perPage) ? $request->perPage : null;

        $paramTables = ParamTable::select('*');
        
        if ($search != null) {
            $paramTables
            ->orWhere('code_table', 'ILIKE', '%' . $search . '%')
            ->orWhere('name_table', 'ILIKE', '%' . $search . '%');
        }

        if ($request->paginate && ($request->paginate === "false" || $request->paginate === false)) {
            $data = $paramTables->get();
        } else {
            $data = $paramTables->paginate($perPage);
        }
               

        return $data;

    }

    public function findForId($id){
        $paramTables = ParamTable::where('id', $id)->first();
        return $paramTables;
    }

}
