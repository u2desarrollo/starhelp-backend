<?php

namespace App\Repositories;

use App\Entities\ContactProvider;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactProviderRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:27 pm UTC
 *
 * @method ContactProvider findWithoutFail($id, $columns = ['*'])
 * @method ContactProvider find($id, $columns = ['*'])
 * @method ContactProvider first($columns = ['*'])
*/
class ContactProviderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'category_id',
        'seller_id',
        'percentage_commision',
        'contract_number',
        'code_price_list',
        'priority',
        'percentage_discount',
        'credit_quota_customer',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment3',
        'replacement_days',
        'electronic_order',
        'update_shopping_list',
        'calculate_sale_prices',
        'last_purchase',
        'observation',
        'blocked',
        'image',
        'retention_source',
        'percentage_retention_source',
        'retention_iva',
        'percentage_retention_iva',
        'retention_ica',
        'percentage_retention_ica',
        'code_ica_id',
        'payment_bank_code',
        'account_type',
        'account_number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactProvider::class;
    }

    public function searchConveyor()
    {
        return ContactProvider :: where ('category_id', 917)
            ->with('contact')
            ->whereHas('contact')
            ->get();
    }
}
