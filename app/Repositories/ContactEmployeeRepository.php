<?php

namespace App\Repositories;

use App\Entities\ContactEmployee;
use App\Entities\Contact;
use App\Entities\CashReceiptsApp;
use App\Entities\User;
use App\Entities\Document;
use App\Mail\AuroraMail;
use App\Http\Controllers\ApiControllers\DocumentPDFController;
use App\Http\Controllers\ApiControllers\DocumentAPIController;
use Illuminate\Http\Request;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

/**
 * Class ContactEmployeeRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:26 pm UTC
 *
 * @method ContactEmployee findWithoutFail($id, $columns = ['*'])
 * @method ContactEmployee find($id, $columns = ['*'])
 * @method ContactEmployee first($columns = ['*'])
*/
class ContactEmployeeRepository extends BaseRepository
{

    private $documentPDFController;
    private $documentAPIController;

    public function __construct(DocumentPDFController $documentPDFController, DocumentAPIController $documentAPIController)
    {
        $this->documentPDFController = $documentPDFController;
        $this->documentAPIController = $documentAPIController;
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'category_id',
        'cost_center_id',
        'contract_number',
        'percent_commision',
        'position_id',
        'observation',
        'photo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactEmployee::class;
    }

    /**
     * Obtener todos los terceros que tengan un usuario asociado con rol Vendedor
     *
     * @param Request $request Contiene el filtro a aplicar en la consulta
     * @return mixed Contiene una colección con los tercero obtenidos
     * @author Jhon García
     */
    public function getSellers($request)
    {
        return Contact::select('id', 'identification', "name", "surname")
            ->whereHas('users', function ($query) {
                $query->whereHas('getRoles', function ($getRoles) {
                    $getRoles->where('role', 'Vendedor');
                })->withTrashed();
            })
            ->whereRaw('concat(identification, \' \', name, \' \', surname) ILIKE ?', ['%' . $request->filter . '%'])
            ->get();
    }

    public function alertEmployeesWithoutActivity($configAurora)
    {
        $fromDate = Carbon::now()->format('Y-m-d').' 00:00:00';
        $toDate = Carbon::now()->format('Y-m-d').' '.$configAurora->hour;
        $code_voucher_type = '010';

        // Pedidos
        $documentsUsersIds = Document::whereHas('vouchertype', function ($query) use ($code_voucher_type) {
            $query->where('code_voucher_type', $code_voucher_type);
        })
        ->where('in_progress', false)
        ->whereBetween('document_date', [$fromDate, $toDate])
        ->groupBy('user_id')
        ->pluck('user_id');


        // Recibos de casa
        $cashReceiptsAppUsersIds = CashReceiptsApp::whereBetween('creation_date', [$fromDate, $toDate])
        ->groupBy('contact_id')
        ->pluck('contact_id');

        // Usuarios sin actividad
        $users = User::with([
            'contact'
        ])
        ->whereNotIn('id', $documentsUsersIds)
        ->whereNotIn('contact_id', $cashReceiptsAppUsersIds)
        ->whereHas('contact', function ($query) {
            $query->where('is_employee', true);
        })
        ->whereHas('getRoles', function ($query) {
            $query->where('role', 'Vendedor');
        })
        ->get()
        ->toArray();

        if (count($users) === 0) {
            return true;
        }

        // Ultima actividad
        foreach ($users as $key => $user) {
            $res = $this->getLastActivityUser($user);
            $users[$key]['lastActivity'] = $res;
        }

        $responsePdf = $this->documentPDFController->alertEmployeesWithoutActivityPdf([
            'users' => $users,
            'dates' => [
                'fromDate' => $fromDate,
                'toDate' => $toDate,
            ]
        ]);

        try {

            // Enviar emails
            $emails = json_decode($configAurora->email);
            $message = 'Los siguientes asesores no han presentado actividad el dia de hoy hasta las '.$configAurora->hour. ' a.m.';
            $mail = new AuroraMail($message, 'Aurora Asesores sin actividad.', $responsePdf);
            Mail::to($emails)->send($mail);
            return true;

        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return false;
        }
    }


    public function getLastActivityUser($user)
    {
        $code_voucher_type = '010';

        // Pedidos
        $document = Document::whereHas('vouchertype', function ($query) use ($code_voucher_type) {
            $query->where('code_voucher_type', $code_voucher_type);
        })
        ->where('in_progress', false)
        ->where('user_id', $user['id'])
        ->latest('document_date')
        ->first();

        // Rec Caja
        $cashReceiptsApp = CashReceiptsApp::where('contact_id', $user['contact_id'])
        ->latest('creation_date')
        ->first();

        if (($document && $cashReceiptsApp) && $document->document_date > $cashReceiptsApp->creation_date) {
            return [
                'date' => $document->document_date,
                'type' => 'Pedido',
                'contact' => $document->contact
            ];
        }

        return [
            'date' => $cashReceiptsApp ? $cashReceiptsApp->creation_date : null,
            'type' => 'Rec Caja',
            'contact' => $cashReceiptsApp ? $cashReceiptsApp->warehouse->contactt : null
        ];
    }
}
