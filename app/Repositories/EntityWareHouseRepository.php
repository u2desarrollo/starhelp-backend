<?php

namespace App\Repositories;

use App\Entities\EntityWareHouse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EntityWareHouseRepository
 * @package App\Repositories
 * @version January 8, 2019, 7:43 pm UTC
 *
 * @method EntityWareHouse findWithoutFail($id, $columns = ['*'])
 * @method EntityWareHouse find($id, $columns = ['*'])
 * @method EntityWareHouse first($columns = ['*'])
*/
class EntityWareHouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'code_warehouse',
        'description_warehouse',
        'address',
        'latitude',
        'length',
        'telephone',
        'email',
        'contact',
        'observation',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EntityWareHouse::class;
    }

}
