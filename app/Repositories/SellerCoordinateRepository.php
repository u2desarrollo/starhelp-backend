<?php

namespace App\Repositories;

use App\Entities\SellerCoordinate;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Log;

/**
 * Class SellerCoordinateRepository
 * @package App\Repositories
 * @version June 10, 2019, 2:58 pm UTC
 *
 * @method SellerCoordinate findWithoutFail($id, $columns = ['*'])
 * @method SellerCoordinate find($id, $columns = ['*'])
 * @method SellerCoordinate first($columns = ['*'])
*/
class SellerCoordinateRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'seller_id',
		'latitude',
		'longitude',
		'datetime'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return SellerCoordinate::class;
	}

	public function create(array $coordinates) {
		$create = SellerCoordinate::create([
			'seller_id' => $coordinates['iduser'],
			'latitude' => $coordinates['latitud'],
			'longitude' => $coordinates['longitud'],
			'datetime' => date('Y-m-d H:i:s')
		]);

		$created = ['segven_cod_usersd' => $create->id, 'segven_fec_evento' => $create->datetime];

		return $created;
    }

    public function getCoordinateSeller($request){
        $full_data=[];
		$i=0;
		$coordinatesSeller=[];
		$from="2019-07-05 15:47:52";
        $to="2019-07-05 16:01:45";
		$sellerCoordinates=SellerCoordinate::select("id","seller_id","latitude","longitude","datetime")->where('seller_id',$request["id"])->whereBetween('datetime',[$request['date']." 00:00",$request['date']." 23:59"])->get();;
        // $sellerCoordinates=SellerCoordinate::whereBetween('datetime',[$from,$to])->get();
		// return $sellerCoordinates;
		foreach($sellerCoordinates as $sellerCoordinate){
			$lat=(float)$sellerCoordinate->latitude;
			$lng=(float)$sellerCoordinate->longitude;
				$coordinates=[
					'lat'=>$lat,
					'lng'=>$lng
                ];
				$full_data[$i]=new \ArrayObject($coordinates);
				$i+=1;
		}
		return $full_data;
	}
	
	public function getLineCoordinate($request){
        $full_data=[];
		$i=0;
		$coordinatesSeller=[];
		$from="2019-07-05 15:47:52";
        $to="2019-07-05 16:01:45";
		$sellerCoordinates=SellerCoordinate::select("id","seller_id","latitude","longitude","datetime")->where('seller_id',$request["id"])->whereBetween('datetime',[$request['date']." 00:00",$request['date']." 23:59"])->get();;
        // $sellerCoordinates=SellerCoordinate::whereBetween('datetime',[$from,$to])->get();
		// return $sellerCoordinates;
		foreach($sellerCoordinates as $sellerCoordinate){
			$lat=(float)$sellerCoordinate->latitude;
			$lng=(float)$sellerCoordinate->longitude;
				$coordinates=[
					'lat'=>$lat,
					'lng'=>$lng
                ];
                $arreglo=[
                    'position'=>$coordinates
                ];
				$full_data[$i]=new \ArrayObject($arreglo);
				$i+=1;
		}
		return $full_data;
	}
	
	public function getCoordinates($visits, $id, $date)
	{
		$coordinates = [];
		$total_coordinates = [];
		$total_coordinates_per_visit = [];
		$limit_coordinates_per_visit = 5;

		foreach ($visits as $key => $value) {
			if ($key > 0) {
				$finish_date = date_format($value->checkin_date, 'Y-m-d H:i:s');
				$init_date = date_format($visits[$key-1]->checkin_date, 'Y-m-d H:i:s');


				$seller__coordinates = SellerCoordinate::where('seller_id', $id)->whereBetWeen('datetime', [$init_date, $finish_date])->orderBy('datetime')->get();

				$total_coordinates_per_visit[] = count($seller__coordinates);
				$total_coordinates[] = $seller__coordinates;

				if ($total_coordinates_per_visit > $limit_coordinates_per_visit ) {
					//preoceso para limitar cantidad de cordenadas entre visitas
				}

			}

		}

		foreach ($total_coordinates as $key => $value) {
			foreach ($value as $key => $v) {
				$coordinates[]=$v;
			}
		}

		return $coordinates;

		
	}


	public function currentLocation($id)
	{
		$seller__coordinates = SellerCoordinate::where('seller_id', $id)->orderBy('datetime', 'desc')->first();

		$total_coordinates[] = $seller__coordinates;

		return $total_coordinates;

		
	}
}
