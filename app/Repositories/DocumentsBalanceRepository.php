<?php

namespace App\Repositories;

use App\Entities\DocumentsBalance;
use App\Entities\Receivable;
use App\Entities\Document;
use Illuminate\Http\Request;
use InfyOm\Generator\Common\BaseRepository;
use DB;
/**
 * Class ReceivableRepository
 * @package App\Repositories
 * @version May 30, 2019, 9:39 pm UTC
 *
 * @method Receivable findWithoutFail($id, $columns = ['*'])
 * @method Receivable find($id, $columns = ['*'])
 * @method Receivable first($columns = ['*'])
 */

class DocumentsBalanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'document_id',
        'year_month',
        'original_invoice_value',
        'invoice_balance',
        'accounting_account'
    ];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return DocumentsBalance::class;
    }

    public function tracking(Request $request){

        // Variables
        $warehouse = $request->warehouse;
        $sortBy    = $request->sortBy;
        $sort      = $request->sort;

        $receivable = DocumentsBalance::select('id','document_id','year_month','invoice_balance')
            ->whereHas('document', function ($query) use ($warehouse) {
                $query->select('id', 'document_date', 'consecutive');
                $query->where('warehouse_id', $warehouse);
            })
            ->with([
                'document',
                /*'payReceivables' => function ($query) {
                    $query->select('voucher_number', 'vouchertype_id', 'receivable_id', 'pay_date')
                        ->selectRaw('SUM(debit_value) AS debit_value')
                        ->selectRaw('SUM(applied_value) AS applied_value')
                        ->with(['voucherstype'])
                        ->groupBy('voucher_number', 'vouchertype_id', 'receivable_id', 'pay_date')
                        ->orderBy('pay_date', 'ASC');
                }*/
            ])
            ->orderBy($sortBy, $sort)
            ->get();
            
        return $receivable;
    }

    /**
     * @santiago
     */

    public function dispatch()
    {
        try {
            DB::beginTransaction();
            $dispatch = DocumentsBalance::where('invoice_balance', 0)
            ->with(['document' => function ($query){
                    $query->with(['documentsProducts' => function ($query) {
                            $query->with('product');
                        }
                    ]);
                }
            ])
            ->get();
            return $dispatch;
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
}
