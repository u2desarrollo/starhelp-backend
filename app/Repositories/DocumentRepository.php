<?php

namespace App\Repositories;

use App\Entities\BranchOffice;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\CashReceiptsApp;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Entities\DiscountSoonPayment;
use App\Entities\Document;
use App\Entities\DocumentInformation;
use App\Entities\DocumentProduct;
use App\Entities\Inventory;
use App\Entities\Parameter;
use App\Entities\Price;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\User;
use App\Entities\VouchersType;
use App\Traits\CollectionsTrait;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Container\Container;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;
use stdClass;

/**
 * Class DocumentRepository
 * @package App\Repositories
 * @version April 25, 2019, 3:23 pm UTC
 *
 * @method Document findWithoutFail($id, $columns = ['*'])
 * @method Document find($id, $columns = ['*'])
 * @method Document first($columns = ['*'])
 */
class DocumentRepository extends BaseRepository
{
    use CollectionsTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'branchoffice_id',
        'vouchertype_id',
        'consecutive',
        'prefix',
        'warehouse_id',
        'document_date',
        'contact_id',
        'inventory_operation',
        'model_code',
        'thread',
        'base_document',
        'erp_id',
        'cufe',
        'cufe_state',
        'document_state',
        'document_last_state',
        'erp_state',
        'erp_last_state',
        'city',
        'zone',
        'observation',
        'crossing_prefix',
        'crossing_number_document',
        'issue_date',
        'term_days',
        'due_date',
        'insurance_value',
        'freight_value',
        'other_value',
        'discount_value',
        'in_progress',
        'pay_condition',
        'send_point',
        'buy_order',
        'seller_id',
        'user_id',
        'viewed',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Document::class;
    }

    public function applyCashReceiptsToInvoiceBalance($document)
    {
        $sumTotalPaid = 0;
        $document->saldo = 0;
        $document->ValorOriginal = round(intval($document->ValorOriginal), 0, PHP_ROUND_HALF_DOWN);

        //  +++  RESTA DE CONTRIBUCIONES AL SALDO DE LA APP  +++
        //se llama el metodo que se encarga de traer los recibos de caja que no han sido
        //  consignados y ni aplicados, se debe de restar los valores del pago a la correspondiente factura para que
        // el vendedor vea reflejado sus registros de caja

        // se comenta temporalmente
        //$cashreceipts = CashReceiptsRepository::getPaymentWithoutApplying();

        //se pasa un array vacio
        //para que no encuentre rc y no reste nada a las facuras
        $cashreceipts = [];

        foreach ($cashreceipts as $cashReceipt) {
            foreach ($cashReceipt->contribution_invoices_app as $contribution) {
                if ($document->id == $contribution->document_id) {
                    //suma de retenciones
                    $sumwithholdings = $contribution->rete_iva + $contribution->rete_ica + $contribution->rete_fte;
                    $sumTotalPaid += $contribution->total_paid + $sumwithholdings;
                } else {
                    $document->saldo = round($document->receivable[0]->Saldo, 0, PHP_ROUND_HALF_DOWN);
                }
            }
        }
        $document->saldo = round($document->receivable[0]->Saldo - $sumTotalPaid, 0, PHP_ROUND_HALF_DOWN);

        //    +++ FIN  +++
        // }
        return $document;
    }

    public function applyDctPPToInvoices($invoices, $element)
    {
        // +++  PROCESO DESCUENTO PRONTO PAGO  +++

        //descuento segun las promociones
        $valueDPP = 0;
        //cantidad de dias transcurridos despues de expedida la factura
        $days = 0;
        $DelinquentCustomer = false;

        // verifico que el cliente no tenga facturas vencidas o sea moroso
        foreach ($invoices as $key => $invoice) {

            $due_date = strtotime(date($invoice->due_date ? $invoice->due_date : $invoice->due_date));
            $today = strtotime(date("Y-m-d"));

            if ($today > $due_date) {
                //    \Log::info($invoice->id);
                $DelinquentCustomer = true;
            }
        }

        if (!$DelinquentCustomer) {
            // determino que la factura no este vencida
            $due_date = strtotime(date($element->due_date ? $element->due_date : $element->due_date));
            $today = strtotime(date("Y-m-d"));
            if ($today <= $due_date) {
                //  \Log::info('entro a validar');
                $issueD = strtotime(date($element->issue_date));
                $date = strtotime(date("Y-m-d"));

                // DETERMINO LA CANTIDAD DE DIAS QUE TIENER DESPUES DE CREADA LA FACTURA
                //  APARTIR DE ESTE DATO DETERMINO QUE PORCENTAJE DE DESCUENTO APLICAR
                $currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
                $issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($element->issue_date))));

                $days = $currentDate->diffInDays($issueDate);
                foreach ($element->documentsProducts as $key => $v) {

                    $discountsSoonPayment = DiscountSoonPayment::orderBy('days_up', 'asc')->get();
                    foreach ($discountsSoonPayment as $key => $val) {
                        // cumple todas las condiciones
                        if (
                            $v->line_id == $val->line_id && $days <= $val->days_up &&
                            $element->branchoffice_id == $val->branchoffice_id &&
                            $v->subline_id == $val->subline_id && $val->subline_id != null &&
                            $v->brand_id == $val->brand_id && $val->brand_id != null
                        ) {
                            //            \Log::info('entro   1');
                            $v->valuedpp = round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            $valueDPP += round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            break;
                        } else

                        //cumple todas excepto marca
                        if (
                            $v->line_id == $val->line_id &&
                            $days <= $val->days_up &&
                            $element->branchoffice_id == $val->branchoffice_id &&
                            $v->subline_id == $val->subline_id && $val->subline_id != null

                        ) {
                            //              \Log::info('entro   2');
                            $v->valuedpp = round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            $valueDPP += round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            break;
                        } else
                        //cumple todas excepto marca y sub-linea
                        if (
                            $v->line_id == $val->line_id &&
                            $days <= $val->days_up &&
                            $element->branchoffice_id == $val->branchoffice_id
                        ) {
                            //                \Log::info('entro   3');
                            $v->valuedpp = round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            $valueDPP += round(($v->total_value_brut * $val->percentage) / 100, 0, PHP_ROUND_HALF_DOWN);
                            break;
                        }
                    }
                }
                //se resta en saldo , solo para app, en cartera solo es informacion
                $element->saldo -= round($valueDPP, 0, PHP_ROUND_HALF_DOWN);
                //}
            }
        }
        return [$valueDPP, $days];
        //  +++   FIN  +++
    }

    /**
     * @param array $input
     * @return LengthAwarePaginator|Collection|mixed
     */
    public function create(array $input)
    {
        try {

            DB::beginTransaction();

            // Calcular el número del consecutivo
            if (!isset($input['vouchertype_id']) || $input['vouchertype_id'] == null) {
                $vouchertype_id = 1;
                // hacer consulta para saber si quien esta creando el pedido(iduser), es el mismo vendedor(sellerid), si es asi dejar el viewed en true
                // hacer coincidir seller_id y user_id para poderlos comparar
                $user = DB::table('users')->select('contact_id')->where('id', $input['user_id'])->first();
                if ($user->contact_id == $input['seller_id']) {
                    $input['viewed'] = true;
                }
            } else {
                $vouchertype_id = $input['vouchertype_id'];
            }

            if (empty($input['consecutive'])) {
                $consecutive = Document::where('vouchertype_id', $vouchertype_id)->max('consecutive');
                if ($consecutive) {
                    $input['consecutive'] = 1;
                } else {
                    $input['consecutive'] = $consecutive + 1;
                }
            }

            // validamos si hay sede
            if (empty($input['branchoffice_id'])) {
                // En caso de que no haya se busca segun el cliente
                $contactWarehouse = ContactWarehouse::find($input['warehouse_id']);

                // Capturamos el ID de la bodega.
                $branchOfficeWarehouse = BranchOfficeWarehouse::where('branchoffice_id', $contactWarehouse->branchoffice->id)
                    ->where('warehouse_code', '01')
                    ->first();

                // Asignamos el ID de la sucursal y bodega.
                $input['branchoffice_id'] = $contactWarehouse->branchoffice->id;
                $input['branchoffice_warehouse_id'] = $branchOfficeWarehouse->id;
            }

            $input['issue_date'] = date('Y-m-d');

            if (!isset($input['document_date'])) {
                $input['document_date'] = date('Y-m-d H:i:s');
            }

            if (!isset($input['from_service_request'])) {
                $input['from_service_request'] = false;
            }

            // Consultar el tipo de comprobante
            $vouchertype = VouchersType::find($input['vouchertype_id']);

            // Traemos el prefijo
            $input['prefix'] = $vouchertype->prefix;
            $input['origin_document'] = $vouchertype->origin_document;

            // Crear documento en la base de datos
            $cart = Document::create(Arr::only($input, [
                'contact_id',
                'warehouse_id',
                'consecutive',
                'branchoffice_id',
                'branchoffice_warehouse_id',
                'vouchertype_id',
                'model_id',
                'seller_id',
                'user_id',
                'address',
                'viewed',
                'in_progress',
                'issue_date',
                'prefix',
                'projects_id',
                'observation',
                'document_date',
                'trm_value',
                'from_service_request',
                'model_id'
            ]));

            if (!empty($input['documents_information'])) {
                $documentInformation = new DocumentInformation();
                $documentInformation->price_list = $input['documents_information']['price_list'];
                $cart->documentsInformation()->save($documentInformation);
            }

            // Guardar thread
            Document::where('id', $cart->id)->update([
                'thread' => $cart->id,
                'affects_prefix_document' => $cart->prefix,
                'affects_number_document' => $cart->consecutive,
                'affects_date_document' => $cart->document_date,
                'affects_document_id' => $cart->id,
            ]);

            DB::commit();
            // Retornar los datos del documento recién creado
            return $this->getDocument($cart->id);
        } catch (\Exception $e) {
            dd($e->getMessage() . " " . $e->getLine());
            Log::error($e->getMessage() . " " . $e->getLine());
            DB::rollBack();
        }
    }

    public function getDocument($id, $request = [])
    {
        $document_filter = Document::find($id);
        $document = Document::where('id', $id)
            ->with(
                [
                    'projects',
                    'paymentMethods.paymentMethod:id,code_parameter,name_parameter',
                    'refUpdateDocument:id,vouchertype_id,thread,consecutive',
                    'refUpdateDocument.document:id,vouchertype_id,thread,consecutive',
                    'refUpdateDocument.document.vouchertype:id,name_voucher_type,prefix',
                    'files',
                    'operationType',
                    'template',
                    'userAuthorizesWallet',
                    'branchofficeWarehouse',
                    'userSolicitAuthorization',
                    'userLastModification',
                    'branchoffice.city',
                    'user:id,name,surname,email,photo,signature',
                    'warehouse:id,code,description,email,telephone,address,contact_id,branchoffice_id,city_id,payment_type_id,types_negotiation_portfolio',
                    'warehouse.branchoffice.city',
                    'warehouse.city',
                    'warehouse.city.department',
                    'warehouse.paymentType',
                    'warehouse.typesNegotiationPortfolio_',
                    'documentsInformation:id,document_id,price_list,vouchers_type_electronic_bill,vouchers_type_resolution,commission_percentage_applies,commission_percentage,protractor,guide_number',
                    'vouchertype.template.operationType',
                    'document.vouchertype',
                    'document.template',
                    'document.documentsInformation:id,document_id,price_list,vouchers_type_electronic_bill,vouchers_type_resolution,commission_percentage_applies,commission_percentage',
                    'document.documentsProducts:id,document_id,product_id,line_id,subline_id,brand_id,subbrand_id,quantity,original_quantity,unit_value_before_taxes,discount_value,total_value_brut,product_repeat_number',
                    'document.documentsProducts.product:id,code,description,quantity_available',
                    'document.documentsProducts.seller:id,identification,check_digit,name,surname,address,main_telephone,cell_phone',
                    'document.documentsProducts.documentProductTaxes.tax',
                    'contact:id,email,identification_type,subscriber_id,identification,check_digit,name,surname,address,main_telephone,cell_phone,city_id,pay_days,fiscal_responsibility_id,country_code,tax_regime,direct_invoice_blocking,address_object',
                    'contact.identificationt',
                    'contact.city.department',
                    'contact.customer:id,contact_id,category_id,logo,retention_source,retention_iva,retention_ica,observation,credit_quota,expiration_days,percentage_discount',
                    'contact.provider:id,contact_id,category_id,logo,retention_source,percentage_retention_source,retention_iva,percentage_retention_iva,retention_ica,percentage_retention_ica,observation',
                    'contact.employee:id,contact_id,category_id,cost_center_id,photo',
                    'contact.subscriber',
                    'contact.warehouses',
                    'seller:id,identification_type,subscriber_id,identification,check_digit,name,surname,address,main_telephone,cell_phone,city_id',
                    'seller.city',
                    'seller.customer:id,contact_id,category_id,logo,retention_source,retention_iva,retention_ica,observation,credit_quota',
                    'seller.provider:id,contact_id,category_id,logo,retention_source,percentage_retention_source,retention_iva,percentage_retention_iva,retention_ica,percentage_retention_ica,observation',
                    'seller.employee:id,contact_id,category_id,cost_center_id,photo',
                    'seller.subscriber',
                    'seller.warehouses',
                    'documentsProducts' => function ($query) use ($request, $document_filter) {
                        $query->with(
                            [
                                'product' => function ($subquery_two) use ($request, $document_filter) {
                                    $subquery_two->select('id', 'code','previous_code_system', 'description', 'quantity_available', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'modify_description_sale', 'manage_inventory', 'weight')
                                        ->with(
                                            [
                                                'line.taxes',
                                                'subline',
                                                'brand',
                                                'subbrand',
                                                'price',
                                                'inventory' => function ($query) use ($request, $document_filter) {
                                                    $query->where('branchoffice_warehouse_id', $document_filter->branchoffice_warehouse_id);
                                                },
                                            ]
                                        );
                                },
                                'promotion.productsPromotions',
                                'branchofficeWarehouse',
                                'seller:id,identification,check_digit,name,surname,address,main_telephone,cell_phone',
                                'documentProductTaxes.tax',
                                'account',
                            ]
                        );
                        $query->orderBy('id');
                    },
                    'document_transactions'
                ]
            )
            ->first();

        $document = $this->addMinimalPriceDocumentProducts($document, $request);

        return $document;
    }

    public function addMinimalPriceDocumentProducts($document, $request)
    {

        if (!is_array($request)) {
            return $document;
        }

        if (isset($request['model_id']) && !empty($request['model_id'])) {
            $model = Template::find($request['model_id']);
        }

        if (!empty($model->minimal_price)) {

            $priceListId = $model->minimal_price;

            $priceList = Parameter::find($priceListId);

            foreach ($document->documentsProducts as $key => $documentProduct) {

                if ($priceListId == '24463') {

                    $inventory = Inventory::where('product_id', $documentProduct->product->id)
                        ->where('branchoffice_warehouse_id', $document->branchoffice_warehouse_id)
                        ->first();

                    if ($inventory) {

                        $documentProduct->minimalPrice = [
                            'price' => round($inventory->average_cost),
                            'product_id' => $documentProduct->product->id,
                            'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id,
                            'branchoffice_id' => null,
                        ];
                    } else {
                        $documentProduct->minimalPrice = [
                            'price' => 0,
                            'product_id' => $documentProduct->product->id,
                            'branchoffice_id' => null,
                        ];
                    }
                } else {
                    // Validamos por SEDE
                    $minimalPrice = Price::where('product_id', $documentProduct->product_id)
                        ->where('price_list_id', $priceList->id)
                        ->where('branchoffice_id', $document->branchoffice_id)
                        ->first();

                    if (!$minimalPrice) {
                        $minimalPrice = Price::where('product_id', $documentProduct->product_id)
                            ->where('price_list_id', $priceList->id)
                            ->whereNull('branchoffice_id')
                            ->first();
                    }

                    $documentProduct->minimalPrice = $minimalPrice;
                }

            }
        }

        return $document;
    }

    public function getDocumentsForThreadId($thread_id)
    {

        $documents = Document::where(['thread' => $thread_id, 'in_progress' => false])
            ->with([
                'documentsProducts',
                'vouchertype' => function ($subquery) {
                    $subquery->with([
                        'template' => function ($subquery) {
                            $subquery->with('operationType');
                        },
                    ]);
                },
            ])
            ->orderBy('created_at', 'ASC')
            ->get();

        return $documents;
    }

    // Obtener el carrito
    public function getCart($contact, $warehouse, $user)
    {
        $cart = Document::select('id', 'contact_id', 'consecutive', 'warehouse_id', 'observation', 'buy_order', 'seller_id', 'user_id', 'vouchertype_id', 'branchoffice_id', 'branchoffice_warehouse_id')
            ->where([
                'contact_id' => $contact,
                'warehouse_id' => $warehouse,
                'user_id' => $user,
                'in_progress' => true,
            ])
            ->with([
                'documentsProducts' => function ($query) {
                    $query->select([
                        'id',
                        'document_id',
                        'product_id',
                        'brand_id',
                        'line_id',
                        'quantity',
                        'seller_id',
                        'subline_id',
                        'unit_value_before_taxes',
                        'discount_value',
                        'promotion_id',
                        'is_benefit',
                        'inventory_cost_value',
                    ]);
                    $query->with([
                        'product' => function ($query) {
                            $query->with([
                                'price',
                                'line' => function ($query) {
                                    $query->with([
                                        'taxes',
                                    ]);
                                },
                            ]);
                        },
                        'documentsProductsTaxes',
                        'promotion' => function ($query) {
                            $query->with([
                                'productsPromotions' => function ($query) {
                                    $query->with([
                                        'product' => function ($query) {
                                            $query->with([
                                                'price',
                                                'line' => function ($query) {
                                                    $query->with([
                                                        'taxes',
                                                    ]);
                                                },
                                            ]);
                                        },
                                    ]);
                                },
                                'specificProduct' => function ($query) {
                                    $query->with([
                                        'price',
                                        'line' => function ($query) {
                                            $query->with([
                                                'taxes',
                                            ]);
                                        },
                                    ]);
                                },
                                'line',
                                'subline',
                                'brand',
                                'subbrand',
                            ]);
                        },
                    ]);
                },
                'documentsInformation' => function ($query) {
                    $query->select('id', 'document_id', 'price_list');
                },
            ])
            ->whereHas('vouchertype', function($query){
                $query->where('code_voucher_type', '001');
            })
            ->first();

        if ($cart) {

            // En caso de que no haya se busca segun el cliente
            $contactWarehouse = ContactWarehouse::find($warehouse);

            // Capturamos el ID de la bodega.
            $branchOfficeWarehouse = BranchOfficeWarehouse::where('branchoffice_id', $contactWarehouse->branchoffice->id)
                ->where('warehouse_code', '01')
                ->first();

            // Asignamos el ID de la sucursal y bodega.
            $branchoffice_id = $contactWarehouse->branchoffice->id;
            $branchoffice_warehouse_id = $branchOfficeWarehouse->id;

            // Validamos si es diferente
            if ($cart->branchoffice_id != $branchoffice_id) {

                $cart->update([
                    'branchoffice_id' => $branchoffice_id,
                    'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                ]);
            }

            $cart = $this->addMinimalPriceDocumentProducts($cart, [
                'model_id' => 2,
            ]);

            return $cart;
        }

        return new \App\Entities\Document;
    }

    // Finalizar carrito
    public function finishCart($cart, $request)
    {
    }

    public function printDocument($document)
    {
        $impresion = array();

        $fecha_inicio = new \DateTime($document->document_date);
        $fecha_fin = new \DateTime($document->document_date);

        $impresion['id'] = $document->id;
        $impresion['id_'] = $document->id . '-' . substr($document->due_date, 0, 4) . substr($document->due_date, 5, 2) . substr($document->due_date, 8, 2);
        $impresion['qr'] = !is_null($document->documentsInformation) && $document->documentsInformation->vouchers_type_electronic_bill && !empty($document->qr) ? $document->qr : $document->id;
        $impresion['cufe'] = $document->cufe;
        $impresion['vouchertype_id'] = $document->vouchertype_id;
        $impresion['vouchertype'] = $document->vouchertype->toArray();
        $impresion['operationType_id'] = $document->operationType_id;
        $impresion['document_name'] = !is_null($document->documentsInformation) ? $document->documentsInformation->document_name : null;

        $impresion['project'] = $document->template->select_projects ? ($document->projects ? $document->projects->description : null ) :null;


        $validate_ = ContactCustomer::where('contact_id', $document->contact_id)->first();
        $impresion['cupo_credito'] = 'CONTADO';
        if ($validate_) {
            if ($validate_->credit_quota > 0) {
                $impresion['cupo_credito'] = 'CRÉDITO';
                $document_date = Carbon::parse($document->document_date); //->format('Y-m-d H:i:s')
                $fecha_fin = $document_date->addDays(30);
            }
        }

        // modelo
        $impresion["modelo"] = $document->template ? $document->template->code . ' - ' . $document->template->description : '';
        $impresion["codigo_modelo"] = $document->template ? $document->template->code : null;
        $impresion["tituloDevol"] = $document->voucherType->code_voucher_type == '155' || $document->voucherType->code_voucher_type == '156' || $document->voucherType->code_voucher_type == '270' ? $document->template->handling_other_parameters_description : null;
        $param = $document->voucherType->code_voucher_type == '155' || $document->voucherType->code_voucher_type == '156' || $document->voucherType->code_voucher_type == '270' ? Parameter::find($document->handling_other_parameters_value) : null;
        $impresion["razonDevol"] = $document->voucherType->code_voucher_type == '155' || $document->voucherType->code_voucher_type == '156' || $document->voucherType->code_voucher_type == '270' ? (isset($param->code_parameter) ? $param->code_parameter : null) . ' - ' . (isset($param->name_parameter) ? $param->name_parameter : null) : null;
        $impresion["modelo"] = $document->template ? $document->template->code . ' - ' . $document->template->description : '';

        // $impresion["tituloDevol"] = $document->voucherType->code_voucher_type == '155' ? $document->template->handling_other_parameters_description : null;
        // $param = $document->voucherType->code_voucher_type == '155' ? Parameter:: find($document->handling_other_parameters_value) : null;
        // $impresion["razonDevol"] = $document->voucherType->code_voucher_type == '155' ? $param->code_parameter . ' - ' . $param->name_parameter : null;

        /**
         * assets images
         */
        $impresion["documentoBase"] = $document->document != null ? ( is_null($document->document->prefix) ? $document->document->consecutive :  trim($document->document->prefix) . ' - ' . $document->document->consecutive) : null;
        $impresion["elaboro"] = $document->user ? $document->user->name . ' ' . $document->user->surname : '';
        $impresion['logo'] = storage_path() . '/app/public/colsaisa_logo.png';
        $impresion['gradient'] = storage_path() . '/app/public/gradient_bar.png';

        /**
         * subscriber Data
         */

        $impresion['subs_name'] = $document->contact->subscriber->name;
        $impresion['subs_identification'] = $document->contact->subscriber->identification;
        $impresion['subs_check_digit'] = $document->contact->subscriber->check_digit;

        /**
         * datos modelo template
         */
        $model = Template::find($document->model_id);

        //dd($model->operationType);

        $impresion['handle_payment_methods'] = $model ? $model->handle_payment_methods : '';
        $impresion['name_document'] = $model ? $model->description : '';
        $impresion['total_letters'] = $model ? $model->total_letters : '';
        $impresion['id_tem'] = $model ? $model->id_tem : '';
        $impresion['operation_type_code'] = $model->operationType ? $model->operationType->code : '';
        $impresion['retention_management'] = $model ? $model->retention_management : '';
        $impresion['template_contact_type'] = $model ? $model->contact_type : '';

        if (($model && $model->print_tittle == '') || ($model && $model->print_tittle == null)) {
            $impresion['name_document'] = $model->description;
        } else if ($model && $model->print_tittle != '' && $model->print_tittle != null) {
            $impresion['name_document'] = $model->print_tittle;
        }

        $impresion['contact'] = $model ? $model->contact_type : '';

        $impresion['numero'] = $document->consecutive;
        $impresion['fecha_inicio'] = $fecha_inicio->format('M d, Y');
        $impresion['fecha_inicio_hora'] = $fecha_inicio->format('H:m');
        $impresion['fecha_fin'] = $fecha_fin->format('M d, Y');

        /*
         * Datos de factura
         */
        // $impresion['name_document'] = $document->vouchertype->name_voucher_type;
        $impresion['resolucion'] = $document->vouchertype->resolution;
        $impresion['prefijo'] = $document->prefix;
        $impresion['observacion1'] = $document->observation;
        $impresion['observacion_title'] = isset($document->vouchertype->template->observation_tittle_1) ? $document->vouchertype->template->observation_tittle_1 : '';

        /*
         * Datos de contacto
         */
        $impresion['contacto_celular'] = $document->contact->customer->cell_phone;
        $impresion['contacto_cupo'] = !is_null($document->contact->customer) ? $document->contact->customer->credit_quota : null;
        $impresion['contacto_plazo'] = !is_null($document->contact->customer) ? $document->contact->customer->expiration_days : null;
        $impresion['contacto_descuento'] = !is_null($document->contact->customer) ? $document->contact->customer->percentage_discount : null;

        $impresion['contacto_nombre'] = empty($document->contact->surname) ? $document->contact->name : $document->contact->name . ' ' . $document->contact->surname;
        $impresion['contacto_nit'] = $document->contact->identification;

        $impresion['contacto_direccion'] = isset($document->warehouse->address) ? $document->warehouse->address : $document->contact->address;
        $impresion['contacto_telefono'] = isset($document->warehouse->telephone) ? $document->warehouse->telephone : $document->contact->cell_phone;
        if (isset($document->warehouse->email)) {
            $impresion['contacto_email'] = $document->warehouse->email;
        } else {
            $impresion['contacto_email'] = $document->contact->email;
        }
        $impresion['contacto_ciudad'] = isset($document->warehouse->city) && $document->warehouse->city != null ? $document->warehouse->city->city : null;
        $impresion['contacto_departamento'] = isset($document->warehouse->city) && $document->warehouse->city != null ? (!is_null($document->warehouse->city->department) ? $document->warehouse->city->department->department : null ) : null;
        $impresion['contacto_sucursal'] = $document->warehouse ? $document->warehouse->code . '-' . $document->warehouse->description : null;
        $impresion['titulo_cruce'] = !$document->template || $document->template->tittle == null ? 'Documento Cruce' : $document->template->tittle;
        $impresion['documento_cruce'] = $document->crossing_number_document == null ? null : $document->crossing_number_document;
        $impresion['fec_documento_cruce'] = $document->crossing_number_document == null ? null : $document->crossing_date_document;
        $impresion['medio_pago'] = $document->warehouse ? ($document->warehouse->paymentType != null ? $document->warehouse->paymentType->name_parameter : Parameter::find(24622)->name_parameter) : Parameter::find(24622)->name_parameter;
        // $impresion['cupo_credito'] = $document->warehouse ? ($document->warehouse->typesNegotiationPortfolio_ != null ? $document->warehouse->typesNegotiationPortfolio_->name_parameter : null ) : null;

        $impresion['codigo'] = $document->contact->identification . '-' . $document->contact->check_digit;
        $impresion['nume'] = $document->contact->identification . '-' . $document->contact->check_digit;
        $impresion['fax'] = $document->contact->fax;

        /**
         * Datos almacén
         */
        $impresion['bodega'] = $document->branchofficeWarehouse ? $document->branchofficeWarehouse->warehouse_description : '';
        $impresion['direccion'] = $document->branchoffice->address;
        $impresion['sede'] = $document->branchoffice->name;
        $impresion['bodega'] = $document->branchofficeWarehouse ? $document->branchofficeWarehouse->warehouse_description : '';
        $impresion['pending_authorization'] = $document->pending_authorization;
        $impresion['nombre_usuario'] = $document->user ? $document->user->name . ' ' . $document->user->surname : '';
        $impresion['fechaA'] = $document->document_date;

        $impresion["user_id_solicit_authorization"] = $document->user_id_solicit_authorization != null ? User::find($document->user_id_solicit_authorization) : null;
        $impresion["date_solicit_authorization"] = $document->date_solicit_authorization;

        $impresion['telefono'] = $document->branchoffice->telephone;
        if ($document->warehouse != null) {
            $impresion['nombre'] = $document->warehouse->description;
            $impresion['nit'] = $document->warehouse->code;
            $impresion['email'] = $document->warehouse->email;
            $impresion['city'] = $document->branchoffice->city->city;
        } else {
            $impresion['nombre'] = '';
            $impresion['nit'] = '';
            $impresion['email'] = '';
            $impresion['city'] = '';
        }

        /**
         * Datos Vendedor
         */
        //dd($document->voucherType->toArray());
        $impresion['vendedor'] = !empty($document->seller) ? $document->seller->name . ($document->seller->surname != '' && $document->seller->surname != '.' ? ' ' . $document->seller->surname : '') : '';
        $impresion['moneda'] = 'PESOS';
        $impresion['estado'] = $document->parameter->name_parameter;

        if (!empty($document->trm_value) && $document->trm_value > 0) {
            $impresion['total_value_brut_docment'] = $document->total_value_brut_us;
        } else {
            $impresion['total_value_brut_docment'] = $document->total_value_brut;
        }

        $subtotal_sin_desc = 0;
        $taxes = 0;
        $full_total = 0;
        $total_discounts = 0;

        /**
         * Medio de pago
         */
        $impresion['forma_pago'] = $document->pay_condition;
        $new_payways = array();
        $payways = $document->paymentWay;
        //Log::info(json_encode($payways));

        foreach ($payways as $payway) {
            $new_payway = [];
            foreach ($payway->parameter as $key2 => $value2) {
                $new_payway['name_parameter'] = $value2->name_parameter;
            }
            $new_payway['value'] = $payway['value'];
            $new_payways[] = $new_payway;
        }

        $impresion['medios_pagos'] = $new_payways;

        $new_products = array();
        $products = $document->documentsProducts;

        if ($impresion["operationType_id"] == 1 || $impresion["operationType_id"] == 2) {
            $products = DocumentProduct::where('document_id', $document->id)
                ->select('id', 'description', 'operationType_id', 'document_id', 'product_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'quantity', 'original_quantity', 'unit_value_before_taxes', 'discount_value', 'physical_unit', 'is_benefit', 'promotion_id', 'total_value_brut', 'total_value', 'total_value_us', 'total_value_brut_us', 'discount_value_us', 'branchoffice_warehouse_id')
                ->get();
        }

        //Log::info(json_encode($products));
        $impresion['iva_doc'] = 0;
        $impresion['iva_obs'] = 0;

        $impresion['iva_obsq'] = 0;
        $impresion['iva_doc'] = 0;
        $impresion['weight'] = 0;
        $impresion["branchoffice_change"] = $document->template ? $document->template->branchoffice_change : '';
        $impresion['trm'] = false;
        $impresion['valor_trm'] = $document->trm_value;

        if (!is_null($document->status_id_authorized)) {
            $impresion['status_id_authorized'] = true;
            $impresion['PortfolioAuthorization'] = $document->userAuthorizesWallet->name;
            $impresion['date_authorized'] = $document->date_authorized;
        } else {
            $impresion['status_id_authorized'] = false;
        }

        /**
         * metodos de pago oyt
         */
        $impresion['metodos_pago'] = [];
        $impresion['cambio'] = null;
        foreach ($document->paymentMethods as $key => $paymentMethod) {
            array_push($impresion['metodos_pago'], [
                'metodo_pago' => $paymentMethod->paymentMethod->code_parameter . ' - ' . $paymentMethod->paymentMethod->name_parameter,
                'valor' => $paymentMethod->paymentMethod->code_parameter == "01" ? $paymentMethod->total_value : $paymentMethod->value,
            ]);
            $impresion['cambio'] = $paymentMethod->paymentMethod->code_parameter == "01" ? $paymentMethod->value_change : $impresion['cambio'];
        }

        foreach ($products as $product) {
            $impresion['weight'] += floatval(str_replace(',', '.', $product["product"]["weight"])) * $product["quantity"];

            //validamos si es obsequio
            if ($product['is_benefit']) {
                $impresion['iva_obs'] += $product['total_value'];
            }

            $set_product = true;
            if ($product['quantity'] == 0) {
                if ($impresion["operationType_id"] != 1 && $impresion["operationType_id"] != 2) {
                    $set_product = false;
                }
                if ($document->template->quantity_zero) {
                    if ($product['total_value_brut'] > 0 || $product['inventory_cost_value'] > 0) {
                        $set_product = true;
                    }
                }
            }

            if ($document->template->credit_note) {
                $set_product = true;
            }

            if ($set_product) {
                $new_product = [];
                $new_product['code'] = $product['product']['code'];
                $new_product['previous_code_system'] = $product['product']['previous_code_system'];
                $new_product['line'] = !is_null($product['product']['line']) ? $product['product']['line']['line_description'] : null;
                $new_product['code'] = $product['product']['code'];
                $new_product['description'] = !empty($product['description']) ? $product['description'] : $product['product']['description'];
                $new_product['quantity'] = $product['quantity'];
                if (!is_null($impresion["branchoffice_change"])) {
                    $new_product['bodegadp'] = BranchOffice::find($product['branchofficeWarehouse']['branchoffice_id'])->name . ' - ' . $product['branchofficeWarehouse']['warehouse_description'];
                }

                //$new_product['quantity_committed'] = $product['bonus_quantity'];

                //revisar esta solucion temporal
                // $new_product['unit_value_before_taxes'] = $product['unit_value_before_taxes'];
                //$new_product['unit_value_before_taxes'] = $product['total_value_brut'] / $product['quantity'];

                if (!empty($document->trm_value) && $document->trm_value > 0) {
                    $new_product['unit_value_before_taxes'] = $product['quantity'] <= 0 ? $product['total_value_brut_us'] : $product['total_value_brut_us'] / $product['quantity'];
                    $new_product['discount_value'] = $product['discount_value_us'];
                    $new_product['total_value_brut'] = $product['total_value_brut_us'];
                    $new_product['total_value'] = $product['total_value_us'];
                    $impresion['trm'] = true;
                } else {
                    $impresion['trm'] = false;
                    $new_product['unit_value_before_taxes'] = $product["unit_value_before_taxes"]/*$product['total_value_brut'] / $product['quantity']*/;
                    $new_product['discount_value'] = $product['discount_value'];
                    $new_product['total_value_brut'] = $product['total_value_brut'];
                    $new_product['total_value'] = $product['total_value'];
                }

                $new_product['total_discount'] = floatval($product['discount_value']) * $product['quantity'];
                $new_product['iva'] = $product['total_value_brut'];
                $quantity = $product['product']['quantity_available'];

                if ($quantity < $product['quantity']) {
                    if ($quantity < 0) {
                        $new_product['back_order'] = $product['quantity'];
                    } else if ($quantity >= 1) {
                        $new_product['back_order'] = $quantity - $product['quantity'];
                    }
                } else {
                    $new_product['back_order'] = 0;
                }

                $new_product['iva_p'] = 0;

                foreach ($product->documentProductTaxes as $key => $value) {
                    if ($value->tax_id == 1) {
                        $new_product['iva_p'] = $value->value;
                        if ($product->unit_value_before_taxes == 0 || $product->is_benefit) {
                            $impresion['iva_obsq'] += $new_product['iva_p'];
                        } else {
                            $impresion['iva_doc'] += $new_product['iva_p'];
                        }
                    }
                }
                // $impresion['iva_doc'] = $iva;

                $new_products[] = $new_product;

                $subtotal_sin_desc = $subtotal_sin_desc + $new_product['total_value_brut'];
                $total_discounts = $total_discounts + $new_product['total_discount'];
                $taxes = $taxes + $new_product['iva'];
            }
        }

        if ($document->vouchertype->code_voucher_type == 205) {
            $new_productss = $new_products;
            $new_products = [];
            $flete = [];
            foreach ($new_productss as $key => $product) {
                if ($product['code'] != 'FLETE') {
                    $new_products[] = $product;
                } else {
                    $flete[] = $product;
                }
            }
            if (count($flete) > 0) {
                foreach ($flete as $key => $felt) {
                    $new_products[] = $felt;
                }
            }
        }

        /**
         * retenciones
         */
        $new_retentions = array();
        foreach ($products as $product) {
            foreach ($product->documentProductTaxes as $key => $value) {
                if ($value->tax_id == 2 || $value->tax_id == 3 || $value->tax_id == 5) {
                    $new_retention = [];
                    $new_retention['id'] = $value->tax_id;
                    $new_retention['descripcion'] = $value->tax->description;
                    $new_retention['percentage'] = $value->tax_percentage;
                    $new_retention['base_value'] = $value->base_value;
                    $new_retention['total'] = (floatval($new_retention['percentage']) * $new_retention['base_value']) / 100;
                    $new_retentions[] = $new_retention;
                }
            }
        }

        $impresion['retenciones'] = $new_retentions;

        $rete_iva_total = 0;
        $rete_fuente_total = 0;
        $rete_ica_total = 0;

        $rete_ica_base_value = 0;
        $rete_fuente_base_value = 0;
        $rete_iva_base_value = 0;

        foreach ($impresion['retenciones'] as $retencion) {
            switch ($retencion["id"]) {
                case 2:
                    $rete_iva_total = $rete_iva_total + $retencion["total"];
                    $rete_iva_base_value = $rete_iva_base_value + $retencion["base_value"];
                    break;

                case 3:
                    $rete_fuente_total = $rete_fuente_total + $retencion["total"];
                    $rete_fuente_base_value = $rete_fuente_base_value + $retencion["base_value"];
                    break;

                case 5:
                    $rete_ica_total = $rete_ica_total + $retencion["total"];
                    $rete_ica_base_value = $rete_ica_base_value + $retencion["base_value"];
                    break;
            }
        }

        $impresion["retenciones_total"]["rete_iva"]["base_value"] = $rete_iva_base_value;
        $impresion["retenciones_total"]["rete_fuente"]["base_value"] = $rete_fuente_base_value;
        $impresion["retenciones_total"]["rete_ica"]["base_value"] = $rete_ica_base_value;

        $impresion["retenciones_total"]["rete_iva"]["total"] = $rete_iva_total;
        $impresion["retenciones_total"]["rete_fuente"]["total"] = $rete_fuente_total;
        $impresion["retenciones_total"]["rete_ica"]["total"] = $rete_ica_total;

        $impresion["retenciones_total"]["rete_iva"]["porcentage"] = '15%';
        $impresion["retenciones_total"]["rete_fuente"]["porcentage"] = '3.5%';
        $impresion["retenciones_total"]["rete_ica"]["porcentage"] = '0.01104%';

        $impresion["vehicle_plate"] = $document->contactVehicle != null ? $document->contactVehicle->license_plate : '';
        $impresion["vehicle"] = $document->contactVehicle != null ? $document->contactVehicle->vehicleType->brandVehicle->name_parameter . ' - ' . $document->contactVehicle->vehicleType->vehicle_type : '';
        $impresion["kilometraje"] = is_null($document->documentVehicle) ? null : $document->documentVehicle->mileage;
        $impresion['pie_pagina'] = $document->template->footnotes;

        $impresion['products'] = $new_products;
        $impresion['subtotal_sin_desc'] = $subtotal_sin_desc;
        $impresion['total_discounts'] = $total_discounts;
        $impresion['subtotal'] = $subtotal_sin_desc - $total_discounts;
        $impresion['taxes'] = $taxes;
        $impresion['full_total'] = $document->total_value;
        $impresion['full_total'] = $impresion['total_value_brut_docment'] + $impresion['iva_doc'] + $impresion['iva_obsq'];

        //cancelado
        $impresion['canceled'] = false;
        if ($document->canceled) {
            $impresion['canceled'] = $document->canceled;
            $user_canceled = User::find($document->canceled_user_id);
            $impresion['user_canceled'] = $user_canceled ? $user_canceled->name : '';
            $impresion['canceled_date'] = $document->canceled_date;
        }

        //retenciones
        $impresion['ret_fue_total'] = $document->contact->tax_regime == 949 ? ($document->operationType_id == 5 || $document->operationType_id == 1 ? 0 : $document->ret_fue_total) : $document->ret_fue_total;
        $impresion['ret_iva_total'] = $document->ret_iva_total;
        $impresion['ret_ica_total'] = $document->ret_ica_total;

        $impresion["total_retention"] = $impresion["ret_fue_total"] + $impresion["ret_iva_total"] + $impresion["ret_ica_total"];

        $impresion['total_to_pay'] = $impresion['full_total'] - $impresion["total_retention"];

        return $impresion;
    }

    public function tracking($request)
    {

        $contact = DB::table('contacts_warehouses')->find($request->warehouse);

        $documents = Document::select('id', 'document_date', 'consecutive', 'document_state', 'warehouse_id', 'vouchertype_id', 'state_backorder', 'closed', 'status_id_authorized', 'contact_id')
            ->with([
                'status_authorized',
                'stateBackorder',
                'warehouse:id,description',
                'documentThread.documentsInformation',
                'documentsProducts.documentsProductsTaxes',
                'documentThread.documentsProducts.product',
                'documentsProducts.product:id,code,description',
                'documentThread.documentsProducts.documentsProductsTaxes',
                'documentThread.documentsProducts:id,document_id,product_id,quantity,unit_value_before_taxes,total_value_brut,total_value',
                'documentsProducts:id,document_id,product_id,quantity,unit_value_before_taxes,total_value_brut,total_value',
                'documentThread' => function ($subquery) {
                    $subquery->select('id', 'document_date', 'consecutive', 'thread', 'vouchertype_id', 'total_value', 'total_value_brut')
                        ->whereHas('documentsProducts', function ($subquery) {
                            $subquery->where('quantity', '>', 0);
                        })
                        ->where('operationType_id', '!=', 8)
                        ->where('consecutive', '!=', 0)
                        ->orderBy('document_date', 'DESC');
                },
            ])
            ->whereBetween('document_date', $request->filter_date)
            ->where('contact_id', $contact->contact_id)
            ->where('operationType_id', 8)
            ->when($request->consecutive != '', function ($query) use ($request) {
                $query->where('consecutive', $request->consecutive)
                    ->orWhereHas('documentThread', function ($subquery) use ($request) {
                        $subquery->where('consecutive', $request->consecutive)
                            ->whereHas('documentsProducts', function ($subquery) {
                                $subquery->where('quantity', '>', 0);
                            })
                            ->where('consecutive', '!=', 0)
                            ->where('operationType_id', '!=', 8);
                    });
            })
            ->when(isset($request->filter_status) && $request->filter_status != '', function ($subquery) use ($request) {
                if ($request->filter_status == 'CERRADO') {
                    $subquery->where('closed', true);
                } else {
                    $subquery->whereHas('stateBackorder', function ($stateBackorder) use ($request) {
                        $stateBackorder->where('name_parameter', $request->filter_status);
                    })->where('closed', false);
                }
            })
            ->orderBy('document_date', 'DESC')
            ->get();

        /* $documents->map(function ($d) use ($request) {
        $countDocumentsProducts = $d->documentsProducts->count();
        $d->documentsProducts->map(function ($dp) use ($d) {
        $dp->quantityInvoiced = 0;
        $d->documentThread->map(function ($dt) use ($dp) {
        if ($dp->quantityInvoiced < $dp->quantity) {
        $dt->documentsProducts->map(function ($dpt) use ($dp) {
        if ($dpt->product_id == $dp->product_id) {
        $dp->quantityInvoiced += $dpt->quantity;
        }
        });
        }
        });
        });

        $countDocumentsProductsInvoiced = $d->documentsProducts->filter(function ($dp) use ($d) {
        return $dp->quantityInvoiced >= $dp->quantity;
        })->count();

        if ($countDocumentsProductsInvoiced == $countDocumentsProducts) {
        $d->state = 'FACTURADO';
        } else if ($d->documentThread->count() > 0) {
        $d->state = 'FACTURADO PARCIAL';
        } else {
        $d->state = 'PENDIENTE';
        }

        });

        if (isset($request->filter_status) && $request->filter_status != '') {
        $documents = $documents->filter(function ($d) use ($request) {
        return $d->state == $request->filter_status;
        })->values();
        }*/

        return $documents;
    }

    public function orders(int $contact_seller)
    {
        /*
         * @contact_seller = tercero del vendedor
         * @user_seller = usuario del vendedor
         */

        $user_seller = DB::table('users')->select('id')->where('contact_id', $contact_seller)->first();
        $orders = DB::table('documents')->where('seller_id', $contact_seller)->whereRaw('COALESCE(user_id, 0) != ?', array($user_seller->id))->where('vouchertype_id', 3)->where('viewed', false)->count();
        $counting = ['nuevos_pedidos' => $orders];
        return $counting;
    }

    public function getDocuments($request)
    {
        if ($request->branch == "" && $request->voucherType == "") {
            $documentsData = Document::select("id", "branchoffice_id", "contact_id", "consecutive", "vouchertype_id", "document_last_state")
                ->with('contact', 'branchoffice', 'voucherstype', 'documentsProducts')
                ->where('id', 0)
                ->take(5000)
                ->get();
        } else if ($request->branch == "" && $request->voucherType != "") {
            $documentsData = Document::select("id", "branchoffice_id", "contact_id", "consecutive", "vouchertype_id", "document_last_state")
                ->with('contact', 'branchoffice', 'voucherstype', 'documentsProducts')
                ->where("vouchertype_id", $request->voucherType)
                ->whereBetWeen('document_last_state', [$request->date[0], $request->date[1]])
                ->take(5000)
                ->get();
        } else if ($request->branch != "" && $request->voucherType == "") {
            $documentsData = Document::select("id", "branchoffice_id", "contact_id", "consecutive", "vouchertype_id", "document_last_state")
                ->with('contact', 'branchoffice', 'voucherstype', 'documentsProducts')
                ->where("branchoffice_id", $request->branch)
                ->whereBetWeen('document_last_state', [$request->date[0], $request->date[1]])
                ->take(5000)
                ->get();
        } else if ($request->branch != "" && $request->voucherType != "") {
            $documentsData = Document::select("id", "branchoffice_id", "contact_id", "consecutive", "vouchertype_id", "document_last_state")
                ->with('contact', 'branchoffice', 'voucherstype', 'documentsProducts')
                ->where([["vouchertype_id", "=", $request->voucherType], ["branchoffice_id", "=", $request->branch]])
                ->whereBetWeen('document_last_state', [$request->date[0], $request->date[1]])
                ->take(5000)
                ->get();
        }

        return $documentsData;
        /* return $request; */
    }

    /**
     * Obtiene los documentos base
     *
     * @param int $voucher_type tipo de comprobante
     * @return array
     */
    public function baseDocuments($request)
    {

        $voucher_type = $request->voucher_type;
        $search = $request->search;
        $date_start = $request->date_start;
        $date_end = $request->date_end;

        $document = Document::where('vouchertype_id', $voucher_type)
            ->with([
                'documentsProducts',
                'seller',
                'warehouse',
                'vouchertype',
                'document',
                'status',
                'products' => function ($query) {
                    $query->select('id', 'document_id');
                    $query->selectRaw('SUM(missing_quantity) AS back_order')->groupBy('document_id', 'id');
                },
            ])
            ->where('in_progress', false);

        if (!empty($date_start) && !empty($date_end)) {
            $document->whereBetween('document_date', [$date_start, $date_end]);
        }

        if (!empty($search)) {
            $document->whereHas('warehouse', function ($query) use ($search) {
                $query->where('description', 'ILIKE', '%' . $search . '%');
            });
        }

        return $document->orderBy('document_date', 'ASC')->get();
    }

    /**
     * Obtiene las facturas
     *
     * @param int idcliente Código del cliente
     * @return array
     */
    public function getInvoices($idcliente, $id = null)
    {
        //  filtro por sede de facturas de acuerdo al vendedor
        //$branchofficeIdAuthenticatedSeller=Auth::user()->branchoffice_id;

        $invoices = Document::where('operationType_id', 5)->when($idcliente != null, function ($query) use ($idcliente) {
            $query->where('contact_id', $idcliente);
            //  filtro por sede de facturas de acuerdo al vendedor
            //where('branchoffice_id', $branchofficeIdAuthenticatedSeller);
        })->when($id != null, function ($query) use ($id) {
            $query->where('id', $id);
        })
            ->select(
                'id',
                'consecutive AS numeroFactura',
                'prefix',
                'invoice_link AS urlFactura',
                'document_date as issue_date',
                'branchoffice_id',
                'due_date',
                'total_value as ValorOriginal',
                // 'software_siigo as retes',
                'contact_id'
            )

        /** solo para pruebas  */
        //->selectRaw("CASE WHEN prefix = '' then 'siigo' else COALESCE(prefix, 'siigo') END AS prefix")
            ->selectRaw("CASE WHEN true then true END AS retes")
        /** solo para pruebas  */
            ->selectRaw('CAST(COALESCE(discount_value, 0) AS int) AS descuentoPP')
            ->with(['receivable' => function ($query) {
                $query->select('document_id', 'invoice_balance as Saldo'); // debes de jalarte como minimo el id foreign
            }])
            ->whereHas('receivable', function ($query) {
                $query->where('invoice_balance', '>', 0); // debes de jalarte como minimo el id foreign
            })->with('documentsProducts')
            ->
        orderBy('due_date', 'asc')->
            get();

        if (count($invoices) > 0) {
            foreach ($invoices as $i => $invoice) {

                $invoice->numeroFactura = '-' . $invoice->numeroFactura;
                $invoice->prefix = trim($invoice->prefix);
                //        metodos q aplican la resta de los rc de caja q no han sido aplicados y
                $this->applyCashReceiptsToInvoiceBalance($invoice);
                //            aplican los dcto pronto pago
                $valueDPPAndDays = $this->applyDctPPToInvoices($invoices, $invoice);

                $fechaVencimiento = json_decode(json_encode($invoice->due_date));
                $invoice->fechaVencimiento = substr($fechaVencimiento->date, 0, 10);

                if ($invoice->saldo <= 0) {
                    unset($invoices[$i]);
                }
                $invoice->descuentoPP = $valueDPPAndDays[0];
                $invoice->days_doc = $valueDPPAndDays[1];
                unset($invoice->descuentopp);
                unset($invoice->receivable);
                unset($invoice->valorOriginal);
                //unset($invoice->issue_date);
                unset($invoice->documentsProducts);
            }
        }
        // se agrega una ultima factura que hace referencia a un anticipo
        $antic = new Document();
        $antic->id = 0;
        $antic->numeroFactura = '-1';
        $antic->prefix = 'ANTIC';
        $antic->urlFactura = null;
        $antic->issue_date = null;
        $antic->ValorOriginal = 10000000;
        $antic->retes = true;
        $antic->saldo = 10000000;
        $antic->document_last_state = null;
        $antic->descuentoPP = 0;
        $antic->days_doc = 0;
        $antic->fechaVencimiento = '';

        $invoices->push($antic);
        return $invoices->values();
    }

    public function getInvoicesCol($idcliente, $id = null)
    {
        //  filtro por sede de facturas de acuerdo al vendedor
        //$branchofficeIdAuthenticatedSeller=Auth::user()->branchoffice_id;
        $ldate = date('Ym');
        //dd($ldate);
        $invoices = Document::join('balances_for_documents', 'balances_for_documents.affects_document_id', '=', 'documents.id')
            ->join('accounts', 'accounts.id', '=', 'balances_for_documents.account_id')
            ->join('contacts', 'contacts.id', '=', 'documents.contact_id')
            ->join('parameters', 'parameters.id', '=', 'accounts.wallet_type')
            ->join('paramtables', 'parameters.paramtable_id', '=', 'paramtables.id')
            ->select(
                'documents.id',
                'documents.prefix',
                'documents.consecutive as numeroFactura',
                'documents.document_date as issue_date',
                'documents.due_date as fechaVencimiento',
                'balances_for_documents.final_balance as saldo',
                'balances_for_documents.year_month',
                'accounts.code'
            )
            ->selectRaw('(date(now()) - date(documents.due_date)) as days_due')
            ->selectRaw('case when (date(now()) - date(documents.due_date)) >0 then '."'Vencida'".' else '."'Normal'".' end as state')
            ->where(['parameters.code_parameter'=>'1','paramtables.code_table'=>'C-004'])
            ->where(['balances_for_documents.contact_id'=>$idcliente,'balances_for_documents.year_month'=>$ldate])
            ->orderBy('documents.due_date', 'asc')
            ->get();

            ///dd($invoices);

        return $invoices->values();
    }

    /**
     * Obtiene los recibos de caja
     * @param int iduser  Código del usuario
     * @return array
     * sumar el valor total
     */

    // recibos de caja de un vendedor sin consignar

    public function getCashReceipts($iduser)
    {

        $cashreceipts = CashReceiptsApp::select('id', 'consecutive AS numeroRecibo', 'invoice_link AS urlRecibo', 'creation_date AS fecha', 'warehouse_id')
            ->where('contact_id', $iduser)
            ->where('consignment_id', null)
            ->with(
                [
                    'warehouse',
                    'paymentMethodsApp',
                    'contributionInvoicesApp' => function ($query) {
                        $query->with(['document']);
                    },
                ]
            )->orderBy('id', 'asc')->get();

        $responseCashreceipts = [];
        $indice = 0;
        foreach ($cashreceipts as $key) {
            $key->efectivo = 0;
            $key->cheque = 0;
            $key->transferencia = 0;
            $key->numeroRecibo = 'RC-' . $key->numeroRecibo;
            $keyArr = $key->toArray();
            //me arroja un error al intentar acceder atravez de $keyArr
            foreach ($keyArr['contribution_invoices_app'] as $k) {
                $key->nombreCliente = $key->warehouse->description;
                break;
            }

            foreach ($keyArr['payment_methods_app'] as $value) {
                if ($value['parameter_id'] == 849) {
                    $key->efectivo += $value['total_paid'];
                } else if ($value['parameter_id'] == 850) {
                    $key->cheque += $value['total_paid'];
                } else if ($value['parameter_id'] == 851) {
                    $key->transferencia += $value['total_paid'];
                }
            }
            $key->valorTotal = $key->cheque + $key->efectivo + $key->transferencia;
            $keyy = $key->toArray();
            unset($keyy['contribution_invoices_app']);
            unset($keyy['payment_methods_app']);
            unset($keyy['warehouse']);
            $responseCashreceipts[$indice] = $keyy;
            $indice++;
        }

        return $responseCashreceipts;
    }

    public function getCashReceiptsAll($iduser = null, $idWarehouse = null, $dateRange = [])
    {
        //  parametro $all me indica si debo buscar todos los  rc
        // o solo los que faltan por consignacion

        $cashreceipts = CashReceiptsApp::select('id', 'consecutive AS numeroRecibo', 'invoice_link AS urlRecibo', 'creation_date AS fecha', 'warehouse_id')->where('contact_id', $iduser)
            ->when($idWarehouse != null, function ($query) use ($idWarehouse) {
                $query->where('warehouse_id', $idWarehouse);
            })
            ->when(count($dateRange) == 2, function ($query) use ($dateRange) {
                $query->where('creation_date', '>=', $dateRange[0])
                    ->Where('creation_date', '<=', $dateRange[1] . ' 23:59:59');
            })
            ->with(
                [
                    'warehouse',
                    'paymentMethodsApp',
                    'contributionInvoicesApp' => function ($query) {
                        $query->with(['document']);
                    },
                ]
            )->orderBy('creation_date', 'desc')
            ->get();

        $responseCashreceipts = [];
        $indice = 0;
        foreach ($cashreceipts as $key) {
            $key->efectivo = 0;
            $key->cheque = 0;
            $key->transferencia = 0;
            $key->numeroRecibo = 'RC-' . $key->numeroRecibo;
            $keyArr = $key->toArray();
            //me arroja un error al intentar acceder atravez de $keyArr
            foreach ($keyArr['contribution_invoices_app'] as $k) {
                $key->nombreCliente = $key->warehouse->description;
                break;
            }

            foreach ($keyArr['payment_methods_app'] as $value) {
                if ($value['parameter_id'] == 849) {
                    $key->efectivo += $value['total_paid'];
                } else if ($value['parameter_id'] == 850) {
                    $key->cheque += $value['total_paid'];
                } else if ($value['parameter_id'] == 851) {
                    $key->transferencia += $value['total_paid'];
                }
            }
            $key->valorTotal = $key->cheque + $key->efectivo + $key->transferencia;
            $keyy = $key->toArray();
            unset($keyy['contribution_invoices_app']);
            unset($keyy['payment_methods_app']);
            unset($keyy['warehouse']);
            $responseCashreceipts[$indice] = $keyy;
            $indice++;
        }

        return $responseCashreceipts;
    }

    /**
     * @param $document
     * @return mixed
     */
    public function finalizeDocument(Document $document)
    {
        // Finalizar Documento
        $document->fill(Arr::except($document->toArray(), [
            'total_value_brut',
            'total_value',
        ]));

        $document->save();

        // Retornar info
        return $document;
    }

    /**
     * Este metodo actualiza la informacion de
     * document_information.
     *
     * @param $document
     * @param null $request
     * @return
     * @author Kevin Galindo
     */
    public function finalizeDocumentInformation($document, $request = null)
    {
        // Organizamos la nformacion a actualizar
        $dataUpdate = [
            'vouchers_type_prefix'          => $document->vouchertype->prefix,
            'vouchers_type_range_from'      => $document->vouchertype->range_from,
            'vouchers_type_range_up'        => $document->vouchertype->range_up,
            'vouchers_type_resolution'      => $document->vouchertype->resolution,
            'document_name'                 => $request->document_name,
            'vouchers_type_electronic_bill' => $document->vouchertype->electronic_bill,
            'commission_percentage_applies' => !is_null($request) && $request->commission_percentage_applies === 'true',
            'commission_percentage'         => !is_null($request) ? $request->commission_percentage : 0,
            'protractor'                    => !is_null($request) && $request->protractor != 'null' ? $request->protractor : null,
            'guide_number'                  => !is_null($request) && $request->guide_number != 'null' ? $request->guide_number : null
        ];

        if (!isset($request->date_entry)) {
            $dataUpdatEntry = ['date_entry' => $request->date_entry];
            $dataUpdate = array_merge($dataUpdatEntry, $dataUpdate);
        }

        // Actualizamos la informacion.
        $document->documentsInformation->update($dataUpdate);

        // Retornar info
        return $document;
    }

    /**
     * listar facturas con productos
     * para despachos
     *
     * @author Santiago Torres
     */

    public function listInfoForDespatch($data)
    {
        $despatchs = array();
        foreach ($data as $key => $despatch_data) {
            foreach ($despatch_data as $key => $despatch) {
                $despatch->toArray();

                $new_despatch = [];
                $new_despatch["invoice"] = trim($despatch["prefix"]) . '' . $despatch["consecutive"];

                $new_despatch["zone"] = $despatch["warehouse"]["zone"] != null ? $despatch["warehouse"]["zone"]["name_parameter"] : null;
                if ($despatch["documentsInformation"] != null) {
                    $new_despatch["protractor"] = $despatch["documentsInformation"]["contact"] != null ? $despatch["documentsInformation"]["contact"]["identification"] . ' ' . $despatch["documentsInformation"]["contact"]["name"] : null;
                    $new_despatch["guide"] = $despatch["documentsInformation"]["guide_number"] != null ? $despatch["documentsInformation"]["guide_number"] : null;
                } else { $new_despatch["protractor"] = null;
                    $new_despatch["guide"] = null;
                }

                $new_despatch["client"] =
                /**$despatch["contact"]["identification"]. ' '. */
                $despatch["contact"]["name"] . ' ' . $despatch["contact"]["surname"];
                $new_despatch["Addres_client"] = $despatch["contact"]["address"] . ' / ' . $despatch["contact"]["city"]["city"];
                $new_despatch["telephone"] = $despatch["contact"]["main_telephone"];
                $new_despatch["contact"] = $despatch["contact"]["email"];
                $new_despatch["obs_Despatach"] = $despatch["observation"];
                $new_despatch["tot_items"] = $despatch["order"];
                $new_despatch["date"] = $despatch["document_date"];

                $new_despatch["tot_prod"] = 0;
                $new_despatch["tot_brut"] = 0;
                $new_despatch["weight"] = 0;

                $products = array();
                foreach ($despatch->documentsProducts as $key => $documentProduct) {
                    //return floatval (str_replace(',','.',$documentProduct["product"]["weight"]));
                    $new_despatch["weight"] += floatval(str_replace(',', '.', $documentProduct["product"]["weight"])) * $documentProduct["quantity"];
                    $new_product = [];
                    $new_product["code"] = $documentProduct["product"]["code"];
                    $new_product["description"] = $documentProduct["product"]["description"];
                    $new_product["line"] = $documentProduct["product"]["line"]["line_description"];
                    $new_product["lot"] = $documentProduct["lot"];
                    $new_product["quantity"] = $documentProduct["quantity"];

                    $new_despatch["tot_prod"] += $documentProduct["quantity"];
                    $new_despatch["tot_brut"] += $documentProduct["total_value_brut"];

                    $products[] = $new_product;
                }

                $new_despatch["products"] = $products;

                $despatchs[] = $new_despatch;
            }
            return $despatchs;
        }

        return $despatchs;
    }

    /**
     * listar facturas con productos
     * para despachos
     *
     * @author Santiago Torres
     */

    public function listProductsPickin($data)
    {
        $despatchs = array();
        foreach ($data as $key => $despatch_data) {
            foreach ($despatch_data as $key => $despatch) {
                $despatch->toArray();
                foreach ($despatch->documentsProducts as $key => $documentProduct) {
                    if ($documentProduct["product"]["manage_inventory"]) {
                        $new_product = [];
                        $new_product["branchoffice_warehouse_id"] = $despatch["branchoffice_warehouse_id"];
                        $new_product["code"] = $documentProduct["product"]["code"];
                        $new_product["description"] = $documentProduct["product"]["description"];
                        $new_product["line"] = $documentProduct["product"]["line"]["line_description"];
                        $new_product["quantity"] = $documentProduct["quantity"];
                        $new_product["location"] = $documentProduct["product"]->inventory[0]->location;

                        foreach ($documentProduct["product"]["inventory"] as $key => $inventory) {
                            if ($inventory["branchoffice_warehouse_id"] == $new_product["branchoffice_warehouse_id"]) {
                                $new_product["inventory"] = $inventory;
                            }
                        }

                        $despatchs[] = $new_product;
                    }
                }
            }

            // return $despatchs;

            $groupProductsForPickin = array_reduce($despatchs, function ($product, $item) {
                if (!isset($product[$item['branchoffice_warehouse_id']][$item['code']])) {
                    $product[$item['branchoffice_warehouse_id']][$item['code']] = [
                        'location' => $item['location'],
                        'code' => $item['code'],
                        'description' => $item['description'],
                        'line' => $item['line'],
                        'quantity' => $item['quantity'],
                    ];
                } else {
                    $product[$item['branchoffice_warehouse_id']][$item['code']]['quantity'] += $item['quantity'];
                }
                return $product;
            });

            return $groupProductsForPickin;
        }
    }

    /**
     * Este método se encarga de buscar y
     * retornar la información de cualquier documento.
     *
     * @param Request $request Contiene todos los posibles filtros a aplicar en la consulta
     * @return LengthAwarePaginator una colección de documentos
     * @author Kevin Galindo
     */
    public function getSearchDocument($request)
    {
        // Variables
        $perPage = $request->has('perPage') ? $request->perPage : 15;

        $branchoffice_id = $request->branchoffice_id;
        $warehouse_id = $request->warehouse_id;
        $vouchertype_id = $request->vouchertype_id;
        $model_id = $request->model_id;
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;
        $contact_id = $request->contact_id;
        $filterText = $request->filterText;

        // Realizamos la consulta
        $document = Document::with([
            'template.operationType',
            'contact:id,identification,name,surname',
            'seller:id,identification,name,surname',
            'warehouse:id,code,description',
            'vouchertype.template',
            'status',
        ])
            ->where('in_progress', false);

        if ($branchoffice_id) {
            $document->where('branchoffice_id', $branchoffice_id);
        }

        //Bodega
        if ($warehouse_id) {
            $document->where('branchoffice_warehouse_id', $warehouse_id);
        }

        if ($filterText) {
            $document->whereRaw('CAST(consecutive AS TEXT) ILIKE ?', ['%' . $filterText . '%']);
        }

        //Tipo de compra
        if ($vouchertype_id) {
            $vouchersType = VouchersType::find($vouchertype_id, ['id', 'code_voucher_type']);

            $document->whereHas('vouchertype', function ($query) use ($vouchersType) {
                $query->where('code_voucher_type', $vouchersType->code_voucher_type);
            });
        }

        //Modelo
        if ($model_id) {
            //$model = Template::where('id', $model_id)->first();
            $document->where('model_id', $model_id);
        }

        //Desde
        if ($dateFrom) {
            $document->where('document_date', '>=', $dateFrom . ' 00:00:00');
        }

        //Hasta
        if ($dateTo) {
            $document->where('document_date', '<=', $dateTo . ' 23:59:59');
        }

        //Contact
        if ($contact_id) {
            //$document->where('contact_id', $contact_id);
            $document->whereHas('contact', function ($query) use ($contact_id) {
                $query->where('identification', 'ilike', "%$contact_id%");
                $query->orWhere('name', 'ilike', "%$contact_id%");
                $query->orWhere('surname', 'ilike', "%$contact_id%");
            });
        }

        $document->orderBy('consecutive', 'DESC');

        return $document->paginate($perPage);
    }

    /**
     *
     * @author santiago torres
     */

    public function searchTransfer($id)
    {
        $transferDocument = Document::where('operationType_id', 3)
        // where('vouchertype_id', 13)
            ->where('thread', $id)
            ->with('branchofficeWarehouse.branchoffice')
            ->with(['user' => function($user){
                $user->withTrashed();
            }])
            ->with('branchoffice')
            ->with(['documentsInformation' => function ($query) {
                $query->with('additional_contact');
            }])
            ->with(['document' => function ($query) {
                $query->with('branchofficeWarehouse.branchoffice');
                $query->with('branchoffice');
                $query->with(['documentsProducts' => function ($query) {
                    $query->with('product');
                }]);
                $query->with(['documentsInformation' => function ($query) {
                    $query->with('additional_contact');
                    $query->with('additional_contact_warehouse');
                }]);
            }])
            ->with(['documentsProducts' => function ($query) {
                $query->with('product');
            }])
            ->with('contact')
            ->first();
        return $transferDocument;
    }

    /**
     *
     *
     */

    public function searchTransferEntry($params)
    {
        $transferDocument = Document::where('vouchertype_id', '13')
            ->where("operationType_id", '3')
        //->where('branchoffice_warehouse_id', $params->bodega_salida)
            ->where('branchoffice_id', $params->sede_salida)
            ->where("incorporated_document", false)
            ->with('branchofficeWarehouse')
            ->with('branchoffice')
            ->with(['documentsProducts' => function ($q) use ($params) {
                $q->with(['product' => function ($q) use ($params) {
                    $q->with('line');
                    $q->with('price');
                    $q->with('brand');
                    $q->with(['inventory' => function ($q) use ($params) {
                        $q->where('branchoffice_warehouse_id', $params->bodega_salida);
                    }]);
                    $q->with('subline');
                }]);
            }])
            ->with(['document' => function ($query) {
                $query->with('branchofficeWarehouse');
                $query->with('branchoffice');
            }])
            ->wherehas('branchofficeWarehouse', function ($query) use ($params) {
                $query->where('warehouse_code', 'TR');
                $query->where('id', $params->bodega_salida);
            })
            ->get();

        return $transferDocument;
    }

    /**
     *
     */
    public function getInfoForFile()
    {
        $documents = Document::where(function ($query) {
            $query->where('erp_id', '');
            $query->orWhereNull('erp_id');
        })
        //se finalizo
            ->where('in_progress', false)
            ->wherehas('operationType', function ($query) {
                $query->where('inventory_movement', 2);
            })
            ->wherehas('vouchertype', function ($query) {
                $query->where('code_voucher_type', '<>', 200);
            })
            ->with([
                'documentsInformation:id,commission_percentage_applies,commission_percentage,document_id',
                'template',
                'branchofficeWarehouse',
                'branchoffice',
                'operationType',
                'seller',
                'user' => function ($query) {
                    $query->withTrashed();
                },
                'document' => function ($query) {
                    $query->with([
                        'branchoffice',
                        'branchofficeWarehouse',
                        'vouchertype',
                    ]);
                },
                'vouchertype',
                'contact' => function ($query) {
                    $query->with([
                        'identificationt',
                        'city',
                        'customer',
                        'provider',
                    ]);
                },
                'warehouse' => function ($query) {
                    $query->with(
                        'typesNegotiationPortfolio_',
                        'paymentType',
                        'city',
                        'zone',
                        'branchoffice',
                        'seller'
                    );
                },
                'documentsProducts' => function ($query) {
                    $query->with([
                        'product' => function ($q) {
                            $q->with([
                                'line' => function ($query) {
                                    $query->with('taxes');
                                },
                                'brand',
                                'subbrand',
                                'subline',
                            ]);
                        },
                        'operationType',
                        'branchofficeWarehouse',
                        'account',
                        'documentProductTaxes',
                    ]);
                },
            ])
            ->orderBy('id', 'ASC')
            ->take(500)
            ->get();

        $contactswharehouse = $documents->map(function ($item, $key) {
            $item->vouchertype->template = Template::find($item->model_id);
            $item->cos_refer = 0;
            $item->total_cost = 0;

            if ($item->template->code == '142') {
                $dp_base = DocumentProduct::select('id', 'document_id', 'inventory_cost_value')->where('document_id', $item->thread)->get();
                foreach ($dp_base as $key => $dp_b) {
                    $item->cos_refer += $dp_b->inventory_cost_value;
                }
            }
            $products_not_exist = [];

            // foreach ($item->documentsProducts as $key => $dp) {
            //     if($dp->product == null){
            //         $products_not_exist[]=$dp->product_id;
            //     }
            // }

            // if (count($products_not_exist) > 0){
            //     dd($item->id);
            //     dd($products_not_exist);
            // }
            $item->documentsProducts->map(function ($dp, $key) use ($item) {
                $dp->account_code = !is_null($dp->account) ? $dp->account->code : null;
                $item->total_cost += $dp->inventory_cost_value;
                $dp->cos_refer = 0;

                $dp->costo_unit = $dp->quantity != 0 ? $dp->inventory_cost_value / $dp->quantity : 0;
                $dp->vr_iva = null;
                $dp->pc_iva = null;
                $dp->vr_impo = null;
                $dp->pc_impo = null;

                $item->commission_percentage_applies = 'NO';
                $item->commission_percentage = null;
                if (!is_null($item->documentsInformation)) {
                    if ($item->documentsInformation->commission_percentage_applies) {
                        $item->commission_percentage_applies = 'SI';
                        $item->commission_percentage = number_format($item->documentsInformation->commission_percentage, 2, '.', '');
                    }
                }

                $dp->cod_line = $dp->product->line ? $dp->product->line->line_code : null;
                $dp->description_line = $dp->product->line ? $dp->product->line->line_description : null;

                $dp->cod_subline = $dp->product->subline ? $dp->product->subline->subline_code : null;
                $dp->description_subline = $dp->product->subline ? $dp->product->subline->subline_description : null;

                $dp->cod_brand = $dp->product->brand ? $dp->product->brand->brand_code : null;
                $dp->description_brand = $dp->product->brand ? $dp->product->brand->description : null;

                $dp->cod_subbrand = $dp->product->subbrand ? $dp->product->subbrand->subbrand_code : null;
                $dp->description_subbrand = $dp->product->subbrand ? $dp->product->subbrand->description : null;

                $dp->documentProductTaxes->map(function ($dpt, $key) use ($dp) {
                    switch ($dpt->tax_id) {
                        case '1':
                            $dp->vr_iva = $dpt->value;
                            $dp->pc_iva = $dpt->tax_percentage;
                            break;

                        case '4':
                            $dp->vr_impo = $dpt->value;
                            $dp->pc_impo = $dpt->tax_percentage;
                            break;
                    }
                });
            });

            if ($item->document) {
                $item->origin_branchoffice_warehouse_code = $item->document->branchofficeWarehouse->warehouse_code;

                $item->document_branchoffice_code = $item->document->branchoffice->code;
                $item->document_vouchertype_code_voucher_type = $item->document->vouchertype->code_voucher_type;
                $item->document_consecutive = $item->document->consecutive;
                $item->document_prefix = $item->document->prefix;
            } else {
                $item->origin_branchoffice_warehouse_code = '';
                $item_document_branchoffice_code = '';
                $item->document_vouchertype_code_voucher_type = '';
                $item->document_prefix = '';
            }

            $item->document_cross_prefix = $item->crossing_prefix;
            $item->document_cross_con = $item->crossing_number_document;

            if ($item->seller) {
                $item->seller_identification = $item->seller->identification;
                $item->seller_name = $item->seller->name;
            } else {
                $item->seller_identification = '';
                $item->seller_name = '';
            }

            $item->hour = explode(' ', $item->created_at);
            $item->hour = str_replace(':', '', $item->hour[1]);

            // $item->duedate = explode(' ', $item->due_date);
            // $item->duedate = str_replace('-', '', $item->duedate[0]);
            if ($item->due_date != null) {
                $item->duedate = date_format($item->due_date, 'Ymd');

                if ($item->duedate != date_format($item->due_date, 'Ymd')) {
                    $item->duedate = date_format($item->due_date, 'Ymd');
                }
            } else {
                $item->duedate = null;
            }

            $item->iva = 0;
            $item->rete_iva = 0;
            $item->rete_fuente = 0;
            $item->rete_ica = 0;
            $item->ipoconsumo = 0;
            $a = [];
            $item->TOT_CANTS = count($item->documentsProducts);

            foreach ($item->documentsProducts as $key => $documentProduct) {
                $a[$key] = $documentProduct->product_id;

                foreach ($documentProduct->product->line->taxes as $key => $value) {
                    switch ($value->id) {
                        case 1:
                            $documentProduct->PC_IVA_COMPRA_PROD = $value->percentage_purchase;
                            $documentProduct->PC_IVA_VENTA_PROD = $value->percentage_sale;
                            break;

                        case 4:
                            $documentProduct->PC_IMPO_COMPRA_PROD = $value->percentage_purchase;
                            $documentProduct->PC_IMPO_VENTA_PROD = $value->percentage_sale;
                            break;
                    }
                }

                foreach ($documentProduct->documentProductTaxes as $key => $documentProductTaxes) {
                    switch ($documentProductTaxes->tax_id) {
                        case '1':
                            $item->iva += $documentProductTaxes->value;
                            break;

                        case '2':
                            $item->rete_iva += $documentProductTaxes->value;
                            break;

                        case '3':
                            $item->rete_fuente += $documentProductTaxes->value;
                            break;

                        case '5':
                            $item->rete_ica += $documentProductTaxes->value;
                            break;

                        case '4':
                            $item->ipoconsumo += $documentProductTaxes->value;
                            break;
                    }
                }
                $item->rete_iva = $item->ret_iva_total;
                $item->rete_fuente = $item->ret_fue_total;
                $item->rete_ica = $item->ret_ica_total;
            }
            $item->TOT_prods = count(array_unique($a));

            $item->base_iva = $item->total_value_brut + $item->iva;
            $item->base_INPO = $item->total_value_brut + $item->ipoconsumo;

            $item->date = str_replace('-', '', $item->document_date);

            if ($item->warehouse != null) {
                $item->warehouse->payment_type = $item->warehouse->paymentType == null ? null : $item->warehouse->paymentType->name_parameter;
                $item->warehouse->types_negotiation = $item->warehouse->typesNegotiationPortfolio_ == null ? null : $item->warehouse->typesNegotiationPortfolio_->name_parameter;
                $item->warehouse->seller_ident = $item->warehouse->seller == null ? null : $item->warehouse->seller->identification;
                $item->warehouse->des_ident = $item->warehouse->seller == null ? null : $item->warehouse->seller->name . ' ' . $item->warehouse->seller->surname;
                $department_id = $item->warehouse->city == null ? null : ($item->warehouse->city->department_id < '10' ? '0' . $item->warehouse->city->department_id : $item->warehouse->city->department_id);
                $item->warehouse->code_suc_city = $item->warehouse->city == null ? null : $department_id . '' . $item->warehouse->city->city_code;
                $item->warehouse->code_desc_city = $item->warehouse->city == null ? null : $item->warehouse->city->city;
                $item->warehouse->code_zone = $item->warehouse->zone == null ? null : $item->warehouse->zone->code_parameter;
                $item->warehouse->code_branchoffice = $item->warehouse->branchoffice == null ? null : $item->warehouse->branchoffice->code;
            }

            $item->contact->code_categ_prov = is_null($item->contact->provider) ? null : $item->contact->provider->category_id;
            $item->contact->code_categ_cli = is_null($item->contact->customer) ? null : $item->contact->customer->category_id;

            $item->contact->customer_retention_source = is_null($item->contact->customer) ? 'NO' : ($item->contact->customer->retention_source != null ? 'SI' : 'NO');
            $item->contact->customer_retention_iva = is_null($item->contact->customer) ? 'NO' : ($item->contact->customer->retention_iva != null ? 'SI' : 'NO');
            $item->contact->customer_retention_ica = is_null($item->contact->customer) ? 'NO' : ($item->contact->customer->retention_ica != null ? 'SI' : 'NO');
            $item->contact->provider_retention_source = is_null($item->contact->provider) ? 'NO' : ($item->contact->provider->retention_source != null ? 'SI' : 'NO');
            $item->contact->provider_retention_iva = is_null($item->contact->provider) ? 'NO' : ($item->contact->provider->retention_iva != null ? 'SI' : 'NO');
            $item->contact->provider_retention_ica = is_null($item->contact->provider) ? 'NO' : ($item->contact->provider->retention_ica != null ? 'SI' : 'NO');

            $item->contact->_is_customer = $item->contact->is_customer == 1 ? 'SI' : 'NO';
            $item->contact->_is_provider = $item->contact->is_provider == 1 ? 'SI' : 'NO';
            $item->contact->_is_employee = $item->contact->is_employee == 1 ? 'SI' : 'NO';
            $item->contact->code_parameter = $item->contact->identificationt != null ? $item->contact->identificationt->code_parameter : null;
            $fiscal_responsibility_code = Parameter::find($item->contact->fiscal_responsibility_id);
            $item->contact->fiscal_responsibility_code = $fiscal_responsibility_code ? $fiscal_responsibility_code->code_parameter : null;
            return [
                "warehouse" => $item->warehouse,
                "contact" => $item->contact,
            ];
        });

        //return array_keys($contactswharehouse[0]['warehouse']->toArray());

        /*foreach ($contactswharehouse as $key => $contactwharehouse) {
        foreach ( array_keys($contactswharehouse[0]['warehouse']->toArray()) as $key => $keya) {
        // $a = trim($contactwharehouse['warehouse']->$keya);
        $contactwharehouse['warehouse'][$keya] =trim($contactwharehouse['warehouse'][$keya]);
        }
        }*/

        return [$contactswharehouse->unique()->toArray(), $documents];
    }

    public function createDocumentForDocumentTransaction($request)
    {
        $exists = true;
        $consecutive = explode('-', $request->num_doc);
        $prefix = $request->prefix;
        $consecutive = count($consecutive) == 1 ? $consecutive[0] : $consecutive[1];
        while ($exists) {
            $aux = Document::where('consecutive', $consecutive)
                ->where('vouchertype_id', $request->voucherType_id)
                ->where('in_progress', false)
                ->exists();
            if ($aux) {
                ++$consecutive;
            } else {
                $exists = false;
            }
        }

        $model = Template::find($request->template_id);
        // $total = $this->getTotalDC($request);

        $data = [
            'vouchertype_id' => $request->voucherType_id,
            'consecutive' => $consecutive,
            'prefix' => $prefix,
            'user_id' => $request->user_id,
            'contact_id' => $request->contact_id,
            'branchoffice_id' => $request->branchoffice_id,
            "incorporated_document" => false,
            "observation" => $request->observation,
            "observation_two" => $request->observation2,
            "issue_date" => Carbon::now(),
            'document_date' => strlen($request->date) == 10 ? $request->date . date(' H:i:s') : $request->date,
            'city' => $request->city_id,
            'operationType_id' => $model ? $model->operationType_id : null,
            'model_id' => $request->template_id,
            // 'accounting_debit_value'            =>  $total['debit'],
            // 'accounting_credit_value'           =>  $total['credit'],
            'accounting_user_last_modification' => $request->user_id,
            'accounting_date_last_modification' => Carbon::now(),
            'center_cost_id' => $request->center_cost,
            'projects_id' => $request->projects_id,
            'seller_id' => $request->seller_id,

            'affects_prefix_document' => strtoupper($request->affects_prefix_document),
            'affects_number_document' => $request->affects_number_document,
            // 'affects_date_document' => $request->crossing_due_date_document,
            'due_date' => $request->due_date,
            'term_days' => $request->term_days,

        ];
        $document = Document::create($data);
        if (VouchersType :: find($request->voucherType_id)->origin_document) {
            $document->affects_date_document = $document->document_date;
            $document->affects_document_id = $document->id;
            $document->save();
        }
        return $document;
    }

    public function getTotalDC($request)
    {
        $total = [];
        foreach ($request->document_transactions as $key => $document_transaction) {
            if ($document_transaction->debit_credit == 1) {
                $total['debit'] += $document_transaction->value;
            } elseif ($document_transaction->debit_credit == 2) {
                $total['credit'] += $document_transaction->value;
            }
        }
        return $total;
    }

    public function updateDocumentForDocumentTransaction($document, $request)
    {
        $model = Template::find($request->template_id);
        Document::where('id', $document)
            ->update([
                'user_id' => $request->user_id,
                'contact_id' => $request->contact_id,
                'branchoffice_id' => $request->branchoffice_id,
                "observation" => $request->observation,
                "observation_two" => $request->observation2,
                'document_date' => strlen($request->date) == 10 ? $request->date . date(' H:i:s') : $request->date,
                'city' => $request->city_id,
                'operationType_id' => $model ? $model->operationType_id : null,
                'model_id' => $request->template_id,
                'accounting_user_last_modification' => $request->user_id,
                'accounting_date_last_modification' => Carbon::now(),
                'center_cost_id' => $request->center_cost,
                'projects_id' => $request->projects_id,
                'seller_id' => $request->seller_id,
                'affects_prefix_document' => strtoupper($request->affects_prefix_document),
                'affects_number_document' => $request->affects_number_document,
                // 'affects_date_document' => $request->crossing_date_document,
                'due_date' => $request->due_date,
                'term_days' => $request->term_days,
            ]);

        return Document::find($document);
    }

    /**
     * crear documentos para las solicitudes de sericio
     * @author santiago torres | kevin galindo
     */

    public function createDocument(array $input)
    {
        try {

            DB::beginTransaction();
            // Calcular el número del consecutivo
            $vouchertype_id = 1;

            if (!isset($input['vouchertype_id']) || $input['vouchertype_id'] == null) {
                $vouchertype_id = 1;
                // hacer consulta para saber si quien esta creando el pedido(iduser), es el mismo vendedor(sellerid), si es asi dejar el viewed en true
                // hacer coincidir seller_id y user_id para poderlos comparar
                $user = DB::table('users')->select('contact_id')->where('id', $input['user_id'])->first();
                if ($user->contact_id == $input['seller_id']) {
                    $input['viewed'] = true;
                }
            } else {
                $vouchertype_id = $input['vouchertype_id'];
            }

            $consecutive = Document::where('vouchertype_id', $vouchertype_id)->max('consecutive');
            if ($consecutive) {
                $input['consecutive'] = 1;
            } else {
                $input['consecutive'] = $consecutive + 1;
            }

            if (!isset($input['branchoffice_id']) || $input['branchoffice_id'] == null) {
                // Consultar la sede por defecto
                $branchoffice = DB::table('branchoffice_warehouses')
                    ->select('branchoffice_warehouses.id', 'branchoffice_warehouses.branchoffice_id')
                    ->join('branchoffices AS b', 'b.id', '=', 'branchoffice_warehouses.branchoffice_id')
                    ->where('b.main', true)
                    ->where('branchoffice_warehouses.warehouse_code', '01')
                    ->first();

                if ($branchoffice) {
                    $input['branchoffice_id'] = $branchoffice->branchoffice_id;
                    $input['branchoffice_warehouse_id'] = $branchoffice->id;
                }
            }

            $input['issue_date'] = date('Y-m-d');

            // Traemos el prefijo
            $input['prefix'] = VouchersType::find($input['vouchertype_id'])->prefix;

            // Crear documento en la base de datos
            //dd($input['document_date']);
            $cart = Document::create(Arr::only($input, [
                'contact_id',
                'warehouse_id',
                'consecutive',
                'branchoffice_id',
                'branchoffice_warehouse_id',
                'vouchertype_id',
                'seller_id',
                'user_id',
                'address',
                'viewed',
                'in_progress',
                'issue_date',
                'prefix',
                'document_date',
            ]));

            $documentInformation = new DocumentInformation();
            $documentInformation->price_list = $input['documents_information']['price_list'];

            $cart->documentsInformation()->save($documentInformation);

            // Guardar thread
            Document::where('id', $cart->id)->update([
                'thread' => $cart->id,
            ]);

            DB::commit();
            // Retornar los datos del documento recién creado
            return $this->getDocument($cart->id);
        } catch (\Exception $e) {
            Log::error($e->getMessage() . " " . $e->getLine());
            DB::rollBack();
        }
    }

    /**
     * Actualiza Id_erp del document
     *
     * @author Santiago Torres
     */

    public function updateErpId($id, $erpId)
    {
        $document = Document::find($id);
        if ($document) {
            $document->erp_id = $erpId;
            $document->save();
            return $document;
        } else {
            return 'no existe el documento';
        }
    }

    /**
     *
     * traslados para archivo plano
     *
     * @author santiago torres
     */

    public function gettransfersForFile()
    {
        $documents = Document::where(function ($query) {
            $query->where('erp_id', '');
            $query->orWhereNull('erp_id');
        })
        //se finalizo
            ->where('in_progress', false)
            ->whereIn('operationType_id', [3, 4]) // tipo de operación
        // ->where('vouchertype_id', 13)
            ->with([
                'branchofficeWarehouse',
                'branchoffice',
                'operationType',
                'seller',
                'user',
                'document' => function ($query) {
                    $query->with([
                        'branchoffice',
                        'branchofficeWarehouse',
                        'vouchertype',
                    ]);
                },
                'vouchertype',
                'contact' => function ($query) {
                    $query->with([
                        'identificationt',
                        'city',
                        'customer',
                        'provider',
                    ]);
                },
                'warehouse' => function ($query) {
                    $query->with(
                        'city',
                        'zone'
                    );
                },
                'documentsProducts' => function ($query) {
                    $query->with([
                        'product' => function ($q) {
                            $q->with([
                                'line' => function ($query) {
                                    $query->with('taxes');
                                },
                                'brand',
                                'subbrand',
                                'subline',
                            ]);
                        },
                        'operationType',
                        'documentProductTaxes',
                    ]);
                },
            ])
            ->orderBy('id', 'ASC')
            ->take(500)
            ->get();

        $contactswharehouse = $documents->map(function ($item, $key) {
            $item->vouchertype->template = Template::find($item->model_id);
            $item->total_cost = 0;
            $item->documentsProducts->map(function ($dp, $key) use ($item) {
                $item->total_cost += $dp->inventory_cost_value;
                $dp->costo_unit = $dp->quantity != 0 ? $dp->inventory_cost_value / $dp->quantity : 0;
                $dp->vr_iva = null;
                $dp->pc_iva = null;
                $dp->vr_impo = null;
                $dp->pc_impo = null;

                $dp->cod_line = $dp->product->line ? $dp->product->line->line_code : null;
                $dp->description_line = $dp->product->line ? $dp->product->line->line_description : null;

                $dp->cod_subline = $dp->product->subline ? $dp->product->subline->subline_code : null;
                $dp->description_subline = $dp->product->subline ? $dp->product->subline->subline_description : null;

                $dp->cod_brand = $dp->product->brand ? $dp->product->brand->brand_code : null;
                $dp->description_brand = $dp->product->brand ? $dp->product->brand->description : null;

                $dp->cod_subbrand = $dp->product->subbrand ? $dp->product->subbrand->subbrand_code : null;
                $dp->description_subbrand = $dp->product->subbrand ? $dp->product->subbrand->description : null;

                $dp->documentProductTaxes->map(function ($dpt, $key) use ($dp) {
                    switch ($dpt->tax_id) {
                        case '1':
                            $dp->vr_iva = $dpt->value;
                            $dp->pc_iva = $dpt->tax_percentage;
                            break;

                        case '4':
                            $dp->vr_impo = $dpt->value;
                            $dp->pc_impo = $dpt->tax_percentage;
                            break;
                    }
                });

                if ($item->document) {
                    $dp->cost_ref = $dp->cost_ref;
                } else {
                    $dp->cost_ref = '';
                }
            });

            if ($item->document) {
                $item->origin_branchoffice_warehouse_code = $item->document->branchofficeWarehouse->warehouse_code;
                if ($item->voucherType->id == 13) {
                    $item->vouchertype_code_voucher_type = 140;
                } else {
                    $item->vouchertype_code_voucher_type = $item->voucherType->code_voucher_type;
                }
                $item->operationType_code = 140;
                $item->vouchertype_template_code = 140;

                $item->document_branchoffice_code = $item->document->branchoffice->code;
                $item->document_vouchertype_code_voucher_type = 240;
                $item->document_consecutive = $item->document->consecutive;
                $item->document_prefix = $item->document->prefix;
            } else {
                $item->origin_branchoffice_warehouse_code = '';
                if ($item->voucherType->id == 13) {
                    $item->vouchertype_code_voucher_type = 240;
                } else {
                    $item->vouchertype_code_voucher_type = $item->voucherType->code_voucher_type;
                }
                // $item->vouchertype_code_voucher_type = 240;
                $item->operationType_code = 240;
                $item->vouchertype_template_code = 240;

                $item_document_branchoffice_code = '';
                $item->document_vouchertype_code_voucher_type = '';
                $item->document_prefix = '';
            }

            if ($item->seller) {
                $item->seller_identification = $item->seller->identification;
                $item->seller_name = $item->seller->name;
            } else {
                $item->seller_identification = '';
                $item->seller_name = '';
            }

            $item->hour = explode(' ', $item->created_at);
            $item->hour = str_replace(':', '', $item->hour[1]);

            // $item->duedate = explode(' ', $item->due_date);
            // $item->duedate = str_replace('-', '', $item->duedate[0]);
            if ($item->due_date != null) {
                $item->duedate = date_format($item->due_date, 'Ymd');

                if ($item->duedate != date_format($item->due_date, 'Ymd')) {
                    $item->duedate = date_format($item->due_date, 'Ymd');
                }
            } else {
                $item->duedate = null;
            }

            $item->iva = 0;
            $item->rete_iva = 0;
            $item->rete_fuente = 0;
            $item->rete_ica = 0;
            $item->ipoconsumo = 0;
            $a = [];
            $item->TOT_CANTS = count($item->documentsProducts);

            foreach ($item->documentsProducts as $key => $documentProduct) {
                $a[$key] = $documentProduct->product_id;

                foreach ($documentProduct->product->line->taxes as $key => $value) {
                    switch ($value->id) {
                        case 1:
                            $documentProduct->PC_IVA_COMPRA_PROD = $value->percentage_purchase;
                            $documentProduct->PC_IVA_VENTA_PROD = $value->percentage_sale;
                            break;

                        case 4:
                            $documentProduct->PC_IMPO_COMPRA_PROD = $value->percentage_purchase;
                            $documentProduct->PC_IMPO_VENTA_PROD = $value->percentage_sale;
                            break;
                    }
                }

                foreach ($documentProduct->documentProductTaxes as $key => $documentProductTaxes) {
                    switch ($documentProductTaxes->tax_id) {
                        case '1':
                            $item->iva += $documentProductTaxes->value;
                            break;

                        case '2':
                            $item->rete_iva += $documentProductTaxes->value;
                            break;

                        case '3':
                            $item->rete_fuente += $documentProductTaxes->value;
                            break;

                        case '5':
                            $item->rete_ica += $documentProductTaxes->value;
                            break;

                        case '4':
                            $item->ipoconsumo += $documentProductTaxes->value;
                            break;
                    }
                }
                $item->rete_iva = $item->ret_iva_total;
                $item->rete_fuente = $item->ret_fue_total;
                $item->rete_ica = $item->ret_ica_total;
            }
            $item->TOT_prods = count(array_unique($a));

            $item->base_iva = $item->total_value_brut + $item->iva;
            $item->base_INPO = $item->total_value_brut + $item->ipoconsumo;

            $item->date = str_replace('-', '', $item->document_date);

            if ($item->warehouse != null) {
                $item->warehouse->code_suc_city = $item->warehouse->city == null ? null : $item->warehouse->city->city_code;
                $item->warehouse->code_zone = $item->warehouse->zone == null ? null : $item->warehouse->zone->code_parameter;
            }

            $item->contact->code_categ_prov = $item->contact->provider->category_id;
            $item->contact->code_categ_cli = $item->contact->customer->category_id;

            $item->contact->customer->_retention_source = $item->contact->customer->retention_source != null ? 'SI' : 'NO';
            $item->contact->customer->_retention_iva = $item->contact->customer->retention_iva != null ? 'SI' : 'NO';
            $item->contact->customer->_retention_ica = $item->contact->customer->retention_ica != null ? 'SI' : 'NO';

            $item->contact->provider->_retention_source = $item->contact->provider->retention_source != null ? 'SI' : 'NO';
            $item->contact->provider->_retention_iva = $item->contact->provider->retention_iva != null ? 'SI' : 'NO';
            $item->contact->provider->_retention_ica = $item->contact->provider->retention_ica != null ? 'SI' : 'NO';

            $item->contact->_is_customer = $item->contact->is_customer == 1 ? 'SI' : 'NO';
            $item->contact->_is_provider = $item->contact->is_provider == 1 ? 'SI' : 'NO';
            $item->contact->_is_employee = $item->contact->is_provider == 1 ? 'SI' : 'NO';
            $item->contact->code_parameter = $item->contact->identificationt != null ? $item->contact->identificationt->code_parameter : null;
            return [
                "warehouse" => $item->warehouse,
                "contact" => $item->contact,
            ];
        });

        //return array_keys($contactswharehouse[0]['warehouse']->toArray());

        /*foreach ($contactswharehouse as $key => $contactwharehouse) {
        foreach ( array_keys($contactswharehouse[0]['warehouse']->toArray()) as $key => $keya) {
        // $a = trim($contactwharehouse['warehouse']->$keya);
        $contactwharehouse['warehouse'][$keya] =trim($contactwharehouse['warehouse'][$keya]);
        }
        }*/

        return [$contactswharehouse->unique()->toArray(), $documents];
    }

    /**
     * obtiene los docuementsProducts para recontruir la información
     *
     * @author Santiago Torres
     */
    public function searchDocumentsProductsReconstructor($request, $product, $idDocumentsValues)
    {
        $kardex = Document::where('in_progress', false)
            ->with(['documentsProducts' => function ($query) use ($request, $product) {
                $query->where('product_id', $product);
                $query->where('manage_inventory', true);
                $query->with(['operationType' => function ($query) {
                    $query->where('affectation', '<>', 3);
                }]);
                $query->whereHas('operationType', function ($query) {
                    $query->where('affectation', '<>', 3);
                });
                $query->with(['document' => function ($query) use ($request) {
                    // if ($request->date) {
                    //     $query->where('document_date', '>=', $request->date);
                    // }
                }]);
                $query->orderBy('id', 'desc');
            }])
            ->whereHas('documentsProducts', function ($query) use ($request, $product) {
                $query->where('product_id', $product);
                $query->where('manage_inventory', true);
                $query->with(['operationType' => function ($query) {
                    $query->where('affectation', '<>', 3);
                }]);
                $query->whereHas('operationType', function ($query) {
                    $query->where('affectation', '<>', 3);
                });
                $query->with(['document' => function ($query) use ($request) {
                    if ($request->date) {
                        $query->where('document_date', '>=', $request->date);
                    }
                }]);
                $query->orderBy('id', 'desc');
            });

        if ($request->date) {
            $kardex->where('document_date', '>=', $request->date);
        }

        $kardex->orderBy('document_date', 'ASC')
            ->get();

        $kardex = $kardex->get();
        $fullKardex = [];

        foreach ($kardex as $key => $doc) {
            foreach ($doc->documentsProducts as $key => $value) {
                $value->fecha = str_replace(' ', '-', $value->document->document_date);
                // $value->fecha = str_replace(' ', '-', $doc->document_date);
                if ($value->quantity != 0) {
                    $value->Vr_Un_Costo = number_format($value->inventory_cost_value / $value->quantity, 2, '.', '');
                } else {
                    $value->Vr_Un_Costo = 0;
                }

                if ($value->stock_after_operation != 0) {
                    $value->Cto_Prom = number_format($value->stock_value_after_operation / $value->stock_after_operation, 2, '.', '');
                } else {
                    $inv = Inventory::where('product_id', $value->product_id)->where('branchoffice_warehouse_id', $value->document->branchoffice_warehouse_id)->first();
                    $value->Cto_Prom = 0;
                }

                $fullKardex[] = $value;
            }
        }
        return $fullKardex;
    }

    /**
     * Obtiene el ultimo docuent para reconstrucrtor
     * @author Santiago Torres
     */
    public function getStockAndStockValues($request, $branchoffice_warehouse_id, $product_id = false)
    {

        //agregar filtro por fcha
        $document = Document::where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->select('id', 'document_date', 'branchoffice_warehouse_id')
            ->with(['documentsProducts' => function ($query) use ($request, $product_id) {
                $query->select('stock_after_operation', 'stock_value_after_operation', 'document_id');
                $query->where('stock_after_operation', '<>', null);
                if ($product_id) {
                    $query->where('product_id', $product_id);
                }
            }])
            ->wherehas('documentsProducts', function ($query) use ($request, $product_id) {
                $query->select('stock_after_operation', 'stock_value_after_operation', 'document_id');
                $query->where('stock_after_operation', '<>', null);
                if ($product_id) {
                    $query->where('product_id', $product_id);
                }
            });
        if ($request->date) {
            // si hay fecha se buscan los documentos anteriores se lista en orden descendente y se toma el primero
            // para tomar un documento anterior
            $document->where('document_date', '<', $request->date);
            $document->orderby('document_date', 'DESC');
        } else {
            $document->orderby('document_date', 'ASC');
        }
        return $document->first();
    }

    public function getInvoiceReceipt(array $data)
    {
        try {
            $cash = CashReceiptsApp::select('id', 'consecutive', 'creation_date as fecha', 'warehouse_id')
                ->where('id', (int) $data['rc'])->where('contact_id', $data['iduser'])
                ->with(['contributionInvoicesApp.document' => function ($query) {
                    $query->with(['receivable']);
                }])->with([
                'paymentMethodsApp',
                'warehouse' => function ($query) {
                    $query->with('contactt', 'seller.users');
                },
            ])->first();

            if ($cash) {

                $cashArray = $cash->toArray();
                unset($cashArray['id']);
                $cashArray['id'] = 'RC-' . $cashArray['consecutive'] . ' (Duplicado)';
                unset($cashArray['consecutive']);
                $cashArray['totalPagado'] = 0;
                $cashArray['cliente'] = $cashArray['warehouse']['contactt']['name'] . ' ' . $cashArray['warehouse']['contactt']['surname'];
                $cashArray['iden'] = $cashArray['warehouse']['contactt']['identification'];
                $cashArray['vendedor'] = $cashArray['warehouse']['seller'] ? $cashArray['warehouse']['seller']['name'] . ' ' . $cashArray['warehouse']['seller']['surname'] : "";

                //EMAILS
                $cashArray['vendedorEmail'] = $cashArray['warehouse']['seller'] ? $cashArray['warehouse']['seller']['email'] : "";
                if ($cashArray['warehouse']['seller']) {
                    $cashArray['emailCashReceiptsCopy'] = count($cashArray['warehouse']['seller']['users']) > 0 ? $cashArray['warehouse']['seller']['users'][0]['email_cash_receipts_app'] : '';
                } else {
                    $cashArray['emailCashReceiptsCopy'] = "";
                }
                $cashArray['ClienteEmail'] = $cashArray['warehouse']['contactt']['email'];

                //facturas
                $cashArray['facturas'] = [];
                $cashArray['formasPago'] = [];
                $cashArray['descuentoProntoPago'] = [];
                foreach ($cashArray['contribution_invoices_app'] as $key) {
                    $fact = new stdClass();
                    $fact->id = $key['document_id'];
                    $fact->fecha = $key['created_at'];
                    $fact->numeroFactura = $key['document_id'] ? 'F-' . $key['document']['consecutive'] : 'ANTIC-1';
                    $fact->saldo = $key['invoice_document'] != null ? $key['invoice_document'] : 0;
                    $fact->valorConsignar = $key['total_paid'];
                    $cashArray['totalPagado'] += $fact->valorConsignar;

                    //retneciones
                    $fact->rete_fte = $key['rete_fte'];
                    $fact->rete_iva = $key['rete_iva'];
                    $fact->rete_ica = $key['rete_ica'];
                    $fact->dctpp = $key['value_discount_prompt_payment'];

                    if ($key['document']) {
                        foreach ($key['document']['receivable'] as $value) {
                            $fact->saldo = round($value['invoice_balance']);
                        }
                    } else {
                        $fact->saldo = 10000000;
                    }

                    if ($key['value_discount_prompt_payment'] != null && $key['invoice_days'] != null) {
                        $descuentoAplicado = new stdClass();
                        $descuentoAplicado->numeroFactura = $fact->numeroFactura;
                        $descuentoAplicado->diasFactura = $key['invoice_days'];
                        $descuentoAplicado->descuentoPP = $key['value_discount_prompt_payment'];

                        array_push($cashArray['descuentoProntoPago'], $descuentoAplicado);
                    }
                    array_push($cashArray['facturas'], $fact);
                }

                //metodos de pago
                $formaPagoE = null;
                new stdClass();
                $band = 0;
                foreach ($cashArray['payment_methods_app'] as $key) {

                    if ($key['parameter_id'] == 849) {
                        if ($band == 0) {
                            $formaPagoE = new stdClass();
                            $formaPagoE->id = $key['id'];
                            $formaPagoE->tipo = 'Efectivo';
                            $formaPagoE->valor = 0;
                            $formaPagoE->entidad = $key['bank'];
                            $formaPagoE->numRef = $key['check_number'];
                        }

                        $formaPagoE->valor += $key['total_paid'];
                        if (($band + 1) == count($cashArray['payment_methods_app'])) {
                        }

                        $band++;
                    }
                    if ($key['parameter_id'] == 850) {
                        $formaPago = new stdClass();
                        $formaPago->id = $key['id'];
                        $formaPago->tipo = 'Cheque';
                        $formaPago->valor = $key['total_paid'];
                        $formaPago->entidad = $key['bank'];
                        $formaPago->numRef = $key['check_number'];
                        array_push($cashArray['formasPago'], $formaPago);
                    }

                    if ($key['parameter_id'] == 851) {
                        $formaPago = new stdClass();
                        $formaPago->id = $key['id'];
                        $formaPago->tipo = 'Transferencia';
                        $formaPago->valor = $key['total_paid'];

                        $formaPago->entidad = $key['bank'];
                        $formaPago->numRef = $key['check_number'];
                        array_push($cashArray['formasPago'], $formaPago);
                    }
                }
                if (!$formaPagoE == null) {
                    array_push($cashArray['formasPago'], $formaPagoE);
                }
                unset($cashArray['payment_methods_app']);
                unset($cashArray['warehouse']);
                unset($cashArray['contribution_invoices_app']);
                return $cashArray;
            }
            return [];
        } catch (\Exception $th) {
            \Log::info($th);
            throw new \Exception('ERROR in the line ' . $th->getLine() . ' of ' . $th->getFile() . ' : ' . $th->getMessage());
        }
    }

    public function getOrdersManagement($request, $user)
    {
        $seller_id = null;

        if ($user->hasRole('Vendedor')) {
            $seller_id = $user->contact_id;
        } else if (isset($request->seller_id)) {
            $seller_id = $request->seller_id;
        }

        $documents = Document::select('documents.id', 'document_date', 'consecutive', 'documents.contact_id', 'documents.seller_id', 'documents.user_id', 'documents.observation', 'total_value', 'total_value_brut', 'vouchertype_id', 'status_id_authorized', 'state_backorder', 'closed')
            ->selectRaw('(CASE WHEN documents.user_id = cw.seller_id THEN true ELSE false END) AS make_customer')
            ->join('contacts_warehouses AS cw', 'cw.id', '=', 'documents.warehouse_id')
            ->with([
                'stateBackorder',
                'user:id,contact_id',
                'contact.city:id,city',
                'user.contact:id,name,surname',
                'documentsProducts.documentsProductsTaxes',
                'contact.customer:id,contact_id,credit_quota',
                'documentsProducts.product:id,code,description',
                'contact:id,name,surname,city_id,identification,address,main_telephone',
                'documentsProducts:id,document_id,product_id,lot,serial_number,description,observation,quantity,unit_value_before_taxes,total_value,total_value_brut',
                'documentThread' => function ($documentThread) {
                    $documentThread->select('id', 'consecutive', 'document_date', 'thread', 'total_value', 'total_value_brut', 'vouchertype_id', 'operationType_id')
                        ->with([
                            'documentsInformation:id,document_id,protractor,guide_number',
                            'documentsInformation.contactProtractor:id,name,surname,web_page',
                            'vouchertype:id,code_voucher_type,name_voucher_type',
                            'documentsProducts' => function ($documentsProducts) {
                                $documentsProducts->select('id', 'document_id', 'product_id', 'lot', 'serial_number', 'description', 'observation', 'quantity', 'unit_value_before_taxes', 'total_value', 'total_value_brut')
                                    ->with([
                                        'product:id,code,description',
                                        'documentsProductsTaxes',
                                    ])
                                    ->where('quantity', '>', 0);
                            },
                        ])
                        ->where('operationType_id', '!=', 8)
                        ->where('consecutive', '!=', 0);
                },
            ])
            ->whereHas('contact', function ($contact) use ($request) {
                $contact->whereRaw('concat(identification, \' \', name, \' \', surname) ilike ?', ['%' . $request->filter . '%']);
            })
            ->when(isset($request->filter_status) && $request->filter_status != '', function ($subquery) use ($request) {
                if ($request->filter_status == 'CERRADO') {
                    $subquery->where('closed', true);
                } else {
                    $subquery->whereHas('stateBackorder', function ($stateBackorder) use ($request) {
                        $stateBackorder->where('name_parameter', $request->filter_status);
                    })->where('closed', false);
                }
            })
            ->when(isset($request->filter_date) && is_array($request->filter_date), function ($query) use ($request) {
                $query->whereBetween('document_date', [substr($request->filter_date[0], 0, 10) . ' 00:00:00', substr($request->filter_date[1], 0, 10) . ' 23:59:59']);
            })
            ->when($seller_id != null, function ($query) use ($seller_id) {
                $query->where('cw.seller_id', $seller_id);
            })
            ->where('operationType_id', 8)
            ->orderBy('document_date', 'DESC')
            ->get();

        return $documents;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getDocumentsToAuthorize($user_id)
    {
        return Document::select('id', 'consecutive', 'document_date', 'model_id', 'user_id', 'branchoffice_id', 'contact_id')
            ->with([
                'contact:id,name,surname',
                'template:id,description',
                'user:id,name,surname',
                'branchoffice:id,name',
            ])
            ->where([
                'pending_authorization' => true,
                'closed' => false,
            ])
            ->whereHas('template', function ($template) use ($user_id) {
                $template->whereHas('authorizedUsers', function ($authorizedUsers) use ($user_id) {
                    $authorizedUsers->where('user_id', $user_id);
                });
            })->get();
    }

    public function setErp_idNull($id)
    {
        $document = Document::find($id);
        $document->erp_id = null;
        $document->save();
        return $document->id;
    }

    public function searchDocumentsVisits($visits)
    {
        foreach ($visits as $key => $visit) {
            $documents = Document::select('id', 'seller_id', 'contact_id', 'document_date', 'vouchertype_id', 'total_value')
                ->where('seller_id', $visit->seller_id)
                ->where('contact_id', $visit->contact_id)
                ->whereDate('document_date', substr($visit->checkin_date, 0, 10))
                ->wherehas('vouchertype', function ($query) {
                    $query->where('code_voucher_type', '010');
                })
                ->with(['vouchertype' => function ($query) {
                    $query->where('code_voucher_type', '010');
                }])
                ->get();
            $total_documents = 0;
            foreach ($documents as $key => $document) {
                $total_documents += $document->total_value;
            }
            $visit->documents = $documents;
            $visit->total_documents = '$ ' . number_format($total_documents, 0, '', '.');
            $visit->total_documents_wf = $total_documents;
            $visit->total_quantity_documents = count($documents);
        }

        return $visits;
    }

    public function retReturnedDocument($id, $state)
    {
        $document = Document::find($id);
        $document->returned_document = $state;
        $document->save();
        return $document;
    }

    public function getReturnsFromBill($id)
    {
        return $documents = Document::where('thread', $id)
            ->where('in_progress', false)
            ->whereIn("operationType_id", [1, 2])
            ->get();
    }

    public function searchDocumentForClearErp($request)
    {
        $fecha_icicio = $request->date[0] . ' 00:00:00';
        $fecha_fin = $request->date[1] . ' 23:59:59';
        $type_operations = $request->type_operations;

        $document = Document::select('id', 'document_date', 'operationType_id')
            ->whereBetWeen('document_date', [$fecha_icicio, $fecha_fin])
            ->where('in_progress', false);

        if ($type_operations && is_array($type_operations)) {
            $document->whereIn('operationType_id', $type_operations);
        }
        return $document->get();
    }

    public function detailDocument($id, $request)
    {
        $document = Document::where('id', $id)->
            with(['vouchertype', 'contact', 'documentsProducts' => function ($documentsProduct) {
            $documentsProduct->with(['product', 'documentsProductsTaxes']);
        }, 'receivable', 'transactions' => function ($query) {
            $query->orderBy('id', 'asc')->with([
                'document' => function ($query) {
                    $query->with([
                        'vouchertype',
                    ]);
                },
            ]);
        },
            'contributionInvoicesApp' => function ($query) {
                $query->with(['cashReceiptsApp' => function ($cashr) {
                    $cashr->with('consignement');
                }])->whereHas('cashReceiptsApp');
            },
        ])->get();

        //  hallar el iva de mi documento

        $document[0]->documentsProducts->map(function ($product, $key) use ($document) {
            $product->valuedpp = 0;
            $product->documentsProductsTaxes->map(function ($tax, $key) use ($document) {
                $document[0]->iva += $tax->value;
            });
        });

        //      Opero mi recibo de caja
        $balance = 0;
        foreach ($document[0]->transactions as $key => $transaction) {

            if (trim($transaction->transaction_type) == 'D') {
                $balance += $transaction->transaction_value;
            } elseif (trim($transaction->transaction_type) == 'C' && $transaction->payment_applied_erp == 1) {
                $balance -= $transaction->transaction_value;
            }

            $transaction->balance = $balance;
        }

        //  proceso   recibos de caja
        $arrayContributions = collect();
        $consignementDate = null;
        $document[0]->contributionInvoicesApp->map(function ($contributionI, $key) use ($consignementDate, $arrayContributions) {
            $dateHour = explode(' ', $contributionI->created_at);
            $contributionI->date = $dateHour[0];
            $contributionI->hour = $dateHour[1];

            if ($contributionI->cashReceiptsApp->consignement) {
                $consignementDate = explode(' ', $contributionI->cashReceiptsApp->consignement->date_time);
            }
            $contributionI->consignementDate = $consignementDate ? $consignementDate[0] : '';
            if ($contributionI->cashReceiptsApp->payment_applied_erp == 0) {
                $arrayContributions->push($contributionI);
            }
        });

        $document[0]->arrayContributions = $arrayContributions;

        ///   hallar descuento pronto pago de mi factura

        foreach ($document as $key => $doc) {
            $dR = new DocumentRepository(new Container);
            $valueDPPAndDays = $dR->applyDctPPToInvoices($document, $doc);
            $doc->dctpp = number_format($valueDPPAndDays[0]);
        }

        return $document;
    }

    public function auditsDocument($id)
    {
        return DB::select('select * from public.fn_getauditdocument(?);', [$id]);

    }

    public function auditsDocumentProducts($id)
    {
        $s = DB::select('select * from public.fn_getauditdocumentproduct(?);', [$id]);
        \Log::info(json_encode(count($s)));
        return $s;

    }

    public function auditsDocumentProductsTax($id)
    {
        return DB::select('select * from public.fn_getauditdocumentproducttax(?);', [$id]);

    }

    /**
     * Obtiene los documentos que han requerido o requieren autorización
     * @param $dates
     * @return mixed
     * @author Jhon García
     */
    public function getAuthorizedDocuments($dates, $user)
    {
        return Document::select('documents.id', 'document_date', 'consecutive', 'date_solicit_authorization', 'user_id_authorization', 'user_id_solicit_authorization', 'model_id', 'documents.contact_id', 'documents.warehouse_id', 'closed', 'pending_authorization', 'documents.updated_at')
            ->join('users AS u', 'u.id', '=', 'documents.user_id_solicit_authorization')
            ->with([
                'template:id,code,description',
                'userSolicitAuthorization:id,name,surname',
                'contact:id,identification,name,surname',
                'warehouse:id,code,description',
                'userAuthorization:id,name,surname',
            ])
            ->whereBetween('document_date', $dates)
            ->whereNotNull('user_id_solicit_authorization')
//            ->where('user_id_solicit_authorization', $user->id)
            ->orderBy('u.name', 'asc')
            ->orderBy('documents.date_solicit_authorization', 'asc')
            ->get();
    }

    public function getFinishedDocumentsPendingToSend()
    {
        $documents = Document::select('id', 'vouchertype_id', 'operationType_id', 'document_date', 'consecutive', 'contact_id', 'warehouse_id', 'thread')
            ->with([
                'contact:id,identification,name,surname',
                'warehouse:id,code,description',
                'vouchertype:id,code_voucher_type,name_voucher_type',
                'document:id',
                'document.documentsInformation:id,document_id,vouchers_type_electronic_bill',
            ])
            ->whereIn('operationType_id', [1, 5])
            ->where('in_progress', false)
            ->where('document_date', '>=', '2020-11-03')
            ->whereHas('documentsInformation', function ($documentsInformation) {
                $documentsInformation->where('vouchers_type_electronic_bill', false);
            })
            ->whereHas('vouchertype', function ($vouchertype) {
                $vouchertype->where('electronic_bill', true);
            })
            ->orderBy('document_date', 'desc')
            ->get();

        $documents = $documents->filter(function ($document) {
            if ($document->operationType_id == 1) {
                if (!is_null($document->document) && !is_null($document->document->documentsInformation)) {
                    return $document->document->documentsInformation->vouchers_type_electronic_bill;
                }
                return false;
            }
            return true;
        });

        return $documents;
    }

    /**
     * Este metodo se encarga de
     * calcular el valor unitario por decimales.
     *
     * @author Kevin Galindo
     */
    public function electronicFacRounding($document, $model, $document_products_edit_iva = null)
    {
        if ($model->pay_taxes) {
            $total_value_brut_round = 0;
            $total_value_iva_round = 0;
            $tax_iva_percentage = 0;

            foreach ($document->documentsProducts as $key => $dp) {

                if ($document_products_edit_iva) {
                    $key_edit_iva = array_search(
                        $dp->id,
                        array_column($document_products_edit_iva, 'id')
                    );

                    if ($key_edit_iva !== false) {
                        $taxIva = $dp->documentProductTaxes->where('tax_id', 1)->first();
                        $document->documentsProducts[$key]['total_value_iva'] = $dp->quantity > 0 ? ($taxIva ? $taxIva->value : 0) : 0;
                        continue;
                    }
                }

                // Beneficio
                if ($dp->is_benefit) {
                    $dp_total_value_brut_round = $dp->total_value_brut;

                    $taxIva = $dp->documentProductTaxes->where('tax_id', 1)->first();
                    $dp_total_value_iva_round = $taxIva ? $taxIva->value : 0;
                }
                // Validar si el producto maneja iva
                else if ($dp->product->line->taxes->where('code', "10")->first()) {
                    if ($taxIva = $dp->documentProductTaxes->where('tax_id', 1)->first()) {
                        $tax_iva_percentage = (int) $taxIva->tax_percentage;
                    }

                    if ($tax_iva_percentage != 0) {
                        // pasar a decimales
                        $tax_iva_percentage = $tax_iva_percentage / 100;

                        if ($dp->total_value > 0) {
                            // $dp_total_value_brut_round = round( ($dp->total_value/ (1 + $tax_iva_percentage))+0.005 ,2);
                            $dp_total_value_brut_round = round(($dp->total_value / 1.19), 2);
                            $dp_total_value_iva_round = round($dp->total_value - $dp_total_value_brut_round, 2);
                        } else {
                            $dp_total_value_brut_round = 0;
                            $dp_total_value_iva_round = 0;
                        }

                    } else {
                        $dp_total_value_brut_round = $dp->total_value_brut;
                        $dp_total_value_iva_round = 0;
                    }

                    foreach ($dp->documentProductTaxes as $keyT => $dpt) {
                        if ($dpt->tax_id == 1) {
                            $dp->documentProductTaxes[$keyT]['base_value'] = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_brut_round;
                            $dp->documentProductTaxes[$keyT]['value'] = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_iva_round;

                            $dpt->base_value = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_brut_round;
                            $dpt->value = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_iva_round;
                            $dpt->save();
                            $dpt->load('tax');
                        }
                    }

                } else {
                    $dp_total_value_brut_round = $dp->total_value_brut;
                    $dp_total_value_iva_round = 0;
                }

                $document->documentsProducts[$key]['total_value_brut'] = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_brut_round;
                $document->documentsProducts[$key]['total_value_iva'] = $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_iva_round;

                $total_value_brut_round += $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_brut_round;
                $total_value_iva_round += $dp->quantity == 0 && $model->credit_note == false ? 0 : $dp_total_value_iva_round;

                DocumentProduct::find($dp->id)->update([
                    'total_value_brut' => $dp->quantity == 0 && !$model->credit_note ? 0 : $dp_total_value_brut_round
                ]);

            }

            $document['total_value_brut'] = $total_value_brut_round;
            $document['total_value_iva'] = $total_value_iva_round;

            Document::find($document->id)->update([
                'total_value_brut' => $total_value_brut_round,
            ]);

        } else {
            foreach ($document->documentsProducts as $keydp => $documentProduct) {

                // Sumamos los inpuestos
                foreach ($documentProduct->documentProductTaxes as $key => $documentProductTax) {
                    // Iva
                    if ($documentProductTax->tax->code === '10') {
                        $document->documentsProducts[$keydp]['total_value_iva'] = $documentProductTax->value;
                    }
                }
            }
        }

        return $document;
    }

    public function getItemizedCashReceipts(array $data)
    {
        $response = [];
        try {
            $cash = CashReceiptsApp::select('id', 'consecutive', 'creation_date as fecha', 'warehouse_id')
                ->when(!empty($data['rc']), function ($query) use ($data) {
                    $query->where('id', (int) $data['rc']);
                })
                ->when(empty($data['rc']), function ($query) use ($data) {
                    $query->whereNull('consignment_id');
                })
                ->where('contact_id', $data['iduser'])
                ->with(
                    [
                        'contributionInvoicesApp.document.receivable',
                    ]
                )->with([
                'paymentMethodsApp',
                'warehouse.contactt',
                'warehouse.seller.users',
            ])->get();

            if (count($cash) > 0) {

                foreach ($cash as $key => $rc) {
                    $cashArray = $rc->toArray();
                    $cashArray['consecutive'] = 'RC-' . $cashArray['consecutive'] . ' (Duplicado)';
                    $cashArray['totalPagado'] = 0;
                    $cashArray['cliente'] = $cashArray['warehouse']['contactt']['name'] . ' ' . $cashArray['warehouse']['contactt']['surname'];
                    $cashArray['iden'] = $cashArray['warehouse']['contactt']['identification'];
                    $cashArray['vendedor'] = $cashArray['warehouse']['seller']['name'] . ' ' . $cashArray['warehouse']['seller']['surname'];

                    //EMAILS
                    $cashArray['vendedorEmail'] = $cashArray['warehouse']['seller']['email'];
                    $cashArray['emailCashReceiptsCopy'] = count($cashArray['warehouse']['seller']['users']) > 0 ? $cashArray['warehouse']['seller']['users'][0]['email_cash_receipts_app'] : '';
                    $cashArray['ClienteEmail'] = $cashArray['warehouse']['contactt']['email'];

                    //facturas
                    $cashArray['facturas'] = [];
                    $cashArray['formasPago'] = [];
                    $cashArray['descuentoProntoPago'] = [];
                    foreach ($cashArray['contribution_invoices_app'] as $key) {
                        $fact = new stdClass();
                        $fact->id = $key['document_id'];
                        $fact->fecha = $key['created_at'];
                        $fact->numeroFactura = $key['document_id'] ? 'F-' . $key['document']['consecutive'] : 'ANTIC-1';
                        $fact->saldo = $key['invoice_document'] != null ? $key['invoice_document'] : 0;
                        $fact->valorConsignar = $key['total_paid'];
                        $cashArray['totalPagado'] += $fact->valorConsignar;

                        //retneciones
                        $fact->rete_fte = $key['rete_fte'];
                        $fact->rete_iva = $key['rete_iva'];
                        $fact->rete_ica = $key['rete_ica'];
                        $fact->dctpp = $key['value_discount_prompt_payment'];

                        if ($key['document']) {
                            foreach ($key['document']['receivable'] as $value) {
                                $fact->saldo = round($value['invoice_balance']);
                            }
                        } else {
                            $fact->saldo = 10000000;
                        }

                        if ($key['value_discount_prompt_payment'] != null && $key['invoice_days'] != null) {
                            $descuentoAplicado = new stdClass();
                            $descuentoAplicado->numeroFactura = $fact->numeroFactura;
                            $descuentoAplicado->diasFactura = $key['invoice_days'];
                            $descuentoAplicado->descuentoPP = $key['value_discount_prompt_payment'];

                            array_push($cashArray['descuentoProntoPago'], $descuentoAplicado);
                        }
                        array_push($cashArray['facturas'], $fact);
                    }

                    //metodos de pago
                    $formaPagoE = null;
                    new stdClass();
                    $band = 0;
                    foreach ($cashArray['payment_methods_app'] as $key) {

                        if ($key['parameter_id'] == 849) {
                            if ($band == 0) {
                                $formaPagoE = new stdClass();
                                $formaPagoE->id = $key['id'];
                                $formaPagoE->tipo = 'Efectivo';
                                $formaPagoE->valor = 0;
                                $formaPagoE->entidad = $key['bank'];
                                $formaPagoE->numRef = $key['check_number'];
                            }

                            $formaPagoE->valor += $key['total_paid'];
                            if (($band + 1) == count($cashArray['payment_methods_app'])) {
                            }

                            $band++;
                        }
                        if ($key['parameter_id'] == 850) {
                            $formaPago = new stdClass();
                            $formaPago->id = $key['id'];
                            $formaPago->tipo = 'Cheque';
                            $formaPago->valor = $key['total_paid'];
                            $formaPago->entidad = $key['bank'];
                            $formaPago->numRef = $key['check_number'];
                            array_push($cashArray['formasPago'], $formaPago);
                        }

                        if ($key['parameter_id'] == 851) {
                            $formaPago = new stdClass();
                            $formaPago->id = $key['id'];
                            $formaPago->tipo = 'Transferencia';
                            $formaPago->valor = $key['total_paid'];

                            $formaPago->entidad = $key['bank'];
                            $formaPago->numRef = $key['check_number'];
                            array_push($cashArray['formasPago'], $formaPago);
                        }
                    }
                    if (!$formaPagoE == null) {
                        array_push($cashArray['formasPago'], $formaPagoE);
                    }
                    unset($cashArray['payment_methods_app']);
                    unset($cashArray['warehouse']);
                    unset($cashArray['contribution_invoices_app']);
                    array_push($response, $cashArray);

                }

                return $response;

            }
            return $cash;
        } catch (\Exception $th) {
            \Log::info($th);
            throw new \Exception('ERROR in the line ' . $th->getLine() . ' of ' . $th->getFile() . ' : ' . $th->getMessage());
        }
    }
    /**
     * fun cion lista documento contable
     * @author Santiago Torres
     */
    public function getAccountDocument($accounting_document_id)
    {
        return Document::where('id', $accounting_document_id)
            ->with([
                'user:id,name,surname',
                'vouchertype:id,code_voucher_type,name_voucher_type',
                'template:id,code,description',
                'branchoffice',
                'accounting_transactions' => function ($query) {
                    $query->with([
                        'account',
                        'projects',
                        'document',
                        'payment',
                    ])
                    ->orderBy('order', 'ASC');
                    $query->where('operation_value', '!=', 0);
                },
                'contact' => function ($query) {
                    $query->with('subscriber');
                },
            ])
            ->first();
    }

    public function getNotPreviousDocuments()
    {
        return Document:: select()
        ->whereHas('documentsInformation', function ($documentsInformation){
            $documentsInformation->where('document_previous_system', '<>', 'true');
        })
        ->with([
            'documentsProducts.documentsProductsTaxes',
            'documentsInformation',
            'accounting_transactions',
            'files'
        ])
        ->get();
    }

    public function validateAffectsDocumentId($affects_document_id)
    {
        return Document::where('affects_document_id', $affects_document_id)->exists();
    }

    public function searchDocument_($request)
    {
        $searc_doc = Document::where('in_progress', false)
            ->where('consecutive', $request->consecutive)
            ->where('branchoffice_id', $request->branchoffice_id)
            ->with(['seller', 'accounting_transactions'])
            ->when($request->voucherType_id, function ($query) use ($request){
                $query->where('vouchertype_id', $request->voucherType_id);
            })
            ->when($request->prefix, function ($query) use ($request){
                $query->where('prefix', $request->prefix);
            })
            ->with(['seller'])
            ->first();

        return (object) [
            'code' => '200',
            'completed' => false,
            'data' => (object) [
                'data' => $searc_doc,
                'message' => '',
            ],
        ];
    }

    public function getEditDocument($document_id)
    {
        $old_document = $this->getAccountingDocumentEdit($document_id);
        $old_document = $old_document->toArray();

        $old_document['in_progress'] = true;
        $old_document['ref_update_doc'] = $old_document['id'];
        $new_document = Document::create(Arr::except($old_document, ['id', 'erp_id']));
        // dd($new_document->ToArray());
        $old_document['new_document_id'] = $new_document->id;

        return $old_document;
    }

    public function getAccountingDocumentEdit($document_id)
    {
        return Document::with([
            'documentsProducts.product:id,code,description',
            'documentsProducts.line:id,line_description',
            'documentsProducts.documentsProductsTaxes',
            'documentsInformation',
            'accounting_transactions' => function ($query) {
                $query->with([
                    'affectsDocument',
                    'document',
                    'contact:id,identification,name,surname',
                    'account'
                ]);
                $query->orderBy('id');
            },
            'files',
            'seller',
            'template'

            // //oyt
            // 'paymentMethods',
            // 'documentVehicle',
            // 'documentVehicleInspection',

        ])
            ->find($document_id);
    }

    /**
     * Trae el resumen del cuadre de caja por fecha
     * @author Kevin Galindo
     */
    public function getDetailDocumentsPaymentMethods($dateFrom, $dateTo, $branchoffice_id)
    {
        $result = DB::select('select ppm.code_parameter, ppm.name_parameter, sum(dpm.total_value) as value
        from documents d
        inner join documents_payment_methods dpm on dpm.document_id = d.id
        left join parameters ppm on ppm.id = dpm.payment_method_id
        where d.in_progress = false
        and (d.canceled = false or d.canceled is null)
        and(d.closed = false or d.closed is null)
        and d.document_date between ? and ?
        and d.branchoffice_id = ?
        group by ppm.name_parameter, ppm.code_parameter
        order by ppm.code_parameter', [
            $dateFrom,
            $dateTo,
            $branchoffice_id,
        ]);

        return $result;
    }

    /**
     * trae la informacion para el cuadre de caja
     * @author Kevin Galindo
     */
    public function getSquareCashRegister($dateFrom, $dateTo, $branchoffice_id)
    {
        $result = DB::select("select
                    d.id as document_id,
                    d.consecutive as document_consecutive,
                    c.identification as contact_identification,
                    c.\"name\" || ' ' || c.surname  as contact_name,
                    th.consecutive as thread_consecutive,
                    d.total_value as document_total,
                    d.total_value_brut as document_total_brut,
                    d.total_value - d.total_value_brut as total_iva,
                    ppm.name_parameter as payment_method,
                    dpm.total_value as payment_method_value,
                    t.description as model_description,
                    t.code as model_code
            from documents d
            inner join documents_payment_methods dpm on dpm.document_id = d.id
            left join parameters ppm on ppm.id = dpm.payment_method_id
            left join contacts c on c.id = d.contact_id
            left join documents th  on th.id = d.thread
            left join templates t on t.id = d.model_id
            where d.in_progress = false
            and (d.canceled = false or d.canceled is null)
            and(d.closed = false or d.closed is null)
            and d.deleted_at is null
            and d.document_date between ? and ?
            and d.branchoffice_id = ?
            order by t.code
        ", [
            $dateFrom,
            $dateTo,
            $branchoffice_id,
        ]);

        return collect($result);
    }

    public function getDocumentsWms()
    {

        $documents = Document::
            distinct('documents.id')
            ->select('documents.id',
                'documents.consecutive',
                'documents.contact_id',
                'contacts.surname',
                'contacts.identification',
                'contacts.address as address_contact',
                'contacts.email as email_contact',
                'cities_contact_warehouse.city_code as city_code_contact',
                'cities_contact_warehouse.city as city_contact',
                'cities_contact_warehouse.department_id as department_id_sucursal_contact',
                'departments_contact_warehouse.department as department_contact',
                'contacts.main_telephone as telephone_contact',
                'documents.observation',
                'branchoffice_warehouses.warehouse_code',
                'branchoffice_warehouses.warehouse_description',
                'voucherstypes.code_voucher_type as type_operation',
                'voucherstypes.affectation as affectation',
                'branchoffices.id as id_sucursal',
                'branchoffices.name as name_sucursal',
                'branchoffices.address as address_sucursal',
                'branchoffices.telephone as telephone_sucursal',
                'branchoffices.email as email_sucursal',
                'templates.code as code_model','voucherstypes.prefix','templates.select_send_wms',
                'city_branchoffices.city_code as city_code_branchoffice',
                'city_branchoffices.city as city_branchoffice',
                'departament_branchoffices.id as department_id_branchoffice',
                'departament_branchoffices.department as department_branchoffice')
            ->selectRaw("to_char(documents.document_date,'YYYY-MM-DD') as document_date_wms")
            ->selectRaw('documents.prefix || documents.consecutive as complete_cod')
            ->selectRaw("(CASE WHEN contacts.surname = ''  THEN contacts.name WHEN contacts.surname is NULL THEN contacts.name ELSE contacts.name ||' '|| contacts.surname end) AS complete_name")
            ->join('contacts', 'contacts.id', '=', 'documents.contact_id')
            ->join('contacts_warehouses', 'contacts_warehouses.id', '=', 'documents.warehouse_id')
            ->join('branchoffice_warehouses', 'branchoffice_warehouses.id', '=', 'documents.branchoffice_warehouse_id')
            ->join('branchoffices', 'branchoffices.id', '=', 'branchoffice_warehouses.branchoffice_id')
            ->leftJoin('cities as city_branchoffices', 'city_branchoffices.id', '=', 'branchoffices.city_id')
            ->leftJoin('departments as departament_branchoffices', 'departament_branchoffices.id', '=', 'city_branchoffices.department_id')
            ->leftJoin('cities as cities_contact_warehouse', 'cities_contact_warehouse.id', '=', 'contacts_warehouses.city_id')
            ->leftJoin('departments as departments_contact_warehouse', 'departments_contact_warehouse.id', '=', 'cities_contact_warehouse.department_id')
            ->join('templates', 'templates.id', '=', 'documents.model_id')
            ->join('voucherstypes', 'voucherstypes.id', '=', 'documents.vouchertype_id')
        //->whereBetween('documents.document_date', ["current_date-interval '1 days'","current_date+interval '1 days'"])
            ->whereIn('templates.select_send_wms',[1,3])
            ->where('in_progress','=',false)
            ->where('documents.update_wms', '=', false);
            //->where('types_operation.affectation', '=', 1);

        $documents->whereRaw("documents.document_date between current_date-interval '150 days' AND current_date+interval '150 days'")->limit(100);
        //->where('types_operation.affectation', '=', 1);
        return $documents->orderBy('documents.id', 'DESC')->get();
    }

    public function getUpdateDocumentsWms($id_document, $action = 'A', $state)
    {

        $current = Document::where('id', $id_document)->first();

        // Actualiza Estado a True y Fecha Actual del Consumo de WMS para no ser tenido en cuenta posteriormente.
        if ($action == 'A') {
            $update = Document::where('id', $id_document)->update([
                'update_wms' => true,
                'date_update_wms' => now(),
                'error_wms' => $state,
            ]);
        } else {
            $update = Document::where('id', $id_document)->update([
                'update_wms' => true,
                'date_update_wms_ultime' => now(),
                'error_wms' => $state,
            ]);
        }
    }

    public function getUpdateDocumentsWmsError($id_document, $state)
    {

        $current = Document::where('id', $id_document)->first();

        // Actualiza Estado a True y Fecha Actual del Consumo de WMS para no ser tenido en cuenta posteriormente.

        $update = Document::where('id', $id_document)->update([
            'error_wms' => $state,
        ]);

    }

    public function getDocumentsProductsWms($id_document)
    {
        $documents = Document::
            distinct('documents_products.id as id_documents_products')
            ->select('documents_products.id',
                'products.code',
                'documents.consecutive',
                'documents.document_date',
                'documents.contact_id',
                'documents_products.quantity',
                'documents_products.unit_value_before_taxes',
                'branchoffice_warehouses.warehouse_code',
                'branchoffice_warehouses.warehouse_description',
                'types_operation.code as type_operation'
            )
            ->selectRaw('documents.prefix || documents.consecutive as complete_cod')
            ->join('documents_products', 'documents_products.document_id', '=', 'documents.id')
            ->join('branchoffice_warehouses', 'branchoffice_warehouses.id', '=', 'documents.branchoffice_warehouse_id')
            ->join('products', 'products.id', '=', 'documents_products.product_id')
            ->join('types_operation', 'types_operation.id', '=', 'documents.operationType_id')
            ->where('documents_products.document_id', '=', $id_document);
        return $documents->orderBy('documents_products.id', 'DESC')->get();
    }

    public function createDocumentsWms($request)
    {

        //dd($request->product_array);

        $type_voucher = VouchersType::where(["code_voucher_type" => 100])->first();
        $type_voucher_id = $type_voucher->id;
        $prefix_voucher = $type_voucher->prefix;

        if (isset($request->id_contact)) {

            $contact = Contact::where(['identification' => $request->id_contact])->first();
            $contact_id = $contact->id;
        }

        $branchOfficeWarehouse_source = $branch_office_warehouse = BranchOfficeWarehouse::where(['warehouse_code' => $request->branchoffice_warehouse_code_source])->select('id', 'branchoffice_id')->first();

        if (!isset($branchOfficeWarehouse_source->id)) {


            $documentsResult = array(
                "data" => $request->branchoffice_warehouse_code_source,
                "state" => "error",
                "msg" => "Not Existe Branch Office Warehouse source in the Starcommerce",
            );

        } else {
            $id_branchOfficeWarehouse_source = $branchOfficeWarehouse_source->id;
            $id_branchOffice_source = $branchOfficeWarehouse_source->branchoffice_id;
            $branchOfficeWarehouse_destination = $branch_office_warehouse = BranchOfficeWarehouse::where(['warehouse_code' => $request->branchoffice_warehouse_code_destination])->select('id', 'branchoffice_id')->first();
            if (!isset($branchOfficeWarehouse_destination->id)) {

                $documentsResult = array(
                    "data" => $request->branchoffice_warehouse_code_destination,
                    "state" => "error",
                    "msg" => "Not Existe Branch Office Warehouse destination in the Starcommerce",
                );
            } else {
                $id_branchOfficeWarehouse_destination = $branchOfficeWarehouse_destination->id;
                $id_branchOffice_destination = $branchOfficeWarehouse_destination->branchoffice_id;
                $model = Template::where(["voucherType_id" => $type_voucher_id])->get();
                $consecutive = VouchersType::where('code_voucher_type', 100)->lockForUpdate()->first();

                $consecutive->consecutive_number = $consecutive->consecutive_number + 1;

                $exists = true;

                // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.

                while ($exists) {

                    $aux = Document::where('consecutive', $consecutive->consecutive_number)
                        ->where('vouchertype_id', $consecutive->id)
                        ->where('in_progress', false)
                        ->exists();

                    if ($aux) {

                        ++$consecutive->consecutive_number;

                    } else {

                        $exists = false;

                    }

                }

                $consecutive->save();

                $dataExit = [
                    'vouchertype_id' => $type_voucher_id,
                    'consecutive' => $consecutive->consecutive_number,
                    'prefix' => $prefix_voucher,
                    'contact_id' => isset($contact_id) ? $contact_id : 1,
                    'document_state' => 1,
                    'branchoffice_id' => $id_branchOffice_source,
                    'branchoffice_warehouse_id' => $id_branchOfficeWarehouse_source,
                    'operationType_id' => $model[0]->operationType_id,
                    'incorporated_document' => false,
                    'observation' => $request->observation,
                    "issue_date" => Carbon::now(),
                    'warehouse_id' => isset($contact_id) ? $contact_id : 1,
                    'document_date' => date("Y-m-d H:i:s"),
                    'model_id' => $model[0]->id,
                    'user_id' => $request->id_user,
                    'in_progress' => false

                ];

                $documentExit = Document::create($dataExit);

                $dataEntry = [
                    'vouchertype_id' => $type_voucher_id,
                    'consecutive' => $consecutive->consecutive_number,
                    'prefix' => $prefix_voucher,
                    'contact_id' => $contact_id,
                    'document_state' => 1,
                    'branchoffice_id' => $id_branchOffice_destination,
                    'branchoffice_warehouse_id' => $id_branchOfficeWarehouse_destination,
                    'operationType_id' => $model[1]->operationType_id,
                    'incorporated_document' => false,
                    'observation' => $request->observation,
                    "issue_date" => Carbon::now(),
                    'warehouse_id' => $contact_id,
                    'document_date' => date("Y-m-d H:i:s"),
                    'model_id' => $model[1]->id,
                    'thread' => $documentExit->id,
                    'user_id' => $request->id_user,
                    'in_progress' => false

                ];

                $documentEntry = Document::create($dataEntry);

                $countProduct = count($request->product_array);

                $documentsResult = array();

                $f = 0;
                $c = 0;
                for ($x = 0; $x < $countProduct; $x++) {
                    //dd($request->product_array[$x]);
                    $product = Product::where(["code" => $request->product_array[$x]['cod_prod']])->first();
                    if(!isset($product->id)){

                            $error_products[$x] = $request->product_array[$x]['cod_prod'];

                    }else{

                        $inventoryExit = Inventory::where(["product_id" => $product->id, "branchoffice_warehouse_id" => $id_branchOfficeWarehouse_source])->first();
                        $inventoryEntry = Inventory::where(["product_id" => $product->id, "branchoffice_warehouse_id" => $id_branchOfficeWarehouse_destination])->first();

                        //dd($product,$inventoryExit,$inventoryEntry);

                        $product_id = $product->id;
                        $line_id = $product->line_id;
                        $subline_id = $product->subline_id;
                        $brand_id = $product->brand_id;
                        $product_name = $product->description;
                        $manage_inventory = $product->manage_inventory;
                        $lot_wms = json_encode($request->product_array[$x]['lot_array']);
                        $dataExit = [
                            'document_id' => $documentExit->id,
                            'product_id' => $product_id,
                            'line_id' => $line_id,
                            'subline_id' => $subline_id,
                            'brand_id' => $brand_id,
                            'quantity' => $request->product_array[$x]['quantity_total'],
                            'unit_value_before_taxes' => isset($inventoryExit) ? $inventoryExit->average_cost : 0,
                            'total_value_brut' => isset($inventoryExit) ? $inventoryExit->average_cost * $request->product_array[$x]['quantity_total'] : 0,
                            'manage_inventory' => $manage_inventory,
                            'branchoffice_warehouse_id' => $id_branchOfficeWarehouse_source,
                            'operationType_id' => $model[0]->operationType_id,
                            'quantity_wms' => $request->product_array[$x]['quantity_total'],
                            "lot_wms" => $lot_wms,

                        ];

                        $documentProdExit = DocumentProduct::create($dataExit);
                        $array_doc_prod_exit[$c] = array_merge(array("id_document"=>$documentProdExit->document_id),array("id_product"=>$documentProdExit->product_id),array("code_product"=>$product->code));
                        $c++;

                        $documentsResult = array(["id_documento_entry" => $documentEntry->id, "id_documento_exit" => $documentExit->id]);
                        $dataEntry = [
                            'document_id' => $documentEntry->id,
                            'product_id' => $product_id,
                            'line_id' => $line_id,
                            'subline_id' => $subline_id,
                            'brand_id' => $brand_id,
                            'quantity' => $request->product_array[$x]['quantity_total'],
                            'unit_value_before_taxes' => isset($inventoryEntry) ? $inventoryEntry->average_cost : 0,
                            'total_value_brut' => isset($inventoryEntry) ? $inventoryEntry->average_cost * $request->product_array[$x]['quantity_total'] : 0,
                            'manage_inventory' => $manage_inventory,
                            'branchoffice_warehouse_id' => $id_branchOfficeWarehouse_destination,
                            'operationType_id' => $model[1]->operationType_id,
                            'quantity_wms' => $request->product_array[$x]['quantity_total'],
                            "lot_wms" => $lot_wms,

                        ];
                        $documentProdEntry = DocumentProduct::create($dataEntry);
                        //$array_doc_prod_entry[$x] = $documentProdEntry->id;
                        $array_doc_prod_entry[$f] = array_merge(array("id_document"=>$documentProdEntry->document_id),array("id_product"=>$documentProdEntry->product_id),array("code_product"=>$product->code));
                        $f++;

                    }

                    //$documentProd = DocumentProduct::create($data);
                }

                $documentsResult = array(["state"=> "ok","msg"=>"Perfect transfer between warehouses","id_documento_entry" => $documentEntry->id, "id_documento_exit" => $documentExit->id], "products_exit" => $array_doc_prod_exit, "products_entry" => $array_doc_prod_entry, "products_errors"=>isset($error_products)?$error_products:"Sin Errores en Productos");
            }

        }

        return $documentsResult;
    }

    public function createDocumentsMovementsWMS($request)
    {

        //dd($request->product_array);

        if($request->type_document == 'ENTRY'){

            $type_voucher = VouchersType::where(["code_voucher_type" => 150])->first();
            $type_voucher_id = $type_voucher->id;
            $prefix_voucher = $type_voucher->prefix;

        }else{

            $type_voucher = VouchersType::where(["code_voucher_type" => 260])->first();
            $type_voucher_id = $type_voucher->id;
            $prefix_voucher = $type_voucher->prefix;

        }

        if (isset($request->id_contact)) {

            $contact = Contact::where(['identification' => $request->id_contact])->first();
            if (!isset($contact->id)) {
                $documentsResult = array(
                    "data" => $request->branchoffice_warehouse_code,
                    "state" => "error",
                    "msg" => "Not Exist Contact Id in the Starcommerce",
                );
            }else{
                $contact_id = $contact->id;
                $branchOfficeWarehouse = $branch_office_warehouse = BranchOfficeWarehouse::where(['warehouse_code' => $request->branchoffice_warehouse_code])->select('id', 'branchoffice_id')->first();

                if (!isset($branchOfficeWarehouse->id)) {


                    $documentsResult = array(
                        "data" => $request->branchoffice_warehouse_code,
                        "state" => "error",
                        "msg" => "Not Exist Branch Office Warehouse in the Starcommerce",
                    );

                } else {
                        $id_branchOfficeWarehouse = $branchOfficeWarehouse->id;
                        $id_branchOffice = $branchOfficeWarehouse->branchoffice_id;

                        $model = Template::where(["voucherType_id" => $type_voucher_id])->get();

                        if($request->type_document == 'ENTRY'){

                            $consecutive = VouchersType::where('code_voucher_type', 150)->lockForUpdate()->first();

                        }else{

                            $consecutive = VouchersType::where('code_voucher_type', 260)->lockForUpdate()->first();

                        }

                        $consecutive->consecutive_number = $consecutive->consecutive_number + 1;

                        $exists = true;

                        // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.

                        while ($exists) {

                            $aux = Document::where('consecutive', $consecutive->consecutive_number)
                                ->where('vouchertype_id', $consecutive->id)
                                ->where('in_progress', false)
                                ->exists();

                            if ($aux) {

                                ++$consecutive->consecutive_number;

                            } else {

                                $exists = false;

                            }

                        }

                        $consecutive->save();

                        $dataDocument = [
                            'vouchertype_id' => $type_voucher_id,
                            'consecutive' => $consecutive->consecutive_number,
                            'prefix' => $prefix_voucher,
                            'contact_id' => isset($contact_id) ? $contact_id : 1,
                            'document_state' => 1,
                            'branchoffice_id' => $id_branchOffice,
                            'branchoffice_warehouse_id' => $id_branchOfficeWarehouse,
                            'operationType_id' => $model[0]->operationType_id,
                            'incorporated_document' => false,
                            'observation' => $request->observation,
                            "issue_date" => Carbon::now(),
                            'warehouse_id' => isset($contact_id) ? $contact_id : 1,
                            'document_date' => date("Y-m-d H:i:s"),
                            'model_id' => $model[0]->id,
                            'user_id' => $request->id_user,
                            'in_progress' => false

                        ];

                        $dataDocument = Document::create($dataDocument);

                        $countProduct = count($request->product_array);

                        $documentsResult = array();
                        $g = 0;
                        for ($s = 0; $s < $countProduct; $s++) {
                            //dd($request->product_array[$s]);
                            $product = Product::where(["code" => $request->product_array[$s]['cod_prod']])->first();
                            if(!isset($product->id)){

                                    $error_products[$s] = $request->product_array[$s]['cod_prod'];

                            }else{

                                $inventoryDocument = Inventory::where(["product_id" => $product->id, "branchoffice_warehouse_id" => $id_branchOfficeWarehouse])->first();


                                $product_id = $product->id;
                                $line_id = $product->line_id;
                                $subline_id = $product->subline_id;
                                $brand_id = $product->brand_id;
                                $product_name = $product->description;
                                $manage_inventory = $product->manage_inventory;
                                $lot_wms = json_encode($request->product_array[$s]['lot_array']);
                                $dataProdDocument = [
                                    'document_id' => $dataDocument->id,
                                    'product_id' => $product_id,
                                    'line_id' => $line_id,
                                    'subline_id' => $subline_id,
                                    'brand_id' => $brand_id,
                                    'quantity' => $request->product_array[$s]['quantity_total'],
                                    'unit_value_before_taxes' => isset($inventoryDocument) ? $inventoryDocument->average_cost : 0,
                                    'total_value_brut' => isset($inventoryDocument) ? $inventoryDocument->average_cost * $request->product_array[$s]['quantity_total'] : 0,
                                    'manage_inventory' => $manage_inventory,
                                    'branchoffice_warehouse_id' => $id_branchOfficeWarehouse,
                                    'operationType_id' => $model[0]->operationType_id,
                                    'quantity_wms' => $request->product_array[$s]['quantity_total'],
                                    "lot_wms" => $lot_wms,

                                ];
                                $product_code = $product['code'];
                                //dd($product_code);
                                $documentProd = DocumentProduct::create($dataProdDocument);
                                $array_doc_prod[$g] = array_merge(array("id_product"=>$documentProd->product_id),array("code_product"=>$documentProd->product_id));
                                $g++;

                                $documentsResult = array(["id_documento" => $dataDocument->id]);

                            }

                            //$documentProd = DocumentProduct::create($data);
                        }

                        if(!empty($documentProd)){

                            $documentsResult = array("document"=>["state"=> "ok","msg"=>"Perfect Movement Document","id_documento" => $documentProd->id], "products_document" => isset($array_doc_prod)?$array_doc_prod:"Sin Errores en Productos", "products_errors"=>isset($error_products)?$error_products:"Sin Errores en Productos");
                        }else{
                            $documentsResult = array(
                                "data" => $request->product_array,
                                "state" => "error",
                                "msg" => "Not Exist Products in the Starcommerce",
                            );
                        }


                }
            }
        }


        return $documentsResult;
    }

    public function getDocumentBalanceCrossing($request)
    {
        dd("test");
    }
}
