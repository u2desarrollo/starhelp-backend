<?php

namespace App\Repositories;

use App\Entities\MenuSubscriber;
use App\Entities\Module;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MenuSubscriberRepository
 * @package App\Repositories
 * @version March 6, 2019, 2:01 pm UTC
 *
 * @method MenuSubscriber findWithoutFail($id, $columns = ['*'])
 * @method MenuSubscriber find($id, $columns = ['*'])
 * @method MenuSubscriber first($columns = ['*'])
 */
class MenuSubscriberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'module_id',
        'order',
        'menu_id',
        'menu_subscriber_id',
        'alternate_description',
        'subscriber_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MenuSubscriber::class;
    }


    public function findAllForSubscribersAndModule($subscriber_id)
    {

        $modules = Module::all();

        foreach ($modules as $module) {
            $menu_subscriber = MenuSubscriber::where('subscriber_id', $subscriber_id)
                ->where('module_id', $module->id)
                ->where('menu_subscriber_id', null)
                ->with('module')
                ->with('menu')
                ->with('secondLevel')
                ->orderBy('order', 'ASC')
                ->get();

            $module->first_level = $menu_subscriber;
        }


        return $modules;

    }

    public function updateAll($modules, $id)
    {

        try {
            DB::beginTransaction();

            MenuSubscriber::where('subscriber_id', $id)->delete();

            $order_first_level = 1;
            $order_second_level = 1;
            $order_third_level = 1;

            foreach ($modules as $module) {
                foreach ($module['first_level'] as $fl) {
                    $fl['order'] = $order_first_level;
                    $first_level = MenuSubscriber::create($fl);
                    $order_first_level++;
                    foreach ($fl['second_level'] as $sl) {
                        $sl['order'] = $order_second_level;
                        $sl['menu_subscriber_id'] = $first_level->id;
                        $second_level = MenuSubscriber::create($sl);
                        $order_second_level++;
                        foreach ($sl['third_level'] as $tl) {
                            $tl['order'] = $order_third_level;
                            $tl['menu_subscriber_id'] = $second_level->id;
                            $third_level = MenuSubscriber::create($tl);
                            $order_third_level++;
                        }
                    }

                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

        }

    }


}
