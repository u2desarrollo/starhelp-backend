<?php

namespace App\Repositories;

use App\Entities\FirmDate;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class FirmDateRepository
 * @package App\Repositories
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class FirmDateRepository extends BaseRepository
{

    public function model()
    {
        return FirmDate::class;
    }

    public function findAll($request)
    {
        $pagination = $request->pagination ? (boolean) $request->pagination : false;
        $filterText = $request->filterText;
        $perPage    = $request->perPage;

        $firmDates = FirmDate::with([
            'account',
        ])->orderBy('id');

        return $pagination 
        ? $firmDates->paginate($perPage)
        : $firmDates->get();        
    }

    public function findById($id)
    {
        return FirmDate::with([
            'account',
        ])
        ->find($id);     
    }

    public function update($data, $id)
    {
        FirmDate::find($id)->update($data);
        return FirmDate::find($id);
    }
}
