<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Entities\ProductAttachment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductAttachmentRepository
 *
 * @package App\Repositories
 * @version March 26, 2019, 5:56 pm UTC
 * @method ProductAttachment findWithoutFail($id, $columns = ['*'])
 * @method ProductAttachment find($id, $columns = ['*'])
 * @method ProductAttachment first($columns = ['*'])
 */
class ProductAttachmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'description',
        'url',
        'type',
        'main'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductAttachment::class;
    }

    /**
     * Cambiar la imagen principal de un producto
     *
     * @param ProductAttachment $product_attachment
     * @author Jhon García
     */
    public function setMainToImage(ProductAttachment $product_attachment)
    {
        try {
            DB::beginTransaction();
            Log::info($product_attachment);

            // Establecer todas las imágenes como no principales
            ProductAttachment::where('product_id', $product_attachment->product_id)
                ->where('type', 'i')
                ->update(['main' => false]);

            // Establecer la imagen como principal
            $product_attachment->main = true;

            $product_attachment->save();

            DB::commit();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }
    }

    /**
     * Obtener las imagenes de un producto
     *
     * @param Product $product Contiene la información del producto
     * @return mixed Contiene las imagenes del producto
     * @author Jhon García
     */
    public function getImagesFromProductId(Product $product)
    {
        // Obtener las imágenes de un producto
        return ProductAttachment::where('product_id', $product->id)
            ->where('type', 'i')
            ->orderBy('main', 'desc')
            ->get();
    }
}
