<?php

namespace App\Repositories;

use App\Entities\VouchersType;
use App\Entities\BranchOffice;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VouchersTypeRepository
 * @package App\Repositories
 * @version December 27, 2018, 10:15 pm UTC
 *
 * @method VouchersType findWithoutFail($id, $columns = ['*'])
 * @method VouchersType find($id, $columns = ['*'])
 * @method VouchersType first($columns = ['*'])
*/
class VouchersTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'branchoffice_id',
        'code_voucher_type',
        'name_voucher_type',
        'short_name',
        'prefix',
        'consecutive_number',
        'electronic_bill',
        'resolution',
        'affectation',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VouchersType::class;
    }

    /**
     * Método para obtener el listado de vouchersTypes de acuerdo a los parámetros recibidos
     * @param $request contiene los parámetros para filtrar
     * @return $vourchesType contiene una collección de de vouchersTypes
     */
    public function findAll($request)
    {
        // Variable
        $filter = $request->filter;
        $branchoffice = $request->branchoffice;
        $paginate = $request->paginate;

        
        if (substr($branchoffice, 0, 1) == '0') {
            $vourchesType = VouchersType::whereHas(
                'branchoffice' , function ($query) use ($branchoffice){
                    $query->where('code', $branchoffice);
                }
            );
        }else{
            $vourchesType = VouchersType::where('branchoffice_id', $branchoffice);
        }
        if ($paginate == 'true') {

            if (!empty($filter)) {
                $vourchesType->where(function ($query) use ($filter) {
                    $query->where('code_voucher_type', 'ilike', "%$filter%");
                    $query->orWhere('name_voucher_type', 'ilike', "%$filter%");
                });
            }
            $vourchesType = $vourchesType->orderBy($request->sortBy, $request->sort)
                                        ->paginate($request->perPage);
        }else{
            $vourchesType->orderBy('code_voucher_type','ASC');
            $vourchesType = $vourchesType->get();
        }

        return $vourchesType;
    }
    public function findForId($id){
        $vourchesType = VouchersType::where('id',$id)->first();
        return $vourchesType;
    }
    public function getVouchersType(){
        $vourchesType=VouchersType::all('id','name_voucher_type');
        return $vourchesType;
    }

    /**
     * @author Kevin Galindo
     */
    public function createVouchersType($input)
    {
        // creamos un nuevo tipo de comprobante
        $vouchersType = VouchersType::create($input);

        // 
        $branchOffices = BranchOffice::select('id')->where('id', '!=' , $input['branchoffice_id'])->get();

        foreach ($branchOffices as $key => $branchOffice) {
            $input['branchoffice_id'] = $branchOffice->id;
            VouchersType::create($input);
        }

        return $vouchersType;
    }

    public function getConsecutive($vouchersType_id)
    {
        return VouchersType :: find($vouchersType_id);
    }

    public function getVoucherType($request)
    {
        return VouchersType::where ('code_voucher_type', $request->code_voucher_type)->where('branchoffice_id', $request->branchoffice)->first();
    }

    public function getConsecutiveByCode($code, $branchoffice_id)
    {
        return VouchersType::where ('code_voucher_type', $code)->where('branchoffice_id', $branchoffice_id)->first();
    }

    public function updateConsecutive($id, $consecutive)
    {
        return VouchersType::where ('id', $id)->update(['consecutive_number'=> $consecutive]);
    }
}
