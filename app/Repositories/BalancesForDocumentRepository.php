<?php

namespace App\Repositories;

use App\Entities\Parameter;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\BalancesForDocument;

/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class BalancesForDocumentRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BalancesForDocument::class;
    }

    /**
     * Método para filtrar registros de acuerdo 
     * a los parametros recibidos
     * 
     * @author Kevin Galindo
     */
    public function findAll($request) {
    	return [];
    }

    /**
     * Método trae los saldos a favor del cliente.
     * 
     * @author Kevin Galindo
     */
    public function fetchFavorBalanceContact($request) {
        $balancesForDocuments = BalancesForDocument::where('year_month', $request->monthDate)
        ->where('account_id', $request->account_id)
        ->where('contact_id', $request->contact_id)
        ->where('final_balance', '<', 0)
        ->with([
            'document' => function ($query) {
                $query->with([
                    'branchofficeWarehouse',
                    'branchoffice',
                    'vouchertype'
                ]);
            },
            'account'
        ])
        ->get();

    	return $balancesForDocuments;
    }

    /**
     * Método trae los saldos pendientes del cliente.
     * 
     * @author Kevin Galindo
     */
    public function fetchPendingBalanceContact($request) {
        $balancesForDocuments = BalancesForDocument::where('year_month', $request->monthDate)
        ->where('account_id', $request->account_id)
        ->where('contact_id', $request->contact_id)
        ->where('final_balance', '>', 0)
        ->with([
            'document' => function ($query) {
                $query->with([
                    'branchofficeWarehouse',
                    'branchoffice',
                    'vouchertype'
                ]);
            },
            'account'
        ])
        ->get();

    	return $balancesForDocuments;
    }
}
