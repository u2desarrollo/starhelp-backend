<?php

namespace App\Repositories;

use App\Entities\ServiceRequest;
use App\Entities\ServiceRequestState;
use App\Entities\ServiceRequestStatusHistory;
use App\Entities\ServiceRequestType;

use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use Carbon\Carbon;


class ServiceRequestTypeRepository extends BaseRepository
{

    public function model()
    {
        return ServiceRequestType::class;
    }

    /**
     * 
     * @author santiago torres | kevin galindo
     */
    public function search()
    {
        return ServiceRequestType::all();
    }

    public function getStateByServiceRequestType($request)
    {
        $serviceRequestState = ServiceRequestState::where('service_requests_types_id', $request->service_requests_type_id)
                                                  ->get();

        return $serviceRequestState;
    }
}
