<?php

namespace App\Repositories;

use App\Entities\Configuration;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ConfigurationRepository
 * @package App\Repositories
 * @version December 18, 2018, 7:55 pm UTC
 *
 * @method Configuration findWithoutFail($id, $columns = ['*'])
 * @method Configuration find($id, $columns = ['*'])
 * @method Configuration first($columns = ['*'])
*/
class ConfigurationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subscriber_id',
        'product_digits',
        'seller_management',
        'brand_management',
        'line_management',
        'category_management',
        'line_description',
        'subline_description',
        'category_description',
        'subcategory_description',
        'type_description',
        'recalculate_sales_price',
        'percentage_minimum_variation',
        'rounding_factor',
        'type_voucher_inventory',
        'account_entry',
        'account_exit',
        'consecutive_product_code',
        'consecutive_transfer',
        'consecutive_sales_orders',
        'consecutive_purchase_orders',
        'folios_electronic_invoice',
        'last_date_close',
        'firm_date',
        'base_retention_source_products',
        'percentage_retention_source_products',
        'base_retention_source_services',
        'percentage_retention_source_services',
        'percentage_reteiva',
        'code_activity_ica',
        'code_ciiu',
        'percentage_reteica',
        'bill_to_pay',
        'receivable',
        'account_heritage_utility',
        'account_heritage_lost',
        'account_pyg_result_exercise',
        'monthly_utility_transfer_or_lost',
        'annual_utility_transfer_or_lost',
        'type_voucher_result_exercise',
        'first_logo',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Configuration::class;
    }
}
