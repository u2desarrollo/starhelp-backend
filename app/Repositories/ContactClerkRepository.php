<?php

namespace App\Repositories;

use App\Entities\ContactClerk;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactClerkRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:28 pm UTC
 *
 * @method ContactClerk findWithoutFail($id, $columns = ['*'])
 * @method ContactClerk find($id, $columns = ['*'])
 * @method ContactClerk first($columns = ['*'])
*/
class ContactClerkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'name',
        'surname',
        'address',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactClerk::class;
    }
}
