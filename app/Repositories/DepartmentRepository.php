<?php

namespace App\Repositories;

use App\Entities\Department;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DepartmentRepository
 * @package App\Repositories
 * @version December 27, 2018, 9:49 pm UTC
 *
 * @method Department findWithoutFail($id, $columns = ['*'])
 * @method Department find($id, $columns = ['*'])
 * @method Department first($columns = ['*'])
*/
class DepartmentRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'name'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Department::class;
	}

	public function getDepartment($request)
	{
		$filter = $request->filter;

		$department = Department::select('id', 'department');

		if ($filter) {
			$department->where('department', 'ilike', '%'.$filter.'%');
		}

		return $department->orderBy('department')->get();
		
	}
}