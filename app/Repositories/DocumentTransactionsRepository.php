<?php

namespace App\Repositories;

use App\Entities\BalancesForAccount;
use App\Entities\Document;
use App\Entities\DocumentTransactions;
use App\Entities\ParamTable;
use App\Entities\Parameter;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Auth;

class DocumentTransactionsRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocumentTransactions::class;
    }

    public function findAll($request)
    {
    }

    public function store($request, $document)
    {
        $data = $this->setDataForUpdateOrCreate($request, $document);
        $document_transaction = DocumentTransactions::create($data);
        return DocumentTransactions::with([
            'document' => function ($query) {
                $query->select('id', 'in_progress');
            },
        ])->find($document_transaction->id);
    }

    public function update_($id, $request, $document)
    {
        $document_transaction = DocumentTransactions::where('id', $id)
            ->update($this->setDataForUpdateOrCreate($request, $document));
        return DocumentTransactions::with([
            'document' => function ($query) {
                $query->select('id', 'in_progress');
            },
        ])->find($id);
    }

    public function setDataForUpdateOrCreate($request, $document)
    {
        $consecutive = explode('-', $request->document_transaction['no_doc']);
        return [
            // 'year_month' => substr(str_replace('-', '', $document->document_date), 0,6 ),
            'year_month' => substr(str_replace('-', '', Carbon::now()), 0, 6),
            'account_id' => $request->document_transaction['account_id'],
            'order' => $request->document_transaction['order'],
            'observations' => $request->document_transaction['observation'],
            'contact_id' => $request->document_transaction['contact_id'],
            // 'document_number' => isset($request->document_transaction['documet_id']) ? $request->document_transaction['documet_id'] : null,
            'operation_value' => str_replace(['$', '.'], '', $request->document_transaction['value']),
            'base_value' => $request->document_transaction['base_value'] ? str_replace(['$', '.'], '', $request->document_transaction['base_value']) : 0,
            'transactions_type' => $request->document_transaction['debit_credit'],
            'document_id' => $document->id,
            'vouchertype_id' => $document->vouchertype_id,
            'document_date' => $document->document_date,
            'consecutive' => $document->consecutive,
            'projects_id' => isset($request->document_transaction['projects_id']) ? $request->document_transaction['projects_id'] : null,
            'payment_method' => $request->document_transaction['payment_method'],
            'affects_document_id' => isset($request->document_transaction['affects_document_id']) && !is_null($request->document_transaction['affects_document_id']) ? $request->document_transaction['affects_document_id'] : null,
            'crossing_document_id' => isset($request->document_transaction['documet_id']) ? $request->document_transaction['documet_id'] : null,
            'bank_document_number' => isset($request->document_transaction['bank_document_number']) ? $request->document_transaction['bank_document_number'] : null,
            'commissionable_concept_id' => isset($request->document_transaction['commissionable_concept_id']) ? $request->document_transaction['commissionable_concept_id'] : null,
            // 'affects_prefix_document' => $request->document_transaction["affects_prefix_document"],
            // 'crossing_consecutive' => $request->document_transaction["crossing_number"],
            // 'crossing_due_date_document' => $request->document_transaction["crossing_date"],
        ];
    }

    public function getTransactionsFromDocument($document_id)
    {
        $document_transactions = DocumentTransactions::where('document_id', $document_id)
            ->with(['document' => function ($query) {
                $query->where('in_progress', false);
            },
                'contact:id,identification,name,surname',
                'account:id,code,name,manage_contact_balances,manage_projects,manage_way_to_pay,cost_center_1,cost_center_2,cost_center_3,manage_base_value',
            ])
            ->get();
        return (object) [
            'code' => '200',
            'completed' => true,
            'data' => (object) [
                'message' => 'exito',
                'data' => $document_transactions,
            ],
        ];
    }
    public function destroy($id)
    {
        DocumentTransactions::find($id)->delete();

        return (object) [
            'code' => '200',
            'completed' => true,
            'data' => (object) [
                'message' => 'se ha eliminado con exito',
            ],
        ];
    }
    public function auxiliaryBook($request)
    {
        $document_transactions_ = DocumentTransactions::
            select('id', 'detail', 'operation_value', 'base_value', 'document_id', 'account_id', 'contact_id', 'vouchertype_id', 'bank_document_number', 'transactions_type',
            DB::raw("date(document_date) as document_date"),
            DB::raw("(CASE WHEN transactions_type = 'D' THEN operation_value ELSE 0 END) AS debits"),
            DB::raw("(CASE WHEN transactions_type = 'C' THEN operation_value ELSE 0 END) AS credits")
        )
            ->where('year_month', '>=', $request->year_month_from)
            ->where('year_month', '<=', $request->year_month_up)
            ->with([
                'document' => function ($query) use ($request) {
                    $query->select('id', 'consecutive', 'branchoffice_id');
                    $query->with([
                        'branchoffice:id,name,code',
                    ]);
                },
                'account' => function ($query) use ($request) {
                    $query->select('id', 'code', 'name', 'debit_credit');
                    $query->orderBy('code');
                },
                'contact:id,identification,name,surname',
                'vouchertype:id,code_voucher_type,name_voucher_type',
            ])
            ->whereHas('document', function ($query) use ($request) {
                $query->where('in_progress', false);
                if (isset($request->branchoffice_id) && !is_null($request->branchoffice_id) && !empty($request->branchoffice_id)) {
                    $query->where('branchoffice_id', $request->branchoffice_id);
                }
                if (isset($request->contact_warehouse_id) && !is_null($request->contact_warehouse_id) && !empty($request->contact_warehouse_id)) {
                    $query->where('warehouse_id', $request->contact_warehouse_id);
                }
            })
            ->whereHas(
                'account', function ($query) use ($request) {
                    $query->select('id', 'code', 'name');
                    if (isset($request->account_up) && !is_null($request->account_up) && !empty($request->account_up)) {
                        // $query->where('code', 'ILIKE',  substr($request->account_from, 0, 1) . '%');
                        $query->where('code', '>=', $request->account_from);
                        $query->where('code', '<=', $request->account_up);

                    } else {
                        $query->where('code', 'ILIKE', $request->account_from . '%');
                    }

                    $query->orderBy('code');
                }
            );
        if (isset($request->contact_id) && !is_null($request->contact_id) && !empty($request->contact_id)) {
            $document_transactions_->where('contact_id', $request->contact_id);
        }
        if (isset($request->vouchertype_id) && !is_null($request->vouchertype_id) && !empty($request->vouchertype_id)) {
            $document_transactions_->where('vouchertype_id', $request->vouchertype_id);
        }

        $document_transactions_->orderBy('document_date', 'ASC')->orderBy('account_id', 'ASC');

        //validamos si hay tercero o sucursal
        if ((isset($request->contact_id) && !is_null($request->contact_id) && !empty($request->contact_id)) ||
            (isset($request->contact_warehouse_id) && !is_null($request->contact_warehouse_id) && !empty($request->contact_warehouse_id))) {
            return $document_transactions_->get();
        } else {
            //buscamos el saldo
            $document_transactions_ = $this->setBalances($document_transactions_->get(), $request);
            return $document_transactions_;
        }

    }

    public function reportCommissions($request)
    {
        $authUser = User::where('id', Auth::user()->id)->
        with('getRoles')->first();
        if ($authUser->getRoles[0]->role == 'Vendedor') {
            $authUser = 'Vendedor';
        } else {
            $authUser = 'No Vendedor';
        }
        
        if($request->type_query == '0'){
            $document_transactions_ = DB::table('document_transactions as dt')
               ->selectRaw("distinct
                       concat(s.name, ' ', s.surname) as seller,
                       date(d.document_date) as document_date_cash_receipt,
                       CASE WHEN trim(d.prefix) <> '' THEN concat(trim(d.prefix), '-', d.consecutive) ELSE concat('', d.consecutive) END as cash_receipt,
                       concat(c.name, ' ', c.surname) as contact,
                       CASE WHEN trim(ad.prefix) <> '' THEN concat(trim(ad.prefix), '-', ad.consecutive) ELSE concat('', ad.consecutive) END as invoice,
                       dto.value as discount,
                       round(cast((pay.value - dto.value) * 0.84 as numeric), 2) as pay,
                       p.code,
                       p.description,
                       dp.unit_value_before_taxes,
                       round(cast((dp.total_value_brut / ad.total_value_brut) * 100 as numeric), 2) as participation_percentage,
                       case when coalesce(dp.unit_value_before_taxes, 0) != 0 and coalesce(dp.quantity, 0) != 0 then round(cast(100 - (dp.total_value_brut / (dp.unit_value_before_taxes * dp.quantity)) * 100 as numeric), 2) else 0 end as discount_invoice,
                       date(ad.document_date) as document_date_invoice,
                       round(cast((((pay.value - dto.value) * 0.84) * (((dp.total_value_brut / ad.total_value_brut) * 100) / 100)) as numeric), 2) as pay_product,
                       d.document_date::date - ad.document_date::date as days_pay,
                       di.commission_percentage,
                       round(cast(((((pay.value - dto.value) * 0.84) * (((dp.total_value_brut / ad.total_value_brut) * 100) / 100))*(di.commission_percentage/100)) as numeric), 2) as value_commission")
               ->join('documents as ad', 'ad.id', '=', 'dt.affects_document_id')
               ->join('contacts as s','s.id','=','ad.seller_id')
               ->join('documents as d','d.id','=','dt.document_id')
               ->join('voucherstypes as v','v.id','=','d.vouchertype_id')
               ->join('contacts as c','c.id','=','d.contact_id')
               ->join('documents_products as dp','dp.document_id','=','ad.id')
               ->join('documents_informations as di','di.document_id','=','ad.id')
               ->join('products as p','p.id','=','dp.product_id')
               ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '530535%' group by dt.document_id) as dto"),'dto.document_id', '=', 'dt.document_id')
               ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '130505%' group by dt.document_id) as pay"),'pay.document_id','=','dt.document_id')
               ->where('v.code_voucher_type', '=', '310')
               ->where('dt.year_month', '>=', $request->year_month_from)
               ->where('dt.year_month', '<=', $request->year_month_up);
               if($authUser == 'Vendedor'){
                $document_transactions_->where('s.id', '=', Auth::user()->contact_id)->get();
               }               
               $document_transactions_ = $document_transactions_->get();
               $parametria = ParamTable::where('code_table','=','065')->select('id','code_table','name_table')->first();
               $parametriaPorcentajes = Parameter::where('paramtable_id','=',$parametria->id)->select('paramtable_id','code_parameter','name_parameter','numeric_data_first','numeric_data_second','numeric_data_third')->get();
    
               $document_transactions_->map(function ($document_transaction) use($parametriaPorcentajes)
               {
                   $days_pay = $document_transaction->days_pay;
                   $discount_invoice = $document_transaction->discount_invoice?0:$document_transaction->discount_invoice;
                   $entry = false;
    
                   //dd($parametriaPorcentajes);
                   $active = false;
                   for ($g=0; $g < $parametriaPorcentajes->count() ; $g++) {
    
                       if($days_pay <= $parametriaPorcentajes[$g]['numeric_data_second']){
                           if($parametriaPorcentajes[$g]['numeric_data_third'] != '' && $discount_invoice <= $parametriaPorcentajes[$g]['numeric_data_third'] && $active == false){
                               $document_transaction->commission_percentage = $parametriaPorcentajes[$g]['numeric_data_first'];
                               $document_transaction->value_commission = round($document_transaction->pay_product*($document_transaction->commission_percentage/100));
                               $active = true;
                           }else if($parametriaPorcentajes[$g]['numeric_data_third'] == '' && $active == false){
                               $document_transaction->commission_percentage = $parametriaPorcentajes[$g]['numeric_data_first'];
                               $document_transaction->value_commission = round($document_transaction->pay_product*($document_transaction->commission_percentage/100));
                               $active = true;
                           }
    
                       }
    
                   }
    
                   if($days_pay >= 81){
                       $document_transaction->commission_percentage = "0";
                       $document_transaction->value_commission = "0";
                       $active = true;
                   }
    
               });

        }else{
            
            $document_transactions_ = DB::table('document_transactions as dt')
               ->selectRaw("distinct
                       ad.id,
                       concat(s.name, ' ', s.surname) as seller,
                       date(d.document_date) as document_date_cash_receipt,
                       CASE WHEN trim(d.prefix) <> '' THEN concat(trim(d.prefix), '-', d.consecutive) ELSE concat('', d.consecutive) END as cash_receipt,
                       concat(c.name, ' ', c.surname) as contact,
                       CASE WHEN trim(ad.prefix) <> '' THEN concat(trim(ad.prefix), '-', ad.consecutive) ELSE concat('', ad.consecutive) END as invoice,
                       dto.value as discount,
                       round(cast((pay.value - dto.value) * 0.84 as numeric), 2) as pay,
                       round(cast(((((pay.value - dto.value) * 0.84) * (((dp.total_value_brut / ad.total_value_brut) * 100) / 100))*(di.commission_percentage/100)) as numeric), 2) as value_commission")
               ->join('documents as ad', 'ad.id', '=', 'dt.affects_document_id')
               ->join('contacts as s','s.id','=','ad.seller_id')
               ->join('documents as d','d.id','=','dt.document_id')
               ->join('voucherstypes as v','v.id','=','d.vouchertype_id')
               ->join('contacts as c','c.id','=','d.contact_id')
               ->join('documents_informations as di','di.document_id','=','ad.id')
               ->join('documents_products as dp','dp.document_id','=','ad.id')
               ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '530535%' group by dt.document_id) as dto"),'dto.document_id', '=', 'dt.document_id')
               ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '130505%' group by dt.document_id) as pay"),'pay.document_id','=','dt.document_id')
               ->where('v.code_voucher_type', '=', '310')
               ->where('dt.year_month', '>=', $request->year_month_from)
               ->where('dt.year_month', '<=', $request->year_month_up);
               if($authUser == 'Vendedor'){
                $document_transactions_->where('s.id', '=', Auth::user()->contact_id);
               }

               $document_transactions_ = $document_transactions_->get();
               $parametria = ParamTable::where('code_table','=','065')->select('id','code_table','name_table')->first();
               $parametriaPorcentajes = Parameter::where('paramtable_id','=',$parametria->id)->select('paramtable_id','code_parameter','name_parameter','numeric_data_first','numeric_data_second','numeric_data_third')->get();
               $year_month_from = $request->year_month_from;
               $year_month_up = $request->year_month_up;
               $user = Auth::user()->contact_id;
               $document_transactions_->map(function ($document_transactions) use($parametriaPorcentajes,$year_month_from,$year_month_up,$user,$authUser)
               {
                        $document_days_invoice = DB::table('document_transactions as dt')
                        ->selectRaw("
                                p.code,
                                round(cast((((pay.value - dto.value) * 0.84) * (((dp.total_value_brut / ad.total_value_brut) * 100) / 100)) as numeric), 2) as pay_product,
                                case when coalesce(dp.unit_value_before_taxes, 0) != 0 and coalesce(dp.quantity, 0) != 0 then round(cast(100 - (dp.total_value_brut / (dp.unit_value_before_taxes * dp.quantity)) * 100 as numeric), 2) else 0 end as discount_invoice,
                                d.document_date::date - ad.document_date::date as days_pay")
                        ->join('documents as ad', 'ad.id', '=', 'dt.affects_document_id')
                        ->join('contacts as s','s.id','=','ad.seller_id')
                        ->join('documents as d','d.id','=','dt.document_id')
                        ->join('voucherstypes as v','v.id','=','d.vouchertype_id')
                        ->join('contacts as c','c.id','=','d.contact_id')
                        ->join('documents_products as dp','dp.document_id','=','ad.id')
                        ->join('documents_informations as di','di.document_id','=','ad.id')
                        ->join('products as p','p.id','=','dp.product_id')
                        ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '530535%' group by dt.document_id) as dto"),'dto.document_id', '=', 'dt.document_id')
                        ->join(DB::raw("(select dt.document_id,sum(dt.operation_value) as value from document_transactions dt inner join accounts a on a.id = dt.account_id where a.code ilike '130505%' group by dt.document_id) as pay"),'pay.document_id','=','dt.document_id')
                        ->where('v.code_voucher_type', '=', '310')
                        ->where('dt.year_month', '>=', $year_month_from)
                        ->where('dt.year_month', '<=', $year_month_up)
                        ->where('ad.id', '<=', $document_transactions->id);
                        if($authUser == 'Vendedor'){
                            $document_days_invoice->where('s.id', '=', $user)->get();
                           }               
                           $document_days_invoice = $document_days_invoice->get();
                        $document_days_invoice->map(function ($document_transaction) use($parametriaPorcentajes)
                        {
                            $days_pay = $document_transaction->days_pay;
                            $discount_invoice = $document_transaction->discount_invoice?0:$document_transaction->discount_invoice;
                            $entry = false;
                
                            //dd($parametriaPorcentajes);
                            $active = false;
                            for ($g=0; $g < $parametriaPorcentajes->count() ; $g++) {
                
                                if($days_pay <= $parametriaPorcentajes[$g]['numeric_data_second']){
                                    if($parametriaPorcentajes[$g]['numeric_data_third'] != '' && $discount_invoice <= $parametriaPorcentajes[$g]['numeric_data_third'] && $active == false){
                                        $document_transaction->commission_percentage = $parametriaPorcentajes[$g]['numeric_data_first'];
                                        $document_transaction->value_commission = round($document_transaction->pay_product*($document_transaction->commission_percentage/100));
                                        $active = true;
                                    }else if($parametriaPorcentajes[$g]['numeric_data_third'] == '' && $active == false){
                                        $document_transaction->commission_percentage = $parametriaPorcentajes[$g]['numeric_data_first'];
                                        $document_transaction->value_commission = round($document_transaction->pay_product*($document_transaction->commission_percentage/100));
                                        $active = true;
                                    }
                
                                }
                
                            }
                
                            if($days_pay >= 81){
                                $document_transaction->commission_percentage = "0";
                                $document_transaction->value_commission = "0";
                                $active = true;
                            }
                
                        });

                        $plus_commission = $document_days_invoice->sum('value_commission');
                        
                        $document_transactions->value_commission = $plus_commission;
    
               });
               

        }


        return $document_transactions_;

    }

    public function stateFinanze($request)
    {
        $document_transactions_ = DocumentTransactions::
            select('id', 'detail', 'operation_value', 'base_value', 'document_id', 'account_id', 'contact_id', 'vouchertype_id', 'bank_document_number', 'transactions_type',
            DB::raw("date(document_date) as document_date"),
            DB::raw("(CASE WHEN transactions_type = 'D' THEN operation_value ELSE 0 END) AS debits"),
            DB::raw("(CASE WHEN transactions_type = 'C' THEN operation_value ELSE 0 END) AS credits")
        )
            ->whereRaw('substr(year_month,1,4)', '>=', $request->year_month_from)
            ->with([
                'document' => function ($query) use ($request) {
                    $query->select('id', 'consecutive', 'branchoffice_id');
                    $query->with([
                        'branchoffice:id,name,code',
                    ]);
                },
                'account' => function ($query) use ($request) {
                    $query->select('id', 'code', 'name', 'debit_credit');
                    $query->orderBy('code');
                },
                'contact:id,identification,name,surname',
                'vouchertype:id,code_voucher_type,name_voucher_type',
            ])
            ->whereHas('document', function ($query) use ($request) {
                $query->where('in_progress', false);
                if (isset($request->branchoffice_id) && !is_null($request->branchoffice_id) && !empty($request->branchoffice_id)) {
                    $query->where('branchoffice_id', $request->branchoffice_id);
                }
                if (isset($request->contact_warehouse_id) && !is_null($request->contact_warehouse_id) && !empty($request->contact_warehouse_id)) {
                    $query->where('warehouse_id', $request->contact_warehouse_id);
                }
            })
            ->whereHas(
                'account', function ($query) use ($request) {
                    $query->select('id', 'code', 'name');
                    if (isset($request->account_up) && !is_null($request->account_up) && !empty($request->account_up)) {
                        // $query->where('code', 'ILIKE',  substr($request->account_from, 0, 1) . '%');
                        $query->where('code', '>=', $request->account_from);
                        $query->where('code', '<=', $request->account_up);

                    } else {
                        $query->where('code', 'ILIKE', $request->account_from . '%');
                    }

                    $query->orderBy('code');
                }
            );
        if (isset($request->contact_id) && !is_null($request->contact_id) && !empty($request->contact_id)) {
            $document_transactions_->where('contact_id', $request->contact_id);
        }
        if (isset($request->vouchertype_id) && !is_null($request->vouchertype_id) && !empty($request->vouchertype_id)) {
            $document_transactions_->where('vouchertype_id', $request->vouchertype_id);
        }

        $document_transactions_->orderBy('document_date', 'ASC')->orderBy('account_id', 'ASC');

        //validamos si hay tercero o sucursal
        if ((isset($request->contact_id) && !is_null($request->contact_id) && !empty($request->contact_id)) ||
            (isset($request->contact_warehouse_id) && !is_null($request->contact_warehouse_id) && !empty($request->contact_warehouse_id))) {
            return $document_transactions_->get();
        } else {
            //buscamos el saldo
            $document_transactions_ = $this->setBalances($document_transactions_->get(), $request);
            return $document_transactions_;
        }

    }

    public function setBalances($document_transactions_, $request)
    {
        $document_transactions_list = [];
        // lista de cuentas para buscar el saldo solo una vez por cuenta
        $account_id_list = [];
        //seteamos lafecha
        $date = new Carbon(substr($request->year_month_from, 0, 4) . '-' . substr($request->year_month_from, 4, 2));
        // restamos un mes y obtenemos la fecha en formato YYYYMM
        $year_month = $date->addMonth(-1)->format('Ym');
        // variable para calcular el saldo
        $control_balance = 0;

        foreach ($document_transactions_ as $key => $dt) {
            // validamos si el id de la cuenta no esta en el array
            if (!in_array($dt->account_id, $account_id_list)) {
                //Buscamos el saldo final del mes anterior
                $balance = BalancesForAccount::select('id', 'final_balance')->where('account_id', $dt->account_id)->where('year_month', $year_month)->first();
                $control_balance = !is_null($balance) ? $balance->final_balance : 0;
                //gurdamos el id de la cuenta en el array para no buscar de nuevo el saldo
                $account_id_list[] = $dt->account_id;
                // agregamos a las partidas el saldo
                $document_transactions_list[] = (object) ['balance' => $control_balance];
            }
            //calculamos el saldo de la partida si es debito suma, credito resta
            if ($dt->transactions_type == 'D') {
                $dt->balance = $control_balance + $dt->operation_value;
            } elseif ($dt->transactions_type == 'C') {
                $dt->balance = $control_balance - $dt->operation_value;
            }
            $control_balance = $dt->balance;
            $document_transactions_list[] = $dt;
        }
        return $document_transactions_list;
    }

    public function getTransactionsWallet($id, $request)
    {
        return DocumentTransactions::select(
            'id', 'document_id', 'affects_document_id', 'transactions_type', 'document_date',
            DB::raw("(CASE WHEN transactions_type = 'D' THEN operation_value ELSE 0 END) AS debits"),
            DB::raw("(CASE WHEN transactions_type = 'C' THEN operation_value ELSE 0 END) AS credits")
        )
            ->with('document:id,prefix,consecutive,document_date,vouchertype_id')
            ->with('document.vouchertype:id,code_voucher_type,name_voucher_type')
            ->whereHas('document', function ($affectsDocument) {
                $affectsDocument->where('in_progress', false);
            })
            ->whereHas('account', function ($account) {
                $account->where('manage_document_balances', true);
            })
            ->where('affects_document_id', $id)
            ->where('year_month', '<=' ,$request->year_month)
            ->orderBy('document_date')
            ->get();
    }

    public function auxiliaryBookContact($request)
    {
        $document_transactions_ = DocumentTransactions::
            select('id', 'document_date', 'detail', 'operation_value', 'base_value', 'document_id', 'account_id', 'contact_id', 'vouchertype_id', 'bank_document_number', 'transactions_type',
            DB::raw("(CASE WHEN transactions_type = 'D' THEN operation_value ELSE 0 END) AS debits"),
            DB::raw("(CASE WHEN transactions_type = 'C' THEN operation_value ELSE 0 END) AS credits")
        )
        // ->where('year_month', '>=', $request->year_month_from)
        // ->where('year_month', '<=', $request->year_month_up)
            ->whereBetween('year_month', [$request->year_month_from, $request->year_month_up])
            ->with([
                'document' => function ($query) use ($request) {
                    $query->select('id', 'consecutive', 'branchoffice_id');
                    $query->with([
                        'branchoffice:id,name,code',
                    ]);
                },
                'account' => function ($query) use ($request) {
                    $query->select('id', 'code', 'name', 'debit_credit');
                    $query->orderBy('code');
                },
                'contact:id,identification,name,surname',
                'vouchertype:id,code_voucher_type,name_voucher_type',
            ])
            ->whereHas('document', function ($query) use ($request) {
                $query->where('in_progress', false);
            })
            ->whereHas(
                'account', function ($query) use ($request) {
                    $query->select('id', 'code', 'name');
                    if (isset($request->account_up) && !is_null($request->account_up) && !empty($request->account_up)) {
                        // $query->where('code', 'ILIKE',  substr($request->account_from, 0, 1) . '%');
                        $query->where('code', '>=', $request->account_from);
                        $query->where('code', '<=', $request->account_up);
                    } else {
                        $query->where('code', 'ILIKE', $request->account_from . '%');
                    }
                    $query->orderBy('code');
                }
            );
        if (isset($request->contact_id) && !is_null($request->contact_id) && !empty($request->contact_id)) {
            $document_transactions_->where('contact_id', $request->contact_id);
        }
        $document_transactions_->orderBy('contact_id', 'ASC')->orderBy('account_id', 'ASC')->orderBy('document_date', 'ASC');

        return $document_transactions_->get();
        $document_transactions_ = $this->setBalances($document_transactions_->get(), $request);
        return $document_transactions_;

    }
}
