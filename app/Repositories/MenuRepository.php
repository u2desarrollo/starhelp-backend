<?php

namespace App\Repositories;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\Menu;
use App\Entities\MenuVideoTutorial;
use App\Entities\MenuSubscriber;
use App\Entities\Module;
use App\Entities\Permission;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MenuRepository
 *
 * @package App\Repositories
 * @version March 6, 2019, 2:00 pm UTC
 * @method Menu findWithoutFail($id, $columns = ['*'])
 * @method Menu find($id, $columns = ['*'])
 * @method Menu first($columns = ['*'])
 */
class MenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'module_id',
        'order',
        'description',
        'url',
        'icon',
        'menu_id',
        'permission_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Menu::class;
    }

    /**
     * Método que consulta todos los items de menu
     *
     * @return mixed
     */
    public function findAll()
    {

        $modules = Module::all();

        return Cache::remember('menu', 10, function (){
            $modules = Module::all();

            foreach ($modules as $module) {
                $menu_subscriber = Menu::where('module_id', $module->id)
                    ->where('level', 1)
                    ->with('module')
                    ->with([
                        'secondLevel'
                    ])
                    ->orderBy('order', 'ASC')
                    ->get();

                $module->first_level = $menu_subscriber;
            }

            return $modules;
        });
    }


    /**
     * @param $modules
     * @return mixed
     */
    public function store($modules)
    {
        try {
            DB::beginTransaction();

            DB::select('TRUNCATE TABLE menus RESTART IDENTITY;');

            $order_first_level = 1;

            foreach ($modules as $module) {
                foreach ($module['first_level'] as $first_level) {
                    $first_level['order'] = $order_first_level++;
                    $first_level['level'] = 1;
                    $first_level['module_id'] = $module['id'];

                    if (isset($first_level['permission'])) {
                        $permission_id = $this->getPermission($first_level['permission']);
                        $first_level['permission_id'] = $permission_id;
                    }

                    $first_level_create = Menu::create($first_level);

                    if (isset($first_level['second_level'])) {

                        $order_second_level = 1;
                        foreach ($first_level['second_level'] as $second_level) {
                            $second_level['order'] = $order_second_level++;
                            $second_level['menu_id'] = $first_level_create->id;
                            $second_level['level'] = 2;
                            $second_level['module_id'] = $module['id'];

                            if (isset($second_level['permission'])) {
                                $permission_id = $this->getPermission($second_level['permission']);
                                $second_level['permission_id'] = $permission_id;
                            }
                            Menu::create($second_level);
                        }
                    }
                }
            }

            DB::commit();

            Cache::forget('menu');

            return $this->findAll();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();
        }

    }

    public function getPermission($permission)
    {
        $p = Permission::where('permission', $permission)->first();

        if ($p) {
            return $p->id;
        }

        return null;
    }

    /**
     * MenuVideoTutorial
     */
    public function createMenuVideoTutorial($data)
    {
        return MenuVideoTutorial::create($data);
    }

    public function updateMenuVideoTutorial($data, $id)
    {
        return MenuVideoTutorial::find($id)->update($data);
    }

    public function deleteMenuVideoTutorial($data, $id)
    {
        return MenuVideoTutorial::find($id)->delete($data);
    }

    public function findAllMenuVideoTutorial($request)
    {
        return MenuVideoTutorial::with([
            'menu',
            'user'
        ])
        ->where('menu_id', $request->menu_id)
        ->orderBy('id')
        ->get();
    }

}
