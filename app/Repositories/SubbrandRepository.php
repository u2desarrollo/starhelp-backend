<?php

namespace App\Repositories;

use App\Entities\Subbrand;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubbrandRepository
 * @package App\Repositories
 * @version March 4, 2019, 7:56 pm UTC
 *
 * @method Subbrand findWithoutFail($id, $columns = ['*'])
 * @method Subbrand find($id, $columns = ['*'])
 * @method Subbrand first($columns = ['*'])
*/
class SubbrandRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'brand_id',
        'subbrand_code',
        'description',
        'image',
        'lock_buy',
        'block_sale',
        'data_sheet',
        'margin_cost_percentage'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subbrand::class;
    }
    public function findAll($request){
        $brands = $request->brands;
        $brand_id = $request->brand_id;

        $subbrands = Subbrand::orderBy('description','ASC');

        if ($brands && is_array($brands)) {
            $subbrands->whereIn('brand_id', $brands);
        }

        if ($brand_id) {
            $subbrands->where('brand_id', $brand_id);
        }

        return $subbrands->orderBy('description','Asc')->get();
    }
}
