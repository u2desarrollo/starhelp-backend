<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ExtraDataRepositoryRepository;
use App\Entities\ExtraDataRepository;
use App\Validators\ExtraDataRepositoryValidator;

/**
 * Class ExtraDataRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ExtraDataRepositoryRepositoryEloquent extends BaseRepository implements ExtraDataRepositoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExtraDataRepository::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
