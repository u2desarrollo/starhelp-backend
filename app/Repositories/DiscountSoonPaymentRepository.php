<?php

namespace App\Repositories;

use App\Entities\BranchOffice;
use App\Entities\DiscountSoonPayment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LineRepository
 * @package App\Repositories
 * @version February 12, 2019, 4:29 pm UTC
 *
 * @method Line findWithoutFail($id, $columns = ['*'])
 * @method Line find($id, $columns = ['*'])
 * @method Line first($columns = ['*'])
*/
class DiscountSoonPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    /*protected $fieldSearchable = [
        'parameter',
        'line_id',
        'brand_id',
        'percentage'
    ];*/

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DiscountSoonPayment::class;
    }

    public function test()
    {
        return DiscountSoonPayment :: get();
    }

    public function createDiscount($request)
    {
        $data = [
            'line_id' => $request->line,
            'branchoffice_id'=>$request->branchoffice_id,
            'subline_id'    => $request->sub_line,
            'days_up'   => $request->days_up,
            'percentage' => $request->percentage,
            'brand_id' => $request->brand_id
        ];

        return DiscountSoonPayment :: create($data);
    }

    public function searchDiscounts()
    {
        // return DiscountSoonPayment :: with(['line','subline','branchoffice','brand'])
        // ->get();

        return $discounts=DiscountSoonPayment::select('discounts_soon_payment.id','days_up','percentage','line_id' ,'brand_id','subline_id' ,'l.line_description',
        'b.name','b.code' )->
        join('lines AS l','discounts_soon_payment.line_id','=','l.id')->
        join('branchoffices AS b','discounts_soon_payment.branchoffice_id','=','b.id')->
        with(['subline','brand'])->
        orderByRaw('b.name, l.line_description asc')->get();



    }

    public function searchDiscount($id)
    {
        return DiscountSoonPayment :: where('id', $id)
                        ->with(['line','subline','brand'])
                        ->first();
    }

    public function updateDiscount($request)
    {
        $discount = DiscountSoonPayment ::  where('id', $request->id)->first();
        $discount->line_id = $request->line;
        $discount->subline_id = $request->sub_line;
        $discount->days_up = $request->days_up;
        $discount->percentage = $request->percentage;
        $discount->branchoffice_id = $request->branchoffice_id;
        $discount->brand_id = $request->brand_id;
        $discount->save();
        return $discount;
    }

    public function deleteDiscount($id)
    {
        $discount = DiscountSoonPayment ::  where('id', $id)->first();
        $discount->delete();

    }

}
