<?php

namespace App\Repositories;

use App\Entities\PromotionRule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromotionRuleRepository
 * @package App\Repositories
 * @version April 17, 2019, 8:37 pm UTC
 *
 * @method PromotionRule findWithoutFail($id, $columns = ['*'])
 * @method PromotionRule find($id, $columns = ['*'])
 * @method PromotionRule first($columns = ['*'])
*/
class PromotionRuleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parameters_id',
        'promotion_group_rule_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromotionRule::class;
    }
}
