<?php

namespace App\Repositories;

use App\Entities\Inventoryx;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentProduct;
use App\Entities\Document;

/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class InventoryxRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'inventory_cost',
        'iva_operation',
        'control_existence_outputs',
        'sales_price_control',
        'control_price_list'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inventoryx::class;
    }

    public function create(array $branchOfficeWarehouses)
    {
        $response=[];
        foreach ($branchOfficeWarehouses as $key => $value) {
            $response[]=Inventoryx::create($value);
        }
        return $response;
/*         return $branchOfficeWarehouses->map( function ($item, $key){
            return Inventoryx::create($item->toArray());
        }); */
    }

    public function createX($data)
    {
        $response=Inventoryx::create($data);
        return $response;
    }
    
    /**
     * Finalizar o actualizar el inventario
     * @author Santiago torres
     * @param $documentProduct id del documentProduct
     */

    public function finalize($documentProduct)
    {

        $documentProduct_id = $documentProduct;

        $dc = DocumentProduct :: where ('id', $documentProduct_id)->first();

        $documentProduct = DocumentProduct::select("id","branchoffice_warehouse_id","document_id", "product_id", "quantity", "unit_value_before_taxes", "operationType_id", "total_value_brut", "manage_inventory", "inventory_cost_value", "fixed_cost")
        ->where('id', $documentProduct_id)
        ->with([
            'operationType',
            'document' => function($q){
                $q->select("id", "branchoffice_warehouse_id", "vouchertype_id" , "thread");
            },
            'product' => function($q){
                $q->with(['line']);
            }
        ])
        ->wherehas('document')
        ->wherehas('product', function($q){
            $q->wherehas('line' , function($q){
                $q->where('inventory_update', true);
                $q->where('service', false);
            });
        })
        ->first();

        if ($documentProduct == null) {
            return [];
        }
            
        $validation = Inventoryx :: where('product_id', $documentProduct->product_id)
                    -> where('branchoffice_warehouse_id',$documentProduct->branchoffice_warehouse_id)
                    ->first();
        //return $documentProduct;

        if ($documentProduct->manage_inventory) {
            

            if (!$documentProduct->fixed_cost) {
                switch ($documentProduct->operationType->inventory_cost) {  
                    case '2' :
                        $dc->inventory_cost_value = $dc->total_value_brut;
                    break;
                    case '3':
                        if ($validation) {
                            if ($validation->average_cost){
                                $dc->inventory_cost_value = $dc->quantity * floatval($validation->average_cost);
                            }
                        }
                    break;
                    case '4':
                        $base = Document ::where('id', $documentProduct->document->thread)->first();
                        $documentProductBase = DocumentProduct :: where('document_id', $base->id)->where('product_id', $documentProduct->product_id)->first();
                        
                        if ($documentProductBase) {
                            if ($dc->quantity == $documentProductBase->quantity) {
                                $dc->inventory_cost_value =  $documentProductBase->inventory_cost_value;
                            }else {
                                $dc->inventory_cost_value =  $documentProductBase->inventory_cost_value/$documentProductBase->quantity * $documentProduct->quantity;
                            }
                        }else{
                            if ($validation) {
                                if ($validation->average_cost){
                                    $dc->inventory_cost_value = $dc->quantity * floatval($validation->average_cost);
                                }
                            }else{
                                $dc->inventory_cost_value = 0;
                            }
                        }
                    break;
                }

            }
            
            
            
    
    
            if ($documentProduct->operationType->affectation == 1) {
    
                if ($validation){
                    $stock = $validation->stock + $documentProduct->quantity;
                    $stock_values = $validation->stock_values + $dc->inventory_cost_value;
    
    
                    if ($validation->stock != 0) {
                        $average_cost = number_format($validation->stock_values/$validation->stock, 2, '.', '');
                    }else{
                        //si el stock es 0 se deja el costo promedio
                        $average_cost = $validation->average_cost;
                    }
                }else{
                    $stock = $documentProduct->quantity;
                    $stock_values = $dc->inventory_cost_value == null ? 0 :$dc->inventory_cost_value;
                    $average_cost = $stock != 0 ? number_format($stock_values/$stock, 2, '.', '') : 0;
                }
    
            }elseif ($documentProduct->operationType->affectation == 2) {
                if ($validation){
                    $stock = $validation->stock - $documentProduct->quantity;
                    $stock_values = $validation->stock_values - $dc->inventory_cost_value;
                    $available = $validation->available;

                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {
                        $available-=$documentProduct->quantity;
                    }


                    if($validation->stock != 0){
                        $average_cost = $validation->stock_values/$validation->stock;
                    }else{
                        //si el stock es 0 se deja el costo promedio
                        $average_cost = $validation->average_cost;
                    }

                }else{
                    if ($documentProduct->document->branchofficeWarehouse->warehouse_code == '01' ) {

                        $available = $documentProduct->quantity; 
                    }else{
                        $available = 0;
                    }

                    $stock = 0-$documentProduct->quantity;
                    $stock_values = 0-$dc->inventory_cost_value;
                    if ($stock!=0) {
                        $average_cost = number_format($stock_values/$stock, 2, '.', '');
                    }else{
                        $average_cost = 0;
                    }
                }
            }
            
            if (isset($stock)) {
                // Actualizamos inventario
                $r =Inventoryx::updateOrCreate(
                    [
                        'product_id' => $documentProduct->product_id,
                        'branchoffice_warehouse_id' => $documentProduct->branchoffice_warehouse_id
                    ],
                    [
                        'branchoffice_warehouse_id' => $documentProduct->branchoffice_warehouse_id,
                        'product_id' => $documentProduct->product_id,
                        'stock' => $stock,
                        'stock_values' => $stock_values,
                        'average_cost' => $average_cost
                    ]
                );
        
                if ($r->stock != 0) {
                    $r->average_cost = number_format($r->stock_values/$r->stock, 2, '.', '');
                   // $r->average_cost = bcdiv($r->average_cost, '1', 2);
                    $r->save();
                }else{
                    // $r->average_cost = 0;
                    // $r->save();
                }
        
                $dc->stock_after_operation = $stock;
                $dc->stock_value_after_operation = $stock_values;
                $dc->save();

                $negatives = false;
                // validamos los negativos
                if ($stock < 0) {
                    $negatives = true;
                }

                return [
                        'negatives'=>$negatives ? true : false,
                        'data' => $r,
                        'status' => true,
                        'message' => 'se ha actualizado el inventario'];
            }
        }
                            
    }
    

    public function getinventoryforupdate()
    {
        return Inventoryx ::  select(
            'branchoffice_warehouse_id',
            'product_id',
            'stock',
            'stock_values',
            'average_cost'
        )->get();
    }
}
