<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Entities\Price;
use App\Entities\Parameter;
use App\Entities\DocumentProduct;
use App\Traits\CacheTrait;
use Illuminate\Http\Request;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Arr;

/**
 * Class PriceRepository
 * @package App\Repositories
 * @version April 3, 2019, 7:09 pm UTC
 *
 * @method Price findWithoutFail($id, $columns = ['*'])
 * @method Price find($id, $columns = ['*'])
 * @method Price first($columns = ['*'])
*/
class PriceRepository extends BaseRepository
{
    use CacheTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'price_list_id',
        'branchoffice_id',
        'price',
        'include_iva',
        'previous_price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Price::class;
    }


    /* Obtiene los precios segun la lista */
    public function getPricesGrid($request){

        $prices = Product::select([
            "id",
            "code",
            "description",
            "line_id",
            "subline_id",
            "brand_id",
            "subbrand_id",
            "quantity_available"
        ])
        ->with([
            'price' => function($query) use ($request){
                $query->where('price_list_id', $request->list);
            },
            'prices' => function ($query) {
                $query->with(['parameter']);
            },
            'brand',
            'line',
            'subbrand',
            'subline',
            'inventory'
        ])
        ->orderByRaw('id ASC')
        ->take(5000)
        ->where(function ($query) {
            $query->orWhere('state', true);
            $query->orWhere('state', null);
        });

        if ($request->line_id) {
            $prices->where('line_id', $request->line_id);
        }

        if ($request->brand) {
            $prices->where('brand_id', $request->brand);
        }

        if ($request->branchOffice_id) {
            $prices->whereHas('price', function($query) use ($request){
                $query->where('branchoffice_id', $request->branchOffice_id);
            });
        }

        if ($request->product) {
            $prices->where(function($query) use ($request) {
                $query->where('code','ILIKE', '%'.$request->product.'%');
                $query->orWhere('description','ILIKE', '%'.$request->product.'%');
            });
        }

        $prices = $prices->get();

        $prices->map(function($item, $key){
            $item->exist = 0;
            $item->cto_prom = 0;
            $item->count = count($item->inventory);
            $item->inventory->map(function($inv, $key) use ($item) {
                $item->exist += $inv->stock;
                $item->cto_prom += $inv->average_cost;
            });
            if ($item->count != 0) {
                $item->cto_prom = $item->cto_prom / $item->count;
            }
        });

        return $prices;

    }

    public function updatePrice(array $input, $id){

        return $price = Price::updateOrCreate(Arr::only($input, ['product_id', 'price_list_id', 'branchoffice_id']), $input);

    }


    /**
     * Actualiza precios
     *
     * @param Request $request Contiene un array con los precios a actualizar
     * @return array Contiene los precios actualizados
     * @author Jhon García
     */
    public function updatedPriceArray(Request $request)
    {
        $prices = $request->all();
        $prices_updated = [];

        foreach ($prices as $key => $price) {
            $prices_updated[$key] = Price::updateOrCreate(
                Arr::only($price, ['product_id', 'price_list_id', 'branchoffice_id']),
                Arr::only($price, ['product_id', 'price_list_id', 'branchoffice_id', 'price'])
            );
        }

        return $prices_updated;
    }

    public function updatedPriceArrayExcel($request){
        $array = $request->all();
        $resposne = [];

        foreach($array as $key => $datay){

            $codList = strlen($datay['cod_list']) === 1 ? '00'.$datay['cod_list'] : $datay['cod_list'];

            $product = Product::where('code', $datay['code'])->first();
            $priceList = Parameter::where('code_parameter', $codList)
            ->whereHas('paramtable', function($query) {
                $query->where('code_table', 'LDP');
            })->first();

            if ($product && $priceList) {
                $resposne[$key] = Price::updateOrCreate(
                    [
                        'product_id' => $product->id,
                        'price_list_id' => $priceList->id,
                        'branchoffice_id' => null,
                    ],
                    [
                        'product_id' => $product->id,
                        'price_list_id' => $priceList->id,
                        'branchoffice_id' => null,
                        'price' => $datay["new_price"] ? str_replace(',', '.', $datay["new_price"]) : 0
                    ]
                );
            }

        }
        return $resposne;
    }

    public function updatepriceModel($document_product_id)
    {
        $new_value = null;

        $update_document_product = $document_product = DocumentProduct::where('id', $document_product_id)
            ->with([
                    'product.line',
                    'product.brand',
                    'product.prices',
                    'document.vouchertype.template.operationType'
                ])->first();

        $model = $this->getModelForIdFromCache($document_product->document->model_id);

        $document_product->product->price = $document_product->product->prices->firstWhere('price_list_id', $model->price_list);

        if (!empty($document_product->product->price) && !empty($document_product->product->price->price_list_id)) {
            return [
                'newPrice'    => [],
                'documentPro' => $update_document_product
            ];
        }

        if (!is_null($model->price_list)) {
            switch ($model->method) {
                case '1':
                    return 'igual al valor del documento';
                    break;
                case '2':
                    if ($model->condition == '1') {
                        if (!empty($document_product->product->brand) && !empty($document_product->product->brand->percentage_utility)) {
                            $new_value = $document_product->unit_value_before_taxes * $document_product->product->brand->percentage_utility / 100;
                        }
                    }
                    break;
                case '3':
                    if ($model->condition == '1') {
                        if (!empty($document_product->product->line) && !empty($document_product->product->line->percentage_utility)) {
                            $new_value = $document_product->unit_value_before_taxes * $document_product->product->line->percentage_utility / 100;
                        }
                    }
                    break;
            }

            if (!is_null($new_value)) {
                $new_value += $document_product->unit_value_before_taxes;

                if (!is_null($document_product->product->price) && $new_value > $document_product->product->price->price) {
                    $new_price = Price::find($document_product->product->price->id);
                    $new_price->previous_price = $new_price->price;
                    $new_price->price = $new_value;
                    $new_price->save();

                    return [
                        'newPrice'    => $new_price,
                        'documentPro' => $update_document_product
                    ];
                }
            }
        }
    }

    public function setPricesFromCsv($row, $product)
    {
        if (empty($row["Precios "]) || $row["Precios "] == '#N/A' || $row["Precios "] == '#N/D') {
            return false;
        }
        $parameter = Parameter:: select('id')
            ->where('code_parameter', '001')
            ->where('paramtable_id', 14)
            ->first();

        $price = Price:: updateOrCreate(
            [
                'product_id'    => $product->id,
                'price_list_id' => $parameter->id
            ],
            [
                'product_id'    => $product->id,
                'price_list_id' => $parameter->id,
                'price'         => $row["Precios "]
            ]
        );

        return $price;
    }

    public function formatPriceArrayExcel($request)
    {
        $file = $request->file('archivo_importar');
        $fileData = collect($this->csvToArray($file->getRealPath()));

        // Validaciones
        $errorRows = $fileData->filter(function ($item) {
            return false !== stristr($item['NuevoPrecio'], '.');
        });

        if (count($errorRows) > 0) {
            return [
                'code' => '400',
                'message' => 'Los precios no pueden tener decimales ni separadores de miles'
            ];
        }

        $productsCodes = $fileData->pluck('Codigo');
        $codList = strlen($fileData[0]['CodLista']) === 1 ? '00'.$fileData[0]['CodLista'] : $fileData[0]['CodLista'];
        $priceList = Parameter::where('code_parameter', $codList)
        ->whereHas('paramtable', function($query) {
            $query->where('code_table', 'LDP');
        })->first();

        $prices = Product::select([
            "id",
            "code",
            "description",
            "line_id",
            "subline_id",
            "brand_id",
            "subbrand_id",
            "quantity_available"
        ])
        ->with([
            'price' => function($query) use ($priceList){
                $query->where('price_list_id', $priceList->id);
            },
            'prices' => function ($query) {
                $query->with(['parameter']);
            },
            'brand',
            'line',
            'subbrand',
            'subline',
            'inventory'
        ])
        ->orderByRaw('id ASC')
        ->where(function ($query) {
            $query->orWhere('state', true);
            $query->orWhere('state', null);
        })
        ->whereIn('code', $productsCodes)
        ->get();

        $prices->map(function($item, $key) use ($fileData, $codList) {

            $product = $fileData->where('Codigo', $item->code)->first();

            $item->new_price = $product['NuevoPrecio'];
            $item->cod_list = $codList;
            $item->exist = 0;
            $item->cto_prom = 0;
            $item->count = count($item->inventory);
            $item->inventory->map(function($inv, $key) use ($item) {
                $item->exist += $inv->stock;
                $item->cto_prom += $inv->average_cost;
            });
            if ($item->count != 0) {
                $item->cto_prom = $item->cto_prom / $item->count;
            }
        });

        return [
            'code' => '200',
            'data' => $prices
        ];
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        $delimiter = $this->detectDelimiter($filename);
        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = str_replace('"', " ", $row);
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $row = preg_replace('!\s+!', '', $row);

                    $header = $row;

                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    public function detectDelimiter($csvFile)
    {
        $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

        $handle = fopen($csvFile, "r");
        $firstLine = fgets($handle);
        fclose($handle); 
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }
}
