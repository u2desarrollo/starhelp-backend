<?php

namespace App\Repositories;

use App\Entities\Parameter;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ParameterRepository
 * @package App\Repositories
 * @version December 18, 2018, 4:35 pm UTC
 *
 * @method Parameter findWithoutFail($id, $columns = ['*'])
 * @method Parameter find($id, $columns = ['*'])
 * @method Parameter first($columns = ['*'])
*/
class ParameterRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'paramtable_id',
		'code_parameter',
		'name_parameter',
		'alphanum_data_first',
		'alphanum_data_second',
		'alphanum_data_third',
		'numeric_data_first',
		'numeric_data_second',
		'numeric_data_third',
		'date_data_first',
		'date_data_second',
		'date_data_third',
		'image'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Parameter::class;
	}

	/**
	 * Método para obtener el listado de parameters de acuerdo a los parámetros recibidos
	 * @param $request contiene los parámetros para filtrar
	 * @return $parameters contiene una collección de de parameters
	 */
	public function findForParamTable($request){
		
		$paginate = $request->paginate == 'true' ? true : false; 
		$filter   = $request->filter; 

		// Traemos los parametros segun el id de la tabla
		if ($request->codeParamTable) {
			$parameters = Parameter::whereHas('paramtable', function ($query) use ($request) {
				$query->where('code_table', $request->codeParamTable);
			});
		} else {
			$parameters = Parameter::where('paramtable_id', $request->idParamTable);
		}

		// Validamos si tiene filtro de busqueda
		if ($filter) {
			$parameters->where(function ($query) use ($filter) {
				$query->where('name_parameter', 'ILIKE', '%' . $filter . '%');
				$query->orWhere('code_parameter', 'LIKE', '%' . $filter . '%');
			});
		}

		// Con paginacion
		if ($paginate) {
			// Traemos la informacion paginada
			$parameters = $parameters->orderBy($request->sortBy, $request->sort)->paginate($request->perPage);
		}
		
		// Sin paginacion
		else {
			// Traemos la informacion sin paginar
			$parameters = $parameters->get();
		}

		return $parameters;
	}

	public function findForId($id){
		$parameters = Parameter::where('id', $id)
			->where('paramtable_id', $request->idParamTable)
			->first();
		return $parameters;
	}

	public function getPriceList(){
		$price_list=Parameter::where('paramtable_id',14)->get();
		return $price_list;
	}
	public function getTypePayment(){
		$typePayment=Parameter::where('paramtable_id',69)->get();
		return $typePayment;
	}
	public function getTypeNegotiationPortfolio()
	{
		$type_negotiation_portfolio=Parameter::where('paramtable_id',112)->get();
		return $type_negotiation_portfolio;
	}
	public function getParameterCategory($request){
		$group=Parameter::where('paramtable_id',$request->id)->get();
		return $group;
	}

	/**
	 * Obtiene los estados de los documentos
	 *
	 * @return array
	 */
	public function documentsStatus() {
		//$status = Parameter::whereIn('paramtable_id', [64, 67])->get();
		$status = Parameter::where('paramtable_id', 64)->get();
		return $status;
	}

	/**
	 * cunsulta para trar un parametro
	 * @santiago torres
	 * @param codigo tabla de parametro, codigo parametro
	 * @return parameter 
	*/
	public function getParameter($paramtable_code, $parameter_code)
	{
		$parameter = Parameter :: where('code_parameter', $parameter_code)
		->orWhere('name_parameter', $parameter_code)
		->whereHas('paramtable', function ($query) use ($paramtable_code){
			$query->where('code_table', $paramtable_code);
		})
		->first();

		if ($parameter){
			return $parameter->id;
		}else{
			return null;
		}
	}
}