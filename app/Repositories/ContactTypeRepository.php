<?php

namespace App\Repositories;

use App\Entities\ContactType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactTypeRepository
 * @package App\Repositories
 * @version January 10, 2019, 4:51 pm UTC
 *
 * @method ContactType findWithoutFail($id, $columns = ['*'])
 * @method ContactType find($id, $columns = ['*'])
 * @method ContactType first($columns = ['*'])
*/
class ContactTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'category_code',
        'address',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'web_page',
        'birthdate',
        'gender',
        'stratum',
        'point_sale_code',
        'latitude',
        'length',
        'seller_code',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority',
        'code_zone',
        'code_price_list',
        'percentage_discount',
        'credit_quota_customer',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment3',
        'contract_number',
        'last_purchase',
        'blocked',
        'observation',
        'image',
        'retention_source',
        'retention_iva',
        'retention_ica',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'seller',
        'contact',
        'percentage_commision',
        'replacement_days',
        'electronic_order',
        'update_shopping_list',
        'calculate_sale_prices',
        'percentage_retention_source',
        'percentage_retention_iva',
        'percentage_retention_iva_copy1',
        'code_ica',
        'payment_bank_code',
        'account_type',
        'account_number',
        'cost_center',
        'position',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactType::class;
    }
}
