<?php

namespace App\Repositories;

use App\Entities\VehicleType;
use InfyOm\Generator\Common\BaseRepository;

class VehicleTypeRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return VehicleType::class;
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function findAll(array $request)
    {
        return VehicleType::select('*')
            ->when(isset($request['filter']) && !empty($request['filter']), function ($filter) use ($request) {
                $filter->where('vehicle_type', 'ILIKE', '%' . $request['filter'] . '%')
                    ->orWhereHas('brandVehicle', function($brandVehicle) use ($request) {
                        $brandVehicle->where('name_parameter', 'ILIKE', '%' . $request['filter'] . '%');
                    });
            })
            ->take(30)
            ->get();
    }
}
