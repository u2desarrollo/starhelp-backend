<?php

namespace App\Repositories;

use App\Entities\Entity;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EntityRepository
 * @package App\Repositories
 * @version January 2, 2019, 8:42 pm UTC
 *
 * @method Entity findWithoutFail($id, $columns = ['*'])
 * @method Entity find($id, $columns = ['*'])
 * @method Entity first($columns = ['*'])
 */
class EntityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subscribers_id',
        'identification',
        'check_digit',
        'name',
        'surname',
        'is_customer',
        'is_provider',
        'is_employee',
        'state',
        'cities_id',
        'code_ciiu',
        'commercial_address',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'web_page',
        'birthdate',
        'gender',
        'stratum',
        'point_sale_code',
        'latitude',
        'length',
        'class_person',
        'taxpayer_type',
        'tax_regime',
        'declarant',
        'self_retainer',
        'category_code_customer',
        'seller_code',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority_customer',
        'code_zone_customer',
        'code_price_list_customer',
        'percentage_discount_customer',
        'credit_quota_customer',
        'expiration_date_credit_quota_customer',
        'days_soon_payment_customer_1',
        'percentage_soon_payment_customer_1',
        'days_soon_payment_customer_2',
        'percentage_soon_payment_customer_2',
        'days_soon_payment_customer_3',
        'percentage_soon_payment_customer_3',
        'contract_number_customer',
        'last_purchase_customer',
        'blocked_customer',
        'observation_customer',
        'image_customer',
        'retention_source_customer',
        'retention_iva_customer',
        'retention_ica_customer',
        'code_ica_customer',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'category_code_provider',
        'seller_provider',
        'contact_provider',
        'comments_provider',
        'percentage_commision_provider',
        'contract_number_provider',
        'code_price_list_provider',
        'priority_provider',
        'percentage_discount_provider',
        'credit_quota_provider',
        'expiration_date_credit_quota_provider',
        'days_soon_payment_provider_1',
        'percentage_soon_payment_provider_1',
        'days_soon_payment_provider_2',
        'percentage_soon_payment_provider_2',
        'days_soon_payment_provider_3',
        'percentage_soon_payment_provider_3',
        'replacement_days_provider',
        'electronic_order_provider',
        'update_shopping_list',
        'calculate_sale_prices_provider',
        'last_purchase_provider',
        'blocked_provider',
        'observation_provider',
        'image_provider',
        'retention_source_provider',
        'percentage_retention_source_provider',
        'retention_iva_provider',
        'percentage_retention_iva_provider',
        'retention_ica_provider',
        'code_ica_provider',
        'payment_bank_code',
        'account_type_provider',
        'account_number_provider',
        'code_category_employee',
        'cost_center_employee',
        'contract_number_employee',
        'percentage_commision_employee',
        'position_employee',
        'image_employee',
        'comments_employee'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Entity::class;
    }

    /**
     * Método para obtener el listado de entities de acuerdo a los parámetros recibidos
     * @param $request contiene los parámetros para filtrar
     * @return $entities contiene una collección de de entities
     */
    public function findAll($request)
    {
        if ($request->paginate == 'true') {
            $entities = Entity::where('identification', 'LIKE', '%' . $request->filter . '%')
                ->orWhere('name', 'LIKE', '%' . $request->filter . '%')
                ->orWhere('surname', 'LIKE', '%' . $request->filter . '%')
                ->with('warehouses')
                ->orderBy($request->sortBy, $request->sort)
                ->paginate($request->perPage);
        } else {
            $entities = Entity::all();
        }

        return $entities;
    }

}
