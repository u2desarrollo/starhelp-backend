<?php

namespace App\Repositories;

use App\Entities\Account;
use App\Entities\Parameter;
use Illuminate\Support\Arr;
use InfyOm\Generator\Common\BaseRepository;
use App\Entities\DocumentProduct;
use App\Entities\Document;
use App\Entities\DocumentTransactions;

/**
 * Class InventoryRepository
 * @package App\Repositories
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @method Inventory findWithoutFail($id, $columns = ['*'])
 * @method Inventory find($id, $columns = ['*'])
 * @method Inventory first($columns = ['*'])
 */
class AccountRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Account::class;
    }

    public function getAccountTotalize($account)
    {
        return Account :: select('id', 'code')
        ->where('code', '<', $account->code)
        ->whereRaw('LENGTH(code) < ?', [strlen($account->code)])
        ->orderBy('code', 'desc')
        ->first();
    }

    public function updateAccountTotalize($account_id_totalize, $account_id)
    {
        Account :: where('id', $account_id)
        ->update([
            'account_id_totalize' => $account_id_totalize
        ]);
    }


    public function createAccount($data)
    {
        return Account :: create($data);
    }

    public function updateAccount($account_id, $data)
    {
        $account_edited = Account :: where ('id', $account_id)->update($data);
        return $account_edited;
    }

    /**
     * Método para filtrar cuentas de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $cuentas colección de cuentas
     * @author santaigo torres
     */
    public function findAll($request){
    	if ($request->paginate == 'true') {

	        $accounts = Account::where(function($query) use ($request){
                    $query->where('code', 'ILIKE', '%' . $request->filter . '%');
                    $query->orWhere('name', 'ILIKE', '%' . $request->filter . '%');
                })
                ->where(function($query) use ($request){
                    if (isset($request->onlyAux)) {
                        if ($request->onlyAux  != 'false') {
                            $query->where('level', 24575);
                        }
                    }
                    if (isset($request->notAux)) {
                        if ($request->notAux != 'false') {
                            $query->where('level', '<>' ,24575);
                        }
                    }
                })
                ->whereHas('parameter_level', function ($query) use($request){
                    if (isset($request->level)) {
                        $query->where('code_parameter', $request->level);
                    }
                })
                ->with( [
                    'parameter_level'=> function ($query) use($request){
                        if (isset($request->level)) {
                            $query->where('code_parameter', $request->level);
                        }
                    },
                    'parameter_group',
                    'account_totalize:id,code,name'
                ])
	            ->orderBy($request->sortBy, $request->sort)
	            ->paginate($request->perPage);
    	}else{
    		$accounts = Account::orderBy('description', 'Asc')->get();
    	}

    	return $accounts;

    }

    public function validateCode($code)
    {
        $code_to_validate = Account::withTrashed()->where('code', $code)->first();
        if ($code_to_validate != null){
            throw new \Exception("El código " . $code. " ya existe");
        }
    }

    public function findAccountById($id)
    {
        $account = Account::where('id', $id)
        ->with(['user_modification'=>function($query){
            $query->select('id','name', 'surname');
        }])
        ->first();
        return (object) [
            'code' => '201',
            'completed' => true,
            'data' => (object) [
                'data' => $account,
                'message' => 'Exito'
            ]
        ];
    }
    
    public function deleteAccount($id)
    {
        $account_is_aux = Account::where('id', $id)
        ->with(['parameter_level'=>function ($query){
            $query->where('code_parameter', 0);
        }])
        ->whereHas('parameter_level',function ($query){
            $query->where('code_parameter', 0);
        })
        ->first();

        if (is_null($account_is_aux)) {
            return (object) [
                'code' => '500',
                'completed' => true,
                'data' => (object) [
                    'data' => '',
                    'message' => 'La Cuenta No Es Auxiliar'
                ]
            ];
        }else{
            $account_is_in_document_transactions = DocumentTransactions::where('account_id')->first();
            if (is_null($account_is_in_document_transactions)) {
                $account = Account::find($id);
                $account->delete();
                return (object) [
                    'code' => '200',
                    'completed' => true,
                    'data' => (object) [
                        'data' => '',
                        'message' => 'Se elimino la cuenta correctamente'
                    ]
                ];
            }else{
                return (object) [
                    'code' => '500',
                    'completed' => true,
                    'data' => (object) [
                        'data' => '',
                        'message' => 'La Cuenta está en transacciones'
                    ]
                ];
            }
        }
    }

    public function accountsFromRoot($account_root_id)
    {
        $account_root = Account::find($account_root_id);
        $accounts = Account::where('code', 'ILIKE', '%' . $account_root->code . '%')->where('level', 24575)->orderBy('code', 'asc')->get();
    	return $accounts;
    }

    public function accountsDefaultCrossBalances()
    {
        $accounts = Account::where('code', '130505')->first();
    	return $accounts;
    }
}
