<?php

namespace App\Repositories;

use App\Entities\PortfolioShipment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitCustomerRepository
 * @package App\Repositories
 * @version June 5, 2019, 5:22 pm UTC
 *
 * @method VisitCustomer findWithoutFail($id, $columns = ['*'])
 * @method VisitCustomer find($id, $columns = ['*'])
 * @method VisitCustomer first($columns = ['*'])
*/
class PortfolioShipmentRepository extends BaseRepository
{
	/**
	 * @var array
	 */


	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return PortfolioShipment::class;
	}



}
