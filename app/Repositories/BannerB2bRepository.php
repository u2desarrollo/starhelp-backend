<?php

namespace App\Repositories;

use App\Entities\BannerB2b;
use Illuminate\Http\Request;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BannerB2bRepository
 * @package App\Repositories
 * @version March 21, 2019, 2:15 pm UTC
 *
 * @method BannerB2b findWithoutFail($id, $columns = ['*'])
 * @method BannerB2b find($id, $columns = ['*'])
 * @method BannerB2b first($columns = ['*'])
*/
class BannerB2bRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'line_id',
		'parameter_id',
		'name',
		'big_banner',
		'small_banner',
		'since',
		'until',
		'state'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return BannerB2b::class;
	}

	public function findAll($request) {
        $banners = BannerB2b::where('name', 'ILIKE', '%'.$request->filter.'%')
            ->orderBy($request->sortBy, $request->sort)
            ->paginate($request->perPage);
		return $banners;
    }
}
