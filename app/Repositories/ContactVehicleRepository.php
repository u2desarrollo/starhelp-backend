<?php

namespace App\Repositories;

use App\Entities\ContactVehicle;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactVehicleRepository permite definir métodos para realizar las transacciones
 * necesarias entre el modelo ContactVehicle y la tabla de la base de datos contact_vehicles
 * @package App\Repositories
 * @author Jhon García
 */
class ContactVehicleRepository extends BaseRepository
{
    /**
     * @return string
     * @author Jhon García
     */
    public function model()
    {
        return ContactVehicle::class;
    }

    /**
     * Método que obtiene y retorna de la base de datos un listado de tipos de vehículo
     * @param array $request Contiene todos los posibles filtros a aplicar en la consulta
     * @return Collection listado de tipos de vehículo
     * @author Jhon García
     */
    public function findAll(array $request)
    {
        return ContactVehicle::select('id',
                'vehicle_type_id',
                'contact_id',
                'year',
                'license_plate',
                'serial_number',
                'average_daily_kilometers',
                'width',
                'rin',
                'profile')
            ->with([
                'contact' => function ($query) {
                    $query->select('id', 'identification', 'name', 'surname');
                }
            ])
            ->when(isset($request['license_plate']) && !empty($request['license_plate']), function ($license_plate) use ($request) {
                $license_plate->where('license_plate', $request['license_plate']);
            })
            ->when(isset($request['contact_id']) && !empty($request['contact_id']), function ($contact_id) use ($request) {
                $contact_id->where('contact_id', $request['contact_id']);
            })
            // ->where('contact_id', $request['contact_id'])
            ->get();

    }
}
