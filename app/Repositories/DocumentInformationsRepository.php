<?php

namespace App\Repositories;

use App\Entities\DocumentInformation;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentInformationsRepository
 * @package App\Repositories
 * @version April 25, 2019, 3:26 pm UTC
 *
 * @method DocumentInformations findWithoutFail($id, $columns = ['*'])
 * @method DocumentInformations find($id, $columns = ['*'])
 * @method DocumentInformations first($columns = ['*'])
*/
class DocumentInformationsRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'document_id',
		'mac_address',
		'money',
		'trm',
		'branchoffice_dispatch',
		'delivery_establishment',
		'delivery_place',
		'delivery_date',
		'delivery_days',
		'delivery_observation',
		'home_delivery',
		'domiciliary',
		'protractor',
		'guide_number',
		'registration_tag',
		'order_type',
		'cost_center',
		'bussiness_code',
		'price_list',
		'include_tax',
		'additional_contact',
		'additional_branchoffice',
		'additional_warehouse',
		'since',
		'until',
		'incorporated',
		'reversed',
		'modified',
		'anulled',
		'special_bearings',
		'printed'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return DocumentInformation::class;
	}

	/**
	 * Limpiar columnas accounting_error y accounting_description del documento
	 * @author Santiago Torres
	 * @param $document_id
	 */
	public function clearAccountingErrors($document_id)
	{
		DocumentInformation :: where('document_id', $document_id)->update([
			'accounting_error'=> false,
			'accounting_description'=> null
		]);
	}
	public function setAccountingError($document_id, $message)
	{
		DocumentInformation :: updateOrCreate(
			['document_id'=> $document_id],
			[
				'accounting_error' => true, 
				'accounting_description' => DB::raw("CONCAT(accounting_description,' $message')")
			]	
		);
	}
}