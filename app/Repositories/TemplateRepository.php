<?php

namespace App\Repositories;

use App\Entities\Template;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TemplatesRepository
 * @package App\Repositories
 * @version August 1, 2019, 10:41 pm UTC
 *
 * @method Templates findWithoutFail($id, $columns = ['*'])
 * @method Templates find($id, $columns = ['*'])
 * @method Templates first($columns = ['*'])
*/
class TemplateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'description',
        'operationType_id',
        'voucherType_id',
        'consecutive_handling',
        'return_model_id',
        'select_branchoffice',
        'select_warehouse',
        'select_cost_center',
        'seller_mode',
        'currency',
        'observation_tittle_1',
        'observation_tittle_2',
        'footnotes',
        'user_id',
        'select_conveyor',
        'capt_delivery_address',
        'capt_license',
        'final_function',

        'contact_capture',
        'contact_type',
        'contact_category',
        'contact_id',

        'basic_contact_information',
        'financial_info',
        'button_360',
        'send_contact_email',

        'doc_base_management',
        'voucher_type_id',
        'model_id',
        'see_backorder',

        'doc_cruce_management',
        'tittle',
        'valid_exist',
        'value_control',
        'quantity_control',
        'crossing_document_dates',

        'add_products',
        'price_list_id',
        'add_promotions_button',
        'recalc_iva_less_cost',
        'recalc_iva_base',
        'up_excel_button',
        'down_excel_button',

        'line_id',
        'see_location',
        'update_location',

        'modify_prices',
        'see_discount',
        'edit_discount',
        'see_bonus_amount',
        'edit_bonus_amount',
        'see_vrunit_base',
        'see_cant_backorder',
        'backorder_quantity_control',
        'see_physical_cant',
        'edit_physical_cant',
        'see_lots',
        'edit_lots',
        'edit_unit_value',
        'see_applied_promotion',
        'see_seller',
        'edit_seller',

        'see_product_observation',
        'edit_product_observation',
        'hide_value',
        'cents_values',
        'hands_free_operation',

        'discount_foot_bill',
        'retention_management',
        'handle_payment_methods',
        'payment_methods_allowed_id',
        'purchase_difference_management',

        'print',
        'print_template',
        'print_tittle',
        'product_order',
        'total_letters',
        'cash_drawer_opens',
        'edit_value_iva',
        'handle_payment_methods'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Template::class;
    }

    /**
     * Método para filtrar templates de acuerdo a los parametros recibidos
     * @param $request contiene los parametros del request
     * @return $templates colección de templates
     */
    public function findAll($request){
    	if ($request->paginate == 'true') {

	        $templates = Template::where('code', 'ILIKE', '%' . $request->filter . '%')
                ->orWhere('description', 'ILIKE', '%' . $request->filter . '%')
                ->with(['voucherType',
                    'voucherType1',
                    'currency',
                    'priceList',
                    'contactCategory',
                    'contacts',
                    'transactions'=>function($query){
                        $query->with(['accountAux']);
                        $query->orderBy('order');
                    },
                    'lines',
                    'users',
                    'modelo',
                    'modelo1',
                    'operationType'
                ])
	            ->orderBy($request->sortBy, $request->sort)
	            ->paginate($request->perPage);
    	}else{
    		$templates = Template::orderBy('description', 'Asc')->get();
    	}

    	return $templates;

    }

    public function findForCode($code)
    {
        return $templates = Template::select('code', 'id')->where('code', $code)->first();
    }

    public function findForId($id)
    {
        $templates = Template::where('id', $id)
            ->Orwhere('code', $id)
            ->with([
                'parameters',
                'users',
                'lines',
                'voucherType',
                'voucherType1',
                'operationType',
                'authorizedUsers',
                'transactions' => function ($query) {
                    $query->orderBy('order', 'ASC');
                    $query->with([
                        'accountAux',
                        'accountRoot',
                        'defaultContact',
                    ]);
                },
                'templateAccountings' => function ($query) {
                    $query->with([
                        'concept',
                        'according',
                        'account',
                        'template',
                    ])->orderBy('order_accounting');
                }
            ])->first();

        $templates = $templates->toArray();

        if (!is_null($templates['users'])) {
            for ($i = 0; $i < count($templates['users']); $i++) {
                $templates['user_id'][$i] = $templates['users'][$i]['id'];
            }
        }

        if (!is_null($templates['lines'])) {
            for ($i = 0; $i < count($templates['lines']); $i++) {
                $templates['line_id'][$i] = $templates['lines'][$i]['id'];
            }
        }

        if (!is_null($templates['parameters'])) {
            $templates['payment_methods_allowed_id'] = [];
            for ($i = 0; $i < count($templates['parameters']); $i++) {
                $templates['payment_methods_allowed_id'][$i] = $templates['parameters'][$i]['id'];
            }
        }

        unset($templates['parameters']);
        unset($templates['users']);
        unset($templates['lines']);
        return $templates;
    }

    public function createTemplate(array $input) {
		try{
			DB::beginTransaction();

			// Crear carrito en la base de datos
            $template = Template::create($input);
           /*  $template->lines()->attach($input['line_id']);
            $template->users()->attach($input['user_id']);
 */

			DB::commit();
			// Retornar los datos del carrito recién creado
			return $template;
		}catch(\Exception $e){
            DB::rollBack();
            return $e->getMessage();
		}
    }



    public function update(array $input , $id )
    {
        $template = Template::findOrFail($id);
        $template->update(Arr::except($input,['payment_methods_allowed_id','line_id','user_id']));
        $template->lines()->sync($input['line_id']);
        $template->users()->sync($input['user_id']);
        $template->parameters()->sync($input['payment_methods_allowed_id']);
        return 'modelo actualizado correctamente';
    }

}
