<?php

namespace App\Repositories;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Entities\Parameter;

use Exception;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

use Illuminate\Support\Facades\DB;
use Excel;
use Illuminate\Support\Arr;

/**
 * Class ContactWarehouseRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:27 pm UTC
 *
 * @method ContactWarehouse findWithoutFail($id, $columns = ['*'])
 * @method ContactWarehouse find($id, $columns = ['*'])
 * @method ContactWarehouse first($columns = ['*'])
 */
class ContactWarehouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'code',
        'description',
        'address',
        'latitude',
        'length',
        'telephone',
        'email',
        'contact',
        'observation',
        'state'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactWarehouse::class;
    }

    public function findCustomersByType($request, $user)
    {

        $is_seller = false;

        foreach ($user->getRoles as $role) {
            if (isset($role->role) && strtoupper($role->role) == 'VENDEDOR') {
                $is_seller = true;
                break;
            }
        }

        if ($request->paginate == 'true') {
            $type = $request->employee_type;
            if (!isset($request->ide)) {

                $customers = ContactWarehouse::select(
                    'id',
                    'description',
                    'code',
                    'credit_quota',
                    'price_list_id',
                    'contact_id',
                    'seller_id',
                    'pay_condition',
                    'send_point',
                    'telephone',
                    'address',
                    'minimum_order_value',
                    'branchoffice_id',
                    'discount_percentage'
                )
                    ->when($is_seller, function ($query) use ($user) {
                        $query->where('seller_id', $user->contact_id);
                    })
                    ->where('state', true)
                    ->with([
                        'branchoffice',
                        'contact',
                        'contact.customer',
                        'contact.employee',
                        'contact.provider',
                        'seller',
                        'seller.employee',
                        'line',
                        'line.line'
                    ])
                    ->whereHas('contact', function ($query) use($request) {
                        $query->whereHas('customer', function ($query) use($request) {
                            $query->where('category_id', '!=', '24473');
                            $query->orWhere('category_id', null);
                            if ($request->is_customer) {
                                $query->where('is_customer', $request->is_customer);
                                $query->where('is_provider', false);
                                $query->where('is_employee', false);
                                $query->where('is_other', false);
                            }
                        });
                    });

                // Validamos el filtro de branchoffice_id
                if ($request->branchoffice_id) {
                    $customers->where('branchoffice_id', $request->branchoffice_id);
                }


                // Validamos el filtro de busqueda
                if (!empty($request->filter)) {
                    $array_filter = explode(' ', $request->filter);

                    foreach ($array_filter as $key => $filter) {
                        $customers->where(function ($query) use ($filter) {

                            $query->where('description', 'ilike', '%' . $filter . '%');

                            $query->orWhereHas('contact', function ($query) use ($filter) {
                                $query->where('identification', 'ilike', '%' . $filter . '%');
                            });

                            $query->orWhereHas('contact', function ($query) use ($filter) {
                                $query->where('name', 'ilike', '%' . $filter . '%');
                            });

                            $query->orWhereHas('contact', function ($query) use ($filter) {
                                $query->where('surname', 'ilike', '%' . $filter . '%');
                            });
                        });
                    }
                }

                // Traemos la informacion
                $customers = $customers->orderBy('description', 'ASC')->paginate(30);

                foreach ($customers as $key => $value) {
                    if ($value->contact->tradename != '' || $value->contact->tradename != null) {
                        $part = explode('/', $value->contact->tradename);
                        $value->contact->tradename = count($part) == 2 ? ' / ' . $part[1] : ' / ' . $part[0];
                    }
                }
                //dd($customers);
            }
        } else {
            $customers = ContactWarehouse::select('contacts_warehouses.id', 'description', 'credit_quota', 'price_list_id', 'contact_id', 'seller_id', 'pay_condition', 'send_point', 'telephone', 'minimum_order_value', 'branchoffice_id')
                ->where('state', true)
                ->where('contacts_warehouses.id', $request->ide)
                ->with([
                    'branchoffice',
                    'contact' => function ($query) {
                        $query->with(['customer', 'employee', 'provider']);
                    },
                    'seller' => function ($query) {
                        $query->with(['employee']);
                    },
                    'line' => function ($query) {
                        $query->with(['line']);
                    }
                ])
                ->first();
        }

        return $customers;
    }

    public function findCustomersNotPaginate($request, $user)
    {

        $customers = ContactWarehouse::select(
            'id',
            'description',
            'code',
            'credit_quota',
            'price_list_id',
            'contact_id',
            'seller_id',
            'pay_condition',
            'send_point',
            'telephone',
            'address',
            'minimum_order_value',
            'branchoffice_id'
        )
            ->where('state', true)
            ->with([
                'branchoffice',
                'contact' => function ($query) {
                    $query->with(['customer', 'employee', 'provider']);
                },
                'seller' => function ($query) {
                    $query->with(['employee']);
                },
                'line' => function ($query) {
                    $query->with(['line']);
                }
            ]);

        // Validamos el filtro de branchoffice_id
        if ($request->branchoffice_id) {
            $customers->where('branchoffice_id', $request->branchoffice_id);
        }

        // Validamos el filtro de busqueda
        if (!empty($request->filter)) {
            $array_filter = explode(' ', $request->filter);

            foreach ($array_filter as $key => $filter) {
                $customers->where(function ($query) use ($filter) {

                    $query->where('description', 'ilike', '%' . $filter . '%');

                    $query->orWhereHas('contact', function ($query) use ($filter) {
                        $query->where('identification', 'ilike', '%' . $filter . '%');
                    });

                    $query->orWhereHas('contact', function ($query) use ($filter) {
                        $query->where('name', 'ilike', '%' . $filter . '%');
                    });

                    $query->orWhereHas('contact', function ($query) use ($filter) {
                        $query->where('surname', 'ilike', '%' . $filter . '%');
                    });
                });
            }
        }

        $customers = $customers->orderBy('description', 'ASC')->take(5)->get();

        return $customers;
    }

    public function uploadExcel($request)
    {
        try {
            if ($request->file('sucursales')) {
                $file = $request->file('sucursales');

                if ($file->isValid()) {
                    $registros = Excel::load($file->getRealPath(), function ($reader) {
                        DB::beginTransaction();

                        foreach ($reader->toArray() as $key => $row) {
                            $fila = [];

                            foreach ($row as $k => $v) {
                                $fila[$k] = trim($v);
                            }

                            $sucursal = (isset($fila['sucursal']) && strlen(trim($fila['sucursal'])) > 0 ? trim($fila['sucursal']) : 0);
                            $identification = (isset($fila['tercero']) && strlen(trim($fila['tercero'])) > 0 ? trim($fila['tercero']) : "");
                            $nombre = (isset($fila['nombre']) && strlen(trim($fila['nombre'])) > 0 ? trim($fila['nombre']) : "");
                            $direccion = (isset($fila['direccion']) && strlen(trim($fila['direccion'])) > 0 ? trim($fila['direccion']) : "");
                            $telefono_1 = (isset($fila['telefono_1']) && strlen(trim($fila['telefono_1'])) > 0 ? trim($fila['telefono_1']) : 0);
                            $telefono_2 = (isset($fila['telefono_2']) && strlen(trim($fila['telefono_2'])) > 0 ? trim($fila['telefono_2']) : 0);
                            $celular = (isset($fila['telefono_3']) && strlen(trim($fila['telefono_3'])) > 0 ? trim($fila['telefono_3']) : 0);
                            $email = (isset($fila['email']) && strlen(trim($fila['email'])) > 0 ? trim($fila['email']) : "");
                            $observaciones = (isset($fila['observaciones']) && strlen(trim($fila['observaciones'])) > 0 ? trim($fila['observaciones']) : "");
                            $credit_quota = (isset($fila['cupo_credito']) && strlen(trim($fila['cupo_credito'])) > 0 ? floatval($fila['cupo_credito']) : 0);
                            $forma_de_pago = (isset($fila['forma_de_pago']) && strlen(trim($fila['forma_de_pago'])) > 0 ? trim($fila['forma_de_pago']) : "");

                            // dd($fila);


                            $cliente = Contact::where('identification', $identification)->first();

                            if (!$cliente) {
                                $identification_type = (isset($row['tipo_documento']) && strlen(trim($row['tipo_documento'])) > 0 ? trim($fila['tipo_documento']) : 1);
                                $identification_type = mb_strtoupper($identification_type, 'UTF-8') == "NIT" ? 1 : 2;
                                $check_digit = (isset($row['dig_verificacion']) && strlen(trim($row['dig_verificacion'])) > 0 ? trim($row['dig_verificacion']) : "");
                                $city_id = "148";

                                $datos = [
                                    'subscriber_id' => 1,
                                    'identification_type' => $identification_type,
                                    'identification' => $identification,
                                    'check_digit' => $check_digit,
                                    'name' => $nombre,
                                    'surname' => ".",
                                    'address' => $direccion,
                                    'main_telephone' => $telefono_1,
                                    'cell_phone' => $celular,
                                    'city_id' => $city_id,
                                    'is_customer' => 1,
                                    'is_provider' => 0,
                                    'is_employee' => 0,
                                    'class_person' => 8,
                                    'taxpayer_type' => 13,
                                    'tax_regime' => 11,
                                    'declarant' => 0,
                                    'self_retainer' => 0,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ];

                                $contact = Contact::create($datos);
                            } else {
                                $arrWarehouse = [
                                    'contact_id' => $cliente->id,
                                    'code' => $identification,
                                    'description' => substr($nombre, 0, 100),
                                    'state' => 1,
                                    'address' => substr($direccion, 0, 70),
                                    'telephone' => substr($telefono_1, 0, 10),
                                    'telephone2' => substr($telefono_2, 0, 10),
                                    'cellphone' => substr($celular, 0, 10),
                                    'email' => mb_strtolower($email, 'UTF-8'),
                                    'observation' => $observaciones,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                    'seller_id' => $cliente->id,
                                    'price_list_id' => '271',
                                    'credit_quota' => $credit_quota,
                                    'pay_condition' => $forma_de_pago
                                ];

                                $warehouse = ContactWarehouse::create($arrWarehouse);

                                if ($warehouse) {
                                    DB::commit();
                                }
                            }
                        }
                    })->toArray();

                    if (count($registros) > 0) {
                        return response()->json(['message' => "Warehouses imported successfully"], 200);
                    } else {
                        throw new Exception("There were no imported warehouses", 400);
                    }
                } else {
                    throw new Exception("The requested file is not valid", 400);
                }
            } else {
                throw new Exception("The requested file does not exist", 404);
            }
        } catch (Exception  $e) {
            $codigo = (in_array($e->getCode(), [400, 200, 404, 500]) ? $e->getCode() : 401);

            return response()->json(['error' => $e->getMessage()], $codigo);
        }
    }

    public function slug($text, $strict = false)
    {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.\s]+~u', '-', $text);

        // trim
        $text = trim($text, '-');
        setlocale(LC_CTYPE, 'en_GB.utf8');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', ' ', $text);
        if (empty($text)) {
            return 'empty_$';
        }
        if ($strict) {
            $text = str_replace(".", " ", $text);
        }
        return mb_strtoupper($text, 'UTF-8');
    }


    public function searchBiId($id)
    {
        // $contactW = ContactWarehouse::where('id', $id)->get();
        $contactW = ContactWarehouse::find($id);
        return $contactW;
    }

    public function getInfoForFile()
    {
        $contact = ContactWarehouse::wherehas('contactt', function ($query) {
            $query->where(function($query){
                $query->where('erp_id', '');
                $query->orWhereNull('erp_id');
            });
        })
            ->with([
                'contactt' => function ($query) {
                    $query->where('erp_id', null);
                    $query->with([
                        'identificationt',
                        'city',
                        'customer',
                        'provider',
                    ]);
                },
                'city',
                'paymentType',
                'zone',
                'branchoffice',
                'typesNegotiationPortfolio_',
                'seller'
            ])
            ->take(1000)
            ->get();

        $contact->map(function ($item, $key) {

            $item->contactt->customer_retention_source  =   is_null($item->contactt->customer) ? 'NO' : ($item->contactt->customer->retention_source != null ? 'SI' : 'NO');
            $item->contactt->customer_retention_iva     =   is_null($item->contactt->customer) ? 'NO' : ($item->contactt->customer->retention_iva != null ? 'SI' : 'NO');
            $item->contactt->customer_retention_ica     =   is_null($item->contactt->customer) ? 'NO' : ($item->contactt->customer->retention_ica != null ? 'SI' : 'NO');

            $item->contactt->provider_retention_source  =   is_null($item->contactt->provider) ? 'NO' : ($item->contactt->provider->retention_source != null ? 'SI' : 'NO');
            $item->contactt->provider_retention_iva     =   is_null($item->contactt->provider) ? 'NO' : ($item->contactt->provider->retention_iva != null ? 'SI' : 'NO');
            $item->contactt->provider_retention_ica     =   is_null($item->contactt->provider) ? 'NO' : ($item->contactt->provider->retention_ica != null ? 'SI' : 'NO');

            $item->code_suc_city = $item->city == null ? null : $item->city->department_id . $item->city->city_code;
            $item->code_desc_city = $item->city == null ? null : $item->city->city;
            $item->code_branchoffice = $item->branchoffice == null ? null : $item->branchoffice->code;
            $item->code_zone = $item->zone == null ? null : $item->zone->code_parameter;

            $item->contactt->code_categ_prov    = is_null($item->contactt->provider) ? null : $item->contactt->provider->category_id;
            $item->contactt->code_categ_cli     = is_null($item->contactt->customer) ? null : $item->contactt->customer->category_id;

            $item->contactt->_is_customer = $item->contactt->is_customer == 1 ? 'SI' : 'NO';
            $item->contactt->_is_provider = $item->contactt->is_provider == 1 ? 'SI' : 'NO';
            $item->contactt->_is_provider = $item->contactt->is_provider == 1 ? 'SI' : 'NO';

            $item->contactt->code_parameter = $item->contactt->identificationt != null ? $item->contactt->identificationt->code_parameter : null;
            $item->contactt->city_code = $item->contactt->city != null ? $item->contactt->city->city_code : null;


            $item->code_suc_city = $item->city == null ? null : ($item->warehouse ? $item->city->department_id . '' . $item->warehouse->city->city_code : '');
            $departement_id = $item->city == null ? null : ($item->city->department_id < '10' ? '0' . $item->city->department_id : $item->city->department_id);
            $item->code_suc_city = $item->city == null ? null : $departement_id . '' . $item->city->city_code;

            $item->payment_type =  $item->paymentType == null ? null : $item->paymentType->name_parameter;
            $item->typesNegotiation =  $item->typesNegotiationPortfolio_ == null ? null : $item->typesNegotiationPortfolio_->name_parameter;

            $item->seller_ident = $item->seller == null ? null : $item->seller->identification;
            $item->des_ident = $item->seller == null ? null : $item->seller->name . ' ' . $item->seller->surname;

            $fiscal_responsibility_code = Parameter :: find($item->contactt->fiscal_responsibility_id);
            $item->contactt->fiscal_responsibility_code = $fiscal_responsibility_code ? $fiscal_responsibility_code->code_parameter : null;
        });

        return $contact;
    }

    public function update(array $attributes, $id)
    {
        return ContactWarehouse::where('id', $id)->update(Arr::only($attributes, [
            'code', 'description', 'address', 'latitude', 'telephone', 'email', 'seller_id', 'observation', 'state', 'length', 'city_id', 'branchoffice_id', 'creation_date',
            'other_telephones',
            'payment_type_id',
            'discount_percentage',
            'zone_id',
            'types_negotiation_portfolio',
            'zone_despatch_id'
        ]));
    }

    /**
     * actualizar correo
     *
     * @author santiago torres | kevin galindo
     */
    public function updateeMail($request)
    {
        $contact = ContactWarehouse::where('contact_id', $request->contact)->first();

        $contact->email = $request->to;
        $contact->save();

        return $contact;
    }

    /**
     * lista os provedores
     *
     * @author santiago torres | kevin galindo
     */

    public function searchProviders($request)
    {
        return ContactWarehouse::with(['contactt' => function ($query) {
            $query->where('is_provider', true);
        }])
            ->where('description', 'ilike', '%' . $request->filter . '%')
            ->get();
    }

    /**
     * Valida que la sucursal no tenga documentos asociados para eliminarla
     * @param $id
     * @return bool[]
     * @throws Exception
     */
    public function delete($id)
    {
        $contact = ContactWarehouse::with('allDocuments')->find($id);

        if ($contact) {
            if ($contact->allDocuments->count() > 0) {
                return ['status' => false, 'message' => 'No es posible eliminar debido a que la sucursal tiene documentos asociados'];
            }
        } else {
            return ['status' => false, 'message' => 'No existe la sucursal'];
        }

        $contact->delete();

        return ['status' => true];

    }

    public function create(array $input)
    {
        return ContactWarehouse::create(Arr::only($input, [
            'contact_id',
            'code',
            'description',
            'address',
            'latitude',
            'length',
            'telephone',
            'telephone2',
            'cellphone',
            'email',
            'observation',
            'state',
            'seller_id',
            'price_list_id',
            'credit_quota',
            'erp_id',
            'pay_condition',
            'send_point',
            'picture',
            'ret_fue_prod_vr_limit',
            'ret_fue_prod_percentage',
            'ret_fue_serv_vr_limit',
            'ret_fue_serv_percentage',
            'ret_iva_prod_vr_limit',
            'ret_iva_prod_percentage',
            'ret_iva_serv_vr_limit',
            'ret_iva_serv_percentage',
            'ret_ica_prod_vr_limit',
            'ret_ica_prod_percentage',
            'ret_ica_serv_vr_limit',
            'ret_ica_serv_percentage',
            'branchoffice_id',
            'creation_date',
            'other_telephones',
            'payment_type_id',
            'discount_percentage',
            'zone_id',
            'zone_despatch_id'
        ]));
    }


    public function getContactWarehouse($request)
    {
        $contactWarehouses = ContactWarehouse::select('id', 'code', 'description')
            ->where('description', 'ILIKE', '%'.$request->filter.'%')
            ->limit(100);
        if (isset($request->contact_id)) {
            if (!is_null($request->contact_id)) {
                $contactWarehouses->where('contact_id', $request->contact_id);
            }
        }
        // $contactWarehouses->get();
        return $contactWarehouses->get();
    }
}
