<?php

namespace App\Repositories;

use App\Entities\RelatedProduct;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RelatedProductRepository
 * @package App\Repositories
 * @version March 29, 2019, 4:01 pm UTC
 *
 * @method RelatedProduct findWithoutFail($id, $columns = ['*'])
 * @method RelatedProduct find($id, $columns = ['*'])
 * @method RelatedProduct first($columns = ['*'])
*/
class RelatedProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'product_related_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RelatedProduct::class;
    }
}
