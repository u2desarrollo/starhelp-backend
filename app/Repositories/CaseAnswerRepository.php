<?php

namespace App\Repositories;

use App\Entities\CaseAnswer;
use InfyOm\Generator\Common\BaseRepository;

class CaseAnswerRepository extends BaseRepository
{

    public function model()
    {
        return CaseAnswer::class;
    }
}
