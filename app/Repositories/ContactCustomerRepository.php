<?php

namespace App\Repositories;

use App\Entities\ContactCustomer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactCustomerRepository
 * @package App\Repositories
 * @version January 15, 2019, 9:26 pm UTC
 *
 * @method ContactCustomer findWithoutFail($id, $columns = ['*'])
 * @method ContactCustomer find($id, $columns = ['*'])
 * @method ContactCustomer first($columns = ['*'])
*/
class ContactCustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'category_id',
        'seller_id',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority',
        'price_list_id',
        'zone_id',
        'percentage_discount',
        'credit_quota',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment3',
        'last_purchase',
        'blocked',
        'observation',
        'logo',
        'retention_source',
        'retention_iva',
        'retention_ica',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'code_ica_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactCustomer::class;
    }
    
    public function getContactCategory($id)
    {
        return ContactCustomer::select('id', 'category_id')->where('id', $id)->first();
    }
}
