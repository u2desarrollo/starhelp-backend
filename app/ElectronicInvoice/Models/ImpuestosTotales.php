<?php


namespace App\ElectronicInvoice\Models;

/**
 * Class ImpuestosTotales
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class ImpuestosTotales
{
    public $codigoTOTALImp;
    public $montoTotal;

    /**
     * ImpuestosTotales constructor.
     * @param $impuestosTotales
     * @author Jhon Garcia
     */
    public function __construct($impuestosTotales)
    {
        $this->setCodigoTOTALImp($impuestosTotales->tax_id);

        $this->setMontoTotal($impuestosTotales->value);
    }

    /**
     * @param mixed $codigoTOTALImp
     * @author Jhon Garcia
     */
    public function setCodigoTOTALImp($codigoTOTALImp): void
    {
        $this->codigoTOTALImp = $this->comparaCodigoTOTALImp($codigoTOTALImp);
    }

    /**
     * @param $codigoTOTALImp
     * @return mixed
     * @author Jhon García
     */
    private function comparaCodigoTOTALImp($codigoTOTALImp)
    {

        /**
         * TIPOS DE IMPUESTO
         *
         * '01' => 'IVA''
         *  02' => 'IC'
         * '03' => 'ICA'
         * '04' => 'INC'
         * '05' => 'Retención sobre el IVA'
         * '06' => 'Retención sobre fuente por renta'
         * '07' => 'Retención sobre el ICA'
         * '20' => 'FtoHorticultura'
         * '21' => 'Timbre'
         * '22' => 'Bolsas'
         * '23' => 'INCarbono'
         * '24' => 'INCombustibles'
         * '25' => 'Sobretasa Combustibles'
         * '26' => 'Sordicom'
         * 'ZY' => 'No causa'
         * 'ZZ' => 'Otros tributos, tasas, contribuciones, y similares'
         */

        $codes = [
            0 => '00',
            1 => '01',
            2 => '05',
            3 => '06',
            4 => '02',
            5 => '07',
        ];

        if (is_null($codigoTOTALImp) || !isset($codes[$codigoTOTALImp])){
            return $codes[0];
        }

        return $codes[$codigoTOTALImp];

    }

    /**
     * @param mixed $montoTotal
     * @author Jhon Garcia
     */
    public function setMontoTotal($montoTotal): void
    {
        $this->montoTotal = strval(number_format(round($montoTotal,2), 2, ".", ""));
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getCodigoTOTALImp()
    {
        return doubleval($this->codigoTOTALImp);
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getMontoTotal()
    {
        return doubleval($this->montoTotal);
    }

}
