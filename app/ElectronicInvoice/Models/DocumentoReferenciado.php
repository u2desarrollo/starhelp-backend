<?php


namespace App\ElectronicInvoice\Models;

use Carbon\Carbon;

/**
 * Class DocumentoReferenciado
 * @package App\ElectronicInvoice\Models
 * @author Jhon García
 */
class DocumentoReferenciado
{
    public $codigoEstatusDocumento;
    public $codigoInterno;
    public $cufeDocReferenciado;
    public $descripcion = Array();
    public $fecha;
    public $fechaFinValidez;
    public $fechaInicioValidez;
    public $numeroDocumento;
    public $tipoCUFE;
    public $tipoDocumento;
    public $tipoDocumentoCodigo;

    /**
     * DocumentoReferenciado constructor.
     * @param $documento_referenciado
     * @param $codigoInterno
     * @author Jhon García
     */
    public function __construct($documento_referenciado, $codigoInterno)
    {
        $this->setCodigoEstatusDocumento(6);

        $this->setCodigoInterno($codigoInterno);

        $this->setCufeDocReferenciado($documento_referenciado->cufe);

        $this->setFecha($documento_referenciado->document_date);

        $this->setNumeroDocumento($documento_referenciado);
    }

    /**
     * @param mixed $codigoEstatusDocumento
     * @author Jhon García
     */
    public function setCodigoEstatusDocumento($codigoEstatusDocumento): void
    {
        $this->codigoEstatusDocumento = $codigoEstatusDocumento;
    }

    /**
     * @param mixed $codigoInterno
     * @author Jhon García
     */
    public function setCodigoInterno($codigoInterno): void
    {
        $this->codigoInterno = $codigoInterno;
    }

    /**
     * @param mixed $cufeDocReferenciado
     * @author Jhon García
     */
    public function setCufeDocReferenciado($cufeDocReferenciado): void
    {
        $this->cufeDocReferenciado = $cufeDocReferenciado;
    }

    /**
     * @param array $descripcion
     * @author Jhon García
     */
    public function setDescripcion(array $descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @param mixed $fecha
     * @author Jhon García
     */
    public function setFecha($fecha): void
    {
        $this->fecha = Carbon::parse($fecha)->format('Y-m-d');;
    }

    /**
     * @param mixed $fechaFinValidez
     * @author Jhon García
     */
    public function setFechaFinValidez($fechaFinValidez): void
    {
        $this->fechaFinValidez = $fechaFinValidez;
    }

    /**
     * @param mixed $fechaInicioValidez
     * @author Jhon García
     */
    public function setFechaInicioValidez($fechaInicioValidez): void
    {
        $this->fechaInicioValidez = $fechaInicioValidez;
    }

    /**
     * @param $documento_referenciado
     * @author Jhon García
     */
    public function setNumeroDocumento($documento_referenciado): void
    {
        $this->numeroDocumento = $documento_referenciado->vouchertype->prefix . $documento_referenciado->consecutive;
    }

    /**
     * @param mixed $tipoCUFE
     * @author Jhon García
     */
    public function setTipoCUFE($tipoCUFE): void
    {
        $this->tipoCUFE = $tipoCUFE;
    }

    /**
     * @param mixed $tipoDocumento
     * @author Jhon García
     */
    public function setTipoDocumento($tipoDocumento): void
    {
        $this->tipoDocumento = $tipoDocumento;
    }

    /**
     * @param mixed $tipoDocumentoCodigo
     * @author Jhon García
     */
    public function setTipoDocumentoCodigo($tipoDocumentoCodigo): void
    {
        $this->tipoDocumentoCodigo = $tipoDocumentoCodigo;
    }

}
