<?php

namespace App\ElectronicInvoice\Models;

use App\Entities\Department;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class Direccion
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class Direccion
{
    public $aCuidadoDe;
    public $aLaAtencionDe;
    public $bloque;
    public $buzon;
    public $calle;
    public $calleAdicional;
    public $ciudad;
    public $codigoDepartamento;
    public $correccionHusoHorario;
    public $departamento;
    public $departamentoOrg;
    public $direccion;
    public $distrito;
    public $habitacion;
    public $lenguaje;
    public $municipio;
    public $nombreEdificio;
    public $numeroParcela;
    public $pais;
    public $piso;
    public $region;
    public $subDivision;
    public $ubicacion;
    public $zonaPostal;

    /**
     * Metodo constructor
     * Direccion constructor.
     * @param $contact
     * @param $onlyCountryAndAddress
     * @throws Exception
     * @author Jhon Garcia
     */
    public function __construct($contact, $onlyCountryAndAddress)
    {
        $this->setPais($contact->country_code);

        $this->setDireccion($contact->address);

        if (!$onlyCountryAndAddress) {

            if (!is_null($contact->city)) {

                $this->setCiudad($contact);

                $this->setCodigoDepartamento($contact);

                $this->setDepartamento($contact);

                $this->setLenguaje("es");

                $this->setMunicipio($contact);

            }else{
                throw new Exception('El tercero no tiene asociada una Ciudad');
            }
        }
    }

    /**
     * @param mixed $aCuidadoDe
     * @author Jhon Garcia
     */
    public function setACuidadoDe($aCuidadoDe): void
    {
        $this->aCuidadoDe = $aCuidadoDe;
    }

    /**
     * @param mixed $aLaAtencionDe
     * @author Jhon Garcia
     */
    public function setALaAtencionDe($aLaAtencionDe): void
    {
        $this->aLaAtencionDe = $aLaAtencionDe;
    }

    /**
     * @param mixed $bloque
     * @author Jhon Garcia
     */
    public function setBloque($bloque): void
    {
        $this->bloque = $bloque;
    }

    /**
     * @param mixed $buzon
     * @author Jhon Garcia
     */
    public function setBuzon($buzon): void
    {
        $this->buzon = $buzon;
    }

    /**
     * @param mixed $calle
     * @author Jhon Garcia
     */
    public function setCalle($calle): void
    {
        $this->calle = $calle;
    }

    /**
     * @param mixed $calleAdicional
     * @author Jhon Garcia
     */
    public function setCalleAdicional($calleAdicional): void
    {
        $this->calleAdicional = $calleAdicional;
    }

    /**
     * @param $contact
     * @throws Exception
     * @author Jhon Garcia
     */
    public function setCiudad($contact): void
    {
        if (is_null($contact->country_code) || $contact->country_code == 'CO') {
            if (is_null($contact->city)) {
                throw new Exception('El tercero no tiene asociada una Ciudad');
            } else {
                $this->ciudad = $contact->city->city;
            }
        } else if (!is_null($contact->city)) {
            $this->ciudad = $contact->city->city;
        }
    }

    /**
     * @param $contact
     * @author Jhon Garcia
     */
    public function setCodigoDepartamento($contact): void
    {
        if (!is_null($contact->city)) {
            $this->codigoDepartamento = str_pad($contact->city->department_id, '2', '0', STR_PAD_LEFT);
        }
    }

    /**
     * @param mixed $correccionHusoHorario
     * @author Jhon Garcia
     */
    public function setCorreccionHusoHorario($correccionHusoHorario): void
    {
        $this->correccionHusoHorario = $correccionHusoHorario;
    }

    /**
     * @param mixed $contact
     * @author Jhon Garcia
     */
    public function setDepartamento($contact): void
    {
        if (!is_null($contact->city)) {

            $department_id = str_pad($contact->city->department_id, '2', '0', STR_PAD_LEFT);

            $department = Department::where('id', $department_id)->first();

            if ($department) {
                $this->departamento = $department->department;
            }
        }
    }

    /**
     * @param mixed $contact
     * @author Jhon Garcia
     */
    public function setDepartamentoOrg($contact): void
    {

    }

    /**
     * @param mixed $direccion
     * @author Jhon Garcia
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    /**
     * @param mixed $distrito
     * @author Jhon Garcia
     */
    public function setDistrito($distrito): void
    {
        $this->distrito = $distrito;
    }

    /**
     * @param mixed $habitacion
     * @author Jhon Garcia
     */
    public function setHabitacion($habitacion): void
    {
        $this->habitacion = $habitacion;
    }

    /**
     * @param mixed $lenguaje
     * @author Jhon Garcia
     */
    public function setLenguaje($lenguaje): void
    {
        $this->lenguaje = $lenguaje;
    }

    /**
     * @param $contact
     * @author Jhon Garcia
     */
    public function setMunicipio($contact): void
    {
        if (!is_null($contact->city)) {
            $this->municipio = str_pad($contact->city->department_id, '2', '0', STR_PAD_LEFT) . $contact->city->city_code;
        }
    }

    /**
     * @param mixed $nombreEdificio
     * @author Jhon Garcia
     */
    public function setNombreEdificio($nombreEdificio): void
    {
        $this->nombreEdificio = $nombreEdificio;
    }

    /**
     * @param mixed $numeroParcela
     * @author Jhon Garcia
     */
    public function setNumeroParcela($numeroParcela): void
    {
        $this->numeroParcela = $numeroParcela;
    }

    /**
     * @param mixed $pais
     * @author Jhon Garcia
     */
    public function setPais($pais): void
    {
        if (is_null($pais)) {
            $this->pais = 'CO';
        } else {
            $this->pais = $pais;
        }
    }

    /**
     * @param mixed $piso
     * @author Jhon Garcia
     */
    public function setPiso($piso): void
    {
        $this->piso = $piso;
    }

    /**
     * @param mixed $region
     * @author Jhon Garcia
     */
    public function setRegion($region): void
    {
        $this->region = $region;
    }

    /**
     * @param mixed $subDivision
     * @author Jhon Garcia
     */
    public function setSubDivision($subDivision): void
    {
        $this->subDivision = $subDivision;
    }

    /**
     * @param mixed $ubicacion
     * @author Jhon Garcia
     */
    public function setUbicacion($ubicacion): void
    {
        $this->ubicacion = $ubicacion;
    }

    /**
     * @param mixed $zonaPostal
     * @author Jhon Garcia
     */
    public function setZonaPostal($zonaPostal): void
    {
        $this->zonaPostal = $zonaPostal;
    }

}
