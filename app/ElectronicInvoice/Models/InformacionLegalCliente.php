<?php


namespace App\ElectronicInvoice\Models;


use Exception;

class InformacionLegalCliente
{
    public $codigoEstablecimiento;
    public $nombreRegistroRUT;
    public $numeroIdentificacion;
    public $numeroIdentificacionDV;
    public $tipoIdentificacion;

    /**
     * Metodo constructor
     * InformacionLegalCliente constructor.
     * @param $contact
     * @param $warehouse
     * @author Jhon Garcia
     */
    public function __construct($contact, $warehouse)
    {
        $this->setCodigoEstablecimiento($warehouse->code);

        $this->setNombreRegistroRUT($contact);

        $this->setNumeroIdentificacion($contact->identification);

        $this->setNumeroIdentificacionDV($contact->check_digit);

        $this->setTipoIdentificacion($contact->identificationt);
    }

    /**
     * @param mixed $codigoEstablecimiento
     * @author Jhon Garcia
     */
    public function setCodigoEstablecimiento($codigoEstablecimiento): void
    {
        $this->codigoEstablecimiento = $codigoEstablecimiento;
    }

    /**
     * @param mixed $tercero
     * @author Jhon Garcia
     */
    public function setNombreRegistroRUT($tercero): void
    {
        $this->nombreRegistroRUT = $tercero->name . ' ' . (string) $tercero->surname;
    }

    /**
     * @param mixed $numeroIdentificacion
     * @author Jhon Garcia
     */
    public function setNumeroIdentificacion($numeroIdentificacion): void
    {
        $this->numeroIdentificacion = $numeroIdentificacion;
    }

    /**
     * @param mixed $numeroIdentificacionDV
     * @author Jhon Garcia
     */
    public function setNumeroIdentificacionDV($numeroIdentificacionDV): void
    {
        $this->numeroIdentificacionDV = $numeroIdentificacionDV;
    }

    /**
     * @param mixed $tipoIdentificacion
     * @throws Exception
     * @author Jhon Garcia
     */
    public function setTipoIdentificacion($tipoIdentificacion): void
    {

        if(is_null($tipoIdentificacion) || is_null($tipoIdentificacion->alphanum_data_first)){
            throw new Exception('El tercero no tiene un tipo de documento válido');
        }

        $this->tipoIdentificacion = $tipoIdentificacion->alphanum_data_first;
    }


}
