<?php


namespace App\ElectronicInvoice\Models;

use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class Cliente
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class Cliente
{
    public $actividadEconomicaCIIU;
    public $destinatario = array();
    public $detallesTributarios = array();
    public $direccionFiscal;
    public $direccionCliente;
    public $email;
    public $informacionLegalCliente;
    public $nombreRazonSocial;
    public $notificar;
    public $numeroDocumento;
    public $numeroIdentificacionDV;
    public $responsabilidadesRut = array();
    public $tipoIdentificacion;
    public $tipoPersona;
    public $telefono;

    /**
     * Método constructor
     * Cliente constructor.
     * @param $document
     * @throws Exception
     * @author Jhon Garcia
     */
    public function __construct($document)
    {
        $this->setActividadEconomicaCIIU($document->contact->code_ciiu);

        $this->setDestinatario($document->contact);

        $this->setDetallesTributarios(['ZZ']);

        $this->setDireccionFiscal($document->contact);

        $this->setDireccionCliente($document->contact);

        $this->setEmail($document->contact->email);

        $this->setInformacionLegalCliente($document);

        $this->setNombreRazonSocial($document->contact);

        $this->setNotificar("SI");

        $this->setNumeroDocumento($document->contact->identification);

        $this->setNumeroIdentificacionDV($document->contact);

        $this->setResponsabilidadesRut($document->contact);

        $this->setTipoIdentificacion($document->contact->identificationt);

        $this->setTipoPersona($document->contact);

        $this->setTelefono($document->contact->main_telephone);
    }

    /**
     * @param mixed $actividadEconomicaCIIU
     * @author Jhon Garcia
     */
    public function setActividadEconomicaCIIU($actividadEconomicaCIIU): void
    {
        $this->actividadEconomicaCIIU = $actividadEconomicaCIIU;
    }

    /**
     * Metodo que agrega un valor al array destinatario
     * @param array $contact
     * @author Jhon Garcia
     */
    public function setDestinatario($contact): void
    {
        $destinatario = new Destinatario($contact);

        array_push($this->destinatario, $destinatario);
    }

    /**
     * @param array $detallesTributarios
     * @author Jhon Garcia
     */
    public function setDetallesTributarios(array $detallesTributarios): void
    {

        foreach ($detallesTributarios as $detalleTributario) {
            $tributo = new Tributos($detalleTributario);
            array_push($this->detallesTributarios, $tributo);
        }

    }

    /**
     * Metodo que establece un valor al atributo direccionFiscal
     * @param $contact
     * @throws Exception
     * @author Jhon Garcia
     */
    public function setDireccionFiscal($contact): void
    {
        $onlyCountryAndAddress = true;

        if (is_null($contact->country_code) || $contact->country_code == 'CO'){
            $onlyCountryAndAddress = false;
        }

        $this->direccionFiscal = new Direccion($contact, $onlyCountryAndAddress);
    }

    /**
     * Metodo que establece un valor al atributo direccionCliente
     * @param $contact
     * @throws Exception
     * @author Jhon Garcia
     */
    public function setDireccionCliente($contact): void
    {
        $onlyCountryAndAddress = true;

        if (is_null($contact->country_code) || $contact->country_code == 'CO'){
            $onlyCountryAndAddress = false;
        }

        $this->direccionCliente = new Direccion($contact, $onlyCountryAndAddress);
    }

    /**
     * Metodo que establece un valor al atributo email
     * @param $email
     * @author Jhon Garcia
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public function setInformacionLegalCliente($document)
    {
        $this->informacionLegalCliente = new InformacionLegalCliente($document->contact, $document->warehouse);
    }

    /**
     * @param mixed $tercero
     * @author Jhon Garcia
     */
    public function setNombreRazonSocial($tercero): void
    {
        $this->nombreRazonSocial = $tercero->name . ' ' . (string)$tercero->surname;
    }

    /**
     * @param mixed $notificar
     * @author Jhon Garcia
     */
    public function setNotificar($notificar): void
    {
        $this->notificar = $notificar;
    }

    /**
     * @param mixed $numeroDocumento
     * @author Jhon Garcia
     */
    public function setNumeroDocumento($numeroDocumento): void
    {
        $this->numeroDocumento = $numeroDocumento;
    }

    /**
     * @param mixed $contact
     * @author Jhon Garcia
     */
    public function setNumeroIdentificacionDV($contact): void
    {
        if ($contact->identification_type == 1) {
            $this->numeroIdentificacionDV = $contact->check_digit;
        }
    }

    /**
     * @param $contact
     * @author Jhon Garcia
     */
    public function setResponsabilidadesRut($contact): void
    {
        $obligaciones = new Obligaciones($contact);

        array_push($this->responsabilidadesRut, $obligaciones);
    }

    /**
     * @param mixed $tipoIdentificacion
     * author Jhon Garcia
     * @throws Exception
     */
    public function setTipoIdentificacion($tipoIdentificacion): void
    {
        if (is_null($tipoIdentificacion) || is_null($tipoIdentificacion->alphanum_data_first)) {
            throw new Exception('El tercero no tiene un tipo de documento válido');
        }

        $this->tipoIdentificacion = $tipoIdentificacion->alphanum_data_first;
    }

    public function comparaTipoIdentificacion($tipoIdentificacion)
    {

        /**
         * TIPOS DE IDENTIFICACION
         *
         * '11' => 'Registro civil'
         * '12' => 'Tarjeta de identidad'
         * '13' => 'Cédula de ciudadanía'
         * '21' => 'Tarjeta de extranjería'
         * '22' => 'Cédula de extranjería'
         * '31' => 'NIT'
         * '41' => 'Pasaporte'
         * '42' => 'Documento de identificación extranjero'
         * '50' => 'NIT de otro país'
         * '91' => 'NUIP'
         */

        $tiposIdentificacion = [
            0 => '00', // Valor por defecto
            1 => '31',
            2 => '13',
            3 => '12',
            4 => '41',
            228 => '22',
        ];

        if (is_null($tipoIdentificacion) || !isset($tiposIdentificacion[$tipoIdentificacion])) {
            return $tiposIdentificacion[0];
        }

        return $tiposIdentificacion[$tipoIdentificacion];

    }

    /**
     * @param $contact
     * @author Jhon Garcia
     */
    public function setTipoPersona($contact): void
    {
        if ($contact->taxpayer_type == 14) {
            $this->tipoPersona = '2';
        } else {
            $this->tipoPersona = '1';
        }
    }

    /**
     * @param $main_telephone
     */
    private function setTelefono($main_telephone)
    {
        $this->telefono = $main_telephone;
    }

}
