<?php


namespace App\ElectronicInvoice\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * Class Adjunto
 * @package App\ElectronicInvoice\Models
 * @author Jhon García
 */
class Adjunto
{
    public $archivo;
    public $email = array();
    public $enviar;
    public $formato;
    public $nombre;
    public $numeroDocumento;
    public $tipo;

    /**
     * Adjunto constructor.
     * @param $document
     * @throws \Exception
     */
    public function __construct($document)
    {
        $this->setEmail($document->contact->email_electronic_invoice, $document->contact->email);

        $this->setNombre($document);

        $this->setNumeroDocumento($document);
    }

    /**
     * @param mixed $archivo
     * @throws \Exception
     */
    public function setArchivo($archivo): void
    {
        $path = storage_path('app/public/') . $archivo;

        if (!Storage::disk('public')->exists($archivo)) {
            throw new \Exception('No se puede encontrar el archivo');
        }

        $this->archivo = file_get_contents($path);
    }

    /**
     * @param array $email_electronic_invoice
     * @param $email
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function setEmail($email_electronic_invoice, $email): void
    {
        if (config('app.test_electronic_billing')) {
            $emailsDefault = config('app.emails_test_electronic_billing');

            $emailsAsArray = explode(';', $emailsDefault);
            foreach ($emailsAsArray as $emailDefault) {
                if (filter_var($emailDefault, FILTER_VALIDATE_EMAIL)) {
                    array_push($this->email, $emailDefault);
                }
            }

            if (sizeof($this->email) <= 0) {
                array_push($this->email, 'desarrollo@u2.com.co');
            }
        } else {
            // if (!empty($email_electronic_invoice) && filter_var($email_electronic_invoice, FILTER_VALIDATE_EMAIL)) {
                // array_push($this->email, $email_electronic_invoice);
                
            if (!empty($email_electronic_invoice)) {
                // $email_electronic_invoice = str_replace( ['[', '"', ']', ','] , ['', '', '', ';'], json_encode($email_electronic_invoice));

                $parts_email_electronic_invoice = explode(';', $email_electronic_invoice);
                foreach ($parts_email_electronic_invoice as $single_email) {
                    if (filter_var($single_email, FILTER_VALIDATE_EMAIL)){
                        array_push($this->email, $single_email);
                    }
                }

            }else if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                array_push($this->email, $email);
            }
        }

        if (count($this->email) <= 0){
            throw new \Exception('El tercero no cuenta con una dirección de correo valida');
        }
    }

    /**
     * @param mixed $enviar
     */
    public function setEnviar($enviar): void
    {
        $this->enviar = $enviar;
    }

    /**
     * @param mixed $formato
     */
    public function setFormato($formato): void
    {
        $this->formato = $formato;
    }

    /**
     * @param $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @param $document
     */
    public function setNumeroDocumento($document): void
    {
        $this->numeroDocumento = $document->vouchertype->prefix . $document->consecutive;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo): void
    {
        $this->tipo = $tipo;
    }


}
