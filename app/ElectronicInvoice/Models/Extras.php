<?php


namespace App\ElectronicInvoice\Models;


class Extras
{
    public $controlInterno1;
    public $controlInterno2;
    public $nombre;
    public $pdf;
    public $valor;
    public $xml;

    public function __construct($extra)
    {
        $this->setControlInterno1($extra['controlInterno1']);

        $this->setControlInterno2($extra['controlInterno2']);

        $this->setNombre($extra['nombre']);

        $this->setPdf($extra['pdf']);

        $this->setValor($extra['valor']);

        $this->setXml($extra['xml']);
    }

    /**
     * @param mixed $controlInterno1
     */
    public function setControlInterno1($controlInterno1): void
    {
        $this->controlInterno1 = $controlInterno1;
    }

    /**
     * @param mixed $controlInterno2
     */
    public function setControlInterno2($controlInterno2): void
    {
        $this->controlInterno2 = $controlInterno2;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @param mixed $pdf
     */
    public function setPdf($pdf): void
    {
        $this->pdf = $pdf;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @param mixed $xml
     */
    public function setXml($xml): void
    {
        $this->xml = $xml;
    }

}
