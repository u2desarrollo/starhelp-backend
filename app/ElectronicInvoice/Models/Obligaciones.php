<?php


namespace App\ElectronicInvoice\Models;


class Obligaciones
{
    public $obligaciones;
    public $regimen;

    /**
     * Obligaciones constructor.
     * @param $contact
     */
    public function __construct($contact)
    {
        $this->setObligaciones($contact->fiscal_responsibility_id);
        $this->setRegimen($contact);
    }


    /**
     * @param mixed $obligacion
     */
    public function setObligaciones($obligacion): void
    {
        /**
         * OBLIGACIONES
         *
         * 'O-13'       => 'Gran contribuyente'
         * 'O-15'       => 'Autorretenedor'
         * 'O-23'       => 'Agente de retención de IVA'
         * 'O-47'       => 'Régimen simple de tributación'
         * 'R-99-PN'    => 'No responsable'
         */

        $obligaciones = [
            0 => '00',
            24564 => 'O-13',
            24565 => 'O-15',
            24566 => 'O-23',
            24567 => 'O-47',
            24568 => 'R-99-PN'
        ];

        if (is_null($obligacion) || !isset($obligaciones[$obligacion])){
            $this->obligaciones = $obligaciones[0];
        }else {
            $this->obligaciones = $obligaciones[$obligacion];
        }
    }

    /**
     * @param mixed $contact
     */
    public function setRegimen($contact): void
    {
        if ($contact->taxpayer_type == 14) {
            $this->regimen = '49';
        }else{
            $this->regimen = '48';
        }
    }


}
