<?php


namespace App\ElectronicInvoice\Models;

use Illuminate\Support\Facades\Log;

/**
 * Class Destinatario
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class Destinatario
{
    public $canalDeEntrega;
    public $email = array();
    public $nitProveedorReceptor;
    public $telefono;

    /**
     * Metodo constructor
     * Destinatario constructor.
     * @param $contact
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function __construct($contact)
    {
        $this->setCanalDeEntrega("0");

        $this->setEmail($contact->email_electronic_invoice, $contact->email);

        $this->setNitProveedorReceptor($contact->identification);

        $this->setTelefono($contact->main_telephone);
    }

    /**
     * @param mixed $canalDeEntrega
     * @author Jhon Garcia
     */
    public function setCanalDeEntrega($canalDeEntrega): void
    {
        $this->canalDeEntrega = $canalDeEntrega;
    }

    /**
     * @param array $email_electronic_invoice
     * @param $email
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function setEmail($email_electronic_invoice, $email): void
    {
        if (config('app.test_electronic_billing')) {
            $emailsDefault = config('app.emails_test_electronic_billing');

            $emailsAsArray = explode(';', $emailsDefault);
            foreach ($emailsAsArray as $emailDefault) {
                if (filter_var($emailDefault, FILTER_VALIDATE_EMAIL)) {
                    array_push($this->email, $emailDefault);
                }
            }

            if (sizeof($this->email) <= 0) {
                array_push($this->email, 'desarrollo@u2.com.co');
            }
        } else {
            if (!empty($email_electronic_invoice)) {

                // $email_electronic_invoice = str_replace( ['[', '"', ']', ','] , ['', '', '', ';'], json_encode($email_electronic_invoice));

                $parts_email_electronic_invoice = explode(';', $email_electronic_invoice);
                foreach ($parts_email_electronic_invoice as $single_email) {
                    if (filter_var($single_email, FILTER_VALIDATE_EMAIL)){
                        array_push($this->email, $single_email);
                    }
                }
            }else if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                array_push($this->email, $email);
            }
        }

        if (count($this->email) <= 0){
            throw new \Exception('El tercero no cuenta con una dirección de correo valida');
        }

    }

    /**
     * @param mixed $nitProveedorReceptor
     * @author Jhon Garcia
     */
    public function setNitProveedorReceptor($nitProveedorReceptor): void
    {
        $this->nitProveedorReceptor = $nitProveedorReceptor;
    }

    /**
     * @param mixed $telefono
     * @author Jhon Garcia
     */
    public function setTelefono($telefono): void
    {
        $this->telefono = $telefono;
    }


}
