<?php


namespace App\ElectronicInvoice\Models;

/**
 * Class FacturaDetalle
 *
 * @author Jhon Garcia
 * @package App\ElectronicInvoice\Models
 */
class FacturaDetalle
{
    public $cantidadPorEmpaque;
    public $cantidadReal;
    public $cantidadRealUnidadMedida;
    public $cantidadUnidades;
    public $codigoProducto;
    public $descripcion;
    public $descripcionTecnica;
    public $documentosReferenciados = array();//es un array de tipo DocumentoReferenciado
    public $estandarCodigo;
    public $estandarCodigoProducto;
    public $impuestosDetalles = array();
    public $impuestosTotales = array();
    public $marca;
    public $muestraGratis;
    public $precioTotal;
    public $precioTotalSinImpuestos;
    public $codigoTipoPrecio;
    public $precioReferencia;
    public $precioVentaUnitario;
    public $secuencia;
    public $unidadMedida;
    private $only_values;

    /**
     * FacturaDetalle constructor.
     *
     * @param $facturaDetalle
     * @param $secuencia
     * @param $trmValue
     * @param $only_values
     * @author Jhon Garcia
     */
    public function __construct($facturaDetalle, $secuencia, $trmValue, $only_values)
    {
        $this->setOnlyValues($only_values);

        $this->setCantidadPorEmpaque('1');

        $this->setCantidadReal($facturaDetalle->quantity);

        $this->setCantidadRealUnidadMedida("ZZ");

        $this->setCantidadUnidades($facturaDetalle->quantity);

        $this->setCodigoProducto($facturaDetalle->product->code);

        $this->setDescripcion($facturaDetalle->product->description);

        $this->setDescripcionTecnica($facturaDetalle->product->long_description);

        $this->setEstandarCodigo("999");

        $this->setEstandarCodigoProducto("999");

        $this->setImpuestosDetalles($facturaDetalle);

        $this->setImpuestosTotales($facturaDetalle->documentProductTaxes);

        $this->setPrecioTotalSinImpuestos($trmValue > 0 ? $facturaDetalle->total_value_brut_us : $facturaDetalle->total_value_brut);

        $this->setPrecioVentaUnitario($facturaDetalle, $trmValue);

        $this->setSecuencia($secuencia);

        $this->setUnidadMedida('ZZ');

        $this->setPrecioReferencia($facturaDetalle);

        $this->setMuestraGratis($facturaDetalle);

        $this->setCodigoTipoPrecio($facturaDetalle);
    }

    /**
     * @param $only_values
     */
    public function setOnlyValues($only_values)
    {
        $this->only_values = $only_values;
    }

    /**
     * @param mixed $cantidadPorEmpaque
     * @author Jhon Garcia
     */
    public function setCantidadPorEmpaque($cantidadPorEmpaque): void
    {
        $this->cantidadPorEmpaque = $cantidadPorEmpaque;
    }

    /**
     * @param mixed $cantidadReal
     * @author Jhon Garcia
     */
    public function setCantidadReal($cantidadReal): void
    {
        if ($this->only_values) {
            $this->cantidadReal = 1;
        } else {
            $this->cantidadReal = $cantidadReal;
        }
    }

    /**
     * @param mixed $cantidadRealUnidadMedida
     * @author Jhon Garcia
     */
    public function setCantidadRealUnidadMedida($cantidadRealUnidadMedida): void
    {
        $this->cantidadRealUnidadMedida = $cantidadRealUnidadMedida;
    }

    /**
     * @param mixed $cantidadUnidades
     * @author Jhon Garcia
     */
    public function setCantidadUnidades($cantidadUnidades): void
    {
        if ($this->only_values) {
            $this->cantidadUnidades = 1;
        } else {
            $this->cantidadUnidades = $cantidadUnidades;
        }
    }

    /**
     * @param mixed $codigoProducto
     * @author Jhon Garcia
     */
    public function setCodigoProducto($codigoProducto): void
    {
        $this->codigoProducto = $codigoProducto;
    }

    /**
     * @param mixed $descripcion
     * @author Jhon Garcia
     */
    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @param mixed $descripcionTecnica
     * @author Jhon Garcia
     */
    public function setDescripcionTecnica($descripcionTecnica): void
    {
        $this->descripcionTecnica = $descripcionTecnica;
    }

    /**
     * @param array $documentosReferenciados
     * @author Jhon Garcia
     */
    public function setDocumentosReferenciados(array $documentosReferenciados): void
    {
        $this->documentosReferenciados = $documentosReferenciados;
    }

    /**
     * @param mixed $estandarCodigo
     * @author Jhon Garcia
     */
    public function setEstandarCodigo($estandarCodigo): void
    {
        $this->estandarCodigo = $estandarCodigo;
    }

    /**
     * @param mixed $estandarCodigoProducto
     * @author Jhon Garcia
     */
    public function setEstandarCodigoProducto($estandarCodigoProducto): void
    {
        $this->estandarCodigoProducto = $estandarCodigoProducto;
    }

    /**
     * @param $facturaDetalle
     * @author Jhon Garcia
     */
    public function setImpuestosDetalles($facturaDetalle): void
    {
        foreach ($facturaDetalle->documentProductTaxes as $impuestoDetalles) {

            $quantity = $this->only_values ? 1 : $facturaDetalle->quantity;

            $baseImponible = $facturaDetalle->is_benefit ? $facturaDetalle->unit_value_before_taxes * $quantity : 0;
            $impuestosDetallesObj = new FacturaImpuestos($impuestoDetalles, $baseImponible);
            array_push($this->impuestosDetalles, $impuestosDetallesObj);
        }
    }

    /**
     * @param array $impuestosTotales
     * @author Jhon Garcia
     */
    public function setImpuestosTotales($impuestosTotales): void
    {
        //Recorre todos los documents_products_taxes
        foreach ($impuestosTotales as $impuestos) {
            $impuesto = new ImpuestosTotales($impuestos);
            $agregado = false;

            //Recorre el array de impuestos totales de la factura
            foreach ($this->impuestosTotales as $impuestoTotal) {
                //Valida si existe un impuesto ya existe en el array, con respecto al código y el porcentaje del mismo
                if ($impuesto->getCodigoTOTALImp() == $impuestoTotal->getCodigoTOTALImp()) {
                    //Si existe suma el valor del impuesto y de la base
                    $impuestoTotal->setMontoTotal($impuestoTotal->getMontoTotal() + $impuesto->getMontoTotal());

                    $agregado = true;
                    break;
                }
            }

            // En caso de que no lo encuentre en el array, lo agrega
            if (!$agregado) {
                array_push($this->impuestosTotales, $impuesto);
            }
        }
    }

    /**
     * @param mixed $marca
     * @author Jhon Garcia
     */
    public function setMarca($marca): void
    {
        $this->marca = $marca;
    }

    /**
     * @param $total_value_brut
     * @author Jhon Garcia
     */
    public function setPrecioTotalSinImpuestos($total_value_brut): void
    {
        $this->precioTotalSinImpuestos = strval(number_format(round($total_value_brut, 2), 2, '.', ''));
    }

    /**
     * @param $facturaDetalle
     * @param $trmValue
     * @author Jhon Garcia
     */
    public function setPrecioVentaUnitario($facturaDetalle, $trmValue): void
    {
        $quantity = $this->only_values ? 1 : $facturaDetalle->quantity;

        if ($trmValue > 0) {
            $precioVentaUnitario = $facturaDetalle->total_value_brut_us / $quantity;
        } else {
            $precioVentaUnitario = $facturaDetalle->total_value_brut / $quantity;
        }

        $this->precioVentaUnitario = strval(number_format(round($precioVentaUnitario, 2), 2, '.', ''));
    }

    /**
     * @param mixed $secuencia
     * @author Jhon Garcia
     */
    public function setSecuencia($secuencia): void
    {
        $this->secuencia = strval($secuencia);
    }

    /**
     * @param mixed $unidadMedida
     * @author Jhon Garcia
     */
    public function setUnidadMedida($unidadMedida): void
    {
        $this->unidadMedida = $unidadMedida;
    }

    /**
     * @param $documentProduct
     * @author Jhon Garcia
     */
    public function setPrecioReferencia($documentProduct)
    {
        if ($documentProduct->is_benefit) {
            if (!is_null($documentProduct->unit_value_list) && $documentProduct->unit_value_list != 0) {
                $this->precioReferencia = strval(number_format(round($documentProduct->unit_value_list, 2), 2, '.', ''));
            } else {
                $this->precioReferencia = strval(number_format(round($documentProduct->unit_value_before_taxes, 2), 2, '.', ''));
            }
        }
    }

    /**
     * @param $facturaDetalle
     * @author Jhon Garcia
     */
    public function setMuestraGratis($facturaDetalle)
    {
        if ($facturaDetalle->is_benefit) {
            $this->muestraGratis = 1;
        } else {
            $this->muestraGratis = 0;
        }
    }

    /**
     * @param $facturaDetalle
     * @author Jhon Garcia
     */
    public function setCodigoTipoPrecio($facturaDetalle)
    {
        if ($facturaDetalle->is_benefit) {
            $this->codigoTipoPrecio = '01';
        }
    }

}
