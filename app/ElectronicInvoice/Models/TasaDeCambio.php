<?php


namespace App\ElectronicInvoice\Models;


use Carbon\Carbon;

class TasaDeCambio
{
    private $baseMonedaDestino;
    private $baseMonedaOrigen;
    private $fechaDeTasaDeCambio;
    private $monedaDestino;
    private $monedaOrigen;
    private $tasaDeCambio;

    /**
     * TasaDeCambio constructor.
     * @param $document
     */
    public function __construct($document)
    {
        $this->setBaseMonedaDestino('1.00');
        $this->setBaseMonedaOrigen('1.00');
        $this->setFechaDeTasaDeCambio($document->document_date);
        $this->setMonedaDestino('COP');
        $this->setMonedaOrigen('USD');
        $this->setTasaDeCambio($document->trm_value);
    }

    /**
     * @param mixed $baseMonedaDestino
     */
    public function setBaseMonedaDestino($baseMonedaDestino): void
    {
        $this->baseMonedaDestino = $baseMonedaDestino;
    }

    /**
     * @param mixed $baseMonedaOrigen
     */
    public function setBaseMonedaOrigen($baseMonedaOrigen): void
    {
        $this->baseMonedaOrigen = $baseMonedaOrigen;
    }

    /**
     * @param mixed $fechaDeTasaDeCambio
     */
    public function setFechaDeTasaDeCambio($fechaDeTasaDeCambio): void
    {
        $fecha = Carbon::parse($fechaDeTasaDeCambio)->format('Y-m-d');

        $this->fechaDeTasaDeCambio = $fecha;
    }

    /**
     * @param mixed $monedaDestino
     */
    public function setMonedaDestino($monedaDestino): void
    {
        $this->monedaDestino = $monedaDestino;
    }

    /**
     * @param mixed $monedaOrigen
     */
    public function setMonedaOrigen($monedaOrigen): void
    {
        $this->monedaOrigen = $monedaOrigen;
    }

    /**
     * @param mixed $tasaDeCambio
     */
    public function setTasaDeCambio($tasaDeCambio): void
    {
        $this->tasaDeCambio = $tasaDeCambio;
    }

}
