<?php


namespace App\ElectronicInvoice\Models;

/**
 * Class FacturaImpuestos
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class FacturaImpuestos
{
    public $baseImponibleTOTALImp;
    public $codigoTOTALImp;
    public $controlInterno;
    public $porcentajeTOTALImp;
    public $unidadMedida;
    public $unidadMedidaTributo;
    public $valorTOTALImp;
    public $valorTributoUnidad;

    /**
     * FacturaImpuestos constructor.
     * @param $facturaImpuestos
     * @param null $baseImponible
     * @author Jhon Garcia
     */
    public function __construct($facturaImpuestos, $baseImponible)
    {
        $this->setBaseImponibleTOTALImp($baseImponible == 0 ? $facturaImpuestos->base_value: $baseImponible);

        $this->setCodigoTOTALImp($facturaImpuestos->tax_id);

        $this->setControlInterno("");

        $this->setPorcentajeTOTALImp($facturaImpuestos->tax->percentage_sale);

        $this->setUnidadMedida("");

        $this->setUnidadMedidaTributo("");

        $this->setValorTOTALImp($facturaImpuestos->value);

        $this->setValorTributoUnidad("");
    }

    /**
     * @param mixed $baseImponibleTOTALImp
     * @author Jhon Garcia
     */
    public function setBaseImponibleTOTALImp($baseImponibleTOTALImp): void
    {
        $this->baseImponibleTOTALImp = strval(number_format(round($baseImponibleTOTALImp, 2), 2, ".", ""));
    }

    /**
     * @param mixed $codigoTOTALImp
     * @author Jhon Garcia
     */
    public function setCodigoTOTALImp($codigoTOTALImp): void
    {

        $this->codigoTOTALImp = $this->comparaCodigoTOTALImp($codigoTOTALImp);

    }

    /**
     * @param $codigoTOTALImp
     * @return mixed
     * @author Jhon García
     */
    private function comparaCodigoTOTALImp($codigoTOTALImp)
    {

        /**
         * TIPOS DE IMPUESTO
         *
         * '01' => 'IVA''
         *  02' => 'IC'
         * '03' => 'ICA'
         * '04' => 'INC'
         * '05' => 'Retención sobre el IVA'
         * '06' => 'Retención sobre fuente por renta'
         * '07' => 'Retención sobre el ICA'
         * '20' => 'FtoHorticultura'
         * '21' => 'Timbre'
         * '22' => 'Bolsas'
         * '23' => 'INCarbono'
         * '24' => 'INCombustibles'
         * '25' => 'Sobretasa Combustibles'
         * '26' => 'Sordicom'
         * 'ZY' => 'No causa'
         * 'ZZ' => 'Otros tributos, tasas, contribuciones, y similares'
         */

        $codes = [
            0 => '00',
            1 => '01',
            2 => '05',
            3 => '06',
            4 => '02',
            5 => '07',
        ];

        if (is_null($codigoTOTALImp) || !isset($codes[$codigoTOTALImp])){
            return $codes[0];
        }

        return $codes[$codigoTOTALImp];

    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getCodigoTOTALImp()
    {
        return $this->codigoTOTALImp;
    }


    /**
     * @param mixed $controlInterno
     * @author Jhon Garcia
     */
    public function setControlInterno($controlInterno): void
    {
        $this->controlInterno = $controlInterno;
    }

    /**
     * @param mixed $porcentajeTOTALImp
     * @author Jhon Garcia
     */
    public function setPorcentajeTOTALImp($porcentajeTOTALImp): void
    {
        $this->porcentajeTOTALImp = $porcentajeTOTALImp;
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getPorcentajeTOTALImp()
    {
        return $this->porcentajeTOTALImp;
    }

    /**
     * @param mixed $unidadMedida
     * @author Jhon Garcia
     */
    public function setUnidadMedida($unidadMedida): void
    {
        $this->unidadMedida = $unidadMedida;
    }

    /**
     * @param mixed $unidadMedidaTributo
     * @author Jhon Garcia
     */
    public function setUnidadMedidaTributo($unidadMedidaTributo): void
    {
        $this->unidadMedidaTributo = $unidadMedidaTributo;
    }

    /**
     * @param mixed $valorTOTALImp
     * @author Jhon Garcia
     */
    public function setValorTOTALImp($valorTOTALImp): void
    {
        $this->valorTOTALImp = strval(number_format(round($valorTOTALImp, 2), 2, ".", ""));
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getValorTOTALImp()
    {
        return doubleval($this->valorTOTALImp);
    }

    /**
     * @param mixed $valorTributoUnidad
     * @author Jhon Garcia
     */
    public function setValorTributoUnidad($valorTributoUnidad): void
    {
        $this->valorTributoUnidad = $valorTributoUnidad;
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getValorTributoUnidad()
    {
        return doubleval($this->valorTributoUnidad);
    }

    /**
     * @return mixed
     * @author Jhon Garcia
     */
    public function getBaseImponibleTOTALImp()
    {
        return doubleval($this->baseImponibleTOTALImp);
    }



}
