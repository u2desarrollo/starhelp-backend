<?php


namespace App\ElectronicInvoice\Models;

/**
 * Class Tributos
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class Tributos
{

    /**
     * Tributos constructor.
     * @param $codigoImpuesto
     * @author Jhon Garcia
     */
    public function __construct($codigoImpuesto)
    {
        $this->codigoImpuesto = $codigoImpuesto;
    }

    /**
     * @param mixed $codigoImpuesto
     * @author Jhon Garcia
     */
    public function setCodigoImpuesto($codigoImpuesto): void
    {
        $this->codigoImpuesto = $codigoImpuesto;
    }

    public $codigoImpuesto;

    public $extras = array();
}
