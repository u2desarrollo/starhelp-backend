<?php


namespace App\ElectronicInvoice\Models;

use App\Entities\Document;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class FacturaGeneral
 * @package App\ElectronicInvoice\Models
 * @author Jhon Garcia
 */
class FacturaGeneral
{
    public $cantidadDecimales;
    public $cliente;
    public $extras = array();
    public $consecutivoDocumento;
    public $detalleDeFactura = array();//es un array de tipo FacturaDetalle
    public $documentosReferenciados = array();//es un array de tipo DocumentoReferenciado
    public $fechaEmision;
    public $impuestosGenerales = array();// almacenar todos los tipos de impuestos que estaran en la clase FacturaImpuestos
    public $impuestosTotales = array();
    public $mediosDePago = array();
    public $moneda;
    public $rangoNumeracion;
    public $redondeoAplicado;
    public $tipoDocumento;
    public $tipoOperacion;
    public $totalBaseImponible;
    public $totalBrutoConImpuesto;
    public $totalMonto;
    public $totalProductos;
    public $totalDescuentos;
    public $totalSinImpuestos;
    public $tasaDeCambio;

    /**
     * Metodo constructor
     * FacturaGeneral constructor.
     * @param $document
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function __construct($document)
    {
        $this->setCantidadDecimales("2");

        $this->setCliente($document);

        $this->setExtras();

        $this->setConsecutivoDocumento($document);

        $this->setDetalleDeFactura($document);

        $this->setDocumentosReferenciados($document);

        $this->setFechaEmision($document->document_date);

        $this->setImpuestosGenerales($document);

        $this->setImpuestosTotales($document->documentsProducts);

        $this->setMediosDePago(['ZZZ'], $document);

        $this->setMoneda($document);

        $this->setRangoNumeracion($document);

        $this->setRedondeoAplicado("0.00");

        $this->setTipoDocumento($document);

        $this->setTipoOperacion($document);

        $this->setTotalBaseImponible($document);

        $this->setTotalBrutoConImpuesto($document);

        $this->setTotalMonto($document);

        $this->setTotalProductos(count($document->documentsProducts));

        $this->setTotalSinImpuestos($document);

        $this->setTasaDeCambio($document);
    }

    /**
     * Metodo que establece un valor para el atributo cantidadDecimales
     * @author Jhon Garcia
     */
    public function setCantidadDecimales($cantidadDecimales)
    {
        $this->cantidadDecimales = $cantidadDecimales;
    }

    /**
     * Metodo que establece un valor para el atributo cliente
     * @param $document
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function setCliente($document)
    {
        $this->cliente = new Cliente($document);
    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public function setConsecutivoDocumento($document): void
    {
        $this->consecutivoDocumento = $document->vouchertype->prefix . $document->consecutive;
    }

    /**
     * @param $document
     */
    public function setDetalleDeFactura($document): void
    {
        foreach ($document->documentsProducts as $detalleDeFactura) {

            $only_values = $document->template->credit_note && $detalleDeFactura->quantity == 0;

            $secuencia = count($this->detalleDeFactura) + 1;

            $detalleDeFacturaObj = new FacturaDetalle($detalleDeFactura, $secuencia, $document->trm_value, $only_values);

            array_push($this->detalleDeFactura, $detalleDeFacturaObj);
        }
    }

    /**
     * @param array $documentosReferenciados
     * @author Jhon Garcia
     */
    public function setDocumentosReferenciados($document): void
    {
        if ($document->operationType_id == 1) {
            $documento = Document::where('id', $document->thread)->first();

            $documento_referenciado_1 = new DocumentoReferenciado($documento, 4);
            $documento_referenciado_2 = new DocumentoReferenciado($documento, 5);

            array_push($this->documentosReferenciados, $documento_referenciado_1);
            array_push($this->documentosReferenciados, $documento_referenciado_2);
        }
    }

    /**
     * @param mixed $fechaEmision
     * @author Jhon Garcia
     */
    public function setFechaEmision($fechaEmision): void
    {
        $document_date = Carbon::parse($fechaEmision)->format('Y-m-d H:i:s');

        $this->fechaEmision = $document_date;
    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public function setImpuestosGenerales($document): void
    {
        //Recorre todos los documents_products
        foreach ($document->documentsProducts as $detalleDeFactura) {

            //Recorre todos los documents_products_taxes
            foreach ($detalleDeFactura->documentProductTaxes as $impuestos) {

                $quantity = $document->template->credit_note && $detalleDeFactura->quantity == 0 ? 1 : $detalleDeFactura->quantity;

                $baseImponible = $detalleDeFactura->is_benefit ? $detalleDeFactura->unit_value_before_taxes * $quantity : 0;
                $facturaImpuestos = new FacturaImpuestos($impuestos, $baseImponible);
                $agregado = false;

                //Recorre el array de impuestos de la factura
                foreach ($this->impuestosGenerales as $impuestoGeneral) {

                    //Valida si existe un impuesto ya existe en el array, con respecto al código y el porcentaje del mismo
                    if ($facturaImpuestos->getCodigoTOTALImp() == $impuestoGeneral->getCodigoTOTALImp()
                        && $facturaImpuestos->getPorcentajeTOTALImp() == $impuestoGeneral->getPorcentajeTOTALImp()) {

                        //Si existe suma el valor del impuesto y de la base
                        $impuestoGeneral->setValorTOTALImp($facturaImpuestos->getValorTOTALImp() + $impuestoGeneral->getValorTOTALImp());
                        $impuestoGeneral->setBaseImponibleTOTALImp($facturaImpuestos->getBaseImponibleTOTALImp() + $impuestoGeneral->getBaseImponibleTOTALImp());

                        $agregado = true;
                        break;
                    }
                }

                // En caso de que no lo encuentre en el array, lo agrega
                if (!$agregado) {
                    array_push($this->impuestosGenerales, $facturaImpuestos);
                }

            }

        }

    }

    /**
     * @param $detallesDeFactura
     * @author Jhon Garcia
     */
    public function setImpuestosTotales($detallesDeFactura): void
    {
        //Recorre todos los documents_products
        foreach ($detallesDeFactura as $detalleDeFactura) {

            //Recorre todos los documents_products_taxes
            foreach ($detalleDeFactura->documentProductTaxes as $impuestos) {

                $impuesto = new ImpuestosTotales($impuestos);
                $agregado = false;

                //Recorre el array de impuestos totales de la factura
                foreach ($this->impuestosTotales as $impuestoTotal) {

                    //Valida si existe un impuesto ya existe en el array, con respecto al código y el porcentaje del mismo
                    if ($impuesto->getCodigoTOTALImp() == $impuestoTotal->getCodigoTOTALImp()) {

                        //Si existe suma el valor del impuesto y de la base
                        $impuestoTotal->setMontoTotal($impuestoTotal->getMontoTotal() + $impuesto->getMontoTotal());

                        $agregado = true;
                        break;

                    }
                }

                // En caso de que no lo encuentre en el array, lo agrega
                if (!$agregado) {
                    array_push($this->impuestosTotales, $impuesto);
                }

            }

        }
    }

    /**
     * @param array $mediosDePago
     * @author Jhon Garcia
     */
    public function setMediosDePago(array $mediosDePago, $document): void
    {
        foreach ($mediosDePago as $medioDePago) {

            $medioDePagoObj = new MediosDePago($medioDePago, $document->contact->customer);

            array_push($this->mediosDePago, $medioDePagoObj);

        }
    }

    /**
     * @param mixed $document
     * @author Jhon Garcia
     */
    public function setMoneda($document): void
    {
        if ($document->trm_value > 0) {
            $this->moneda = 'USD';
        } else {
            $this->moneda = 'COP';
        }
    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public function setRangoNumeracion($document): void
    {
        $this->rangoNumeracion = $document->vouchertype->prefix . "-" . $document->vouchertype->range_from;
    }

    /**
     * @param mixed $redondeoAplicado
     * @author Jhon Garcia
     */
    public function setRedondeoAplicado($redondeoAplicado): void
    {
        $this->redondeoAplicado = $redondeoAplicado;
    }

    /**
     * @param mixed $document
     * @author Jhon Garcia
     */
    public function setTipoDocumento($document): void // PENDIENTE DEFINIR LOS DEMAS CASOS DEL SWITCH
    {
        $this->tipoDocumento = $this->comparaTipoDocumento($document);
    }

    /**
     * @param $document
     * @return mixed
     * @author Jhon Garcia
     */
    public function comparaTipoDocumento($document)
    {

        /**
         * TIPOS DE DOCUMENTO
         *
         * '01' => 'Factura de Venta Nacional'
         * '02' => 'Factura de Exportación'
         * '03' => 'Factura de Contingencia'
         * '91' => 'Nota Crédito (Exclusivo en referencias a documentos)'
         * '92' => 'Nota Débito (Exclusivo en referencias a documentos)'
         */

        if ($document->trm_value > 0) {
            if ($document->operationType_id == 1) {
                return '91';
            }
            return '02';
        }

        $tiposDocumento = [
            0 => '00', //Valor por defecto
            5 => '01',
            1 => '91'
        ];

        if (is_null($document->operationType_id) || !isset($tiposDocumento[$document->operationType_id])) {
            return $tiposDocumento[0];
        }

        return $tiposDocumento[$document->operationType_id];

    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public function setTipoOperacion($document): void
    {
        if ($document->operationType_id == 1) {
            $this->tipoOperacion = '20';
        } else {
            $this->tipoOperacion = '10';
        }
    }

    /**
     * @param $document
     * @author Jhon Garcia
     */
    public
    function setTotalBaseImponible($document): void
    {
        $totalBaseImponible = 0;

        //Recorre todos los documents_products
        foreach ($document->documentsProducts as $detalleDeFactura) {
            //Recorre todos los documents_products_taxes
            foreach ($detalleDeFactura->documentProductTaxes as $impuesto) {

                $quantity = $document->template->credit_note && $detalleDeFactura->quantity == 0 ? 1 : $detalleDeFactura->quantity;

                $baseImponible = $detalleDeFactura->is_benefit ? $detalleDeFactura->unit_value_before_taxes * $quantity : $impuesto->base_value;
                $totalBaseImponible += $baseImponible;
            }
        }

        $this->totalBaseImponible = strval(number_format(round($totalBaseImponible, 2), 2, '.', ''));

    }

    /**
     * @param mixed $document
     * @author Jhon Garcia
     */
    public
    function setTotalBrutoConImpuesto($document): void
    {
        if ($document->trm_value > 0) {
            $this->totalBrutoConImpuesto = strval(number_format(round($document->total_value_us, 2), 2, '.', ''));;
        } else {
            $this->totalBrutoConImpuesto = strval(number_format(round($document->total_value,2), 2, '.', ''));;
        }
    }

    /**
     * @param mixed $document
     * @author Jhon Garcia
     */
    public
    function setTotalMonto($document): void
    {
        if ($document->trm_value > 0) {
            $this->totalMonto = strval(number_format(round($document->total_value_us,2), 2, '.', ''));;
        } else {
            $this->totalMonto = strval(number_format(round($document->total_value,2), 2, '.', ''));;
        }
    }

    /**
     * @param mixed $totalProductos
     * @author Jhon Garcia
     */
    public
    function setTotalProductos($totalProductos): void
    {
        $this->totalProductos = strval($totalProductos);
    }

    /**
     * @param mixed $document
     * @author Jhon Garcia
     */
    public
    function setTotalSinImpuestos($document): void
    {
        if ($document->trm_value > 0) {
            $this->totalSinImpuestos = strval(number_format(round($document->total_value_brut_us,2), 2, '.', ''));;
        } else {
            $this->totalSinImpuestos = strval(number_format(round($document->total_value_brut,2), 2, '.', ''));;
        }
    }

    /**
     * @param $document
     */
    public
    function setTasaDeCambio($document): void
    {
        if ($document->trm_value > 0) {
            $this->tasaDeCambio = new TasaDeCambio($document);
        }
    }

    private function setExtras()
    {
        $extraAsArray = [
            'controlInterno1' => 'Responder a',
            'controlInterno2' => '',
            'nombre' => '14',
            'pdf' => '0',
            'valor' => 'contador@colsaisa.com',
            'xml' => '0'
        ];


        $extra = new Extras($extraAsArray);

        array_push($this->extras, $extra);
    }

}
