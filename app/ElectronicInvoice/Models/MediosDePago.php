<?php


namespace App\ElectronicInvoice\Models;

use Carbon\Carbon;

/**
 * Class MediosDePago
 * @package App\ElectronicInvoice\Models
 * @author Jhon García
 */
class MediosDePago
{
    public $medioPago;
    public $metodoDePago;
    public $numeroDeReferencia;
    public $fechaDeVencimiento;

    /**
     * MediosDePago constructor.
     * @param $medioPago
     * @author Jhon García
     */
    public function __construct($medioPago, $customer)
    {

        $this->setMedioPago($medioPago);

        $this->setMetodoDePago('2');

        $this->setNumeroDeReferencia('');

        $this->setFechaDeVencimiento($customer);

    }

    /**
     * @param mixed $medioPago
     * @author Jhon García
     */
    public function setMedioPago($medioPago): void
    {
        $this->medioPago = $medioPago;
    }

    /**
     * @param mixed $metodoDePago
     * @author Jhon García
     */
    public function setMetodoDePago($metodoDePago): void
    {
        $this->metodoDePago = $metodoDePago;
    }

    /**
     * @param mixed $numeroDeReferencia
     * @author Jhon García
     */
    public function setNumeroDeReferencia($numeroDeReferencia): void
    {
        $this->numeroDeReferencia = $numeroDeReferencia;
    }

    /**
     * @param mixed $fechaDeVencimiento
     * @author Jhon García
     */
    public function setFechaDeVencimiento($customer): void
    {

        $expiration_days = $customer->expiration_days;

        $current_date = new Carbon();

        $this->fechaDeVencimiento = $current_date->addDays($expiration_days)->format('Y-m-d');

    }




}
