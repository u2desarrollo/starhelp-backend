<?php


namespace App\ElectronicInvoice\Services;

use Illuminate\Support\Facades\Log;

/**
 * Class WebService
 * @package App\ElectronicInvoice\Services
 */
class WebService extends SoapU2
{
    public function __construct()
    {
    }

    /**
     * @param string $wsdl
     * @param array $options
     * @param array $params
     * @return array $list
     * @author Jhon Garcia
     * Funcion que permite Cargar Certificado
     */
    public function loadCertificado($wsdl, $options, $params)
    {
        try {
            $soap = new SoapClient($wsdl, $options);
            $dat = $soap->CargarCertificado($params);
            $inter = $dat->CargarCertificadoResult;
            $list = (array)$inter;
            var_dump($list);
        } catch (Exception $e) {
            var_dump($soap->__getLastResponse());
        }
        return $list;
    }

    /**
     * Funcion para generar descargas en PDF o XML
     * @param string $wsdl
     * @param array $options
     * @param array $params
     * @return array $list
     * @author Jhon Garcia
     */
    public function Descargas($wsdl, $options, $params, $tipoDescarga)
    {
        try {
            $soap = new SoapClient($wsdl, $options);
            if ($tipoDescarga == "pdf") {
                $dat = $soap->DescargaPDF($params);
                $inter = $dat->DescargaPDFResult;
            } else if ($tipoDescarga == "xml") {
                $dat = $soap->DescargaXML($params);
                $inter = $dat->DescargaXMLResult;
            }
            $list = (array)$inter;
        } catch (Exception $e) {
            var_dump($soap->__getLastResponse());
        }
        return $list;
    }

    // Declaramos las funciones que se conectan al web service

    /**
     * Funcion para enviar una factura
     * @param $wsdl
     * @param $options
     * @param $params
     * @return array
     * @throws \Exception
     * @author Jhon Garcia
     */
    public function enviar($wsdl, $options, $params)
    {
        try {
            $soap = new SoapU2($wsdl, $options, 'Enviar');

            $dat = $soap->Enviar($params);

            return (array)$dat->EnviarResult;

        } catch (\Exception $exception) {
            return [
                'codigo' => $exception->getCode(),
                'mensaje' => $exception->getMessage(),
                'mensajesValidacion' => []
            ];
        }

    }

    public function enviocorreo($wsdl, $options, $params)
    {
        try {
            $soap = new SoapClient($wsdl, $options);
            $dat = $soap->EnvioCorreo($params);
            $inter = $dat->EnvioCorreoResult;
            $list = (array)$inter;
        } catch (Exception $e) {
            $list = (array)$soap->__getLastResponse();
        }
        return $list;
    }


    public function getEstadoDocumento($wsdl, $options, $params)
    {
        try {
            $soap = new SoapU2($wsdl, $options, 'EstadoDocumento');
            $dat = $soap->EstadoDocumento($params);
            $inter = $dat->EstadoDocumentoResult;
            return (array)$inter;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function foliosrestantes($wsdl, $options, $params)
    {
        $list;
        try {
            $soap = new SoapClient($wsdl, $options);
            $dat = $soap->FoliosRestantes($params);
            $inter = $dat->FoliosRestantesResult;
            $list = (array)$inter;
            //var_dump($list);
        } catch (Exception $e) {
            var_dump($soap->__getLastResponse());
        }
        return $list;
    }

    public function estructura($wsdl, $options, $params)
    {
        $list;
        try {
            $soap = new SoapClient($wsdl, $options);
            $tipos = $soap->__getTypes();
            var_dump($tipos);

        } catch (Exception $e) {
            var_dump($soap->__getLastResponse());
        }
        return $list;
    }

    public function validarxml($wsdl, $options, $params)
    {
        try {
            $soap = new SoapClient($wsdl, $options);
            $dat = $soap->ValidarXml($params);
            $inter = $dat->ValidarXmlResult;
            $list = (array)$inter;
        } catch (Exception $e) {
            var_dump($soap->__getLastResponse());
        }
        return $list;
    }

    /**
     * @param $wsdl
     * @param $options
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function cargarAdjuntos($wsdl, $options, $params)
    {
        try {
            $soap = new SoapU2($wsdl, $options, 'CargarAdjuntos');
            $dat = $soap->CargarAdjuntos($params);
            return (array)$dat->CargarAdjuntosResult;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            throw new \Exception($exception->getMessage());
        }
    }
}
