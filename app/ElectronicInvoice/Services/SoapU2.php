<?php

namespace App\ElectronicInvoice\Services;

use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class SoapU2
 * @package App\ElectronicInvoice\Services
 * @author Jhon García
 */
class SoapU2 extends \SoapClient
{
    private $timeout = 60; // Tiempo en segundos
    private $wsdl;
    private $action;

    /**
     * SoapU2 constructor.
     * @param $wsdl
     * @param $options
     * @param $action
     * @throws \SoapFault
     * @author Jhon García
     */
    function __construct($wsdl, $options, $action)
    {
        parent::__construct($wsdl, $options);
        $this->setTimeOut();
        $this->wsdl = $wsdl;
        $this->setAction($action);
    }

    /**
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param bool $one_way
     * @return mixed|string|string[]|null
     * @throws Exception
     */
    public function __doRequest($request, $location, $action, $version, $one_way = FALSE)
    {
        // Implementación de consumo del webservice con curl e implementando un timeout
        $curl = curl_init($this->wsdl);

        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "SOAPAction:http://tempuri.org/IService/" . $this->action));
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // Se puede activar esta opción para evitar la verificación de certificado
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // Se puede activar esta opción para evitar la verificación de certificado

        $curlResponse = curl_exec($curl);
        $xml = explode("\r\n", $curlResponse);

        // Descartar caracteres BOM
        if (array_key_exists(3, $xml)) {
            $response = preg_replace('/^(\x00\x00\xFE\xFF|\xFF\xFE\x00\x00|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/', "", $xml[3]);
        } else {
            $response = $xml[0];
        }

        //Validar si existe un error en curl
        if (curl_errno($curl)) {
            throw new Exception('Error curl: ' . curl_error($curl), 600);
        }

        curl_close($curl);

        return $response;
    }

    /**
     * @param $action
     * @author Jhon García
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @author Jhon García
     */
    private function setTimeOut()
    {
        $this->timeout = config('app.time_out');
    }
}
