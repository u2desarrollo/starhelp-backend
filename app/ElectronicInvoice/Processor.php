<?php

namespace App\ElectronicInvoice;

use App\ElectronicInvoice\Models\Adjunto;
use App\ElectronicInvoice\Models\FacturaGeneral;
use App\ElectronicInvoice\Services\WebService;
use App\Entities\Document;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

/**
 * Clase Processor que permite definir los metodos necesarios para implementar logica de negocio
 * correspondiente a la facturacion electronica
 * Class Processor
 * @package App\ElectronicInvoice
 * @author Jhon Garcia
 */
class Processor
{
    /** @var FacturaGeneral $invoice | Documento final que se envia al ws */
    private $invoice;
    /** @var Document $document | Documento proveniente de la base de datos */
    private $document;
    /** @var $service */
    private $service;
    /** @var $options */
    private $options;

    /**
     * Metodo constructor
     * Processor constructor.
     * @param Document $document
     * @throws Exception
     * @author Jhon Garcia
     */
    public function __construct(Document $document)
    {
        $this->setDocument($document);

        $this->service = new WebService();

        $this->invoice = new FacturaGeneral($document);

        $this->options = array('exceptions' => true, 'trace' => true);
    }

    /**
     *
     * @throws Exception
     * @author Jhon Garcia
     */
    public function init()
    {

        $params = array(
            'tokenEmpresa' => config('app.token_empresa'),
            'tokenPassword' => config('app.token_password'),
            'factura' => $this->invoice,
            'adjuntos' => '11',
            'connection_timeout' => 1
        );

        Log::info('//----------- Emitiendo documento ' . $this->invoice->consecutivoDocumento . ' -------------//');

        $resultado = $this->service->enviar(config('app.wsdl'), $this->options, $params);

        if ($resultado['codigo'] == 200 || $resultado['codigo'] == 201) {

            Log::info('//----------- Documento ' . $this->invoice->consecutivoDocumento . ' emitido exitosamente -------------//');

            return $resultado;
        } else if ($resultado['codigo'] == 600) {

            sleep(5);

            Log::warning('//----------- Tiempo máximo de espera excedido para la emisión del documento ' . $this->invoice->consecutivoDocumento . ' -------------//');

            $params = array(
                'tokenEmpresa' => config('app.token_empresa'),
                'tokenPassword' => config('app.token_password'),
                'documento' => $this->invoice->consecutivoDocumento
            );

            return $this->validarEstadoDocumento($params);
        }

        Log::error('//--------- Error durante la emisión del documento' . $this->invoice->consecutivoDocumento . ' -------------//');
        // -------------------- RESPONSE SOAP ------------------------------ //
        Log::error($resultado);
        // -------------------- DOCUMENT FORMAT THE FACTORY ------------------------------ //
        Log::info(json_encode((array)$this->invoice));

        $errorsAsHtml = "<p>" . $resultado['mensaje'] . "</p>";

        if (!is_null($resultado['mensajesValidacion'])) {
            $errorsAsHtml .= "<ul>";
            foreach ($resultado['mensajesValidacion'] as $error) {
                if (is_array($error)) {
                    $errorsAsHtml .= "<li>" . implode(' ', $error) . "</li>";
                } else {
                    $errorsAsHtml .= "<li>" . $error . "</li>";
                }
            }

            $errorsAsHtml .= "</ul>";
        }

        throw new Exception($errorsAsHtml);
    }

    /**
     * @param array $params
     * @return array|string
     * @throws Exception
     */
    public function validarEstadoDocumento(array $params)
    {
        Log::info('//----------- Consultando estado del documento ' . $this->invoice->consecutivoDocumento . ' -------------//');

        $resultado = $this->service->getEstadoDocumento(config('app.wsdl'), $this->options, $params);

        if ($resultado['codigo'] == 200) {
            Log::info('//----------- Documento ' . $this->invoice->consecutivoDocumento . ' emitido exitosamente -------------//');

            $resultado['qr'] = $resultado['cadenaCodigoQR'];
            return $resultado;
        }

        Log::error('//--------- Error durante la emisión del documento' . $this->invoice->consecutivoDocumento . ' -------------//');
        // -------------------- RESPONSE SOAP ------------------------------ //
        Log::error($resultado);
        // -------------------- DOCUMENT FORMAT THE FACTORY ------------------------------ //
        Log::info(json_encode((array)$this->invoice));

        throw new Exception('Error durante la emisión del documento.', 400);

    }

    /**
     * @param $document
     * @return mixed
     */
    public function setDocument($document): void
    {
        $this->document = $document;
    }

    /**
     *
     * @param $adjunto
     * @return array|mixed
     * @throws Exception
     * @author Jhon Garcia
     */
    public function attachPdf($adjunto)
    {

        $params = array(
            'tokenEmpresa' => config('app.token_empresa'),
            'tokenPassword' => config('app.token_password'),
            'adjunto' => $adjunto
        );

        $resultado = $this->service->cargarAdjuntos(config('app.wsanexo'), $this->options, $params);

        if ($resultado['codigo'] == 200) {
            return $resultado;
        }

        Log::info(Arr::except((array) $adjunto, ['archivo']));
        Log::error($resultado);

        throw new Exception($resultado['mensaje']);
    }

}
