<?php

use Carbon\Carbon;

/**
 * Esta funcion se encarga de pasar un archivo csv a un array
 * segun el tipo de limitador.
 *
 * @param string $filename
 * @param string $delimiter
 * @return array
 * @author Kevin Galindo
 */
function csvToArray($filename = '', $delimiter = ';')
{
    //Valida si el archivo existe y si se puede abrir.
    if (!file_exists($filename) || !is_readable($filename)) return false;

    //Creamos las variables de la cabecera y la data que tendra el array.
    $header = [];
    $data = [];

    //Recorremos el archivo y acomodamos la data.
    if (($handle = fopen($filename, 'r')) !== false) {

        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
            if (!$header) {
                // Elimina los caracteres no válidos u ocultos
                $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                $header = array_map('strtoupper', $row);
                $header = array_map('trim', $header);
                $header = array_map(function($value) { return str_replace(' ', '_', $value); }, $header);
            } else {
                $data[] = array_combine($header, $row);
            }
        }

        fclose($handle);
    }

    if (count($data[0]) < 0) {
        return false;
    }

    //Retornamos la informacion.
    return $data;
}

function createDateFromFormat($date, $formatOrigin, $format)
{
    return $date ? Carbon::createFromFormat($formatOrigin, $date)->format($format) : null; 
}