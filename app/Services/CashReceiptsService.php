<?php

namespace App\Services;

use App\Entities\BranchOffice;
use App\Entities\CashReceiptsApp;
use App\Entities\Contact;
use App\Entities\ContactWarehouse;
use App\Entities\Document;
use App\Entities\Parameter;
use App\Entities\User;
use App\Entities\VouchersType;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CashReceiptsService
{
    public function __construct()
    {
    }

    public function getCashReceiptsApp()
    {
        $cashReceiptsApp = CashReceiptsApp::where(['id_erp' => null])
            ->orWhereHas('consignement', function ($consignement) {
                $consignement->whereNull('id_erp');
            })
            ->with([
                'paymentMethodsApp',
                'consignement',
                'contributionInvoicesApp', 'warehouse' => function ($query) {
                    $query->with('contact', 'branchoffice');
                }, 'contact'
            ])->get();
        return json_decode($cashReceiptsApp);
    }


    public function getDocument($id)
    {

        return Document::where('id', $id)->with('contact')->first();
    }

    public function getBranchoffice($id)
    {
        return BranchOffice::select('code')->where('id', $id)->first();
    }


    public function getVoucherType($id)
    {
        return VouchersType::select('code_voucher_type')->where('id', $id)->first();
    }


    public function getClient($id)
    {
        return Contact::where('id', $id)->first();
    }


    public function createFlatCashReceipts()
    {
        try {

            $consignements = collect();
            $invoices = collect();

            //   comilla doble para que el compilador reconozca la instruccion
            $lineBreak = "\r\n";
            $tab = "\t";

            // creo el archivo
            $date = now();
            Storage::disk('local')->put('public/CashReceipts/APPWEB-' . str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
                str_replace(':', '', date_format(date_create($date), 'H:i:s')) . '.txt', null);

            //abro el archivo con permisos de escritura
            $file = fopen(storage_path('app/public/CashReceipts/APPWEB-' . str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
                str_replace(':', '', date_format(date_create($date), 'H:i:s')) . '.txt'), "a");

            // *******  CODIGO CORREPONDIENTE AL RC    ***********  //
            // inserto al archivo los encabezados
            fwrite($file, 'INDICATIVO   |ID-WEB   |COD-CLI   |NOM-CLI   |APELL-CLI   |COD-SUC-CLI  |NOM-SUC-CLI    |FEC-COM   |HORA-COM   |COD-VEND     |NUM-COM     |NOM-VEND   |OBSERVA  |ORIGEN   |PAGO-APLIC    |FORMA-PAGO1   |VR-PAGO1   |NOM-ENTIDAD1   |NUM-IDENT1  |FORMA-PAGO2    |VR-PAGO2   |NOM-ENTIDAD2   |NUM-IDENT2   |FORMA-PAGO3     |VR-PAGO3  |NOM-ENTIDAD3   |NUM-IDENT3   |ID-CONSIGN    |' . $lineBreak);

            $cashReceipts = $this->getCashReceiptsApp();

            if (count($cashReceipts) > 0) {
                foreach ($cashReceipts as $key => $value) {
                    // capturo las consigaciones para imprimirlas al final
                    if ($value->consignement != null) {
                        //$v = $consignements->contains($value->consignement);
                        $consignements->push($value->consignement);
                    }
                    // lineas a insertar en el archivo
                    $textRec = '';
                    $formaPago = '';

                    //inserto en el string del rc los datos propios|

                    //  /*. $value->warehouse->code */ no es necesario andres tomara el
                    //correspondiente a cada factura

                    $textRec = 'Rec         |' . $value->id . '   |' . $value->warehouse->contact->identification . '   |' . $value->warehouse->contact->name . '     |'
                        . $value->warehouse->contact->surname . '       |' /*. $value->warehouse->code */ . '    |' . $value->warehouse->description . '    |' .
                        str_replace('-', '', date_format(date_create($value->creation_date), 'Y-m-d')) . '   |' .
                        str_replace(':', '', date_format(date_create($value->creation_date), 'H:i:s')) . '   |' . $value->contact->identification . '   |'
                        . $value->consecutive . '       |' . $value->contact->name . '   ' . $value->contact->surname . '   |' .
                        str_replace("\n", " ", $value->observations) . '   |1   |      |';


                    // capturo los valores que puedan llegar hacer pagados en efectivo
                    $valorPagadoEfectivo = 0;
                    // cantidad de impresiones , variable utilixada solo para el tema de rc
                    $cantImpr = 0;
                    // accedo a las manera de pago utilizadas en el rc
                    foreach ($value->payment_methods_app as $key => $data) {

                        // depende del codigo evidenciado en el elemento defino a que tipo de pago corresponde
                        switch ($data->parameter_id) {
                            case '849':
                                $formaPago = 'EFECTIVO';
                                $valorPagadoEfectivo += $data->total_paid;
                                break;

                            case '850':
                                $formaPago = 'CHEQUE';
                                break;

                            case '851':
                                $formaPago = 'TRANSFERENCIA';
                                break;
                        }

                        // concateno las formas de pago encontradas a la variable correspondiente al rc
                        //diferentes a efectivo
                        if ($data->parameter_id != 849) {
                            $cantImpr++;
                            $textRec .= $formaPago . '     |' . $data->total_paid . '       |' . $data->bank . '       |' . $data->check_number . '     |';
                        }
                    }
                    if ($valorPagadoEfectivo > 0) {
                        $cantImpr++;
                        $textRec .= 'EFECTIVO  |' . $valorPagadoEfectivo . '    |         |        |';
                    }


                    if ($cantImpr < 3) {
                        for ($i = 0; $i < (3 - $cantImpr); $i++) {
                            $textRec .= '      |         |      |      |';
                        }
                    }

                    $textRec .= $value->consignment_id . '    |';
                    fwrite($file, $textRec . $lineBreak);
                    // *******   FIN  *******  //


                    // recorro el array de facturas
                    foreach ($value->contribution_invoices_app as $key => $data) {

                        //recojo todas las facturas  para su posterior impresion
                        if ($data->document_id == null) {
                            $data->contactIdentification = $value->warehouse->contact->identification;
                            $data->contactName = $value->warehouse->contact->name;
                            $data->contactSurname = $value->warehouse->contact->surname;
                            $data->WarehouseBranchofficeCode = $value->warehouse->branchoffice->code;
                        }
                        $invoices->push($data);
                    }
                }

                //  *****  CODIGO CORRESPONDIENTE A LA FACTURA   *****///
                fwrite($file, '' . $lineBreak);
                fwrite($file, 'INDICATIVO   |ID-WEB   |ID-WEB-RC   |PVE-OFI   |TIP-COM     |NUM-COM      |PREFIJO   |COD-CLI   |NOM-CLI   |APELL-CLI    |COD-SUC-CLI   |NOM-SUC-CLI    |VR-PAGADO    |VR-DCT-PP    |DIAS-TRA-FAC  |RETE_FTE|  SIGNO  |RETE_IVA|  SIGNO   |RETE_ICA     |   SIGNO  |' . $lineBreak);
                foreach ($invoices->unique()->values() as $key => $data) {

                    if ($data->document_id) {

                        $textFac = '';
                        //  obtengo la factura
                        $document = $this->getDocument($data->document_id);
                        // obtengo el punto de venta
                        $branchoffice = $this->getBranchoffice($document->branchoffice_id);
                        // obtengo el voucher
                        $voucher = $this->getVoucherType($document->vouchertype_id);
                        // obtengo el tercero
                        $contact = $this->getClient($document->contact_id);
                        $warehouses = $this->getWarehouseById($document->warehouse_id);

                        $textFac = 'Fac    |' . $document->id . '  |' .
                            $data->cash_receipts_app_id . '  |' . $branchoffice->code . '     |' . $voucher->code_voucher_type . '    |' .
                            $document->consecutive . '    |' . $document->prefix . '     |' . $contact->identification . '  |' .
                            $contact->name . '  |' . $contact->surname . '  |' . $warehouses->code . '     |' . $warehouses->description . '     |' .
                            $data->total_paid . '       |' . $data->value_discount_prompt_payment . '    |' . $data->invoice_days . '     |' .


                            str_replace('-', '', $data->rete_fte) . '     |';
                        $textFac .= substr($data->rete_fte, 0, 1) == '-' ? '  -  |' . str_replace('-', '', $data->rete_iva) . '   |' : '      |' . str_replace('-', '', $data->rete_iva) . '      |';
                        $textFac .= substr($data->rete_iva, 0, 1) == '-' ? '  -  |' . str_replace('-', '', $data->rete_ica) . '   |' : '      |' . str_replace('-', '', $data->rete_ica) . '      |';
                        $textFac .= substr($data->rete_ica, 0, 1) == '-' ? '  -  |' : '     |';

                        fwrite($file, $textFac . $lineBreak);

                        //   **************  FIN  **************  //

                    } else {
                        $textFac = 'Fac    |         |' .
                            $data->cash_receipts_app_id . '  |' . $data->WarehouseBranchofficeCode . '      |       |1   |ANTIC     |' . $data->contactIdentification . '|' . $data->contactName . '|' . $data->contactSurname . ' |       |        |'
                            . $data->total_paid . '       |' . $data->value_discount_prompt_payment . '    |' . $data->invoice_days . '     |' .


                            str_replace('-', '', $data->rete_fte) . '     |';
                        $textFac .= substr($data->rete_fte, 0, 1) == '-' ? '  -  |' . str_replace('-', '', $data->rete_iva) . '   |' : '      |' . str_replace('-', '', $data->rete_iva) . '      |';
                        $textFac .= substr($data->rete_iva, 0, 1) == '-' ? '  -  |' . str_replace('-', '', $data->rete_ica) . '   |' : '      |' . str_replace('-', '', $data->rete_ica) . '      |';
                        $textFac .= substr($data->rete_ica, 0, 1) == '-' ? '  -  |' : '     |';


                        fwrite($file, $textFac . $lineBreak);
                    }
                }

                fwrite($file, '                                                                                                                      ' . $lineBreak);
                fwrite($file, 'INDICATIVO   |ID-WEB   |SW-APLICADO     |FEC-CONSIGN     |COD-VEND    |NOM-VEND      |COD-BANCO      |NUM-CONSIGN      |HORA-CONSIGN       |OBSERVA   |ORIGEN   |' .
                    $lineBreak);

                // EXTRAIGO LAS CONSIGNACIONES CORRESPONDIENTES A LOS RC
                foreach ($consignements->unique()->values() as $key => $v) {
                    $user = $this->getClientByParamUser($v->user_id);
                    $bank = $this->getCodeBank($v->bank_id);


                    $textcon = 'Con    |' . $v->id . '|     |' . str_replace('-', '', date_format(date_create($v->created_at), 'Y-m-d')) . '   |' .
                        $user->contact->identification . '     |' . $user->contact->name . ' ' . $user->contact->surname . '     |' .
                        $bank->code_parameter . '    |' . $v->consignment_number . '  |' .
                        str_replace(':', '', date_format(date_create($v->created_at), 'H:i:s')) . '   |     |1      |';

                    fwrite($file, $textcon . $lineBreak);
                }
                fclose($file);
                // metodo que realiza las inserciones en el sftp
                $this->operateFile($date);
            }

            Storage::disk('local')->delete('public/CashReceipts/APPWEB-' . str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
                str_replace(':', '', date_format(date_create($date), 'H:i:s')) . '.txt');
            Log::info('//--------------- Archivo plano de RECIBOS DE CAJA generado y enviado al servido SFTP exitosamente');
        } catch (\Throwable $th) {
            Log::alert($th);
        }
    }


    public function operateFile($date)
    {
        $sftp = Storage::disk('sftp');
        $file = Storage::disk('local')->get('public/CashReceipts/APPWEB-' . str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
            str_replace(':', '', date_format(date_create($date), 'H:i:s')) . '.txt');

        $sftp->put('ERP/PENDING/APPWEB-' . str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
            str_replace(':', '', date_format(date_create($date), 'H:i:s')) . '.txt', $file);
    }


    public function getClientByParamUser($id)
    {
        return json_decode(User::where('id', $id)->with('contact')->first());
    }


    public function getCodeBank($id)
    {
        return json_decode(Parameter::select('code_parameter')->where('id', $id)->first());
    }

    public function getWarehouseById($id)
    {

        return DB::table('contacts_warehouses')->select(['code', 'description'])->where('id', $id)->first();
    }


    public function createFlat($info, $contacts, $transfer)
    {
        if (count($info[0]) == 0 && count($info[1]) == 0 && count($contacts) == 0 && count($transfer[1]) == 0) {
            return null;
        }
        $sftp = Storage::disk('sftp');
        $lineBreak = "\r\n";
        if (count($info) > 0 || count($contacts) > 0) {

            //creo y abro el archivo con permisos de escritura
            //Storage::disk('local')->put('colsaisa/FlatFile.txt', null);
            $file = fopen(storage_path('app/colsaisa/FlatFile.txt'), "a");

            // inserto al archivo los titulos
            fwrite($file, 'TIPO-REG|CODIGO-NIT-PPAL|DIGITO-PPAL|TIPO-IDENT|NOMBRES-RAZON-PPAL|APELLIDOS-PPAL|CODIGO-SUC|DESCRIPCION-SUC|ID-WEB-SUC|CLIENTE|PROVEEDOR|EMPLEADO|DIRECCION-PPAL|TELEFONO-PPAL|COD-CIUDAD-PPAL|EMAIL-PPAL|DIRECCION-SUC|TELEFONO-SUC|COD-CIUDAD-SUC|CELULAR-SUC|EMAIL-SUC|COD-CATEG-CLI|COD-CATEG-PROV|CUPO-CREDITO|COD-ZONA|CLI-RET-FTE|CLI-RET-IVA|CLI-RET-ICA|PRO-RET-FTE|PRO-RET-IVA|PRO-RET-ICA|COD-SEDE|DESC-CIUDAD|COD-VENDEDOR|NOM-VENDEDOR|RESPONS-FISCAL|TIPO-NEGOCIACION' .                $lineBreak);

            foreach ($info[0] as $key => $value) {
                $warehouseCode = '';
                $warehouseDescription = '';
                $warehouseId = '';
                $warehouseaddress = '';
                $warehousetelephone = '';
                $warehousecode_suc_city = '';
                $warehousecellphone = '';
                $warehouseemail = '';
                $warehouscode_zone = '';
                $warehousecode_branchoffice = '';
                $warehousecode_desc_city = '';
                $warehouseseller_ident = '';
                $warehousedes_ident = '';
                $warehousetypesNegotiation = '';

                if ($value["warehouse"] != null) {
                    $warehouseCode = $value["warehouse"]->code;
                    $warehouseDescription = $value["warehouse"]->description;
                    $warehouseId = $value["warehouse"]->id;
                    $warehouseaddress = $value["warehouse"]->address;
                    $warehousetelephone = $value["warehouse"]->telephone;
                    $warehousecode_suc_city = $value["warehouse"]->code_suc_city;
                    $warehousecode_desc_city = $value["warehouse"]->code_desc_city;
                    $warehousecode_branchoffice = $value["warehouse"]->code_branchoffice;
                    $warehousecellphone = $value["warehouse"]->cellphone;
                    $warehouseemail = $value["warehouse"]->email;
                    $warehouscode_zone = $value["warehouse"]->code_zone;
                    $warehouseseller_ident = $value["warehouse"]->seller_ident;
                    $warehousedes_ident =  $value["warehouse"]->des_ident;
                    $warehousetypesNegotiation = $value["warehouse"]->types_negotiation;
                }
                fwrite($file, 'TER|' . // TIPO-REG
                preg_replace('/\s+/', ' ', trim($value["contact"]->identification)) . '|' .                 // CODIGO-NIT-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->check_digit)) . '|' .                    // DIGITO-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->code_parameter)) . '|' .                 // TIPO-IDENT
                preg_replace('/\s+/', ' ', trim($value["contact"]->name)) . '|' .                           // NOMBRES-RAZON-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->surname)) . '|' .                        // APELLIDOS-PPAL
                preg_replace('/\s+/', ' ', trim($warehouseCode)) . '|' .                                    // CODIGO-SUC
                preg_replace('/\s+/', ' ', trim($warehouseDescription)) . '|' .                             // DESCRIPCION-SUC
                preg_replace('/\s+/', ' ', trim($warehouseId)) . '|' .                                      // ID-WEB-SUC
                preg_replace('/\s+/', ' ', trim($value["contact"]->_is_customer)) . '|' .                   // CLIENTE
                preg_replace('/\s+/', ' ', trim($value["contact"]->_is_provider)) . '|' .                   // PROVEEDOR
                preg_replace('/\s+/', ' ', trim($value["contact"]->_is_employee)) . '|' .                   // EMPLEADO
                preg_replace('/\s+/', ' ', trim($value["contact"]->address)) . '|' .                        // DIRECCION-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->cell_phone)) . '|' .                     // TELEFONO-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->city->city_code)) . '|' .                // COD-CIUDAD-PPAL
                preg_replace('/\s+/', ' ', trim($value["contact"]->email)) . '|' .                          // EMAIL-PPAL
                preg_replace('/\s+/', ' ', trim($warehouseaddress)) . '|' .                                 // DIRECCION-SUC
                preg_replace('/\s+/', ' ', trim($warehousetelephone)) . '|' .                               // TELEFONO-SUC
                preg_replace('/\s+/', ' ', trim($warehousecode_suc_city)) . '|' .                           // COD-CIUDAD-SUC
                preg_replace('/\s+/', ' ', trim($warehousecellphone)) . '|' .                               // CELULAR-SUC
                preg_replace('/\s+/', ' ', trim($warehouseemail)) . '|' .                                   // EMAIL-SUC
                preg_replace('/\s+/', ' ', trim($value["contact"]->code_categ_cli)) . '|' .                 // COD-CATEG-CLI
                preg_replace('/\s+/', ' ', trim($value["contact"]->code_categ_prov)) . '|' .                // COD-CATEG-PROV
                preg_replace('/\s+/', ' ', trim(is_null($value["contact"]->customer) ? '' :$value["contact"]->customer->credit_quota)) . '|' .         // CUPO-CREDITO
                preg_replace('/\s+/', ' ', trim($warehouscode_zone)) . '|' .                                // COD-ZONA
                preg_replace('/\s+/', ' ', trim($value["contact"]->customer_retention_source)) . '|' .      // CLI-RET-FTE
                preg_replace('/\s+/', ' ', trim($value["contact"]->customer_retention_iva)) . '|' .         // CLI-RET-IVA
                preg_replace('/\s+/', ' ', trim($value["contact"]->customer_retention_ica)) . '|' .         // CLI-RET-ICA
                preg_replace('/\s+/', ' ', trim($value["contact"]->provider_retention_source)) . '|' .      // PRO-RET-FTE
                preg_replace('/\s+/', ' ', trim($value["contact"]->provider_retention_iva)) . '|' .         // PRO-RET-IVA
                preg_replace('/\s+/', ' ', trim($value["contact"]->provider_retention_ica)) . '|'.          // PRO-RET-ICA
                preg_replace('/\s+/', ' ', trim($warehousecode_branchoffice)) . '|'.                        // COD-SEDE
                preg_replace('/\s+/', ' ', trim($warehousecode_desc_city)) .'|'.                            // DESC-CIUDAD
                preg_replace('/\s+/', ' ', trim($warehouseseller_ident)) .  '|'.                            // IDENT-VENDEDOR
                preg_replace('/\s+/', ' ', trim($warehousedes_ident)) .  '|'.                               // NOM-VENDEDOR
                preg_replace('/\s+/', ' ', trim($value["contact"]->fiscal_responsibility_code)) .'|'.       // RESPONS-FISCAL
                preg_replace('/\s+/', ' ', trim($warehousetypesNegotiation)) .                                  // TIPO-NEGOCIACION

                    $lineBreak);
            }

            foreach ($contacts as $key => $value) {
                fwrite($file, 'TER|' . // TIPO-REG
                preg_replace('/\s+/', ' ', trim($value->contactt->identification)) . '|' . // CODIGO-NIT-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->check_digit)) . '|' . // DIGITO-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->code_parameter)) . '|' . // TIPO-IDENT
                preg_replace('/\s+/', ' ', trim($value->contactt->name)) . '|' . // NOMBRES-RAZON-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->surname)) . '|' . // APELLIDOS-PPAL
                preg_replace('/\s+/', ' ', trim($value->code)) . '|' . // CODIGO-SUC
                preg_replace('/\s+/', ' ', trim($value->description)) . '|' . // DESCRIPCION-SUC
                preg_replace('/\s+/', ' ', trim($value->id)) . '|' . // ID-WEB-SUC
                preg_replace('/\s+/', ' ', trim($value->contactt->_is_customer)) . '|' . // CLIENTE
                preg_replace('/\s+/', ' ', trim($value->contactt->_is_provider)) . '|' . // PROVEEDOR
                preg_replace('/\s+/', ' ', trim($value->contactt->_is_employee)) . '|' . // EMPLEADO
                preg_replace('/\s+/', ' ', trim($value->contactt->address)) . '|' . // DIRECCION-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->cell_phone)) . '|' . // TELEFONO-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->city->city_code)) . '|' . // COD-CIUDAD-PPAL
                preg_replace('/\s+/', ' ', trim($value->contactt->email)) . '|' . // EMAIL-PPAL
                preg_replace('/\s+/', ' ', trim($value->address)) . '|' . // DIRECCION-SUC
                preg_replace('/\s+/', ' ', trim($value->telephone)) . '|' . // TELEFONO-SUC
                preg_replace('/\s+/', ' ', trim($value->code_suc_city)) . '|' . // COD-CIUDAD-SUC
                preg_replace('/\s+/', ' ', trim($value->cellphone)) . '|' . // CELULAR-SUC
                preg_replace('/\s+/', ' ', trim($value->email)) . '|' . // EMAIL-SUC
                preg_replace('/\s+/', ' ', trim($value->contactt->code_categ_cli)) . '|' . // COD-CATEG-CLI
                preg_replace('/\s+/', ' ', trim($value->contactt->code_categ_prov)) . '|' . // COD-CATEG-PROV
                preg_replace('/\s+/', ' ', trim( is_null($value->contactt->customer) ? '' : $value->contactt->customer->credit_quota)) . '|' . // CUPO-CREDITO
                preg_replace('/\s+/', ' ', trim($value->code_zone)) . '|' . // COD-ZONA
                preg_replace('/\s+/', ' ', trim($value->contactt->customer_retention_source)) . '|' . // CLI-RET-FTE
                preg_replace('/\s+/', ' ', trim($value->contactt->customer_retention_iva)) . '|' . // CLI-RET-IVA
                preg_replace('/\s+/', ' ', trim($value->contactt->customer_retention_ica)) . '|' . // CLI-RET-ICA
                preg_replace('/\s+/', ' ', trim($value->contactt->provider_retention_source)) . '|' . // PRO-RET-FTE
                preg_replace('/\s+/', ' ', trim($value->contactt->provider_retention_iva)) . '|' . // PRO-RET-IVA
                preg_replace('/\s+/', ' ', trim($value->contactt->provider_retention_ica)) .'|' .// PRO-RET-ICA
                preg_replace('/\s+/', ' ', trim($value->code_branchoffice)) . '|'.// COD-SEDE
                preg_replace('/\s+/', ' ', trim($value->code_desc_city)) .'|'. // DESC-CIUDAD
                preg_replace('/\s+/', ' ', trim($value->seller_ident)) . '|'.// IDENT-VENDEDOR
                preg_replace('/\s+/', ' ', trim($value->des_ident)) . '|'.// NOM-VENDEDOR
                preg_replace('/\s+/', ' ', trim($value->contactt->fiscal_responsibility_code)) . '|'.// RESPONS-FISCAL
                preg_replace('/\s+/', ' ', trim($value->typesNegotiation)) . // TIPO-NEGOCIACION
                $lineBreak);
            }

            fwrite($file, '' . $lineBreak);
            fwrite($file, '' . $lineBreak);

            fwrite($file, 'TIPO-REG|ID-WEB|COD-SEDE|COD-TIP-COM|NUM-COM|PREFIJO|FECHA|WD-HORA|COD-TERCERO|COD-SUCURSAL|COD-TIPO-OPERACION|COD-VENDEDOR|NOMBRE-VENDEDOR|COD-BODEGA|CENTRO-COSTOS|OBSERVACION|COD-MODELO|COD-SEDE-BASE|BODEGA-BASE|COD-TIP-COM-BASE|NUM-COM-BASE|PREFIJO-BASE|DOC-CRUCE|PREFIJO-CRUCE|FECHA-DOC-CRUCE|DIAS-PLAZO|FECHA-VENC|VR-BRUTO|VR-IVA|BASE-IVA|VR-IMPOCONSUMO|BASE-IMPO|VR-TOTAL|VR-RETE-FTE|VR-RETE-IVA|VR-RETE-ICA|TOT-PRODS|TOT-CANTS|ID-USUARIO|NOMBRE-USUARIO|NUM-IMPORT|COTO-REF|COSTO-TOTAL|MANEJA-PORCENTAJE-COMISION|PORCENTAJE-COMISION' .
                $lineBreak);

            fwrite($file, 'TIPO-REG|COD-PRODUCTO|DESCRIPCION|NUM-LOTE|FECHA-VENC-LOTE|COD-TIPO-OPERACION|COD-SEDE|COD-BODEGA|CANTIDAD|VR-UNIT-ANTES-IMP|VR-BRUTO|VR-IVA|PC-IVA|VR-IMPO|PC-IMPO|VR-TOTAL|COSTO-UNIT|COSTO-TOTAL|SALDO-UND|SALDO-VRS|SALDO-UND-CIA|SALDO-VRS-CIA|COD-LINEA|DESCRIP-LINEA|COD-SUBLINEA|DESCRIP-SUBLINEA|COD-MARCA|DESCRIP-MARCA|COD-SUBMARCA|DESCRIP-SUBMARCA|PC-IVA-COMPRA-PROD|PC-IVA-VENTA-PROD|PC-IMPO-COMPRA-PROD|PC-IMPO-VENTA-PROD|COST-REF|SEDE-AFECTACION|BODEGA-AFECTACION|CUENTA' .
                $lineBreak);

            foreach ($info[1] as $key => $value) {
                $valuewarehousecode = '';
                if ($value->warehouse != null) {
                    $valuewarehousecode = $value->warehouse->code;
                }
                fwrite($file, 'DOC|' .
                preg_replace('/\s+/', ' ', trim($value->id)) . '|' .                                            // ID-WEB
                preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                            // COD-SEDE
                preg_replace('/\s+/', ' ', trim($value->vouchertype->code_voucher_type)) . '|' .                // COD-TIP-COM
                preg_replace('/\s+/', ' ', trim($value->consecutive)) . '|' .                                   // NUM-COM
                preg_replace('/\s+/', ' ', trim($value->prefix)) . '|' .                                        // PREFIJO
                preg_replace('/\s+/', ' ', trim($value->date)) . '|' .                                          // FECHA
                preg_replace('/\s+/', ' ', trim($value->hour)) . '|' .                                          // WD-HORA
                preg_replace('/\s+/', ' ', trim($value->contact->identification)) . '|' .                       // COD-TERCERO
                preg_replace('/\s+/', ' ', trim($valuewarehousecode)) . '|' .                                   // COD-SUCURSAL
                preg_replace('/\s+/', ' ', trim($value->operationType->code)) . '|' .                           // COD-TIPO-OPERACION
                preg_replace('/\s+/', ' ', trim($value->seller_identification)) . '|' .                         // COD-VENDEDOR
                preg_replace('/\s+/', ' ', trim($value->seller_name)) . '|' .                                   // NOMBRE-VENDEDOR
                preg_replace('/\s+/', ' ', trim($value->branchofficeWarehouse->warehouse_code)) . '|' .         // COD-BODEGA
                preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                            // CENTRO-COSTOS
                preg_replace('/\s+/', ' ', trim($value->observation)) . '|' .                                   // OBSERVACION
                preg_replace('/\s+/', ' ', trim($value->template->code)) . '|' .                                // COD-MODELO
                preg_replace('/\s+/', ' ', trim($value->document_branchoffice_code)) . '|' .                    // COD-SEDE-BASE
                preg_replace('/\s+/', ' ', trim($value->origin_branchoffice_warehouse_code)) . '|' .            // BODEGA-BASE
                preg_replace('/\s+/', ' ', trim($value->document_vouchertype_code_voucher_type)) . '|' .        // COD-TIP-COM-BASE
                preg_replace('/\s+/', ' ', trim($value->document_consecutive)) . '|' .                          // NUM-COM-BASE
                preg_replace('/\s+/', ' ', trim($value->document_prefix)) . '|' .                               // PREFIJO-BASE
                preg_replace('/\s+/', ' ', trim($value->document_cross_con)) .'| ' .                            // DOC-CRUCE
                preg_replace('/\s+/', ' ', trim($value->document_cross_prefix)) . '| ' .                        // PREFIJO-CRUCE
                '| ' .                                                                                          // FECHA-DOC-CRUCE
                preg_replace('/\s+/', ' ', trim($value->term_days)) . '|' .                                     // DIAS-PLAZO
                preg_replace('/\s+/', ' ', trim($value->duedate)) . '|' .                                       // FECHA-VENC
                preg_replace('/\s+/', ' ', trim($value->total_value_brut)) . '|' .                              // VR-BRUTO
                preg_replace('/\s+/', ' ', trim($value->iva)) . '|' .                                           // VR-IVA
                preg_replace('/\s+/', ' ', trim($value->base_iva)) . '|' .                                      // BASE-IVA
                preg_replace('/\s+/', ' ', trim($value->ipoconsumo)) . '|' .                                    // VR-IMPOCONSUMO
                preg_replace('/\s+/', ' ', trim($value->base_INPO)) . '|' .                                     // BASE-IMPO
                preg_replace('/\s+/', ' ', trim($value->total_value)) . '|' .                                   // VR-TOTAL
                preg_replace('/\s+/', ' ', trim($value->rete_fuente)) . '|' .                                   // VR-RETE-FTE
                preg_replace('/\s+/', ' ', trim($value->rete_iva)) . '|' .                                      // VR-RETE-IVA
                preg_replace('/\s+/', ' ', trim($value->rete_ica)) . '|' .                                      // VR-RETE-ICA
                preg_replace('/\s+/', ' ', trim($value->TOT_prods)) . '|' .                                     // TOT-PRODS
                preg_replace('/\s+/', ' ', trim($value->TOT_CANTS)) . '|' .                                     // TOT-CANTS
                preg_replace('/\s+/', ' ', trim($value->user_id)) . '|' .                                       // ID-USUARIO
                preg_replace('/\s+/', ' ', trim($value->user->name)) .'|' .                                     // NOMBRE-USUARIO'
                preg_replace('/\s+/', ' ', trim($value->import_number)) .'|' .                                  // NUM-IMPORT
                preg_replace('/\s+/', ' ', trim($value->cos_refer)) . '|' .                                     // COSTO-REF
                preg_replace('/\s+/', ' ', trim($value->total_cost)) .  '|' .                                   // COSTO-total
                preg_replace('/\s+/', ' ', trim($value->commission_percentage_applies)) .'|' .                  // MANEJA-PORCENTAJE-COMISION
                preg_replace('/\s+/', ' ', trim($value->commission_percentage)) .                               // PORCENTAJE-COMISION
                    $lineBreak);
                foreach ($value->documentsProducts as $key => $dp) {
                    fwrite($file, 'PRO|' .                                                                  // TIPO-REG
                        preg_replace('/\s+/', ' ', trim($dp->product->code)) . '|' .                            // COD-PRODUCTO
                        preg_replace('/\s+/', ' ', trim($dp->description)) . '|' .                              // DESCRIPCION
                        preg_replace('/\s+/', ' ', trim($dp->lot)) . '|  ' .                                     // NUM-LOTE
                        '   |' .                                                                                // FECHA-VENC-LOTE
                        preg_replace('/\s+/', ' ', trim($value->operationType->code)) . '|' .                   // COD-TIPO-OPERACION
                        preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                    // COD-SEDE
                        preg_replace('/\s+/', ' ', trim($value->branchofficeWarehouse->warehouse_code)) . '|' . // COD-BODEGA
                        preg_replace('/\s+/', ' ', trim($dp->quantity)) . '|' .                                 // CANTIDAD
                        preg_replace('/\s+/', ' ', trim($dp->unit_value_before_taxes)) . '|' .                  // VR-UNIT-ANTES-IMP
                        preg_replace('/\s+/', ' ', trim($dp->total_value_brut)) . '|' .                         // VR-BRUTO
                        preg_replace('/\s+/', ' ', trim($dp->vr_iva)) . '|' .                                   // VR-IVA
                        preg_replace('/\s+/', ' ', trim($dp->pc_iva)) . '|' .                                   // PC-IVA
                        preg_replace('/\s+/', ' ', trim($dp->vr_impo)) . '|' .                                  // VR-IMPO
                        preg_replace('/\s+/', ' ', trim($dp->pc_impo)) . '|' .                                  // PC-IMPO
                        preg_replace('/\s+/', ' ', trim($dp->total_value)) . '|' .                              // VR-TOTAL
                        preg_replace('/\s+/', ' ', trim($dp->costo_unit)) . '|' .                               // COSTO-UNIT
                        preg_replace('/\s+/', ' ', trim($dp->inventory_cost_value)) . '|' .                     // COSTO-TOTAL
                        preg_replace('/\s+/', ' ', trim($dp->stock_after_operation)) . '|' .                    // SALDO-UND
                        preg_replace('/\s+/', ' ', trim($dp->stock_value_after_operation)) . '|  ' .             // SALDO-VRS
                        '  | ' .                                                                                 // SALDO-UND-CIA
                        '  |' .                                                                                 // SALDO-VRS-CIA
                        preg_replace('/\s+/', ' ', trim($dp->cod_line)) . '|' .                                 // COD-LINEA
                        preg_replace('/\s+/', ' ', trim($dp->description_line)) . '|' .                         // DESCRIP-LINEA
                        preg_replace('/\s+/', ' ', trim($dp->cod_subline)) . '|' .                              // COD-SUBLINEA
                        preg_replace('/\s+/', ' ', trim($dp->description_subline)) . '|' .                      // DESCRIP-SUBLINEA
                        preg_replace('/\s+/', ' ', trim($dp->cod_brand)) . '|' .                                // COD-MARCA
                        preg_replace('/\s+/', ' ', trim($dp->description_brand)) . '|' .                        // DESCRIP-MARCA
                        preg_replace('/\s+/', ' ', trim($dp->cod_subbrand)) . '|' .                             // COD-SUBMARCA
                        preg_replace('/\s+/', ' ', trim($dp->description_subbrand)) . '|' .                     // DESCRIP-SUBMARCA
                        preg_replace('/\s+/', ' ', trim($dp->PC_IVA_COMPRA_PROD)) . '|' .                       // PC-IVA-COMPRA-PROD
                        preg_replace('/\s+/', ' ', trim($dp->PC_IVA_VENTA_PROD)) . '|' .                        // PC-IVA-VENTA-PROD
                        preg_replace('/\s+/', ' ', trim($dp->PC_IMPO_COMPRA_PROD)) . '|' .                      // PC-IMPO-COMPRA-PROD
                        preg_replace('/\s+/', ' ', trim($dp->PC_IMPO_VENTA_PROD)) . '|' .                         // PC-IMPO-VENTA-PROD
                        '|' .                                                                                    // COST-REF
                        preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                      //SEDE-AFECTACION
                        preg_replace('/\s+/', ' ', trim($dp->branchofficeWarehouse->warehouse_code)) . '|' .      //BODEGA-AFECTACION
                        preg_replace('/\s+/', ' ', trim($dp->account_code)) . '' .                                // CUENTA

                        $lineBreak);
                }
            }

            //traslados

            if (count($transfer[1]) > 0) {
                foreach ($transfer[1] as $key => $value) {
                    $valuewarehousecode = '';
                    if ($value->warehouse != null) {
                        $valuewarehousecode = $value->warehouse->code;
                    }
                    if ($value->user != null) {
                        $nameUser = $value->user->name;
                    } else {
                        $nameUser = '';
                    }
                    fwrite($file, 'DOC|' .
                        preg_replace('/\s+/', ' ', trim($value->id)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->vouchertype_code_voucher_type)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->consecutive)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->prefix)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->date)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->hour)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->contact->identification)) . '|' .
                        preg_replace('/\s+/', ' ', trim(/*$valuewarehousecode*/ '')) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->operationType_code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->seller_identification)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->seller_name)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->branchofficeWarehouse->warehouse_code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->observation)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->vouchertype_template_code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->document_branchoffice_code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->origin_branchoffice_warehouse_code)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->document_vouchertype_code_voucher_type)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->document_consecutive)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->document_prefix)) . '|' . '    |   |   |' .
                        preg_replace('/\s+/', ' ', trim($value->term_days)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->duedate)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->total_value_brut)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->iva)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->base_iva)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->ipoconsumo)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->base_INPO)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->total_value)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->rete_fuente)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->rete_iva)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->rete_ica)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->TOT_prods)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->TOT_CANTS)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->user_id)) . '|' .
                        preg_replace('/\s+/', ' ', trim($nameUser)) . '|' .
                        preg_replace('/\s+/', ' ', trim($value->import_number)) . '|' .// NUM-IMPORT
                        preg_replace('/\s+/', ' ', trim('')) . '|' .                                        // COSTO-REF
                        preg_replace('/\s+/', ' ', trim($value->total_cost)) .                                          // COSTO-total
                        $lineBreak);
                    foreach ($value->documentsProducts as $key => $dp) {
                        fwrite($file, 'PRO|' .                                                                  // TIPO-REG
                            preg_replace('/\s+/', ' ', trim($dp->product->code)) . '|' .                            // COD-PRODUCTO
                            preg_replace('/\s+/', ' ', trim($dp->product->description)) . '|' .                     // DESCRIPCION
                            preg_replace('/\s+/', ' ', trim($dp->lot)) . '|  ' .                                     // NUM-LOTE
                            '  |' .                                                                                 // FECHA-VENC-LOTE
                            preg_replace('/\s+/', ' ', trim($value->operationType->code)) . '|' .                   // COD-TIPO-OPERACION
                            preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                    // COD-SEDE
                            preg_replace('/\s+/', ' ', trim($value->branchofficeWarehouse->warehouse_code)) . '|' . // COD-BODEGA
                            preg_replace('/\s+/', ' ', trim($dp->quantity)) . '|' .                                 // CANTIDAD
                            preg_replace('/\s+/', ' ', trim($dp->unit_value_before_taxes)) . '|' .                  // VR-UNIT-ANTES-IMP
                            preg_replace('/\s+/', ' ', trim($dp->total_value_brut)) . '|' .                         // VR-BRUTO
                            preg_replace('/\s+/', ' ', trim($dp->vr_iva)) . '|' .                                   // VR-IVA
                            preg_replace('/\s+/', ' ', trim($dp->pc_iva)) . '|' .                                   // PC-IVA
                            preg_replace('/\s+/', ' ', trim($dp->vr_impo)) . '|' .                                  // VR-IMPO
                            preg_replace('/\s+/', ' ', trim($dp->pc_impo)) . '|' .                                  // PC-IMPO
                            preg_replace('/\s+/', ' ', trim($dp->total_value)) . '|' .                              // VR-TOTAL
                            preg_replace('/\s+/', ' ', trim($dp->costo_unit)) . '|' .                               // COSTO-UNIT
                            preg_replace('/\s+/', ' ', trim($dp->inventory_cost_value)) . '|' .                     // COSTO-TOTAL
                            preg_replace('/\s+/', ' ', trim($dp->stock_after_operation)) . '|' .                    // SALDO-UND
                            preg_replace('/\s+/', ' ', trim($dp->stock_value_after_operation)) . '| ' .              // SALDO-VRS
                            '   |  ' .                                                                               // SALDO-UND-CIA
                            '   |' .                                                                                // SALDO-VRS-CIA
                            preg_replace('/\s+/', ' ', trim($dp->cod_line)) . '|' .                                 // COD-LINEA
                            preg_replace('/\s+/', ' ', trim($dp->description_line)) . '|' .                         // DESCRIP-LINEA
                            preg_replace('/\s+/', ' ', trim($dp->cod_subline)) . '|' .                              // COD-SUBLINEA
                            preg_replace('/\s+/', ' ', trim($dp->description_subline)) . '|' .                      // DESCRIP-SUBLINEA
                            preg_replace('/\s+/', ' ', trim($dp->cod_brand)) . '|' .                                // COD-MARCA
                            preg_replace('/\s+/', ' ', trim($dp->description_brand)) . '|' .                        // DESCRIP-MARCA
                            preg_replace('/\s+/', ' ', trim($dp->cod_subbrand)) . '|' .                             // COD-SUBMARCA
                            preg_replace('/\s+/', ' ', trim($dp->description_subbrand)) . '|' .                     // DESCRIP-SUBMARCA
                            preg_replace('/\s+/', ' ', trim($dp->PC_IVA_COMPRA_PROD)) . '|' .                       // PC-IVA-COMPRA-PROD
                            preg_replace('/\s+/', ' ', trim($dp->PC_IVA_VENTA_PROD)) . '|' .                        // PC-IVA-VENTA-PROD
                            preg_replace('/\s+/', ' ', trim($dp->PC_IMPO_COMPRA_PROD)) . '|' .                      // PC-IMPO-COMPRA-PROD
                            preg_replace('/\s+/', ' ', trim($dp->PC_IMPO_VENTA_PROD)) . '|' .                       // PC-IMPO-VENTA-PROD
                            preg_replace('/\s+/', ' ', trim($dp->cost_ref)) .                                       // COST-REF
                            preg_replace('/\s+/', ' ', trim($value->branchoffice->code)) . '|' .                      //SEDE-AFECTACION
                            preg_replace('/\s+/', ' ', trim($dp->branchofficeWarehouse->warehouse_code)) . '' .      //BODEGA-AFECTACION
                            $lineBreak);
                    }
                }
            }

            fclose($file);
            //  fecha actual
            $date = now();
            // obtengo el archivo recientemente creado
            $file = Storage::disk('local')->get('colsaisa/FlatFile.txt');

            //  subo el archivo a el sftp
            $sftp->put('ERP/PENDING/INVWEB-' .
                str_replace('-', '', date_format(date_create($date), 'Y-m-d')) . '-' .
                str_replace(':', '', date_format(date_create($date), 'H:i:s'))
                . '.txt', $file);
            $file = Storage::disk('local')->delete('colsaisa/FlatFile.txt');
        }
        Log::info('//--------------- Archivo plano de INVENTARIO generado y enviado al servido FTP exitosamente');
    }
}
