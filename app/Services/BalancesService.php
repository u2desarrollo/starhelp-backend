<?php

namespace App\Services;

use App\Entities\DocumentTransactions;
use App\Entities\BalancesForDocument;
use App\Events\NotifyCompletionOfBalanceUpdateEvent;
use Illuminate\Support\Facades\Cache;
use App\Entities\BalancesForAccount;
use App\Entities\BalancesForContact;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Entities\Parameter;
use App\Entities\Account;
use Illuminate\Support\Arr;
use App\Entities\Document;
use Carbon\Carbon;

/**
 * Class BalancesService
 *
 * @author Jhon García
 * @package App\Services
 */
class BalancesService
{
    private $tables;
    private $levels_accounts;
    private $year_month;
    private $accounts = [];
    private $masive_balances;
    private $accounts_for_updating_superior_accounts = [];
    private $multiplier = 1;
    private $document;

    /**
     * BalancesService constructor.
     *
     * @author Jhon García
     */
    public function __construct()
    {
        $this->tables = [
            'balances_for_contacts'  => BalancesForContact::$columns,
            'balances_for_accounts'  => BalancesForAccount::$columns,
            'balances_for_documents' => BalancesForDocument::$columns
        ];

        $this->setLevelsAccounts();
    }

    public function searchWallet($request)
    {
        $wallet = DB::select("select * from public.fn_wallet_query(?, ?, ?, ?)", [$request->year_month, $request->contact_id, $request->wallet_type, $request->seller_id]);
        return $wallet;
    }

    /**
     * @param $year_month
     * @param $contact_id
     * @param null $wallet_type
     * @param null $seller_id
     * @return array
     */
    public function searchWalletByContactId($year_month, $contact_id = null, $wallet_type = null, $seller_id = null)
    {
        return DB::select("select sum(final_balance) as final_balance from public.fn_wallet_query(?, ?, ?, ?)", [$year_month, $contact_id, $wallet_type, $seller_id]);
    }


    public function getMovementsAccountContact($request)
    {
        // $balance_for_contact_repository = new BalanceForContactRepository;
        // $documents_transactions = $this->balance_for_contact_repository->getMovementsAccountContact($request);
        $documents_transactions = BalancesForContact :: whereBetween('year_month', [$request->year_month_from, $request->year_month_up])
        ->with([
            'contact:id,identification,name,surname',
            'account'=>function($account) use ($request){
                $account->select('id','code','name', 'debit_credit');
                if (isset($request->account_up) && !is_null($request->account_up) && !empty($request->account_up)) {
                    $account->where('code', '>=',  $request->account_from );
                    $account->where('code', '<=',  $request->account_up );
                }else{
                    $account->where('code', 'ILIKE',  $request->account_from . '%');
                }
            }
        ])
        ->whereHas('account',function($account) use ($request){
            $account->select('id','code','name', 'debit_credit');
            if (isset($request->account_up) && !is_null($request->account_up) && !empty($request->account_up)) {
                $account->where('code', '>=',  $request->account_from );
                $account->where('code', '<=',  $request->account_up );
            }else{
                $account->where('code', 'ILIKE',  $request->account_from . '%');
            }
        })
        ->where(function($query){
            $query->where('initial_balance_month', '<>', 0)
            ->where('debits_month', '<>', 0)
            ->where('credits_month', '<>', 0)
            ->where('final_balance', '<>', 0);
        })
        ->get();

        return [
            'data' => $documents_transactions
        ];
    }


    public function searchAccount($request)
    {
        $balance_for_accounts = Account::
            selectRaw("accounts.id,accounts.code,accounts.name,(CASE WHEN balances_for_accounts.final_balance is null THEN 0 ELSE balances_for_accounts.final_balance END) AS final_balance")
            ->leftJoin('balances_for_accounts', 'accounts.id', '=', 'balances_for_accounts.account_id');
             if(!empty($request->year_month)){
                $balance_for_accounts->whereRaw('balances_for_accounts.year_month = '."'".$request->year_month."'");
            }
            if(!empty($request->level)){
                $balance_for_accounts->whereRaw('(accounts.level = '.$request->level."'");
            }
            if(!empty($request->account_from) && !empty($request->account_up)){
                $balance_for_accounts->whereRaw('(accounts.code >='."'". $request->account_from."'".' AND accounts.code <='."'".$request->account_up."')");
            }

            $balance_for_accounts->orderBy('accounts.code');
            return $balance_for_accounts->get();
    }

    public function getBalanceContact($request,$id_account)
    {
        //dd($id_account, $request);
        $balance_for_accounts = BalancesForContact::
            selectRaw("contacts.identification,contacts.id,
            balances_for_contacts.account_id,
            contacts.identification,
            contacts.name,
            (CASE WHEN balances_for_contacts.final_balance is null THEN 0 ELSE balances_for_contacts.final_balance END) AS final_balance")
            ->Join('accounts', 'balances_for_contacts.account_id', '=', 'accounts.id')
            ->Join('contacts', 'contacts.id', '=', 'balances_for_contacts.contact_id')
            ->where('balances_for_contacts.account_id','=',$id_account);
             if(!empty($request->year_month)){
                $balance_for_accounts->whereRaw('balances_for_contacts.year_month = '."'".$request->year_month."'");
            }
            if(!empty($request->account_from) && !empty($request->account_up)){
                $balance_for_accounts->whereRaw('(accounts.code >='."'". $request->account_from."'".' AND accounts.code <='."'".$request->account_up."')");
            }
            $balance_for_accounts->orderBy('accounts.code');
            return $balance_for_accounts->get();
    }

    public function getBalanceDocument($request,$id_contact,$id_account)
    {
        $balance_for_accounts = BalancesForDocument::
            selectRaw("(CASE WHEN documents.prefix IS NOT NULL THEN documents.prefix||'-'||documents.consecutive ELSE ''||documents.consecutive END) AS consecutive,
            balances_for_documents.account_id,
            documents.due_date,
            documents.document_date,
            contacts.name,
            (CASE WHEN balances_for_documents.final_balance is null THEN 0 ELSE balances_for_documents.final_balance END) AS final_balance")
            ->Join('accounts', 'balances_for_documents.account_id', '=', 'accounts.id')
            ->Join('contacts', 'contacts.id', '=', 'balances_for_documents.contact_id')
            ->Join('documents', 'documents.id', '=', 'balances_for_documents.contact_id')
            ->where('balances_for_documents.contact_id','=',$id_contact)
            ->where('balances_for_documents.account_id','=',$id_account);
             if(!empty($request->year_month)){
                $balance_for_accounts->whereRaw('balances_for_documents.year_month = '."'".$request->year_month."'");
            }
            if(!empty($request->account_from) && !empty($request->account_up)){
                $balance_for_accounts->whereRaw('(accounts.code >='."'". $request->account_from."'".' AND accounts.code <='."'".$request->account_up."')");
            }
            $balance_for_accounts->orderBy('accounts.code');
            return $balance_for_accounts->get();
    }


    public function getBalanceDocumentContactAccount($request,$id_contact,$id_account)
    {

        $balance_for_accounts = BalancesForDocument::
            selectRaw("balances_for_documents.account_id,
            documents.document_date,
            voucherstypes.name_voucher_type,
            (CASE WHEN documents.prefix IS NULL THEN ''||documents.consecutive ELSE documents.prefix||'-'||documents.consecutive END) as number_doc,
            documents.observation,
            contacts.identification||'-'||contacts.name||' '||contacts.surname as contact,
            accounts.code||'-'||accounts.name as account,
            balances_for_documents.contact_id,
            balances_for_documents.debits_month,
            balances_for_documents.credits_month,
            documents.consecutive,documents.due_date,
            (CASE WHEN balances_for_documents.final_balance is null THEN 0 ELSE balances_for_documents.final_balance END) AS final_balance")
            ->Join('accounts', 'balances_for_documents.account_id', '=', 'accounts.id')
            ->Join('documents', 'documents.id', '=', 'balances_for_documents.affects_document_id')
            ->Join('voucherstypes', 'documents.vouchertype_id', '=', 'voucherstypes.id')
            ->Join('contacts', 'contacts.id', '=', 'balances_for_documents.contact_id')
            ->where('balances_for_documents.account_id','=',$id_account)->where('balances_for_documents.contact_id','=',$id_contact);
             if(!empty($request->year_month_from) && !empty($request->year_month_up)){
                $balance_for_accounts->whereBetween('balances_for_documents.year_month',[$request->year_month_from,$request->year_month_up]);
            }
            if(!empty($request->account_from) && !empty($request->account_up)){
                $balance_for_accounts->whereRaw('(accounts.code >='."'". $request->account_from."'".' AND accounts.code <='."'".$request->account_up."')");
            }

            $balance_for_accounts->orderBy('accounts.code');
            return $balance_for_accounts->get();
    }

    /**
     * Establece un valor para level_accounts
     *
     * @author Jhon García
     */
    private function setLevelsAccounts()
    {
        $this->levels_accounts = Cache::remember('levels_accounts', 60, function () {
            return Parameter::select('code_parameter')
                ->where('paramtable_id', 98)
                ->orderBy('code_parameter', 'ASC')
                ->get();
        });
    }

    /**
     * Establecer el valor de document de acuerdo al id
     *
     * @param int $document_id Contiene el id del documento afectado
     * @author Jhon García
     */
    private function setDocument(int $document_id)
    {
        $this->document = Document::find($document_id, ['id', 'contact_id']);
    }

    /**
     * Establecer el valor de accounts de acuerdo al document_id
     *
     * @param int $document_id Contiene el id del documento
     * @author Jhon García
     */
    private function setAccounts($document_id)
    {
        $this->accounts = DocumentTransactions::select('account_id')
            ->where('document_id', $document_id)
            ->get()
            ->pluck('account_id')
            ->toArray();
    }

    /**
     * Método principal que se encarga de llamar los demás métodos involucrados en la actualización de saldos
     *
     * @param string $year_month Contiene el año-mes para ejecutar el cierre
     * @param int $document_id Contiene el id del documento afectado
     * @param bool $update Contiene un booleano que termina si es una actualización de documento
     * @param int $user_id Contiene el id del usuario que realizó la actualización de saldos
     * @author Jhon García
     */
    public function updateBalances($year_month, $document_id, $update, $user_id)
    {
        $current_year_month = now()->firstOfMonth()->addMonths(1);

        $this->year_month = Carbon::parse($year_month)->firstOfMonth();

        // Variable que determina si se deben copiar los saldos del mes anterior
        $copy_balances_previous_month = true;

        // Determinar si es una actualización masiva o por documento
        $this->masive_balances = is_null($document_id);

        // Si no es masiva se llama la función para obtener las cuentas del documento afectado
        if (!$this->masive_balances) {
            $this->setDocument($document_id);
            $this->setAccounts($document_id);
            $copy_balances_previous_month = false;

            // Validar si es una actualización para establecer el multiplicador de los valores
            if ($update) {
                $this->multiplier = -1;
            }
        }

        try {
            DB::beginTransaction();

            Log::info('//-----------------------Iniciando actualización de saldos desde ' . $this->year_month->format('Y-m-d'));

            do {
                $this->accounts_for_updating_superior_accounts = $this->accounts;

                foreach ($this->tables as $table => $columns) {
                    if ($this->masive_balances) {
                        $this->clearBalances($table);
                    }

                    if ($this->masive_balances || $copy_balances_previous_month) {
                        $this->copyBalancesPreviousMonth($table, $columns);
                    }
                }

                $this->getAndUpdateBalances();

                $this->updateSuperiorAccountsBalances();

                $this->year_month->addMonths(1);

                $copy_balances_previous_month = true;

            } while ($this->year_month <= $current_year_month);

            DB::commit();

            Log::info('//-----------------------Finalizando actualización de saldos hasta ' . $this->year_month->addMonths(-1)->format('Y-m-d'));

            if ($this->masive_balances && !is_null($user_id)) {
                event(new NotifyCompletionOfBalanceUpdateEvent($user_id, true));
            }

        } catch (\Exception $exception) {

            Log::error($exception);

            DB::rollBack();

            if ($this->masive_balances && !is_null($user_id)) {
                event(new NotifyCompletionOfBalanceUpdateEvent($user_id, false));
            }
        }
    }

    /**
     * Limpia los saldos actuales de una tabla
     *
     * @param string $table Contiene el nombre de la tabla
     * @author Jhon García
     */
    private function clearBalances($table)
    {
        DB::table($table)->where('year_month', $this->year_month->format('Ym'))->delete();
    }

    /**
     * Copiar los saldos del mes anterior
     *
     * @param string $table Contiene el nombre de la tabla
     * @param $columns
     * @author Jhon García
     */
    private function copyBalancesPreviousMonth($table, $columns)
    {
        $old_year_month = Carbon::parse($this->year_month->format('Y-m-d'))->addMonths(-1);

        $month = (int)$this->year_month->format('m');

        $old_registers = DB::table($table)
            ->select($table . '.*', 'a.resume_beginning_year')
            ->join('accounts AS a', 'a.id', '=', $table . '.account_id')
            ->where('year_month', $old_year_month->format('Ym'))
            ->when(!$this->masive_balances, function ($accounts) {
                $accounts->whereIn('account_id', $this->accounts);
            })
            ->when(!$this->masive_balances && $table == 'balances_for_documents', function ($balance_for_document) use ($table) {
                $balance_for_document->where($table . '.affects_document_id', $this->document->id);
            })
            ->when(!$this->masive_balances && $table == 'balances_for_contacts', function ($balance_for_contact) use ($table) {
                $balance_for_contact->where($table . '.contact_id', $this->document->contact_id);
            })
            ->get();

        // Validar si es actualización masiva de saldos
        if ($this->masive_balances) {
            foreach ($old_registers as $old_register) {
                if ($old_register->final_balance != 0 && (($month == 1 && !$old_register->resume_beginning_year) || $month != 1)) {
                    $new_register = array_merge(((array)$old_register), [
                        'year_month'            => $this->year_month->format('Ym'),
                        'initial_balance_month' => $old_register->final_balance,
                        'debits_month'          => 0,
                        'credits_month'         => 0
                    ]);

                    DB::table($table)->insert(Arr::only($new_register, $columns));
                }
            }
        } else {

            switch ($table) {
                case 'balances_for_documents':
                    $only = ['document_id', 'contact_id', 'account_id', 'year_month'];
                    break;
                case 'balances_for_contacts':
                    $only = ['contact_id', 'account_id', 'year_month'];
                    break;
                default:
                    $only = ['account_id', 'year_month'];
                    break;
            }

            foreach ($old_registers as $old_register) {
                $old_register = (array)$old_register;

                $old_register['year_month'] = $this->year_month->format('Ym');

                DB::table($table)->where(Arr::only($old_register, $only))
                    ->update(['initial_balance_month' => $old_register['final_balance']]);
            }
        }
    }

    /**
     * Actualiza los saldos
     *
     * @author Jhon García
     */
    private function getAndUpdateBalances()
    {
        $query = 'select * from public.fn_get_balances(?, ?)';

        $parameters = [
            $this->year_month->format('Ym'),
            ''
        ];

        if (!$this->masive_balances) {
            $accounts = implode(',', $this->accounts);

            $parameters = [
                $this->year_month->format('Ym'),
                $accounts
            ];
        }

        $balances = DB::select($query, $parameters);

        foreach ($balances as $balance) {

            $balance = (array)$balance;

            $this->updateBalancesForDocuments($balance);
            $this->updateBalancesForContacts($balance);
            $this->updateBalancesForAccounts($balance);
        }

    }

    /**
     * Actualiza los saldos por documento
     *
     * @param array $balance
     * @author Jhon García
     */
    private function updateBalancesForDocuments(array $balance)
    {
        if ($balance['manage_document_balances'] && !is_null($balance['affects_document_id'])) {

            $balance_for_document = DB::table('balances_for_documents')
                ->where(Arr::only($balance, [
                    'year_month',
                    'account_id',
                    'contact_id',
                    'affects_document_id'
                ]))->first();


            if ($balance_for_document) {

                $balance_for_document = collect($balance_for_document)->toArray();

                if ($this->masive_balances) {
                    $balance_for_document['credits_month'] += $balance['credits_month'];
                    $balance_for_document['debits_month'] += $balance['debits_month'];
                }else{
                    $balance_for_document['credits_month'] = $balance['credits_month'] * $this->multiplier;
                    $balance_for_document['debits_month'] = $balance['debits_month'] * $this->multiplier;
                }

                $balance_for_document['final_balance'] = $balance_for_document['initial_balance_month'] + $balance_for_document['debits_month'] - $balance_for_document['credits_month'];
                DB::table('balances_for_documents')
                    ->where('id', $balance_for_document['id'])
                    ->update(Arr::only($balance_for_document, ['credits_month', 'debits_month', 'final_balance']));
            } else {
                DB::table('balances_for_documents')
                    ->insert(Arr::only($balance, BalancesForDocument::$columns));
            }
        }
    }

    /**
     * Actualiza los saldos por tercero
     *
     * @param $balance
     * @author Jhon García
     */
    private function updateBalancesForContacts(array $balance)
    {
        if ($balance['manage_contact_balances']) {

            $balance_for_contact = DB::table('balances_for_contacts')
                ->where(Arr::only($balance, [
                    'year_month',
                    'account_id',
                    'contact_id'
                ]))->first();

            if ($balance_for_contact) {

                $balance_for_contact = collect($balance_for_contact)->toArray();

                $balance_for_contact['original_balance'] += $balance['original_balance'] * $this->multiplier;
                $balance_for_contact['credits_month'] += $balance['credits_month'] * $this->multiplier;
                $balance_for_contact['debits_month'] += $balance['debits_month'] * $this->multiplier;
                $balance_for_contact['final_balance'] = $balance_for_contact['initial_balance_month'] + $balance_for_contact['debits_month'] - $balance_for_contact['credits_month'];

                DB::table('balances_for_contacts')
                    ->where('id', $balance_for_contact['id'])
                    ->update(Arr::only($balance_for_contact, ['original_balance', 'credits_month', 'debits_month', 'final_balance']));
            } else {
                DB::table('balances_for_contacts')
                    ->insert(Arr::only($balance, BalancesForContact::$columns));
            }
        }
    }

    /**
     * Actualiza los saldos por cuenta
     *
     * @param array $balance Contiene la información de saldos
     * @author Jhon García
     */
    private function updateBalancesForAccounts(array $balance): void
    {
        $balance_for_account = DB::table('balances_for_accounts')
            ->where(Arr::only($balance, [
                'year_month',
                'account_id',
            ]))->first();

        if ($balance_for_account) {

            $balance_for_account = collect($balance_for_account)->toArray();

            $balance_for_account['original_balance'] += $balance['original_balance'] * $this->multiplier;
            $balance_for_account['credits_month'] += $balance['credits_month'] * $this->multiplier;
            $balance_for_account['debits_month'] += $balance['debits_month'] * $this->multiplier;
            $balance_for_account['final_balance'] = $balance_for_account['initial_balance_month'] + $balance_for_account['debits_month'] - $balance_for_account['credits_month'];

            DB::table('balances_for_accounts')
                ->where('id', $balance_for_account['id'])
                ->update(Arr::only($balance_for_account, ['original_balance', 'credits_month', 'debits_month', 'final_balance']));
        } else {
            DB::table('balances_for_accounts')
                ->insert(Arr::only($balance, BalancesForAccount::$columns));
        }
    }

    /**
     * Actualiza los saldos de las cuentas superiores
     *
     * @author Jhon García
     */
    private function updateSuperiorAccountsBalances()
    {
        if ($this->masive_balances) {
            foreach ($this->levels_accounts as $level) {

                $query = 'select * from public.fn_get_balances_for_accounts(?, ?, ?)';

                $parameters = [
                    $this->year_month->format('Ym'),
                    $level->code_parameter,
                    ''
                ];

                $balances = DB::select($query, $parameters);

                foreach ($balances as $balance) {

                    $balance = (array)$balance;

                    $this->updateBalancesForAccounts($balance);
                }
            }
        } else {
            while (count($this->accounts_for_updating_superior_accounts)) {

                $accounts = implode(',', $this->accounts_for_updating_superior_accounts);

                $query = 'select * from public.fn_get_balances_for_accounts(?, ?, ?)';

                $parameters = [
                    $this->year_month->format('Ym'),
                    '',
                    $accounts
                ];

                $balances = DB::select($query, $parameters);

                $this->accounts_for_updating_superior_accounts = collect($balances)->pluck('account_id')->toArray();

                foreach ($balances as $balance) {

                    $balance = (array)$balance;

                    $this->updateBalancesForAccounts($balance);
                }

            }
        }
    }


}
