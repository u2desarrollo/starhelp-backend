<?php

namespace App\Services;

//Entities
use App\Entities\VouchersType;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\ServiceRequestsProduct;

// Repositories
use App\Repositories\ServiceRequestRepository;
use App\Repositories\DocumentRepository;

//services
use App\Services\DocumentsService;
use App\Services\DocumentsProductsService;


//Otros
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Collection;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;


class ServiceRequestService
{

    protected $serviceRequestRepository;
    protected $documentsService;
    protected $documentRepository;
    protected $documentsProductsService;

    public function __construct(ServiceRequestRepository $serviceRequestRepository,
                                DocumentsService $documentsService,
                                DocumentRepository $documentRepository,
                                DocumentsProductsService $documentsProductsService)
    {
        $this->serviceRequestRepository = $serviceRequestRepository;
        $this->documentsService = $documentsService;
        $this->documentRepository = $documentRepository;
        $this->documentsProductsService = $documentsProductsService;
    }

    /**
     * Este metodo se encarga de traer todas las
     * solicitudes de servicios junto con su historico de estado.
     * Ademas de su siguiente estado.
     *
     * @author Kevin Galindo | Santiago Torres
     */
    public function getServiceRequestWithHistoryStatus($request)
    {
        // Paginacion.
        $page = $request->has('page') ? $request->page : 1;
        $perPage = $request->has('perPage') ? $request->perPage : 15;
        $offset = ($page * $perPage) - $perPage;

        // Traemos la informacion
        $serviceRequests = $this->serviceRequestRepository->getServiceRequest($request);

        $serviceRequests->map(function ($item, $key) use ($serviceRequests){
            $item->statusHistory->map(function ($item2, $key) use ($item, $serviceRequests){
                if ($item2->state == null) {
                    unset($serviceRequests[$key]);
                }
            });
        });

        //$serviceRequests = $serviceRequests->values();

        foreach ($serviceRequests as $key => $serviceRequest) {
            // Traemos el proximo estado
            $serviceRequest->next_state = $this->serviceRequestRepository->getNextstateServiceRequest($serviceRequest);

            // Traemos el estado actual
            $serviceRequest->actual_state = $serviceRequest->statusHistory->last();

            // Traemos el estado de cerrado
            $serviceRequest->state_close = $this->serviceRequestRepository->getCloseStatusServiceRequest($serviceRequest->service_requests_types_id);

            // Historial completo
            //$serviceRequest->status_history_complete = $this->serviceRequestHistoryStatusWithPending($serviceRequest->id, $request);

        }

        $data = new LengthAwarePaginator(
            array_slice($serviceRequests->toArray(), $offset, $perPage),
            count($serviceRequests),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );

        return $data;
    }

    /**
     *
     * Este metodo se encarga de actualizar el estado
     * para la solicitud de servicio.
     *
     * @author Kevin Galindo | Santiago Torres
     */
    public function updateState($request)
    {
        // Agregamos la fecha
        $data = $request->all();
        $data['date'] = Carbon::now();

        // Creamos el registro
        $res = $this->serviceRequestRepository->addStateHistoryServiceRequest($data);

        // Si trae productos los agregamos
        if (!empty($data['products'])) {

            foreach ($data['products'] as $key => $product) {

                if ($product['quantity'] <= 0) {
                    continue;
                }

                $saveProduct = [
                    'service_requests_id' => $data['service_requests_id'],
                    'service_requests_state_id' => $data['service_requests_state_id'],
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'unit_value' => $product['unit_value'],
                ];

                $this->serviceRequestRepository->saveServiceRequestsProduct($saveProduct);
            }
        }

        // retornamos la informacion
        if ($res['code'] == '201') {
            // Traemos el registro de la solicitud
            $serviceRequest = $this->serviceRequestRepository->findById($request, $data['service_requests_id']);

            // Traemos el proximo estado
            $serviceRequest->next_state = $this->serviceRequestRepository->getNextstateServiceRequest($serviceRequest);

            // Traemos el estado actual
            $serviceRequest->actual_state = $serviceRequest->statusHistory->last();

            // Traemos el estado de cerrado
            $serviceRequest->state_close = $this->serviceRequestRepository->getCloseStatusServiceRequest($serviceRequest->service_requests_types_id);

            //$serviceRequest->status_history_complete = $this->serviceRequestHistoryStatusWithPending($serviceRequest->id, $request);


            $res['data'] = $serviceRequest;

            return $res;
        }

        return $res;
    }

    /**
     * Este metodo se encarga de validar el estado
     * que se mostrara en el modal
     *
     * @author Kevin Galindo
     */
    public function serviceRequestHistoryStatusWithPending($serviceRequest_id, $request)
    {
        $serviceRequest = $this->serviceRequestRepository->findById($request, $serviceRequest_id);
        $statusCompleted = $serviceRequest->statusHistory;
        $allStatus = $serviceRequest->type->states;

        // Recorremos todos los estados
        foreach ($allStatus as $keyStatu => $state) {

            $state->completed = false;

            // Recorremos los estado completados
            foreach ($statusCompleted as $key => $stateCompleted) {
                if ($state->id === $stateCompleted->service_requests_state_id) {
                    $state->completed   = true;
                    $state->nameUser    = $stateCompleted->user->name;
                    $state->date        = $stateCompleted->date;
                    $state->observation = $stateCompleted->observation;

                    if ($state->join_name) {
                        $res = $allStatus->search(function($item) use ($state) {
                            return $state->join_name === $item->join_name && $item->id !== $state->id ;
                        });

                        $allStatus->pull($res);
                    }
                }
            }

            if (!$state->completed && $state->join_name ) {
                $name = $state->join_name;
                $name .= "(".$state->name;

                foreach ($allStatus as $keyallStatus => $stateallStatus) {

                    if ($state->join_name_completed === true) {
                        continue;
                    }

                    if ($stateallStatus->join_name) {
                        if ($stateallStatus->join_name === $state->join_name &&
                            $stateallStatus->id !== $state->id) {
                            $name .= ', '.$stateallStatus->name;
                            $stateallStatus->join_name_completed = true;
                        }
                    }
                }

                $name .= ")";

                $state->name = $name;
            }

        }

        $returnAllStatus = $allStatus->filter(function($item) {
            return $item->join_name_completed !== true;
        });

        // // Eliminamos pasos ya hechos
        // foreach ($returnAllStatus as $key => $state) {
        //     if (!$state->completed) {
        //         $res = $allStatus->filter(function($item) use ($state){
        //             return $state->service_requests_state_id === $item->service_requests_state_id && $item->completed;
        //         })->first();

        //         if ($res) {
        //             $state->remove = true;
        //         }
        //     }
        // }

        // $returnAllStatus = $allStatus->filter(function($item) {
        //     return $item->remove !== true;
        // });

        return $returnAllStatus;
    }

    /**
     * Esta funcion se encarga de crear un
     * documento segun los parametros de la solicitud de servicio
     *
     * @author Santiago Torres
     */
    public function createDocument($request)
    {
        try {
            DB::beginTransaction();
            // Data
            $dataCreateDocument = $request;
            $serviceRequest = $this->serviceRequestRepository->find($dataCreateDocument->request_service_id);
            $serviceRequestActualState = $serviceRequest->statusHistory->last();

            $dataCreateDocument->request->add(["prefix" => VouchersType :: find($dataCreateDocument->vouchertype_id)->prefix]);
            $dataCreateDocument->request->add(["in_progress" => true]);
            $dataCreateDocument->request->add(["branchoffice_id" => BranchOfficeWarehouse::find($dataCreateDocument->branchoffice_warehouse_id)->branchoffice_id]);
            $dataCreateDocument->request->add(["documents_information" => ["price_list" => null]]);
            $dataCreateDocument->request->add(["observation" => $serviceRequest->type->name]);
            $dataCreateDocument->request->add(["from_service_request" => true]);

            // Crear documento
            $document = $this->documentRepository->create($dataCreateDocument->toArray());

            // Agregar producto
            $dataCreateDocument->request->add(["seller_id"=> null]);
            $dataCreateDocument->request->add(["document_id" =>$document->id]);
            $dataCreateDocument->request->add(["promotion_id" => null]);

            if ($serviceRequestActualState->service_requests_state_id == 10) {
                if ($serviceRequest->unit_value && $serviceRequest->unit_value > 0 ) {
                    $dataCreateDocument->request->add(["unit_value_before_taxes" => $serviceRequest->unit_value]);
                }
            }

            //
            if ($request->manage_inventory) {
                $dataCreateDocument->request->add(["manage_inventory" => $request->manage_inventory]);
            }

            if (
                $serviceRequestActualState->state->id == "38" ||
                $serviceRequestActualState->state->id == "39"
            ) {
                $serviceRequestsProducts = ServiceRequestsProduct::where('service_requests_id', $serviceRequest->id)->get();

                foreach ($serviceRequestsProducts as $key => $product) {

                    $dataCreateDocument->request->add(["product_id" => $product->product_id]);
                    $dataCreateDocument->request->add(["quantity" =>  $product->quantity]);
                    $dataCreateDocument->request->add(["original_quantity" =>  $product->quantity]);
                    $dataCreateDocument->request->add(["is_benefit" => false]);

                    $documentProduct = $this->documentsProductsService->createDocumentProduct($dataCreateDocument->all());
                }

            } else {
                $dataCreateDocument->request->add(["product_id" => $serviceRequest->product_id]);
                $dataCreateDocument->request->add(["quantity" =>  1]);
                $dataCreateDocument->request->add(["original_quantity" =>  1]);
                $dataCreateDocument->request->add(["is_benefit" => false]);

                $documentProduct = $this->documentsProductsService->createDocumentProduct($dataCreateDocument->all());
            }

            //finalizar el document
            $dataCreateDocument->request->add(["ret_fue_prod_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_prod_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_prod_total"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_serv_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_serv_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_serv_total"=> 0]);
            $dataCreateDocument->request->add(["ret_fue_total"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_prod_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_prod_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_prod_total"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_serv_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_serv_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_serv_total"=> 0]);
            $dataCreateDocument->request->add(["ret_iva_total"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_prod_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_prod_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_prod_total"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_serv_vr_limit"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_serv_percentage"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_serv_total"=> 0]);
            $dataCreateDocument->request->add(["ret_ica_total"=> 0]);
            $dataCreateDocument->request->add(["documentProductsDifferences" => null]);
            $dataCreateDocument->request->add(["observation" => $serviceRequest->type->name]);

            if ($serviceRequestActualState->state->id == "42") {
                $dataCreateDocument->request->add(["show_values_pdf" => false]);
            }

            $finalize = $this->documentsService->finalizeDocument($dataCreateDocument);

            $serviceRequestActualState->document_id = $document->id;
            $serviceRequestActualState->save();

            DB :: commit();
            return $finalize;
        } catch (\Exception $e) {
            DB::rollBack();

            //borramos el ultimo estado

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

}
