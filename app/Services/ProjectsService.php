<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Repositories\ProjectsRepository;
use App\Repositories\ParameterRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class ProjectsService
{
    private $projectsRepository;
    private $parameterRepository;

    public function __construct(ProjectsRepository $projectsRepository ,ParameterRepository $parameterRepository)
    {
        $this->projectsRepository = $projectsRepository;
        $this->parameterRepository = $parameterRepository;;
    }


}
