<?php

namespace App\Services;

use App\Mail\NotificationErrorCronMail;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CarteraService
{
    public function InitPersistDDBB()
    {
        set_time_limit(300);

        $error = false;
        $subject = 'ERROR EN EJECUCIÓN DE TAREAS CRON DE CARTERA !!!';
        $text = '';

        $dataBalance = Collect();
        $dataTransaction = collect();
        $filesBalance = collect();
        $filesTransactions = collect();

        $sftp = Storage::disk('sftp');

        $namesFiles = $sftp->files('WEB/PENDING');

        $sftp->getDriver()->getAdapter()->disconnect();

        //decido a que Array deben de ir cada nombre de archivo dependiendo del mismo
        foreach ($namesFiles as $key => $value) {
            $part = explode('/', $value);
            if (substr($part[count($part) - 1], 0, 6) == 'CARERP') {
                $filesBalance->push($part[count($part) - 1]);
            } else if (substr($part[count($part) - 1], 0, 6) == 'TRAERP') {
                $filesTransactions->push($part[count($part) - 1]);
            }
        }

        $allFiles = $filesBalance->merge($filesTransactions);

        if ($allFiles->count()) {
            Log::info('//--------------- Archivos encontrados de CARTERA en servidor SFTP: ' . implode(", ", $allFiles->toArray()));

            try {
                // Obtiene y almacena los archivos de DOCUMENTOS en el servidor local
                foreach ($filesBalance as $key => $i) {
                    $sftp->getDriver()->getAdapter()->connect();
                    $file = $sftp->get('WEB/PENDING/' . $i);
                    Storage::disk('local')->put('public/portfolio/' . $i, $file);
                    $sftp->delete('WEB/PENDING/' . $i);
                    $sftp->getDriver()->getAdapter()->disconnect();
                }

                // Obtiene y almacena los archivos de TRANSACCIONES en el servidor local
                foreach ($filesTransactions as $key => $elm) {
                    $sftp->getDriver()->getAdapter()->connect();
                    $file = $sftp->get('WEB/PENDING/' . $elm);
                    Storage::disk('local')->put('public/portfolio/' . $elm, $file);
                    $sftp->delete('WEB/PENDING/' . $elm);
                    $sftp->getDriver()->getAdapter()->disconnect();
                }

                $service_to_charge = new SyncDocumentsService();
                foreach ($filesBalance as $key => $v) {
                    $dataBalance->push($service_to_charge->syncData($v));
                }

                $instance = new SyncTransactionsService();
                foreach ($filesTransactions as $key => $element) {
                    $dataTransaction->push($instance->syncData($element));
                }

                Log::info('//--------------- Archivos de CARTERA procesados con éxito: ' . implode(", ", $allFiles->toArray()));
            } catch (\Exception $e) {
                $error = true;
                $text = 'NO SE HAN PODIDO PROCESAR LOS ARCHIVOS DE CARTERA';
                Log::critical('//---------------- ' . $text);
                Log::critical($e->getMessage());
            }
        } else {
            $error = true;
            $text = 'NO SE ENCONTRARON ARCHIVOS DE CARTERA Y/O TRANSACCIONES EN EL SERVIDOR SFTP';
            Log::critical('//---------------- ' . $text);
        }

        if ($error){
            $dayOfWeek = date('N');
            $lastDateSend = Cache::get('lastDateSendErrorCronInvoice', now()->addMinutes(-61)->format('Y-m-d H:i:s'));
            $hour = date('H');
            $lastHour = 17;

            if ($dayOfWeek > 5){
                $lastHour = 12;
            }

            if ($hour >= 8 && $hour <= $lastHour && $dayOfWeek < 7 && Carbon::parse($lastDateSend)->diffInMinutes(now()) > 60) {
                $emails = config('app.emails_notification_error_cron');

                $emailsAsArray = explode(';', $emails);

                Mail::to($emailsAsArray)->send(new NotificationErrorCronMail($text, $subject));

                Cache::put('lastDateSendErrorCronInvoice', now()->format('Y-m-d H:i:s'), 3600);
            }
        }

        $sftp->getDriver()->getAdapter()->disconnect();
    }
}
