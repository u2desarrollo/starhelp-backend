<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Entities\Account;
use App\Entities\Tax;
use App\Repositories\AccountRepository;
use App\Repositories\ParameterRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class AccountService
{
    private $accountRepository;
    private $parameterRepository;

    public function __construct(AccountRepository $accountRepository ,ParameterRepository $parameterRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->parameterRepository = $parameterRepository;;
    }

    public function fixACcountTotalize()
    {
        try {
            DB::beginTransaction();
            $request= (object)[
                'paginate' => false
            ];
    
            $accounts = $this->accountRepository->findAll($request);
    
            foreach ($accounts as $key => $account) {
                $this->setAccountIdTotalize($account);
            }
            
            DB :: commit();
            return 'El proceso de correccion ha finalizado exitosamente';

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return $e->getFile() . $e->getLine() . $e->getMessage();

        }
    }

    public function setAccountIdTotalize($account)
    {
        if (strlen($account->code) == 1) {
            return false;
        }
        $account_totalize = $this->accountRepository->getAccountTotalize($account);
        $this->accountRepository->updateAccountTotalize($account_totalize->id, $account->id);

    }

    /**
     * @author Santiago Torres
     */
    public function setAcoounts()
    {
        try {
            
            DB::beginTransaction();
            // convertimops la información del archivo a array
            $csv_info = $this->csvToArray(storage_path() . '/app/public/PLAN-CTAS.csv');
            // $data = [];
            foreach ($csv_info as $key => $info) {
                // si el nombre está vacio ponemos el código
                $name                       = trim($info["Nombre"]) ? trim($info["Nombre"]) :trim($info["Cod-Cta"]);
                // validamos si es debito o credito
                $natur                      = $this->validateNatur(trim($info["Natur"]));
                // seteamos el grupo
                $group                      = $this->getGroup($info);
                if ($group == NULL) {
                    throw new \Exception("No se ha encontrado el grupo ". trim($info["Grupo"]));
                }
                // seteamos el nivel
                $level                      = $this->parameterRepository->getParameter('C-002', trim($info["Nivel"]));
                // seteamos la categoria
                $category                   = isset($info["Categ"]) ? $this->parameterRepository->getParameter('C-003', trim($info["Categ"]) ? trim($info["Categ"]) : null ) : null;
                //  verificamos si maneja saldos tercero
                $manage_contact_balances    = trim($info["Man-Ter"]) == "SI" || trim($info["Man-Ter"]) == "Si" ? true : false;
                // maneja documento
                $manage_document_balances   = isset($info["Man-Doc"]) ? trim($info["Man-Doc"]) == "SI" ? true : false : false;
                // validamos si hay account_totalize sino ponemos la misma cuenta
                $account_id_totalize        = trim($info["Cta-Acum"]) ? Account :: where('code', trim($info["Cta-Acum"]))->first() : null;
                // seteamos tipo cartera 
                $wallet_type                = $this->parameterRepository->getParameter('C-004', trim($info["Tipo-Car"]) ? trim($info["Tipo-Car"]) : null );
                // validamos si maneja saldos por documento
                $quota_balances             = isset($info["Man-Cuot"]) ? $this->validatequota_balances(trim($info["Man-Cuot"])) : null;
                // centros de costo
                $cost_center_1              = isset($info["Cen-cos1"]) ? $this->parameterRepository->getParameter('C-010', trim($info["Cen-cos1"]) ? trim($info["Cen-cos1"]) : null ) : null;
                $cost_center_2              = isset($info["Cen-cos2"]) ? $this->parameterRepository->getParameter('C-011', trim($info["Cen-cos2"]) ? trim($info["Cen-cos2"]) : null ) : null;
                $cost_center_3              = isset($info["Cen-cos3"]) ? $this->parameterRepository->getParameter('C-012', trim($info["Cen-cos3"]) ? trim($info["Cen-cos3"]) : null ) : null;
                
                if (isset($info["Cen-cost"])) {
                    $cost_center_1 = $info["Cen-cost"] == 'Si' ? true : false;
                }

                // fecha apertura
                $account_created_at = isset($info["F-apert"]) ? substr($info["F-apert"], 0, 4).'-'.substr($info["F-apert"], 4, 2).'-'.substr($info["F-apert"], 6, 2).' 00:00:00' : null;

                // impuestos
                $tax = $this->getTaxes($info);

                // seteamos los valores que van a ir a la base de datos
                $data = [
                    'code'                      => trim($info["Cod-Cta"]),
                    'name'                      => utf8_encode($name),
                    'short_name'                => isset($info["Nombre-Corto"]) ? trim($info["Nombre-Corto"]) : null,
                    'description'               => isset($info["Descripcion"]) ? trim($info["Descripcion"]) : null,
                    'debit_credit'              => $natur,
                    'group'                     => $group,
                    'level'                     => $level,
                    'account_id_totalize'       => !is_null($account_id_totalize) ? $account_id_totalize->id : null, // buscar el id
                    'category'                  => $category,
                    'branchoffice_id'           => null, // revisar
                    'contact_id'                => null, // revisar
                    'manage_contact_balances'   => $manage_contact_balances,
                    'is_customer'               => $this->validateContactType('is_customer', $info),
                    'is_provider'               => $this->validateContactType('is_provider', $info),
                    'is_employee'               => $this->validateContactType('is_employee', $info),
                    'is_others'                 => $this->validateContactType('is_others', $info),
                    'resume_beginning_year'     => trim($info["Resu-Ter"]) == "SI" ? true : false,
                    'wallet_type'               => $wallet_type,
                    'document_description'      => null,
                    'manage_quota_balances'     => $quota_balances,
                    'cost_center_1'             => $cost_center_1,
                    'cost_center_2'             => $cost_center_2,
                    'cost_center_3'             => $cost_center_3,
                    'manage_businesses'         => $this->validateBusiness($info),
                    'manage_businesses_stage'   => isset($info["Etapas"]) ? trim($info["Etapas"])            ? true : false : false,
                    'manage_bank_document'      => isset($info["Concil"]) ? trim($info["Concil"]) == 'SI'    ? true : false : false,
                    'manage_way_to_pay'         => isset($info["Form-Pag"]) ? trim($info["Form-Pag"]) == 'SI'  ? true : false : false,
                    'tax_id'                    => $tax,
                    'Active_account'            => true,
                    'account_created_at'        => $account_created_at,
                    'account_cancellation_date' => null,
                    'pevious_account'           => trim($info["Cta-Anter"]) ? trim($info["Cta-Anter"]) : null,
                    'manage_document_balances'  => $manage_document_balances,
                ];
                $validate_account = Account :: where('code', $data["code"])->first();
                if (is_null($validate_account)) {
                    $account = $this->accountRepository->createAccount($data);
                }else{
                    $account = $this->accountRepository->updateAccount($validate_account->id, $data);
                }
            }
            
            DB :: commit();
            return $account;

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function getTaxes($info)
    {
        if (isset($info["Impuestos"])) {
            $code = null;
            switch ($info["Impuestos"]) {
                case 'ICA':
                    $code = 99;
                break;

                case 'IVA':
                    $code = 10;
                break;
                
                case 'RENTA':
                    $code = 98;
                break;

                case 'RETENCION DE ICA':
                    $code = 24;
                break;

                case 'RETENCION EN LA FUENTE':
                    $code = 20;
                break;
                
            }
            if (!is_null($code)) {
                $tax_ =  Tax :: select('id', 'code')->where('code', $code)->first();
                return !is_null($tax_) ? $tax_->id : null;
            }else{
                return null;
            }
        }
    }

    public function getGroup($info)
    {
        $code_group = trim($info["Grupo"]) == '' ? substr($info["Cod-Cta"], 0, 1) : trim($info["Grupo"]);
        return $this->parameterRepository->getParameter('C-001', $code_group);
    }

    public function validateBusiness($info)
    {
        switch (trim($info["Negocios"])) {
            case 'SI':
                return 1;
            break;

            case 'OB':
                return 3;
            break;
            
            default:
                return 0;
            break;
        }
    }
    public function validateContactType($type, $info)
    {
        $contact_types = explode('-', $info["TipoTercero"]);

        switch ($type) {
            case 'is_customer':
                return in_array('CLIENTES', $contact_types);
            break;

            case 'is_provider':
                return in_array('PROVEEDOR', $contact_types);
            break;

            case 'is_employee':
                return in_array('EMPLEADO', $contact_types);
            break;

            case 'is_others':
                return in_array('OTROS', $contact_types);
            break;
            
            default:
                return false;
            break;
        }
        
    }

    public function validatequota_balances($quota_balances)
    {
        if ($quota_balances != 'SI' && $quota_balances != 'NO') {
            return null;
        }
        if ($quota_balances != 'SI') {
            return true;
        }elseif ($quota_balances != 'NO') {
            return false;
        }
    }

    public function setmanage_contact_balances($manage_contact_balances)
    {
        if ($manage_contact_balances) {
            return true;
        }else{
            return false;
        }
    }

    public function validateNatur($info_natur)
    {
        if ($info_natur != "D" && $info_natur != "C") {
            throw new \Exception("la naturaleza debe ser 'D' ó 'C'");
        }
        if ($info_natur == "D") {
            $natur = 1;
        }elseif ($info_natur == "C") {
            $natur = 2;
        }
        return $natur;
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @return array
     * @param string $filename
     * @param string $delimiter
     * @author Santiago Torres
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
                    $row = preg_replace('!\s+!', '',  $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    public function createAccount($request)
    {
        try {
            // if (!$this->accountRepository->validateCode($request->code)) {
            //     throw new \Exception("El código " .$request->code. " ya existe");
            // }
            $this->accountRepository->validateCode($request->code);
            DB::beginTransaction();
            $data = $this->setDataUpdateOrCreate($request);
            $new_account = $this->accountRepository->createAccount($data);
            DB :: commit();
            return (object) [
                'code' => '201',
                'completed' => true,
                'data' => (object) [
                    'data' => $new_account,
                    'message' => 'Cuenta Creada Exitosamente '
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function updateAccount($id, $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->setDataUpdateOrCreate($request);
            $data['user_id_last_modification']=$request->user_id;
            $account = $this->accountRepository->updateAccount($id, $data);
            DB :: commit();
            return (object) [
                'code' => '200',
                'completed' => true,
                'data' => (object) [
                    'data' => $account,
                    'message' => 'Cuenta Actualizada Exitosamente '
                ]
            ];
            // return $account;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function setDataUpdateOrCreate($request)
    {
        return $data = [
            'code'                      => $request->code,
            'name'                      => strtoupper($request->name),
            'short_name'                => $request->short_name,
            'description'               => $request->description,
            'debit_credit'              => $request->debit_credit,
            'group'                     => $request->group,
            'level'                     => $request->level,
            'account_id_totalize'       => $request->account_id_totalize,
            'category'                  => $request->category,
            'branchoffice_id'           => $request->branchoffice_id,
            'contact_id'                => $request->contact_id,
            'manage_contact_balances'   => $request->manage_contact_balances != null ? ($request->manage_contact_balances == 1 ? true : false) : false , // bool,
            'is_customer'               => $request->is_customer != null ? ($request->is_customer == 1 ? true : false): false , // bool,
            'is_provider'               => $request->is_provider != null ? ($request->is_provider == 1 ? true : false): false , // bool,
            'is_employee'               => $request->is_employee != null ? ($request->is_employee == 1 ? true : false): false , // bool,
            'is_others'               => $request->is_others != null ? ($request->is_others == 1 ? true : false): false , // bool,
            'resume_beginning_year'     => $request->resume_beginning_year != null ? ($request->resume_beginning_year == 1 ? true : false): false , // bool,
            'wallet_type'               => $request->wallet_type,
            'document_description'      => $request->document_description,
            'manage_quota_balances'     => $request->manage_quota_balances != null ? ($request->manage_quota_balances == 1 ? true : false): false , // bool,
            'cost_center_1'             => $request->cost_center_1,
            'cost_center_2'             => $request->cost_center_2,
            'cost_center_3'             => $request->cost_center_3,
            'manage_projects'         => $request->manage_projects != null ? ($request->manage_projects == 1 ? true : false): false , // bool,
            'manage_projects_stage'   => $request->manage_projects_stage != null ? ($request->manage_projects_stage == 1 ? true : false): false , // bool,
            'manage_bank_document'      => $request->manage_bank_document != null ? ($request->manage_bank_document == 1 ? true : false): false ,  // bool,
            'manage_way_to_pay'         => $request->manage_way_to_pay != null ? ($request->manage_way_to_pay == 1 ? true : false): false , // bool,
            'tax_id'                    => $request->tax_account,
            'Active_account'            => $request->Active_account != null ? ($request->Active_account == 1 ? true : false): false , // bool,
            'pevious_account'           => $request->pevious_account,
            'results_account'           => $request->results_account,
            'manage_document_balances'  => $request->manage_document_balances != null ? ($request->manage_document_balances == 1 ? true : false): false , // bool,
            'manage_base_value'         => $request->manage_base_value != null ? ($request->manage_base_value == 1 ? true : false): false , // bool,
            'commissionable_concept'    => $request->commissionable_concept,
            'line_id'           => $request->line_id,
        ];
    }
    
    public function deleteAccount($id)
    {
        return $this->accountRepository->deleteAccount($id);
    }

}
