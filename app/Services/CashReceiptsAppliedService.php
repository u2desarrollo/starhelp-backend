<?php

namespace App\Services;

use App\Entities\CashReceiptsApp;
use App\Entities\Consignment;
use App\Mail\NotificationErrorCronMail;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class  CashReceiptsAppliedService
{
    public function __construct()
    {
    }


    public function updateCashReceipts($element)
    {
        CashReceiptsApp::where('id', $element[1])->update([
            'id_erp' => $element[2],

        ]);
    }

    public function set($element)
    {
        CashReceiptsApp::where('id', $element[1])->update([
            'id_erp' => $element[2],

        ]);
    }
    public function updateConsignement($element)
    {
        $consignement = Consignment::where('id', $element[1])->with('cashReceiptsApp')->first();
        foreach ($consignement->cashReceiptsApp as $key => $value) {
            $value->payment_applied_erp = $element[2];
            $value->save();
        }
        $consignement->id_erp = $element[3];
        $consignement->save();
    }


    public function syncData($nameFile)
    {
        $file = fopen(storage_path('app/public/CashReceipts/' . $nameFile), "r");

        // Recorremos todas las lineas del archivo hasta la ultima
        while (!feof($file)) {
            // // Leyendo una linea
            $line = fgets($file);
            // //sacando un array apartir de la linea extraida del archivo
            $array = explode("|", utf8_encode($line));
            // //el archivo puede venir con una linea  vacia con la funcion count puedo saber si la linea actual tiene datos
            if (count($array) > 1) {

                if ($array[0] == 'Rec' || $array[0] == 'Con') {

                    // de acuerdo al primer parametro puedo saber si el registro corresponde au rc ó a una consignacion
                    switch ($array[0]) {
                        case 'Rec':
                            $this->updateCashReceipts($array);
                            break;

                        case 'Con':
                            $this->updateConsignement($array);
                            break;
                    }
                }
            }
        }

        fclose($file);
    }


    /**
     *
     */
    public function InitPersistDDBB()
    {
        set_time_limit(300);

        $error = false;
        $subject = 'ERROR EN EJECUCIÓN DE TAREAS CRON DE RECIBOS DE CAJA !!!';
        $text = '';

        $responseCashReceipts = collect();

        $sftp = Storage::disk('sftp');

        $namesFiles = $sftp->files('WEB/PENDING');
        //decido a que Array deben de ir cada nombre de archivo dependiendo del mismo
        if (count($namesFiles) > 0) {
            foreach ($namesFiles as $key => $value) {
                $part = explode('/', $value);
                if (substr($part[count($part) - 1], 0, 6) == 'APPERP') {
                    $responseCashReceipts->push($part[count($part) - 1]);
                }
            }
        }

        if (count($responseCashReceipts) > 0) {
            try {
                Log::info('//--------------- Archivos encontrados de RECIBOS DE CAJA en servidor SFTP: ' . implode(", ", $responseCashReceipts->toArray()));

                foreach ($responseCashReceipts as $key => $i) {
                    $file = $sftp->get('WEB/PENDING/' . $i);
                    Storage::disk('local')->put('public/CashReceipts/' . $i, $file);
                }

                // Cierra lo conexión sftp
                $sftp->getDriver()->getAdapter()->disconnect();

                foreach ($responseCashReceipts as $key => $value) {
                    $this->syncData($value);
                    Storage::disk('local')->delete('public/CashReceipts/' . $value);
                    $sftp->move('WEB/PENDING/' . $value, 'WEB/SUCCESS/' . $value);
                }

                Log::info('//--------------- Archivos de RECIBOS DE CAJA procesados con éxito: ' . implode(", ", $responseCashReceipts->toArray()));
            } catch (FileNotFoundException $e) {
                $error = true;
                $text = 'NO SE HAN PODIDO PROCESAR LOS ARCHIVOS DE RECIBOS DE CAJA';
                Log::critical('//---------------- ' . $text);
                Log::critical($e->getMessage());
            }
        } else {
            $error = true;
            $text = 'NO SE ENCONTRARON ARCHIVOS DE RECIBOS DE CAJA EN EL SERVIDOR SFTP';
            Log::critical('//---------------- ' . $text);
        }

        if ($error){
            $dayOfWeek = date('N');
            $lastDateSend = Cache::get('lastDateSendErrorCronCashReceipts', now()->addMinutes(-61)->format('Y-m-d H:i:s'));
            $hour = date('H');
            $lastHour = 17;

            if ($dayOfWeek > 5){
                $lastHour = 12;
            }

            if ($hour >= 8 && $hour <= $lastHour && $dayOfWeek < 7 && Carbon::parse($lastDateSend)->diffInMinutes(now()) > 60) {
                $emails = config('app.emails_notification_error_cron');

                $emailsAsArray = explode(';', $emails);

                Mail::to($emailsAsArray)->send(new NotificationErrorCronMail($text, $subject));

                Cache::put('lastDateSendErrorCronCashReceipts', now()->format('Y-m-d H:i:s'), 3600);
            }
        }

    }
}
