<?php

namespace App\Services;

// Entities
use App\Entities\Document;
use App\Entities\DocumentProduct;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\Contact;
use App\Entities\ContributionInvoicesApp;
use App\Repositories\DocumentRepository;
// Repositories
use App\Repositories\DocumentsBalanceRepository;

// Otros
use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Support\Collection;

class DocumentsBalanceService
{

    private $documentsBalanceRepository;

    public function __construct(DocumentsBalanceRepository $documentsBalanceRepository)
    {
        $this->documentsBalanceRepository = $documentsBalanceRepository;
    }

    public function getTrackingByContactId($contact_id)
    {
        if (!Contact::where('id', $contact_id)->exists()) {
            return [
                'data' => [
                    'message' => 'Registro no existe'
                ],
                'code' => '404'
            ];
        }

        $yearMonth = Carbon::now()->format('Y-m');

        $totalBalance = 0;

        // Traemos las facturas del documento
        $documents = Document::select(
            'id',
            'vouchertype_id',
            'document_date',
            'issue_date',
            'due_date',
            'total_value',
            'consecutive',
            'prefix',
            'thread',
            'branchoffice_id'
        )
            ->with([
                // 'invoices' => function ($query) use ($yearMonth) {
                //     $query->select('id', 'vouchertype_id', 'document_date', 'due_date', 'total_value', 'consecutive', 'thread');
                //     $query->with([

                'receivable' => function ($query) use ($yearMonth) {
                    $query->where('invoice_balance', '>', 0);
                },
                'transactions' => function ($query) {
                    $query->orderBy('document_date', 'asc')->orderBy('id', 'asc')->with([
                        'document' => function ($query) {
                            $query->with([
                                'vouchertype'
                            ]);
                        }
                    ]);
                },
                'vouchertype',
                'documentsProducts'
                //]);

                //  $query->where(function ($query) {
                //     $query->where('vouchertype_id', '=', '2');
                // $query->orWhere('vouchertype_id', '=', '7');
                //  });

                //       $query->orderBy('id', 'ASC');
                // }
            ])
            ->whereHas('receivable', function ($query) use ($yearMonth) {
                $query->where('invoice_balance', '>', 0);
            })
            ->where('operationType_id', 5)
            ->where('contact_id', $contact_id)
            ->get();

        foreach ($documents as $key => $document) {
            $documentDate=explode(' ',$document->document_date);
            $document->issue_date=$documentDate[0];
            $a = new DocumentRepository(new Container);
            $valueDPPAndDays = $a->applyDctPPToInvoices($documents, $document);
            $document->dctpp = $valueDPPAndDays[0];

            //hallar dias de factura
            $datework = strtotime(date($document->document_date));
            $now = strtotime(date("Y-m-d"));
            $document->days_documents = 0;
            if ($datework < $now) {
                $datework = $document->document_date;
                $now = Carbon::now();
                $document->days_documents = $now->diffInDays($datework);
                
            }

            $balance = 0;

            foreach ($document->transactions as $key => $transaction) {

                if (trim($transaction->transaction_type) == 'D') {
                    $balance += $transaction->transaction_value;
                } elseif (trim($transaction->transaction_type) == 'C' && $transaction->payment_applied_erp == 1) {
                    $balance -= $transaction->transaction_value;
                }

                $transaction->balance = $balance;
            }

        }

        $documents = $documents->sortByDesc('expiration_days')->values()->all();

        return [
            'data' => $documents,
            'code' => 200
        ];
    }
}
