<?php


namespace App\Services;

use App\Entities\ProductAttachment;
use App\Repositories\ProductAttachmentRepository;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

/**
 * Class ProductAttachmentService
 *
 * @package App\Services
 */
class ProductAttachmentService extends BaseService
{
    /**
     * @var ProductAttachmentRepository
     */
    private $product_attachment_repository;

    /**
     * ProductAttachmentService constructor.
     *
     * @param ProductAttachmentRepository $product_attachment_repository
     */
    public function __construct(ProductAttachmentRepository $product_attachment_repository)
    {
        $this->product_attachment_repository = $product_attachment_repository;
    }

    /**
     * Guarda y redimensiona una imagen asociada a un producto
     *
     * @param Request $request Contiene la imagen a almacenar
     * @param int $product_id Contiene el id del producto
     * @return LengthAwarePaginator|Collection|mixed Contiene el registro de base de datos
     * @throws Exception
     * @author Jhon García
     */
    public function storeImages(Request $request, $product_id)
    {
        try {
            DB::beginTransaction();

            // Variables
            $original_name = $request->file('file')->getClientOriginalName();
            $extension = $request->file('file')->getClientOriginalExtension();
            $uuid = Str::uuid()->toString();
            $new_name = $uuid . '.' . $extension;

            // Almacenar la imagen en tamaño real para el sistema web
            $request->file('file')->storeAs('public/products/web', $new_name);

            // Con base en la imagen original, redimensionar la imagen para el app
            $resized_image = Image::make(storage_path('app/public/products/web/' . $new_name))
                ->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode($extension, 75);

            // Guardar imagen redimensionada
            $resized_image->save(storage_path('/app/public/products/app/' . $new_name));

            $main = !ProductAttachment::where('product_id', $product_id)
                ->where('main', true)
                ->first();

            // Construir array con información a registrar en base de datos
            $data = [
                'product_id'  => $product_id,
                'description' => $original_name,
                'type'        => 'i',
                'url'         => $new_name,
                'main'        => $main
            ];

            // Guardar product attachment
            $product_attachment = $this->product_attachment_repository->create($data);

            DB::commit();

            return $product_attachment;

        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
            throw new Exception('Error al guardar imagen', 400);
        }
    }


}
