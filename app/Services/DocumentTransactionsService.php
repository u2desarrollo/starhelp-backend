<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Entities\VouchersType;
use App\Entities\Template;
use App\Entities\Document;
use App\Entities\TypesOperation;
use App\Entities\Account;
use App\Entities\BalancesForContact;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\DocumentTransactions;
use App\Repositories\DocumentTransactionsRepository;
use App\Repositories\ParameterRepository;
use App\Repositories\DocumentRepository;
use App\Repositories\VouchersTypeRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class DocumentTransactionsService
{
    private $documentTransactionsRepository;
    private $parameterRepository;
    private $documentRepository;
    private $ouchersTypeRepository;

    public function __construct(DocumentTransactionsRepository $documentTransactionsRepository ,ParameterRepository $parameterRepository, DocumentRepository $documentRepository, VouchersTypeRepository $vouchersTypeRepository)
    {
        $this->documentTransactionsRepository = $documentTransactionsRepository;
        $this->parameterRepository = $parameterRepository;
        $this->documentRepository = $documentRepository;
        $this->vouchersTypeRepository = $vouchersTypeRepository;
    }

    /**
     * creacion/edicion de documento para partidas(document_transaction)
     * creacion de partidas(document_transaction)
     * @author Santiago Torres
     */
    public function store($request)
    {
        try {
            DB::beginTransaction();

                if ($request->document_id) {
                    $document = $this->documentRepository->updateDocumentForDocumentTransaction($request->document_id , $request);
                }else{
                    $document = $this->documentRepository->createDocumentForDocumentTransaction($request);
                }

                if (isset($request->document_transaction["id_document_transactions"])) {
                    $document_transaction = $this->documentTransactionsRepository->update_($request->document_transaction["id_document_transactions"], $request, $document);
                }else{
                    $document_transaction = $this->documentTransactionsRepository->store($request, $document);
                }


                // $voucher_type = $this->vouchersTypeRepository->updateConsecutive($document->vouchertype_id, $document->consecutive);//de ultimas se actualiza el tipo de comprobante
            DB::commit();
            return (object) [
                'code' => '200',
                'completed' => true,
                'data' => (object) [
                    'message' => 'exito',
                    'data'  => $document_transaction
                ]
            ];

        }catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creación del documento: '
                ]
            ];
        }
    }

    /**
     * funcion para crear partidas de un csv
     * @author Santiago Torres
     *
     */
    public function setTransactionsFromCsv()
    {
        $line_csv = 0;
        try {
            DB::beginTransaction();
            $csv_info = $this->csvToArray(storage_path() . '/app/public/transac_100.csv');
            foreach ($csv_info as $key => $dt) {
                $line_csv = $key;
                $document= null;
                if ($dt["Id-Web"] == "0") {
                    $document = $this->createDocument($dt);
                }else{
                    $document = Document :: find($dt["Id-Web"]);
                }
                $document_transaction = $this->createDocumentTransaction($dt, $document);
            }
            $this->setCrossingId();
            DB :: commit();
            return 'se han creado exitosamente';
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info('/////// Error Proceso masivo de partidas /////////');
            Log::info($line_csv.' '.$e->getFile().' '.$e->getLine().' '.$e->getMessage());
            Log::info('/////// //////////////////////////////// /////////');

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creación del documento: '
                ]
            ];
        }
    }

    public function setCrossingId()
    {
        $documents_transactions = DocumentTransactions :: select('id', 'document_id', 'crossing_consecutive' ,'affects_prefix_document')->get();
        foreach ($documents_transactions as $key => $documents_transaction) {
            $document = Document :: select('id', 'consecutive', 'prefix')->where('in_progress' , false)->where('consecutive' , $documents_transaction->crossing_consecutive)->where('prefix' , $documents_transaction->affects_prefix_document)->first();
            if (!is_null($document)) {
                $documents_transaction->crossing_document_id = $document->id;
                $documents_transaction->save();
            }
        }
    }

    /**
     * crear partidas desde el excel
     * @author Santiago torres
     */
    public function createDocumentTransaction($dt, $document)
    {
        if (is_null($document)) {
            return true;
        }
        $contact_idetification = explode('-', $dt["Cod-Ter-dt"]);
        $contact_idetification = $contact_idetification[0];
        $contact = Contact :: select('id')->where('identification', $contact_idetification)->first();
        if (is_null($contact)) {
            $data_contact=[
                "Cod-Ter" => $dt["Cod-Ter-dt"],
                "Nombre-Ter" => $dt["Nombre-Ter-dt"],
                "Grup" => $dt["Grup-dt"],
            ];
            $contact = $this->createContact($data_contact);
        }
        $account = Account :: select('id')->where('code' ,$dt["Cta-Puc"])->first();
        if (is_null($account)) {
            return true;
        }
        $due_date = strlen($dt["Fec-Ven"]) == 8 ? substr($dt["Fec-Ven"], 0, 4) . '-' . substr($dt["Fec-Ven"], 4, 2) . '-' . substr($dt["Fec-Ven"], 6, 2). ' 00:00:00' : null;
        if($dt["Fec-Ven"] == 20210230 || $dt["Fec-Ven"] == 20210229 || $dt["Fec-Ven"] == 20024312 || !checkdate(intval(substr($dt["Fec-Ven"], 4, 2)) , intval(substr($dt["Fec-Ven"], 6, 2)), intval(substr($dt["Fec-Ven"], 0, 4))) ){
            $due_date = '2021-02-28 00:00:00';
        }

        $year_month = substr(str_replace('-', '' , $document->document_date), 0, 6);
        $document_transaction = [
            'crossing_consecutive' => $dt["num-fact"],//cruce
            'affects_prefix_document' => $dt["Pref-FAC"],//cruce
            'number_bussiness' => $dt["Negocio"],
            'year_month' => $year_month,
            'document_date' => $document->document_date,
            'consecutive' => $document->consecutive,
            'contact_id' => $contact->id,
            'account_id' => $account->id,
            'detail'  => preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $dt["Detalle-dt"]),
            'refer_detail'  => preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $dt["Detalle-Refer"]),
            'due_date' => is_null($due_date) ? $document->document_date : $due_date,
            // 'pay_day' => $dt["Plazo"],
            'operation_value' => str_replace('.', '', $dt["Vr-Oper"]),
            'transactions_type' => $dt["T"] == 'D' ? 'D' : 'C',
            'base_value' => strlen(trim($dt["Vr-Base"])) == 0 ? null : $dt["Vr-Base"] ,
            'document_id'=> $document->id,
            'vouchertype_id' => $document->vouchertype_id,
        ];

        return DocumentTransactions :: create($document_transaction);
    }


    /**
     * crea documento si id web == 0
     * @author Santiago Torres
     */
    public function createDocument($dt)
    {
        $branchOffice_id = 305;//todos son sede bogotá
        $vouchersType = VouchersType :: select('id')->where('branchoffice_id', $branchOffice_id)->where('code_voucher_type', $dt["TipoComprobante"])->first();
        if (is_null($vouchersType)) {
            $vouchersType = $this->createVouchersType($dt);
        }
        $document = Document :: where('in_progress', false)->where('consecutive', $dt["Consecutivo"])->where('branchoffice_id', 305)->where('vouchertype_id', $vouchersType->id)->first();
        if (is_null($document)) {
            $contact_csv = explode('-',$dt["Cod-Ter"]);
            $contact_idetification = $contact_csv[0];
            $contact = Contact :: select('id')->where('identification', $contact_idetification)->first();
            if (is_null($contact)) {
                $data_contact=[
                    "Cod-Ter" => $dt["Cod-Ter"],
                    "Nombre-Ter" => $dt["Nombre-Ter"],
                    "Grup" => $dt["Grup"],
                ];
                $contact = $this->createContact($data_contact);
            }
            $contact_warehouse = null;
            if (count($contact_csv) > 0) {
                if(isset($contact_csv[1])){
                    $contact_warehouse = ContactWarehouse :: select('id')->where('contact_id',$contact->id)->where('code', trim($contact_csv[1]))->first();
                }
            }
            $model = Template :: select('id')->where('code', $dt["Modelo"])->first();
            if (is_null($model)) {
                $model = $this->createTemplate($dt);
            }
            $date = substr($dt["Fec-Com"], 0, 4) . '-' . substr($dt["Fec-Com"], 4, 2) . '-' . substr($dt["Fec-Com"], 6, 2). ' 00:00:00';
            $typesOperation = TypesOperation :: select('id')->where('code', trim($dt["Op-Inv"]))->first();
            $seller = Contact :: select('id')->where('identification', $dt["Vended"])->first();
            $code_branchoffice_warehouse = strlen(trim($dt["Bodega"])) == 1 ? '0'.trim($dt["Bodega"]) : trim($dt["Bodega"]);
            $branchoffice_warehouse = BranchOfficeWarehouse :: select('id')->where('branchoffice_id', 305)->where('warehouse_code', $code_branchoffice_warehouse )->first();
            $document_base = Document :: select('id')->where('prefix', trim($dt["Pref-BASE"]))->where('consecutive', $dt["No-DocBase"])->first();
            $data = [
                'vouchertype_id' => $vouchersType->id,
                'consecutive' =>  $dt["Consecutivo"],
                'document_date' => $date,
                'contact_id' => $contact->id,
                'warehouse_id' => !is_null($contact_warehouse) ? $contact_warehouse->id : null,
                'model_id' => $model->id,
                'branchoffice_id'=>$branchOffice_id,
                'operationType_id' => !is_null($typesOperation) ? $typesOperation->id : null,
                'seller_id' => !is_null($seller) ? $seller->id : null,
                'branchoffice_warehouse_id' => !is_null($branchoffice_warehouse) ? $branchoffice_warehouse->id : null,
                'observation' => utf8_decode($dt["Detalle"]),
                'observation_two' =>utf8_decode($dt["Detalle-Doc"]),
                'thread' => !is_null($document_base) ? $document_base->id : null,
                'in_progress'=> false,
                'accounting_debit_value'=> str_replace( '.', '' ,$dt["Total-DB"]),
                "issue_date" => Carbon :: now(),
                'accounting_credit_value'=> str_replace( '.', '' ,$dt["Total-CR"])
            ];
            $document = Document :: create($data);
        }
        return $document;
    }

    /**
     * crear tipo de comprobante
     * @author santiago torres
     */
    public function createVouchersType($dt)
    {
        return VouchersType :: create ([
            'branchoffice_id' => 305,
            'code_voucher_type' => $dt["TipoComprobante"],
            'name_voucher_type' => utf8_decode($dt["TipoCompr"]),
            'consecutive_number' => 0,
            'electronic_bill' => false,
            'affectation' => 1,
            'state' => true
        ]);
    }


    /**
     * crear modelo
     */
    public function createTemplate($dt)
    {
        return Template::create([
            'code'=>$dt["Modelo"],
            'description'=> utf8_decode($dt["TipoCompr"])
        ]);
    }

    /**
     * crea tercero
     * @author santiago torres
     */
    public function createContact($dt)
    {
        $identification = explode('-',$dt["Cod-Ter"]);
        return Contact :: create([
            'subscriber_id'=>1,
            'name'=> utf8_decode($dt["Nombre-Ter"]),
            'is_customer' => $dt["Grup"] ==  '10' || $dt["Grup"] == '30' ? true : false,
            'is_provider' => $dt["Grup"] ==  '20' || $dt["Grup"] == '30' ? true : false,
            'is_employee' => $dt["Grup"] ==  '60' ? true : false,
            'identification' => $identification[0],
            'identification_type'=>1
        ]);
    }
    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @return array
     * @param string $filename
     * @param string $delimiter
     * @author Santiago Torres
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
                    $row = preg_replace('!\s+!', '',  $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }


    public function auxiliaryBook($request)
    {
        $documents_transactions = $this->documentTransactionsRepository->auxiliaryBook($request);
        return [
            'data' => $documents_transactions
        ];
    }
    
    public function auxiliaryBookContact($request)
    {
        $documents_transactions = $this->documentTransactionsRepository->auxiliaryBookContact($request);
        $documents_transactions = $this->setBalancesAuxiliaryBookContact($documents_transactions, $request);
        return [
            'data' => $documents_transactions
        ];
    }

    public function reportCommissions($request)
    {
        $documents_transactions = $this->documentTransactionsRepository->reportCommissions($request);
        return [
            'data' => $documents_transactions
        ];
    }

    /**
     * Lista de transacciones cartera
     * @author Santiago Torres
     */
    public function getTransactionsWallet($id, $request)
    {
        $documents_transactions = $this->documentTransactionsRepository->getTransactionsWallet($id, $request);
        $balance = 0;
        foreach ($documents_transactions as $key => $document_transaction) {
            if ($document_transaction->debits != 0) {
                $balance += $document_transaction->debits;
            }elseif ($document_transaction->credits != 0) {
                $balance -= $document_transaction->credits;
            }
            $document_transaction->balance = $balance;
            $document_transaction->voucher = empty(trim($document_transaction->document->prefix)) ? $document_transaction->document->consecutive: $document_transaction->document->prefix . '-' . $document_transaction->document->consecutive;
//            $document_transaction->affects_document_date = $document_transaction->affectsDocument->document_date;
        }
        return $documents_transactions;
    }

    public function setBalancesAuxiliaryBookContact($document_transactions_, $request)
    {
        $document_transactions_list = [];
        // lista de cuentas-tercero para buscar el saldo solo una vez por cuenta-tercero
        $account_id_list = [];
        //seteamos lafecha
        $date = new Carbon(substr($request->year_month_from, 0, 4).'-'.substr($request->year_month_from, 4, 2));
        // restamos un mes y obtenemos la fecha en formato YYYYMM
        $year_month = $date->addMonth(-1)->format('Ym');
        // variable para calcular el saldo
        $control_balance = 0;

        foreach ($document_transactions_ as $key => $dt) {

            // validamos si el id de la cuenta-tercero no esta en el array
            if(!in_array($dt->account_id.'-'.$dt->contact_id, $account_id_list)){
                //Buscamos el saldo final del mes anterior
                $balance = BalancesForContact :: select('id', 'final_balance')->where('account_id', $dt->account_id)->where('contact_id', $dt->contact_id)->where('year_month', $year_month)->first();
                $control_balance = !is_null($balance) ? $balance->final_balance : 0 ;
                //gurdamos el id de la cuenta en el array para no buscar de nuevo el saldo
                $account_id_list[] = $dt->account_id.'-'.$dt->contact_id;
                // agregamos a las partidas el saldo
                $document_transactions_list[] = (object) ['balance' =>  $control_balance];
            }

            //calculamos el saldo de la partida si es debito suma, credito resta
            if ($dt->transactions_type == 'D') {
                $dt->balance = $control_balance + $dt->operation_value;
            }elseif ($dt->transactions_type == 'C') {
                $dt->balance = $control_balance - $dt->operation_value;
            }


            $control_balance = $dt->balance;
            $document_transactions_list[] = $dt;
        }
        return $document_transactions_list;
    }
}
