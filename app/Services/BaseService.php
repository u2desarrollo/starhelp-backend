<?php


namespace App\Services;


use App\Entities\BranchOfficeWarehouse;
use App\Entities\Template;
use Illuminate\Support\Facades\Cache;

class BaseService
{
    /**
     * Obtiene una bodega por el id de la sede y el código de la bodega
     *
     * @param int $branchoffice_id Contiene el id de la sede
     * @param string $warehouse_code Contiene el id de la bodega
     * @return mixed Contiene el objeto de la bodega obtenida
     * @author Jhon Garcíam
     */
    public function getBranchOfficeWarehouse(int $branchoffice_id, string $warehouse_code)
    {
        return Cache::remember('branch_office_warehouse_' . $branchoffice_id . '_' . $warehouse_code, 1, function () use ($warehouse_code, $branchoffice_id) {
            return BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)
                ->where('warehouse_code', $warehouse_code)
                ->first();
        });
    }

    /**
     * Obtiene un modelo por el id del mismo
     *
     * @param int $model_id Contiene el id del modelo a consultar
     * @return mixed Contiene el objeto del modelo obtenido
     * @author Jhon García
     */
    public function getModelForIdFromCache(int $model_id)
    {
        return Cache::remember('model_' . $model_id, 1, function () use ($model_id) {
            return Template::select('*')
                ->with([
                    'priceList' => function ($query) {
                        $query->with([
                            'paramtable'
                        ]);
                    },
                    'voucherType',
                    'operationType',
                    'branchOfficeWarehouse'
                ])
                ->find($model_id);
        });
    }
}
