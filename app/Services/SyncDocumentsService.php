<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\DB;

class SyncDocumentsService extends Service
{

    public function __construct()
    {

        parent::__construct();

        $this->code_branchoffice = 5;
        $this->code_voucher_type = 6;
        $this->prefix = 7;
        $this->consecutive = 8;
        $this->transaction_type = 9;
        $this->document_date = 10;
        $this->issue_date = 10;
        $this->due_date = 11;
        $this->total_value = 12;
        $this->invoice_balance = 13;
        $this->identification_seller = 14;
        $this->name_seller = 15;
        $this->observation = 16;
        $this->account_code = 17;
        $this->erp_id_document = 18;
        $this->erp_id_receivable = 19;
        $this->software_siigo = 20;
    }

    public function syncData($nameFile)
    {
        //abro mi archivo
        if (is_null($nameFile)) {
            $file = fopen(storage_path('app/public/portfolio/CARERP-20200810-181922.csv'), "r");
        } else {
            $file = fopen(storage_path('app/public/portfolio/' . $nameFile), "r");
        }

        Log::info('//--------------- Procesando archivo de cartera: ' . $nameFile);
        // la variable &cant realiza un conteo con el motivo de saber en que momento ya vienen datos diferentes al encabezado de mi archivo
        $cant = 0;
        $cantProcesados = 0;

        try {
            DB::beginTransaction();

            DB::table('documents_balance')->update(['update_or_create' => 0]);

            // Recorremos todas las lineas del archivo hasta la ultima
            while (!feof($file)) {
                // Leyendo una linea
                $line = fgets($file);
                //sacando un array apartir de la linea extraida del archivo
                $this->array = explode(";", utf8_encode($line));
                //el archivo puede venir con una linea  vacia con la funcion count puedo saber si la linea actual tiene datos
                if ($cant > 0 && (count($this->array) > 1)) {
                    $this->cleanSpaces();
                    $this->branchofficeTreatment();
                    $this->voucherTratment();
                    $this->sellersTreatment();
                    $this->thirdPartyTreatment();
                    $this->contactWarehouseTreatment();
                    $this->documentTratment();
                    $this->receivableTratment();
                    $cantProcesados++;
                }
                $cant++;
            }

            if ($cantProcesados > 0) {
                DB::table('documents_balance')->where(['update_or_create' => 0])->delete();
            }

            DB::commit();
            Log::info("//--------------- Archivo de cartera $nameFile procesado con exito. Total registros: $cant");
        } catch (\Exception $th) {
            Log::error("//--------------- Error in the line " . $th->getLine() . " of " . $th->getFile() . " procesando archivo de cartera: " . $th->getMessage());
            DB::rollBack();
        } finally {
            // Cerrando el archivo
            fclose($file);
            //elimino el archivo del storage de mi app
            if (!is_null($nameFile)) {
                Storage::disk('local')->delete('public/portfolio/' . $nameFile);
            }
        }
    }


    public function documentTratment()
    {
        //creo un objeto de tipó Document y valido si existe en caso de no, lo creo y lo retorno
        //un tipo de deocumento es la factura y el otro es el movimiento se distinguen por voucher

        $document = [
            'branchoffice_id' => $this->branchoffice->id,
            'vouchertype_id' => $this->voucher_type->id,
            'consecutive' => intval($this->array[$this->consecutive]),
            'document_date' => $this->transformDate($this->array[$this->document_date]),  // este elemento contiene un mutado,
            'warehouse_id' => $this->contactWarehouse->id,
            'prefix' => $this->array[$this->prefix],
            'erp_id' => $this->array[$this->erp_id_document],
            'inventory_operation' => 'Por definir',
            'document_state' => 1,
            'total_value' => $this->array[$this->total_value],
            'contact_id' => $this->contact->id,
            'seller_id' => is_null($this->seller) ? null : $this->seller->id,
            'document_last_state' => date('Y-m-d'),
            'observation' => $this->array[$this->observation],
            'issue_date' => $this->transformDate($this->array[$this->issue_date]),
            'due_date' => $this->transformDate($this->array[$this->due_date]),
            'operationType_id' => 5,
            'update_or_create' => 1,
            'software_siigo' => $this->array[$this->software_siigo] == 'SIIGO' ? true : false,
            'deleted_at' => null,
            'in_progress' => false
        ];

        $this->document = DB::table('documents')->where(Arr::only($document, [
            'branchoffice_id', 'vouchertype_id', 'consecutive', 'prefix', 'operationType_id', 'deleted_at', 'in_progress'
        ]))->first();

        if (!$this->document) {
            DB::table('documents')->insert($document);
        } elseif ($this->array[$this->software_siigo] == 'SIIGO') {
            $this->document = DB::table('documents')->where(Arr::only($document, [
                'branchoffice_id', 'vouchertype_id', 'consecutive', 'prefix', 'operationType_id', 'deleted_at'
            ]))->update(Arr::only($document, ['contact_id', 'due_date','document_date']));
        }

        $this->document = DB::table('documents')->where(Arr::only($document, [
            'branchoffice_id', 'vouchertype_id', 'consecutive', 'prefix', 'operationType_id', 'deleted_at', 'in_progress'
        ]))->first();
    }

    public function receivableTratment()
    {
        //creo un objeto de tipó Receivable y valido si existe en caso de no, lo creo y lo retorno
        $receivable = [
            'document_id' => $this->document->id,
            'year_month' => $this->transformYearMonth($this->array[$this->year_month]),
            'account_code' => $this->array[$this->account_code],
            'document_date' => $this->transformDate($this->array[$this->year_month]),
            'contact_code' => $this->contact->id,
            'document_code' => $this->document->consecutive,
            'invoice_balance' => round(str_replace(' ', '', $this->array[$this->invoice_balance])),
            'erp_id' => $this->array[$this->erp_id_receivable],
            'quota_number' => 1,
            'update_or_create' => 1
        ];

        DB::table('documents_balance')
            ->updateOrInsert(Arr::only($receivable, ['document_id', 'erp_id']), $receivable);
    }
}
