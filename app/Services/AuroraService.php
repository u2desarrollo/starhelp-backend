<?php

namespace App\Services;

use App\Entities\Aurora;
use App\Repositories\AuroraRepository;
use App\Repositories\ContactEmployeeRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;

// use App\Repositories
use App\Services\DocumentsService;
use Validator;

class AuroraService
{
    private $auroraRepository;
    private $documentsService;
    private $contactEmployeeRepository;

    public function __construct(AuroraRepository $auroraRepository, DocumentsService $documentsService, ContactEmployeeRepository $contactEmployeeRepository )
    {
        $this->auroraRepository = $auroraRepository;
        $this->documentsService = $documentsService;
        $this->contactEmployeeRepository = $contactEmployeeRepository;
    }

    public function createProcess($request)
    {
        try {
            DB::beginTransaction();
            $request = $request->toArray();
            $request["email"] = json_encode($request["email"]);
            $aurora =  Aurora :: updateOrCreate(['id'=> $request["id"]] ,Arr::except($request, ['options_select', 'Hour_input', 'minutes', 'id']));
            DB :: commit();
            return (object) [
                'code' => '200',
                'data' => (object) [
                    'data'=>$aurora,
                    'message' => 'Se Ha creado Exítosamente'
                ]
            ];
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            DB::rollBack();
            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error al crear proceso'
                ]
            ];
        }

    }

    public function deleteAccount($id)
    {
        Aurora :: find($id)->delete();
        return (object) [
            'code' => '200',
            'data' => (object) [
                'data'=> '',
                'message' => 'Se Ha Eliminado Exítosamente'
            ]
        ];
    }

    /**
     * Este metodo inicializa aurora
     * para validar la configuracion para realizar ciertos procesos. 
     * @author Kevin Galindo
     */
    public function aurora()
    {
        $configAurora = Aurora::where('active', true)
        ->where('id', 3)
        ->orderBy('id')
        ->get();
        
        foreach ($configAurora as $key => $config) {

            $execute = $this->validateFrequency($config);

            if ($execute) {
                $state = $this->validateOperation($config);
                echo "PROCESO: ".$config->option_->name_parameter."ESTADO:".$state;
            }

        }

        echo "-- FIN AURORA --";
    }

    /**
     * Este metodo se encarga de validar si un
     * proceso se debe ejecutar o no dependiendo
     * de la frecuencia.
     * 
     * @author Kevin Galindo
     */
    private function validateFrequency($config)
    {
        $execute = false;

        switch ($config->frequency) {
            case 1:
            break;
            
            case 2:
                $execute = $this->validateHourlyFrequency($config);
            break;
            
            case 3:
                $execute = $this->validateDailyFrequency($config);
            break;
                
            case 4:
                $execute = $this->validateWeekdayFrequency($config, 'lunes');    
            break;
            
            case 5:
                
            break;
            
            case 6:
                
            break;
            
            case 7:
                
            break;
            
            case 8:
                
            break;
            
            case 9:
                
            break;
            
            case 10:
                
            break;
            
            case 11:
                
            break;
            
            default:
            
            break;
        }


        return $execute;

        // $frequency = $frequencies->where('id', $config->frequency)->first();
        // $frequencies = collect([
        //     [
        //         'id' => 1,
        //         'value' => 'Cada minuto'
        //     ],
        //     [
        //         'id' => 2,
        //         'value' => 'Cada Hora'
        //     ],
        //     [
        //         'id' => 3,
        //         'value' => 'Diariamente'
        //     ],
        //     [
        //         'id' => 4,
        //         'value' => 'Semanal Lunes'
        //     ],
        //     [
        //         'id' => 5,
        //         'value' => 'Semanal Martes'
        //     ],
        //     [
        //         'id' => 6,
        //         'value' => 'Semanal Miercoles'
        //     ],
        //     [
        //         'id' => 7,
        //         'value' => 'Semanal Jueves'
        //     ],
        //     [
        //         'id' => 8,
        //         'value' => 'Semanal Viernes'
        //     ],
        //     [
        //         'id' => 9,
        //         'value' => 'Semanal Sabado'
        //     ],
        //     [
        //         'id' => 10,
        //         'value' => 'Semanal Domingo',
        //     ],
        //     [
        //         'id' => 11,
        //         'value' => 'Mensual',
        //     ]
        // ]);
    }

    /**
     * Este metodo se encarga de validar y ejecutar
     * la operacion
     * 
     * @author Kevin Galindo
     */
    private function validateOperation($config)
    {
        $operation = $config->option_->code_parameter;

        switch ($operation) {
            case 'P02':
                // $state = $this->documentsService->retainAuthorizedDocuments($config); 
            break;
            case 'A04':
                $state = $this->contactEmployeeRepository->alertEmployeesWithoutActivity($config); 
            break;
        }

        if ($state) {
            $config->last_execution_date = date('Y-m-d H:i:s');
            $config->save();
        }

        return $state;        
    }


    /**
     * Este metodo se encarga de validar
     * si se ejecutara una funcion "DIARIAMENTE"
     * 
     * @author Kevin Galindo
     */
    private function validateDailyFrequency($config)
    {
        $hour = str_replace(' ', '', $config->hour);
        $date = date('Y-m-d');
        $dateTime = Carbon::parse($date." ".$hour);
        $execute = false;

        if (Carbon::now() > $dateTime) {
            
            if (!$config->last_execution_date || Carbon::parse($config->last_execution_date)->diffInDays($dateTime) > 0) {
                $execute = true;
            }
        }

        return $execute;
    }

    /**
     * Este metodo se encarga de validar
     * si se ejecutara una funcion "CADA HORA"
     * 
     * @author Kevin Galindo
     */
    private function validateHourlyFrequency($config)
    {
        $execute = false;
        $last_execution_date = $config->last_execution_date ? Carbon::parse($config->last_execution_date) : null;
        $now = Carbon::now();

        $minutesDiff = $last_execution_date ? $last_execution_date->diffInMinutes($now) : 60;

        if ($minutesDiff >= 60) {
            $execute = true;
        }

        return $execute;
    }

    /**
     * Este metodo se encarga de validar
     * si se ejecutara una funcion "DIA DE LA SEMANA"
     * 
     * @author Kevin Galindo
     */
    private function validateWeekdayFrequency($config, $weekday)
    {
        $execute = false;
        $last_execution_date = $config->last_execution_date ? Carbon::parse($config->last_execution_date) : null;
        $now = Carbon::now();

        $minutesDiff = $last_execution_date ? $last_execution_date->diffInMinutes($now) : 60;

        if ($minutesDiff >= 60) {
            $execute = true;
        }

        return $execute;
    }
}
