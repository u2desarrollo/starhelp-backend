<?php

namespace App\Services;

use App\Entities\FixAsset;
use App\Entities\FixAssetMonthlyDepreciation;
use App\Entities\Parameter;
use App\Entities\Contact;
use App\Entities\Account;
use App\Entities\FixAssetAddition;
use App\Entities\TypesOperation;
use App\Entities\City;
use App\Entities\Subscriber;
use App\Entities\DocumentTransactions;
use App\Entities\Document;
// use App\Repositories\AccountRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;
use Validator;

class FixAssetService
{

    public function __construct()
    {
        
    }

    public function loadFixedAssetsFile($request)
    {
        try {
            DB::beginTransaction();

            $file = $request->file('archivo_importar');
            $fileData = csvToArray($file->getRealPath());
            $data = [];

            foreach ($fileData as $key => $item) {
                $consecut = FixAsset::max('consecut');

                $active_group = Parameter::where('name_parameter', 'ILIKE', '%'.$item['GRUPO_ACTIVOS'].'%')
                ->whereHas('paramtable', function ($query) {
                    $query->where('code_table', 'C-015');
                })->first();

                $provider = Contact::where('identification', $item['NIT_PROVEEDOR'])->first();

                $record_account  = Account::where('code', $item['CUENTA_REGISTRO'])->first();
                $balance_account = Account::where('code', $item['CUENTA_BALANCE'])->first();
                $result_account  = Account::where('code', $item['CUENTA_RESULTADO'])->first();
                $city = City::where('city', strtoupper($item['CIUDAD']))->first();

                $dataSave = [
                    'consecut'                        => $consecut === 0 || $consecut === null ? 1001 : ++$consecut ,
                    'plate_number'                    => $item['NUM_PLACA'],
                    'previous_code'                   => 0,
                    'active_group_id'                 => $active_group ? $active_group->id : null,
                    'description'                     => $item['DESCRIPCION'],
                    'long_description'                => $item['DESCRIP_LARGA'],
                    'purchase_date'                   => createDateFromFormat($item['FECHA_DE_COMPRA'], 'd/m/Y', 'Y-m-d'),
                    'date_regist'                     => createDateFromFormat($item['FECHA_DE_REGISTO'], 'd/m/Y', 'Y-m-d'),
                    'year_month_start_caus'           => createDateFromFormat($item['ANO_MES_INI_CAUSACION'], 'd/m/Y', 'Y-m'),
                    'year_month_end_caus'             => createDateFromFormat($item['ANO_MES_FIN_CAUSACION'], 'd/m/Y', 'Y-m'),
                    'year_month_last_caus'            => createDateFromFormat($item['ANO_MES_ULT_CAUSACION'], 'd/m/Y', 'Y-m'),
                    'months_to_caus'                  => $item['MESES__A_CAUSAR'],
                    'provider_id'                     => $provider ? $provider->id : null,
                    'invoice_prefix'                  => $item['PREFIJO-FACTURA'],
                    'invoice_number'                  => $item['NUMERO_FACTURA'] ? $item['NUMERO_FACTURA'] : 0,
                    // 'invoice_document_id'          => '',
                    'purchase_value'                  => $item['VALOR_BRUTO_DE_COMPRA']     ? str_replace(',', '.',$item['VALOR_BRUTO_DE_COMPRA'])     : 0,
                    'iva_value'                       => $item['VALOR_IVA_DE_COMPRA']       ? str_replace(',', '.',$item['VALOR_IVA_DE_COMPRA'])       : 0,
                    'addition_value'                  => $item['VALOR_ADICIONES']           ? str_replace(',', '.',$item['VALOR_ADICIONES'])           : 0,
                    'historical_cost_value'           => $item['VALOR_COSTO_HISTORICO']     ? str_replace(',', '.',$item['VALOR_COSTO_HISTORICO'])     : 0,
                    'salvage_value'                   => $item['VALOR_SALVAMENTO']          ? str_replace(',', '.',$item['VALOR_SALVAMENTO'])          : 0,
                    'previous_caus_value'             => $item['VALOR_DEPRECIADO_ANTERIOR'] ? str_replace(',', '.',$item['VALOR_DEPRECIADO_ANTERIOR']) : 0,
                    'value_for_causing'               => $item['VALOR-POR-DEPRECIAR']       ? str_replace(',', '.',$item['VALOR-POR-DEPRECIAR'])       : 0,
                    // 'monthly_deprec_value'         => '',
                    'caus_status'                     => $item['ESTADO_DEPRECIACION'] == 1 ? true : false,
                    'record_account_id'               => $record_account ? $record_account->id : null,
                    'balance_account_id'              => $balance_account ? $balance_account->id : null,
                    'result_account_id'               => $result_account ? $result_account->id : null,
                    // 'brand'                        => '',
                    // 'model'                        => '',
                    // 'serial_number'                => '',
                    // 'engine_number'                => '',
                    // 'deeds'                        => '',
                    // 'quantity'                     => '',
                    // 'class_id'                     => '',
                    'city_id'                         => $city ? $city->id : null,
                    // 'branchoffice_id'             => '',
                    // 'address'                      => '',
                    // 'location'                     => '',
                    // 'cost_center_id'               => '',
                    // 'contact_responsible_id'       => '',
                    // 'delivery_date'                => '',
                    // 'delivery_doc'                 => '',
                    // 'derecognized_date'            => '',
                    // 'contact_sale_id'              => '',
                    // 'value_derecognized'           => '',
                    // 'derecognized_type_id'         => '',
                    // 'derecognized_observation'     => '',
                    // 'derecognized_document_id'     => '',
                    // 'maintenance_provider_id'      => '',
                    // 'contract_description'         => '',
                    // 'contract_expiration_date'     => '',
                    // 'last_maintenance_date'        => '',
                    // 'spare_parts'                  => '',
                    // 'maintenance_notes'            => '',
                    // 'policy_number'                => '',
                    // 'policy_provider_id'           => '',
                    // 'policy_expiration_date'       => '',
                    // 'coverages'                    => '',
                    // 'premium_value'                => '',
                    // 'insured_value'                => '',
                    // 'policy_observation'           => '',
                    'date_last_commercial_appraisal'  => createDateFromFormat($item['FECHA_ULT_AVALUO_COMER'], 'd/m/Y', 'Y-m-d'),
                    'value_last_commercial_appraisal' => $item['VALOR_ULT_AVALUO_COMER'] ? $item['VALOR_ULT_AVALUO_COMER'] : 0
                ];

                $data[] = $dataSave;

                FixAsset::create($dataSave);
            }

            DB::commit();

            return [
                'code' => '200',
                'data' => $data
            ];


        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'code' => '500',
                'data' => [
                    [
                        'MESSAGE' => $e->getMessage(),
                        'LINE' => $e->getLine(),
                        'FILE' => $e->getFile(),
                    ]
                ]
            ];
        }
    } 


    public function downloadFixedAssets($request)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $fixAssets = FixAsset::select([
            'consecut',
            'plate_number',
            'previous_code',
            'active_group_id',
            'description',
            'long_description',
            'purchase_date',
            'date_regist',
            'year_month_start_caus',
            'year_month_end_caus',
            'year_month_last_caus',
            'months_to_caus',
            'provider_id',
            'invoice_prefix',
            'invoice_number',
            'invoice_document_id',
            'purchase_value',
            'iva_value',
            'addition_value',
            'historical_cost_value',
            'salvage_value',
            'previous_caus_value',
            'value_for_causing',
            'monthly_deprec_value',
            'caus_status',
            'record_account_id',
            'balance_account_id',
            'result_account_id',
        ])->get();
        
        $columns = [
            'Codigo',
            'Num Placa',
            'Cod Anterior',
            'Grupo Activos',
            'Descripcion',
            'Descrip Larga',
            'Fecha de Compra',
            'Fecha de Registo',
            'Año Mes Ini Causacion',
            'Año Mes Fin Causacion',
            'Año Mes Ult Causacion',
            'Meses a Causar',
            'Proveedor',
            'Prefijo Factura',
            'Numero Factura',
            'Documento Factura',
            'Valor de Compra',
            'Valor IVA',
            'Valor Adiciones',
            'Valor Costo Historico',
            'Valor Salvamento',
            'Valor Causado Anterior',
            'Valor por Causar',
            'Valor Depreciacion Mes',
            'Estado Depreciación',
            'Cuenta Registro',
            'Cuenta Balance',
            'Cuenta Resultado'
        ];

        $dataRows = [];

        foreach ($fixAssets as $key => $item) {
            $dataRows[] = [
                $item->consecut,
                $item->plate_number,
                $item->previous_code,
                $item->active_group ? $item->active_group->name_parameter : '',
                $item->description,
                $item->long_description,
                $item->purchase_date,
                $item->date_regist,
                $item->year_month_start_caus,
                $item->year_month_end_caus,
                $item->year_month_last_caus,
                $item->months_to_caus,
                $item->provider ? $item->provider->name." ".$item->provider->surname : '',
                $item->invoice_prefix,
                $item->invoice_number,
                $item->invoice_document ? $item->invoice_document->prefix." ".$item->invoice_document->consecutive : '',
                $item->purchase_value,
                $item->iva_value,
                $item->addition_value,
                $item->historical_cost_value,
                $item->salvage_value,
                $item->previous_caus_value,
                $item->value_for_causing,
                $item->monthly_deprec_value,
                $item->caus_status ? "Deprecia" : "No deprecia",
                $item->record_account ? $item->record_account->code." ".$item->record_account->name : '',
                $item->balance_account ? $item->balance_account->code." ".$item->balance_account->name : '',
                $item->result_account ? $item->result_account->code." ".$item->result_account->name : '',
            ];
        }

        $callback = function () use ($columns, $dataRows) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ([$columns] as $column) 
                fputcsv($file, $column, ';');
            

            foreach ($dataRows as $key => $row)
                fputcsv($file, $row, ';');
            

            //Cerramos la escritura del archivo.
            fclose($file);
        };

        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200
        ];
    }

    public function createDepreciationMonthly($request, $id)
    {
        try{
            $fixAsset       = FixAsset::find($id);
            $startDateCause = $fixAsset->year_month_start_caus;
            $nowDateCause   = Carbon::now()->format('Ym'); //'201812';
            $monthsCaus     = $fixAsset->months_to_caus;
            $purchaseValue  = $fixAsset->purchase_value;
            $ivaValue       = $fixAsset->iva_value;
            $salvageValue   = $fixAsset->salvage_value;
            $additionsValue = FixAssetAddition::where('fix_asset_id', $fixAsset->id)
            ->whereYear('date_addition', Carbon::createFromFormat('Ym', $nowDateCause)->format('Y'))
            ->whereMonth('date_addition', Carbon::createFromFormat('Ym', $nowDateCause)->format('m'))
            ->sum('value');

            // Eliminamos todos los registros antiguos
            DB::delete('delete from fix_asset_monthly_depreciation where fix_asset_id = ?', [$id]);

            // Primer registro
            $initialQuotaHistoricalCost = $purchaseValue + $ivaValue + $additionsValue;
            $initialQuotaBalanceToDepreciated  = $initialQuotaHistoricalCost;
            $initialQuotaAccumulatedDepreciationValue  = $fixAsset->previous_caus_value;


            // Calcular el mes acumulado
            $monthStartCause = Carbon::createFromFormat('Ym', $startDateCause)->format('Y');
            $monthNowCause   = Carbon::createFromFormat('Ym', $nowDateCause)->format('Y');
            $cumulativeMonth = ((($monthNowCause*12) + 12) - (($monthStartCause*12) + 3)) + 1;

            // Organizamos informacion
            $firstRegister = [
                'fix_asset_id' => $fixAsset->id,
                'year_month' => Carbon::createFromFormat('Ym', $nowDateCause)->format('Ym'),
                'value_additions' => $additionsValue,
                'historical_cost' => $initialQuotaHistoricalCost,
                'balance_to_depreciated' => $initialQuotaBalanceToDepreciated,
                'depreciate_value_month' => $initialQuotaAccumulatedDepreciationValue, 
                'accumulated_depreciation_value' => $initialQuotaAccumulatedDepreciationValue, 
                'months_accumulated' => $cumulativeMonth,
                'initial_recording' => true,
            ];

            // Creamos el primer registro.
            $firstRegisterCreate = FixAssetMonthlyDepreciation::create($firstRegister);

            // Creamos las cuotas
            $startCalculateQuota = true;
            $lastCumulativeMonth = $cumulativeMonth;
            $lastDateCause = $nowDateCause;

            do {

                $previousDepreciation = FixAssetMonthlyDepreciation::where([
                    'fix_asset_id' => $id,
                    'months_accumulated' => $lastCumulativeMonth,
                ])->first();

                // datos
                $lastDateCause = Carbon::createFromFormat('Ym', $lastDateCause)->addMonths(1)->format('Ym');
                $additionsValue = FixAssetAddition::where('fix_asset_id', $fixAsset->id)
                ->whereYear('date_addition', Carbon::createFromFormat('Ym', $lastDateCause)->format('Y'))
                ->whereMonth('date_addition', Carbon::createFromFormat('Ym', $lastDateCause)->format('m'))
                ->sum('value');

                $historicalCost = $previousDepreciation->historical_cost + $additionsValue;
                $balanceToDepreciated = $historicalCost - $previousDepreciation->accumulated_depreciation_value;
                $depreciateValueMonth = ($historicalCost - $salvageValue) / $monthsCaus;
                $accumulatedDepreciationValue = $depreciateValueMonth + $previousDepreciation->accumulated_depreciation_value;
                $lastCumulativeMonth += 1;

                $quotaRegister = [
                    'fix_asset_id' => $fixAsset->id,
                    'year_month' => $lastDateCause,
                    'value_additions' => $additionsValue,
                    'historical_cost' => $historicalCost,
                    'balance_to_depreciated' => $balanceToDepreciated,
                    'depreciate_value_month' => $depreciateValueMonth, 
                    'accumulated_depreciation_value' => $accumulatedDepreciationValue, 
                    'months_accumulated' => $lastCumulativeMonth,
                    'initial_recording' => false,
                ];

                // Realizamos la validacion para continuar el ciclo
                if ($salvageValue) {
                    $depreciateValue = $balanceToDepreciated - $depreciateValueMonth;
                    if ($depreciateValue <= $salvageValue) {
                        $startCalculateQuota = false;
                        FixAssetMonthlyDepreciation::create($quotaRegister);
                    } else {
                        FixAssetMonthlyDepreciation::create($quotaRegister);
                    }
                } else {
                    if ($balanceToDepreciated === 0) {
                        $startCalculateQuota = false;
                    } else {
                        FixAssetMonthlyDepreciation::create($quotaRegister);
                    }
                }
            } while ($startCalculateQuota);

            DB::commit();

            return [
                'code' => 200,
                'data' => FixAssetMonthlyDepreciation::where('fix_asset_id', $id)->get()
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getFile() . $e->getLine() . $e->getMessage());
            return [
                'code' => 200,
                'data' => 'Error al crear los registros'
            ];
        }
    }

    public function getDepreciationMonthly($request, $id)
    {
        return [
            'code' => 200,
            'data' => FixAssetMonthlyDepreciation::where('fix_asset_id', $id)->orderBy('months_accumulated')->get()
        ];
    }

    public function createMasiveDepreciationMonthly($request)
    {
        $fixAssets = FixAsset::all();
        $data = [];

        foreach ($fixAssets as $key => $fixAsset) {
            $response = $this->createDepreciationMonthly($request, $fixAsset->id);
            $data[] = [
                'id' =>$fixAsset->id,
                'response' => $response
            ];
        }

        return $data;
    }

    public function voucherAssetsFixedDepreciation($request, $yearMonth)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find(3);
            $dateValid = Carbon::createFromFormat('Ymd', $yearMonth.'01')->format('Y-m-d');

            if (!$subscriber->vouchertype_id) {
                return [
                    'data' => [
                        'message' => 'El subcriptor no tiene un tipo de comprobante seleccionado.'
                    ],
                    'code' => 400
                ];
            }

            if (Document::where('document_date', $dateValid)->exists()) {
                return [
                    'data' => [
                        'message' => 'Ya existe un comprobante para ese periordo. Favor regenerar comprobante'
                    ],
                    'code' => 400
                ];
            }

            $depreciations = FixAssetMonthlyDepreciation::where('year_month', $yearMonth)->get();

            if (count($depreciations) === 0) {
                return [
                    'data' => [
                        'message' => 'No hay registros para crear el comprobante.'
                    ],
                    'code' => 400
                ];
            }

            $vouchertype_id = $subscriber->vouchertype_id;
            $contact_id = $subscriber->id;
            $typesOperation = TypesOperation::where('code', $subscriber->vouchertype->code_voucher_type)->first();
            $contact = Contact::where('identification', $subscriber->identification)->first();

            if (!$contact) {
                return [
                    'data' => [
                        'message' => 'No se encontro registro en contacts del suscriptor.'
                    ],
                    'code' => 404
                ];
            }

            $consecutive = $this->getVoucherTypeConsecutive($subscriber->vouchertype);

            $document = Document::create([
                'vouchertype_id' => $vouchertype_id,
                'consecutive' => $subscriber->vouchertype->consecutive_number+1,
                'document_date' => $dateValid,
                'contact_id' => $contact->id,
                'document_state' => 1,
                'document_last_state' => $dateValid,
                'issue_date' => $dateValid,
                'in_progress' => true
            ]);

            $totalValue = 0;
            foreach ($depreciations as $key => $depreciation) {

                if ($depreciation->fixAsset->accountAccumulatedDepreciation) {
                    $dataSaveAccountAccumulatedDepreciation = [
                        'year_month' => $yearMonth,
                        'account_id' => $depreciation->fixAsset->accountAccumulatedDepreciation->id,
                        'contact_id' => $contact->id,
                        'transactions_type' => 'C',
                        'affects_document_id' => $document->id,
                        'origin_document' => true,
                        'document_id' => $document->id,
                        'operation_value' => $depreciation->depreciate_value_month
                    ];
                }

                if ($depreciation->fixAsset->accountDepreciationExpense) {
                    $dataSaveAccountDepreciationExpense = [
                        'year_month' => $yearMonth,
                        'account_id' => $depreciation->fixAsset->accountDepreciationExpense->id,
                        'contact_id' => $contact->id,
                        'transactions_type' => 'D',
                        'affects_document_id' => $document->id,
                        'origin_document' => true,
                        'document_id' => $document->id,
                        'operation_value' => $depreciation->depreciate_value_month
                    ];
                }

                $totalValue += $depreciation->depreciate_value_month; 

                DocumentTransactions::create($dataSaveAccountAccumulatedDepreciation);
                DocumentTransactions::create($dataSaveAccountDepreciationExpense);

            }

            $document->total_value = $totalValue;
            $document->observation = 'comprobante-activos-fijos-depreciacion';
            $document->save();

            DB::commit();

            return [
                'code' => '201',
                'data' => Document::with([
                    'accounting_transactions'
                ])->find($document->id)
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'code' => '500',
                'data' => [
                    [
                        'MESSAGE' => $e->getMessage(),
                        'LINE' => $e->getLine(),
                        'FILE' => $e->getFile(),
                    ]
                ]
            ];
        }


    }

    public function getVoucherTypeConsecutive($vouchersType)
    {
        
        $consecutive = 0;

        //Validamos si existe comprobante
        if ($vouchersType) {
            // Asignamos valores
            $consecutive = $vouchersType->consecutive_number + 1;
            $exists = true;

            // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
            while ($exists) {
                $aux = Document::where('consecutive', $consecutive)
                    ->where('vouchertype_id', $vouchersType->id)
                    ->where('in_progress', false)
                    ->exists();
                if ($aux) {
                    ++$consecutive;
                } else {
                    $exists = false;
                }
            }

            // Actualizamos el consecutivo del tipo de comprobante.
            $vouchersType->consecutive_number = $consecutive;
            $vouchersType->save();
        }

        return $consecutive;
    }

    public function getVoucherAssetsFixedDepreciation($request) {
        return [
            'code' => '200',
            'data' => Document::with([
                'accounting_transactions'
            ])
            ->where('observation', 'comprobante-activos-fijos-depreciacion')
            ->get()
        ];
    }

    public function regenerateVoucherAssetsFixedDepreciation($request, $document_id)
    {
        try {
            DB::beginTransaction();

            $document = Document::find($document_id);
            $yearMonth = Carbon::parse($document->document_date)->format('Ym');
            $depreciations = FixAssetMonthlyDepreciation::where('year_month', $yearMonth)->get();
            $contact = $document->contact;

            // Eliminamos todos los documents transaccion
            DB::delete('delete from document_transactions where document_id = ?', [$document->id]);


            $totalValue = 0;
            foreach ($depreciations as $key => $depreciation) {

                if ($depreciation->fixAsset->accountAccumulatedDepreciation) {
                    $dataSaveAccountAccumulatedDepreciation = [
                        'year_month' => $yearMonth,
                        'account_id' => $depreciation->fixAsset->accountAccumulatedDepreciation->id,
                        'contact_id' => $contact->id,
                        'transactions_type' => 'C',
                        'affects_document_id' => $document->id,
                        'origin_document' => true,
                        'document_id' => $document->id,
                        'operation_value' => $depreciation->depreciate_value_month,
                        'fix_asset_id' => $depreciation->fix_asset_id
                    ];
                }

                if ($depreciation->fixAsset->accountDepreciationExpense) {
                    $dataSaveAccountDepreciationExpense = [
                        'year_month' => $yearMonth,
                        'account_id' => $depreciation->fixAsset->accountDepreciationExpense->id,
                        'contact_id' => $contact->id,
                        'transactions_type' => 'D',
                        'affects_document_id' => $document->id,
                        'origin_document' => true,
                        'document_id' => $document->id,
                        'operation_value' => $depreciation->depreciate_value_month,
                        'fix_asset_id' => $depreciation->fix_asset_id
                    ];
                }

                $totalValue += $depreciation->depreciate_value_month; 

                DocumentTransactions::create($dataSaveAccountAccumulatedDepreciation);
                DocumentTransactions::create($dataSaveAccountDepreciationExpense);

            }

            $document->total_value = $totalValue;
            $document->observation = 'comprobante-activos-fijos-depreciacion';
            $document->save();

            DB::commit();

            return [
                'code' => '200',
                'data' => Document::with([
                    'accounting_transactions'
                ])->find($document->id)
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'code' => '500',
                'data' => [
                    [
                        'MESSAGE' => $e->getMessage(),
                        'LINE' => $e->getLine(),
                        'FILE' => $e->getFile(),
                    ]
                ]
            ];
        }


    }

    public function regenerateExcelVoucherAssetsFixed($request, $document_id)
    {
        $document = Document::find($document_id);
        $transactions = $document->accounting_transactions->where('transactions_type', 'C');

        $headerCsv = [
            'Codigo',
            'Descripcion',
            'F-inicio',
            'Meses',
            'Costo-Hist',
            'Deprec-Acum',
            'Deprec-Mes',
            'Saldo-Por-Depr'
        ];

        $dataCsv = [];

        foreach ($transactions as $key => $transaction) {

            $yearMonth = Carbon::parse($transaction->document->document_date)->format('Ym');
            $depreciation = FixAssetMonthlyDepreciation::where('year_month', $yearMonth)
            ->where('fix_asset_id', $transaction->fixAsset->id)
            ->first();

            $dataCsv[] = [
                'Codigo'         =>  $transaction->fixAsset->consecut,
                'Descripcion'    =>  $transaction->fixAsset->description,
                'F-inicio'       =>  $transaction->fixAsset->year_month_start_caus,
                'Meses'          =>  $transaction->fixAsset->months_to_caus,
                'Costo-Hist'     =>  $depreciation->historical_cost,
                'Deprec-Acum'    =>  $depreciation->accumulated_depreciation_value,
                'Deprec-Mes'     =>  $depreciation->depreciate_value_month,
                'Saldo-Por-Depr' =>  $depreciation->balance_to_depreciated,
            ];
        }


        $yearMonth = Carbon::parse($document->document_date)->format('Ym');

        // dd(collect($dataCsv)->sortBy('Descripcion')->values()->toArray());

        return [
            'data' => [
                'data' => $dataCsv,
                'header' => $headerCsv,
                'nameFile' => 'comprobante-activos-fijos-'.$yearMonth
            ],
            'code' => 200
        ];
    }


}
