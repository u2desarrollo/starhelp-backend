<?php

namespace App\Services;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\ContactWarehouse;
use App\Entities\Inventory;
use App\Entities\Document;

use App\Entities\Parameter;
use App\Entities\ParamTable;
use App\Entities\BranchOffice;
use App\Entities\Price;
use App\Entities\Product;
use App\Repositories\InventoryRepository;

use App\Services\DocumentsService;
use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;
use App\Repositories\BranchOfficeWarehouseRepository;
use App\Repositories\DocumentProductRepository;
use App\Repositories\DocumentRepository;
use App\Repositories\InventoryxRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Fill;


class InventoryService
{

    private $inventoryRepository;
    private $branchOfficeWarehouseRepository;
    private $documentProductRepository;
    private $documentRepository;
    private $inventoryxRepository;

    public function __construct(
        BranchOfficeWarehouseRepository $branchOfficeWarehouseRepository,
        DocumentProductRepository $documentProductRepository,
        DocumentRepository $documentRepository,
        InventoryxRepository $inventoryxRepository)
    {
        $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepository;
        $this->documentProductRepository = $documentProductRepository;
        $this->documentRepository = $documentRepository;
        $this->inventoryxRepository = $inventoryxRepository;
        $this->inventoryRepository = new InventoryRepository(new Container());
    }


    public function calculateAvailability($request)
    {
        $pdf = NULL;
        $documentRepository = NULL;
        $documentProductRepository = NULL;
        $inventoryRepository = NULL;
        $documentsProductsService = NULL;
        $documentService = new DocumentsService(
            $pdf,
            $documentRepository,
            $documentProductRepository,
            $inventoryRepository,
            $documentsProductsService
        );

        $documents = $documentService->getOrderIfRejected(false, $request);

        try {
            // Recorremos los docuemntos
            foreach ($documents as $key => $document) {
                // Recorremos los productos del documento
                foreach ($document->documentsProducts as $key_dp => $documentProduct) {
                    $availability = null;
                    $inventory = Inventory::where('product_id', $documentProduct->product_id)
                        ->where('branchoffice_warehouse_id', $document->branchoffice_warehouse_id)
                        ->first();

                    if ($documentProduct->quantity > 0) {
                        $availability = ($inventory->stock - $documentProduct->quantity);
                        $inventory->available = $availability;
                        $inventory->save();
                    }
                }

            }
            return true;
        } catch (Exception $e) {
            echo $e->getMessage() . ' in the line ' . $e->getLine();
            return false;
        }

    }

    /**
     * @author Jhon García
     */
    public function generateReport($request)
    {

        $report = $this->inventoryRepository->getDataReport($request);

        $data = [];

        $headers = ['SEDE', 'BODEGA', 'CÓDIGO', 'DESCRIPCIÓN', 'LINEA', 'MARCA', 'SALDO EN UNIDADES', 'SALDO EN VALORES', 'COSTO PROMEDIO'];

        array_push($data, $headers);

        $total_unit_balance = 0;
        $total_value_average = 0;
        foreach ($report as $line) {
            $total_unit_balance += $line->unit_balance;
            $total_value_average += $line->value_average;

            $line = [
                $line->branchoffice_name,
                $line->branchoffice_warehouse_name,
                strval($line->product_code),
                $line->product_description,
                $line->line_description,
                $line->brand_description,
                $line->unit_balance,
                '$' . number_format($line->value_balance, 2, '.', ','),
                '$' . number_format($line->value_average, 2, '.', ','),
            ];

            array_push($data, $line);

        }

        array_push($data, []);
        array_push($data, [
            null,
            null,
            null,
            null,
            null,
            'Totales...',
            $total_unit_balance,
            '$' . number_format($total_value_average, 2, '.', ','),
            null,
        ]);


        $path = storage_path("app/public/reports/");

        $fileName = 'ValueInventory' . date('YmdHis') . '.csv';

        $fullName = $path . $fileName;

        $this->generateCSV($data, $fullName);

        return $fileName;

    }

    public function generateCSV($data, $fileName)
    {
        $file_handle = fopen($fileName, 'w');
        foreach ($data as $line) {
            fputs($file_handle, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fwrite($file_handle, implode(';', $line)."\n");
            // fputcsv($file_handle,  str_replace('"', '',$line), ";");
        }
        rewind($file_handle);
        fclose($file_handle);
    }


    /**
     * proceso para reconstruir el inventario de uno o mas productos
     *
     * @author santiago torres
     */
    public function fixGlobalValues($request)
    {
        set_time_limit(0);

        echo "Iniciando reconstrucción de inventario \n";
        //limpiar la tabla inventoriesx

        // validamos si se en el request se envio un product
        // si no se envio se hace valoriza  la tabla inventory todos los productos en cero
        if (!$request->product) {
            // DB::table('inventories')->truncate();
            $set_values_inventory_0 = $this->inventoryRepository->updateAllValuesTo0();
        }

        echo "Truncando tabla inventoriesx \n";
        DB::table('inventoriesx')->truncate();

        //lista de bodegas
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->allBranchOfficeWarehouse();

        //productos que se reconstruira el inventario
        echo "Obteniendo productos a reconstruir \n";
        if (!$request->product) {
            $list_products = $this->documentProductRepository->getProductFromDocuments();
            $products = [];
            foreach ($list_products as $key => $product) {
                $products[] = $product->product_id;
            }
        } else {
            $products = explode('|', $request->product);
            // Si se envia producto desde el request solo se reconstruye ese producto
            // $products=[$request->product];
        }

        // se crea variable de con los documentos usados para los valores iniciales de inventoryx
        $valoresiniciales = [];
        $idDocumentsValues = [];


        // Si se envia fecha desde el request
        //se toma el documento anterior a la fecha para crear valores iniciales en inventoryx
        if ($request->date) {

            // recorremos el arrat de productos a recontruir
            foreach ($products as $key => $value) {

                foreach ($branchOfficeWarehouses as $key => $branchOfficeWarehouse) {
                    //saldos en unidades y valores anteriores al documenro
                    $before_balances = $this->documentProductRepository->getBeforeBalancesDocument($value, $request->date, $branchOfficeWarehouse->branchoffice_warehouse_id);
                    // dd($before_balances->document_id);

                    if ($before_balances) {
                        $idDocumentsValues [] = [$before_balances->document_id];
                        $data = [
                            "stock"                     => $before_balances->stock_after_operation,
                            "stock_values"              => $before_balances->stock_value_after_operation,
                            "average_cost"              => $before_balances->stock_after_operation > 0 ? $before_balances->stock_value_after_operation / $before_balances->stock_after_operation : 0,
                            'product_id'                => $before_balances->product_id,
                            'branchoffice_warehouse_id' => $before_balances->branchoffice_warehouse_id,
                        ];
                        $valoresiniciales[] = $this->inventoryxRepository->createX($data);

                    }

                }

            }
        } else {
            if ($request->product) {
                foreach ($products as $key => $value) {
                    $this->inventoryRepository->updateValuesTo0($value);
                }
            }
        }

        echo "Reconstruyendo inventario \n";
        $counter = 0;

        $inventoryxF = [];
        //reconstruir producto por producto
        foreach ($products as $key => $product) {
            $documentsProductsReconstructor = $this->documentRepository->searchDocumentsProductsReconstructor($request, $product, $idDocumentsValues);


            foreach ($documentsProductsReconstructor as $key => $documentProduct) {
                $inventoryxF[] = $this->inventoryxRepository->finalize($documentProduct->id);
            }

            $counter++;

            if ($counter % 50 == 0){
                echo $counter . " de " . count($products) . " inventarios de producto reconstruidos \n";
            }
        }


        //lista de documentos para reconstruir
        //$documentsProductsReconstructor = $this->documentRepository->searchDocumentsProductsReconstructor($request, $products, $idDocumentsValues);

        //almacena la info que se va a reconstruir en inventoryx
        // $inventoryxF = [];

        // recorremos los documentos y calculamos los saldos en unidades y valores
        // foreach ($documentsProductsReconstructor as $key => $document) {
        // foreach ($documentsProductsReconstructor as $key => $documentProduct) {
        //     $inventoryxF[] = $this->inventoryxRepository->finalize($documentProduct->id);
        // }
        // }


        echo "Obteniendo registros de inventoriesx \n";

        // obtenemos la info de inventoryx para pasarla a inventory
        $inventoryx = $this->inventoryxRepository->getinventoryforupdate();

        echo "Copiando registros de inventoriesx a inventories \n";

        $counter = 0;

        $inventory = [];
        //recorremos la info de inventoryx para almacenarla en inventory
        foreach ($inventoryx as $key => $value) {
            $inventory[] = $this->inventoryRepository->updateInventoryFromInventoryx($value);

            $counter++;

            if ($counter % 100 == 0){
                echo $counter . " de " . count($products) . " registros copiados \n";
            }
        }

        return [
            'products'                       => $products,
            'branchOfficeWarehouses'         => $branchOfficeWarehouses,
            'valoresiniciales'               => $valoresiniciales,
            'idDocumentsValues'              => $idDocumentsValues,
            'documentsProductsReconstructor' => $documentsProductsReconstructor,
            'inventoryxF'                    => $inventoryxF,
            'inventoryx'                     => count($inventoryx),
            'inventory'                      => count($inventory),
        ];
    }

    public function rebuildDocument($document_id)
    {
        $request = new Request();


        DB::table('inventoriesx')->truncate();

        //raemos el documento
        $document = Document::find($document_id);
        $user_rebuild = $document->user_id_last_modification;
        $rebuild_date = new \DateTime();
        $request->merge(['date' => $document->document_date]);

        // dd($request->toArray());
        if (!$document) {
            return (object)[
                'code' => '500',
                'data' => (object)[
                    'data'    => '',
                    'message' => 'No existe el documento'
                ]
            ];
        }

        //traemos los peoductos del documento
        $products = $this->documentProductRepository->getProductsFromDocument($document_id);
        $valoresiniciales = [];

        //lista de bodegas
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->allBranchOfficeWarehouse();
        $data = [];
        $idDocumentsValues = [];
        foreach ($products as $key => $value) {
            $this->inventoryRepository->updateValuesTo0($value->product_id);

            foreach ($branchOfficeWarehouses as $key => $branchOfficeWarehouse) {
                //saldos en unidades y valores anteriores al documenro
                $before_balances = $this->documentProductRepository->getBeforeBalancesDocument($value->product_id, $document->document_date, $branchOfficeWarehouse->branchoffice_warehouse_id);
                // dd($before_balances->document_id);

                if ($before_balances) {
                    $idDocumentsValues [] = [$before_balances->document_id];
                    $data = [
                        "stock"                     => $before_balances->stock_after_operation,
                        "stock_values"              => $before_balances->stock_value_after_operation,
                        "average_cost"              => $before_balances->stock_after_operation > 0 ? $before_balances->stock_value_after_operation / $before_balances->stock_after_operation : 0,
                        'product_id'                => $before_balances->product_id,
                        'branchoffice_warehouse_id' => $before_balances->branchoffice_warehouse_id,
                    ];
                    $valoresiniciales[] = $this->inventoryxRepository->createX($data);

                }

            }

        }


        $negatives_values = false;
        $inventory_result = null;
        $inventoryxF = [];
        //reconstruir producto por producto
        foreach ($products as $key => $product) {
            $documentsProductsReconstructor = $this->documentRepository->searchDocumentsProductsReconstructor($request, $product->product_id, $idDocumentsValues);


            foreach ($documentsProductsReconstructor as $key => $documentProduct) {
                $this->documentProductRepository->setInfoRebuild($documentProduct->id, $user_rebuild, $rebuild_date, $documentProduct->stock_after_operation);
                $inventory_result = $this->inventoryxRepository->finalize($documentProduct->id);
                if ($inventory_result["negatives"]) {
                    $negatives_values = true;
                }
                $inventoryxF[] = $inventory_result;
                $erp_null = $this->documentRepository->setErp_idNull($documentProduct->document->id);
            }
        }

        // obtenemos la info de inventoryx para pasarla a inventory
        $inventoryx = $this->inventoryxRepository->getinventoryforupdate();

        $inventory = [];
        //recorremos la info de inventoryx para almacenarla en inventory
        foreach ($inventoryx as $key => $value) {
            $inventory[] = $this->inventoryRepository->updateInventoryFromInventoryx($value);
        }

        return (object)[
            'code' => '200',
            'data' => (object)[
                'data'      => $valoresiniciales,
                'message'   => 'ok',
                'negatives' => $negatives_values ? 'La modificación de este documento genero saldos negativos, por favor verificar kardex de los productos' : false
            ]
        ];

    }

    /**
     * Generar informe de reposición
     *
     * @author Jhon García
     */
    public function generateReportRepositioningAssistant()
    {
        //Obtenemos la los datos a ingresar en el reporte
        $report = $this->inventoryRepository->getDataForRepositioningAssistant();

        if (count($report) > 0) {

            $spreadSheet = new Spreadsheet();
            $sheet = $spreadSheet->getActiveSheet();
            $sheet->setTitle('Informe de reposición');

            $letter = 'A';
            $row = 2;
            $months_security = 2;

            foreach ($report as $line) {
                $letter = 'A';
                $sheet->setCellValue($letter++ . $row, $line->name_branchoffice);
                $sheet->setCellValue($letter++ . $row, $line->line_description);
                $sheet->setCellValue($letter++ . $row, $line->brand_description);
                $sheet->setCellValue($letter++ . $row, $line->product_code);
                $sheet->setCellValue($letter++ . $row, $line->product_description);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_first_month < 0 ? 0 : $line->total_quantity_first_month);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_second_month < 0 ? 0 : $line->total_quantity_second_month);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_third_month < 0 ? 0 : $line->total_quantity_third_month);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_fourth_month < 0 ? 0 : $line->total_quantity_fourth_month);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_fifth_month < 0 ? 0 : $line->total_quantity_fifth_month);
                $sheet->setCellValue($letter++ . $row, $line->total_quantity_sixth_month < 0 ? 0 : $line->total_quantity_sixth_month);
                $sheet->setCellValue($letter++ . $row, '=AVERAGE(F' . $row . ':K' . $row . ')');
                $sheet->setCellValue($letter++ . $row, '=IFERROR(AVERAGE(I' . $row . ':K' . $row . ')/AVERAGE(F' . $row . ':K' . $row . '),0)');
                $sheet->setCellValue($letter++ . $row, $line->current_existence);
                $sheet->setCellValue($letter++ . $row, $line->total_stock);
                $sheet->setCellValue($letter++ . $row, $months_security);
                $sheet->setCellValue($letter++ . $row, '=L' . $row . '*P' . $row);
                $sheet->setCellValue($letter++ . $row, '=IFERROR(N' . $row . '/L' . $row . ',0)');
                $sheet->setCellValue($letter++ . $row, '=IF(O' . $row . '-Q' . $row . '<(2*L' . $row . '*M' . $row . '),"COMPRAR","NO")');
                $sheet->setCellValue($letter++ . $row, '=IF(S' . $row . '="COMPRAR",L' . $row . '*M' . $row . ',0)');
                $sheet->setCellValue($letter++ . $row, $line->total_back_order);
                $sheet->setCellValue($letter++ . $row, $line->document_date);
                $sheet->setCellValue($letter++ . $row, '$ ' . number_format($line->unit_value_before_taxes, 0, ",", "."));
                $row++;
            }

            $row--;

            //Establecemos el formato condicional para la columna 'FACTOR'
            $sheet->getStyle('M2:M' . $row)->setConditionalStyles($this->getConditionalFormat($sheet, $row));

            //Se establece el filtro para todas las columnas
            $sheet->setAutoFilter('A1:W' . $row);

            $this->setStyles($sheet, $row);

            $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

            $writer->setPreCalculateFormulas(false);

            $fileName = storage_path("app/public/reports/") . 'Sugerido de reposicion.xlsx';

            $writer->save($fileName);

            return 'Sugerido de reposicion.xlsx';
        }
    }

    /**
     * Retorna el formato condicional para unas reglas especificas
     *
     * @param $sheet
     * @return mixed
     * @author Jhon García
     */
    public function getConditionalFormat($sheet, $row)
    {
        $conditional1 = new Conditional();
        $conditional1->setConditionType(Conditional::CONDITION_CELLIS);
        $conditional1->setOperatorType(Conditional::OPERATOR_LESSTHAN);
        $conditional1->addCondition('0.8');
        $conditional1->getStyle()->getFont()->setBold(true);
        $conditional1->getStyle()->getFont()->getColor()->setARGB('9C0005');
        $conditional1->getStyle()->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFC7C7');
        $conditional1->getStyle()->getFill()->getEndColor()->setARGB('FFC7C7');

        $conditional2 = new Conditional();
        $conditional2->setConditionType(Conditional::CONDITION_CELLIS);
        $conditional2->setOperatorType(Conditional::OPERATOR_BETWEEN);
        $conditional2->addCondition('0.8');
        $conditional2->addCondition('1.1');
        $conditional2->getStyle()->getFont()->setBold(true);
        $conditional2->getStyle()->getFont()->getColor()->setARGB('9C5700');
        $conditional2->getStyle()->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB9C');
        $conditional2->getStyle()->getFill()->getEndColor()->setARGB('FFEB9C');

        $conditional3 = new Conditional();
        $conditional3->setConditionType(Conditional::CONDITION_CELLIS);
        $conditional3->setOperatorType(Conditional::OPERATOR_GREATERTHAN);
        $conditional3->addCondition('1.1');
        $conditional3->getStyle()->getFont()->setBold(true);
        $conditional3->getStyle()->getFont()->getColor()->setARGB('006100');
        $conditional3->getStyle()->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('C6EFCE');
        $conditional3->getStyle()->getFill()->getEndColor()->setARGB('C6EFCE');

        $conditionalStyles = $sheet->getStyle('M2:M' . $row)->getConditionalStyles();
        $conditionalStyles[] = $conditional1;
        $conditionalStyles[] = $conditional2;
        $conditionalStyles[] = $conditional3;

        return $conditionalStyles;
    }

    public function setStyles($sheet, $row)
    {
        $months = ['', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];

        $headers = [
            'SEDE',
            'LÍNEA',
            'MARCA',
            'REFERENCIA',
            'PRODUCTO',
            'VENTAS ' . $months[(int)strtoupper(now()->addMonths(-5)->format('m'))],
            'VENTAS ' . $months[(int)strtoupper(now()->addMonths(-4)->format('m'))],
            'VENTAS ' . $months[(int)strtoupper(now()->addMonths(-3)->format('m'))],
            'VENTAS ' . $months[(int)strtoupper(now()->addMonths(-2)->format('m'))],
            'VENTAS ' . $months[(int)strtoupper(now()->addMonths(-1)->format('m'))],
            'VENTAS ' . $months[(int)strtoupper(now()->format('m'))],
            'PROMEDIO DE VENTAS',
            'FACTOR',
            'EXISTENCIA BODEGA',
            'EXISTENCIA TOTAL',
            'MESES SEGURIDAD',
            'CANT SEGURIDAD',
            'INVENTARIO MESES',
            '¿PEDIDO?',
            'CANT A COMPRAR',
            'CANT BACKORDER',
            'FECHA ULTIMA COMPRA',
            'COSTO ULTIMA COMPRA',
        ];

        $styles = [
            'A' => [
                'width' => 20
            ],
            'B' => [
                'width' => 22
            ],
            'C' => [
                'width' => 12
            ],
            'D' => [
                'width' => 25
            ],
            'E' => [
                'width'    => 40,
                'wrapText' => true,
                'autoSize' => false
            ],
            'F' => [
                'width' => 12
            ],
            'G' => [
                'width' => 12
            ],
            'H' => [
                'width' => 12
            ],
            'I' => [
                'width' => 12
            ],
            'J' => [
                'width' => 12
            ],
            'K' => [
                'width' => 12
            ],
            'L' => [
                'width' => 12
            ],
            'M' => [
                'width' => 12
            ],
            'N' => [
                'width' => 12
            ],
            'O' => [
                'width' => 12
            ],
            'P' => [
                'width' => 12
            ],
            'Q' => [
                'width' => 12
            ],
            'R' => [
                'width' => 12
            ],
            'S' => [
                'width' => 12
            ],
            'T' => [
                'width' => 12
            ],
            'U' => [
                'width' => 12
            ],
            'V' => [
                'width' => 12
            ],
            'W' => [
                'width' => 12
            ]
        ];

        $generalStyles = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'borders'   => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN
                ]
            ]
        ];

        $counter = 0;

        foreach ($styles as $letter => $style) {
            $sheet->setCellValue($letter . '1', $headers[$counter++]);
            $sheet->getStyle($letter . '1')->getAlignment()->setWrapText(true);
            $sheet->getStyle($letter . '1')->getFont()->setBold(true);
            $sheet->getStyle($letter . '1')->getFont()->getColor()->setARGB('FFFFFF');
            $sheet->getStyle($letter . '1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('6386D3');
            $sheet->getStyle($letter . '1')->getFill()->getEndColor()->setARGB('6386D3');
            $sheet->getColumnDimension($letter)->setWidth($style['width']);
            $sheet->getColumnDimension($letter)->setAutoSize(isset($style['autoSize']) ? $style['autoSize'] : false);
            $sheet->getStyle($letter . '2:' . $letter . $row)->getAlignment()->setWrapText(isset($style['wrapText']) ? $style['wrapText'] : false);
        }

        $sheet->getStyle('A1:W' . $row)->applyFromArray($generalStyles);
    }

    /**
     * Consultar la disponibilidad de productos de acuerdo a la sede del cliente
     *
     * @param Request $request Contiene la información de los productos a consultar y del cliente
     * @return array Contiene la disponibilidad y el precio de los productos
     * @author Jhon García
     */
    public function getAvailabilityAndPricesForProducts($request)
    {
        $date_if_new = now()->addMonths(-1)->format('Y-m-d');

        //tabla de parametro lista de precio
        $price_list = ParamTable::select('id', 'code_table')->where('code_table', 'LDP')->first();

        // parametro precio minimo de venta
        $minumum_price_list = Parameter :: select('id', 'paramtable_id', 'code_parameter')
        ->where('paramtable_id', $price_list->id)
        ->where('code_parameter', '002')
        ->first();

        // Factor por el que se debe multiplicar el precio
        $factor = 1;

        // Obtener la sucursal del cliente
        $contact_warehouse = ContactWarehouse::findOrFail($request->clibod_cod_bod, ['id', 'branchoffice_id', 'discount_percentage']);

        // Obtener el porcentaje de descuento
        $discount_percentage = (double)$contact_warehouse->discount_percentage;

        // Validar si el porcentaje de descuento es mayor o menor que 0
        if ($discount_percentage > 0){
            $factor -= ($discount_percentage / 100);
        }elseif ($discount_percentage < 0){
            $factor += ($discount_percentage / 100);
        }

        // Obtener la bodega principal de la sede a la que pertenece el cliente
        $branch_office_warehouse = BranchOfficeWarehouse::select('id')
            ->where([
                'warehouse_code'  => '01',
                'branchoffice_id' => $contact_warehouse->branchoffice_id
            ])
            ->first();

        $branchoffices = BranchOffice::select('id','name')
        ->get();

        // Obtener los precios de los productos
        $prices = Price::select('price', 'branchoffice_id', 'price_list_id','product_id', 'created_at')
            ->whereIn('product_id', $request->productos)
            ->where(function ($branch_office) use ($contact_warehouse) {
                $branch_office->where('branchoffice_id', $contact_warehouse->branchoffice_id)
                    ->orWhereNull('branchoffice_id');
            })
            ->orderBy('branchoffice_id', 'DESC')
            ->orderBy('price_list_id', 'ASC')
            ->get();

        // Obtener la disponibilidad de los productos en la bodega asociada al cliente
        $products = Product::select('id', 'id as product_id')
            ->where(function ($query) {
                $query->where('lock_sale', false);
                $query->orWhere('lock_sale', null);
            })
            ->whereHas('price', function ($query) {
                $query->where('price', '!=', '0');
            })
            ->where(function ($query) {
                $query->orWhere('state', true);
                $query->orWhere('state', null);
            })
            ->whereIn('id', $request->productos)
            ->get()
            ->toArray();

        $inventory = Inventory::select(
            'bw.id as branchoffice_warehouse_id',
            'bw.warehouse_description as branchoffice_warehouse',
            'b.id as branchoffice_id',
            'b.name as branchoffice',
            'product_id',
            'stock',
            'stock_values',
            'available'
        )
            ->join('branchoffice_warehouses as bw', 'bw.id', '=', 'inventories.branchoffice_warehouse_id')
            ->join('branchoffices as b', 'b.id', '=', 'bw.branchoffice_id')
            ->where('bw.warehouse_code', '01')
            ->whereIn('product_id', $request->productos)
            ->get();

        // Recorrer los productos para asignar el precio
        foreach ($products as $key => $product) {

            $new = false;

            $minimum_price = $prices->where('product_id', $product['id'])->where('price_list_id', $minumum_price_list->id)->last();

            $products[$key]['minimum_price'] = !is_null($minimum_price) ? $minimum_price->price : null;

            $price = $prices->firstWhere('product_id', $product['id']);

            if ($price){
                $created_at_price = Carbon::parse($price->created_at);

                if ($created_at_price >  $date_if_new){
                    $new = false;
                }
            }

            $products[$key]['new'] = $new;

            $products[$key]['price'] = round($price->price * $factor);
            $products[$key]['price_list'] = $price->price;

            $products[$key]['inventory'] = [];

            $first_inventory = $inventory->where('branchoffice_warehouse_id', $branch_office_warehouse->id)
                ->where('product_id', $product['id'])
                ->first();

            if ($first_inventory){
                $products[$key]['inventory'][] = $first_inventory;
            }

            $others_inventories = $inventory->where('branchoffice_warehouse_id', '!=', $branch_office_warehouse->id)
                ->where('product_id', $product['id'])
                ->toArray();

            $others_inventories_branchoffice_warehouse = $this->getInventoryBranchofficeWarehouseNotExists($inventory, $product, $branchoffices);

            $others_inventories = array_merge($others_inventories, $others_inventories_branchoffice_warehouse);

            $products[$key]['inventory'] = array_merge($products[$key]['inventory'], $others_inventories);
        }

        return $products;
    }

    public function getInventoryBranchofficeWarehouseNotExists($inventories, $product, $branchoffices)
    {
        $branchoffice_id = $inventories->pluck('branchoffice_id')->toArray();

        $branchoffices = $branchoffices->whereNotIn('id', $branchoffice_id)
        ->toArray();

        $inventory=[];

        foreach ($branchoffices as $key => $branchoffice) {
            $inventory[]= [
                "branchoffice_warehouse_id"=> null,
                "branchoffice_warehouse"=> null,
                "branchoffice_id"=> $branchoffice["id"],
                "branchoffice"=> $branchoffice["name"],
                "product_id"=> $product['id'],
                "stock"=> 0,
                "stock_values"=> "0",
                "available"=> 0
            ];
        }

        return $inventory;
    }

}
