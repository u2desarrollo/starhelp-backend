<?php

namespace App\Services;

use App\Entities\Document;
use App\Entities\Inventory;
use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\VouchersType;
use App\Entities\City;
use App\Entities\Subbrand;
use App\Entities\Brand;
use App\Entities\ProductPromotion;
use App\Entities\Promotion;
use App\Entities\Parameter;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\TypesOperation;
use App\Repositories\DocumentRepository;

use App\Repositories\DocumentProductRepository;

use App\Traits\CacheTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Repositories\BranchOfficeWarehouseRepository;
use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;


class DocumentsProductsService extends BaseService
{
    use CacheTrait;

    private $documentProductRepository;
    private $productService;
    private $documentsService;
    private $documentRepository;
    private $branchOfficeWarehouseRepository;

    public function __construct(
        BranchOfficeWarehouseRepository $branchOfficeWarehouseRepository,
        DocumentProductRepository $documentProductRepo,
        ProductService $productService,
        DocumentRepository $documentRepository
    )
    {
        $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepository;
        $this->documentRepository = $documentRepository;
        $this->documentProductRepository = $documentProductRepo;
        $this->productService = $productService;
    }

    /**
     * Obtiene un documento por el id del mismo
     *
     * @param int $document_id Contiene el id del documento a consultar
     * @return mixed Contiene el objeto del documento obtenido
     * @author Jhon García
     */
    public function getDocument(int $document_id)
    {
        return Cache::remember('document_' . $document_id, 1, function () use ($document_id) {
            return Document::select('id', 'vouchertype_id', 'branchoffice_warehouse_id', 'trm_value')->find($document_id);
        });
    }

    /**
     * Cargar productos desde un csv a un documento
     *
     * @param int $document_id Contiene el id del documento
     * @param Request $request Contiene parametros adicionales para la carga de productos
     * @return array|string[] Contiene los productos cargados o mensajes de error
     * @author Kevin Galindo
     */
    public function uploadFileProductsToDocumentService($document_id, $request)
    {
        set_time_limit(0);

        //Variables
        $model = $request->model_id;
        $warehouse_id = $request->warehouse_id;
        $branchoffice_warehouse_id = $request->branchoffice_warehouse_id;
        $branchoffice_warehouse_id_product = $request->branchoffice_warehouse_id_product;
        $productsNotExist = [];
        $newDocumentProducts = [];

        // Validamos la SEDE
        if ($branchoffice_warehouse_id_product) {
            $branchOfficeWarehouse = BranchOfficeWarehouse::find($branchoffice_warehouse_id_product);
        } else {
            $branchOfficeWarehouse = BranchOfficeWarehouse::find($branchoffice_warehouse_id);
        }

        // Traemos el documento
        $document = Document::find($document_id);

        //Recibimos la información
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());

        //Validamos que la informacion devuelta no sea un valor boolean(false)
        if ($fileData === false) {
            return [
                'message' => 'Error al convertir el archivo csv, recuerde que el archivo debe estar delimitado por ";"',
                'code'    => '500'
            ];
        }

        // Recorremos el archivo
        foreach ($fileData as $key => $value) {

            if (empty($value['CODIGO'])) {
                continue;
            }

            $product = Product::select('id')
                ->where('code', trim($value['CODIGO']))
                ->first();

            if ($product) {

                $dataSave = [
                    'is_benefit'                        => false,
                    'quantity'                          => $value['CANTIDAD'],
                    'model_id'                          => $model,
                    'seller_id'                         => $document->seller_id,
                    'promotion_id'                      => null,
                    'document_id'                       => $document->id,
                    'product_id'                        => $product->id,
                    'contact_id'                        => $document->contact_id,
                    'branchoffice_id'                   => $branchOfficeWarehouse->branchoffice->id,
                    'branchoffice_storage'              => $branchOfficeWarehouse->branchoffice->id,
                    'warehouse_id'                      => $warehouse_id,
                    'branchoffice_warehouse_id_product' => $branchoffice_warehouse_id_product,
                ];

                if (!empty($value['V-UNI'])) {
                    $unit_value_before_taxes = str_replace(",", ".", $value['V-UNI']);
                    $dataSave['unit_value_before_taxes'] = (float)(trim($unit_value_before_taxes));
                }

                if (!empty($value['COSTO-TOTAL'])) {
                    $total_cost = str_replace(",", ".", $value['COSTO-TOTAL']);
                    $dataSave['inventory_cost_value_unit'] = (float)(trim($total_cost)) / $dataSave['quantity'];
                }

                $newDocumentProducts[] = $dataSave;
            } else {
                $productsNotExist[] = $value['CODIGO'];
            }
        }

        // Validamos que los productos que se cargan existan en su totalidad
        if (count($productsNotExist) > 0) {
            return [
                'message' => [
                    'text' => 'algunos productos no existen!',
                    'data' => $productsNotExist
                ],
                'code'    => '400'
            ];
        }

        $documentProducts = [];

        foreach ($newDocumentProducts as $key => $newDocumentProduct) {
            $documentProducts[] = $this->createDocumentProduct((array)$newDocumentProduct);
        }

        return [
            'message' => $documentProducts,
            'code'    => '200'
        ];
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[0]) < 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    /**
     * Este metodo se encarga de calcular el "VBackOrder"
     * y segun "$getTrace" retorna el dato actual o toda la traza
     *
     * @author Kevin Galindo
     */
    public function calculateBackOrder($document_product_id, array $request, $getTrace = false)
    {
        //Variables
        $model_id = $request['model_id'];

        //Traemos el modelo
        $model = Template::find($model_id);

        //Traemos el documento seleccionado
        $documentProductSelected = DocumentProduct::find($document_product_id);

        //Capturamos el tipo de operacion
        $operationTypeModel = $model->operationType->affectation;

        // Capturtamos el documento Base.
        $baseDocumentId = isset($request['document_id']) ? $request['document_id'] : $documentProductSelected->document->id;
        $baseDocument = Document::find($baseDocumentId);

        // Traemos los documentos relacionados con el documento base.
        $documentsThread = $baseDocument->invoices->where('in_progress', false)->sortBy('id');

        // Creamos un variable para guardar los registros.
        $items = [];

        // Creamos los registros.
        $backOrder = 0;

        // Documentos relacionados
        $documentBaseAllInvoices = [];

        // Calculamos la cantidad del "BackOrder" del documento base.
        foreach ($baseDocument->documentsProducts as $key => $baseDocumentProducts) {


            if (
                $documentProductSelected->product_id == $baseDocumentProducts->product_id &&
                $baseDocumentProducts->quantity > 0 &&
                //$documentProductSelected->is_benefit == $baseDocumentProducts->is_benefit &&
                $documentProductSelected->promotion_id == $baseDocumentProducts->promotion_id &&
                $documentProductSelected->product_repeat_number == $baseDocumentProducts->product_repeat_number
            ) {
                // Asignamos el BackOrder
                $backOrder = $baseDocumentProducts->quantity;

                // Agregamos el registro
                $documentBaseAllInvoices [] = [
                    'id'                        => $baseDocument->id,
                    'date'                      => $baseDocument->document_date,
                    'document_vouchertype_name' => $baseDocument->vouchertype->name_voucher_type,
                    'consecutive'               => $baseDocument->consecutive,
                    'invoice_link'              => $baseDocument->invoice_link,
                    'quantity'                  => $baseDocumentProducts->quantity,
                    'backorder'                 => $backOrder,
                    'operationType'             => $baseDocument->operationType_id,
                    'is_benefit'                => $baseDocumentProducts->is_benefit,
                    'prefix'                    => $baseDocumentProducts->prefix,
                    'in_progress'               => $baseDocumentProducts->in_progress,
                    'promotion_id'              => $baseDocumentProducts->promotion_id,
                    'original_quantity'         => $baseDocumentProducts->original_quantity
                ];
            }
        }

        // Guardamos todos los documentos relacionados.
        foreach ($documentsThread as $key => $documentThread) {
            foreach ($documentThread->documentsProducts as $key => $documentsProductsThread) {
                if ($documentsProductsThread->quantity <= 0) continue;
                if ($documentProductSelected->product_id != $documentsProductsThread->product_id) continue;
                if ($documentProductSelected->promotion_id != $documentsProductsThread->promotion_id) continue;
                // if ($documentProductSelected->is_benefit != $documentsProductsThread->is_benefit) continue;
                if ($documentThread->id == $baseDocumentId) continue;

                if ($documentsProductsThread->product_repeat_number > 0) {
                    $existInBase = $baseDocument->documentsProducts
                        ->where('product_id', $documentsProductsThread->product_id)
                        ->where('product_repeat_number', $documentsProductsThread->product_repeat_number)
                        ->first();

                    // if ($document_product_id == '52672') {
                    //     dd($existInBase);
                    // }

                    if ($existInBase) {
                        if ($documentProductSelected->product_repeat_number != $documentsProductsThread->product_repeat_number) continue;
                    }
                }

                $key_product_array = array_search(
                    $documentThread->id,
                    array_column($documentBaseAllInvoices, 'id')
                );

                // Crear
                if ($key_product_array === false) {
                    $documentBaseAllInvoices[] = [
                        'id'                        => $documentThread->id,
                        'date'                      => $documentThread->document_date,
                        'document_vouchertype_name' => $documentThread->vouchertype->name_voucher_type,
                        'consecutive'               => $documentThread->consecutive,
                        'invoice_link'              => $documentThread->invoice_link,
                        'quantity'                  => $documentsProductsThread->quantity,
                        'backorder'                 => 0,
                        'operationType'             => $documentThread->template->operationType->affectation,
                        'operationType_id'          => $documentThread->operationType->id,
                        'is_benefit'                => $documentsProductsThread->is_benefit,
                        'prefix'                    => $documentThread->prefix,
                        'in_progress'               => $documentThread->in_progress,
                        'promotion_id'              => $documentsProductsThread->promotion_id,
                        'original_quantity'         => $documentsProductsThread->original_quantity
                    ];
                } // Actualizar
                else {
                    $documentBaseAllInvoices[$key_product_array]['quantity'] += $documentsProductsThread->quantity;
                }

                if ($documentThread->invoices->count() > 0) {
                    foreach ($documentThread->invoices as $key => $documentThreadInvoices) {
                        foreach ($documentThreadInvoices->documentsProducts as $key => $documentProductsThreadInvoices) {

                            // Validaciones
                            if ($documentProductsThreadInvoices->quantity <= 0) continue;
                            if ($documentProductSelected->product_id != $documentProductsThreadInvoices->product_id) continue;
                            if ($documentProductSelected->promotion_id != $documentProductsThreadInvoices->promotion_id) continue;
                            // if ($documentProductSelected->is_benefit != $documentProductsThreadInvoices->is_benefit) continue;
                            if ($documentProductSelected->product_repeat_number != $documentProductsThreadInvoices->product_repeat_number) continue;
                            if ($documentThreadInvoices->id == $baseDocumentId) continue;

                            // Asignar Valores
                            $documentBaseAllInvoices[] = [
                                'id'                        => $documentThreadInvoices->id,
                                'date'                      => $documentThreadInvoices->document_date,
                                'document_vouchertype_name' => $documentThreadInvoices->vouchertype->name_voucher_type,
                                'consecutive'               => $documentThreadInvoices->consecutive,
                                'invoice_link'              => $documentThreadInvoices->invoice_link,
                                'quantity'                  => $documentProductsThreadInvoices->quantity,
                                'backorder'                 => 0,
                                'operationType'             => $documentThreadInvoices->template->operationType->affectation,
                                'operationType_id'          => $documentThreadInvoices->operationType->id,
                                'is_benefit'                => $documentProductsThreadInvoices->is_benefit,
                                'prefix'                    => $documentThreadInvoices->prefix,
                                'in_progress'               => $documentThreadInvoices->in_progress,
                                'promotion_id'              => $documentProductsThreadInvoices->promotion_id,
                                'original_quantity'         => $documentProductsThreadInvoices->original_quantity
                            ];
                        }
                    }
                }


            }
        }

        // Organizamos el objeto
        $aux = [];
        foreach ($documentBaseAllInvoices as $key => $row) {
            if ($row['in_progress'] == true) {
                unset($documentBaseAllInvoices[$key]);
                continue;
            }
            $aux[$key] = $row['id'];
        }

        array_multisort($aux, SORT_ASC, $documentBaseAllInvoices);

        // Calculamos el BackOrder
        foreach ($documentBaseAllInvoices as $key => $documentBaseInvoices) {

            if ($documentBaseInvoices['id'] == $baseDocument->id) continue;

            if ($operationTypeModel == 1) {
                if ($documentBaseInvoices['operationType'] != 1) {
                    $backOrder = $documentBaseInvoices['quantity'];
                } else {
                    $backOrder -= $documentBaseInvoices['quantity'];
                }
            } else {
                //$backOrder -= $documentBaseInvoices['quantity'];
                switch ($documentBaseInvoices['operationType']) {
                    case 2:
                    case 3:
                        $backOrder -= $documentBaseInvoices['quantity'];
                        // $backOrder = $backOrder < 0 ? 0 : $backOrder;
                        break;
                    case 1:
                        if ($model_id == 23 && $documentBaseInvoices['operationType_id'] == 7) {
                            $backOrder = 0;
                        } else {
                            $backOrder += $documentBaseInvoices['quantity'];
                        }
                        break;
                    default:
                        if ($getTrace && $documentBaseInvoices['operationType'] == 5) {
                            $backOrder -= $documentBaseInvoices['quantity'];
                        }
                        break;
                }

            }
            $documentBaseAllInvoices[$key]['backorder'] = $backOrder;
        }

        if ($getTrace) {
            return $documentBaseAllInvoices;
        } else {
            return end($documentBaseAllInvoices);
        }
    }

    /**
     * Este metodo se encarga de validar y ajustar la informacion para guardar
     * un producto al documento
     *
     * @param $request
     * @return mixed
     * @author Kevin Galindo
     */
    public function createDocumentProduct($request)
    {
        if (is_array($request)) {
            $request_data = $request;

            $request = new Request();

            $request->merge($request_data);
        }

        $request->merge(['validatePromotion' => false]);

        $document_product = null;
        $base_unit_value = $request->base_unit_value;
        $document_product_id = $request->document_product_id;
        $is_benefit = $request->is_benefit;
        $quantity = $request->quantity;
        $original_quantity = $request->original_quantity;
        $model_id = $request->model_id;
        $seller_id = $request->seller_id;
        $promotion_id = $request->promotion_id;
        $document_id = $request->document_id;
        $product_id = $request->product_id;
        $contact_id = $request->contact_id;
        $manage_inventory = $request->manage_inventory;
        $physical_unit = $request->physical_unit;
        $unit_value_before_taxes = $request->unit_value_before_taxes;
        $discount_value = (float)$request->discount_value;
        $discount_value_us = (float)$request->discount_value_us;
        $description = $request->description;
        $total_value_iva = $request->total_value_iva;
        $total_value_brut = $request->total_value_brut;
        $inventory_cost_value_unit = $request->inventory_cost_value_unit;
        $account_id = $request->account_id;

        // Buscamos el documento
        $doc = Document::select('id', 'vouchertype_id', 'branchoffice_warehouse_id', 'trm_value')->find($document_id);

        $model = $this->getModelForIdFromCache($model_id);

        if ($document_product_id) {
            $document_product = DocumentProduct::with('line.taxes')->find($document_product_id);
        }

        // Seteamos la bodega
        $request->branchoffice_warehouse_id = $doc->branchoffice_warehouse_id;

        // Bodega y Sede por defecto segun el modelo.
        if ($model) {
            if (!empty($model->branchoffice_id)) {
                $request->branchoffice_id = $model->branchoffice_id;
            }

            if (!empty($model->branchoffice_warehouse_id)) {
                $branchOfficeWarehouse = $this->getBranchOfficeWarehouse($request->branchoffice_id, $model->branchOfficeWarehouse->warehouse_code);
                if ($branchOfficeWarehouse) {
                    $request->branchoffice_warehouse_id = $branchOfficeWarehouse->id;
                }
            }
        }

        //Traemos el producto
        if ($document_product) {
            $product = $document_product->toArray();
        } else {
            $product = $this->productService->getProduct($product_id, $request);
        }

        // Seteamos el vr unit
        $product = $this->validateUnitValueOfTheProduct($product, [
            'unit_value_before_taxes' => $unit_value_before_taxes,
            'quantity'                => $quantity,
        ]);

        $product = $this->validateTotalValueBrutOfTheProduct($product, $doc, $model, [
            'discount_value_us' => $discount_value_us,
            'discount_value'    => $discount_value,
            'quantity'          => $quantity,
            'total_value_brut'  => $total_value_brut,
        ]);

        // Beneficio
        $product = $this->validateIsBenefitDocumentProduct($document_product, $product, $model, $is_benefit);

        $product['quantity'] = $quantity;
        $product['product_id'] = $document_product ? $document_product['product_id'] : $product['id'];
        $product['promotion_id'] = $promotion_id;
        $product['document_id'] = $document_id;
        $product['seller_id'] = $seller_id;
        $product['contact_id'] = $contact_id;
        $product['base_unit_value'] = $base_unit_value;
        $product['inventory_cost_value'] = $inventory_cost_value_unit * $quantity;
        $product['manage_inventory'] = $manage_inventory !== null ? $manage_inventory : $product['manage_inventory'];
        $product['operationType_id'] = $model ? $model->operationType_id : null;
        $product['fixed_cost'] = $model->edit_cost ? true : false;
        $product['original_quantity'] = $original_quantity ? $original_quantity : $quantity;
        $product['description'] = $description ? $description : $product['description'];
        $product['account_id'] = $account_id ? $account_id : null;

        $product = $this->validateBranchofficeWarehouseOftheProduct($product, $model, $request);

        // Cantidad fisica
        $this->validatedPhysicalQuantity($product, $model, [
            'document_id'   => $document_id,
            'product_id'    => $product_id,
            'is_benefit'    => $is_benefit,
            'quantity'      => $quantity,
            'physical_unit' => $physical_unit,
        ]);

        // Validamos si debemos calcular los totales segun el modelo
        if ($model->priceList && $model->priceList->code_parameter == '002') {
            $newDocumentProduct['total_value'] = 0;
            $newDocumentProduct['total_value_brut'] = 0;
            $newDocumentProduct['unit_value_before_taxes'] = 0;
        }

        $document_product = $this->documentProductRepository->create($product, $document_product_id);

        if ($model->pay_taxes) {
            $this->createDocumentTaxes($product, $document_product, $model->operationType, $total_value_iva, $model);
        }

        $this->validateTotalValueOfTheProduct($product, $model, $document_product, $is_benefit, $doc);

        $request->model = $model;
        $request->documentProduct = $document_product;
        $request->document = $doc;
        $document_product = $this->documentProductRepository->getDocumentProductForId($document_product->id, $request);

        return $document_product;
    }

    public function validateTotalValueOfTheProduct($product, $model, $documentProduct, $is_benefit, $doc)
    {
        if ($model->TRM_value && $model->TRM_value != 2) {

            $trmValue = (float)$doc->trm_value;

            // Total documento.
            $totalTaxes = round($documentProduct->documentProductTaxes->sum('value'));
            $totalValueUs = $documentProduct->total_value_brut_us + $totalTaxes;
            $totalValue = $totalValueUs > 0 ? $totalValueUs * $trmValue : 0;

            if ($is_benefit) {
                // Totales
                $documentProduct->total_value_brut = 0;
                $documentProduct->total_value = $totalValueUs > 0 ? $totalValueUs * $trmValue : 0;
                $documentProduct->total_value_us = $totalTaxes;
            } else {
                // Totales
                $documentProduct->total_value = $totalValue;
                $documentProduct->total_value_us = $totalValueUs;
            }

        } else {

            // Total documento.
            $totalTaxes = round($documentProduct->documentProductTaxes->sum('value'));
            $totalValue = $documentProduct->total_value_brut + $totalTaxes;
            $totalValueUs = 0;

            // Obsequio
            if ($is_benefit) {
                $documentProduct->total_value_brut = 0;
                $documentProduct->total_value = $totalTaxes;
                $documentProduct->total_value_us = $totalValueUs;
            } else {
                // Totales
                $documentProduct->total_value = $totalValue;
                $documentProduct->total_value_us = $totalValueUs;
            }
        }

        $documentProduct->save();

        return $product;
    }

    public function validatedPhysicalQuantity($product, $model, $params)
    {
        if ($model && $model->see_physical_cant) {
            $current_document_product = DocumentProduct::where('document_id', $params['document_id'])
                ->where('product_id', $params['product_id'])
                ->where('is_benefit', $params['is_benefit'])
                ->first();

            if ($current_document_product) {
                if ($current_document_product->quantity != $params['quantity']) {
                    $product['physical_unit'] = $params['quantity'];
                } else {
                    $product['physical_unit'] = $params['physical_unit'];
                }
            } else {
                $product['physical_unit'] = $product['quantity'];
            }
        }

        return $product;
    }

    public function updateDocumentProductsValues($request)
    {
        $saveResults = [];

        try {

            DB::beginTransaction();

            foreach ($request->products as $key => $product) {
                $saveResults[] = $this->createDocumentProduct((array)$product);
            }

            DB::commit();

            return [
                'data' => $saveResults,
                'code' => '200'
            ];

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());

            return [
                'code' => '500'
            ];
        }
    }

    /**
     * Este metodo se encarga de validar que el
     * valor unitario no quede en 0 y asigne un valor si viene
     * por el $request
     *
     * @param $document_product
     * @param $params
     * @return
     * @author Kevin Galindo
     */
    public function validateUnitValueOfTheProduct($document_product, $params)
    {
        $price_list = isset($document_product['price']['price']) ? round($document_product['price']['price']) : $document_product['unit_value_before_taxes'];

        $document_product['unit_value_list'] = $price_list;

        if ($params['quantity'] > 0) {
            if (!$params['unit_value_before_taxes']) {
                $document_product['unit_value_before_taxes'] = $price_list;
            } else {
                $document_product['unit_value_before_taxes'] = (float)$params['unit_value_before_taxes'];
            }
        } else {
            $document_product['unit_value_before_taxes'] = $price_list;
        }

        return $document_product;
    }

    /**
     * calcular el valor bruto del producto
     * teniendo en cuenta el trm si es que maneja.
     *
     * @author Kevin Galindo
     */
    public function validateTotalValueBrutOfTheProduct($product, $doc, $model, $params)
    {
        // Calculos el total bruto para el trm.
        if ($model->TRM_value && $model->TRM_value != 2) {

            // Valor TRM
            $trmValue = (float)$doc->trm_value;

            // Valor Bruto
            $totalValueBrutUs = (float)$product['unit_value_before_taxes'] * $params['quantity'];

            // Descuento.
            if ($params['discount_value_us'] > 0) {
                $totalDiscountValueUs = $params['discount_value_us'] < 1 ? ($totalValueBrutUs * $params['discount_value_us']) : $params['discount_value_us'];
                $totalDiscountValue = $totalDiscountValueUs > 0 ? $totalDiscountValueUs * $trmValue : 0;
            } else {
                $totalDiscountValueUs = 0;
                $totalDiscountValue = 0;
            }

            // Total bruto con descuento
            $totalValueBrutWithDiscountValueUs = $totalValueBrutUs > 0 ? $totalValueBrutUs - $totalDiscountValueUs : 0;
            $totalValueBrutWithDiscountValue = $totalValueBrutWithDiscountValueUs > 0 ? $totalValueBrutWithDiscountValueUs * $trmValue : 0;

            # Actualizamos
            // normal
            $totalValueBrut = $totalValueBrutWithDiscountValue;
            $product['total_value_brut'] = $totalValueBrut;
            $product['discount_value'] = $totalDiscountValue;

            // trm
            $product['total_value_brut_us'] = $totalValueBrutWithDiscountValueUs;
            $product['discount_value_us'] = $totalDiscountValueUs;


        } // Calculos normales
        else {
            // Valor Bruto
            $totalValueBrut = round($product['unit_value_before_taxes'] * $params['quantity']);

            // Descuento
            if ($params['discount_value'] < 1) {
                $totalDiscountValue = round($totalValueBrut * $params['discount_value']);
            } else {
                // Validamos que el descuento no sea mayor que el bruto
                if ($totalValueBrut >= $params['discount_value']) {
                    $totalDiscountValue = $params['discount_value'];
                } else {
                    $totalDiscountValue = 0;
                }

            }

            // Total bruto con descuento
            $totalValueBrutWithDiscountValue = $totalValueBrut > 0 ? $totalValueBrut - $totalDiscountValue : 0;

            # Actualizamos
            // normal
            if ($params['total_value_brut']) {
                $product['total_value_brut'] = $params['total_value_brut'];
            } else {
                $product['total_value_brut'] = $totalValueBrutWithDiscountValue;
                $product['discount_value'] = $totalDiscountValue;
            }

            // trm
            $product['total_value_brut_us'] = 0;
            $product['discount_value_us'] = 0;
        }

        return $product;
    }


    /**
     * Este metodo se encarga de validar
     * la bodega del producto
     *
     * @param array $product
     * @param Template $model
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function validateBranchofficeWarehouseOftheProduct($product, $model, $request)
    {
        if (!empty($model->branchoffice_change) && $request->branchoffice_warehouse_id_product) {
            $product['branchoffice_warehouse_id'] = $request->branchoffice_warehouse_id_product;
        } else {
            $product['branchoffice_warehouse_id'] = $request->branchoffice_warehouse_id;
        }

        return $product;
    }

    /**
     * Este método se encarga de validar
     * si un producto es un obsequio.
     *
     * @param DocumentProduct $documentProduct
     * @param array $product
     * @param Template $model
     * @param boolean $is_benefit
     * @return array
     * @author Kevin Galindo
     */
    public function validateIsBenefitDocumentProduct($documentProduct, $product, $model, $is_benefit)
    {
        // Validar que el producto tenga cantidad.
        if ($documentProduct && $documentProduct->quantity === 0) {
            $is_benefit = false;
        } else if (!$model->hide_values && $documentProduct && $is_benefit !== true) {
            if ($product['total_value_brut'] <= 0) {
                $is_benefit = true;
            } else {
                $is_benefit = false;
            }
        } else if (is_null($is_benefit)) {
            $is_benefit = false;
        }

        $product['is_benefit'] = $is_benefit;

        return $product;
    }

    /**
     * Este metodo se encarga de calcular los
     * valores totales del document_product
     *
     * @author Kevin Galindo
     */
    public function calculateSaveTotalsValues($document_product_id, $model)
    {
        $total_value_taxes = 0;
        $total_value_brut = 0;

        // Traemos el document_product
        $documentProduct = DocumentProduct::find($document_product_id);

        if ($documentProduct) {
            // Validamos si se debe cobrar los impuestos segun el modelo.
            if ($model->pay_taxes) {
                // Traemos los totales de los impuestos ya sumados
                $total_value_taxes = $documentProduct->documentsProductsTaxes->sum('value');
            }

            // traemos el valor total bruto
            $total_value_brut = $documentProduct->is_benefit ? 0 : $documentProduct->total_value_brut;

            // Sumamos los totales
            $total_value = $total_value_taxes + $total_value_brut;

            // Actualizamos el registro
            $documentProduct->update([
                'total_value'      => round($total_value),
                'total_value_brut' => round($total_value_brut)
            ]);
        }

        return $documentProduct;
    }

    public function createDocumentTaxes($product, $documentProduct, $operationType, $total_value_iva = null, $model = null)
    {
        // Validamos si el precio del producto maneja IVA
        $include_iva = $this->validateProductIncludeIva($model);

        // Seleccionamos los impuestos del producto
        $taxes = $product['line']['taxes'];

        // Vr bruto
        $total_value_brut = 0;

        // Calcular TAXES
        if ($taxes) {

            // Recorremos los impuestos
            foreach ($taxes as $keyt => $tax) {

                $valueTax = 0;
                $taxPercentage = 0;

                // Ventas
                if (in_array($operationType->code, ["206", "205", "155", "010", "260", "156"])) {
                    $taxPercentage = (float)$tax['percentage_sale'];
                } else if (in_array($operationType->code, ["105", "020", "270", "015", "271"])) { // Compras
                    $taxPercentage = (float)$tax['percentage_purchase'];
                }

                $total_value_brut = $product['is_benefit']
                    ? round($product['unit_value_before_taxes'] * $product['quantity'])
                    : round($documentProduct->total_value_brut);

                // Calcula el valor por el valor Bruto
                if ($tax['base'] == 1) {
                    $valueTax = round(($total_value_brut * $taxPercentage) / 100);
                }

                $taxesDocumentProduct = [
                    'document_product_id' => $documentProduct->id,
                    'tax_id'              => $tax['id'],
                    'base_value'          => $total_value_brut,
                    'tax_percentage'      => $taxPercentage,
                    'value'               => $include_iva ? 0 : round($valueTax)
                ];

                if ($tax['id'] == 1) {
                    if ($total_value_iva !== null) {
                        $taxesDocumentProduct['value'] = round($total_value_iva);
                    }
                }

                // Creamos el registro del impuesto
                $this->documentProductRepository->createTaxes($taxesDocumentProduct);
            }
        }

        return $documentProduct;
    }

    /**
     * Este metodo valida si un producto incluye iva en su precio
     * y retorna un boolean.
     *
     * @param Int product_id
     * @param Int price_list_id
     * @return boolean
     * @author Kevin Galindo
     */
    public function validateProductIncludeIva($model)
    {
        $include_iva = false;

        if ($model->priceList) {
            if ($model->priceList->paramtable->code_table === 'LDP' && $model->priceList->alphanum_data_second === 'SI') {
                $include_iva = true;
            }
        }

        return $include_iva;
    }

    public function createCsv($documentProducts, $request, $devol = null)
    {

        $fieldsCsvRow1 = [];
        $fieldsCsvRow2 = [];


        if ($request->type_operations) {
            $name_voucher_type = TypesOperation:: select('description')->where('id', $request->type_operations[0])->first()->description;
        }
        $fieldsCsvRow1[] = $name_voucher_type;
        if ($request->city && is_array($request->city)) {
            $cities = City:: select('city')->whereIn('id', $request->city)->get();
            foreach ($cities as $key => $citie) {
                $fieldsCsvRow1[] = $citie->city;
            }
        }
        $fieldsCsvRow1[] = $request->date[0];
        $fieldsCsvRow1[] = $request->date[1];


        $fieldsCsvRow3[] = 'TIPO-OPERACION';
        $fieldsCsvRow3[] = 'FECHA-HORA';
        $fieldsCsvRow3[] = 'SEDE COMPROBANTE';
        $fieldsCsvRow3[] = 'SEDE INVENTARIO';
        $fieldsCsvRow3[] = 'BODEGA INVENTARIO';
        $fieldsCsvRow3[] = 'NUM-DOCUM';
        $fieldsCsvRow3[] = 'IDENT-TERCERO';
        $fieldsCsvRow3[] = 'NOMBRE-TERCERO';
        $fieldsCsvRow3[] = 'VENDEDOR';
        $fieldsCsvRow3[] = 'ZONA-VENDEDOR';
        $fieldsCsvRow3[] = 'CIUDAD-TERCERO';
        $fieldsCsvRow3[] = 'COD-PRODUCTO';
        $fieldsCsvRow3[] = 'DESCRIPCION-PRODUCTO';
        $fieldsCsvRow3[] = 'LINEA';
        $fieldsCsvRow3[] = 'SUBLINEA';
        $fieldsCsvRow3[] = 'MARCA';
        $fieldsCsvRow3[] = 'SUBMARCA';
        $fieldsCsvRow3[] = 'PRECIO-UNIT';
        $fieldsCsvRow3[] = 'CANT';

        $fieldsCsvRow3[] = 'VR-BRUTO';
        $fieldsCsvRow3[] = 'VR-IVA';
        $fieldsCsvRow3[] = 'VR-TOTAL';
        $fieldsCsvRow3[] = 'VR-COSTO';

        //VALIDAR
        $fieldsCsvRow3[] = 'VR-UTILIDAD';
        $fieldsCsvRow3[] = 'PREFIJO CRUCE';
        $fieldsCsvRow3[] = 'CONSECUTIVO CRUCE';
        $fieldsCsvRow3[] = 'FECHA CRUCE';
        $fieldsCsvRow3[] = 'DOCUMENTO BASE';
        $fieldsCsvRow3[] = 'MOTIVO DEVOLUCION';
        $fieldsCsvRow3[] = 'GALONES';
        $fieldsCsvRow3[] = 'OBSERVACIONES';

        $fieldsCsvRow4 = [];

        $date = date('Y-m-d');
        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle(substr($fieldsCsvRow1[0], 0, 15) . ' ' . $fieldsCsvRow1[1] . '-' . substr($fieldsCsvRow1[2], 8, 2));

        $letter = 'A';
        $row = 4;
        $months_security = 2;

        $sheet->setCellValue('A1', $fieldsCsvRow1[0]);
        $sheet->setCellValue('B1', $fieldsCsvRow1[1]);
        $sheet->setCellValue('C1', $fieldsCsvRow1[2]);
        $total_letters = 'A';
        foreach ($documentProducts as $line) {
            $letter = 'A';
            foreach ($line as $key => $value) {
                $total_letters = $letter;


                if ($key == 'data_seet') {
                    if (!is_null($value)) {
                        $data_sheett = null;
                        foreach (json_decode($value) as $key => $item) {
                            if ($item->data_sheet_id == 22) {
                                $data_sheett = trim($item->value);
                            }
                        }
                        $value = $data_sheett != null && $data_sheett != 'UNIDAD' && $data_sheett != '' ? $data_sheett * $line->quantity : 0;
                    }
                }


                if ($key == 'document_date') {
                    $sheet->setCellValue($letter++ . $row, Date::PHPToExcel($value));
                } else {
                    $sheet->setCellValue($letter++ . $row, $value);
                }
                // echo $letter++ . '-' . $row. '  '. $value . '=========================';
            }
            $row++;
        }

        $row--;

        $sheet->setAutoFilter('A3:' . $total_letters . $row);

        $this->setStyles($sheet, $row, $total_letters, $fieldsCsvRow3);

        $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

        $fileName = storage_path("app/public/CSVUsers/") . $fieldsCsvRow1[0] . ' ' . $fieldsCsvRow1[1] . '_' . $fieldsCsvRow1[2] . '.xlsx';
        $writer->save($fileName);


        return '/public/CSVUsers/' . $fieldsCsvRow1[0] . ' ' . $fieldsCsvRow1[1] . '_' . $fieldsCsvRow1[2] . '.xlsx';
    }

    public function setStyles($sheet, $row, $total_letters, $headers)
    {
        $letter_ = 'A';
        $styles = [];
        for ($i = 0; $i < count($headers); $i++) {

            $styles[$letter_] = ['width' => 20];

            if ($headers[$i] == 'DESCRIPCION-PRODUCTO') {
                $styles[$letter_] = ['width' => 90];
            }

            $letter_++;
        }


        $generalStyles = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'borders'   => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN
                ]
            ]
        ];
        $generalSNumtyles = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'borders'   => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN
                ]
            ]
        ];

        $counter = 0;

        foreach ($styles as $letter => $style) {
            $sheet->setCellValue($letter . '3', $headers[$counter++]);
            $sheet->getStyle($letter . '3')->getAlignment()->setWrapText(true);
            $sheet->getStyle($letter . '3')->getFont()->setBold(true);
            $sheet->getStyle($letter . '3')->getFont()->getColor()->setARGB('FFFFFF');
            $sheet->getStyle($letter . '3')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('6386D3');
            $sheet->getStyle($letter . '3')->getFill()->getEndColor()->setARGB('6386D3');
            $sheet->getColumnDimension($letter)->setWidth($style['width']);
            $sheet->getColumnDimension($letter)->setAutoSize(isset($style['autoSize']) ? $style['autoSize'] : false);
            $sheet->getStyle($letter . '2:' . $letter . $row)->getAlignment()->setWrapText(isset($style['wrapText']) ? $style['wrapText'] : false);
        }

        $letter_ = 'A';
        for ($i = 0; $i < count($headers); $i++) {

            $sheet->getStyle($letter_ . '3:' . $letter_ . $row)->applyFromArray($generalStyles);

            if ($headers[$i] == 'VR-BRUTO' || $headers[$i] == 'VR-IVA' || $headers[$i] == 'VR-TOTAL' || $headers[$i] == 'VR-COSTO' || $headers[$i] == 'VR-UTILIDAD' || $headers[$i] == 'PRECIO-UNIT') {
                $sheet->getStyle($letter_ . '3:' . $letter_ . $row)->applyFromArray($generalSNumtyles);
            }

            if ($headers[$i] == 'FECHA-HORA') {
                $sheet->getStyle($letter_ . '3:' . $letter_ . $row)->getNumberFormat()->setFormatCode(
                    NumberFormat::FORMAT_DATE_DATETIME
                );
            }

            $letter_++;


        }

    }

    /**
     * Este metodo se encarga de validar si un producto tiene descuento
     * en la promocion
     *
     * @author Kevin Galindo
     */
    public function validateDiscountProduct($product, $promotion_id)
    {
        // Validamos que sea solo promociones de mix
        if (Promotion::find($promotion_id)->promotion_type_id != 3) {
            return $product;
        }

        // Traemos el registro del producto en promocion
        $productPromotion = ProductPromotion::where('product_id', $product['product_id'])->where('promotion_id', $promotion_id)->first();

        // Validamos si hay registro y ademas si su valor de descuento es mayor a 0
        if ($productPromotion && $productPromotion->discount_value > 0) {
            // Asugnamos valores y recalculamos
            $product['unit_value_before_taxes'] = $productPromotion->discount_value;
            $product['total_value_brut'] = $product['unit_value_before_taxes'] * $product['quantity'];
        }

        // Retornamos la informacion
        return $product;
    }

    public function reporNegative()
    {
        return DocumentProduct:: select('id', 'document_id', 'stock_after_operation', 'quantity', 'product_id')
            ->with(['product'  => function ($query) {
                $query->select('id', 'code', 'description');
            },
                    'document' => function ($query) {
                        $query->select('id', 'branchoffice_id', 'branchoffice_warehouse_id', 'prefix', 'consecutive');
                        $query->with(['branchoffice', 'branchofficeWarehouse']);

                    }
            ])
            ->where('stock_after_operation', '<', '0')->get();
    }

    public function createCsvNegat($documents_prods)
    {

        //Obtenemos los "Labels" para generar el archivo
        $fieldsCsvRow1 = [];

        //Declaramos las cabeceras de la respuesta para descargar el archivo.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $fieldsCsvRow3[] = 'codigo';
        $fieldsCsvRow3[] = 'descripcion';
        $fieldsCsvRow3[] = 'saldo unidades';
        $fieldsCsvRow3[] = 'sede';
        $fieldsCsvRow3[] = 'bodega';
        $fieldsCsvRow3[] = 'consecutivo';


        $fieldsCsvRow4 = [];
        foreach ($documents_prods as $key => $documentProduct) {
            $fieldsCsvRow4[] = [
                $documentProduct->product->code,
                $documentProduct->product->description,
                $documentProduct->stock_after_operation,
                $documentProduct->document->branchoffice->name,
                $documentProduct->document->branchofficeWarehouse ? $documentProduct->document->branchofficeWarehouse->warehouse_description : null,
                $documentProduct->document->prefix . '-' . $documentProduct->document->consecutive
            ];
        }
        $column = [$fieldsCsvRow1];
        $callback = function () use ($column, $fieldsCsvRow4) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ($column as $row) {

                fputcsv($file, $row, ';');
            }

            foreach ($fieldsCsvRow4 as $key => $field) {
                fputcsv($file, $field, ';');
            }

            //Cerramos la escritura del archivo.
            fclose($file);
        };


        //Retornamos el archivo.
        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200
        ];

    }

    /**
     * corrige los costos de las decoluciones anteriores
     *
     * @author santiago torres
     */
    public function fixCostDevs()
    {
        $documents_products = DB:: select('select
            ddpp.id , ddpp.inventory_cost_value , ddpp.quantity
        from
            documents_products ddpp
        inner join documents dd on
            dd.id = ddpp.document_id
        where
            ddpp."operationType_id" = 20
            and dd.deleted_at is null
            and dd.in_progress is false');
        $data_flat_file = [];
        foreach ($documents_products as $key => $dp) {
            $new_cost = $dp->inventory_cost_value * $dp->quantity;
            $dp_new_cost = $this->documentProductRepository->updateCost($dp->id, $new_cost);
            $data_flat_file [] = 'id => ' . $dp->id . '  |  inventory_cost_value_before' . ' => ' . $dp->inventory_cost_value . '  |   inventory_cost_value_after' . ' => ' . $new_cost;
        }
        $data_flat_file [] = '_________________________________';

        $file = fopen(storage_path('app/colsaisa/costos_devoluciones_anteriores.txt'), "a");

        $lineBreak = "\r\n";
        foreach ($data_flat_file as $key => $line) {
            fwrite($file, $line .
                $lineBreak);
        }
    }

    public function createCsvByDocument($by_document, $request)
    {
        $fieldsCsvRow1 = [];
        $fieldsCsvRow2 = [];


        if ($request->type_operations) {
            $name_voucher_type = TypesOperation:: select('description')->where('id', $request->type_operations[0])->first()->description;
        }
        $fieldsCsvRow1[] = $name_voucher_type;
        if ($request->city && is_array($request->city)) {
            $cities = City:: select('city')->whereIn('id', $request->city)->get();
            foreach ($cities as $key => $citie) {
                $fieldsCsvRow1[] = $citie->city;
            }
        }
        $fieldsCsvRow1[] = $request->date[0];
        $fieldsCsvRow1[] = $request->date[1];

        $fieldsCsvRow3[] = 'TIPO-OPERACION';
        $fieldsCsvRow3[] = 'FECHA-HORA';
        $fieldsCsvRow3[] = 'SEDE';
        $fieldsCsvRow3[] = 'NUM-DOCUM';
        $fieldsCsvRow3[] = 'IDENT-TERCERO';
        $fieldsCsvRow3[] = 'NOMBRE-TERCERO';
        $fieldsCsvRow3[] = 'VENDEDOR';
        $fieldsCsvRow3[] = 'ZONA-VENDEDOR';
        $fieldsCsvRow3[] = 'CIUDAD-TERCERO';
        $fieldsCsvRow3[] = 'VR-BRUTO';
        $fieldsCsvRow3[] = 'VR-IVA';
        $fieldsCsvRow3[] = 'VR-TOTAL';
        $fieldsCsvRow3[] = 'VR-COSTO';
        $fieldsCsvRow3[] = 'VR-UTILIDAD';
        $fieldsCsvRow3[] = 'PREFIJO CRUCE';
        $fieldsCsvRow3[] = 'CONSECUTIVO CRUCE';
        $fieldsCsvRow3[] = 'FECHA CRUCE';
        $fieldsCsvRow3[] = 'DOCUMENTO BASE';
        $fieldsCsvRow3[] = 'MOTIVO DEVOLUCION';
        $fieldsCsvRow3[] = 'OBSERVACIONES';
        $fieldsCsvRow3[] = 'ID';
        $fieldsCsvRow4 = [];

        foreach (array_keys($by_document) as $key => $key_documnet) {

            // Totales
            $vr_iva = 0;
            $total_cost = 0;
            foreach ($by_document[$key_documnet] as $key => $value) {
                $vr_iva += $value->iva;
                $total_cost += $value->inventory_cost_value;
            }
            $fieldsCsvRow4[] = [
                $by_document[$key_documnet][0]->voucher_type,
                Date::PHPToExcel($by_document[$key_documnet][0]->document_date),
                $by_document[$key_documnet][0]->branchoffice_document,
                $by_document[$key_documnet][0]->num_doc,
                $by_document[$key_documnet][0]->contact_identification,
                $by_document[$key_documnet][0]->contact_name,
                $by_document[$key_documnet][0]->seller_name,
                $by_document[$key_documnet][0]->seller_zone,
                $by_document[$key_documnet][0]->contact_city,
                $by_document[$key_documnet][0]->total_value_brut,
                $vr_iva,
                $by_document[$key_documnet][0]->total_value,
                $total_cost,
                $by_document[$key_documnet][0]->operation_type_code == 205 || $by_document[$key_documnet][0]->operation_type_code == 155 ? $by_document[$key_documnet][0]->total_value_brut - $total_cost : null,
                $by_document[$key_documnet][0]->affects_prefix_document,
                $by_document[$key_documnet][0]->affects_number_document,
                $by_document[$key_documnet][0]->affects_date_document,
                $by_document[$key_documnet][0]->document_base_prefix,
                $by_document[$key_documnet][0]->motivo_devolucion_,
                $by_document[$key_documnet][0]->observaciones,
                $by_document[$key_documnet][0]->id
            ];


        }


        $date = date('Y-m-d');
        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle(substr($fieldsCsvRow1[0], 0, 15) . ' ' . $fieldsCsvRow1[1] . '-' . substr($fieldsCsvRow1[2], 8, 2));

        $letter = 'A';
        $total_letters = 'A';
        $row = 4;
        $months_security = 2;

        $sheet->setCellValue('A1', $fieldsCsvRow1[0]);
        $sheet->setCellValue('B1', $fieldsCsvRow1[1]);
        $sheet->setCellValue('C1', $fieldsCsvRow1[2]);

        foreach ($fieldsCsvRow4 as $line) {
            $letter = 'A';
            foreach ($line as $key => $value) {
                $total_letters = $letter;
                // echo $letter++ . '-' . $row. '  '. $value . '=========================';
                $sheet->setCellValue($letter++ . $row, $value);
            }
            $row++;
        }

        $row--;

        $sheet->setAutoFilter('A3:' . $total_letters . $row);

        $this->setStyles($sheet, $row, $total_letters, $fieldsCsvRow3);

        $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

        $fileName = storage_path("app/public/CSVUsers/") . $fieldsCsvRow1[0] . ' ' . $fieldsCsvRow1[1] . '_' . $fieldsCsvRow1[2] . '.xlsx';
        $writer->save($fileName);


        return '/public/CSVUsers/' . $fieldsCsvRow1[0] . ' ' . $fieldsCsvRow1[1] . '_' . $fieldsCsvRow1[2] . '.xlsx';
    }

    public function zeroReturns()
    {
        return $this->documentProductRepository->searcZeroReturns();
    }

    public function fixCost($request)
    {
        try {
            DB::beginTransaction();
            $dps = $this->documentProductRepository->getInfoForOperationsReport($request);
            $dpsfix = [];
            foreach ($dps as $key => $documentProduct) {
                $exclude_sales_report = false;
                if ($documentProduct->document->operationType->code == 205 || $documentProduct->document->operationType->code == 156 || $documentProduct->document->operationType->code == 155) {
                    if ($documentProduct->product->exclude_sales_report) {
                        $exclude_sales_report = true;
                    }
                }

                if (!$exclude_sales_report) {
                    if ($documentProduct->product->code != 'FLETE' && $documentProduct->quantity > 0) {
                        $param = $documentProduct->document->voucherType->code_voucher_type == '155' || $documentProduct->document->voucherType->code_voucher_type == '156' || $documentProduct->document->voucherType->code_voucher_type == '270' ? Parameter:: find($documentProduct->document->handling_other_parameters_value) : null;
                        if ($documentProduct->document->operationType_id == 1 || $documentProduct->document->operationType_id == 20) {
                            if ($documentProduct->inventory_cost_value == 0 || $documentProduct->inventory_cost_value == null) {
                                $dpsfix[] = [$documentProduct->id, $documentProduct->product_id];
                            }
                        }
                    }

                }
            }
            $new_cost = [];
            $havent_cost = [];

            foreach ($dpsfix as $key => $value) {
                //buscamos el costo del producto
                $cost = $this->documentProductRepository->SearchCostProduct($value[1], 1);
                if ($cost == null) {
                    $cost = $this->documentProductRepository->SearchCostProduct($value[1], 2);
                }
                if ($cost == null) {
                    $havent_cost [] = $value;
                } else {
                    $new_cost [] = $this->documentProductRepository->setCost($value[0], $cost->documentsProducts[0]->inventory_cost_value);
                }
            }

            DB::commit();

            return [$havent_cost, $new_cost];

        } catch (\Exception $e) {

            DB::rollBack();
            return (object)[
                'code' => '500',
                'data' => (object)[
                    'message' => 'Error en la actualizacion del documento: ' . $e->getMessage() . "\n" . $e->getLine() . "\n File:" . $e->getFile()
                ]
            ];
        }
    }

    public function validateKardex()
    {
        //lista de productos
        $list_products = $this->documentProductRepository->getProductFromDocuments();
        // lista bodegas
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->allBranchOfficeWarehouse();

        // parametros request
        $params = new Request();

        // fecha hyoy
        $now = new \DateTime();
        // seteamos parametro request fecha
        $params->merge(["date" => ['2020-07-01', $now->format('Y-m-d')]]);
        $errores = [];

        foreach ($list_products as $key => $product) {
            foreach ($branchOfficeWarehouses as $key => $branchOfficeWarehouse) {
                // seteamos producto y bodega de request
                $params->merge(["bodega_salida" => $branchOfficeWarehouse->branchoffice_warehouse_id]);
                $params->merge(["product" => $product->product_id]);

                // consultamos el kardex del poducto en la bodega
                $kardex_list = $this->documentProductRepository->searchKardex($params);

                $stock = 0;
                foreach ($kardex_list as $key => $kardex) {
                    // validamos si es una operacion de entrada o salida
                    if ($kardex->operationType->affectation == 1) {
                        // calculamos el stock
                        $stock = $stock + $kardex->quantity;
                    } elseif ($kardex->operationType->affectation == 2) {
                        // calculamos el stock
                        $stock = $stock - $kardex->quantity;
                    }
                    if ($stock != $kardex->stock_after_operation) {
                        // seteamos los errores
                        $errores[] = $kardex->product_id . '   el producto ' . $kardex->description . '-  ' . ' bodega ' . $kardex->document->branchoffice->name . '-' . $kardex->document->branchofficeWarehouse->warehouse_description . ' documento ' . $kardex->document->prefix . '' . $kardex->document->consecutive .
                            ' |stock esperado ' . $stock . ' saldo en valores ' . $kardex->stock_after_operation .
                            ' |document product id ' . $kardex->id;
                        break;
                    }
                }
            }
        }

        $file = fopen(storage_path('app/colsaisa/validate_kardex.txt'), "a");

        $lineBreak = "\r\n";
        foreach ($errores as $key => $error) {
            fwrite($file, $error .
                $lineBreak);
        }

    }

    public function verifyDocumentProductWM($request)
    {
        //dd($request->all());
        $v = \Validator::make($request->all(),[
            'id_document' => 'required|integer',
            'cod_document' => 'required|string',
            'name_user_wms' => 'required|string',
            'product_array.*.id_document_product' => 'required|integer',
            'product_array.*.cod_prod' => 'required|string',
            'product_array.*.quantity_total' => 'required|integer|min:1',
            'product_array.*.lot_array.*.number_lot' => 'string',
            'product_array.*.lot_array.*.quantity_lot' => 'integer|min:1',
        ],['product_array.*.lot_array.*.quantity_lot.*' => 'Quantity Lot, It must be greater than 0, Integer And required :attribute',
           'product_array.*.lot_array.*.number_lot.*' => 'Number Lot, It must be String And required :attribute',
           'product_array.*.quantity_total.*' => 'Quantity Total, It must be greater than 0, Integer And required :attribute',
           'product_array.*.cod_prod.*' => 'Code Product, It must be String And required :attribute',
           'product_array.*.id_document_product.*' => 'Code Product, It must be Integer And required :attribute',
           'cod_document.*' => 'Code Document, It must be String And required :attribute',
           'id_document.*' => 'Code Document, It must be Integer And required :attribute',
           'name_user_wms.*' => 'Name User WMS, It must be String And required :attribute',
          ]);


        if ($v->fails())
        {
            $errors = $v->errors();
            $resultExist = array(
                "data" => $errors->all(),
                "state" => "error",
                "msg" => "Errors Validate Information"
            );
        }else{
            $dwe = $this->documentProductRepository->getDocumentExistWms($request->id_document);
            $arraySendDocument = json_decode(json_encode(array("id_document"=>$request->id_document),true));
            $resultExist = array();
            if($dwe>0){
                $countProducts = count($request->product_array);
                //dd($countProducts);
                for ($x=0; $x <$countProducts ; $x++) {
                    $documentProduct = $this->documentProductRepository->getDocumentProductExistWms($request->id_document,$request->product_array[$x]['id_document_product'],$request->product_array[$x]['cod_prod']);
                    //dd($documentProduct);
                    $arraySend = json_decode(json_encode(array("id_document"=>$request->id_document,"id_product"=>"","cod_product"=>$request->product_array[$x]['cod_prod'],"quantity_wms"=>$request->product_array[$x]['quantity_total'],"lot_wms"=>$request->product_array[$x]['lot_array']),true));
                    $countDocumentProduct = !isset($documentProduct)?0:$documentProduct->count();
                    if($countDocumentProduct>0){

                        //dd(json_decode(json_encode($request->product_array[$x]['lot_array'],true)));
                        $lot = json_decode(json_encode($request->product_array[$x]['lot_array'],true));
                        $lot_origen = json_encode($request->product_array[$x]['lot_array']);
                        $documentProductDocumentUpdate = $this->documentProductRepository->updateDocumentProductExistWms($request->product_array[$x]['id_document_product'],$request->product_array[$x]['quantity_total'],$lot_origen);

                        if(isset($request->nit_protractor) && isset($request->name_user_wms)){
                            $documentsInformationWmsUpdate = $this->documentProductRepository->updateDocumentsInformationWms($request->id_document,$request->nit_protractor,$request->guide_number,$request->name_user_wms);
                        }
                        $documentsWmsUpdate = $this->documentProductRepository->updateDocumentsWms($request->id_document);
                        $documentProductDocumentUpdateJson = json_decode(json_encode(array("id_document"=>$documentProductDocumentUpdate->id_document,"id_product"=>$documentProductDocumentUpdate->id_product,"cod_product"=>$documentProductDocumentUpdate->code_product,"quantity_wms"=>$documentProductDocumentUpdate->quantity_wms,"lot_wms"=>json_decode($documentProductDocumentUpdate->lot_wms,true)),true));

                        if(!empty($documentProductDocumentUpdate->id_product)){

                            $resultExist[$x] = array(
                                "data" => $documentProductDocumentUpdateJson,
                                "state" => "ok",
                                "msg" => "Document Update Quantity and Lot"
                            );

                        }else{

                            $resultExist[$x] = array(
                                "data" => $documentProductDocumentUpdateJson,
                                "state" => "error",
                                "msg" => "Document No Update Problems in the BD"
                            );
                        }

                    }else{
                        $resultExist[$x] = array(
                            "data" => $arraySend,
                            "state" => "error",
                            "msg" => "Product Not Exist in the Document"
                        );
                    }
                }

            }else{
                $resultExist = array(
                    "data" => $arraySendDocument,
                    "state" => "error",
                    "msg" => "Document Not Exist"
                );
            }
        }


        return $resultExist;

    }

}
