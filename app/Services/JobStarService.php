<?php

namespace App\Services;

use App\Entities\JobDoc;
use App\Entities\JobInteractionDoc;
use App\Entities\JobInteraction;

use App\Repositories\JobStarRepository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use Tymon\JWTAuth\Facades\JWTAuth;

class JobStarService
{
    private $jobStarRepository;
    private $user;

    public function __construct(JobStarRepository $jobStarRepository)
    {
        $this->jobStarRepository = $jobStarRepository;
        $this->user = auth()->user();
    }

    public function addFilesToJobStar($request, $id)
    {
        $jobStar = $this->jobStarRepository->findById($id);
        $files = $request->file('files');
        $error = false;

        if (!$files) {
            return true;
        }

        if (!$jobStar) {
            return [
                'data' => 'No hay registro.',
                'code' => 404
            ];
        }

        if (is_array($files)) {
            foreach ($files as $key => $file) {
                $saved = $this->saveFile($file);

                if ($saved !== false) {
                    JobDoc::create([
                        'jobs_star_id' => $jobStar->id,
                        'path_document' => $saved,
                        'name' => $file->getClientOriginalName(),
                        'type' => $file->getClientOriginalExtension()
                    ]);
                } else {
                    $error = true;
                }

            }
        } else {
            $saved = $this->saveFile($files);

            if ($saved !== false) {
                JobDoc::create([
                    'jobs_star_id' => $jobStar->id,
                    'path_document' => $saved,
                    'name' => $file->getClientOriginalName(),
                    'type' => $file->getClientOriginalExtension()
                ]);
            } else {
                $error = true;
            }
        }

        if ($error) {
            return [
                'data' => 'Error al guardar el archivo.',
                'code' => 500
            ];
        }

        return [
            'data' => 'archivo guardado correctamente.',
            'code' => 200
        ];
    }

    public function deleteFileToJobStar($request, $id) {
        try {
            $jobDoc = JobDoc::find($id);

            Storage::disk('s3')->delete('jobstar/'.$jobDoc->path_document);

            $jobDoc->delete();

            return [
                'data' => $jobDoc,
                'code' => 200
            ];
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return [
                'data' => 'Error al eliminar un archivo.',
                'code' => 200
            ];
        }
    }

    public function saveFile($file, $pathFolder = 'jobstar/')
    {
        if (!$file) {
            return false;
        }

        $pathFolder = 'colsaisa/'.$pathFolder;

        try {
            // Nombre del archivo.
            $fileName = Str::uuid() . "." . $file->getClientOriginalExtension();

            //Guarda el archivo en el disco local temporalmente
            $file->storeAs('public/', $fileName);

            //Obtiene el archivo nuevamente
            $fileNew = Storage::disk('public')->get($fileName);

            //Lo guarda en el servicio S3 de Amazon
            Storage::disk('s3')->put($pathFolder.$fileName, $fileNew);

            //Eliminamos el archivo del disco local
            Storage::disk('public')->delete($fileName);

            return $fileName;
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return false;
        }
    }

    public function addInteraction($request, $id) {
        $jobStar = $this->jobStarRepository->findById($id);
        $user    = JWTAuth::parseToken()->toUser();
        $files   = $request->file('files');

        try {
            DB::beginTransaction();

            // Crear interaccion
            $jobInteraction = $this->jobStarRepository->createInteractionJobStar([
                'jobs_star_id' => $jobStar->id,
                'user_id' => $user->id,
                'description' => $request->description
            ]);

            // Guardar archivos
            $resposne = $this->saveFilesInteraction($files, $jobInteraction->id);

            if ($resposne === false) {
                throw new \Exception("Error al guardar los archivos de la interaccion.");
            }

            DB::commit();

            return [
                'data' => $this->jobStarRepository->findInteractionJobStar($jobInteraction->id),
                'code' => 201
            ];

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return [
                'data' => 'Error al crear la interaccion.',
                'code' => 500
            ];
        }

    }

    public function saveFilesInteraction($files, $jobInteractionId)
    {
        $error = false;

        if (!$files) {
            return true;
        }

        if (is_array($files)) {
            foreach ($files as $key => $file) {
                $saved = $this->saveFile($file, 'jobstar-interactions/');

                if ($saved !== false) {
                    JobInteractionDoc::create([
                        'jobs_interactions_id' => $jobInteractionId,
                        'path_document' => $saved,
                        'name' => $file->getClientOriginalName(),
                        'type' => $file->getClientOriginalExtension()
                    ]);
                } else {
                    $error = true;
                }

            }
        } else {
            $saved = $this->saveFile($files);

            if ($saved !== false) {
                JobInteractionDoc::create([
                    'jobs_interactions_id' => $jobInteractionId,
                    'path_document' => $saved,
                    'name' => $files->getClientOriginalName(),
                    'type' => $files->getClientOriginalExtension()
                ]);
            } else {
                $error = true;
            }
        }

        if ($error) {
            return false;
        }

        return true;
    }

    public function create($request)
    {
        $dataSave = $request->all();
        $dataSave['state'] = true;
        $dataSave['user_creation_id'] = $this->user->id;

        $jobStar = $this->jobStarRepository->create($dataSave);
        $response = $this->addFilesToJobStar($request, $jobStar->id);

        return $jobStar;
    }
}
