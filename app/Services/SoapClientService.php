<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use function GuzzleHttp\json_encode;

/**
* @property \SoapClient soapClient
* @property string wsdl
* @property resource context
*/
abstract class SoapClientService
{

	protected $soapClient;
	protected $wsdl;
	protected $context;
	protected $options;
	protected $arrayHomologs;
	protected $responseData;
	protected $consult;
	protected $parameters = [];
	protected $countItems = 0;
	protected $arrayImport;
	protected $configSend = [];
	protected $xml;

	public function __construct()
	{

		$this->wsdl = "http://186.154.234.242:8080/WSUNOEE/WSUNOEE.asmx?WSDL";

		$soapClientOptions = [
			'stream_context' => $this->generateContext(),
			'cache_wsdl' => 0
		];

		$this->soapClient = new \SoapClient($this->wsdl, $soapClientOptions);

		$this->soapClient->__setLocation("http://186.154.234.242:8080/WSUNOEE/WSUNOEE.asmx");

	}

	/**
	* @return resource
	*/
	protected function generateContext()
	{
		$this->options = [
		'http' => [
			'user_agent' => 'PHPSoapClient'
			]
		];

		return $this->context = stream_context_create($this->options);
	}

	/**
	* @param $xmlString
	* @return array
	*/
	protected function loadXmlStringAsArray($xmlString)
	{
		$xml = json_decode(json_encode($xmlString), true);

		$any = $xml["EjecutarConsultaXMLResult"]["any"];

		$array = (array)@simplexml_load_string($any);
		if (!$array) {
			$array = (array)@json_decode($any, true);
		} else {
			$array = (array)@json_decode(json_encode($array), true);
		}
		return $array;
	}

	/**
	* @param $consult
	* @param array $parameters
	* @return mixed|string
	*/
	protected function makeConsultXML()
	{

		$xml = "<Consulta><NombreConexion>".config("soap.database")."</NombreConexion><IdCia>1</IdCia><IdProveedor>PROPARTES</IdProveedor><IdConsulta>:consult</IdConsulta><Usuario>".config("soap.user")."</Usuario><Clave>".config("soap.password")."</Clave><Parametros>:parameters</Parametros></Consulta>";

		$parametersXml = "";

		foreach ($this->parameters as $parameter => $value) {
			$parametersXml .= "<$parameter>$value</$parameter>";
		}

		$xml = str_replace(":consult", $this->consult, $xml);
		$xml = str_replace(":parameters", $parametersXml, $xml);

		return $xml;

	}

	/**
	* @param $consult
	* @param array $parameters
	*/
	protected function executeConsult()
	{
		$parameterXml = $this->makeConsultXML();

		$responseXml = $this->soapClient->EjecutarConsultaXML(["pvstrxmlParametros" => $parameterXml]);

		$responseArray = $this->loadXmlStringAsArray($responseXml);

		if (isset($responseArray["NewDataSet"])) {
			$this->responseData = $responseArray["NewDataSet"]["Resultado"];
		} else {
			$this->responseData = [];
		}
	}

	protected function getDataB2B()
	{

		$this->executeConsult();

		$response = [];

		if (!(array_keys($this->responseData) !== range(0, count($this->responseData) - 1))) {
			foreach ($this->responseData as $keyResponse => $valueResponse) {
				$singleObject = [];
				foreach ($valueResponse as $keyObject => $valueObject) {
					foreach ($this->arrayHomologs as $keyHomologs => $valueHomologs) {
						if ($keyObject == $valueHomologs) {
							if (!is_array($valueObject)) {
								$singleObject[$keyHomologs] = rtrim($valueObject);
							}else{
								$singleObject[$keyHomologs] = '';
							}
							break;
						}
					}
				}
				$response[] = $singleObject;
			}
		} else {
			$singleObject = [];
			foreach ($this->responseData as $keyResponse => $valueResponse) {
				foreach ($this->arrayHomologs as $keyHomologs => $valueHomologs) {
					if ($keyResponse == $valueHomologs) {
						if (!is_array($valueResponse)) {
							$singleObject[$keyHomologs] = rtrim($valueResponse);
						}else{
							$singleObject[$keyHomologs] = '';
						}
						break;
					}
				}
			}

			$response[] = $singleObject;
		}

		return $response;

	}

	/**
	*
	*/
	public function sendData(){

		$response_xml = $this->soapClient->importarXML(["pvstrDatos" => $this->xml, "printTipoError" => 0]);

		if($response_xml->printTipoError != 0){
			throw new \Exception('Error when sending the order to Siesa');
		}

	}

	/**
	* @param $consult
	* @param $parameters
	* @return mixed
	*/
	public abstract function syncData();

}
