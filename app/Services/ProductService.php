<?php

namespace App\Services;

//Entities
use App\Entities\BranchOffice;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\Brand;
use App\Entities\Categorie;
use App\Entities\Contact;
use App\Entities\ContactWarehouse;
use App\Entities\DataSheet;
use App\Entities\DataSheetProduct;
use App\Entities\Discount;
use App\Entities\Inventory;
use App\Entities\Line;
use App\Entities\Parameter;
use App\Entities\Price;
use App\Entities\Product;
use App\Entities\ProductPromotion;
use App\Entities\Promotion;
use App\Entities\Subbrand;
use App\Entities\SubCategorie;
use App\Entities\Subline;
use App\Entities\Subscriber;
use App\Entities\Template;
use App\Repositories\DataSheetRepository;

//Repositories
use App\Repositories\InventoryRepository;
use App\Repositories\LineRepository;
use App\Repositories\PriceRepository;
use App\Repositories\ProductRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\SublineRepository;
use Carbon\Carbon;

// Criteria
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;

// Otros
use Prettus\Repository\Criteria\RequestCriteria;
use SoapClient;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Este controlador permite definirs
 * los métodos necesarios para aplicar
 * la logica de negocio relacionada a
 * la tabla de documentos.
 *
 * @author Kevin Galindo
 * @package App\Services
 */
class ProductService
{
    private $productRepository;

    protected $lineRepositury;
    protected $promotionRepository;
    protected $sublineRepository;
    protected $priceRepository;
    protected $inventoryRepository;

    /**
     * Constructor.
     */
    public function __construct(LineRepository $lineRepo,
                                PromotionRepository $promotionRepository,
                                SublineRepository $sublineRepository,
                                PriceRepository $priceRepository,
                                InventoryRepository $inventoryRepository,
                                DataSheetRepository $dataSheetRepository,
                                ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
        $this->lineRepositury = $lineRepo;
        $this->promotionRepository = $promotionRepository;
        $this->sublineRepository = $sublineRepository;
        $this->priceRepository = $priceRepository;
        $this->inventoryRepository = $inventoryRepository;
        $this->dataSheetRepository = $dataSheetRepository;
    }

    /**
     * Esta funcion se encarga de generar y descargar un archivo en csv
     * con los campos que se requiere para crear el producto segun su linea.
     *
     * @return \Illuminate\Http\Response
     * @author Kevin Galindo
     */
    public function generateCsvForLine($line_id)
    {
        //Declaramos las cabeceras de la respuesta para descargar el archivo.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=galleries.csv',
            'Expires'             => '0',
            'Pragma'              => 'public',
        ];

        //Obtenemos los campos por la linea.
        $fieldsLine = $this->columnProductForLine($line_id);

        //Obtenemos los "Labels" para generar el archivo
        $fieldsCsvRow1 = [];
        $fieldsCsvRow2 = [];

        //Agregamos valores predeterminados
        $fieldsCsvRow1[] = 'CODIGO';
        $fieldsCsvRow1[] = 'DESCRIPCION';
        $fieldsCsvRow1[] = 'LINEA';

        $fieldsCsvRow2[] = 'CODIGO';
        $fieldsCsvRow2[] = 'DESCRIPCION';
        $fieldsCsvRow2[] = 'LINEA';

        foreach ($fieldsLine as $key => $field) {
            $fieldsCsvRow1[] = $field->internal_name;
            $fieldsCsvRow2[] = $field->visible_name;
        }

        //Generamos el archivo
        $column = [$fieldsCsvRow1, $fieldsCsvRow2];
        $callback = function () use ($column) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ($column as $row) {
                fputcsv($file, $row, ';');
            }

            //Cerramos la escritura del archivo.
            fclose($file);
        };

        //Retornamos el archivo.
        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200,
        ];
    }

    /**
     * Esta funcion se encarga de importar la informacion
     * a las respectivas tablas segun el archivos cargado.
     *
     * @return \Illuminate\Http\Response
     * @author Kevin Galindo
     */
    public function importFileProduct($request, $line_id)
    {
        //Traemos la informacion de la linea
        $line = Line::find($line_id);

        // Validamos que la informacion este correcta
        $erros = $this->validateImportProduct($request, $line);

        //Validamos si hay errores, en caso de que si hayan se retornan y se termina el proceso.
        if ($erros !== false) {
            return [
                'message' => $erros['message'],
                'code'    => $erros['code'],
            ];
        }

        //Recibimos la información
        $file = $request->file('archivo-importar');

        //Pasamos la informacion de csv a un array.
        $data = $this->csvToArray($file->getRealPath());

        //Validamos que la informacion devuelta no sea un valor boolean(false)
        if (!$data) {
            return [
                'message' => 'Error al convertir el archivo csv, recuerde que el archivo debe estar delimitado por ";"',
                'code'    => '500',
            ];
        }

        //Eliminamos el encabezado del archivo
        unset($data[0]);

        //Validar Informacion
        $resError = $this->validateColumnImportProduct($data, $line);
        if ($resError !== true) {
            return [
                'message' => $resError,
                'code'    => 400,
            ];
        }

        /*******************************
         * Importar datos a las tablas *
         *******************************/

        //Esta variable guarda la cantidad de datos que se crearon en el proceso
        $statistics = [];

        //IMPORTAR MARCAS
        if (isset($data[1]['MARCA'])) {
            // Esta variable guarda los registro que ya fueron procesados.
            $duplicate = [];

            // Este es un contador que me guqarda las veces que se a creado un registro para MARCA
            $registerCount = 0;

            //Recorremos el arreglo
            foreach ($data as $key => $value) {

                $brand = strtoupper($value['MARCA']);

                /*
                Esta condicional verifica que la marca que esta siendo procesada no vuelva a ser
                procesada dos veces, asi evitando consultar a la base de datos.
                En caso de que se este repitiendo solo se saltara la iteracion.
                 */
                if (in_array($brand, $duplicate)) {
                    continue;
                }

                /*
                Validamos si la marca ya existe.

                NO
                Se crea un nuevo registro en la base de datos.

                SI
                No se realiza accion.
                 */
                if (Brand::where('description', trim($brand))->count() == 0) {
                    // Traemos el ultimo registro de la tabla "Brand"
                    $brand_code = Brand::max('brand_code');

                    // Validamos si existe un registro y de ser asi se suma de 10 tomando el dosigo anterior y si no se deja solo 10
                    $brand_code = !empty($brand_code) && is_numeric($brand_code) ? $brand_code + 10 : 10;

                    // Creamos la MARCA
                    Brand::create([
                        'brand_code'  => $brand_code,
                        'description' => trim($brand),
                        'thumbnails'  => 'TEST',
                    ]);

                    // Guardamos la estadistica
                    $registerCount++;
                }

                //Se guarda la marca que ya fue procesada.
                $duplicate[] = $brand;
            }

            //Guardamos el conteo en las estadisticas
            $statistics['MARCA'] = $registerCount;
        }

        //IMPORTAR SUBMARCA
        if (isset($data[1]['SUBMARCA'])) {

            // Esta variable guarda los registro que ya fueron procesados.
            $duplicate = [];

            // Este es un contador que me guqarda las veces que se a creado un registro para SUBMARCA
            $registerCount = 0;

            // Recorremos el arreglo
            foreach ($data as $key => $value) {

                // Declaramos las variables.
                $subbrand = strtoupper($value['SUBMARCA']);
                $brand = strtoupper($value['MARCA']);
                $repeatedData = $subbrand . $brand;

                /*
                Esta condicional verifica que la marca que esta siendo procesada no vuelva a ser
                procesada dos veces, asi evitando consultar a la base de datos.
                En caso de que se este repitiendo solo se saltara la iteracion.
                 */
                if (in_array($repeatedData, $duplicate)) {
                    continue;
                }

                /*
                Validamos si la marca ya existe y en caso de
                que ya exusta se crea una nueva marca.
                 */
                if (Subbrand::where('description', trim($subbrand))->count() == 0) {

                    // traemos el ultimo registro de la tabla para realizar el consecutivo.
                    $subbrand_code = Subbrand::max('subbrand_code');

                    // validamos si existe algun registro y en caso de que exista se toma el "subbrand_code" y se le suma 10 en caso de que no solo se deja 10
                    $subbrand_code = !empty($subbrand_code) && is_numeric($subbrand_code) ? $subbrand_code + 10 : 10;

                    // Traemos el id de la MARCA
                    $brand_id = Brand::where('description', $brand)->first()->id;

                    // Creamos la SUBMARCA
                    Subbrand::create([
                        'brand_id'      => $brand_id,
                        'subbrand_code' => $subbrand_code,
                        'description'   => trim($subbrand),
                    ]);

                    // Estadisticas
                    $registerCount++;
                }

                //Se guarda la marca que ya fue procesada.
                $duplicate[] = $subbrand . $brand;
            }

            //Guardamos el conteo en las estadisticas
            $statistics['SUBMARCA'] = $registerCount;
        }

        //IMPORTAR SUBLINEA
        if (isset($data[1]['SUBLINEA'])) {
            #Esta variable guarda los registro que ya fueron procesados.
            $duplicate = [];
            #Este es un contador que me guqarda las veces que se a creado un registro para SUBMARCA
            $createCount = 0;

            //Recorremos el arreglo
            foreach ($data as $key => $value) {

                // Declaramos las variables.
                $subline = strtoupper($value['SUBLINEA']);
                $repeatedData = $subline;

                // valida que el valor no sea nulo
                if (empty($subline)) {
                    continue;
                }

                /*
                Esta condicional verifica que la marca que esta siendo procesada no vuelva a ser
                procesada dos veces, asi evitando consultar a la base de datos.
                En caso de que se este repitiendo solo se saltara la iteracion.
                 */
                if (in_array($repeatedData, $duplicate)) {
                    continue;
                }

                /*
                Validamos si la marca ya existe y en caso de
                que ya exusta se crea una nueva marca.
                 */

                if (Subline::where('subline_description', trim($subline))->count() == 0) {
                    // traemos el ultimo registro de la tabla para realizar el consecutivo.
                    $subline_code = Subline::max('subline_code');
                    // validamos si existe algun registro y en caso de que exista se toma el "subline_code" y se le suma 10 en caso de que no solo se deja 10
                    $subline_code = !empty($subline_code) && is_numeric($subline_code) ? $subline_code + 10 : 10;

                    // Creamos la SUBMARCA
                    Subline::create([
                        'line_id'             => $line_id,
                        'subline_code'        => $subline_code,
                        'subline_description' => trim($subline),
                    ]);

                    #Estadisticas
                    $createCount++;
                }

                //Se guarda la marca que ya fue procesada.
                $duplicate[] = $subline;
            }

            //Guardamos el conteo en las estadisticas
            $statistics['SUBCATEGORIA'] = $createCount;
        }

        //IMPORTAR CATEGORIA
        if (isset($data[1]['CATEGORIA'])) {
            // Esta variable guarda los registro que ya fueron procesados.
            $duplicate = [];

            #Este es un contador que me guqarda las veces que se a creado un registro para CATEGORIA
            $createCount = 0;

            //Recorremos el arreglo
            foreach ($data as $key => $value) {

                $categorie = strtoupper($value['CATEGORIA']);

                // valida que el valor no sea nulo
                if (empty($categorie)) {
                    continue;
                }

                /*
                Esta condicional verifica que la marca que esta siendo procesada no vuelva a ser
                procesada dos veces, asi evitando consultar a la base de datos.
                En caso de que se este repitiendo solo se saltara la iteracion.
                 */
                //if (in_array($categorie, $duplicate)) continue;

                /*
                Validamos si la marca ya existe y en caso de
                que ya exusta se crea una nueva marca.
                 */
                if (Categorie::where('description', trim($categorie))->count() == 0) {

                    // Traemos el ultimo registro de la tabla "Categorie"
                    $categorie_code = Categorie::max('categorie_code');

                    #Validamos si existe un registro y de ser asi se suma de 10 tomando el dosigo anterior y si no se deja solo 10
                    $categorie_code = !empty($categorie_code) && is_numeric($categorie_code) ? $categorie_code + 10 : 10;

                    #Si existe alguna sublinea traemos su id
                    $subline_id = isset($value['SUBLINEA']) ? Subline::where('subline_description', strtoupper($value['SUBLINEA']))->first() : null;
                    $subline_id = !empty($subline_id) ? $subline_id->id : null;
                    #Creamos la MARCA
                    Categorie::create([
                        'categorie_code' => trim($categorie_code),
                        'description'    => trim($categorie),
                        'line_id'        => $line_id,
                        'subline_id'     => $subline_id,
                    ]);

                    #Guardamos la estadistica
                    $createCount++;
                }

                //Se guarda la marca que ya fue procesada.
                $duplicate[] = $categorie;
            }

            //Guardamos el conteo en las estadisticas
            $statistics['CATEGORIA'] = $createCount;
        }

        //IMPORTAR SUBCATEGORIA
        if (isset($data[1]['SUBCATEGORIA'])) {
            #Esta variable guarda los registro que ya fueron procesados.
            $duplicate = [];
            #Este es un contador que me guqarda las veces que se a creado un registro para SUBMARCA
            $createCount = 0;

            //Recorremos el arreglo
            foreach ($data as $key => $value) {

                // Declaramos las variables.
                $subcategorie = strtoupper($value['SUBCATEGORIA']);
                $categorie = strtoupper($value['CATEGORIA']);
                $repeatedData = $subcategorie . $categorie;

                /*
                Esta condicional verifica que la marca que esta siendo procesada no vuelva a ser
                procesada dos veces, asi evitando consultar a la base de datos.
                En caso de que se este repitiendo solo se saltara la iteracion.
                 */
                if (in_array($repeatedData, $duplicate)) {
                    continue;
                }

                /*
                Validamos si la marca ya existe y en caso de
                que ya exusta se crea una nueva marca.
                 */
                if (SubCategorie::where('description', trim($subcategorie))->count() == 0) {
                    // traemos el ultimo registro de la tabla para realizar el consecutivo.
                    $subcategorie_code = SubCategorie::max('subcategorie_code');

                    // validamos si existe algun registro y en caso de que exista se toma el "subcategorie_code" y se le suma 10 en caso de que no solo se deja 10
                    $subcategorie_code = !empty($subcategorie_code) && is_numeric($subcategorie_code) ? $subcategorie_code + 10 : 10;

                    // Traemos el id de la MARCA
                    $categorie_id = Categorie::where('description', $categorie)->first()->id;

                    // Creamos la SUBMARCA
                    SubCategorie::create([
                        'categorie_id'      => $categorie_id,
                        'subcategorie_code' => $subcategorie_code,
                        'description'       => trim($subcategorie),
                    ]);

                    #Estadisticas
                    $createCount++;
                }

                //Se guarda la marca que ya fue procesada.
                $duplicate[] = $subcategorie . $categorie;
            }

            //Guardamos el conteo en las estadisticas
            $statistics['SUBCATEGORIA'] = $createCount;
        }

        /******************************************************
         * Guardamos la informacion en la tabla de parametros *
         ******************************************************/

        /*
        Recorremos la ficha de datos de la linea para validar si hacen referencia
        a una tabla de parametro y de ser asi se rellenan la informacion en la tabla de parametros.
         */
        foreach ($line->dataSheets as $key => $value) {

            // Validamos si la ficha de dato hace referencia a una tabla de parametro.
            if ($value->paramtable_id != '') {

                // Recorremos los datos y los rellenamos en la tabla correspondiente.
                foreach ($data as $key => $val) {

                    // guardamos el valor del parametro.
                    $name_parameter = strtoupper($val[$value->internal_name]);

                    if (empty($name_parameter)) {
                        continue;
                    }

                    // Validamos si el registro ya existe.
                    if (Parameter::where('name_parameter', $name_parameter)->count() == 0) {

                        // Traemos el ultimo registro de la tabla "Brand"
                        $code = Parameter::max('code_parameter');

                        // Validamos si existe un registro y de ser asi se suma de 10 tomando el dosigo anterior y si no se deja solo 10
                        $code = !empty($code) && is_numeric($code) ? $code + 10 : 10;

                        // Creamos el parametro
                        Parameter::create([
                            'paramtable_id'       => $value->paramtable_id,
                            'code_parameter'      => $code,
                            'name_parameter'      => $name_parameter,
                            'alphanum_data_first' => 'TEST',
                        ]);
                    }
                }
            }
        }

        /*****************************************
         * Guardamos la informacion en productos *
         *****************************************/

        // Esta variable almacena el listado de los id de las fichas de datos.
        $dataSheetIdArray = [];

        // Se realiza la busqueda de los id de las fechas de datos.
        foreach ($data[1] as $keyDataSheet => $value) {
            $dataSheetId = DataSheet::where('internal_name', $keyDataSheet)->first();
            if ($dataSheetId) {
                $dataSheetIdArray[$keyDataSheet] = $dataSheetId->id;
            }
        }

        // Removemos los campos que no son adicionales para el producto.
        //unset($dataSheetIdArray['LINEA']);
        unset($dataSheetIdArray['MARCA']);
        unset($dataSheetIdArray['SUBMARCA']);
        unset($dataSheetIdArray['CATEGORIA']);
        unset($dataSheetIdArray['SUBCATEGORIA']);
        unset($dataSheetIdArray['DESCRIPCION']);
        unset($dataSheetIdArray['PRECIO']);
        unset($dataSheetIdArray['SALDO']);

        // Recorremos el arreglo y guardamos los productos.
        foreach ($data as $key => $val) {

            // LINEA
            $line_id = isset($val['LINEA']) && !empty($val['LINEA']) ? Line::where('line_description', trim($val['LINEA']))->first()->id : $line_id;

            //SUBLINEA
            $subline_id = isset($val['SUBLINEA']) && !empty($val['SUBLINEA']) ? Subline::where('subline_description', 'ilike', '%' . trim($val['SUBLINEA']) . '%')->first()->id : null;

            //MARCA
            $brand_id = isset($val['MARCA']) && !empty($val['MARCA']) ? Brand::where('description', 'ilike', '%' . trim($val['MARCA']) . '%')->first()->id : null;
            $subbrand_id = isset($val['SUBMARCA']) && !empty($val['SUBMARCA']) ? Subbrand::where('description', 'ilike', '%' . trim($val['SUBMARCA']) . '%')->first()->id : null;

            //CATEGORIA
            $category_id = isset($val['CATEGORIA']) && !empty($val['CATEGORIA']) ? Categorie::where('description', 'ilike', '%' . trim($val['CATEGORIA']) . '%')->first()->id : null;

            $subcategory_id = isset($val['SUBCATEGORIA']) && !empty($val['SUBCATEGORIA']) ? SubCategorie::where('description', 'ilike', '%' . trim($val['SUBCATEGORIA']) . '%')->first()->id : null;

            //Contiene los valores de los data sheets
            $dataSheetProduct = [];

            // Alistamos la informacion para guardarla en la base de datos referente a los data sheets
            foreach ($val as $keyDataSheet => $valueDataSheet) {

                //Valida si el campo existe en el arreglo
                if (
                    isset($dataSheetIdArray[$keyDataSheet]) &&
                    !empty($dataSheetIdArray[$keyDataSheet]) &&
                    !empty($valueDataSheet)
                ) {

                    $dataSheetProduct[] = [
                        // 'column' => $keyDataSheet,
                        'value'         => $valueDataSheet,
                        'data_sheet_id' => $dataSheetIdArray[$keyDataSheet],
                    ];
                }
            }

            // Creamos el producto
            $product = Product::updateOrCreate(
                [
                    'code' => trim($val['CODIGO']),
                ],
                [
                    'code'           => trim($val['CODIGO']),
                    'description'    => trim($val['DESCRIPCION']),
                    'alternate_code' => trim($val['CODIGO-ALTERNO']),
                    'line_id'        => $line_id,
                    'subline_id'     => $subline_id,
                    'brand_id'       => $brand_id,
                    'subbrand_id'    => $subbrand_id,
                    'category_id'    => $category_id,
                    'subcategory_id' => $subcategory_id,
                    'user_id'        => $request->user_id,
                    'weight'         => isset($val['PESO']) ? trim($val['PESO']) : 0,
                ]
            );

            // Crea los datos adicionales sobre el producto.
            DataSheetProduct::updateOrCreate(
                [
                    'product_id' => $product->id,
                ],
                [
                    'product_id' => $product->id,
                    'value'      => json_decode(json_encode($dataSheetProduct)),
                ]
            );

            $products[] = $product;
        }

        // Retornamos la informacion
        return [
            'message' => [
                'productos-creados' => $products,
                'estadisticas'      => $statistics,
                'text'              => 'Se ha importado el archivo correctamente. (' . count($products) . ' productos creados)',
            ],
            'code'    => 200,
        ];
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    public function validateImportProduct($request, $line)
    {
        //Validamos que la linea exista
        if (!$line) {
            return [
                'message' => 'No existe registro sobre la linea',
                'code'    => 400,
            ];
        }

        // Validamos que el id del usuario venga en la request
        if (!$request->user_id) {
            return [
                'message' => 'Falta el id del usuario',
                'code'    => 404,
            ];
        }

        //Recibimos la información
        $file = $request->file('archivo-importar');

        //Validamos que el archivo exista.
        if (!$file) {
            return [
                'message' => 'No se encuentra el archivo "archivo-importar" para realiza la importacion.',
                'code'    => '404',
            ];
        }

        //Validamos que la extension sea
        // if ($file->extension() != 'csv' && $file->extension() != 'txt') {
        //     return [
        //         'message' => 'La extension del archivo no es valida. solo se permite ("csv", "txt")',
        //         'code' => '400'
        //     ];
        // }

        return false;
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    public function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 1) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    public function validateColumnImportProduct($data, $line)
    {
        //Esta Variable guarda los errores.
        $errors = [];

        /*
        Validacion de campos requeridos
         */

        //Esta variable guarda los campos que son requeridos.
        $columnRequired = [];
        /*
        Se realiza una consulta para traer
        los dataSheets que son obligatorios y
        fuardarlos en la variable "$columnRequired"
         */
        foreach ($line->dataSheets as $key => $value) {
            if ($value->pivot->required) {
                $columnRequired[] = $value->internal_name;
            }
        }
        //Campos que no estan en los dataSheets
        $columnRequired[] = 'DESCRIPCION';

        //Se valida si el campo en los datos esta lleno o no.
        foreach ($columnRequired as $key => $value) {
            $columnVoid = 0;
            foreach ($data as $dataKey => $dataValue) {
                if (
                    !isset($dataValue[$value]) ||
                    $dataValue[$value] == '' ||
                    $dataValue[$value] == null
                ) {
                    $columnVoid++;
                }
            }

            if ($columnVoid > 0) {
                $errors[] = 'El valor del campo "' . $value . '" es obligatorio';
            }
        }

        /*
        Validar que el tipo de dato del campo
        coincida con el de la base de dato.
         */
        $columnTypeValidation = [];

        foreach ($line->dataSheets as $key => $value) {
            //$value->type_value_name->name_parameter
            $columnTypeValidation[] = [
                'name' => $value->internal_name,
                'type' => $value->type_value_name->name_parameter,
            ];
        }

        $columnTypeValidation[] = [
            'name' => 'DESCRIPCION',
            'type' => 'TEXTO',
        ];

        foreach ($columnTypeValidation as $key => $value) {
            $columnError = 0;

            foreach ($data as $dataKey => $dataValue) {

                switch ($value['type']) {
                    case 'NUMERO':
                        if (
                            !empty($dataValue[$value['name']]) &&
                            !is_numeric($dataValue[$value['name']])
                        ) {
                            $columnError++;
                        }
                        break;
                }
            }

            if ($columnError > 0) {
                $errors[] = 'El valor del campo "' . $value['name'] . '" debe ser de tipo "' . $value['type'] . '"';
            }
        }

        if (empty($errors)) {
            return true;
        } else {
            return $errors;
        }
    }

    /**
     * Esta funcion se encarga se validar los campos que se
     * requiere para crear un producto por linea.
     *
     * @return array
     * @author Kevin Galindo
     */
    public function columnProductForLine($line_id = null)
    {
        //Buscamos el rewgistro de la linea.
        $line = $this->lineRepositury->findForId($line_id);

        //Retornamos los campos
        return $line->dataSheets;
    }

    /**
     * Validar si los productos tienen
     * promociones
     *
     * @param array $products
     * @param bool $getOnlyPromotions
     * @param $request
     * @return array
     * @author Kevin Galindo
     */
    public function validateProductPromotion(array $products, $getOnlyPromotions = false, $request)
    {
        $productsPromotions = [];

        // Recorremos el listado de productos
        foreach ($products['data'] as $keyProduct => $product) {

            // Validamos si la marca, submarca, linea o sublinea pertenece a alguna promocion.
            $promotions = Promotion::
            with([
                'productsPromotions' => function ($query) {
                    $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                    $query->with([
                        'product' => function ($query) {
                            $query->select('id', 'code', 'description', 'type', 'line_id', 'state');
                            $query->with([
                                'price',
                                'prices',
                                'line' => function ($query) {
                                    $query->with('taxes');
                                },
                            ])
                                ->whereHas('product', function ($query) {
                                    $query->where(function ($query) {
                                        $query->orWhere('state', true);
                                        $query->orWhere('state', null);
                                    });
                                });
                        },
                    ]);
                },
                'line'               => function ($query) {
                    $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                },
                'subline',
                'brand'              => function ($query) {
                    $query->select('id', 'brand_code', 'description', 'image');
                },
                'subbrand'           => function ($query) {
                    $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                },
                'branchoffice'       => function ($query) {
                    $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                },
                'specificProduct',
            ])
                ->where('line_id', $product['line_id'])
                ->where('subline_id', $product['subline_id'])
                ->where('brand_id', $product['brand_id'])
                ->where('subbrand_id', $product['subbrand_id'])
                ->where('branchoffice_id', $request->branchoffice_id)
                ->get();

            if ($promotions->count() == 0) {
                $promotions = Promotion::
                with([
                    'productsPromotions' => function ($query) {
                        $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                        $query->with([
                            'product' => function ($query) {
                                $query->select('id', 'code', 'description', 'type', 'line_id');
                                $query->with([
                                    'price',
                                    'prices',
                                    'line' => function ($query) {
                                        $query->with('taxes');
                                    },
                                ]);
                            },
                        ])
                            ->whereHas('product', function ($query) {
                                $query->where(function ($query) {
                                    $query->orWhere('state', true);
                                    $query->orWhere('state', null);
                                });
                            });
                    },
                    'line'               => function ($query) {
                        $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                    },
                    'subline',
                    'brand'              => function ($query) {
                        $query->select('id', 'brand_code', 'description', 'image');
                    },
                    'subbrand'           => function ($query) {
                        $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                    },
                    'branchoffice'       => function ($query) {
                        $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                    },
                    'specificProduct',
                ])
                    ->where('line_id', $product['line_id'])
                    ->where('brand_id', $product['brand_id'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->get();
            }

            // En caso de pertenecer a una promocion se agrega al listado de promociones.
            foreach ($promotions as $keyPromotion => $promotion) {

                // Validamos el rango de fechas
                $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $promotion->start_date . ' 00:00:00');
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $promotion->end_date . ' 23:59:59');

                if (!Carbon::now()->between($startDate, $endDate)) {
                    continue;
                }

                //Validamos la disponibilidad del producto
                if ($promotion->only_available) {
                    if (!isset($product['inventory'][0]) || $product['inventory'][0]['available'] <= 0) {
                        continue;
                    }
                }

                $products['data'][$keyProduct]['hasPromotion'] = true;
                $products['data'][$keyProduct]['product_promotions'][] = $promotion->toArray();
                $productsPromotions[] = $products['data'][$keyProduct];
            }

            // Validar promocion solo por marca
            $promotions = Promotion::
            with([
                'productsPromotions' => function ($query) {
                    $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                    $query->with([
                        'product' => function ($query) {
                            $query->select('id', 'code', 'description', 'type', 'line_id');
                            $query->with([
                                'price',
                                'prices',
                                'line' => function ($query) {
                                    $query->with('taxes');
                                },
                            ]);
                        },
                    ])
                        ->whereHas('product', function ($query) {
                            $query->where(function ($query) {
                                $query->orWhere('state', true);
                                $query->orWhere('state', null);
                            });
                        });
                },
                'line'               => function ($query) {
                    $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                },
                'subline',
                'brand'              => function ($query) {
                    $query->select('id', 'brand_code', 'description', 'image');
                },
                'subbrand'           => function ($query) {
                    $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                },
                'branchoffice'       => function ($query) {
                    $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                },
                'specificProduct',
            ])
                ->whereNull('line_id')
                ->whereNull('subline_id')
                ->whereNull('subbrand_id')
                ->where('brand_id', $product['brand_id'])
                ->where('branchoffice_id', $request->branchoffice_id)
                ->get();

            // En caso de pertenecer a una promocion se agrega al listado de promociones.
            foreach ($promotions as $keyPromotion => $promotion) {

                // Validamos el rango de fechas
                $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $promotion->start_date . ' 00:00:00');
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $promotion->end_date . ' 23:59:59');

                if (!Carbon::now()->between($startDate, $endDate)) {
                    continue;
                }

                //Validamos la disponibilidad del producto
                if ($promotion->only_available) {
                    if (!isset($product['inventory'][0]) || $product['inventory'][0]['available'] <= 0) {
                        continue;
                    }
                }

                $products['data'][$keyProduct]['hasPromotion'] = true;
                $products['data'][$keyProduct]['product_promotions'][] = $promotion->toArray();
                $productsPromotions[] = $products['data'][$keyProduct];
            }

            // Validar promocion mix
            $productPromotions = ProductPromotion::with([
                'promotion' => function ($query) {
                    $query->with([
                        'productsPromotions' => function ($query) {
                            $query->select('id', 'promotion_id', 'product_id', 'minimum_amount', 'discount', 'pay_soon', 'cash_payment', 'discount_value');
                            $query->with([
                                'product' => function ($query) {
                                    $query->select('id', 'code', 'description', 'type', 'line_id');
                                    $query->with([
                                        'price',
                                        'line' => function ($query) {
                                            $query->with('taxes');
                                        },
                                    ]);
                                },
                            ])
                                ->whereHas('product', function ($query) {
                                    $query->where(function ($query) {
                                        $query->orWhere('state', true);
                                        $query->orWhere('state', null);
                                    });
                                });
                        },
                        'line'               => function ($query) {
                            $query->select('id', 'subscriber_id', 'line_code', 'line_description', 'short_description');
                        },
                        'subline',
                        'brand'              => function ($query) {
                            $query->select('id', 'brand_code', 'description', 'image');
                        },
                        'subbrand'           => function ($query) {
                            $query->select('id', 'brand_id', 'subbrand_code', 'description', 'image');
                        },
                        'branchoffice'       => function ($query) {
                            $query->select('id', 'subscriber_id', 'name', 'address', 'main', 'code');
                        },
                        'specificProduct',
                    ]);
                },
            ])
                ->where('product_id', $product['id'])
                ->whereHas('promotion', function ($query) use ($request) {
                    $query->where('branchoffice_id', $request->branchoffice_id);
                })
                ->get();

            foreach ($productPromotions as $keyProductPromotion => $productPromotion) {
                if ($productPromotion->Promotion->promotion_type_id == 3) {

                    // Validamos el rango de fechas
                    $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $productPromotion->promotion->start_date . ' 00:00:00');
                    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $productPromotion->promotion->end_date . ' 23:59:59');

                    if (!Carbon::now()->between($startDate, $endDate)) {
                        continue;
                    }

                    //Validamos la disponibilidad del producto
                    if ($productPromotion->promotion->only_available) {
                        if (!isset($product['inventory'][0]) || $product['inventory'][0]['available'] <= 0) {
                            continue;
                        }
                    }

                    $promotion = $this->promotionRepository->findForId($productPromotion->promotion_id, $request);
                    if ($promotion) {

                        $promotion = $promotion->toArray();

                        foreach ($promotion['products_promotions'] as $keyPr => $productPromotion) {

                            // Validamos el descuento positivo o negativo de la sucursal
                            if ($request->warehouse_id) {
                                // Traemos la sucursal
                                $warehouse = ContactWarehouse::find($request->warehouse_id);

                                // Validamos si hay algun valor de descuento.
                                if (!empty($warehouse->discount_percentage)) {
                                    $discount_percentage = (float)$warehouse->discount_percentage;

                                    // Validamos si el porcentaje es positivo o negativo
                                    if ($discount_percentage > 0) {
                                        $discount_value = $promotion['products_promotions'][$keyPr]['discount_value'];
                                        $discount_percentage_value = round(($discount_value * $discount_percentage) / 100);
                                        $promotion['products_promotions'][$keyPr]['discount_value'] = $discount_value - $discount_percentage_value;
                                    } else {
                                        $discount_value = $promotion['products_promotions'][$keyPr]['discount_value'];
                                        $discount_percentage_value = round(($discount_value * abs($discount_percentage)) / 100);
                                        $promotion['products_promotions'][$keyPr]['discount_value'] = $discount_value + $discount_percentage_value;
                                    }
                                }
                            }
                            $product = $this->productRepository->findForId($productPromotion['product']['id'], $request);
                            $promotion['products_promotions'][$keyPr]['product'] = $product->toArray();
                            $promotion['products_promotions'][$keyPr]['quantity'] = 0;
                        }
                    }

                    $products['data'][$keyProduct]['hasPromotion'] = true;
                    $products['data'][$keyProduct]['product_promotions'][] = $promotion;
                    $productsPromotions[] = $products['data'][$keyProduct];
                } else {
                    $products['data'][$keyProduct]['product_promotions_unitary'][] = $productPromotion->promotion->toArray();
                }
            }
        }

        // Validamos que retornar
        if ($getOnlyPromotions) {
            return $productsPromotions;
        }

        // Retornamos la informacion
        return $products;
    }

    /**
     * Traer los productos segun los parametros
     *
     * @author Kevin Galindo
     */
    public function getProducts($request)
    {
        $this->productRepository->pushCriteria(new RequestCriteria($request));
        $this->productRepository->pushCriteria(new LimitOffsetCriteria($request));

        // Variables
        $products = $this->productRepository->findAll($request);
        $products = $this->setPriceProducts($products->toArray(), $request);
        $products = $this->getPriceDiscountByCategoryContact($products, $request->contact_id);
        $products = $this->getPriceDiscountByContactDiscountPercentage($products, $request->warehouse);

        // Validamos si debemos traer los que no tengabn precios
        if ($request->price_control) {
            foreach ($products['data'] as $key => $product) {
                if ($product['price']['price'] <= 0) {
                    unset($products['data'][$key]);
                }
            }
        }

        return $products;
    }

    /**
     * Traer los productos segun los parametros
     *
     * @author Kevin Galindo
     */
    public function getProduct($product_id, $request)
    {
        $validatePromotion = isset($request->validatePromotion) ? $request->validatePromotion : true;
        $product = $this->productRepository->findForId($product_id, $request, true);
        $product = $product->toArray();

        $product = $this->setPriceProducts([$product], $request);

        if ($request->contact_id) {
            $product = $this->getPriceDiscountByCategoryContact($product, $request->contact_id);
        }

        if ($request->warehouse_id) {
            $product = $this->getPriceDiscountByContactDiscountPercentage($product, $request->warehouse_id);
        }

        if ($validatePromotion) {
            $product = $this->validateProductPromotion([
                'data' => isset($product[0]) ? [$product[0]] : [$product],
            ], false, $request);
        }

        if (isset($product['data'])) {
            return $product['data'][0];
        } else {
            if (isset($product[0])) {
                return $product[0];
            } else {
                return $product;
            }
        }
    }

    public function getProductSimple($id, $request)
    {
        $product = $this->productRepository->findForIdSimple($id, $request);
        return $product;
    }

    /**
     * Este metodo se encarga de
     * validar si el cliente cuenta
     * con algun descuento
     *
     * @author Kevin Galindo
     */
    public function getPriceDiscountByCategoryContact($products, $contact_id)
    {
        if (!$contact_id || empty($contact_id)) {
            return $products;
        }

        $productData = $products['data'] ?? $products;

        // Traemos el cliente
        $contact = Contact::find($contact_id);

        // Traemos el/los descuentos
        $discounts = Discount::where('parameter', $contact->customer->category_id)->get();

        if ($discounts) {
            foreach ($discounts as $key => $discount) {
                foreach ($productData as $keyp => $product) {

                    // Validar descuento por solo la linea
                    if (is_null($discount->brand_id)) {
                        if ($product['line_id'] == $discount->line_id) {
                            $productData[$keyp]['price']['price'] = $product['price']['price'] - ($product['price']['price'] * $discount->percentage / 100);
                            $productData[$keyp]['price']['percentage'] = $discount->percentage;
                            $productData[$keyp]['price']['discount'] = true;
                            $productData[$keyp]['price']['price_original'] = $product['price']['price'];
                        }

                    }

                    // Validar descuento por solo la linea
                    if (!is_null($discount->brand_id)) {
                        if ($product['line_id'] == $discount->line_id && $product['brand_id'] == $discount->brand_id) {
                            $productData[$keyp]['price']['price'] = $product['price']['price'] * $discount->percentage / 100;
                            $productData[$keyp]['price']['percentage'] = $discount->percentage;
                            $productData[$keyp]['price']['discount'] = true;
                        }

                    }
                }
            }
        }

        if (isset($products['data'])) {
            $products['data'] = $productData;
        } else {
            $products = $productData;
        }
        return $products;
    }

    /**
     * Este metodo trae el descuento segun el
     * valor en el campo "discount_percentage" de
     * la sucursal
     *
     * @author Kevin Galindo
     */
    public function getPriceDiscountByContactDiscountPercentage($products, $warehouse)
    {
        // Validamos si tiene la variable tiene algun valor
        if (!$warehouse || empty($warehouse)) {
            return $products;
        }

        // Traemos el tercero
        $contact = ContactWarehouse::find($warehouse);

        // Validamos si hay algun valor de descuento.
        if (empty($contact->discount_percentage)) {
            return $products;
        }

        // Traemos los productos
        $productData = $products['data'] ?? $products;

        // Recorremos los productos y asignamos el debido descuento.
        foreach ($productData as $keyp => $product) {

            // Validamos si el porcentaje es positivo o negativo
            if ($contact->discount_percentage > 0) {
                $totalDiscount = ($product['price']['price'] * $contact->discount_percentage) / 100;
                $productData[$keyp]['price']['price'] = $product['price']['price'] - $totalDiscount;
            } else {
                $totalDiscount = ($product['price']['price'] * abs($contact->discount_percentage)) / 100;
                $productData[$keyp]['price']['price'] = $product['price']['price'] + $totalDiscount;
            }

        }

        if (isset($products['data'])) {
            $products['data'] = $productData;
        } else {
            $products = $productData;
        }

        return $products;
    }

    /**
     * Este metodo asigna el precio del producto
     * segun los parametros
     *
     * @author Kevin Galindo
     */
    public function setPriceProducts($products, $request)
    {
        $model = Template::find($request->model_id);
        $branchoffice = BranchOffice::find($request->branchoffice_id);

        if (!$branchoffice) {
            $branchOfficeWarehouse = BranchOfficeWarehouse::find($request->branchoffice_warehouse_id);
            $branchoffice = $branchOfficeWarehouse->branchoffice;
        }

        if (!$branchoffice) {
            return $product;
        }

        // Traemos los productos
        $productData = $products['data'] ?? $products;

        // Validamos si existe modelo
        if ($model) {
            $price_list_id = $model->price_list_id;
        } else {
            $price_list_id = Parameter::where('paramtable_id', 14)->where('code_parameter', '001')->first();
            $price_list_id = $price_list_id ? $price_list_id->id : null;
        }

        foreach ($productData as $key => $product) {

            // lista de precio COSTO PROMEDIO
            if ($price_list_id == '24463') {
                $inventory = Inventory::where('product_id', $product['id'])
                    ->where('branchoffice_warehouse_id', $request->branchoffice_warehouse_id)
                    ->first();

                if ($inventory) {
                    $productData[$key]['price']['price'] = $inventory->average_cost;
                } else {
                    $productData[$key]['price']['price'] = 0;
                }

                continue;
            }

            // Traemos la lista de precio segun el modelo y la sede
            $listPrices = Price::where('price_list_id', $price_list_id)
                ->where('product_id', $product['id'])
                ->where('branchoffice_id', $branchoffice->id)
                ->first();

            // Validamos si existe lista de precio.
            if ($listPrices) {
                $productData[$key]['price'] = $listPrices;
            } else {
                // Traemos la lista de precios general
                $listPrices = Price::where('price_list_id', $price_list_id)
                    ->where('product_id', $product['id'])
                    ->whereNull('branchoffice_id')
                    ->first();

                if ($listPrices) {
                    $productData[$key]['price'] = $listPrices;
                } else {
                    $productData[$key]['price'] = ['price' => 0];
                }
            }
        }

        if (isset($products['data'])) {
            $products['data'] = $productData;
        } else {
            $products = $productData;
        }

        return $products;
    }

    /**
     * Este metodo se encarga de
     * traer todos los productos
     * que contengan una promocion
     *
     * @author Kevin Galindo
     */
    public function getProductsPromotion($request)
    {
        // Paginacion.
        $page = $request->page ? $request->page : 1;
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $request->paginate = false;

        $products = $this->productRepository->findAll($request);
        $products = $this->validateProductPromotion([
            'data' => $products->toArray(),
        ], true, $request);

        $data = new LengthAwarePaginator(
            array_slice($products, $offset, $perPage),
            count($products),
            $perPage,
            $page,
            [
                'path'  => $request->url(),
                'query' => $request->query(),
            ]
        );

        return $data;
    }

    /**
     * Obtiene productos a partir de un texto y criterios de clasificación
     *
     * @return array
     */
    public function productByClasification($request)
    {
        $products = $this->productRepository->productByClasification($request);
        /*if ($request->contact_id) {
        $products = $this->getPriceDiscountByCategoryContact($products->toArray(), $request->contact_id);
        }*/
        return $products;
    }

    /**
     * @return array
     */
    public function getProductsGrid($request)
    {
        $products = $this->productRepository->getProductsGrid($request);
        $products = $this->setPriceProducts($products->toArray(), $request);
        $products = $this->getPriceDiscountByCategoryContact($products, $request->contact_id);
        $products = $this->getPriceDiscountByContactDiscountPercentage($products, $request->warehouse_id);
        return $products;
    }

    /**
     * @author Jhon García
     */
    public function generateReport()
    {

        $products = $this->productRepository->getDataReport();

        $data = [];

        $headers = ['CÓDIGO', 'DESCRIPCIÓN', 'LÍNEA', 'SUBLÍNEA', 'MARCA', 'SUBMARCA', 'PRECIO DE LISTA', 'ACTIVO', 'IMAGEN', 'MANEJA INVENTARIO'];

        array_push($data, $headers);

        foreach ($products as $product) {

            $line = [
                strval($product->code),
                $product->description,
                $product->line ? $product->line->line_description : '',
                $product->subline ? $product->subline->subline_description : '',
                $product->brand ? $product->brand->description : '',
                $product->subbrand ? $product->subbrand->description : '',
                '$' . number_format(floatval($product->price ? $product->price->price : 0), 0, ',', '.'),
                $product->state ? 'SI' : 'NO',
                sizeof($product->productsAttachments) > 0 ? 'SI' : 'NO',
                $product->manage_inventory ? 'SI' : 'NO'
            ];

            array_push($data, $line);

        }

        $path = storage_path("app/public/reports/");

        $fileName = 'ReportProducts' . date('YmdHis') . '.csv';

        $fullName = $path . $fileName;

        $this->generateCSV($data, $fullName);

        return $fileName;

    }

    /**
     * @param $data
     * @param $fileName
     */
    public function generateCSV($data, $fileName)
    {
        $file_handle = fopen($fileName, 'w');
        foreach ($data as $line) {
            fputs($file_handle, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fputcsv($file_handle, $line, ";");
        }
        rewind($file_handle);
        fclose($file_handle);
    }

    /**
     *
     */
    public function calculatePromotions()
    {
        $branchOffices = BranchOffice::all();

        $count = 0;
        try {

            Log::info('/-------------- Iniciando calculo de promociones --------------/');

            DB::beginTransaction();

            DB::select('TRUNCATE TABLE products_has_promotions RESTART IDENTITY');

            foreach ($branchOffices as $branchOffice) {

                $request = (object)['branchoffice_id' => $branchOffice->id];

                $products = $this->productRepository->findAllProducts($request)->toArray();

                foreach ($products as $product) {

                    $promotions = $this->getPromotionsForProduct($product, $request);

                    foreach ($promotions as $promotion) {
                        $product_has_promotion = [
                            'product_id'      => $product['id'],
                            'promotion_id'    => $promotion['id'],
                            'branchoffice_id' => $branchOffice->id,
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s'),
                        ];

                        DB::table('products_has_promotions')->insert($product_has_promotion);

                        $count++;
                    }
                }

            }

            DB::commit();

            Log::info('/-------------- ' . $count . ' Promociones calculadas exitosamente --------------/');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }
    }

    /**
     * @param $product
     * @param $request
     * @return mixed
     */
    public function getPromotionsForProduct($product, $request)
    {
        $availability = !isset($product['inventory'][0]) || $product['inventory'][0]['available'] <= 0 ? 0 : $product['inventory'][0]['available'];

        // Validamos si la marca, submarca, linea o sublinea pertenece a alguna promocion.
        return Promotion::select('id')
            ->where(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('line_id', $product['line_id'])
                    ->where('subline_id', $product['subline_id'])
                    ->where('brand_id', $product['brand_id'])
                    ->where('subbrand_id', $product['subbrand_id'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->where('promotion_type_id', '!=', 1);
            })
            ->orWhere(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('line_id', $product['line_id'])
                    ->where('brand_id', $product['brand_id'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->where('promotion_type_id', '!=', 1);
            })
            ->orWhere(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->whereHas('productsPromotions', function ($subQueryTwo) use ($product) {
                        $subQueryTwo->where('product_id', $product['id']);
                    })
                    ->where('promotion_type_id', '!=', 1);
            })
            ->get()->toArray();
    }

    /**
     * @param $product
     * @param $request
     * @return array
     */
    public function getPromotionsDetailsForProduct($product, $request)
    {
        $availability = !isset($product['inventory'][0]) || $product['inventory'][0]['available'] <= 0 ? 0 : $product['inventory'][0]['available'];

        // Validamos si la marca, submarca, linea o sublinea pertenece a alguna promocion.
        return Promotion::
        with([
            'subline',
            'productsPromotions.product.line',
            'productsPromotions.product.price',
            'productsPromotions.product.brand',
            'productsPromotions.product.prices',
            'productsPromotions.product.subline',
            'productsPromotions.product.subbrand',
            'productsPromotions.product.category',
            'productsPromotions.product.line.taxes',
            'brand:id,brand_code,description,image',
            'subbrand:id,brand_id,subbrand_code,description,image',
            'branchoffice:id,subscriber_id,name,address,main,code',
            'line:id,subscriber_id,line_code,line_description,short_description',
            'productsPromotions:id,promotion_id,product_id,minimum_amount,discount,pay_soon,cash_payment,discount_value',
            'productsPromotions.product:id,long_description,code,description,category_id,subline_id,brand_id,subbrand_id,line_id,quantity_available,lock_sale,percentage_iva_sale,manage_inventory,provider,weight',
        ])
            ->where(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('line_id', $product['line_id'])
                    ->where('subline_id', $product['subline_id'])
                    ->where('brand_id', $product['brand_id'])
                    ->where('subbrand_id', $product['subbrand_id'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->where('promotion_type_id', '!=', 1);
            })
            ->orWhere(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('line_id', $product['line_id'])
                    ->where('brand_id', $product['brand_id'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->where('promotion_type_id', '!=', 1);
            })
            ->orWhere(function ($subQuery) use ($availability, $request, $product) {
                $subQuery
                    ->whereRaw('case when only_available and ' . $availability . ' <= 0 then false else true end')
                    ->whereRaw('current_date between start_date and end_date')
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->whereHas('productsPromotions', function ($subQueryTwo) use ($product) {
                        $subQueryTwo->where('product_id', $product['id']);
                    })
                    ->where('promotion_type_id', '!=', 1);
            })
            ->get()->toArray();
    }

    public function getProductByCode($request)
    {
        $data = $this->productRepository->searchProductByCode($request);
        return (object)[
            'code' => '200',
            'data' => (object)[
                'data'    => $data,
                'message' => '',
            ],
        ];
    }

    /**
     * Este método se encarga de devolver la informacion
     * del catalogo de productos para la app.
     *
     * @param Request $request Contiene los filtros de búsqueda a aplicar en la consulta
     * @return array Contiene los productos obtenidos de la base de datos
     * @author Kevin Galindo
     */
    public function appProductCatalog($request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        try {

            $request->merge(['paginate' => false]);

            $products = $this->productRepository->findAllSimple($request);

            $product_as_array = $this->productAddDataSheets($products->toArray());

            return [
                'data' => [
                    'products' => $product_as_array
                ],
                'code' => 200,
            ];

        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return [
                'data' => [
                    'message' => 'Error al enviar el catalogo del productos.'
                ],
                'code' => 400
            ];
        }
    }

    public function appListPriceProductByCustomer($request)
    {
        set_time_limit(0);

        $priceListIdGeneral = 924;
        $priceListIdMinimalSale = 948;
        $priceListByBranchoffices = [];

        // $priceListGeneral = Price::select(
        //     'id',
        //     'product_id',
        //     'price_list_id',
        //     'branchoffice_id',
        //     'price'
        // )
        // ->whereNull('branchoffice_id')
        // ->where('price_list_id', $priceListIdGeneral)
        // ->get();

        // $priceListMinimalSale = Price::select(
        //     'id',
        //     'product_id',
        //     'price_list_id',
        //     'branchoffice_id',
        //     'price'
        // )
        // ->whereNull('branchoffice_id')
        // ->where('price_list_id', $priceListIdMinimalSale)
        // ->get();

        $branchoffices = BranchOffice::orderBy('name')->get();

        foreach ($branchoffices as $key => $branchoffice) {

            $priceList = Price::select(
                'id',
                'product_id',
                'price_list_id',
                'branchoffice_id',
                'price'
            )
                ->where('branchoffice_id', $branchoffice->id)
                ->where('price_list_id', $priceListIdGeneral)
                ->get();

            $priceListByBranchoffices[$branchoffice->id] = $priceList;

        }

        $priceListGeneral = DB::select("select
            id,
            product_id,
            price_list_id,
            branchoffice_id,
            price,
            created_at,
            updated_at,
            minimal_price
        from fn_app_report_list_prices()");

        $dataReturn = [
            'price_list_general'       => $priceListGeneral,
            'price_list_branchoffices' => $priceListByBranchoffices,
            // 'price_list_minimal_sale' => $priceListMinimalSale
        ];

        return [
            'data' => $dataReturn,
            'code' => 200,
        ];
    }

    /**
     * Agregar información de data_sheets a un array de productos
     *
     * @param array $products Contiene los productos a los que se le van a agregar los data_sheets
     * @return array|mixed Contiene los productos con los data_sheets
     * @author Jhon García
     */
    public function productAddDataSheets(array $products)
    {
        $date_if_new = now()->addMonths(-1)->format('Y-m-d');

        $products_data = $products['data'] ?? $products;

        $data_sheets = DataSheet::all();

        // Recorrer los productos
        foreach ($products_data as $key => $product) {

            $new = false;

            if ($products[$key]['price']){
                $created_at_price = Carbon::parse($products[$key]['price']['created_at']);

                if ($created_at_price >  $date_if_new){
                    $new = false;
                }
            }

            $products[$key]['new'] = $new;

            // Construir un array para agregar los criterios que van dentro del campo de busqueda
            $search = [
                $products[$key]['code'],
                $products[$key]['description']
            ];

            // Validar si el producto tiene marca
            if ($products[$key]['brand']) {
                // Agregar la descripción de la marca al array
                array_push($search, $products[$key]['brand']['description']);
            }

            // Validar si el producto tiene línea
            if ($products[$key]['line']) {
                // Agregar la descripción de la línea al array
                array_push($search, $products[$key]['line']['line_description']);
            }

            // Validar si el producto tiene sublinea
            if ($products[$key]['subline']) {
                // Agregar la descripción de la sublinea al array
                array_push($search, $products[$key]['subline']['subline_description']);
            }

            // Establecer el atributo values de data_sheets como un array vacío
            $products_data[$key]['data_sheets']['values'] = [];

            $products_attachments = [];

            foreach ($products_data[$key]['products_attachments'] as $product_attachment) {
                $url = '/storage/products/app/' . $product_attachment['url'];
                $products_attachments[] = [
                    'url' => $url
                ];
            }

            $products_data[$key]['products_attachments'] = $products_attachments;

            // Validar si el producto cuenta con data_sheets
            if ($product['data_sheets']) {

                // Recorrer los data_sheets del producto
                foreach ($product['data_sheets']['value'] as $data_sheet_product) {

                    // Validar el id del data_sheet
                    if (!$data_sheet_product['data_sheet_id']) continue;

                    // Consultar el data_sheet
                    $data_sheet = $data_sheets->firstWhere('id', $data_sheet_product['data_sheet_id']);

                    // Construir array con información del data_sheet
                    $products_data[$key]['data_sheets']['values'][] = [
                        'data_sheet_id' => $data_sheet_product['data_sheet_id'],
                        'title'         => $data_sheet->visible_name,
                        'value'         => $data_sheet_product['value']
                    ];

                    // Agregar el valor del data_sheet al array
                    array_push($search, $data_sheet_product['value']);
                }

                unset($products_data[$key]['data_sheets']['value']);
            }

            // Establecer el valor de search con un string de los criterios de búsqueda
            $products_data[$key]['search'] = implode(' ', $search);
        }

        return $products_data;
    }

    public function addImageProduct(array $products)
    {
        $productsData = $products['data'] ?? $products;

        foreach ($productsData as $key => $product) {

            if ($product['brand']) {
                $imgArray = explode("/", $product['brand']['image']);

                $exists = \Storage::disk('public')->exists('brands/' . $imgArray[count($imgArray) - 1]);

                if ($exists) {
                    $base_64 = base64_encode(\Storage::disk('public')->get('brands/' . $imgArray[count($imgArray) - 1]));
                    $productsData[$key]['image'] = 'data:image/png;base64,' . $base_64;
                } else {
                    $productsData[$key]['image'] = "";
                }

            }

        }

        return $productsData;
    }

    public function appListInventoryProduct($request)
    {
        $request->merge(['paginate' => false]);

        $products = $this->productRepository->findAllSimple($request);

        $productArray = [];

        $inventories = Inventory::select(
            'inventories.id',
            'branchoffice_warehouse_id',
            'branchoffice_warehouses.branchoffice_id',
            'product_id',
            'stock',
            'stock_values',
            'average_cost',
            'inventories.created_at',
            'inventories.updated_at',
            'location',
            'available'
        )
            ->join('branchoffice_warehouses', 'branchoffice_warehouses.id', '=', 'inventories.branchoffice_warehouse_id')
            ->with([
                'product' => function ($query) {
                    $query->select('id', 'code', 'description');
                },
            ])
            ->whereHas('branchofficeWarehouse', function ($query) {
                $query->where('warehouse_code', '01');
            })
            ->whereIn('product_id', $products->pluck('id'));

        if ($request->date_from) {
            $inventories->where('inventories.updated_at', '>=', $request->date_from);
        }

        $inventories = $inventories->get()->toArray();

        foreach ($inventories as $key => $inventory) {
            $key_p = array_search(
                $inventory['product']['id'],
                array_column($productArray, 'product_id')
            );

            // Crear
            if ($key_p === false) {
                $productArray[] = [
                    'product_id'          => $inventory['product']['id'],
                    'product_code'        => $inventory['product']['code'],
                    'product_description' => $inventory['product']['description'],
                    'inventory'           => [Arr::except($inventory, 'product')],
                ];
            } // Actualizar
            else {
                $productArray[$key_p]['inventory'][] = Arr::except($inventory, 'product');
            }
        }

        return [
            'data' => $productArray,
            'code' => 200,
        ];

    }

    /**
     * @author Santiago torres
     *   Funcionamiento para cargar los productos de colrecambios desde un csv (productos_colrecambios.csv)
     */
    public function setProductsColrecambios()
    {
        try {
            DB::beginTransaction();
            // obtenemos informacion del csv
            $csv_info = $this->csvToArray(storage_path() . '/app/public/productos_colrecambios_0901.csv');
            foreach ($csv_info as $key => $row) {
                // creamos el registro de products
                $product = $this->createProductFromCsv($row);
            }

            DB::commit();
            return 'se han creado exitosamente';
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info('/////// Error Proceso masivo de carga productos /////////');
            Log::info($e->getFile() . ' ' . $e->getLine() . ' ' . $e->getMessage());
            Log::info('/////// /////////////////////////////////////// /////////');

            return $e->getFile() . ':' . $e->getLine() . ' ' . $e->getMessage();
        }
    }

    /**
     * Descripcion de producto en carga masiva colrecambios
     *
     * @author santiago Torres
     */
    public function setDescription($row, $line, $subline)
    {
        $description = trim($line->line_description);
        $description .= $subline->subline_description ? ' ' . trim($subline->subline_description) : '';
        $description .= trim($row["Aplicacin"]) != '#N/A' ? ' ' . trim($row["Aplicacin"]) : '';

        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', substr(trim($description), 0, 40));
    }

    /**
     * funcion para crear el registro en products
     */
    public function createProductFromCsv($row)
    {
        if (trim($row["Referencia "]) == '' || trim($row["Referencia "]) == '#N/A') {
            return false;
        }
        // obtenemos la linea
        $line = $this->lineRepositury->getLine($row);
        // obtenemos la sublinea
        $subline = $this->sublineRepository->getSubLine($row, $line);

        // seteamos la informacion del producto
        $data_create_product = [
            'code'                      => trim($row["Referencia "]),
            'previous_code_system'      => trim($row["Referencia Helisa"]),
            'short_description'         => trim($row["Descripcin corta"]),
            'rotation_classification'   => trim($row["Clasificacion Rotacion"]),
            'supplyer_code'             => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($row["cod-proveedor"])),
            'description'               => $this->setDescription($row, $line, $subline),
            'line_id'                   => $line->id,
            'subline_id'                => $subline->id,
            'brand_id'                  => 1,
            'alternate_code'            => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($row["Codigo alterno"])),
            //'units_per_pallet' => isset($row["unds por pallet"]) ? trim($row["unds por pallet"]) : null,
            // 'high'                      => trim($row["Alto "]) != '#N/A' ? trim($row["Alto "]) : null,
            // 'long'                      => trim($row["Largo "]) != '#N/A' ? trim($row["Largo "]) : null,
            // 'width'                     => trim($row["Ancho"]) != '#N/A' ? trim($row["Ancho"]) : null,
            // 'weight'                    => trim($row["Peso "]) != '#N/A' ? trim($row["Peso "]) : null,
            'certificate_of_conformity' => isset($row["Referencia Cert. Conformidad"]) ? trim($row["Referencia Cert. Conformidad"]) != '' ? trim($row["Referencia Cert. Conformidad"]) : null : null,
        ];

        $product = Product::updateOrCreate(
            [
                'code' => $data_create_product['code'],
            ],
            $data_create_product
        );

        // seteamos el precio
        $product->price = $this->priceRepository->setPricesFromCsv($row, $product);

        // seteamos el stock minimo
        $product->stock = $this->inventoryRepository->setMinimunStock($row, $product);

        // seteamos la aplicacion ficha de producto
        $product->aplication = $this->dataSheetRepository->setAplication($row, $product);
        return $product;
    }

    /**
     * Este obtiene un lote de 200 productos para realizar el consumo de estos en el WEBSERVICE por medio de SOAP de WMS y retornar arra con respuesta de este.
     *
     * @param N\A
     * @return Exitosa array('id' => cosecutivo,
     * 'message' => "Soup request Incorrect! "$codigo_producto,
     * 'error' => "Error En Validaciones del WEBSERVICE WMS"
     * );
     * @return NoExitosa array('id' => cosecutivo,
     * 'message' => "Soup request Incorrect! "$codigo_producto,
     * 'error' => "Error En Validaciones del WEBSERVICE WMS"
     * );
     * @return SinInformacion array(
     * 'data' => "Not information to load,
     * 'code' => 200
     * );
     * @author Haider Oviedo @Hom669
     */
    public function createSoapWmsMasive($id_product = '', $action = 'AM')
    {
        //Se obtiene los id de DataSheet para obtener Alto, Ancho, Largo @Hom669
        $sizes = $this->dataSheetRepository->findForInternalNameArray(['ANCHO:', 'ALTO:', 'LARGO:', 'UNDS_PALLET']);
        $countSizes = $sizes->count();

        //Se obtienen el o los productos que se enviaran a WMS @Hom669
        $products = $this->productRepository->getProductsWms($id_product, $action);
        $countproducts = $products->count();

        //$id_subscriber = Auth::user()->subscriber_id;
        //$provider = $this->productRepository->getProviderWms($id_subscriber);
        //Se inicia el array que almacenara todos las respuestas de los consumos uno a uno @Hom669
        $respuestas = array();

        for ($d = 0; $d < $countproducts; $d++) {

            //Se convierte la cadena de vase de datos con el array de dataSheet del producto en una collection para poder realizar filtros sobre esta @Hom669
            $arrayValue = collect(json_decode($products[$d]['value']));
            $countValue = $arrayValue->count();

            //Se realiza explode por si el codigo trae - ya que el WMS no recibira guiones @Hom669
            $code = explode("-", $products[$d]['code']);
            $countCode = count($code);

            //Se realiza validacion por si no se tienen guiones en el codigo @Hom669
            /*             if ($countCode == 1) {
                            $codeprod = $code[0];
                        } else {
                            $codeprod = $code[1];
                        }*/
            $codeprod = $products[$d]['code'];

            //Verificar si es Agregar o Modificar
            $action = 'AM';

            // se verifica que la informacion obtenida en el dataShet del producto tiene el ALTO,LARGO,ANCHO @Hom669
            for ($i = 0; $i < $countSizes; $i++) {
                for ($s = 0; $s < $countValue; $s++) {

                    if ($sizes[$i]['id'] == $arrayValue[$s]->data_sheet_id && $sizes[$i]['internal_name'] == 'LARGO:') {
                        $long = $arrayValue[$s]->value ? $arrayValue[$s]->value : 1;
                    } else if ($sizes[$i]['id'] == $arrayValue[$s]->data_sheet_id && $sizes[$i]['internal_name'] == 'ANCHO:') {
                        $width = $arrayValue[$s]->value ? $arrayValue[$s]->value : 1;
                    } else if ($sizes[$i]['id'] == $arrayValue[$s]->data_sheet_id && $sizes[$i]['internal_name'] == 'ALTO:') {
                        $high = $arrayValue[$s]->value ? $arrayValue[$s]->value : 1;
                    } else if ($sizes[$i]['id'] == $arrayValue[$s]->data_sheet_id && $sizes[$i]['internal_name'] == 'UNDS_PALLET') {
                        $unds_pallets = $arrayValue[$s]->value ? $arrayValue[$s]->value : 1;
                    } else if ($sizes[$i]['id'] == $arrayValue[$s]->data_sheet_id && $sizes[$i]['internal_name'] == 'PESO') {
                        $weigth = $arrayValue[$s]->value ? $arrayValue[$s]->value : 1;
                    }
                }
            }

            //Si no se obtiene informacion de ALTO($high),LARGO($long),ANCHO($width) se le asigna un 1 y si se tiene se convierte a decimal ya que no puede ser vacios y deben ser enteros @Hom669
            if (!isset($high)) {
                $high = 1;
            } else {
                $high = floatval(str_replace(',', '.', $high));
            }

            if (!isset($long)) {
                $long = 1;
            } else {
                $long = floatval(str_replace(',', '.', $long));
            }

            if (!isset($width)) {
                $width = 1;
            } else {
                $width = floatval(str_replace(',', '.', $width));
            }

            if (!isset($unds_pallets)) {
                $unds_pallets = 10000;
            } else {
                $unds_pallets = floatval(str_replace(',', '.', $unds_pallets));
            }

            if (!isset($weigth)) {
                $weigth = 1;
            } else {
                $weigth = floatval(str_replace(',', '.', $weigth));
            }

            // Valores por defecto de lote ($lot), minima cantidad de lote ($min_char_lot), maxima cantidad de lote ($max_char_lot) @Hom669
            $subscriber = Subscriber::where(['state' => true])->first();
            $subscriber_id = $subscriber->license_id;
            $subscriber_nit = $subscriber->identification;
            $subscriber_name = $subscriber->name;

            if ($subscriber_id == 3) {
                $lot = 'S';
                $min_char_lot = 4;
                $max_char_lot = 12;
            } else {
                $lot = 'N';
                $min_char_lot = 0;
                $max_char_lot = 0;
            }

            // Se alimenta Array con informacion de cada uno de los productos @Hom669
            $arreglo_articulos = array(
                'arreglo_articulos' => array(
                    'operacion'                   => $action,
                    'proveedor'                   => $subscriber_nit,//$products[$d]['provider'], //!empty($products[$d]['provider'])?$products[$d]['provider']:$provider[0]['identification'],
                    'prefijo'                     => '',
                    'articulo'                    => $codeprod, //SIN PREFIJO NI GUION
                    'tipo_producto'               => $products[$d]['line_code_wms'] ? $products[$d]['line_code_wms'] : $products[$d]['line_description'],
                    'nombre_tipo_producto'        => $products[$d]['line_description'] ? $products[$d]['line_description'] : 'SIN',
                    'presentacion'                => $products[$d]['presentation_packaging_buy'] ? $products[$d]['presentation_packaging_buy'] : 'UNID',
                    'nombre_pesentacion'          => $products[$d]['presentation_packaging_buy'] ? $products[$d]['presentation_packaging_buy'] : 'UNIDAD',
                    'codigo_barra'                => $products[$d]['code'],
                    'alterno1'                    => $products[$d]['code'],
                    'alterno2'                    => $products[$d]['old_products_code'],
                    'equivalencia_uno'            => '',
                    'equivalencia_dos'            => '',
                    /* 'nombre' => $products[$d]['description'],
                    'nombre_corto' => substr($products[$d]['description'], 0, 19),
                    'nombre_comercial' => $products[$d]['description'],
                    'nombre_foto' => $products[$d]['code'] . ".jpg", */
                    'nombre'                      => $products[$d]['line_description'] ? $products[$d]['line_description'] : 'SIN',
                    'nombre_corto'                => $products[$d]['line_description'] ? $products[$d]['line_description'] : 'SIN',
                    'nombre_comercial'            => $products[$d]['line_description'] ? $products[$d]['line_description'] : 'SIN',
                    'nombre_foto'                 => $products[$d]['code'] . ".jpg",
                    'grupo_linea'                 => 'SIN',
                    'nombre_grupo_linea'          => 'SIN',
                    'tipo_linea'                  => 'SIN',
                    'nombre_tipo_linea'           => 'SIN',
                    'linea'                       => 'SIN',
                    'nombre_linea'                => 'SIN',
                    'sublinea'                    => 'SIN',
                    'nombre_sublinea'             => 'SIN',
                    'tipo_atributo1'              => 'SIN',
                    'atributo1'                   => 'SIN',
                    'tipo_atributo2'              => 'SIN',
                    'atributo2'                   => 'SIN',
                    'tipo_atributo3'              => 'SIN',
                    'atributo3'                   => 'SIN',
                    'tipo_atributo4'              => 'SIN',
                    'atributo4'                   => 'SIN',
                    'tipo_atributo5'              => 'SIN',
                    'atributo5'                   => 'SIN',
                    'tipo_atributo6'              => 'SIN',
                    'atributo6'                   => 'SIN',
                    'porcentaje_iva'              => 'CERO',
                    'porcentaje_retencion'        => 'CERO',
                    'es_importado'                => 'S',
                    'es_producto_terminado'       => 'S',
                    'es_servicio'                 => 'N',
                    'es_controlado'               => 'N',
                    'es_propio'                   => 'N',
                    'es_esencial'                 => 'N',
                    'es_restringido_venta'        => 'N',
                    'es_materia_prima'            => 'N',
                    'es_unidad_minima'            => 'N',
                    'es_presentacion'             => 'N',
                    'es_retornable'               => 'N',
                    'es_empaque'                  => 'N',
                    'es_refrigerado'              => 'N',
                    'controla_temperatura'        => 'N',
                    'temperatura_minima'          => 0,
                    'temperatura_maxima'          => 0,
                    'permite_desempacar'          => 'N',
                    'maneja_fraccion'             => 'N',
                    'maneja_serie'                => 'N',
                    'series_adicionales'          => 0,
                    'min_caracteres_serie'        => 0,
                    'max_caracteres_serie'        => 0,
                    'maneja_lote'                 => $lot,
                    'min_caracteres_lote'         => $min_char_lot,
                    'max_caracteres_lote'         => $max_char_lot,
                    'maneja_documento1'           => 'N',
                    'maneja_documento2'           => 'N',
                    'maneja_estado'               => 'N',
                    'estado_articulo'             => 'SIN',
                    'maneja_fecha_vencimiento'    => 'N',
                    'maneja_fecha_fabricacion'    => 'S',
                    'maneja_revision'             => 'N',
                    'maneja_dimension'            => 'N',
                    'maneja_contenido'            => 'N',
                    //'valor_mercado' => $products[$d]['price'] ? $products[$d]['price'] : 10000, //Preguntar
                    'valor_mercado' => 0,//Se decidio en conversacion con Andres no enviar Valor Mercado @hom669
                    'valor_ultima_compra' => 0,
                    'fecha_ultima_compra' => '2021-06-08',
                    'activo' => $products[$d]['state'] ? 'N' : 'S',
                    'unidad_minima_pedido' => 1,
                    'maximo_apilado' => 10000,
                    'peso_bruto' => str_replace(",", ".", $weigth),
                    'peso' => str_replace(",", ".", $weigth),
                    'unidad_medida_peso' => 'KLG',
                    'largo_bruto' => $long,
                    'largo_neto' => $long,
                    'ancho_bruto' => $width,
                    'ancho_neto' => $width,
                    'alto_bruto' => $high,
                    'alto_neto' => $high,
                    'volumen' => $long * $width * $high,
                    'unidad_medida_longitud' => 'CMS',
                    'factor_conversion' => 1,
                    'unidad_medida_conversion' => isset($products[$d]['presentation_packaging_buy']) ? $products[$d]['presentation_packaging_buy'] : 'UNID',
                    'unidades_por_estiba' => $unds_pallets,
                    'metodo_rotacion' => 'LOTE',
                    'forma_almacenaje' => 3,
                    'tipo_rotacion' => 'A',
                    'validador' => 0,
                    'dias_antes_vencimiento' => 0,
                    'controla_peso' => 'N',
                    'maneja_peso' => 'N',
                    'maneja_unidad_alterna' => 'N',
                    'tipo_unidad_alterna' => '',
                    'unidad_alterna' => isset($products[$d]['presentation_packaging_buy']) ? $products[$d]['presentation_packaging_buy'] : 'UNID',
                    'serie_unica' => 'N',
                    'permite_alistar_mas' => 'N',
                    'porcentaje_alistar_mas' => 0,
                    'porcentaje_tolerancia_mayor' => 0,
                    'porcentaje_tolerancia_menor' => 0,
                    'integra_vocollect'           => 'N',
                    'foto'                        => '',
                    'campos_modifica'             => 'unidad_alterna,unidades_por_estiba,unidad_medida_conversion,peso_bruto,peso,maneja_lote,min_caracteres_lote,activo,max_caracteres_lote,nombre_foto,equivalencia_uno,equivalencia_dos,alterno1,alterno2,codigo_barra,proveedor,unidad_alterna,nombre,nombre_corto,nombre_comercial,unidad_alterna,largo_bruto,largo_neto,ancho_bruto,ancho_neto,alto_bruto,alto_netos,volumen,presentacion,nombre_pesentacion',
                    'datos_presentaciones'        => '',
                    'unidad_medida'               => '',
                    'cantidad'                    => '',
                    'largo'                       => '',
                    'ancho'                       => '',
                    'alto'                        => '',
                ),
            );

            //dd($arreglo_articulos);

            // Este es el webservice que vamos a consumir ruta base env('SOAP_URLWMS') variable .env @Hom669

            $wsdl = env('SOAP_URLWMS') . 'articulos.php?wsdl';
            // Creamos el cliente SOAP que hará la solicitud, generalmente están @Hom669
            // protegidos por un usuario y una contraseña env('SOAP_USERWMS') y env('SOAP_PASSWORDWMS') variable .env @Hom669

            $cliente = new SoapClient($wsdl, [
                'location'       => $wsdl,
                'encoding'       => 'UTF-8',
                'trace'          => 1,
                'exceptions'     => 1,
                'soap_version'   => 'SOAP_1_2',
                'keep_alive'     => false,
                'stream_context' => stream_context_create([
                    'ssl' => [
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true,
                    ],
                ]),
                'login'          => env('SOAP_USERWMS'),
                'password'       => env('SOAP_PASSWORDWMS'),
            ]);

            // Funcion WEBSERVICE SOAP que crea y actualiza los articulos en WMS @Hom669
            $resultado = $cliente->validarArticulos($arreglo_articulos);

            // Se Alimenta array de respuesta Agregados con exito y no aceptados @Hom669
            if ($resultado->arreglo_respuestas->estado == 'NO OK') {
                $respuestasnok = array(
                    'id'      => $d,
                    'message' => "Soup request Incorrect! " . $resultado->arreglo_respuestas->articulo,
                    'error'   => $resultado->arreglo_respuestas->mensaje,
                );
                array_push($respuestas, $respuestasnok);
            } else {

                $product = $this->productRepository->updateproductsWms($products[$d]['id_product'], $action);

                $array_provider = array(
                    'arreglo_proveedor' => array(
                        'identificacion'   => $subscriber_nit,
                        'proveedor'        => $subscriber_nit,
                        'nombre'           => $subscriber_name,
                        'codigo_ean'       => '',
                        'margen_recepcion' => 0,
                    ),
                    'detalle_articulos' => array(
                        'operacion'                   => 'A',
                        'articulo'                    => $codeprod,
                        'nombre'                      => $products[$d]['line_description'] ? $products[$d]['line_description'] : 'SIN',
                        'referencia_proveedor'        => $codeprod,
                        'unidad_minima'               => 1,
                        'cantidad_minima'             => 1,
                        'moneda'                      => 'COP',
                        'valor'                       => 0,
                        'porcentaje_iva'              => 'CERO',
                        'pais_origen'                 => 215,
                        'dias_vencimiento'            => 0,
                        'dias_vencimiento_devolucion' => 0,
                        'codigo_fabricante'           => '',
                        'nombre_fabricante'           => '',
                        'vigencia_registro'           => '',
                        'fecha_vencimiento'           => '',
                        'registro_sanitario'          => '',
                        'fecha_registro_sanitario'    => '',
                        'porcentaje_revision'         => ''
                    )

                );

                $wsdl1 = env('SOAP_URLWMS') . 'articulos_proveedores.php?wsdl';
                // Creamos el cliente SOAP que hará la solicitud, generalmente están @Hom669
                // protegidos por un usuario y una contraseña env('SOAP_USERWMS') y env('SOAP_PASSWORDWMS') variable .env @Hom669

                $cliente1 = new SoapClient($wsdl1, [
                    'location'       => $wsdl1,
                    'encoding'       => 'UTF-8',
                    'trace'          => 1,
                    'exceptions'     => 1,
                    'soap_version'   => 'SOAP_1_2',
                    'keep_alive'     => false,
                    'stream_context' => stream_context_create([
                        'ssl' => [
                            'verify_peer'       => false,
                            'verify_peer_name'  => false,
                            'allow_self_signed' => true,
                        ],
                    ]),
                    'login'          => env('SOAP_USERWMS'),
                    'password'       => env('SOAP_PASSWORDWMS'),
                ]);

                // Funcion WEBSERVICE SOAP que crea y actualiza los articulos en WMS @Hom669
                //dd($array_provider);
                $resultado1 = $cliente1->CrearArticulosProveedores($array_provider);
                if ($resultado1->mensaje[0]->estado == 'OK' && $resultado1->mensaje[0]->estado == 'OK') {
                    $respuestasok = array(
                        'id'      => $d,
                        'message' => "Soup request Correct! " . $resultado->arreglo_respuestas->articulo,
                        'error'   => $resultado->arreglo_respuestas->mensaje,
                    );
                    array_push($respuestas, $respuestasok);
                } else {
                    $respuestasnok = array(
                        'id'      => $d,
                        'message' => "Soup request Error not provider create ",
                        'error'   => $resultado1[0]['mensaje'] . "_" . $resultado1[1]['mensaje'],
                    );
                    array_push($respuestas, $respuestasnok);
                }


            }

            //Log::channel('productslog')->info("Cosumed Product ".$products[$d]['id_product']);
            //Log::channel('productslog')->info("Respuest Web Service ".$resultado->arreglo_respuestas->articulo." Message ".$resultado->arreglo_respuestas->mensaje);

        }

        //Log::channel('productslog')->info("Respuest Web Service Not information to load");
        // Respuesta si no hay informacion @Hom669
        return [
            'data' => $respuestas ? $respuestas : "Not information to load",
            'code' => 200,
        ];


    }

}
