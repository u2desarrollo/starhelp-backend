<?php


namespace App\Services;

use App\Entities\Template;
use App\Entities\Document;
use App\Entities\DocumentTransactions;
use App\Jobs\UpdateBalancesJob;
use App\Repositories\InventoryRepository;
use App\Repositories\DocumentInformationsRepository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Validator;

class DocumentAccountingService
{

    private $inventoryRepository;
    private $balance_service;
    private $documentInformationsRepository;
    public function __construct(InventoryRepository $inventoryRepository,DocumentInformationsRepository $documentInformationsRepository,BalancesService $balance_service)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->documentInformationsRepository = $documentInformationsRepository;
        $this->balance_service = $balance_service;
    }

    /**
     * Este metodo se encarga de realizar la contabilidad
     * de un documento segun el modelo.
     *
     * @author Kevin Galindo
     */
    public function accountingInventoryOperationsByDocument($document, $calculate_balances = true)
    {
        $model = Template::
        with([
            'templateAccountings' => function ($query) {
                $query->with([
                    'concept',
                    'according',
                    'account'
                ])
                ->orderBy('order_accounting');
            }
        ])
        ->find($document->model_id);


        if ($model && !$model->accounting_generate) return true;

        try {
            DB::beginTransaction();

            //limpiar bandera de error
            $this->documentInformationsRepository->clearAccountingErrors($document->id);

            // Eliminamos las transacciones del documento
            $this->deleteAllTransactions($document->id);

            $templateAccountings = $model->templateAccountings;

            foreach ($templateAccountings as $key => $templateAccounting) {

                $conceptCode = $templateAccounting->concept->code_parameter;

                switch ($conceptCode) {
                    case '01':
                        $this->templateAccountingSaleBrut($templateAccounting, $document);
                    break;
                    case '02':
                        $this->templateAccountingCostBrut($templateAccounting, $document);
                    break;
                    case '03':
                        $this->templateAccountingInventory($templateAccounting, $document);
                    break;
                    case '04':
                        $this->templateAccountingWallet($templateAccounting, $document);
                    break;
                    case '05':
                        $this->templateAccountingPaymentMethods($templateAccounting, $document);
                    break;
                    case '06':
                        $this->templateAccountingRetention($templateAccounting, $document, $model);
                    break;
                    case '07':
                        // ICA
                    break;
                    case '08':
                        $this->templateAccountingIva($templateAccounting, $document);
                    break;
                    case '09':
                        // auto retenciones en compras
                        $this->templateAccountingSelfRetentionPurchase($templateAccounting, $document);
                    break;
                    case '10':
                        // auto retenciones en ventas
                        $this->templateselfWithholdingOnSales($templateAccounting, $document);
                    break;

                    case '11':
                        // IMPUESTOS ASUMIDOS
                        $this->taxesAssumed($templateAccounting, $document);
                    break;

                    case '12':
                        // OBSEQUIO CLIENTES
                        $this->templateAccountingGiftCustomer($templateAccounting, $document);
                    break;
                }
            }

            $totalDebit = 0;
            $totalCredit = 0;
            foreach ($document->accounting_transactions as $key => $transaction) {
                if ($transaction->transactions_type == 'D') {
                    $totalDebit += $transaction->operation_value;
                } else {
                    $totalCredit += $transaction->operation_value;
                }
            }

            $totalDifference = $totalDebit - $totalCredit;

            Document::find($document->id)->update([
                'accounting_debit_value' => $totalDebit,
                'accounting_credit_value' => $totalCredit,
                'accounting_total_difference' => $totalDifference < 0 ? $totalDifference*-1 : $totalDifference,
            ]);

            $document_date = Carbon::parse($document->document_date);

            if ($calculate_balances) {
                UpdateBalancesJob::dispatch($document_date->format('Ym'), $document->id)->onQueue('high');
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return false;
        }
    }

    /**
     * Elimina informacion de
     * las transacciones de un documento
     *
     * @author Kevin Galindo
     */
    public function deleteAllTransactions($document_id)
    {
        DocumentTransactions::where('document_id', $document_id)->forceDelete();
    }

    /**
     * Contabiliza segun el concepto "BRUTO VENTA"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingSaleBrut($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {
            if ($document->operationType->code == "155") {
                $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct, 'devol_sale');
            } else {
                $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct, 'sale');
            }

            $operation_value = $documentProduct->total_value_brut;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);

    }

    /**
     * Contabiliza segun el concepto "OBSEQUIO CLIENTES"
     *
     * @author Santiago Torres
     */
    public function templateAccountingGiftCustomer($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {
            $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct);

            $operation_value = $documentProduct->total_value_brut;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);

    }

    /**
     * Contabiliza segun el concepto "IMPUESTOS ASUMIDOS"
     *
     * @author Santiago Torres
     */
    public function taxesAssumed($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {

            $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct);
            $taxIva = $documentProduct->documentProductTaxes->where('tax_id', 1)->first();
            $operation_value = $taxIva ? $taxIva->value : 0;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "AUTORRETENCION COMPRAS 2.5%"
     *
     * @author Satiago Torres
     */
    public function templateAccountingSelfRetentionPurchase($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        // validamos si tiene codigo de regimen
        if ($templateAccounting->concept->alphanum_data_third != null) {
            //validamos se el tercero tiene asociado regimen tributario
            if ($document->contact->taxRegimep != null) {
                // si en la parametrizacion el concepto tiene codigo de regimen y el regimen del tercero es diferente, no se contabiliza
                if ($document->contact->taxRegimep->code_parameter != $templateAccounting->concept->alphanum_data_third) {
                    return;
                }
            }else{
                // si en la parametrizacion el concepto tiene codigo de regimen pero el tercero no tiene, no se contabiliza
                return;
            }
        }


        if ($document->total_value_brut > $templateAccounting->concept->alphanum_data_second) {
            $account = $this->getAccountByAccordingTo($templateAccounting);
            $operation_value = $document->total_value_brut * $templateAccounting->concept->alphanum_data_first /100;
            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);

        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "BRUTO COSTO"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingCostBrut($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {
            $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct, 'cost');
            $operation_value = $documentProduct->inventory_cost_value;
            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "INVENTARIO"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingInventory($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {
            $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct, 'inventory');
            // $operation_value = $this->inventoryRepository->getAverageCostByProduct($documentProduct->product_id, $documentProduct->branchoffice_warehouse_id);
            $operation_value = $documentProduct->inventory_cost_value;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);

    }

    /**
     * Contabiliza segun el concepto "IVA"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingIva($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->documentsProducts as $key => $documentProduct) {

            $account = $this->getAccountByAccordingTo($templateAccounting, $documentProduct, 'inventory');
            $taxIva = $documentProduct->documentProductTaxes->where('tax_id', 1)->first();
            $operation_value = $taxIva ? $taxIva->value : 0;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "CARTERA"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingWallet($templateAccounting, $document) {

        $saveDocumentTransaction = [];
        $account = $this->getAccountByAccordingTo($templateAccounting);
        $totalWallet = $document->total_value;

        $totalWallet -= $document->ret_fue_total;
        $totalWallet -= $document->ret_iva_total;
        $operation_value = $totalWallet -= $document->ret_ica_total;

        $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "AUTORRETENCION EN VENTAS CREE 0,04%"
     *
     * @author Kevin Galindo
     */
    public function templateselfWithholdingOnSales($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        $account = $this->getAccountByAccordingTo($templateAccounting);
        // $operation_value = ($document->total_value_brut * 0.40) / 100;
        $operation_value = $document->total_value_brut * $templateAccounting->concept->alphanum_data_first /100;
        $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "METODOS DE PAGO"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingPaymentMethods($templateAccounting, $document) {

        $saveDocumentTransaction = [];

        foreach ($document->paymentMethods as $key => $paymentMethod) {

            $parameter = $paymentMethod->paymentMethod;
            $account = $this->getAccountByAccordingTo($templateAccounting, null, null, null, null, $parameter);
            $operation_value = $paymentMethod->total_value;

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
        }

        // Crear transacciones
        $this->saveAccounting($saveDocumentTransaction);
    }

    /**
     * Contabiliza segun el concepto "RETENCIONES"
     *
     * @author Kevin Galindo
     */
    public function templateAccountingRetention($templateAccounting, $document, $model) {

        $saveDocumentTransaction = [];
        $execute = true;

        switch ($model->contact_type) {
            // Cliente
            case 1:
                // FUE
                $accountFue         = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'fue');
                $accountFueService  = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'fue_serv');

                // ICA
                $accountIca         = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'ica');
                $accountIcaService  = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'ica_serv');

                // IVA
                $accountIva         = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'iva');
                $accountIvaService  = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'iva_serv');

            break;
            // Proveedor
            case 2:
                // FUE
                $accountFue        = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'fue_provider');
                $accountFueService = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'fue_serv_provider');

                // ICA
                $accountIca        = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'ica_provider');
                $accountIcaService = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'ica_serv_provider');

                // IVA
                $accountIva        = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'iva_provider');
                $accountIvaService = $this->getAccountByAccordingTo($templateAccounting, null, null, $document, 'iva_serv_provider');
            break;
            default:
                $execute = false;
            break;
        }

        if (!$execute) {
            return;
        }

        // Organizar informacion
        $retentions = [
            'fue' => [
                'value' => 'ret_fue_prod_total',
                'account' => $accountFue,
            ],
            'fue_serv' => [
                'value' => 'ret_fue_serv_total',
                'account' => $accountFueService,
            ],

            'ica' => [
                'value' => 'ret_ica_prod_total',
                'account' => $accountIca,
            ],
            'ica_serv' => [
                'value' => 'ret_ica_serv_total',
                'account' => $accountIcaService,
            ],

            'iva' => [
                'value' => 'ret_iva_prod_total',
                'account' => $accountIva,
            ],
            'iva_serv' => [
                'value' => 'ret_iva_serv_total',
                'account' => $accountIvaService,
            ],
        ];


        foreach ($retentions as $key => $retention) {
            $account = $retention['account'];
            $operation_value = $document[$retention['value']];

            $saveDocumentTransaction = $this->saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document);
            // Crear transacciones
            $this->saveAccounting($saveDocumentTransaction);
        }

    }

    /**
     * Este metodo se encarga de traer la cuenta teniendo
     * en cuenta el "segun" del modelo.
     *
     * @author Kevin Galindo
     */
    public function getAccountByAccordingTo($templateAccounting, $documentProduct = null, $typeAccount = null, $document = null, $typeRetention = null, $parameter = null)
    {
        $accordingCode = $templateAccounting->according->code_parameter;

        $account = null;

        switch ($accordingCode) {
            case '01':
                $account = $templateAccounting->account;
            break;
            case '02':
                $account = $this->getAccountByLineOrSubline($documentProduct, $typeAccount);
            break;
            case '03':
                $account = $this->getAccountByClientRetention($document, $typeRetention);
            break;
            case '04':
                // dd(4);
            break;
            case '05':
                $account = Account::find($parameter->accounting_account_id);
            break;
        }

        return $account;

    }

    /**
     * Retorna la cuenta segun la sublinea o
     * linea del producto.
     *
     * @author Kevin Galindo
     */
    public function getAccountByLineOrSubline($documentProduct, $typeAccount) {
        $account = null;

        switch ($typeAccount) {
            case 'sale':
                if ($documentProduct->subline && $documentProduct->subline->saleAccount) {
                    $account = $documentProduct->subline->saleAccount;
                } else if ($documentProduct->line && $documentProduct->line->saleAccount) {
                    $account = $documentProduct->line->saleAccount;
                }
            break;
            case 'inventory':

                if ($documentProduct->subline && $documentProduct->subline->inventorieAccount) {
                    $account = $documentProduct->subline->inventorieAccount;
                } else if ($documentProduct->line && $documentProduct->line->inventorieAccount) {
                    $account = $documentProduct->line->inventorieAccount;
                }
            break;
            case 'cost':

                if ($documentProduct->subline && $documentProduct->subline->costAccount) {
                    $account = $documentProduct->subline->costAccount;

                } else if ($documentProduct->line && $documentProduct->line->costAccount) {
                    $account = $documentProduct->line->costAccount;
                }
            break;
            case 'devol_sale':

                if ($documentProduct->subline && $documentProduct->subline->devolSaleAccount) {
                    $account = $documentProduct->subline->devolSaleAccount;

                } else if ($documentProduct->line && $documentProduct->line->devolSaleAccount) {
                    $account = $documentProduct->line->devolSaleAccount;
                }
            break;
        }

        return $account;
    }

    /**
     * Retorna la cuenta segun el tercero y
     * la retencion.
     *
     * @author Kevin Galindo
     */
    public function getAccountByClientRetention($document, $typeRetention) {
        $account = null;

        switch ($typeRetention) {
            // rete fuente
            case 'fue':
                $account = $document->warehouse->retFueAccount;
            break;
            case 'fue_serv':
                $account = $document->warehouse->retFueServAccount;
            break;
            case 'fue_provider':
                $account = $document->warehouse->retFueProviderAccount;
            break;
            case 'fue_serv_provider':
                $account = $document->warehouse->retFueServProviderAccount;
            break;
            //--
            // rete ica
            case 'ica':
                $account = $document->warehouse->retIcaAccount;
            break;
            case 'ica_serv':
                $account = $document->warehouse->retIcaServAccount;
            break;
            case 'ica_provider':
                $account = $document->warehouse->retIcaProviderAccount;
            break;
            case 'ica_serv_provider':
                $account = $document->warehouse->retIcaServProviderAccount;
            break;
            //--
            // rete iva
            case 'iva':
                $account = $document->warehouse->retIvaAccount;
            break;
            case 'iva_serv':
                $account = $document->warehouse->retIvaServAccount;
            break;
            case 'iva_provider':
                $account = $document->warehouse->retIvaProviderAccount;
            break;
            case 'iva_serv_provider':
                $account = $document->warehouse->retIvaServProviderAccount;
            break;
            //--
        }

        return $account;
    }

    /**
     * Este metodo se encarga de validar y guardar la informacion
     * correspondiente de la trasaccion.
     *
     * @author Kevin Galindo
     */
    public function saveAccountingInArray($saveDocumentTransaction, $templateAccounting, $account, $operation_value, $document)
    {
        if ($account) {
            $transactionsType = $templateAccounting->debit_credit == 'DEBITO' ? 'D' : 'C';

            $key_array_duplicate = array_filter($saveDocumentTransaction, function($item) use($account, $transactionsType){
                return ($item['account_id'] == $account->id && $item['transactions_type'] == $transactionsType);
            });

            // Crear
            if (count($key_array_duplicate) == 0) {

                $dataSave = [
                    'year_month'                 => substr($document->document_date, 0, 4).substr($document->document_date, 5, 2),
                    'document_id'                => $document->id,
                    'branchoffice_id'            => $document->branchoffice_id,
                    'vouchertype_id'             => $document->vouchertype_id,
                    'account_id'                 => $account->id,
                    'contact_id'                 => $document->contact_id,
                    'quota_number'               => null,
                    'consecutive'                => $document->consecutive,
                    'document_date'              => $document->document_date,
                    'seller_id'                  => $document->seller_id,
                    'issue_date'                 => null,
                    'pay_day'                    => null,
                    'due_date'                   => $document->due_date,
                    'concept'                    => $templateAccounting->concept->id,
                    'detail'                     => null,
                    'refer_detail'               => null,
                    'observations'               => $templateAccounting->concept->name_parameter,
                    'type_bank_document'         => null,
                    'bank_document_number'       => null,
                    'cost_center_1'              => null,
                    'cost_center_2'              => null,
                    'cost_center_3'              => null,
                    'number_business'            => null,
                    'payment_method'             => null,
                    'operation_value'            => number_format($operation_value, 2, '.', ''),
                    'transactions_type'          => $transactionsType,
                    'base_value'                 => null,
                    'order'                      => $templateAccounting->order_accounting,
                    'base_document_id'           => $document->document->id,
                    'projects_id'                => $this->setProjectsId($document, $account),
                ];


                $dataSave = $this->saveAccountingInArrayCrossingDocument($dataSave, $document);

                $saveDocumentTransaction[] = $dataSave;
            }
            // Actualizamos.
            else {
                $saveDocumentTransaction[key($key_array_duplicate)]['operation_value'] += number_format($operation_value, 2, '.', '');
            }
        }

        return $saveDocumentTransaction;
    }

    /**
     * setear el negocio de la transaccion
     * @author santiago torres
     * @param Document $document | Account $account
     */
    public function setProjectsId($document, $account)
    {
        // si la cuenta maneja proyeto pero el documento no tiene proyecto gradamos el error
        if ($account->manage_projects != 0 && (is_null($document->projects_id) || empty($document->projects_id))) {
            $this->documentInformationsRepository->setAccountingError($document->id, "La cuenta ".$account->code." maneja Proyecto pero, el documento No");
        }
        return $document->projects_id;
    }

    /**
     * Este metodo se encarga de guardar
     * la informacion en la tabla de document_transactions
     *
     * @author Kevin Galindo
     */
    public function saveAccounting($saveDocumentTransaction)
    {
        foreach ($saveDocumentTransaction as $key => $item) {
            $item['operation_value'] = round($item['operation_value']);
            if ($item['operation_value'] > 0 && !is_null($item['operation_value'])) {
                DocumentTransactions::create($item);
            }
        }
    }

    /**
     * Este metodo se encarga de
     * guardar el documento cruce en la transaccion
     *
     * @author Kevin Galindo
     */
    public function saveAccountingInArrayCrossingDocument($saveDocumentTransaction, $document)
    {

        $saveDocumentTransaction['affects_document_id'] = $document->id;


        if (!$document->origin_document && $document->id !== $document->thread && !is_null($document->thread)) {
            $saveDocumentTransaction['affects_document_id'] = $document->document->id;
        }

        return $saveDocumentTransaction;
    }

    /**
     * Este metodo actualizar el valor "accounting_inconsistencies" del
     * docuemnto.
     *
     * @author Kevin Galindo
     */
    public function updateAccountingInconsistencies($document_id, $value = '')
    {
        return Document::find($document_id)->update([
            'accounting_inconsistencies' => $value
        ]);
    }


    public function accountingInventoryOperations($request)
    {
        $model_id = $request->model_id;
        $branchoffice_id = $request->branchoffice_id;
        $filterDate = $request->filterDate;

        if (!$filterDate) {
            return [
                'code' => '404',
                'message' => 'faltan parametros'
            ];
        }

        $filterDate = [
            $request->filterDate[0]. ' 00:00:00',
            $request->filterDate[1]. ' 23:59:59'
        ];

        // try {
        //     DB::beginTransaction();

        $documents = Document::when($model_id, function($query) use ($model_id) {
            $query->where('model_id', $model_id);
        })
        ->when($branchoffice_id, function($query) use ($branchoffice_id) {
            $query->where('branchoffice_id', $branchoffice_id);
        })
        ->whereBetween('document_date', $filterDate)
        ->whereHas('template', function ($query) {
            $query->where('accounting_generate', true);
        })
        ->where('in_progress', false)
        ->where(function($query) {
            $query->where('canceled', false);
            $query->orWhereNull('canceled');
        })
        ->where(function($query) {
            $query->where('closed', false);
            $query->orWhereNull('closed');
        })
        // ->where('id', '562')
        ->get();

        if ($documents) {
            $withError = false;
            foreach ($documents as $key => $document) {
                Log::info($key);
                $response = $this->accountingInventoryOperationsByDocument($document, false);

                if (!$response) {
                    $withError = true;
                }
            }
        }

            // DB::commit();

        return [
            'code' => '200',
            'message' => $withError ? 'Algunos documentos no se contabilizaron' : 'Finalizado correctamente.'
        ];

        // } catch (\Exception $e) {
        //     Log::error('------------- modificación documento contable ------------');
        //     Log::error($e->getMessage().' '. $e->getLine(). " " . $e->getFile());

        //     DB::rollBack();

        //     return (object) [
        //         'code' => '500',
        //         'completed' => false,
        //         'data' => (object) [
        //             'message' => 'Hubo un error al obtener documeto a modificar'
        //         ]
        //     ];
        // }
    }

    public function accountingInventoryOperationsCountDocuments($request)
    {
        $model_id = $request->model_id;
        $branchoffice_id = $request->branchoffice_id;
        $filterDate = [
            $request->filterDate[0]. ' 00:00:00',
            $request->filterDate[1]. ' 23:59:59'
        ];

        return Document::when($model_id, function($query) use ($model_id) {
            $query->where('model_id', $model_id);
        })
        ->when($branchoffice_id, function($query) use ($branchoffice_id) {
            $query->where('branchoffice_id', $branchoffice_id);
        })
        ->whereBetween('document_date', $filterDate)
        ->whereHas('template', function ($query) {
            $query->where('accounting_generate', true);
        })
        ->where('in_progress', false)
        ->where(function($query) {
            $query->where('canceled', false);
            $query->orWhereNull('canceled');
        })
        ->where(function($query) {
            $query->where('closed', false);
            $query->orWhereNull('closed');
        })
        ->count();
    }

}
