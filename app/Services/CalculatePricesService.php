<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Entities\ContactWarehouse;
use Illuminate\Support\Collection;

/**
* Class CalculatePricesService
* @package App\Services
*/
class CalculatePricesService
{

	private $promotions;
	private $criteria_contact;
	private $criteria_products;
	private $contact_warehouse;
	private $products;
	private $product;

	public function __construct($contact_warehouse_id){

		$this->setContactWarehouse($contact_warehouse_id);

		$this->setPromotions();

		$this->setCriteriaContats();

		$this->setCriteriaProducts();

	}

	/**
	 * Set a value for contact_warehouse
	 */
	private function setContactWarehouse($contact_warehouse_id){

		$this->contact_warehouse = ContactWarehouse::select('contacts_warehouses.id','price_list_id', 'contact_id', 'description')
			->where('contacts_warehouses.id', $contact_warehouse_id)
			->with(['line'])
			->first();

	}

	/**
	 * Set a value for products
	 */
	private function setProducts(){
		$this->products = DB::table('products')
			->select('products.id', 'pri.price')
			->join('prices AS pri', 'pri.product_id', '=', 'products.id')
			->where('products.line_id', $this->contact_warehouse->line->line_id)
			->where('pri.price_list_id', $this->contact_warehouse->price_list_id)
			->orderBy('products.description', 'ASC')
			->get();
	}

	/**
	 * Set a value for promotions
	 */
	private function setPromotions(){

		$this->promotions = DB::select('select * from public.fn_getpromotions()');

	}

	/**
	 * Set a value for criteria_contacts
	 */
	private function setCriteriaContats(){

		$this->criteria_contact = DB::table('contacts_warehouses AS cw')
		->select('cv.parameter_id')
		->join('criteria_values AS cv', 'cv.relation_id', '=', 'cw.id')
		->where('cv.criteria_type_id', 1)
		->where('cw.id', $this->contact_warehouse->id)
		->pluck('parameter_id');

	}

	/**
	 * Set a value for criteria_products
	 */
	private function setCriteriaProducts(){

		$this->criteria_products = DB::table('criteria_values AS cv')
			->select('relation_id', 'parameter_id')
			->join('products AS pro', 'pro.id', '=', 'cv.relation_id')
			->where('criteria_type_id', 2)
			->whereIn('parameter_id', function ($query){
				$query->select('parameters_id')->from('promotion_rule AS pr')
				->join('promotion_group_rules AS pgr','pgr.id', '=', 'pr.promotion_group_rule_id')
				->join('promotions AS p', 'p.id', '=', 'pgr.promotion_id')
				->where(['pgr.state' => true, 'type' => '2', 'p.state' => true])
				->get();
			})
			->orderBy('pro.description', 'ASC')
			->get();

	}

	/**
	 * Method that contains the necessary logic to calculate the discount percentage of a product
	 * @return integer that contains the discount percentage
	 */
	public function calculate($promotion, $product){

		//Validate promotion by price list
		if($promotion->price_list_id != null && $promotion->price_list_id != $this->contact_warehouse->price_list_id){
			return 0;
		}

		if($promotion->type_customer_id != null){
			if(!in_array($promotion->type_customer_id, $this->criteria_contact->toArray())){
				return 0;
			}
		}

		//Validate promotion by criteria contact
		if($promotion->criteria_contact_1 != null){
			if(!in_array($promotion->criteria_contact_1, $this->criteria_contact->toArray())){
				return 0;
			}
		}

		//Validate promotion by criteria product
		if($promotion->criteria_product_1 != null){
			$foundCriteriaProduct1 = false;
			foreach ($this->criteria_products as $p){
				if ($p->relation_id == $product->id && $p->parameter_id == $promotion->criteria_product_1){
					$foundCriteriaProduct1 = true;
				}
			}

			if(!$foundCriteriaProduct1){
				return 0;
			}
		}

		//Validate promotion by criteria contact
		if($promotion->criteria_contact_2 != null){
			if(!in_array($promotion->criteria_contact_2, $this->criteria_contact->toArray())){
				return 0;
			}
		}

		//Validate promotion by criteria product
		if($promotion->criteria_product_2 != null){
			$foundCriteriaProduct2 = false;
			foreach ($this->criteria_products as $p){
				if ($p->relation_id == $product->id && $p->parameter_id == $promotion->criteria_product_2){
					$foundCriteriaProduct2 = true;
				}
			}

			if(!$foundCriteriaProduct2){
				return 0;
			}
		}

		//Validate promotion by contact_id and contact_warehouse_id
		if($promotion->contact_id != null){
			if($promotion->contact_id == $this->contact_warehouse->contact_id){
				if($promotion->contact_warehouse_id != null){
					if($promotion->contact_warehouse_id != $this->contact_warehouse->id){
						return 0;
					}
				}
			}else{
				return 0;
			}
		}

		//Validate promotion by product_id
		if($promotion->product_id != null && $promotion->product_id != $product->id){
			return 0;
		}

		return $promotion->percentage;

	}

	/**
	 * Method that receives an array of products and establishes its percentage of discount for each one
	 */
	public function calculatePromotions() {
		try{

			$this->setProducts();

			$grouped_products =  $this->products->split(4);

			foreach($grouped_products as $group){

				DB::beginTransaction();

				DB::table('promotion_prices')->where('contact_warehouse_id', $this->contact_warehouse->id)->where('created_at', '<', date('Y-m-d'))->delete();

				Log::info("Starting pricing calculation for ". $this->contact_warehouse->description);

				$count = 0;

				foreach($group as $product){

					$percentage = 0;

					foreach($this->promotions as $promotion){

						$percentage = $this->calculate($promotion, $product);

						if ($percentage > 0){
							$promotion_insert = [
								'contact_warehouse_id' => $this->contact_warehouse->id,
								'product_id' => $product->id,
								'price' => $product->price,
								'percentage' => $percentage,
								'created_at' => date('Y-m-d')
							];
		
							DB::table('promotion_prices')->insert($promotion_insert);
							$count++;
							break;
						}
					}

				}

				Log::info("$count promotions calculated successfully for ". $this->contact_warehouse->description);
				DB::commit();

			}
		}catch(\Exception $e){
			DB::rollBack();
			Log::error("Error while calculating promotions for " . $this->contact_warehouse->description . ": " . $e->getMessage() . " in line " . $e->getLine());
		}

	}

	/**
	 * Method that establishes the price of a product according to the id of the same and the id of the client
	 * @param $product
	 */
	private function setPriceProduct($product){

		$this->product = $product;

		$price = DB::table('prices')->select('price')->where([
			'price_list_id' => $this->contact_warehouse->price_list_id,
			'product_id' => $product->id
			])->first();

		$this->product->price->price = $price->price;

	}

	/**
	 * Method that establishes the discount percentage of a product
	 * @return array product whit price and percentage disccount
	 * @param $product
	 */
	public function calculateSingleProduct($product){

		$this->setPriceProduct($product);

		$percentage = 0;

		foreach($this->promotions as $promotion){

			$percentage = $this->calculate($promotion, $product);

			if ($percentage > 0){
				break;
			}

		}

		$this->product->price->percentage = (int) $percentage;

		return  $this->product;

	}

	/**
	 * Method that receives an array of products and establishes its percentage of discount for each one
	 */
	public function calculateMultipleProducts($products){

		$this->products = $products;

		$products_response = [];

		foreach($this->products as $product){

			$percentage = 0;

			foreach($this->promotions as $promotion){

				$product_object = (object)$product;

				$percentage = $this->calculate($promotion, $product_object);

				if ($percentage > 0){
					break;
				}

			}

			$price_object['percentage'] = (int) $percentage;
			$price_object['calculated'] = true;
			$price_object['price'] = $product_object->price['price'];

			$product_object->price = (object) $price_object;

			array_push($products_response, $product_object);

		}

		return $products_response;

	}
}