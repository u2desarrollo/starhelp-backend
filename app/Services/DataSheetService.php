<?php


namespace App\Services;

//Entities
use App\Entities\Document;
use App\Entities\DocumentInformation;
use App\Entities\Line;
use App\Entities\DataSheet;
use App\Entities\DataSheetLine;
use App\Entities\Parameter;

//Repositories
use App\Repositories\LineRepository;
use App\Repositories\DataSheetRepository;

//Otros
use Carbon\Carbon;
use Validator;

/**
 * Este controlador permite definir
 * los métodos necesarios para aplicar
 * la logica de negocio relacionada a
 * la tabla de documentos.
 *
 * @package App\Services
 * @author Kevin Galindo
 */
class DataSheetService
{
    protected $dataSheetRepository;

    /**
     * Constructor.
     *
     */
    public function __construct(DataSheetRepository $dataSheetRepo)
    {
        $this->dataSheetRepository = $dataSheetRepo;
    }

    public function createInternalName($name)
    {
        $charactersNotAllowed = ['/', '°', '-', '(', ')', ' ', "á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã", "ÃŠ", "ÃŽ", "Ã", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã", "Ã‹", "Ñ", "*", "%"];
        $internalName = trim($name);
        $internalName = str_replace($charactersNotAllowed, '_', $internalName);
        $internalName = preg_replace('(_+)', '_', $internalName);
        return trim(strtoupper($internalName), '_');
    }

    public function cleanData($data)
    {
        $data['visible_name'] = strtoupper(preg_replace('( +)', ' ', $data['visible_name']));
        $data['tooltip'] = preg_replace('( +)', ' ', $data['tooltip']);
        return $data;
    }

    public function validateRequest($data)
    {
        //Validamos la informacion
        $v = Validator::make($data, [
            'visible_name' => 'required',
            'type_value' => 'required',
        ]);

        //En caso de no cumplir con las el "Validator" se retorna un mensaje de error.
        if ($v->fails()) return $v->errors();
    }

    public function createDataSheet($data)
    {
        //Validamos los datos de la request
        $resRequest = $this->validateRequest($data);
        if (!empty($resRequest)) return ['message' => 'Por favor, rellene todos los campos.', 'code' => '400'];

        //Limpiamos la informacion
        $data = $this->cleanData($data);

        //Creamos el nombre interno para el DataSheet
        $data['internal_name'] = $this->createInternalName($data['visible_name']);

        //Validamos si ese nombre existe
        $existInternalName =  count($this->dataSheetRepository->findForInternalName($data['internal_name']));
        if ($existInternalName != 0) {
            $res = [
                'message' => 'El este dato ya existe', 
                'code' => '400',
                'internal_name' => $data['internal_name']
            ];
        } else {
            $dataSheet = DataSheet::create($data);
            $res = [
                'message' => 'Creado.',
                'code' => '201',
                'data' => $dataSheet,
                'internal_name' => $data['internal_name']
            ];
        }

        return $res;
    }

    public function updateDataSheet($data, $id)
    {
        //Buscamos el registro
        $dataSheet = $this->dataSheetRepository->findForId($id);

        //VAlidamos si existe
        if (!$dataSheet) return ['message' => 'No se encontro el registro', 'code' => '400'];

        //Actualizamos
        $dataSheet->update($data);

        //Retornamos respuesta
        return ['message' => 'Actualizado correctamente.', 'code' => '200'];
    }

    /**
     * Este metodo se encarga de  cargar 
     * los atributos de forma masiva 
     * 
     * @author Kevin Galindo
     */
    public function loadNameDataSheetMasive($request)
    {
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());

        // Creamos los atributos
        foreach ($fileData as $key => $item) {
            $parameter = Parameter::where('name_parameter', $item['TIPO'])->first();
            $line = Line::where('line_description', $item['LINEA'])->first();
 
            $data = [
                'visible_name' => $item['NOMBRE'],
                'type_value' => $parameter->id,
                'tooltip' => ''
            ];

            
            $res = $this->createDataSheet($data);

            $dataSheet = DataSheet::where('internal_name', $res['internal_name'])->first();


            DataSheetLine::updateOrCreate([
                'data_sheet_id' => $dataSheet->id,
                'line_id' => $line->id
            ], [
                'data_sheet_id' => $dataSheet->id,
                'line_id' => $line->id
            ]);
        }

        // Rerlacionamos la linea
        //dd("ok");
    }

        /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @return array
     * @param string $filename
     * @param string $delimiter
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 1) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }
}
