<?php

namespace App\Services;

use App\Repositories\ContactVehicleRepository;

/**
 * Class ContactVehicleService permite definir métodos para realizar la lógica de negocio
 * correspondiente al modelo ContactVehicle
 * @package App\Services
 * @author Jhon García
 */
class ContactVehicleService extends BaseService
{
    /**
     * ContactVehicleService constructor.
     * @param ContactVehicleRepository $contactVehicleRepository
     * @author Jhon García
     */
    public function __construct(ContactVehicleRepository $contactVehicleRepository)
    {
        $this->repository = $contactVehicleRepository;
    }
}
