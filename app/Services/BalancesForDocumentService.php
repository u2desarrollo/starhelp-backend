<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Entities\Account;
use App\Entities\Document;
use App\Entities\DocumentTransactions;
use App\Entities\VouchersType;
use App\Repositories\AccountRepository;
use App\Repositories\ParameterRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class BalancesForDocumentService
{
    private $accountRepository;
    private $parameterRepository;

    public function __construct(AccountRepository $accountRepository ,ParameterRepository $parameterRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->parameterRepository = $parameterRepository;;
    }

    /**
     * @author Santiago Torres
     */
    public function setAcoounts()
    {
        try {
            
            DB::beginTransaction();
            // convertimops la información del archivo a array
            $csv_info = $this->csvToArray(storage_path() . '/app/public/PLAN-CTAS.csv');
            // $data = [];
            foreach ($csv_info as $key => $info) {
                // si el nombre está vacio ponemos el código
                $name                       = trim($info["Nombre"]) ? trim($info["Nombre"]) :trim($info["Cod-Cta"]);
                // validamos si es debito o credito
                $natur                      = $this->validateNatur(trim($info["Natur"]));
                // seteamos el grupo
                $group                      = $this->parameterRepository->getParameter('C-001', trim($info["Grupo"]));
                if ($group == NULL) {
                    throw new \Exception("No se ha encontrado el grupo ". trim($info["Grupo"]));
                }
                // seteamos el nivel
                $level                      = $this->parameterRepository->getParameter('C-002', trim($info["Nivel"]));
                // seteamos la categoria
                $category                   = $this->parameterRepository->getParameter('C-003', trim($info["Categ"]) ? trim($info["Categ"]) : null );
                //  verificamos si maneja saldos tercero
                $manage_contact_balances    = trim($info["Man-Ter"]) == "SI" ? true : false;
                // maneja documento
                $manage_document_balances   = trim($info["Man-Doc"]) == "SI" ? true : false;
                // validamos si hay account_totalize sino ponemos la misma cuenta
                $account_id_totalize        = trim($info["Cta-Acum"]) ? Account :: where('code', trim($info["Cta-Acum"]))->first() : null;
                // seteamos tipo cartera 
                $wallet_type                = $this->parameterRepository->getParameter('C-004', trim($info["Tipo-Car"]) ? trim($info["Tipo-Car"]) : null );
                // validamos si maneja saldos por documento
                $quota_balances             = $this->validatequota_balances(trim($info["Man-Cuot"]));
                // centros de costo
                $cost_center_1              = $this->parameterRepository->getParameter('C-010', trim($info["Cen-cos1"]) ? trim($info["Cen-cos1"]) : null );
                $cost_center_2              = $this->parameterRepository->getParameter('C-011', trim($info["Cen-cos2"]) ? trim($info["Cen-cos2"]) : null );
                $cost_center_3              = $this->parameterRepository->getParameter('C-012', trim($info["Cen-cos3"]) ? trim($info["Cen-cos3"]) : null );

                // fecha apertura
                $account_created_at = substr($info["F-apert"], 0, 4).'-'.substr($info["F-apert"], 4, 2).'-'.substr($info["F-apert"], 6, 2).' 00:00:00';

                // seteamos los valores que van a ir a la base de datos
                $data = [
                    'code'                      => trim($info["Cod-Cta"]),
                    'name'                      => utf8_encode($name),
                    'short_name'                => trim($info["Nombre-Corto"]),
                    'description'               => trim($info["Descripcion"]),
                    'debit_credit'              => $natur,
                    'group'                     => $group,
                    'level'                     => $level,
                    'account_id_totalize'       => !is_null($account_id_totalize) ? $account_id_totalize->id : null, // buscar el id
                    'category'                  => $category,
                    'branchoffice_id'           => null, // revisar
                    'contact_id'                => null, // revisar
                    'manage_contact_balances'   => $manage_contact_balances,
                    'is_customer'               => null,
                    'is_provider'               => null,
                    'is_employee'               => null,
                    'resume_beginning_year'     => trim($info["Resu-Ter"]) == "SI" ? true : false,
                    'wallet_type'               => $wallet_type,
                    'document_description'      => null,
                    'manage_quota_balances'     => $quota_balances,
                    'cost_center_1'             => $cost_center_1,
                    'cost_center_2'             => $cost_center_2,
                    'cost_center_3'             => $cost_center_3,
                    'manage_businesses'         => trim($info["Negocios"])          ? true : false,
                    'manage_businesses_stage'   => trim($info["Etapas"])            ? true : false,
                    'manage_bank_document'      => trim($info["Concil"]) == 'SI'    ? true : false,
                    'manage_way_to_pay'         => trim($info["Form-Pag"]) == 'SI'  ? true : false,
                    'tax_account'               => null,
                    'Active_account'            => true,
                    'account_created_at'        => $account_created_at,
                    'account_cancellation_date' => null,
                    'pevious_account'           => trim($info["Cta-Anter"]) ? trim($info["Cta-Anter"]) : null,
                    'manage_document_balances'  => $manage_document_balances,
                ];
                $validate_account = Account :: where('code', $data["code"])->first();
                if (is_null($validate_account)) {
                    $account = $this->accountRepository->createAccount($data);
                }else{
                    $account = $this->accountRepository->updateAccount($validate_account->id, $data);
                }
            }
            
            DB :: commit();
            return $account;

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function validatequota_balances($quota_balances)
    {
        if ($quota_balances != 'SI' && $quota_balances != 'NO') {
            return null;
        }
        if ($quota_balances != 'SI') {
            return true;
        }elseif ($quota_balances != 'NO') {
            return false;
        }
    }

    public function setmanage_contact_balances($manage_contact_balances)
    {
        if ($manage_contact_balances) {
            return true;
        }else{
            return false;
        }
    }

    public function validateNatur($info_natur)
    {
        if ($info_natur != "D" && $info_natur != "C") {
            throw new \Exception("la naturaleza debe ser 'D' ó 'C'");
        }
        if ($info_natur == "D") {
            $natur = 1;
        }elseif ($info_natur == "C") {
            $natur = 2;
        }
        return $natur;
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @return array
     * @param string $filename
     * @param string $delimiter
     * @author Santiago Torres
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
                    $row = preg_replace('!\s+!', '',  $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    public function createAccount($request)
    {
        try {
            // if (!$this->accountRepository->validateCode($request->code)) {
            //     throw new \Exception("El código " .$request->code. " ya existe");
            // }
            $this->accountRepository->validateCode($request->code);
            DB::beginTransaction();
            $data = $this->setDataUpdateOrCreate($request);
            $new_account = $this->accountRepository->createAccount($data);
            DB :: commit();
            return (object) [
                'code' => '201',
                'completed' => true,
                'data' => (object) [
                    'data' => $new_account,
                    'message' => 'Cuenta Creada Exitosamente '
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function updateAccount($id, $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->setDataUpdateOrCreate($request);
            $data['user_id_last_modification']=$request->user_id;
            $account = $this->accountRepository->updateAccount($id, $data);
            DB :: commit();
            return (object) [
                'code' => '200',
                'completed' => true,
                'data' => (object) [
                    'data' => $account,
                    'message' => 'Cuenta Actualizada Exitosamente '
                ]
            ];
            // return $account;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la creacion de cuentas: ' . $e->getMessage() ."\n"
                ]
            ];
        }
    }

    public function setDataUpdateOrCreate($request)
    {
        return $data = [
            'code'                      => $request->code,
            'name'                      => $request->name,
            'short_name'                => $request->short_name,
            'description'               => $request->description,
            'debit_credit'              => $request->debit_credit,
            'group'                     => $request->group,
            'level'                     => $request->level,
            'account_totalize'          => $request->account_totalize,
            'category'                  => $request->category,
            'branchoffice_id'           => $request->branchoffice_id,
            'contact_id'                => $request->contact_id,
            'manage_contact_balances'   => $request->manage_contact_balances != null ? ($request->manage_contact_balances == 1 ? true : false) : false , // bool,
            'is_customer'               => $request->is_customer != null ? ($request->is_customer == 1 ? true : false): false , // bool,
            'is_provider'               => $request->is_provider != null ? ($request->is_provider == 1 ? true : false): false , // bool,
            'is_employee'               => $request->is_employee != null ? ($request->is_employee == 1 ? true : false): false , // bool,
            'resume_beginning_year'     => $request->resume_beginning_year != null ? ($request->resume_beginning_year == 1 ? true : false): false , // bool,
            'wallet_type'               => $request->wallet_type,
            'document_description'      => $request->document_description,
            'quota_balances'            => $request->quota_balances != null ? ($request->quota_balances == 1 ? true : false): false , // bool,
            'cost_center_1'             => $request->cost_center_1,
            'cost_center_2'             => $request->cost_center_2,
            'cost_center_3'             => $request->cost_center_3,
            'manage_businesses'         => $request->manage_businesses != null ? ($request->manage_businesses == 1 ? true : false): false , // bool,
            'manage_businesses_stage'   => $request->manage_businesses_stage != null ? ($request->manage_businesses_stage == 1 ? true : false): false , // bool,
            'manage_bank_document'      => $request->manage_bank_document != null ? ($request->manage_bank_document == 1 ? true : false): false ,  // bool,
            'manage_way_to_pay'         => $request->manage_way_to_pay != null ? ($request->manage_way_to_pay == 1 ? true : false): false , // bool,
            'tax_account'               => $request->tax_account,
            'Active_account'            => $request->Active_account != null ? ($request->Active_account == 1 ? true : false): false , // bool,
            'pevious_account'           => $request->pevious_account,
        ];
    }
    
    public function deleteAccount($id)
    {
        return $this->accountRepository->deleteAccount($id);
    }

    public function createDocumentByCrossBalance($request)
    {
        // Validar si un documento ya existe 
        $user = JWTAuth::parseToken()->toUser();


        $documentExists = Document::where('user_id', $user->id)
        ->where('model_id', $request->model_id)
        ->where('vouchertype_id', $request->voucherType_id)
        ->where('branchoffice_id', $request->branchoffice_id)
        ->where('contact_id', $request->contact_id)
        ->where('warehouse_id', $request->contact_warehouse_id)
        ->where('in_progress', true)
        ->first();


        // Si existe se retorna
        if ($documentExists) {
            return $this->getDocumentCrossBalance($documentExists->id);
        }

        // Si no se crea y retorna

        $voucherType = VouchersType::find($request->voucherType_id);

        $documentData = [
            'user_id' => $user->id,
            'model_id' => $request->model_id,
            'vouchertype_id' => $request->voucherType_id,
            'branchoffice_id' => $request->branchoffice_id,
            'contact_id' => $request->contact_id,
            'warehouse_id' => $request->contact_warehouse_id,
            'in_progress' => true,
            'consecutive' => 0,
            'prefix' => $voucherType->prefix,
            'document_date' =>  Carbon::parse(date('Y-m-d H:i:s')),
            'issue_date' =>  Carbon::parse(date('Y-m-d H:i:s')),
            'module' => 'crucesaldos'
        ];

        $document = Document::create($documentData);
        
        return $this->getDocumentCrossBalance($document->id);
    }

    public function getDocumentCrossBalance($id)
    {
        return Document::with([
            'document_transactions' => function ($query) {
                $query->with([
                    'account',
                    'document',
                    'transactionReference' => function ($query) {
                        $query->with([
                            'document',
                        ]);
                    }
                ]);
            }
        ])
        ->find($id);
    }

    public function createTransactionsByDocumentCrossBalance($request)
    {

        try {
            DB::beginTransaction();
            $document = Document::find($request->document_id);
            $balancesFavor = (object) $request->balances_favor;
            $detail = $request->detail;
            $balancesContact = collect((object) $request->balances_contact);
            $optionalSequence = $document->document_transactions->max('optional_sequence') + 1;

            $balancesFavorOperationValue = $balancesContact->sum('cross_value');

            // Creamos el balance a favor (D)
            $dataSave = [
                'year_month'      => date('Ym'),
                'document_id'     => $document->id,
                'branchoffice_id' => $document->branchoffice_id,
                'vouchertype_id'  => $document->vouchertype_id,
                'account_id'      => $balancesFavor->account_id,
                'contact_id'      => $document->contact_id,
                'document_date'   => $document->document_date,
                'seller_id'       => $document->seller_id,
                'issue_date'      => $document->issue_date,
                'due_date'        => $document->due_date,
                'operation_value' => $balancesFavorOperationValue,
                'transactions_type' => 'D',
                'transaction_reference_id' => $balancesFavor->id,
                'optional_sequence' => $optionalSequence,
                'detail' => $detail
            ];

            DocumentTransactions::create($dataSave);
            
            // Recorremos los balance (C)
            foreach ($balancesContact as $key => $balance) {
                $dataSave = [
                    'year_month'      => date('Ym'),
                    'document_id'     => $document->id,
                    'branchoffice_id' => $document->branchoffice_id,
                    'vouchertype_id'  => $document->vouchertype_id,
                    'account_id'      => $balance['account_id'],
                    'contact_id'      => $document->contact_id,
                    'document_date'   => $document->document_date,
                    'seller_id'       => $document->seller_id,
                    'issue_date'      => $document->issue_date,
                    'due_date'        => $document->due_date,
                    'operation_value' => $balance['cross_value'],
                    'transactions_type' => 'C',
                    'transaction_reference_id' => $balance['id'],
                    'optional_sequence' => $optionalSequence,
                    'detail' => $detail
                ];

                DocumentTransactions::create($dataSave);
            }

            DB::commit();

            return [
                'code' => 201,
                'data' => $this->getDocumentCrossBalance($request->document_id)
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 500,
                'data' => 'Error al crear la transaccion'
            ];
        }
    }

    public function deleteDocumentTransactionsBySequence($request)
    {
        return DocumentTransactions::where('document_id', $request->document_id)->where('optional_sequence', $request->optional_sequence)->delete(); 
    }

    public function calculateBalanceContact($request, $balancesForDocuments)
    {
        // Traemos los documentos que tienen transacciones con el tercero y la cuenta relacionados.
        $documentTransactions = DocumentTransactions::with([
            'document'
        ])
        ->where([
            'account_id' => $request->account_id,
            'contact_id' => $request->contact_id,
        ])
        ->whereHas('document', function ($query) {
            $query->where('in_progress', true);
            $query->where('module', 'crucesaldos');
        })
        ->get();

        // traemos los balance que hayan tenido una transaccion
        $transactionReferencesId = $documentTransactions->pluck('transaction_reference_id');
        $balancesForDocumentsMatch = collect($balancesForDocuments)->whereIn('id', $transactionReferencesId);

        // Recorremos y modificamos los valores de los balance
        foreach ($balancesForDocumentsMatch as $key => $balance) {
            // Calculamos el valor
            $transactions = $documentTransactions->where('transaction_reference_id', $balance['id']);            

            $totalFinalBalance = $balance['final_balance'];

            foreach ($transactions as $keyTransaction => $transaction) {
                $transactionsType = $transaction->transactions_type;

                if ($transactionsType === 'D') {
                    $totalFinalBalance += $transaction->operation_value;
                } else {
                    $totalFinalBalance -= $transaction->operation_value;
                }                
            }

            $balance['final_balance'] = $totalFinalBalance;

            // Agregamos al array
            $keyArray = array_search(
                $balance['id'],
                array_column($balancesForDocuments, 'id')
            );

            // Agregamos el producto al array.
            if ($keyArray !== false) {
                $balancesForDocuments[$keyArray] = $balance;
            }
        }

        return collect($balancesForDocuments)->where('final_balance', '<>', '0')->values();
    }

    /**
     * Este metodo se encarga de validar si un usuario ya esta creando
     * un documento de cruce a un tercero en especifico.
     * 
     * @author Kevin Galindo 
     */
    public function checkDocumentInitiatedByAnotherUser($request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $document = Document::with([
            'user' => function ($query) {
                $query->with([
                    'contact'
                ]);
            }
        ])
        ->where([
            'in_progress' => true,
            'module' => 'crucesaldos',
            'contact_id' => $request->contact_id
        ])
        ->whereHas('document_transactions')
        ->where('user_id', '!=', $user->id)
        ->first();

        return $document;
    }

}
