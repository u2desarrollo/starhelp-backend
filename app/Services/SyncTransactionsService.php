<?php


namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SyncTransactionsService extends Service
{

    private $transaction;
    private $payment;
    private $cash_receipt_app;


    public function __construct()
    {

        parent::__construct();

        $this->code_branchoffice = 4;
        $this->code_voucher_type = 5;
        $this->prefix = 6;
        $this->consecutive = 7;
        $this->transaction_type = 9;
        $this->document_date = 13;
        $this->issue_date = 13;
        $this->due_date = 14;
        $this->identification_seller = 15;
        $this->name_seller = 16;
        $this->debits = 17;
        $this->credits = 18;
        $this->observation = 19;
        $this->detail = 19;
        $this->description = 19;
        $this->account_code_puc = 20;
        $this->payment_applied_erp = 21;
        $this->erp_id_document = 22;
        $this->contra_key = 23;
        $this->erp_id_transaction = 24;
        $this->document_number = 12;
    }

    public function syncData($nameFile)
    {
        //abro mi archivo
        if (is_null($nameFile)) {
            $file = fopen(storage_path('app/public/portfolio/TRAERP-20200804-113020.csv'), "r");
        } else {
            $file = fopen(storage_path('app/public/portfolio/' . $nameFile), "r");
        }

        Log::info('//--------------- Procesando archivo de transaciones: ' . $nameFile);
        // la variable &cant realiza un conteo con el motivo de saber en que momento ya vienen datos diferentes al encabezado de mi archivo
        $cant = 0;
        $cantProcesados = 0;

        try {
            DB::beginTransaction();
            DB::table('documents_transactions')->update(['update_or_create' => 0]);

            // Recorremos todas las lineas del archivo hasta la ultima
            while (!feof($file)) {
                // Leyendo una linea
                $line = fgets($file);
                //sacando un array apartir de la linea extraida del archivo
                $this->array = explode(";", utf8_encode($line));
                //el archivo puede venir con una linea  vacia con la funcion count puedo saber si la linea actual tiene datos
                if ($cant > 0 && (count($this->array) > 1)) {
                    //lo usaba para el registro del documento de tipo pago
                    $this->cleanSpaces();
                    $this->branchofficeTreatment();
                    $this->voucherTratment();

                    // si no encuentra factura no hace nada
                    $this->getInvoiceTypeDocument();
                    if ($this->document) {
                        $this->sellersTreatment();
                        $this->thirdPartyTreatment();
                        $this->contactWarehouseTreatment();
                        //     // si llega el campo $[9] como diferente de vacio o nulo es credito
                        $this->transactionTratment();
                        if ($this->array[$this->transaction_type] != '') {
                            $this->cashReceiptAppTreatment();
                        }
                        $cantProcesados++;
                    }
                }
                $cant++;
            }

            if ($cantProcesados > 0) {
                DB::table('documents_transactions')->where(['update_or_create' => 0])->delete();
            }

            DB::commit();
            Log::info("//--------------- Archivo de transacciones $nameFile procesado con exito. Total registros: $cant");
        } catch (\Exception $th) {
            Log::error("//--------------- Error procesando archivo de transacciones: " . json_encode($th->getLine()) . $th->getMessage());
            DB::rollBack();
        } finally {
            // Cerrando el archivo
            fclose($file);
            //elimino el archivo del storage de mi app
            if (!is_null($nameFile)) {
                Storage::disk('local')->delete('public/portfolio/' . $nameFile);
            }
        }
    }

    public function cashReceiptAppTreatment()
    {
        $this->cash_receipt_app = DB::table('cash_receipts_app')
            ->where('id', $this->array[$this->erp_id_transaction])
            ->update([
                'payment_applied_erp' => $this->array[$this->payment_applied_erp]
            ]);
    }


    // este metodo tiene un parametro opcional el cual determina
    // si debo de regidstrar valores como debito o credito
    public function transactionTratment()
    {

        if ($this->array[$this->transaction_type] != '') {
            if ($this->array[$this->payment_applied_erp] == 0) {
                $erp_id = $this->array[$this->erp_id_transaction];
            } else {
                $erp_id = $this->array[$this->contra_key];
            }
        } else {
            $erp_id = $this->array[$this->contra_key];
        }

        if ($this->array[$this->transaction_type] == 'APP' || $this->array[$this->transaction_type] == '') {
            $voucher = DB::table('voucherstypes')->where('code_voucher_type', 310)->first();
        } else {
            $voucher = DB::table('voucherstypes')->where('code_voucher_type', $this->array[$this->transaction_type])->first();
        }

        // creo el objeto y seteo sus valores de acuerdo al tipo de transaccion que se desea registrar
        // ya sea una transaccion de tipo debito ó credito
        $transaction = [
            'document_id' => $this->document->id,
            'year_month' => $this->transformYearMonth($this->array[$this->year_month]),
            'quota_number' => 01,
            'base_value' => 0,
            'account_code_puc' => $this->array[$this->account_code_puc],
            'payment_applied_erp' => $this->array[$this->payment_applied_erp],
            'document_number' => $this->document->consecutive,
            'document_prefix' => $this->document->prefix,
            'document_date' => $this->array[$this->issue_date],
            'term_days_document' => $this->document->term_days,
            'expiration_date_document' => $this->document->due_date,
            'detail' => $this->array[$this->detail],
            'description' => $this->array[$this->detail],
            'transaction_value' => $this->array[$this->transaction_type] != '' ? (int) $this->array[$this->credits] : (int) $this->array[$this->debits],
            'transaction_type' => $this->array[$this->transaction_type] != '' ? 'C' : 'D',
            'erp_id' => $erp_id,
            'update_or_create' => 1,
            'voucher_type' => null

        ];


        if ($this->array[$this->transaction_type] != '' && $this->array[$this->payment_applied_erp] == 0) {
            $transaction['document_number'] = (int) $this->array[$this->erp_id_transaction];
            $transaction['voucher_type'] = $voucher->name_voucher_type . ' - APP ';
        } elseif ($this->array[$this->transaction_type] != '' && $this->array[$this->payment_applied_erp] == 1) {
            $transaction['document_number'] = (int) $this->array[$this->document_number];
            $transaction['voucher_type'] = $voucher->name_voucher_type;
        }
        if ($this->array[$this->transaction_type] == '442') {
            $transaction['voucher_type'] = $voucher->name_voucher_type;
        }
        if ($this->array[$this->transaction_type] == '' && $this->array[$this->code_voucher_type] == 444) {
            $transaction['voucher_type'] = $this->array[$this->detail];
            $transaction['document_number'] = (int) $this->array[$this->document_number];
        }

        // determino si registro un valor credito o debito
        // el debito se actualiza pero el credito se debe de crear

        DB::table('documents_transactions')
            ->updateOrInsert(Arr::only($transaction, ['document_id', 'erp_id']), $transaction);
    }


    public function getInvoiceTypeDocument()
    {

        $this->document = DB::table('documents')
            ->where([
                'consecutive' => $this->array[$this->consecutive],
                'vouchertype_id' => $this->voucher_type->id,
                'branchoffice_id' => $this->branchoffice->id,
                'prefix' => $this->array[$this->prefix],
                'operationType_id' => 5,
                'deleted_at' => null
            ])
            ->first();
    }
}
