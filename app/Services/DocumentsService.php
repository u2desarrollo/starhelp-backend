<?php


namespace App\Services;

use App\ElectronicInvoice\Processor;
use App\Entities\Account;
use App\Entities\ServiceRequestStatusHistory;
use App\Entities\DocumentFile;
use App\Entities\DocumentPaymentMethod;
use App\Entities\User;
use App\Entities\Product;
use App\Entities\Document;
use App\Entities\DocumentInformation;
use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\DocumentProductLot;
use App\Entities\Template;
use App\Entities\Line;
use App\Entities\Inventory;
use App\Entities\Pdf;
use App\Entities\ContactWarehouse;
use App\Entities\DocumentTransactions;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\VouchersType;
use App\Entities\ProductPromotion;
use App\Entities\Promotion;
use App\Entities\Parameter;
use App\Entities\Price;
use App\Entities\Subscriber;
use App\Entities\DocumentsBalance;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\BranchOffice;
use App\Entities\CashReceiptsApp;
use App\Entities\FirmDate;
use App\Events\NewDocumentToAuthorize;
use App\Jobs\SendElectronicInvoiceJob;
use App\Jobs\UpdateBalancesJob;
use App\Mail\NotificationOrderEmail;
use App\Mail\RetainAuthorizedDocumentsMail;
use App\Repositories\PriceRepository;
use App\Repositories\DocumentProductRepository;
use App\Repositories\DocumentRepository;
use App\Repositories\InventoryRepository;
use App\Repositories\TypesOperationRepository;
use App\Repositories\DocumentTransactionsRepository;
use App\Traits\CacheTrait;
use App\Traits\CollectionsTrait;
use App\Traits\GeneratePdfTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Storage;
use App\Repositories\VouchersTypeRepository;
use Illuminate\Pagination\Paginator;

//Otros
use App\Services\DocumentsProductsService;
use Tymon\JWTAuth\Facades\JWTAuth;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Validator;

use DateTime;
use Spatie\ArrayToXml\ArrayToXml;

// Service
use App\Services\DocumentsBalanceService;
use App\Services\ProductService;
use App\Services\InventoryService;
use stdClass;
use SoapClient;

/**
 * Class DocumentsService, permite definir los métodos necesarios para aplicar la logica de negocio relacionada a la tabla de documentos
 *
 * @package App\Services
 */
class DocumentsService
{
    use GeneratePdfTrait;
    use CollectionsTrait;
    use CacheTrait;

    private $documentsProductsService;
    private $documentAccountingService;
    private $documentRepository;
    private $documentProductRepository;
    private $inventoryRepository;
    private $documentsBalanceService;
    private $priceRepository;
    private $inventoryService;
    private $documentTransactionsRepository;
    private $balance_service;
    private $vouchersTypeRepository;
    protected $pdf;
    protected $productService;

    /**
     * DocumentsService constructor.
     *
     * @param Pdf $pdf
     * @param DocumentRepository $documentRepository
     * @param DocumentProductRepository $documentProductRepository
     * @param InventoryRepository $inventoryRepository
     * @param \App\Services\DocumentsProductsService|null $documentsProductsService
     * @param DocumentAccountingService|null $documentAccountingService
     * @param \App\Services\ProductService|null $productService
     * @param \App\Services\DocumentsBalanceService|null $documentsBalanceService
     * @param PriceRepository $priceRepo
     * @param VouchersTypeRepository $vouchersTypeRepository
     * @param \App\Services\InventoryService $inventoryService
     * @param DocumentTransactionsRepository $documentTransactionsRepository
     * @param BalancesService $balance_service
     */
    public function __construct(Pdf $pdf,
                                DocumentRepository $documentRepository,
                                DocumentProductRepository $documentProductRepository,
                                InventoryRepository $inventoryRepository,
                                DocumentsProductsService $documentsProductsService = null,
                                DocumentAccountingService $documentAccountingService = null,
                                ProductService $productService = null,
                                DocumentsBalanceService $documentsBalanceService = null,
                                PriceRepository $priceRepo,
                                VouchersTypeRepository $vouchersTypeRepository,
                                InventoryService $inventoryService,
                                DocumentTransactionsRepository $documentTransactionsRepository,
                                BalancesService $balance_service
    )
    {
        $this->documentRepository = $documentRepository;
        $this->documentProductRepository = $documentProductRepository;
        $this->inventoryRepository = $inventoryRepository;
        $this->pdf = $pdf;
        $this->productService = $productService;
        $this->priceRepository = $priceRepo;

        $this->documentsProductsService = $documentsProductsService;
        $this->documentsBalanceService = $documentsBalanceService;
        $this->inventoryService = $inventoryService;
        $this->vouchersTypeRepository = $vouchersTypeRepository;
        $this->documentTransactionsRepository = $documentTransactionsRepository;
        $this->documentAccountingService = $documentAccountingService;
        $this->balance_service = $balance_service;
    }

    /**
     * Este metodo se encarga de asignar el inventario
     * del producto segun los parametros.
     *
     * @author Kevin Galindo
     */
    public function addInventoryDocumentProducts($document, $request)
    {
        // Optenemos la bodega actual.
        $branchoffice_warehouse_id = $document['branchoffice_warehouse_id'];

        // Buscamos y guardamos el resultado.
        $inventories = Inventory::whereIn('product_id', Arr::pluck($document['documents_products'], 'product.id'))->get();

        // Recorremos los productos.
        foreach ($document['documents_products'] as $key => $documentProduct) {

            // Validamos y asignamos la sede del producto en el documento.
            $dp_branchoffice_warehouse_id = $documentProduct['branchoffice_warehouse_id'] ? $documentProduct['branchoffice_warehouse_id'] : $branchoffice_warehouse_id;

            // Buscamos y guardamos el resultado.
            $inventory = $inventories->where('product_id', $documentProduct['product']['id'])
                ->where('branchoffice_warehouse_id', $dp_branchoffice_warehouse_id)
                ->first();

            $document['documents_products'][$key]['product']['inventory'] = $inventory->toArray();
        }

        // Retornamos el documento.
        return $document;
    }

    /**
     * metodo para eliminar los documentos que no de sistema anterior
     * corregir ka
     *
     * @author Santiago Torres
     */
    public function deleteNotPreviousDocuments()
    {
        try {
            DB::beginTransaction();
            $documents = $this->documentRepository->getNotPreviousDocuments();
            $documents_verify = [];
            foreach ($documents as $key => $document) {

                if (!$this->documentRepository->validateAffectsDocumentId($document->id)) {
                    foreach ($document->documentsProducts as $key => $documentProduct) {
                        //eliminamos impuestos
                        $documentProduct->documentsProductsTaxes()->forceDelete();
                    }

                    //eliminamos Productos
                    $document->documentsProducts()->forceDelete();

                    //eliminamos archivos
                    $document->files()->forceDelete();

                    //eliminamos transacciones
                    $document->accounting_transactions()->forceDelete();

                    //eliminamos document information
                    $document->documentsInformation()->forceDelete();

                    //eliminamos el documento
                    $document->forceDelete();
                } else {
                    $documents_verify[] = $document->id;
                }
            }
            return [
                'documentos que no son previos eliminados adecuadamente',
                $documents_verify
            ];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            return 'error al eliminar documentos previos';

        }

    }

    /**
     * Método utilizado para la finalizacion de un documento
     *
     * @param $request
     * @return mixed
     */
    public function finalizeDocument($request, $fromMassiveoppening = false)
    {
        set_time_limit(0);

        // Seteamos la zona horaria
        date_default_timezone_set('America/Bogota');

        $consecutive = $request->consecutive;
        $user = auth()->user();
        $user_id = $user->id;
        $userAuth = false;

        // Traemos el modelo
        $model = $this->getModelForIdFromCache($request->model_id);

        if ($model->code != '102') {
            // Validamos el modelo para la sede y bodega #CAMBIOMODELOPORDEFECTO
            if ($model) {
                if (!empty($model->branchoffice_id)) {
                    $request->merge(['branchoffice_id' => $model->branchoffice_id]);
                }

                if (!empty($model->branchoffice_warehouse_id)) {
                    $branch_office_warehouse = $this->getBranchOfficeWarehouse($request->branchoffice_id, $model->branchOfficeWarehouse->warehouse_code);
                    if ($branch_office_warehouse) {
                        $request->merge(['branchoffice_warehouse_id' => $branch_office_warehouse->id]);
                    }
                }
            }
        }

        // Diferencias
        $documentProductsDifferences = $request->documentProductsDifferences;

        $document = Document::with([
                'documentsProducts',
                'contact',
                'vouchertype',
                'documentsInformation',
                'paymentMethods',
                'template'
            ])
            ->find($request->document_id);

        if (empty($document)) {
            return (object)[
                'code' => '404',
                'data' => (object)[
                    'message' => 'Documento no encontrado'
                ]
            ];
        }

        // Validamos que los productos tengan cantidades para facturar.
        $productWithQuantity = $document->documentsProducts->where('quantity', '>', 0);
        if ($model->credit_note != true && $productWithQuantity->count() <= 0) {
            return (object)[
                'code' => '400',
                'data' => (object)[
                    'message' => 'Los productos no tienen cantidades a facturar.'
                ]
            ];
        }

        // Validamos la existencia
        if ($validateExistenceProduct = $this->validateExistenceProduct($document, $model)) {
            return $validateExistenceProduct;
        }

        $documentProductsError = $this->validateMinimalPriceDocumentProducts($document, $model);
        if (count($documentProductsError) > 0) {
            return (object)[
                'code' => '400',
                'data' => (object)[
                    'message' => 'Los siguientes productos no cumplen con el precio minimo (' . $model->minimalPriceParam->name_parameter . ') establecido:',
                    'data'    => $documentProductsError,
                    'type'    => 'minimalprice',
                ]
            ];
        }

        try {
            DB::beginTransaction();

            $document->in_progress = false;

            $this->documentValidateDocumentProducts($model, $document);
            $this->documentPurchaseDifference($model, $documentProductsDifferences);

            $document = $this->documentSetDueDate($document, $fromMassiveoppening);

            // Validar Auhtorizaciones
            if ($model->authorizedUsers->count() > 0) {
                $userAuth = $model->authorizedUsers->where('user_id', $user_id)->first();

                if (empty($userAuth)) {
                    $document->in_progress = true;
                    $document->pending_authorization = true;
                    $document->user_id_solicit_authorization = $user_id;
                    $document->date_solicit_authorization = date('Y-m-d H:i:s');
                    $consecutive = 0;
                } else {

                    if ((boolean)$request->authorize_documents === true) {
                        $document->user_id_authorization = $user_id;
                    }

                    $document->pending_authorization = false;
                    $userAuth = false;
                }
            }

            // Validamos el manejo del consecutivo segun el modelo
            if ($model->consecutive_handling === 1) {
                $vouchersType = VouchersType::where('code_voucher_type', $document->vouchertype->code_voucher_type)
                    ->where('branchoffice_id', $document->branchoffice_id)
                    ->lockForUpdate()
                    ->first();
            } else {
                $vouchersType = VouchersType::where('code_voucher_type', $document->vouchertype->code_voucher_type)
                    ->whereHas('branchoffice', function ($query) {
                        $query->where('main', true);
                    })
                    ->lockForUpdate()
                    ->first();
            }

            $document = $this->documentStateEending($document, $model, $request);
            $document = $this->documentSetConsecutive($document, $vouchersType, $consecutive, $userAuth);
            $document = $this->documentSetAdditionalInformation($document, $vouchersType, $model, $request);
            $document = $this->documentRepository->finalizeDocument($document);
            $document = $this->documentRepository->finalizeDocumentInformation($document, $request);
            $document = $this->documentSetInventoryQuantitiesAndValues($document, $userAuth);

            $this->saveFilesDocuments($request, $document->id);
            $this->saveDocumentPaymentsMethods($request, $document);

            $document = $this->electronicInvoice($document, $userAuth);

            if (isset($request->show_values_pdf) && is_bool($request->show_values_pdf)) {
                $show_values = $request->show_values_pdf;
            } else {
                $show_values = true;
            }

            $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document, $show_values);

            $document->update([
                'invoice_link' => $file_name
            ]);

            // ---- CALCULAR DISPONIBILIDAD

            DB::commit();

            $this->setReturnedDocument($document->id);
            $this->sendElectronicInvoice($document, $userAuth);

            $return_document = $this->documentRepository->getDocument($document->id, $request);
            $return_document->filename = $file_name;

            // Actualizar el estado y el backorder del docuemnto.
            $this->updateBackOrderAndMissingValueDocument($document->id);
            $stateRes = $this->documentUpdateState($document->id);

            $this->closeDocumentForReturnReason($return_document, $model, $user);

            if (!$stateRes) {

                // Actualizar valores
                Document::find($request->document_id)->update([
                    'in_progress' => true,
                ]);

                return (object)[
                    'code'      => '500',
                    'completed' => false,
                    'data'      => (object)[
                        'message' => 'Error al actualizar el estado.'
                    ]
                ];
            }

            $dataReturn = [
                'code'      => '200',
                'completed' => false,
                'data'      => $return_document
            ];


            if ($userAuth !== false) {
                $dataReturn['data']['type'] = 'solicit_authorization';
            }

            if ($model->authorizedUsers->count() > 0) {
                event(new NewDocumentToAuthorize());
            }

            // Contabilizar
            $this->documentAccountingService->accountingInventoryOperationsByDocument(Document::find($document->id));

            return (object)$dataReturn;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            Log::info($e);

            // Validamos nuevamente la existencia.
            $productNotStock = [];
            if ($model->operationType->affectation == 2) {
                foreach ($document->documentsProducts as $dp) {

                    if ($dp->product->manage_inventory) {
                        $stockProduct = $this->getStockProduct($dp->product->id, $dp->branchoffice_warehouse_id);

                        if ($dp->quantity > $stockProduct) {
                            $productNotStock[] = [
                                'document_product_id' => $dp->id,
                                'product_id'          => $dp->product->id,
                                'stock'               => $stockProduct
                            ];
                        }
                    }

                }
            }

            if (count($productNotStock) > 0) {
                return (object)[
                    'code' => '404',
                    'data' => (object)[
                        'message' => 'Existencia no Disponible.',
                        'data'    => $productNotStock,
                        'type'    => 'stock',
                    ]
                ];
            }

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() . "\n" . $e->getLine() . "\n" . $e->getFile()
                ]
            ];
        }

    }

    /**
     * Este metodo se encarga de guardar
     * los metodos de pago del documento
     *
     * @author Kevin Galindo
     */
    public function saveDocumentPaymentsMethods($request, $document)
    {

        if (is_string($request->paymentMethods)) {
            $paymentMethods = json_decode($request->paymentMethods, true);
        } else {
            $paymentMethods = $request->paymentMethods;
        }

        if ($paymentMethods) {
            $itemsSave = [];
            foreach ($paymentMethods as $key => $item) {

                if ($item['payment_method_id']) {

                    if (empty($item['value_change'])) {
                        $item['value_change'] = 0;
                    }

                    if (isset($item['new']) && $item['new']) {
                        $itemsSave[] = Arr::except($item, ['id', 'document_id', 'payment_method', 'new']);
                    } else {
                        $itemsSave[] = Arr::except($item, ['document_id', 'payment_method', 'new']);
                    }
                }

            }

            $document->paymentMethods()->sync($itemsSave);
        }

        return $document;
    }

    public function documentValidateDocumentProducts($model, $document)
    {
        if ($model->credit_note) {
            DB::update('update documents_products
                set quantity = 0
                where document_id = ?', [$document->id]);
            // foreach ($document->documentsProducts as $dp) {
            //     $dp->quantity = 0;
            //     $dp->save();
            // }
        } else {
            DB::delete('delete from documents_products where quantity <= 0 and document_id = ?', [$document->id]);
            // foreach ($document->documentsProducts as $dp) {
            //     if ($dp->quantity <= 0) {
            //         $this->documentProductRepository->delete($dp->id);
            //     }
            // }
        }
    }

    public function documentSetInventoryQuantitiesAndValues($document, $userAuth)
    {
        foreach ($document->documentsProducts->sortByDesc('id') as $dp) {
            // Si la existencia es mayor a 0 actualiza, si no elimina el registro
            if ($userAuth === false) {
                $this->inventoryRepository->finalize($dp->id);
            }
            if ($dp->quantity > 0) {
                $this->priceRepository->updatePriceModel($dp->id);
            }
        }

        return $document;
    }

    /**
     * @param $document
     * @param $vouchersType
     * @param $model
     * @param $request
     * @return mixed
     */
    public function documentSetAdditionalInformation($document, $vouchersType, $model, $request)
    {
        $document->invoice_link = "$vouchersType->name_voucher_type No. $document->consecutive";
        $document->operationType_id = $model ? $model->operationType->id : null;
        $document->observation = $request->observation;
        $document->prefix = $vouchersType->prefix;
        $document->model_id = $model ? $model->id : null;
        $document->vouchertype_id = $vouchersType->id;

        $document->contact_id = $request->contact_id;
        $document->warehouse_id = $request->warehouse_id;
        $document->import_number = $request->import_number ? $request->import_number : null;
        $document->seller_id = $request->seller_id ? $request->seller_id : null;
        $document->handling_other_parameters_value = $model->handling_other_parameters_state != 2 ? $request->handling_other_parameters_value : null;
        $document->projects_id = $request->projects_id && $request->projects_id != 'null' ? $request->projects_id : $document->projects_id;

        $document->affects_document_id = $document->id;
        $document->affects_prefix_document = $document->prefix;
        $document->affects_number_document = $document->consecutive;
        $document->affects_date_document = $document->document_date;

        if (!$document->origin_document && $document->id !== $document->thread && !is_null($document->thread)) {
            $document->affects_document_id = $document->document->id;
            $document->affects_prefix_document = $document->document->prefix;
            $document->affects_number_document = $document->document->consecutive;
            $document->affects_date_document = $document->document->document_date;
        }

        if ($request->warehouse_id) {
            $document->warehouse_id = $request->warehouse_id;
        }

        $document->import_number = $request->import_number ? $request->import_number : null;
        $document->seller_id = $request->seller_id ? $request->seller_id : null;
        $document->handling_other_parameters_value = $model && $model->handling_other_parameters_state != 2 ? $request->handling_other_parameters_value : null;

        // Validamos el estado de finalizacion
        $document = $this->documentStateEending($document, $model, $request);

        return $document;
    }

    /**
     * Cerrar documento base dependiendo de el motivo de devolución
     * Funcionamiento exclusivo para Colsaisa
     *
     * @param Document $document Contiene el documento finalizado
     * @param Template $model Contiene el modelo del documento
     * @param User $user Contiene el usuario autenticado
     */
    public function closeDocumentForReturnReason($document, $model, $user)
    {
        // Validar si el suscriptor de el usuario actual es Colsaisa
        if ($user->subscriber_id == 1) {

            // Códigos de modelos que son devolución
            $devolution_codes = [
                '155',
                '270',
                '225'
            ];

            // Validar si el documento es una devolución
            // Validar si el modelo maneja otros parámetros, (Motivo de devolución)
            if (in_array($model->operationType->code, $devolution_codes) && $model->handling_other_parameters_value != 2) {

                // Validar el parámetro enviado
                if (!in_array($document->handling_other_parameters_value, [98, 968])) {

                    // Validar que el id del documento sea diferente al thread
                    if ($document->document->id != $document->document->thread) {

                        // Si cumple la condición se cierra la orden de venta
                        $document->document->document->closed = true;
                        $document->document->document->save();
                    }
                }
            }
        }
    }

    /**
     * @param $document
     * @param $vouchersType
     * @param $consecutive
     * @param $userAuth
     * @return mixed
     */
    public function documentSetConsecutive($document, $vouchersType, $consecutive, $userAuth)
    {
        if ($userAuth === false) {
            if (!$consecutive) {

                $consecutive = 0;

                if ($vouchersType) {

                    // $consecutive = $vouchersType->consecutive_number++;
                    $consecutive = $vouchersType->consecutive_number + 1;
                    $exists = true;

                    // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
                    while ($exists) {
                        $aux = Document::where('consecutive', $consecutive)
                            ->where('vouchertype_id', $vouchersType->id)
                            ->where('in_progress', false)
                            ->where('prefix', $vouchersType->prefix)
                            ->exists();
                        if ($aux) {
                            ++$consecutive;
                        } else {
                            $exists = false;
                        }
                    }

                    // Actualizamos el consecutivo del tipo de comprobante.
                    $vouchersType->consecutive_number = $consecutive;
                    $vouchersType->save();
                }
            }
        }

        $document->consecutive = $consecutive;

        return $document;
    }

    /**
     * Asignar el estado de finalización al documento si el modelo maneja estado de guardado o finalizado.
     *
     * @param $document
     * @param $model
     * @param $request
     * @return mixed
     * @author Kevin Galindo
     */
    public function documentStateEending($document, $model, $request)
    {
        if ($model && $model->document_save_finish) {
            $codeTable = "02";

            if ($request->save_document == 'true') {
                $codeTable = "01";
            }

            $parameter = Parameter::where('code_parameter', $codeTable)
                ->whereHas('paramtable', function ($query) {
                    $query->where('code_table', '062');
                })->first();

            $document->state_ending_id = $parameter->id;
        }

        return $document;
    }

    /**
     * Establecer la fecha de emisión y la fecha de vencimiento de un documento
     *
     * @param Document $document Contiene la información del documento
     * @return mixed
     */
    public function documentSetDueDate($document, $fromMassiveoppening = false)
    {
        if (!$fromMassiveoppening) {
            $document->document_date = date('Y-m-d H:i:s');
        }
        $document_date = Carbon::parse(date('Y-m-d'));
        $expiration_days = 0;

        if (!empty($document->contact->pay_days)) {
            $expiration_days = $document->contact->pay_days;
        }

        $document->due_date = $document_date->addDays($expiration_days)->toDateTimeString();

        return $document;
    }

    public function documentPurchaseDifference($model, $documentProductsDifferences)
    {
        if ($model->purchase_difference_management) {
            if ($documentProductsDifferences) {
                foreach ($documentProductsDifferences as $key => $item) {

                    $item = json_decode($item, true);

                    $dataUpdate = [
                        'type_difference'     => $item['type_difference'],
                        'difference_quantity' => $item['difference_quantity'],
                        'difference_value'    => $item['difference_value']
                    ];

                    DocumentProduct::find($item['id'])->update($dataUpdate);
                }
            }
        }
    }

    /**
     * Este metodo se encarga de calcular el
     * valor faltante en cantidades y valor total a pagar.
     *
     * @author Kevin Galindo
     */
    public function updateBackOrderAndMissingValueDocument($document_id)
    {
        $documentOriginal = Document::find($document_id);
        $documentsToAffect = [];
        $whileStop = false;

        $actualDoc = $documentOriginal;
        $documentsToAffect[] = $documentOriginal;

        while (!$whileStop) {

            if (is_null($actualDoc->document) || $actualDoc->document->consecutive == 0) {
                $whileStop = true;
            } else {
                $actualDoc = $actualDoc->document;
                $documentsToAffect[] = $actualDoc;

                if ($actualDoc->id === $actualDoc->thread) {
                    $whileStop = true;
                }
            }

        }

        $modelIdDoc = $documentsToAffect[0]->model_id;

        foreach ($documentsToAffect as $key => $document) {


            $backorderTotal = 0;
            $backorderValueTotal = 0;

            foreach ($document->documentsProducts as $key => $dp) {
                $backorder = $this->documentsProductsService->calculateBackOrder($dp->id, [
                    'model_id'    => $modelIdDoc,
                    'document_id' => $document->id,
                ]);

                // if ($document->id == 50553) {
                //     dd($backorder);
                // }

                if ($backorder !== false) {
                    $dp->backorder_quantity = $backorder['backorder'];
                    $dp->backorder_value = $dp->quantity != 0 ? $dp->total_value / $dp->quantity * $dp->backorder_quantity : 0;
                    $dp->save();

                    $backorderTotal += $backorder['backorder'];
                    $backorderValueTotal += $dp->backorder_value;
                }
            }


            $document->backorder_quantity = $backorderTotal;
            $document->backorder_value = $backorderValueTotal;
            $document->backorder_updated = true;
            $document->save();

            $modelIdDoc = $document->model_id;

        }

        return $document;
    }

    public function updateBackOrderAllDocuments($params)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $documents =
            Document::where('in_progress', false)
                // ->where('id', 46439)->get();
                ->where(function ($query) {
                    $query->where('canceled', false);
                    $query->orWhereNull('canceled');
                })
                ->when(isset($params['dateMonth']) && !empty($params['dateMonth']), function ($query) use ($params) {

                    // primer dia
                    $fecha = new DateTime($params['dateMonth']);

                    $fecha->modify('first day of this month');
                    $startDate = $fecha->format('Y-m-d');

                    $fecha->modify('last day of this month');
                    $endDate = $fecha->format('Y-m-d');

                    $query->whereBetween('document_date', [$startDate, $endDate]);
                })
                ->where('closed', false)
                ->where('consecutive', '>', 0)
                ->where('model_id', "!=", null)
                ->get();

        echo "CANTIDAD DE DOCUEMNTOS - " . $documents->count() . "\n";

        foreach ($documents as $key => $document) {

            echo $document->id . " - " . count($documents) . "/" . ($key + 1) . "\n";

            if (!Document::select('backorder_updated')->find($document->id)->backorder_updated) {
                $this->updateBackOrderAndMissingValueDocument($document->id);
            }

        }

        // echo "\n----------- FIN -----------";
        return true;
    }

    /**
     * Este metodo se encarga de cargar los
     * archivos adjuntados al documento.
     *
     * @author Kevin Galindo
     */
    public function saveFilesDocuments($request, $document_id)
    {
        if ($request->filesUpload) {
            foreach ($request->filesUpload as $key => $file) {
                $nameOriginal = $file->getClientOriginalName();
                $extension = \File::extension($nameOriginal);
                $size = $file->getSize();

                $path = Storage::disk('public')->put('files-documents', $file);

                DocumentFile::create([
                    'name'        => $nameOriginal,
                    'extension'   => $extension,
                    'path'        => $path,
                    'size'        => $size,
                    'sent'        => false,
                    'document_id' => $document_id
                ]);
            }
        }

        // Eliminar archivos.
        if ($request->filesDelete) {
            foreach ($request->filesDelete as $key => $file) {
                $data = json_decode($file);
                DocumentFile::where('document_id', $document_id)
                    ->where("name", $data->name)
                    ->where("size", $data->size)
                    ->delete();
            }
        }
    }

    /**
     * Este metodo se encarga de actualizar
     * la informacion general del documento.
     *
     * @author Kevin Galindo
     */
    public function updateGeneralDataDocument($id, $request)
    {
        // Traemos la informacion que se va a actualizar
        $dataUpdate = Arr::except($request->all(), [
            '_method',
            'token',
            'id'
        ]);

        // Traemos el documento
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            return [
                'code' => 404,
                'data' => 'El documento con el id {' . $id . '} no existe.'
            ];
        }

        // Validamos las retenciones
        if (
            $dataUpdate['ret_fue_total'] != $document->ret_fue_total ||
            $dataUpdate['ret_iva_total'] != $document->ret_iva_total ||
            $dataUpdate['ret_ica_total'] != $document->ret_ica_total
        ) {
            $dataUpdate['edit_retention'] = true;
        }

        if (isset($dataUpdate['seller_id']) && $dataUpdate['seller_id'] == 'null') {
            $dataUpdate['seller_id'] = null;
        }

        if (isset($dataUpdate['observation']) && $dataUpdate['observation'] == 'null') {
            $dataUpdate['observation'] = null;
        }

        $document = $this->documentRepository->update($dataUpdate, $id);

        $this->saveFilesDocuments($request, $document->id);

        // Generar el pdf.
        $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document, true);

        Document::find($document->id)->update([
            'invoice_link' => $file_name
        ]);

        $document->filename = $file_name;

        return [
            'data' => $document,
            'code' => 200
        ];
    }

    /**
     * Este metodo se utiliza para calcular y
     * actualizar los valores del documento, ya sean
     * totales o retenciones.
     *
     * @author Kevin Galindo
     */
    public function updateDocumentValues($request)
    {
//        DB::connection()->enableQueryLog();

        ## VALIDACIONES ##

        // validamos que el id del documento venga en el "$request"
        if (!$request->document_id) {
            return [
                'data' => [
                    'message' => 'el ID del documento el obligatorio.'
                ],
                'code' => 400
            ];
        }

        // validamos que el model_id venga en el "$request"
        if (!$request->model_id) {
            return [
                'data' => [
                    'message' => 'el ID del modelo el obligatorio.'
                ],
                'code' => 400
            ];
        }

        ## VARIABLES ##

        // Traemos el documento.
        $document = Document::where('id', $request->document_id)
            ->with(
                [
                    'documentsProducts.documentProductTaxes.tax',
                    'document'
                ]
            )
            ->first();

        // Valores del documento.
        $totalBrutDoc = 0;
        $totalDiscountDoc = 0;
        $totalDoc = 0;
        $totalTaxIvaDoc = 0;
        $taxesDoc = [];

        $trmTotalBrutDoc = 0;
        $trmTotalDiscountDoc = 0;
        $trmTotalDoc = 0;
        $trmTotalTaxIvaDoc = 0;

        $model = $this->getModelForIdFromCache($request->model_id);

        // Validar si el documento es una salida o una entrada.
        $sale = $model->operationType->affectation == 2;

        // Validamos si maneja TRM
        $manageTrm = !empty($model->TRM_value) && $model->TRM_value != 2;

        ## CALCULAMOS EL TOTAL BRUTO Y EL VALOR TOTAL DEL DOCUMENTO ##

        // Recorremos los productos del documento.
        foreach ($document->documentsProducts as $key => $documentProduct) {

            // Validamos si el documento maneja TRM
            if ($manageTrm) {
                $trmTotalDoc += $documentProduct->total_value_us;
                $trmTotalBrutDoc += $documentProduct->total_value_brut_us;
                $trmTotalDiscountDoc += $documentProduct->discount_value_us;
            }

            // Sumar el valor total bruto y total de cada producto.
            $totalTaxIvaDoc += $documentProduct->documentProductTaxes->where('tax_id', 1)->sum('value');
            $totalBrutDoc += $documentProduct->total_value_brut;
            $totalDoc += $documentProduct->total_value;
            $totalDiscountDoc += $documentProduct->discount_value;
        }

        // Asignamos los totales del documento.
        $document->total_value_brut = $manageTrm ? $totalBrutDoc : round($totalBrutDoc);
        $document->total_value = $manageTrm ? $totalDoc : round($totalDoc);
        $document->discount_value = $manageTrm ? $totalDiscountDoc : round($totalDiscountDoc);

        //trm
        $document->total_value_us = $trmTotalDoc;
        $document->total_value_brut_us = $trmTotalBrutDoc;
        $document->discount_value_us = $trmTotalDiscountDoc;

        $document->save();

        ## CALCULAMOS LAS RETENCIONES ##
        $request->document = $document;
        $request->model = $model;
        $res = $this->updateDocumentValuesRetention($document->id, $request);
        $document = $res['data'];

        $document = $this->documentRepository->electronicFacRounding($document, $model, $request->document_products_edit_iva);

        $taxesDoc = [];
        foreach ($document->documentsProducts as $key => $documentProduct) {

            // Sumamos los impuestos
            foreach ($documentProduct->documentProductTaxes as $key => $documentProductTax) {
                // Validamos si el tax ya existe en el arreglo.
                $key_tax = array_search(
                    $documentProductTax->tax_id,
                    array_column($taxesDoc, 'tax_id')
                );

                // Crear
                if ($key_tax === false) {
                    $taxesDoc[] = [
                        'tax_id'      => $documentProductTax->tax_id,
                        'value'       => $documentProductTax->value,
                        'percentage'  => $sale ? $documentProductTax->tax->percentage_purchase : $documentProductTax->tax->percentage_sale,
                        'base_value'  => $documentProductTax->base_value,
                        'description' => $documentProductTax->tax->description
                    ];
                } // Actualizar
                else {
                    $taxesDoc[$key_tax]['value'] += $documentProductTax->value;
                }
            }
        }

        ## INPUESTOS DEL DOCUMENTO ##
        $document['taxes'] = $taxesDoc;
//        Log::info(count(DB::getQueryLog()));

        return [
            'data' => $document,
            'code' => 200
        ];
    }

    /**
     * Esta funcion se encarga de calcular
     * las retenciones en el documentos.
     *
     * @author Kevin Galindo
     */
    public function updateDocumentValuesRetention($document_id, $request)
    {

        // Validar si el documento es un devolucion.
        $isDevolution = false;

        // Codigos de tipos de operaciones que son devoluciones.
        $codeDevolution = [
            '155',
            '270',
            '225'
        ];

        // Traemos el modelo.
        $model = $this->getModelForIdFromCache($request->model_id);

        $document = isset($request->document) && !empty($request->document)
            ? $request->document
            : Document::find($document_id);

        $documentUpdate = $document;

        // Validamos si el modelo maneja retenciones. si no maneja se pone en 0 todos los valores.
        if ($model->retention_management === 0 || is_null($model->retention_management)) {

            $documentUpdate->update([
                # FUE
                // productos
                'ret_fue_prod_vr_limit'   => 0,
                'ret_fue_prod_percentage' => 0,
                'ret_fue_prod_total'      => 0,
                'ret_fue_base_value'      => 0,

                // servicios
                'ret_fue_serv_vr_limit'   => 0,
                'ret_fue_serv_percentage' => 0,
                'ret_fue_serv_total'      => 0,
                'ret_fue_serv_base_value' => 0,

                // total
                'ret_fue_total'           => 0,

                # IVA
                // productos
                'ret_iva_prod_vr_limit'   => 0,
                'ret_iva_prod_percentage' => 0,
                'ret_iva_prod_total'      => 0,
                'ret_iva_base_value'      => 0,

                // servicios
                'ret_iva_serv_vr_limit'   => 0,
                'ret_iva_serv_percentage' => 0,
                'ret_iva_serv_total'      => 0,
                'ret_iva_serv_base_value' => 0,

                // total
                'ret_iva_total'           => 0,

                # ICA
                // productos
                'ret_ica_prod_vr_limit'   => 0,
                'ret_ica_prod_percentage' => 0,
                'ret_ica_prod_total'      => 0,
                'ret_ica_base_value'      => 0,

                // servicios
                'ret_ica_serv_vr_limit'   => 0,
                'ret_ica_serv_percentage' => 0,
                'ret_ica_serv_total'      => 0,
                'ret_ica_serv_base_value' => 0,

                // total
                'ret_ica_total'           => 0,
            ]);

            return [
                'data' => $document,
                'code' => 200
            ];
        }

        // Validar la lista de precio, en caso de ser '002' los valores se ponen en 0.
        if ($model->priceList && $model->priceList->code_parameter == '002') {
            $documentUpdate->update([
                'total_value'             => 0,
                'total_value_brut'        => 0,
                'ret_fue_prod_vr_limit'   => 0,
                'ret_fue_prod_percentage' => 0,
                'ret_fue_prod_total'      => 0,
                'ret_fue_serv_vr_limit'   => 0,
                'ret_fue_serv_percentage' => 0,
                'ret_fue_serv_total'      => 0,
                'ret_fue_total'           => 0,
                'ret_iva_prod_vr_limit'   => 0,
                'ret_iva_prod_percentage' => 0,
                'ret_iva_prod_total'      => 0,
                'ret_iva_serv_vr_limit'   => 0,
                'ret_iva_serv_percentage' => 0,
                'ret_iva_serv_total'      => 0,
                'ret_iva_total'           => 0,
                'ret_ica_prod_vr_limit'   => 0,
                'ret_ica_prod_percentage' => 0,
                'ret_ica_prod_total'      => 0,
                'ret_ica_serv_vr_limit'   => 0,
                'ret_ica_serv_percentage' => 0,
                'ret_ica_serv_total'      => 0,
                'ret_ica_total'           => 0,
            ]);

            return [
                'data' => $document,
                'code' => 200
            ];
        }

        // Actualizamos las bases de las retenciones.
        $res = $this->updateDocumentBasesRetention($document_id, $request);

        // Validamos el cambio de retencion
        if ($request->edit_retention) {
            $document->edit_retention = true;
            $document->ret_fue_total = $request->ret_fue_total ? $request->ret_fue_total : 0;
            $document->ret_iva_total = $request->ret_iva_total ? $request->ret_iva_total : 0;
            $document->ret_ica_total = $request->ret_ica_total ? $request->ret_ica_total : 0;
            $document->save();

            return [
                'data' => $document,
                'code' => 200
            ];
        }

        // Traemos la sucursal.
        $contactWarehouse = ContactWarehouse::find($request->warehouse_id);

        # Validamos si el documento es una devolucion.
        $isDevolution = in_array($model->operationType->code, $codeDevolution);

        # Validamos si el bruto del documento es igual a su documento anterior se dejan las mismas retenciones.
        if ($isDevolution) {
            if ((float)$document->total_value_brut === (float)$document->document->total_value_brut) {
                $data = $this->updateDocumentValuesRetentionByThread($document_id, $request);
                return [
                    'data' => $data['data'],
                    'code' => 200
                ];
            }
        }

        # Actualizamos vr limite y porcentajes.
        switch ($model->contact_type) {
            case 1:
                $document = $this->setConfigRetentionToDocument($document, $isDevolution, $contactWarehouse, $model);
                break;
            case 2:
                $document = $this->setConfigRetentionToDocumentProvider($document, $isDevolution, $contactWarehouse, $model);
                break;
        }

        // Actualizamos
        $document->save();

        $this->calculateValuesOfRetention($document, $isDevolution);

        return [
            'data' => $document,
            'code' => 200
        ];

    }

    public function setConfigRetentionToDocument($document, $isDevolution, $contactWarehouse, $model)
    {
        # Actualizamos vr limite y porcentajes.
        // FUE (productos)
        $document->ret_fue_prod_vr_limit = $isDevolution ? $document->document->ret_fue_prod_vr_limit : $contactWarehouse->ret_fue_prod_vr_limit;
        $document->ret_fue_prod_percentage = $isDevolution ? $document->document->ret_fue_prod_percentage : $contactWarehouse->ret_fue_prod_percentage;

        // FUE (servicios)
        $document->ret_fue_serv_vr_limit = $isDevolution ? $document->document->ret_fue_serv_vr_limit : $contactWarehouse->ret_fue_serv_vr_limit;
        $document->ret_fue_serv_percentage = $isDevolution ? $document->document->ret_fue_serv_percentage : $contactWarehouse->ret_fue_serv_percentage;

        // IVA (productos)
        $document->ret_iva_prod_vr_limit = $isDevolution ? $document->document->ret_iva_prod_vr_limit : $contactWarehouse->ret_iva_prod_vr_limit;
        $document->ret_iva_prod_percentage = $isDevolution ? $document->document->ret_iva_prod_percentage : $contactWarehouse->ret_iva_prod_percentage;

        // IVA (servicios)
        $document->ret_iva_serv_vr_limit = $isDevolution ? $document->document->ret_iva_serv_vr_limit : $contactWarehouse->ret_iva_serv_vr_limit;
        $document->ret_iva_serv_percentage = $isDevolution ? $document->document->ret_iva_serv_percentage : $contactWarehouse->ret_iva_serv_percentage;

        // ICA (productos)
        $document->ret_ica_prod_vr_limit = $isDevolution ? $document->document->ret_ica_prod_vr_limit : $contactWarehouse->ret_ica_prod_vr_limit;
        $document->ret_ica_prod_percentage = $isDevolution ? $document->document->ret_ica_prod_percentage : $contactWarehouse->ret_ica_prod_percentage;

        // IVA (servicios)
        $document->ret_ica_serv_vr_limit = $isDevolution ? $document->document->ret_ica_serv_vr_limit : $contactWarehouse->ret_ica_serv_vr_limit;
        $document->ret_ica_serv_percentage = $isDevolution ? $document->document->ret_ica_serv_percentage : $contactWarehouse->ret_ica_serv_percentage;


        return $document;
    }

    public function setConfigRetentionToDocumentProvider($document, $isDevolution, $contactWarehouse, $model)
    {
        # Actualizamos vr limite y porcentajes.
        // FUE (productos)
        $document->ret_fue_prod_vr_limit = $isDevolution ? $document->document->ret_fue_prod_vr_limit : $contactWarehouse->ret_fue_prod_vr_limit_provider;
        $document->ret_fue_prod_percentage = $isDevolution ? $document->document->ret_fue_prod_percentage : $contactWarehouse->ret_fue_prod_percentage_provider;

        // FUE (servicios)
        $document->ret_fue_serv_vr_limit = $isDevolution ? $document->document->ret_fue_serv_vr_limit : $contactWarehouse->ret_fue_serv_vr_limit_provider;
        $document->ret_fue_serv_percentage = $isDevolution ? $document->document->ret_fue_serv_percentage : $contactWarehouse->ret_fue_serv_percentage_provider;

        // IVA (productos)
        $document->ret_iva_prod_vr_limit = $isDevolution ? $document->document->ret_iva_prod_vr_limit : $contactWarehouse->ret_iva_prod_vr_limit_provider;
        $document->ret_iva_prod_percentage = $isDevolution ? $document->document->ret_iva_prod_percentage : $contactWarehouse->ret_iva_prod_percentage_provider;

        // IVA (servicios)
        $document->ret_iva_serv_vr_limit = $isDevolution ? $document->document->ret_iva_serv_vr_limit : $contactWarehouse->ret_iva_serv_vr_limit_provider;
        $document->ret_iva_serv_percentage = $isDevolution ? $document->document->ret_iva_serv_percentage : $contactWarehouse->ret_iva_serv_percentage_provider;

        // ICA (productos)
        $document->ret_ica_prod_vr_limit = $isDevolution ? $document->document->ret_ica_prod_vr_limit : $contactWarehouse->ret_ica_prod_vr_limit_provider;
        $document->ret_ica_prod_percentage = $isDevolution ? $document->document->ret_ica_prod_percentage : $contactWarehouse->ret_ica_prod_percentage_provider;

        // IVA (servicios)
        $document->ret_ica_serv_vr_limit = $isDevolution ? $document->document->ret_ica_serv_vr_limit : $contactWarehouse->ret_ica_serv_vr_limit_provider;
        $document->ret_ica_serv_percentage = $isDevolution ? $document->document->ret_ica_serv_percentage : $contactWarehouse->ret_ica_serv_percentage_provider;


        return $document;
    }

    /**
     * Este metodo se encarga de
     * validar los valores de las retenciones
     * de forma parcial o no.
     *
     * @author Kevin Galindo
     */
    public function calculateValuesOfRetention(Document $document, $isDevolution)
    {
        // Si el documento es una devolucion se calcula de forma parcial.
        if ($isDevolution) {
            # FUE
            if ((float)$document->document->ret_fue_prod_total > 0) {
                $document->ret_fue_prod_total = round(((float)$document->ret_fue_base_value * (float)$document->ret_fue_prod_percentage) / 100);
            } else {
                $document->ret_fue_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_fue_serv_total > 0) {
                $document->ret_fue_serv_total = round(((float)$document->ret_fue_serv_base_value * (float)$document->ret_fue_serv_percentage) / 100);
            } else {
                $document->ret_fue_serv_total = 0;
            }

            // totales
            $document->ret_fue_total = $document->ret_fue_prod_total + $document->ret_fue_serv_total;

            # ICA
            // productos
            if ((float)$document->document->ret_ica_prod_total > 0) {
                $document->ret_ica_prod_total = round(((float)$document->ret_ica_base_value * (float)$document->ret_ica_prod_percentage) / 100);
            } else {
                $document->ret_ica_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_ica_serv_total > 0) {
                $document->ret_ica_serv_total = round(((float)$document->ret_ica_serv_base_value * (float)$document->ret_ica_serv_percentage) / 100);
            } else {
                $document->ret_ica_serv_total = 0;
            }

            // totales
            $document->ret_ica_total = $document->ret_ica_prod_total + $document->ret_ica_serv_total;

            # IVA
            // productos
            if ((float)$document->document->ret_iva_prod_total > 0) {
                $document->ret_iva_prod_total = round(((float)$document->ret_iva_base_value * (float)$document->ret_iva_prod_percentage) / 100);
            } else {
                $document->ret_iva_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_iva_serv_total > 0) {
                $document->ret_iva_serv_total = round(((float)$document->ret_iva_serv_base_value * (float)$document->ret_iva_serv_percentage) / 100);
            } else {
                $document->ret_iva_serv_total = 0;
            }

            // totales
            $document->ret_iva_total = $document->ret_iva_prod_total + $document->ret_iva_serv_total;
        } // Se calcula de forma NO parcial.
        else {
            # FUE
            // productos
            if ((float)$document->ret_fue_base_value > 0 && (float)$document->ret_fue_base_value >= (float)$document->ret_fue_prod_vr_limit) {
                $document->ret_fue_prod_total = round(((float)$document->ret_fue_base_value * (float)$document->ret_fue_prod_percentage) / 100);
            } else {
                $document->ret_fue_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_fue_serv_base_value > 0 && (float)$document->ret_fue_serv_base_value >= (float)$document->ret_fue_serv_vr_limit) {
                $document->ret_fue_serv_total = round(((float)$document->ret_fue_serv_base_value * (float)$document->ret_fue_serv_percentage) / 100);
            } else {
                $document->ret_fue_serv_total = 0;
            }

            // totales
            $document->ret_fue_total = $document->ret_fue_prod_total + $document->ret_fue_serv_total;

            # ICA
            // productos
            if ((float)$document->ret_ica_base_value > 0 && (float)$document->ret_ica_base_value >= (float)$document->ret_ica_prod_vr_limit) {
                $document->ret_ica_prod_total = round(((float)$document->ret_ica_base_value * (float)$document->ret_ica_prod_percentage) / 100);
            } else {
                $document->ret_ica_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_ica_serv_base_value > 0 && (float)$document->ret_ica_serv_base_value >= (float)$document->ret_ica_serv_vr_limit) {
                $document->ret_ica_serv_total = round(((float)$document->ret_ica_serv_base_value * (float)$document->ret_ica_serv_percentage) / 100);
            } else {
                $document->ret_ica_serv_total = 0;
            }

            // totales
            $document->ret_ica_total = $document->ret_ica_prod_total + $document->ret_ica_serv_total;

            # IVA

            // productos
            if ((float)$document->ret_iva_base_value > 0 && (float)$document->ret_iva_base_value >= (float)$document->ret_iva_prod_vr_limit) {
                $document->ret_iva_prod_total = round(((float)$document->ret_iva_base_value * (float)$document->ret_iva_prod_percentage) / 100);
            } else {
                $document->ret_iva_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_iva_serv_base_value > 0 && (float)$document->ret_iva_serv_base_value >= (float)$document->ret_iva_serv_vr_limit) {
                $document->ret_iva_serv_total = round(((float)$document->ret_iva_serv_base_value * (float)$document->ret_iva_serv_percentage) / 100);
            } else {
                $document->ret_iva_serv_total = 0;
            }

            // totales
            $document->ret_iva_total = $document->ret_iva_prod_total + $document->ret_iva_serv_total;
        }

        $document->save();

        return $document;
    }

    /**
     * Esta funcion actualiza las retenciones
     * del documento segun su domento base.
     * Esta funcion es para las devoluciones.
     *
     * @param $document_id
     * @param $request
     * @return array
     * @author Kevin Galindo
     */
    public function updateDocumentValuesRetentionByThread($document_id, $request)
    {
        // Traemos el documento.
        $document = Document::find($request->document_id);

        // Traemos el documento thread
        $documentThread = $document->document;

        $document->update([
            # FUE
            // productos
            'ret_fue_prod_vr_limit'   => 0,
            'ret_fue_prod_percentage' => $documentThread->ret_fue_prod_percentage,
            'ret_fue_prod_total'      => $documentThread->ret_fue_prod_total,
            'ret_fue_base_value'      => $documentThread->ret_fue_base_value,

            // servicios
            'ret_fue_serv_vr_limit'   => 0,
            'ret_fue_serv_percentage' => $documentThread->ret_fue_serv_percentage,
            'ret_fue_serv_total'      => $documentThread->ret_fue_serv_total,
            'ret_fue_serv_base_value' => $documentThread->ret_fue_serv_base_value,

            // total
            'ret_fue_total'           => $documentThread->ret_fue_total,

            # IVA
            // productos
            'ret_iva_prod_vr_limit'   => 0,
            'ret_iva_prod_percentage' => $documentThread->ret_iva_prod_percentage,
            'ret_iva_prod_total'      => $documentThread->ret_iva_prod_total,
            'ret_iva_base_value'      => $documentThread->ret_iva_base_value,

            // servicios
            'ret_iva_serv_vr_limit'   => 0,
            'ret_iva_serv_percentage' => $documentThread->ret_iva_serv_percentage,
            'ret_iva_serv_total'      => $documentThread->ret_iva_serv_total,
            'ret_iva_serv_base_value' => $documentThread->ret_iva_serv_base_value,

            // total
            'ret_iva_total'           => $documentThread->ret_iva_total,

            # ICA
            // productos
            'ret_ica_prod_vr_limit'   => 0,
            'ret_ica_prod_percentage' => $documentThread->ret_ica_prod_percentage,
            'ret_ica_prod_total'      => $documentThread->ret_ica_prod_total,
            'ret_ica_base_value'      => $documentThread->ret_ica_base_value,

            // servicios
            'ret_ica_serv_vr_limit'   => 0,
            'ret_ica_serv_percentage' => $documentThread->ret_ica_serv_percentage,
            'ret_ica_serv_total'      => $documentThread->ret_ica_serv_total,
            'ret_ica_serv_base_value' => $documentThread->ret_ica_serv_base_value,

            // total
            'ret_ica_total'           => $documentThread->ret_ica_total,
        ]);

        return [
            'data' => Document::find($document_id),
            'code' => 200
        ];
    }

    /**
     * Esta funcion se encarga de calcular
     * las retenciones en el documentos.
     *
     * @author Kevin Galindo
     */
    public function updateDocumentBasesRetention($document_id, $request)
    {

        // Variables
        $totalBrutInProducts = 0;
        $totalBrutInServices = 0;
        $totalIvaInProducts = 0;
        $totalIvaInServices = 0;

        // Traemos el documento.
        $document = isset($request->document) && !empty($request->document)
            ? $request->document
            : Document::find($document_id);

        // Modificacion de las bases.
        if ($request->edit_base_retention) {

            # Actualizamos los valores

            // Productos
            if (
                $request->ret_fue_base_value &&
                $request->ret_ica_base_value &&
                $request->ret_iva_base_value
            ) {
                $document->ret_fue_base_value = $request->ret_fue_base_value;
                $document->ret_ica_base_value = $request->ret_ica_base_value;
                $document->ret_iva_base_value = $request->ret_iva_base_value;
            }

            // Servicios
            if (
                $request->ret_fue_serv_base_value &&
                $request->ret_ica_serv_base_value &&
                $request->ret_iva_serv_base_value
            ) {
                $document->ret_fue_serv_base_value = $request->ret_fue_serv_base_value;
                $document->ret_ica_serv_base_value = $request->ret_ica_serv_base_value;
                $document->ret_iva_serv_base_value = $request->ret_iva_serv_base_value;
            }

            $document->edit_base_retention = true;

        } else {

            // Recorremos los productos del documento.
            foreach ($document->documentsProducts as $key => $documentProduct) {

                # Validamos el tipo del producto.

                // Servicio.
                if ($documentProduct->line->tipe == 2) {
                    $totalBrutInServices += $documentProduct->total_value_brut;

                    # Sumamos el Iva del servicio
                    $totalIvaInServices += $documentProduct->documentProductTaxes->where('tax_id', 1)->sum('value');
                } // Producto.
                else {
                    $totalBrutInProducts += $documentProduct->total_value_brut;

                    # Sumamos el Iva del producto.
                    $totalIvaInProducts += $documentProduct->documentProductTaxes->where('tax_id', 1)->sum('value');
                }


            }

            # Actualizamos los valores

            // Productos
            $document->ret_fue_base_value = $totalBrutInProducts;
            $document->ret_ica_base_value = $totalBrutInProducts;
            $document->ret_iva_base_value = $totalIvaInProducts;

            // Servicios
            $document->ret_fue_serv_base_value = $totalBrutInServices;
            $document->ret_ica_serv_base_value = $totalBrutInServices;
            $document->ret_iva_serv_base_value = $totalIvaInServices;
        }

        $document->save();

        return [
            'data' => $document,
            'code' => 200
        ];

    }

    /**
     * Llama la emisión de la facturación electrónica
     *
     * @param $document
     * @return mixed
     * @throws \Exception
     */
    public function electronicInvoice($document, $userAuth)
    {
        if ($userAuth === false && config('app.enable_electronic_billing')) {
            if ($document->vouchertype->electronic_bill) {

                $validator = $document->operationType_id == 5; //VENTAS

                if (!$validator && $document->operationType_id == 1) { //DEVOLUCIÓN DE VENTAS
                    $documentThread = Document::where('id', $document->thread)->first();
                    if ($documentThread->documentsInformation->vouchers_type_electronic_bill) {
                        $validator = true;
                    }
                }

                if ($validator) {
                    $processor = new Processor($document);

                    $response_electronic_invoice = $processor->init();

                    $qr = isset($response_electronic_invoice['qr']) && !empty($response_electronic_invoice['qr']) ?
                        $response_electronic_invoice['qr'] : $response_electronic_invoice['cadenaCodigoQR'];

                    Document::find($document->id)->update([
                        'qr'   => $qr,
                        'cufe' => $response_electronic_invoice['cufe']
                    ]);

                    $document->qr = $response_electronic_invoice['qr'];
                    $document->cufe = $response_electronic_invoice['cufe'];
                }
            }
        }

        return $document;
    }

    /**
     * @param $document
     * @throws \Exception
     */
    public function sendElectronicInvoice($document, $userAuth)
    {
        if ($userAuth === false && config('app.enable_electronic_billing')) {
            if ($document->vouchertype->electronic_bill) {

                $validator = $document->operationType_id == 5; //VENTAS

                if (!$validator && $document->operationType_id == 1) { //DEVOLUCIÓN DE VENTAS
                    $documentThread = Document::where('id', $document->thread)->first();
                    if ($documentThread->documentsInformation->vouchers_type_electronic_bill) {
                        $validator = true;
                    }
                }

                if ($validator) {
                    SendElectronicInvoiceJob::dispatch($document);
                }
            }
        }
    }

    public function finalizeUpdateDocument($request)
    {

        // Traemos el documento Original y el que se actualizara
        $documentUpdate = Document::find($request->document['id']);
        $documentOriginal = Document::find($documentUpdate->ref_update_doc);
        $documentsThread = Document::where('thread', $documentOriginal->id)->get();

        // Otros valores para actualizar
        $observation = isset($request->document['observation']) ? $request->document['observation'] : null;

        $model = Template::find($request->model_id);

        $documentUpdate->thread = $documentOriginal->thread;
        $documentUpdate->save();

        $sendElectronicInvoice = $documentUpdate->document_date >= '2020-11-01 00:00:00' && !$documentUpdate->cufe;

        try {
            DB::beginTransaction();

            // Modificamos el campo in_progress
            $documentOriginal->in_progress = true;
            $documentOriginal->consecutive = 0;
            $documentOriginal->save();
            $documentOriginal->delete();

            $documentUpdate->in_progress = false;
            $documentUpdate->erp_id = null;
            $documentUpdate->user_id_last_modification = $request->user_id;
            $documentUpdate->date_last_modification = date('Y-m-d H:i:s');

            $documentUpdate->observation = $observation;

            $documentUpdate = $this->documentStateEending($documentUpdate, $model, $request);

            $documentUpdate->save();

            // Docuemntos relacionados
            foreach ($documentsThread as $key => $documentThread) {
                if ($documentUpdate->id === $documentThread->id) continue;
                $documentThread->thread = $documentUpdate->id;
                $documentThread->save();
            }

            // Reconstruir Documento
            $resultRebuildInventory = $this->inventoryService->rebuildDocument($documentUpdate->id);

            if ($resultRebuildInventory->code == '500') {
                return (object)[
                    'code' => '500',
                    'data' => (object)[
                        'message' => 'Error al recontruir el documento'
                    ]
                ];
            }

            if ($sendElectronicInvoice) {
                $document = $this->electronicInvoice($documentUpdate, false);
            }

            DB::commit();

            // Generar el pdf.

            $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $documentUpdate, true);

            Document::find($documentUpdate->id)->update([
                'invoice_link' => $file_name
            ]);

            $documentUpdate->filename = $file_name;

            if ($sendElectronicInvoice) {
                $this->sendElectronicInvoice($documentUpdate, false);
            }

            $this->saveDocumentPaymentsMethods($request, $documentUpdate);
            $this->updateBackOrderAndMissingValueDocument($documentUpdate->id);
            $stateRes = $this->documentUpdateState($documentUpdate->id);

            if (!$stateRes) {

                Document::find($documentUpdate->id)->update([
                    'in_progress' => true,
                ]);

                return (object)[
                    'code'      => '500',
                    'completed' => false,
                    'data'      => (object)[
                        'message' => 'Error al actualizar el estado.'
                    ]
                ];
            }

            return (object)[
                'code' => '200',
                'data' => [
                    'document'          => $documentUpdate,
                    'rebuild_inventory' => $resultRebuildInventory
                ]
            ];

        } catch (\Exception $e) {

            DB::rollBack();
            return (object)[
                'code' => '500',
                'data' => (object)[
                    'message' => 'Error en la actualizacion del documento: ' . $e->getMessage() . "\n" . $e->getLine() . "\n File:" . $e->getFile()
                ]
            ];
        }

    }

    /**
     * Validar existencia de productos
     *
     * @author Kevin Galindo
     */
    public function getStockProduct($product_id, $branchoffice_warehouse_id)
    {

        $inventory = Inventory::where('product_id', $product_id)
            ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->first();

        return $inventory ? $inventory->stock : 0;
    }

    public function validateExistenceProduct($document, $model)
    {

        $productNotStock = [];

        if ($model->operationType->affectation == 2) {
            foreach ($document->documentsProducts as $dp) {

                if ($dp->product->manage_inventory) {
                    $stockProduct = $this->getStockProduct($dp->product->id, $dp->branchoffice_warehouse_id);

                    if ($dp->quantity > $stockProduct) {
                        $productNotStock[] = [
                            'document_product_id' => $dp->id,
                            'product_id'          => $dp->product->id,
                            'stock'               => $stockProduct
                        ];
                    }
                }

            }
        }

        if (count($productNotStock) > 0) {
            return (object)[
                'code' => '404',
                'data' => (object)[
                    'message' => 'Existencia no Disponible.',
                    'data'    => $productNotStock,
                    'type'    => 'stock',
                ]
            ];
        }

        return null;
    }

    public function validateBackOrderForClosingDocument()
    {
    }

    /**
     * Este metodo se encarga de validar los
     * pedidos que estan retenidos.
     *
     * @param string $status
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function getOrderRejected($status, $request)
    {
        // Paginacion.
        $page = $request->has('page') ? $request->page : 1;
        $perPage = $request->has('perPage') ? $request->perPage : 15;
        $offset = ($page * $perPage) - $perPage;
        $paginate = $request->paginate ? $request->paginate : "true";

        // Configuramos la paginacion
        if ($status == 'RETENIDOS') {
            $response = $this->getOrderIfRejected(true, $request);
        } else if ($status == 'FACTURAR') {
            $response = $this->getOrderIfRejected(false, $request);
        }

        if ($paginate != "false") {
            $data = new LengthAwarePaginator(
                array_slice($response, $offset, $perPage),
                count($response),
                $perPage,
                $page,
                [
                    'path'  => $request->url(),
                    'query' => $request->query()
                ]
            );
        } else {
            $data = $response;
        }

        // Retornamos la informacion.
        return [
            'data' => $data,
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de validar
     * los pedidos que estan retenidos
     *
     * @author Kevin Galindo
     */
    public function getOrderIfRejected($rejected = true, $request)
    {
        // Traemos el modelo
        $model = Template::find($request->model_id);

        // Variables
        $motiveRejected = $request->motiveOrder ? $request->motiveOrder : null;
        $orderRejected = []; # Pedidos retenidos.
        $orderNotRejected = []; # Pedidos NO retenidos.

        // Traemos los pedidos que estan por facturar
        $orders = $this->getDocumentInvoiced(false, $request->toArray());

        // Validamos el estado de los pedidos
        foreach ($orders as $key => $order) {

            // Esta variable guarda el motivo de la retencion.
            $motiveRejection = '';

            // Validamos si el pedido fue aprobado.
            if (!empty($order['status_id_authorized']) &&
                $order['status_id_authorized'] == 912) {
                $orderNotRejected[] = $order;
                continue;
            }

            // Validamos si el pedido fue rechazado.
            if (!empty($order['status_id_authorized']) && $order['status_id_authorized'] == 913) {
                continue;
            }

            #FECHA DE VENCIMIENTO YA ESTE EXPIRADA.
            // Traemos las facturas del cliente.
            $invoices = Document::where('contact_id', $order->contact_id)
                ->where('operationType_id', 5)
                //->where('branchoffice_id', $request->branchoffice_id)
                ->get();

            // Recorremos las facturas
            foreach ($invoices as $key => $itemInvoice) {
                // Validamos que sean facturas.
                if ($itemInvoice->operationType_id == 5) {

                    // Validamos que existan en la tabla "documents_balance"
                    if (DocumentsBalance::where('document_id', $itemInvoice->id)->exists()) {
                        // Validamos que la factura este vencida de ser asi se agrega un error.
                        if ($itemInvoice->facturaVencida) {
                            $motiveRejection = 'Cartera Vencida';
                        }
                    }
                }
            }

            // Validamos el monto superado y ademas si el modelo lo permite
            if ($model->control_wallet_and_credit_quota) {
                $insufficientQuota = $this->validateInsufficientQuota($order);
                if ($insufficientQuota) {
                    $motiveRejection = empty($motiveRejection) ? 'Cupo insuficiente' : $motiveRejection . ' / Cupo insuficiente';
                }
            }

            // validamos si el pedido tiene algun motivo de retencion.
            if (!empty($motiveRejection)) {

                // Validamos el filtro $motiveRejected
                switch ($motiveRejected) {
                    case 'CARTERA_VENCIDA':
                        if ($motiveRejection == 'Cartera Vencida') {
                            $order['motive_rejection'] = $motiveRejection;
                            $orderRejected[] = $order;
                        }
                        break;

                    case 'CUPO_INSUFICIENTE':
                        if ($motiveRejection == 'Cupo insuficiente') {
                            $order['motive_rejection'] = $motiveRejection;
                            $orderRejected[] = $order;
                        }
                        break;

                    case 'CARTERA_VENCIDA_CUPO_INSUFICIENTE':
                        if ($motiveRejection == 'Cartera Vencida / Cupo insuficiente') {
                            $order['motive_rejection'] = $motiveRejection;
                            $orderRejected[] = $order;
                        }
                        break;

                    default:
                        $order['motive_rejection'] = $motiveRejection;
                        $orderRejected[] = $order;
                        break;
                }

            } // En caso de que no tenga ningun motivo de retencion se entiende que se puede facturar
            else {
                $orderNotRejected[] = $order;
            }
        }

        // Validamos si se devuelven los que se van a facturar o los que tienen algun problema.
        if ($rejected) {
            return $orderRejected;
        } else {
            return $orderNotRejected;
        }
    }

    /**
     * Este metodo se encarga de devolver los pedidos
     * segun si estado.
     *
     * @param string $status
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function getOrderByStatusAuthorized($status, $request)
    {
        // Paginacion
        $page = $request->has('page') ? $request->page : 1;
        $perPage = $request->has('perPage') ? $request->perPage : 15;
        $offset = ($page * $perPage) - $perPage;
        $paginate = $request->paginate ? $request->paginate : "true";

        // Filtros
        $search = $request->has('search') ? $request->search : null;
        $fromDate = $request->has('fromDate') ? $request->fromDate : null;
        $toDate = $request->has('toDate') ? $request->toDate : null;
        $branchoffice_id = $request->has('branchoffice_id') ? $request->branchoffice_id : null;

        //Validamos el estado para devolver la respuesta
        if ($status != 'AUTORIZADOS' && $status != 'RECHAZADOS') {
            return [
                'data' => [
                    'message' => 'El parametro no permitido "' . $status . '"'
                ],
                'code' => 400
            ];
        }

        $searchStatus = $status == 'AUTORIZADOS' ? 1 : 2;

        //Buscamos los datos aplicado los filtros si es que los hay

        // POR FECHA
        if (!empty($fromDate)) {
            $orders = Document::select('id', 'vouchertype_id', 'prefix', 'contact_id', 'due_date', 'seller_id', 'status_id_authorized', 'warehouse_id', 'document_date', 'consecutive', 'total_value')
                ->whereHas('status_authorized', function ($q) use ($searchStatus) {
                    $q->where('code_parameter', $searchStatus);
                })
                ->whereBetween('due_date', [$fromDate, $toDate])
                ->with([
                    'vouchertype'       => function ($q) {
                        $q->select('id', 'code_voucher_type', 'name_voucher_type', 'short_name');
                    },
                    'contact'           => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                        $q->with('customer');
                    },
                    'seller'            => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                    },
                    'documentsProducts' => function ($q) {
                        $q->select('id', 'document_id', 'product_id', 'quantity', 'original_quantity');
                        $q->with([
                            'product' => function ($q) {
                                $q->select('id', 'code', 'description');
                            }
                        ]);
                    },
                    'warehouse',
                    'invoices'
                ])
                ->where('branchoffice_id', $branchoffice_id)
                ->orderBy('id', 'asc')
                ->get()
                ->toArray();
        }

        // NORMAL
        if (empty($search) && empty($orders)) {
            $orders = Document::select('id', 'vouchertype_id', 'prefix', 'contact_id', 'due_date', 'seller_id', 'status_id_authorized', 'warehouse_id', 'document_date', 'consecutive', 'total_value')
                ->whereHas('status_authorized', function ($q) use ($searchStatus) {
                    $q->where('code_parameter', $searchStatus);
                })
                ->with([
                    'vouchertype'       => function ($q) {
                        $q->select('id', 'code_voucher_type', 'name_voucher_type', 'short_name');
                    },
                    'contact'           => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                        $q->with('customer');
                    },
                    'seller'            => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                    },
                    'documentsProducts' => function ($q) {
                        $q->select('id', 'document_id', 'product_id', 'quantity', 'original_quantity');
                        $q->with([
                            'product' => function ($q) {
                                $q->select('id', 'code', 'description');
                            }
                        ]);
                    },
                    'warehouse',
                    'invoices'
                ])
                ->where('branchoffice_id', $branchoffice_id)
                ->orderBy('id', 'asc')
                ->get()
                ->toArray();
        }

        // POR BUSQUEDA
        if (!empty($search) && empty($orders)) {
            $orders = Document::select('id', 'vouchertype_id', 'prefix', 'contact_id', 'due_date', 'seller_id', 'status_id_authorized', 'warehouse_id', 'document_date', 'consecutive', 'total_value')
                ->whereHas('status_authorized', function ($q) use ($searchStatus) {
                    $q->where('code_parameter', $searchStatus);
                })
                // Buscar
                ->whereHas('contact', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('seller', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                // Fin Buscar
                ->with([
                    'vouchertype'       => function ($q) {
                        $q->select('id', 'code_voucher_type', 'name_voucher_type', 'short_name');
                    },
                    'contact'           => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                        $q->with('customer');
                    },
                    'seller'            => function ($q) {
                        $q->select('id', 'identification', 'name', 'surname', 'address');
                    },
                    'documentsProducts' => function ($q) {
                        $q->select('id', 'document_id', 'product_id', 'quantity', 'original_quantity');
                        $q->with([
                            'product' => function ($q) {
                                $q->select('id', 'code', 'description');
                            }
                        ]);
                    },
                    'warehouse',
                    'invoices'
                ])
                ->where('branchoffice_id', $branchoffice_id)
                ->orderBy('id', 'asc')
                ->get()
                ->toArray();
        }

        // PAGINACION
        if ($paginate != 'false') {
            $data = new LengthAwarePaginator(
                array_slice($orders, $offset, $perPage),
                count($orders),
                $perPage,
                $page,
                [
                    'path'  => $request->url(),
                    'query' => $request->query()
                ]
            );
        } else {
            $data = $orders;
        }

        return [
            'data' => $data,
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de validar si un
     * cliente tiene cuota insuficiente al realizar un
     * pedido.
     *
     * @param object $order
     * @return boolean
     * @author Kevin Galindo
     */
    public function validateInsufficientQuota($order)
    {
        // Declaramos variables
        $totalWallet = 0;
        $totalQuotaCredit = 0;

        // Traemos el total en cartea
        $data = $this->documentsBalanceService->getTrackingByContactId($order['contact_id']);

        // Sumamos las cantidades para ver el total de cartera
        foreach ($data['data'] as $key => $value) {
            $totalWallet += $value->total_balance;
        }

        // Traemos el cupo de credito
        $contactCustomer = ContactCustomer::where('contact_id', $order['contact_id'])->first();
        if ($contactCustomer) {
            $totalQuotaCredit = $contactCustomer->credit_quota;
        }

        $availableQuota = $totalQuotaCredit - $totalWallet;

        return $order['total_value'] > $availableQuota;
    }

    /**
     * Este metodo se encarga de actualizar el registro
     * del pedido, cambiando su estado segun los parametros.
     *
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function changeStatusOrder($request)
    {
        date_default_timezone_set('America/Bogota');
        if (!$request->document_id) {
            return [
                'data' => [
                    'message' => 'Falta el parametero "documento id"'
                ],
                'code' => 400
            ];
        }
        if (!$request->user_id_authorizes) {
            return [
                'data' => [
                    'message' => 'Falta el parametero "usuario id"'
                ],
                'code' => 400
            ];
        }

        if (!$request->status_id_authorized) {
            return [
                'data' => [
                    'message' => 'Falta el parametero "status_id_authorized"'
                ],
                'code' => 400
            ];
        }

        $order = Document::where('id', $request->document_id)->first();

        $request->request->add([
            "branchoffice_id"           => $order->branchoffice_id,
            "branchoffice_warehouse_id" => $order->branchoffice_warehouse_id
        ]);

        $order->update([
            'observation_authorized' => $request->observation_authorized,
            'max_value_authorized'   => $request->max_value_authorized,
            'user_id_authorizes'     => $request->user_id_authorizes,
            'status_id_authorized'   => $request->status_id_authorized == 913 || $request->status_id_authorized == 912 ? $request->status_id_authorized : 912,
            'date_authorized'        => Carbon::parse(date('Y-m-d H:i:s'))
        ]);

        // CALCULAR LA DISPONIBILIDAD
        /*$request->offsetUnset('document_id');
        $request->offsetUnset('observation_authorized');
        $request->offsetUnset('max_value_authorized');
        $request->offsetUnset('status_id_authorized');
        $request->offsetUnset('user_id_authorizes');

        $this->calculateAvailability($request);*/

        return [
            'data' => [
                'message' => 'Registro actualizado',
                'data'    => $order
            ],
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de actualizar el registro
     * del pedido, cambiando su estado segun los parametros.
     *
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function retainOrderById($id, $request)
    {
        if (!$request->user_id_authorizes) {
            return [
                'data' => [
                    'message' => 'Faltan el parameteros'
                ],
                'code' => 400
            ];
        }

        $order = Document::where('id', $id)->first();
        $order->update([
            'observation_authorized' => $request->observation_authorized,
            'max_value_authorized'   => null,
            'user_id_authorizes'     => $request->user_id_authorizes,
            'status_id_authorized'   => null,
            'date_authorized'        => Carbon::now()
        ]);

        return [
            'data' => [
                'message' => 'Registro actualizado',
                'data'    => $order
            ],
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de devolver los
     * pedidos que ya fueron facturados para el
     * modulo de despacho.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     * @author Santiago Torres | Kevin Galindo
     */
    public function getDespatchInvoiced($request)
    {
        // Opciones de paginacion.
        $page = $request->has('page') ? $request->page : 1;
        $perPage = $request->has('perPage') ? $request->perPage : 15;
        $offset = ($page * $perPage) - $perPage;

        // Filtros
        $search = $request->search;
        $zone_id = $request->zone_id;
        $state = $request->state;
        $branchoffice_id = $request->branchoffice_id;
        $branchoffice_warehouse_id = $request->selected_branchoffice_warehouse ? $request->selected_branchoffice_warehouse : $request->branchoffice_warehouse_id;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $guide = $request->guide;
        $protractor = $request->protractor;
        $protractor = $request->protractor;
        $consecutive = $request->consecutive;
        $despatch = isset($request['despatch']) ? $request['despatch'] : null;
        $contact_id = $request->contact_id;

        // Se trae todos los pedidos que ya fueron facturados.
        //$response = $this->getDespatchListInvoiced(true, $request->toArray());
        $documents = Document::where('in_progress', false)
            // ->where('operationType_id', 5)
            // ->whereIN('operationType_id', [5,4])
            ->where(function ($query) {
                $query->whereNotIn('returned_document', [1, 3]);
                $query->orWhereNull('returned_document');
            })
            ->where('branchoffice_id', $branchoffice_id)
            //->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->whereHas('documentsProducts', function ($query) use ($branchoffice_warehouse_id) {
                $query->where('quantity', '<>', '0');
            })
            ->whereRaw('CASE WHEN documents."operationType_id" = 4 THEN documents.branchoffice_id = 305 ELSE true END')
            ->where(function ($where) {
                $where->where(function ($subWhere) {
                    $subWhere->where('operationType_id', 4)
                        ->whereHas('documentThread', function ($query) {
                            $query->where(function ($query) {
                                $query->where('branchoffice_id', '<>', 305);
                                $query->orWhereIn('branchoffice_warehouse_id', [280, 293, 297, 308, 312, 318, 323, 328, 333, 300, 352, 281, 462, 463, 464, 465, 466, 461, 467, 457, 458, 459, 460]);
                            });
                        });
                })->orWhere('operationType_id', 5);
            })
            ->with([
                'vouchertype:id,code_voucher_type,name_voucher_type',
                'documentThread'       => function ($query) {
                    $query->where(function ($query) {
                        $query->where('branchoffice_id', '<>', 305);
                        $query->orWhereIn('branchoffice_warehouse_id', [280, 293, 297, 308, 312, 318, 323, 328, 333, 300, 352, 281, 462, 463, 464, 465, 466, 461, 467, 457, 458, 459, 460]);
                    });
                },
                'warehouse'            => function ($q) {
                    $q->with('zone');
                    $q->with('city');
                },
                'documentsInformation' => function ($query) {
                    $query->with(['contact', 'additional_contact']);
                    $query->withDefault();
                },
                'contact'              => function ($query) {
                    $query->with('city:id,city');
                },
                'seller',
                'documentsProducts'    => function ($query) use ($branchoffice_warehouse_id) {
                    $query->where('quantity', '<>', '0');
                    $query->with([
                        'line',
                        'product' => function ($query) use ($branchoffice_warehouse_id) {
                            $query->with([
                                'inventory' => function ($query) use ($branchoffice_warehouse_id) {
                                    $query->where('branchoffice_warehouse_id', $branchoffice_warehouse_id);
                                },
                                'line'
                            ]);
                        }
                    ]);
                }
            ]);

        // Filtros
        if ($search) {

            // Consecutivo
            $documents->where(function ($query) use ($search) {
                $query->orWhere(DB::raw('CAST(consecutive AS TEXT)'), 'ILIKE', '%' . $search . '%');

                // Contact
                $query->orWhereHas('contact', function ($query) use ($search) {
                    $query->where('identification', 'ilike', '%' . $search . '%');
                    $query->orWhere('name', 'ilike', '%' . $search . '%');
                    $query->orWhere('surname', 'ilike', '%' . $search . '%');
                });

                // Seller
                $query->orWhereHas('seller', function ($query) use ($search) {
                    $query->where('identification', 'ilike', '%' . $search . '%');
                    $query->orWhere('name', 'ilike', '%' . $search . '%');
                    $query->orWhere('surname', 'ilike', '%' . $search . '%');
                });

            });

        }


        if (!is_null($despatch)) {
            $documents->whereIn('id', $despatch);
        }

        // Fecha de docuemnto
        if (!empty($fromDate) && !empty($toDate)) {
            $documents->whereBetween('document_date', [$fromDate . ' 00:00:00', $toDate . ' 23:59:59']);
        }

        // Zone
        if ($zone_id) {
            $documents->WhereHas('warehouse', function ($q) use ($zone_id) {
                $q->WhereHas('zone', function ($q2) use ($zone_id) {
                    $q2->where('id', $zone_id);
                });
            });
        }

        // guide
        if ($guide) {
            $documents->WhereHas('documentsInformation', function ($q) use ($guide) {
                $q->Where('guide_number', $guide);
            });
        }
        if ($protractor) {
            $documents->WhereHas('documentsInformation', function ($q) use ($protractor) {
                $q->Where('protractor', $protractor);
            });
        }
        if ($consecutive) {
            $documents->where('consecutive', $consecutive);
        }

        if ($contact_id) {
            $documents->where('contact_id', $contact_id);
        }

        // Estado
        if (!empty($state)) {
            $documents->where('document_state', $state);
        } else {
            $documents->where('document_state', '!=', '1000');
            $documents->where('document_state', '!=', '24655');
        }

        $documents = $documents->orderBy('document_date')->paginate($perPage);

        //$documents = array_slice($response, $offset, $perPage);

        // Total del iva por producto en el documento
        foreach ($documents as $key => $document) {
            $document->number_stickers = is_null($document->number_stickers) ? 1 : $document->number_stickers;
            foreach ($document->documentsProducts as $keydp => $documentProduct) {
                foreach ($documentProduct->documentProductTaxes as $keyT => $documentProductTax) {
                    if ($documentProductTax->tax->code == '10') {
                        $document->documentsProducts[$keydp]->total_value_iva = $documentProductTax->value;
                    }
                }
            }
        }
        // dd($documents)

        // Paginacion
        // $data = new LengthAwarePaginator(
        //     $documents->values(),
        //     count($documents),
        //     $perPage,
        //     $page,
        //     [
        //         'path' => $request->url(),
        //         'query' => $request->query()
        //     ]
        // );

        // Retornamos la informacion.
        return [
            'data' => $documents,
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de listar los
     * pedidos que ya fueron facturados para el
     * modulo de despacho.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     * @author Santiago Torres
     */

    public function getDespatchInvoicedList($request)
    {
        $filterDate = $request->has('date') ? $request->date : null;
        $branchoffice_id = $request->has('branchoffice_id') ? $request->branchoffice_id : null;
        // Validamos si hay algun filtro.
        if (!empty($filterDate) && $filterDate != 'null') {

            $documents = Document::whereHas('vouchertype', function ($q) {
                $q->where('code_voucher_type', '030');
            })
                ->with([
                    'warehouse'         => function ($q) {
                        $q->with('zone');
                    },
                    'documentsProducts' => function ($query) {
                        $query->with(['product' => function ($q) {
                            $q->with('line', 'inventory');
                        }
                        ]);
                    },
                    'invoices'          => function ($q) {
                        $q->where('vouchertype_id', 2);
                        $q->with([
                            'documentsProducts' => function ($query) {
                                $query->with(['product' => function ($q) {
                                    $q->with('line');
                                }
                                ]);
                            }
                        ]);
                    },
                    'contact'           => function ($q) {
                        $q->with('city');
                    }
                ])
                ->where('branchoffice_id', $branchoffice_id)
                ->where('document_date', '<=', $filterDate)
                ->orderBy('id', 'desc')
                ->get();
        } // Si no hay filtros se retorna toda la informacion.
        else {

            /* $documents = Document::whereHas('vouchertype', function ($q) {
                $q->where('code_voucher_type', '030');
            })
            ->with([
                'warehouse' => function ($q){
                    $q->with('zone');
                },
                'documentsProducts' => function ($query){
                    $query->with(['product' => function($q){
                        $q->with('line','inventory');
                    }
                    ]);
                },
                'invoices' => function ($q) {
                    $q->where('vouchertype_id', 2);
                    $q->with([
                        'documentsProducts'=> function ($query){
                            $query->with(['product' => function ($q){
                                $q->with('line');
                            }
                            ]);
                        }
                    ]);
                },
                'contact' => function ($q){
                    $q -> with('city');
                }
            ])
            ->where('branchoffice_id' ,$branchoffice_id)
            ->orderBy('id', 'desc')
            ->get(); */
            //->get();
        }

        // Se trae todos los pedidos que ya fueron facturados.
        $response = $this->getDespatchInvoiced($request);
        // $response = $this->getDespatchListInvoiced(true, $request->toArray());
        // getDespatchInvoiced

        // Retornamos la informacion.
        return [
            'data' => $response,
            'code' => 200
        ];
    }

    /*
     * Este metodo se encarga de actualizar la factura
     * que se esta despachando.
     *
     * @param $id
     * @param $request
     * @return array
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceInformation($id, $request)
    {
        $zone_id = $request->has('zone_id') ? $request->zone_id : null;
        $guide_number = $request->has('guide_number') ? $request->guide_number : null;
        $observation = $request->has('observation') ? $request->observation : null;
        $protractor = $request->has('protractor') ? $request->protractor : null;
        $invoice_filed = $request->has('invoice_filed') ? $request->invoice_filed : false;
        $number_stickers = $request->has('number_stickers') ? $request->number_stickers : null;

        //Buscamos el registro
        $document = Document::find($id);
        if (!$document) {
            return [
                'data' => 'No hay registro...',
                'code' => 404
            ];
        }

        // Actualizar zona y observacion
        $document_obs = Document::find($id);
        $document_obs->observation = $observation;
        $document_obs->number_stickers = $number_stickers;
        $document_obs->save();

        $document->warehouse->zone_despatch_id = $zone_id;
        $document->warehouse->save();

        // Actualizar la informacion del documento
        DocumentInformation::updateOrCreate(
            [
                'document_id' => $id
            ],
            [
                'guide_number'  => $guide_number,
                //'delivery_observation' => $delivery_observation,
                'protractor'    => $protractor,
                'invoice_filed' => $invoice_filed
            ]
        );

        return [
            'message' => 'Registro actualizado.',
            'data'    => [$document],
            'code'    => 200
        ];
    }


    /**
     * Este metodo actualiza el estado de la factura.
     *
     * @param $id
     * @return array
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceStatus($id, $request)
    {
        $document = Document::find($id);
        if (!$document) {
            return [
                'data' => 'No hay registro...',
                'code' => 404
            ];
        }

        $state = 1000;
        if (isset($request->state) && $request->state == 'DESHACER') $state = 1;

        $document->document_state = $state;
        $document->save();

        return [
            'data' => 'Actualizado.',
            'code' => 200
        ];
    }

    /**
     * Este metodo actualiza el estado de la factura.
     * de forma masiva
     *
     * @param $request
     * @return array
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceStatusMasive($request)
    {
        //Validamos la informacion
        $v = Validator::make($request->all(), [
            'despatch' => 'required',
        ]);

        //En caso de no cumplir con las el "Validator" se retorna un mensaje de error.
        if ($v->fails()) return ['data' => $v->errors(), 'code' => 400];

        // Actualizamos el estado de los productos despachados.
        $conteo = 0;
        foreach ($request->despatch as $key => $value) {

            if (!isset($value['id'])) continue;
            $document = Document::find($value['id']);
            if ($document && $value['pack_off']) {
                $document->document_state = $request->give_back ? 1 : 1000;
                $document->document_state = $request->admin_operations ? 24655 : $document->document_state;
                $document->save();
                $conteo++;
            }
        }

        //Guardamos los productos de lote.
        if ($request->productLot) {
            foreach ($request->productLot as $key => $value) {
                DocumentProductLot::updateOrCreate(
                    [
                        'documents_products_id' => $value['products_lots']['documents_products_id'],
                    ],
                    [
                        'documents_products_id' => $value['products_lots']['documents_products_id'],
                        'num_lot'               => $value['products_lots']['num_lot'],
                        'expiration_date'       => $value['products_lots']['expiration_date']
                    ]
                );
            }
        }


        return [
            'data' => 'Se han actualizado (' . $conteo . ') registros.',
            'code' => 200
        ];
    }

    /**
     * Este metodo se encarga de valiar si un pedido
     * ya fue facturado en su totalidad y dependiento del
     * valor en la variable "$invoiced" retorna cierta informacion:
     * $invoiced:
     * true = retorna los que han sido facturados
     * false = retorna los que NO han sido facturados
     *
     * @param boolean invoiced | determina si devolver los que esta facturado o lo que no
     * @param Request request  | contiene los parametros
     * @return Document
     * @author Kevin Galindo
     */
    public function getDocumentInvoiced($invoiced = true, array $request = [])
    {
        // Variables
        $model_id = isset($request['model_id']) ? $request['model_id'] : 2;
        $model = Template::find($model_id);
        $data = [];

        // Filtros
        $filterText = isset($request['filter']) ? $request['filter'] : null;
        $product_id = isset($request['product_id']) ? $request['product_id'] : null;
        $despatch = isset($request['despatch']) ? $request['despatch'] : null;
        $search = isset($request['search']) ? $request['search'] : null;
        $filterDate = isset($request['date']) ? $request['date'] : null;
        $state = isset($request['state']) ? $request['state'] : null;
        $branchoffice_id = !empty($request['branchoffice_id']) ? $request['branchoffice_id'] : null;
        $closed = isset($request['closed']) ? $request['closed'] : false;
        $zone_id = isset($request['zone_id']) ? $request['zone_id'] : null;
        $selected_branchoffice_warehouse = isset($request['selected_branchoffice_warehouse']) ? $request['selected_branchoffice_warehouse'] : null;

        // Rango de fechas
        $fromDate = isset($request['fromDate']) ? $request['fromDate'] : null;
        $toDate = isset($request['toDate']) ? $request['toDate'] : null;


        // Traemos los tipos de comprobantes (documento / base)
        $vouchersType = VouchersType::where('code_voucher_type', $model->voucherType->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        $voucherTypeBase = VouchersType::where('code_voucher_type', $model->voucherType1->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        //Realizamos la consulta
        $documents = Document::where('vouchertype_id', $voucherTypeBase->id)
            ->where('in_progress', false)
            ->where('closed', $closed)
            ->with([
                'documentsProducts' => function ($q) use ($selected_branchoffice_warehouse, $product_id) {
                    if (!empty($product_id)) {
                        $q->where('product_id', $product_id);
                    }
                    $q->with([
                        'product' => function ($qq) use ($selected_branchoffice_warehouse) {
                            $qq->with([
                                'line',
                                'inventory' => function ($query) use ($selected_branchoffice_warehouse) {
                                    $query->where('branchoffice_warehouse_id', $selected_branchoffice_warehouse);
                                }
                            ]);
                        },
                        'documentProductTaxes'
                    ]);
                },
                'invoices'          => function ($q) use ($vouchersType) {
                    $q->where('vouchertype_id', $vouchersType->id);
                    $q->where('in_progress', false);
                    $q->with([
                        'documentsProducts' => function ($qq) {
                            $qq->with(['product']);
                        }
                    ]);
                },
                'warehouse'         => function ($q) {
                    $q->with(['zone']);
                },
                'documentsInformation',
                'contact'           => function ($q) {
                    $q->with('customer');
                },
                'seller'            => function ($q) {
                    $q->select('id', 'identification', 'name', 'surname', 'address');
                },
                'vouchertype'
            ]);

        //filtro zona
        if ($zone_id) {
            $documents->WhereHas('warehouse', function ($q) use ($zone_id) {
                $q->WhereHas('zone', function ($q2) use ($zone_id) {
                    $q2->where('id', $zone_id);
                });
            });
        }

        // Validamos la SEDE
        if (!empty($branchoffice_id)) {
            $documents->where('branchoffice_id', $branchoffice_id);
        }

        // Validamos si hay algun filtro.
        if (!is_null($despatch)) {
            $documents->whereIn('id', $despatch);
        }

        if (!empty($search)) {
            $documents
                ->whereHas('contact', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('seller', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('documentsProducts', function ($q) use ($search) {
                    $q->where('description', 'ilike', '%' . $search . '%');
                });
        }

        if (!empty($filterDate) && $filterDate != 'null') {
            $documents->where('document_date', '<=', $filterDate);
        }

        if (!empty($fromDate)) {
            $documents->whereBetween('document_date', [$fromDate . ' 00:00:00', $toDate . ' 23:59:59']);
        }

        // Validamos que estado debe devolver del pedido
        if (!empty($state) && $state == 'DESPACHADO') {
            $documents->whereHas('status', function ($q) {
                $q->where('code_parameter', '5');
            });
        }
        // else {
        //     $documents->whereHas('status', function ($q) {
        //         $q->where('code_parameter', '!=', '5');
        //     });
        // }

        // Validamos el filtro de texto
        if ($filterText) {
            // Consecutivo
            $documents->where(function ($query) use ($filterText) {
                $query->orWhere(DB::raw('CAST(consecutive AS TEXT)'), 'ILIKE', '%' . $filterText . '%');

                // Contact
                $query->orWhereHas('contact', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });

                // Seller
                $query->orWhereHas('seller', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });

            });
        }

        // Traemos la informacion
        $documents = $documents->orderBy('id', 'ASC')->get();

        // Validamos
        foreach ($documents as $key => $document) {

            $documentsProductsBackOrder = [];

            // Traemos el BackOrder
            // foreach ($document->documentsProducts as $key => $documentProduct) {
            //     $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request, false);
            //     $documentsProductsBackOrder[] = $bo;
            //     $documentProduct->back_order = $bo['backorder'];
            // }

            // Validar documentos repetidos
            $repeatQuantity = 0;
            $countDocumentProduct = count($document->documentsProducts);
            foreach ($data as $key => $documentR) {
                foreach ($documentR->documentsProducts as $key => $documentProductR) {
                    foreach ($document->documentsProducts as $key => $documentProduct) {

                        if ($document->vouchertype_id == $documentR->vouchertype_id &&
                            $document->thread == $documentR->thread &&
                            $documentProduct->back_order == $documentProductR->back_order &&
                            $documentProduct->created_at == $documentProductR->created_at &&
                            $documentProduct->product_id == $documentProductR->product_id) {
                            $repeatQuantity++;
                        }

                    }
                }
            }

            if ($repeatQuantity == $countDocumentProduct) continue;

            // validamos el BackOrder
            $count = 0;
            foreach ($documentsProductsBackOrder as $key => $documentProductBackOrder) {
                if ($documentProductBackOrder['backorder'] > 0) {
                    $count++;
                }
            }

            //Validamos que retornar
            if ($invoiced) {
                if ($count === 0) {
                    $data[] = $document;
                }
            } else {
                if ($count !== 0) {
                    $data[] = $document;
                }
            }
        }

        //Devolvemos la informacion
        return $data;
    }


    /**
     * Obtiene los documentos base para el modulo
     * de listar documentos.
     *
     * @param Request $request Contiene los filtros a aplicar en la consulta $from_retained si la
     * consulta es llamada desde pedidos retenidos no debe filtrar por sede de documento
     * @return array|object Contiene los documentos obtenidos de la base de datos
     * @author Kevin Galindo
     */
    public function listBaseDocuments($request, $from_retained = false)
    {
        $model = $this->getModelForIdFromCache($request->model_id);

        // Validamos que el modelo maneje documento base
        if ($model->doc_base_management == 2 && !$model->document_save_finish) {
            return [];
        }

        // Proceso de validacion cartera
        if ($model->operationType->code === '205') {
            if (Subscriber::where('identification', '900338016')->exists()) {
                DB::select('select * from public.fn_document_wallet_problem(?);', [$request->branchoffice_id]);
            } else {
                DB::select('select * from public.fn_document_wallet_problem_general(?);', [$request->branchoffice_id]);
            }
        }

        // Sede por defecto segun el modelo.
        if ($model) {
            // Si el modelo tiene una sede por defecto se setea a los parametros.
            if (!empty($model->branchoffice_id)) {
                $request->merge(['branchoffice_id' => $model->branchoffice_id]);
            }

            // Si el modelo tiene una bodega por defecto se setea a los parametros.
            if (!empty($model->branchoffice_warehouse_id)) {
                $request->merge(['branchoffice_warehouse_id' => $model->branchoffice_warehouse_id]);
            }
        }

        // Filtros/Parametros
        $user = JWTAuth::parseToken()->toUser();

        $user_rol = $user->getRoles->toArray();
        $filterText = $request->filter;
        $seller_id = $request->seller_id;
        $branchoffice_id = $request->branchoffice_id;
        $closed = $request->closed ? $request->closed : false;
        $branchoffice_warehouse_id = $request->branchoffice_warehouse_id;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $motiveRejected = $request->motive_rejected;

        // Estado del documento
        $stateBackorder = $request->state_backorder ? $request->state_backorder : [];
        $stateWallet = $request->state_wallet ? $request->state_wallet : [];

        if ($model->document_save_finish) {
            $code_voucher_type = $model->voucherType->code_voucher_type;
        } else {
            $code_voucher_type = $model->voucherType1->code_voucher_type;
        }

        // Traemos los docuemntos
        $documents = Document::with(
            [
                'warehouse:id,code,description,seller_id',
                'warehouse.branchoffice:id,code,name',
                'seller:id,identification,name,surname,address',
                'contact:id,identification,name,surname,address',
                'vouchertype:id,code_voucher_type,name_voucher_type',
                'contact.customer:id,contact_id,category_id,credit_quota',
                'documentsProducts:id,document_id,product_id,quantity_wms',
                'stateEnding:id,paramtable_id,name_parameter,code_parameter',
                'stateBackorder:id,paramtable_id,name_parameter,code_parameter',
                'warehouse.zone:id,paramtable_id,name_parameter,code_parameter',
                'status_authorized:id,paramtable_id,name_parameter,code_parameter',
                'stateAuthorizationWalletParam:id,paramtable_id,name_parameter,code_parameter',
                'userAuthorizesWallet:id,name,email,surname,branchoffice_id,branchoffice_warehouse_id,contact_id',
                'warehouse:id,code,description,email,telephone,address,contact_id,branchoffice_id,city_id,credit_quota'
            ])
            ->when($user->hasRole('Vendedor'), function ($seller) use ($user) {
                $seller->whereHas('seller', function ($query) use ($user) {
                    $query->where('email', $user->email);
                });
            })
            ->whereHas('vouchertype', function ($query) use ($code_voucher_type) {
                $query->where('code_voucher_type', $code_voucher_type);
            })
            ->where('in_progress', false)
            ->whenNotEmpty('motive_retained', $motiveRejected)
            ->whenNotEmpty('seller_id', $seller_id)
            ->whenNotEmpty('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->whenNotEmpty('branchoffice_warehouse_id', $branchoffice_warehouse_id)
            ->when(!empty($branchoffice_id) && !$from_retained, function ($query) use ($branchoffice_id) {
                $query->where('branchoffice_id', $branchoffice_id);
            })
            ->when(!empty($fromDate) && !empty($toDate), function ($query) use ($fromDate, $toDate) {
                $query->whereBetween('document_date', [$fromDate . ' 00:00:00', $toDate . ' 23:59:59']);
            })
            ->when(count($stateWallet) > 0 && $model->operationType->code === '205', function ($query) use ($stateWallet) {
                $query->whereHas('stateAuthorizationWalletParam', function ($query) use ($stateWallet) {
                    $query->whereIn('code_parameter', $stateWallet);
                });
            })
            ->when(!empty($filterText), function ($query) use ($filterText) {
                $query->where(function ($query) use ($filterText) {

                    $query->orWhere(DB::raw('CAST(consecutive AS TEXT)'), 'ILIKE', '%' . $filterText . '%');

                    // Contacto
                    $query->orWhereHas('contact', function ($query) use ($filterText) {
                        $query->where('identification', 'ilike', '%' . $filterText . '%');
                        $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                        $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                    });

                    // Vendedor
                    $query->orWhereHas('seller', function ($query) use ($filterText) {
                        $query->where('identification', 'ilike', '%' . $filterText . '%');
                        $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                        $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                    });

                });
            })
            ->when(count($stateBackorder) > 0, function ($query) use ($stateBackorder, $closed) {
                if (in_array('4', $stateBackorder)) {
                    $query->where('closed', true);
                } else {
                    $query->where('closed', $closed);
                    $query->whereHas('stateBackorder', function ($query) use ($stateBackorder) {
                        $query->whereIn('code_parameter', $stateBackorder);
                    });
                }

            });

        if ($request->myActivity) {
            $documents->with([
                'documentsProducts' => function ($query) {
                    $query->select('id', 'description', 'operationType_id', 'document_id', 'product_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'quantity', 'original_quantity', 'unit_value_before_taxes', 'discount_value', 'physical_unit', 'is_benefit', 'promotion_id', 'total_value_brut', 'total_value', 'total_value_us', 'total_value_brut_us', 'discount_value_us', 'branchoffice_warehouse_id');
                    $query->where('quantity', '>', 0);
                    $query->with([
                        'product' => function ($query) {
                            $query->select('id', 'code', 'description', 'quantity_available', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'modify_description_sale', 'manage_inventory', 'weight');
                            $query->with([
                                'line',
                                'subline',
                                'brand',
                                'subbrand'
                            ]);
                        }
                    ]);
                }
            ]);
        }

        return $documents->orderBy('document_date', 'ASC')->get();
    }

    public function listBaseDocumentsRetained($request)
    {
        $documents = $this->listBaseDocuments($request, true);

        if ($request->branchoffice_id) {
            $documents = $documents->where('warehouse.branchoffice_id', $request->branchoffice_id)->values();
        }

        foreach ($documents as $key => $document) {
            // Validación si es COLSAISA
            if (Subscriber::where('identification', '900338016')->exists()) {
                $totalWallet = DB::select('select sum(db.invoice_balance) as invoice_balance
                from documents d
                inner join documents_balance db on db.document_id = d.id
                where d.contact_id = ?
                and d."operationType_id" = 5', [$document->contact_id]);
                $totalWallet = $totalWallet[0]->invoice_balance;

            }else{
                $totalWallet = DB::select("select * from public.wallet_contact(?)", [$document->contact_id]);
                $totalWallet = $totalWallet[0]->wallet;
            }

            $creditQuota = $document->contact ? $document->contact->customer->credit_quota : 0;
            $availableQuota = (int)$creditQuota - (int)$totalWallet;

            $documents[$key]['available_quota'] = $availableQuota;
            $documents[$key]['total_receivable'] = $totalWallet;
        }

        return $documents;
    }

    public function myActivityListBaseDocuments($request)
    {
        // Validamos la informacion los parametros obligatorios.
        $v = Validator::make($request->all(), [
            'model_id'        => 'required',
            'branchoffice_id' => 'required',
        ]);

        //En caso de no cumplir con la validacion se retorna un mensaje de error.
        if ($v->fails()) return (object)['data' => $v->errors(), 'code' => 400];

        // Tremos el modelo
        $model = Template::find($request->model_id);

        // Validamos que el modelo maneje docuemnto base
        if ($model->doc_base_management == 2) {
            return [];
        }

        // Capturamos el Tipo de operacion
        $operationType = $model->operationType;

        ## TRAEMOS LOS DOCUEMNTOS ##

        $user = JWTAuth::parseToken()->toUser();

        // Filtros/Parametros
        $filterText = $request->filter;
        $seller_id = $request->seller_id;
        $documentStateWallet = $request->document_state_wallet;
        $branchoffice_id = $request->branchoffice_id;
        $closed = $request->closed ? $request->closed : false;
        $branchoffice_warehouse_id = $request->branchoffice_warehouse_id;
        $fromDate = $request->fromDate;
        $dateFromMyActivity = $request->dateFromMyActivity;
        $toDate = $request->toDate;

        // Retenido
        $detained = $request->detained ? $request->detained : false;
        $completed = $request->completed ? $request->completed : false;
        $motiveRejected = $request->motive_rejected;

        // Estado del documento
        $stateBackorder = $request->state_backorder ? $request->state_backorder : [];
        $stateWallet = $request->state_wallet ? $request->state_wallet : [];

        // Traemos los tipos de comprobantes

        # DOCUMENTO QUE SE CREARA
        $vouchersType = VouchersType::where('code_voucher_type', $model->voucherType->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        # DOCUMENTO BASE
        $voucherTypeBase = VouchersType::where('code_voucher_type', $model->voucherType1->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        // Traemos los docuemntos
        $documents = Document::select(
            'id',
            'vouchertype_id',
            'consecutive',
            'warehouse_id',
            'document_date',
            'contact_id',
            'document_state',
            'in_progress',
            'seller_id',
            'user_id',
            'branchoffice_id',
            'branchoffice_warehouse_id',
            'status_id_authorized',
            'total_value',
            'model_id',
            'total_value_brut',
            'prefix',
            'state_backorder',
            'user_id_authorizes',
            'date_authorized',
            'observation_authorized',
            'state_authorization_wallet'
        )
            ->with([
                'documentsProducts' => function ($query) {
                    $query->select('id', 'description', 'operationType_id', 'document_id', 'product_id', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'quantity', 'original_quantity', 'unit_value_before_taxes', 'discount_value', 'physical_unit', 'is_benefit', 'promotion_id', 'total_value_brut', 'total_value', 'total_value_us', 'total_value_brut_us', 'discount_value_us', 'branchoffice_warehouse_id');
                    $query->where('quantity', '>', 0);
                    $query->with([
                        'product' => function ($query) {
                            $query->select('id', 'code', 'description', 'quantity_available', 'line_id', 'subline_id', 'brand_id', 'subbrand_id', 'modify_description_sale', 'manage_inventory', 'weight');
                            $query->with([
                                'line',
                                'subline',
                                'brand',
                                'subbrand'
                            ]);
                        }
                    ]);
                },
                'warehouse'         => function ($query) {
                    $query->select('id', 'code', 'description', 'email', 'telephone', 'address', 'contact_id', 'branchoffice_id', 'city_id');
                    $query->with(['zone']);
                },
                'contact'           => function ($query) {
                    $query->select('id', 'identification', 'name', 'surname', 'address');
                    $query->with([
                        'customer' => function ($query) {
                            $query->select('id', 'contact_id', 'category_id', 'credit_quota');
                        }
                    ]);
                },
                'seller'            => function ($query) {
                    $query->select('id', 'identification', 'name', 'surname', 'address');
                },
                'vouchertype',
                'stateBackorder',
                'status_authorized',
                'userAuthorizesWallet'
            ])
            // Validamos por codigo del tipo de comprobante.
            ->whereHas('vouchertype', function ($query) use ($voucherTypeBase) {
                $query->where('code_voucher_type', $voucherTypeBase->code_voucher_type);
            })
            ->where('in_progress', false)
            ->where('user_id_authorizes', $user->id)
            ->where('closed', false)
            ->where('branchoffice_id', $branchoffice_id);

        ## FILTROS DE BUSQUEDA ##

        // Texto
        if ($filterText) {
            $documents->where(function ($query) use ($filterText) {

                // Codigo
                $query->orWhere(DB::raw('CAST(consecutive AS TEXT)'), 'ILIKE', '%' . $filterText . '%');

                // Contacto
                $query->orWhereHas('contact', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });

                // Vendedor
                $query->orWhereHas('seller', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });

            });
        }

        // Fecha de docuemnto
        if (!empty($dateFromMyActivity)) {
            $documents->where('date_authorized', '>=', $dateFromMyActivity . ' 00:00:00');
        }

        // Traemos la informacion y organizamos por ID
        $documents = $documents->orderBy('id', 'ASC')->get();

        // Validamos los problemas en cartera
        $documents = $this->getDocumentWalletProblem($documents, $motiveRejected, $detained, true);

        return collect($documents)->sortBy('document_date')->values();
    }

    /**
     * Este metodo se encarga de
     * traer los documentos cuyo back order
     * ya fue completado
     *
     * @author Kevin Galindo
     */
    public function getDocumentCompleted($documents, $model, $completed = true)
    {
        // Validamos si ya fueron finalizados
        foreach ($documents as $key => $document) {

            // $calculateBackOrderRequest = [
            //     'document_id' => $document->id,
            //     'model_id'    => $model->id
            // ];

            // $documentsBackOrder = [];

            // foreach ($document->documentsProducts as $keyDp => $documentProduct) {
            //     if ($documentProduct->quantity > 0) {
            //         $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $calculateBackOrderRequest, false);
            //         $documentsBackOrder[] = $bo;
            //         $document->documentsProducts[$keyDp]['back_order'] = $bo['backorder'];
            //     }
            // }

            $sumBackOrder = $document->backorder_quantity; //collect($documentsBackOrder)->sum('backorder');

            if ($sumBackOrder == 0) {
                $documents[$key]['complete'] = true;
            } else {
                $documents[$key]['complete'] = false;
            }

        }

        // Retornamos la informacion
        return $documents->where('complete', $completed);
    }

    /**
     * Este metodo se encarga de
     * validar los docuemntos para ver si
     * tienen problemas en cartera
     *
     * @author Kevin Galindo
     */
    public function getDocumentWalletProblem($documents, $motiveRejected, $detained = true, $all = false)
    {
        // Validamos si el pedido esta retenido
        foreach ($documents as $key => $document) {

            // Esta variable guarda el motivo de la retencion.
            $motiveRejection = '';

            // Validamos si el pedido fue aprobado.
            if (!empty($document->status_id_authorized) && $document->status_id_authorized == 912) {

                if (!$document->stateAuthorizationWalletParam || ($document->stateAuthorizationWalletParam && $document->stateAuthorizationWalletParam->code_parameter !== "05")) {
                    // Actualizar estado cartera
                    $state = Parameter::whereHas('paramtable', function ($query) {
                        $query->where('code_table', '060');
                    })->where('code_parameter', '05')->first();
                    $document->state_authorization_wallet = $state->id;
                    $document->save();
                }

                $documents[$key]['detained'] = false;
                continue;
            }

            // Validamos si el pedido fue rechazado.
            if (!empty($document->status_id_authorized) && $document->status_id_authorized == 913) {
                if (!$document->stateAuthorizationWalletParam || ($document->stateAuthorizationWalletParam && $document->stateAuthorizationWalletParam->code_parameter !== "09")) {
                    $state = Parameter::whereHas('paramtable', function ($query) {
                        $query->where('code_table', '060');
                    })->where('code_parameter', '09')->first();
                    $document->state_authorization_wallet = $state->id;
                    $document->save();
                }
                // Actualizar estado cartera
                continue;
            }

            ## --FECHA DE VENCIMIENTO YA ESTE EXPIRADA-- ##
            // Traemos las facturas del cliente.
            $invoices = Document::select('id', 'document_date', 'due_date', 'operationType_id', 'contact_id')
                ->where('contact_id', $document->contact_id)
                ->where('operationType_id', 5)
                //->where('in_progress', false)
                ->has('receivable')
                ->get();

            if ($invoices->where('facturaVencida', true)->count() > 0) {
                $motiveRejection = 'Cartera Vencida';
            }

            ## --CUPO DISPONIBLE-- ##
            $totalWallet = 0;
            $totalQuotaCredit = 0;
            $availableQuota = 0;

            // Total Cartera
            $totalWallet = $invoices->sum(function ($item) {
                if (isset($item->receivable[0])) {
                    return round($item->receivable[0]->invoice_balance, 0, PHP_ROUND_HALF_UP);
                } else {
                    return 0;
                }
            });

            // Cupo de credito
            $totalQuotaCredit = $document->contact ? $document->contact->customer->credit_quota : 0;

            // Cupo disponible
            $availableQuota = $totalQuotaCredit - $totalWallet;

            if ($document->total_value > $availableQuota) {
                $motiveRejection = empty($motiveRejection) ? 'Cupo insuficiente' : $motiveRejection . ' / Cupo insuficiente';
            }

            // En caso de tener algun motivo de retencion, se retiene ese documento.
            if (!empty($motiveRejection)) {

                // Actualizar estado cartera
                if (!$document->stateAuthorizationWalletParam || ($document->stateAuthorizationWalletParam && $document->stateAuthorizationWalletParam->code_parameter !== "07")) {
                    $state = Parameter::whereHas('paramtable', function ($query) {
                        $query->where('code_table', '060');
                    })->where('code_parameter', '07')->first();
                    $document->state_authorization_wallet = $state->id;
                    $document->save();
                }

                $documents[$key]['detained'] = true;
                $documents[$key]['motive_rejection'] = $motiveRejection;
                $documents[$key]['available_quota'] = $availableQuota;
                $documents[$key]['total_receivable'] = $totalWallet;
            } else {
                // Actualizar estado cartera
                if (!$document->stateAuthorizationWalletParam || ($document->stateAuthorizationWalletParam && $document->stateAuthorizationWalletParam->code_parameter !== "03")) {
                    $state = Parameter::whereHas('paramtable', function ($query) {
                        $query->where('code_table', '060');
                    })->where('code_parameter', '03')->first();
                    $document->state_authorization_wallet = $state->id;
                    $document->save();
                }

                $documents[$key]['detained'] = false;
            }
        }

        if (!empty($motiveRejected)) {
            return $documents->where('motive_rejection', $motiveRejected)->where('detained', $detained);
        }

        if ($all) {
            return $documents;
        }

        return $documents->where('detained', $detained);
    }

    /**
     * Obtiene los documentos base
     *
     * @author Kevin Galindo
     */
    public function baseDocuments($request)
    {
        $model = Template::find($request->model_id);

        // Validamos el modelo para la sede y bodega #CAMBIOMODELOPORDEFECTO
        if ($model) {
            if (!empty($model->branchoffice_id)) {
                $request->merge(['branchoffice_id' => $model->branchoffice_id]);
            }

            if (!empty($model->branchoffice_warehouse_id)) {
                $branch_office_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $request->branchoffice_id)
                    ->where('warehouse_code', $model->branchOfficeWarehouse->warehouse_code)
                    ->first();
                if ($branch_office_warehouse) {
                    $request->merge(['branchoffice_warehouse_id' => $branch_office_warehouse->id]);
                }
            }
        }

        $operationType = $model->operationType;

        if ($model->doc_base_management == 2) {
            return [];
        }

        // Validamos si el tipo de operacion es una factura de venta
        switch ($operationType->code) {
            case '205':
                $documents = $this->getOrderIfRejected(false, $request);
                break;

            default:
                $documents = $this->getDocumentInvoiced(false, $request->toArray());
                break;
        }

        // Calculamos los valores del documento.
        foreach ($documents as $key => $document) {
            $document = $this->calculateTotalsDocument($document, $request);
        }

        return collect($documents)->sortBy('id')->values();
    }

    /**
     * Este metodo se encarga de validar y devolver
     * el carrito
     *
     * @author Kevin Galindo
     */
    public function checkCart($contact, $warehouse, $user, $request)
    {
        $document = $this->documentRepository->getCart($contact, $warehouse, $user);

        if ($document->id) {
            $document = $this->calculateTotalsDocument($document, $request);
            $document = $this->cartPromotionConcatenate($document->toArray(), $request);

            // Descuentos
            if (isset($document['documents_products'])) {

                // Cargar los productos de descuentos
                foreach ($document['documents_products'] as $keydp => $documentsProduct) {
                    // Descuentos para el document product
                    $product = $this->productService->getPriceDiscountByCategoryContact([$documentsProduct['product']], $contact);
                    $document['documents_products'][$keydp]['product'] = $product[0];

                    // Descuento obsequio
                    if ($documentsProduct['promotion']) {
                        $product = $this->productService->getPriceDiscountByCategoryContact([$documentsProduct['promotion']['specific_product']], $contact);
                        $documentsProduct['promotion']['specific_product'] = $product[0];
                    }
                }

            }

            // Valores de descuento
            $document = $this->calculateTotalDiscount($document);

            foreach ($document['documents_products'] as $key => $documentProduct) {
                if ($documentProduct['promotion_id'] == null) {
                    continue;
                }
                $document['documents_products'][$key]['promotion']['comply'] = false;
                $document['documents_products'][$key]['promotion']['specific_product'] = null;
            }

            $document = $this->discountValueUnitDocumentProduct($document);
        }

        return $document;
    }

    /**
     * Esta funcio se encarga de calcular el valor unitario del descuento
     *
     * @author Kevin Galindo
     */
    public function discountValueUnitDocumentProduct($document)
    {
        foreach ($document['documents_products'] as $key => $document_product) {
            if (empty($document_product['promotion_id']) && $document_product['discount_value'] > 0 && $document_product['quantity'] > 0) {
                if (!empty($document_product['original_quantity'])) {
                    $document['documents_products'][$key]['discount_value_unit'] = $document_product['discount_value'] / $document_product['original_quantity'];
                } else {
                    $document['documents_products'][$key]['discount_value_unit'] = $document_product['discount_value'] / $document_product['quantity'];
                }
            }

            if (isset($document_product['inventory_cost_value'])) {
                if (is_null($document_product['inventory_cost_value']) || $document_product['inventory_cost_value'] == 0) {
                    $document['documents_products'][$key]['inventory_cost_value_unit'] = 0;
                } else {
                    if ($document_product['quantity'] == 0) {
                        $document['documents_products'][$key]['inventory_cost_value_unit'] = 0;
                    } else {
                        $document['documents_products'][$key]['inventory_cost_value_unit'] = $document_product['inventory_cost_value'] / $document_product['quantity'];
                    }
                }
            }
        }

        return $document;
    }

    /**
     * Este metodo se encarga de calcular el total en descuento
     * del documento.
     *
     * @author Kevin Galindo
     */
    public function calculateTotalDiscount($document)
    {
        $document['total_discount'] = 0;

        foreach ($document['documents_products'] as $keydp => $documentProduct) {

            $document['documents_products'][$keydp]['total_discount'] = 0;

            // Validamos que el producto tenga promocion
            if (!isset($documentProduct['promotion_id'])) {
                continue;
            }


            foreach ($documentProduct['samePromotion'] as $keysp => $samePromotion) {

                $productPromotion = ProductPromotion::where('product_id', $samePromotion['product_id'])
                    ->where('promotion_id', $documentProduct['promotion_id'])
                    ->first();

                if ($productPromotion) {
                    $document['total_discount'] += $samePromotion['product']['price']['price'] * $productPromotion->discount / 100;
                    $document['documents_products'][$keydp]['total_discount'] += $samePromotion['product']['price']['price'] * $productPromotion->discount / 100;
                }
            }

        }

        return $document;
    }

    /**
     * Este Metodo se encarga de validar los productos
     * con promociones y juntarlos en uno
     *
     * @author Kevin Galindo
     */
    public function cartPromotionConcatenate($document, $request)
    {
        $model_id = null;

        //Validamos y capturamos el modelo
        if ($request->model_id) {
            $model_id = $request->model_id;
        } else {
            $voucher_type_code = $this->getVoucherTypeById($document['vouchertype_id']);
            if ($voucher_type_code == '001') {
                $model_id = Template::where('code', '010')->first()->id;
            }
        }

        // Buscamos el modelo
        $model = Template::find($model_id);

        if (count($document['documents_products']) <= 0) return $document;

        // Variables
        $products_without_promotions = [];
        $promotion_grouped = [];

        foreach ($document['documents_products'] as $key => $documentProduct) {

            // Validar si el documento no tiene promocion
            if ($documentProduct['promotion_id'] == null) {
                if ($model->pay_taxes) {
                    $documentProduct['total_value_iva'] = collect($documentProduct['taxes'])->where('code', '10')->first()['value'];
                }
                $products_without_promotions[] = $documentProduct;
                continue;
            }

            if ($documentProduct['promotion'] != null) {
                $documentProduct['promotion']['comply'] = false;
            }

            foreach ($documentProduct['promotion']['products_promotions'] as $keyPr => $productPromotion) {
                $documentProduct['promotion']['products_promotions'][$keyPr]['quantity'] = 0;
            }

            // Validar si ya esxiste un elemento agrupado en el array
            $key_promotion_grouped = array_search($documentProduct['promotion_id'], array_column($promotion_grouped, 'promotion_id'));

            // Si no existe se crea
            if (!is_numeric($key_promotion_grouped)) {
                $totalIva = 0;
                if ($model->pay_taxes) {
                    if (count($documentProduct['taxes']) > 0) {
                        $taxIva = collect($documentProduct['taxes'])->where('code', '10')->first();

                        if ($taxIva) {
                            $totalIva = $taxIva['value'];
                        }
                    }
                }

                $promotion_grouped[] = [
                    'promotion_id'     => $documentProduct['promotion_id'],
                    'total_value'      => $documentProduct['total_value'],
                    'total_value_brut' => $documentProduct['total_value_brut'],
                    'total_value_iva'  => $totalIva,
                    'quantity'         => $documentProduct['quantity'],
                    'samePromotion'    => [$documentProduct],
                    'is_benefit'       => $documentProduct['is_benefit'],
                    'product'          => $documentProduct['product'],
                    'promotion'        => $documentProduct['promotion']
                ];

                continue;
            }

            // Si existe se agrega
            $promotion_grouped[$key_promotion_grouped]['samePromotion'][] = $documentProduct;

            // Suman valores
            $promotion_grouped[$key_promotion_grouped]['total_value'] += $documentProduct['total_value'];
            $promotion_grouped[$key_promotion_grouped]['total_value_brut'] += $documentProduct['total_value_brut'];
            $promotion_grouped[$key_promotion_grouped]['quantity'] += $documentProduct['quantity'];

            $totalIva = 0;
            if ($model->pay_taxes) {
                if (count($documentProduct['taxes']) > 0) {
                    $taxIva = collect($documentProduct['taxes'])->where('code', '10')->first();

                    if ($taxIva) {
                        $totalIva = $taxIva['value'];
                    }
                }
            }

            $promotion_grouped[$key_promotion_grouped]['total_value_iva'] += $totalIva;


        }

        $document['documents_products'] = array_merge($products_without_promotions, $promotion_grouped);

        return $document;
    }

    public function getVoucherTypeById($id)
    {
        return VouchersType:: find($id)->code_voucher_type;
    }

    public function getVoucherTypeCode010()
    {
        return VouchersType:: where('code_voucher_type', '010')->first()->code_voucher_type;
    }

    /**
     * Este metodo se encarga de calcular los valores totales como:
     * - valor total
     * - valor total bruto
     * - total inpuestos
     * ---
     * ----
     * -----
     *
     * @author Kevin Galindo
     */
    public function calculateTotalsDocument($document, $request)
    {
        //
        $model_id = null;
        $document_vouchertype_code = $this->getVoucherTypeById($document->vouchertype_id);
        $vouchertype_010 = $this->getVoucherTypeCode010();

        $vouchertype_id = $document_vouchertype_code == '001' ? $vouchertype_010 : $document->vouchertype_id;

        // Definismos variables y seteamos variables
        $document->total_value = 0;
        $document->total_value_brut = 0;
        $document->total_value_iva = 0;
        $document_taxes = [];

        //Validamos y capturamos el modelo
        if ($request->model_id) {
            $model_id = $request->model_id;
        } else {
            if ($document_vouchertype_code == '001') {
                $model_id = Template::where('code', '010')->first()->id;
            }
        }

        $model = Template::find($model_id);

        // Traemos el tipo de operacion
        $operationType = $model->operationType;

        // Calcular las cantidades del document products
        foreach ($document->documentsProducts as $key => $item) {

            // Validamos el descuento
            if ($item->discount_value <= 0) {
                if ($item && (!empty($item->document->trm_value) && $item->document->trm_value > 0)) {
                    $totalValueBrut = (float)$item->unit_value_before_taxes * $item->quantity;
                } else {
                    $totalValueBrut = round($item->unit_value_before_taxes * $item->quantity);
                }

                $totalDiscountValue = $totalValueBrut * $item->discount_value;

                if ($totalDiscountValue > $totalValueBrut) {
                    $totalDiscountValue = $totalValueBrut;
                }

                if ($item && (!empty($item->document->trm_value) && $item->document->trm_value > 0)) {
                    $totalValueBrutWithDiscountValue = $totalValueBrut > 0 ? $totalValueBrut - $totalDiscountValue : 0;
                } else {
                    $totalValueBrutWithDiscountValue = $totalValueBrut > 0 ? round($totalValueBrut - $totalDiscountValue) : 0;
                }

            } else {
                if ($item && (!empty($item->document->trm_value) && $item->document->trm_value > 0)) {
                    $totalValueBrut = (float)$item->unit_value_before_taxes * $item->quantity;
                } else {
                    $totalValueBrut = round($item->unit_value_before_taxes * $item->quantity);
                }

                $totalDiscountValue = $item->discount_value;

                if ($totalDiscountValue > $totalValueBrut) {
                    $totalDiscountValue = $totalValueBrut;
                }

                if ($item && (!empty($item->document->trm_value) && $item->document->trm_value > 0)) {
                    $totalValueBrutWithDiscountValue = $totalValueBrut > 0 ? $totalValueBrut - $totalDiscountValue : 0;
                } else {
                    $totalValueBrutWithDiscountValue = $totalValueBrut > 0 ? round($totalValueBrut - $totalDiscountValue) : 0;
                }
            }

            // Seteamos variables del producto en el documento
            $document->documentsProducts[$key]->total_value = 0;
            $document->documentsProducts[$key]->total_value_brut = $totalValueBrutWithDiscountValue;
            $document->documentsProducts[$key]->total_value_iva = 0;
            $document->documentsProducts[$key]->discount_value = $totalDiscountValue;
            $is_benefit = $item->is_benefit;
            $totalTaxesValue = 0;

            // Validamos si se debe calcular los impuestos segun el modelo
            if ($model->pay_taxes) {
                // Capturamos los impuestos segun la linea
                $taxes = Product::find($item->product_id)->line->taxes;

                // Calculamos los impuestos
                if ($taxes) {

                    $taxesDocumentProduct = [];
                    foreach ($taxes as $keyt => $tax) {

                        $valueTax = 0;
                        $taxPercentage = 0;

                        // Ventas
                        if (
                            $operationType->code == "206" ||
                            $operationType->code == "205" ||
                            $operationType->code == "155" ||
                            $operationType->code == "010" ||
                            $operationType->code == "260" ||
                            $operationType->code == "156"
                        ) {
                            $taxPercentage = (float)$tax['percentage_sale'];
                        }

                        // Compras
                        if (
                            $operationType->code == "105" ||
                            $operationType->code == "020" ||
                            $operationType->code == "270" ||
                            $operationType->code == "015" ||
                            $operationType->code == "271"
                        ) {
                            $taxPercentage = (float)$tax['percentage_purchase'];
                        }

                        // Bruto
                        if ($tax->base == 1) {
                            $valueTax = round(round($item->total_value_brut) * $taxPercentage / 100);
                        }

                        $documentProductTax = DocumentProductTax::where('document_product_id', $item->id)
                            ->where('tax_id', $tax->id)
                            ->first();

                        $dataTax = [
                            'id'           => $tax->id,
                            'code'         => $tax->code,
                            'description'  => $tax->description,
                            'percentage'   => $taxPercentage, //$tax->tax_percentage
                            'base'         => $tax->base,
                            'base_value'   => $item->total_value_brut,
                            'minimum_base' => $tax->minimum_base,
                            'value'        => $valueTax //isset($documentProductTax->value) ? $documentProductTax->value : 0 //$valueTax
                        ];

                        $taxesDocumentProduct[] = $dataTax;
                        $totalTaxesValue += $dataTax['value'];

                        // Valores totales del document_product
                        $document->documentsProducts[$key]->total_value_iva = $tax->code == '10' ? $dataTax['value'] : 0;
                    }

                    $document->documentsProducts[$key]->taxes = $taxesDocumentProduct;
                }
            }

            // En caso de que sea un beneficio se cobra 0
            if ($is_benefit) $document->documentsProducts[$key]->total_value_brut = 0;

            // Sumar al VALOR TOTAL
            $document->documentsProducts[$key]->total_value += $document->documentsProducts[$key]->total_value_brut + $totalTaxesValue;

            // Valores totales del documento
            $document->total_value_brut += !empty($item->document->trm_value) && $item->document->trm_value > 0 ? $document->documentsProducts[$key]->total_value_brut : round($document->documentsProducts[$key]->total_value_brut);
            $document->total_value += !empty($item->document->trm_value) && $item->document->trm_value > 0 ? $document->documentsProducts[$key]->total_value : round($document->documentsProducts[$key]->total_value);
        }

        // Calcular los impuestos del documento
        if ($model->pay_taxes) {
            foreach ($document->documentsProducts as $key => $documentsProducts) {
                foreach ($documentsProducts->documentsProductsTaxes as $keytx => $tax) {
                    // Impuestos del documento
                    $key_doc_taxes = array_search($tax->tax->code, array_column($documentsProducts->taxes, 'code'));
                    $key_document_taxes = array_search($tax->tax->code, array_column($document_taxes, 'code'));
                    if (!is_numeric($key_document_taxes)) {
                        $document_taxes[] = [
                            "id"           => $tax->id,
                            "code"         => $tax->tax->code,
                            "description"  => $tax->tax->description,
                            "percentage"   => isset($documentsProducts->taxes[$key_doc_taxes]) ? $documentsProducts->taxes[$key_doc_taxes]['percentage'] : 0,
                            "base"         => 1,
                            "base_value"   => $documentsProducts->total_value_brut,
                            "minimum_base" => 0,
                            "value"        => $tax->value
                        ];
                    } else {
                        $document_taxes[$key_document_taxes]['value'] += round((int)$tax->value);
                    }
                }
            }
        }

        $document->taxes = $document_taxes;

        return $document;
    }

    /**
     * Este metodo se encarga de validar el
     * carrito para finalizarlo.
     *
     * @param Request $request Contiene los parámetros enviados por el usuario
     * @return array|object|string[]
     * @author Kevin Galindo
     */
    public function completeCart($request)
    {
        // Seteamos la zona horaria
        date_default_timezone_set('America/Bogota');

        $request->request->add(['model_id' => $model_id = Template::where('code', '010')->first()->id]);

        $document = $this->documentRepository->getDocument($request->id);

        // Validamos si esxiste documento
        if (!$document) {
            return [
                'code' => '404',
                'data' => 'Documento no encontrado'
            ];
        }

        // Validamos que la informacion de productos
        $productWithQuantity = $document->documentsProducts->where('document_id', $document->id)->where('quantity', '>', 0);

        if ($productWithQuantity->count() <= 0) {
            return (object)[
                'code' => '400',
                'data' => (object)[
                    'message' => 'Los productos no tienen cantidades que facturar.'
                ]
            ];
        }

        $model = Template::where('code', '010')->first();

        $redirectBranchoffice_id = [
            '288', // Duitama
            '284', // Ibague
            '307', // Bucaramanga
            '286', // SEDE VILLAVICENCIO
            '306', // SEDE CALI
        ];

        // Redireccionar SEDE
        if (Subscriber::where('identification', '900338016')->exists() && in_array($document->branchoffice_id, $redirectBranchoffice_id)) {
            // Traemos la SEDE de bogota.
            $branchOffice = BranchOffice::find(305);

            $document->update([
                'branchoffice_id'           => $branchOffice->id,
                'branchoffice_warehouse_id' => 279
            ]);

            foreach ($document->documentsProducts as $key => $dp) {
                $dp->branchoffice_warehouse_id = 279;
                $dp->save();
            }

            $document->branchoffice_id = $branchOffice->id;
            $document->branchoffice_warehouse_id = 279;
            $document->branchoffice = $branchOffice;
        }

        $document = $this->calculateTotalsDocument($document, $request);

        $cart = $document->toArray();

        $observation = $request->observation;
        $affects_prefix_document = $request->affects_prefix_document;
        $affects_number_document = $request->affects_number_document;

        // Actualizar valores
        Document::where('id', $cart['id'])->update([
            'operationType_id' => $model->operationType_id,
            'model_id'         => $model->id,
        ]);

        try {
            DB::beginTransaction();

            //#MANEJO DEL CONSECUTIVO
            $vouchersType = VouchersType::where('code_voucher_type', '010')
                ->where('branchoffice_id', $cart['branchoffice_id'])
                ->first();

            $consecutive = 0;

            //Validamos si existe comprobante
            if ($vouchersType) {
                // Asignamos valores
                $consecutive = $vouchersType->consecutive_number + 1;
                $exists = true;

                // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
                while ($exists) {
                    $aux = Document::where('consecutive', $consecutive)
                        ->where('vouchertype_id', $vouchersType->id)
                        ->where('in_progress', false)
                        ->exists();
                    if ($aux) {
                        ++$consecutive;
                    } else {
                        $exists = false;
                    }
                }

                // Actualizamos el consecutivo del tipo de comprobante.
                $vouchersType->consecutive_number = $consecutive;
                $vouchersType->save();
            } else {
                DB::rollBack();
                return [
                    'code' => '400',
                    'data' => [
                        'message' => 'Error al finalizar el pedido (tipo de comprobante no existe).'
                    ]
                ];
            }

            // Fecha de vencimiento
            $document_date = Carbon::parse(date('Y-m-d H:i:s'));
            $expiration_days = 0;

            if (!empty($cart['contact']['pay_days'])) {
                $expiration_days = $cart['contact']['pay_days'];
            }

            // Asignamos la fecha de vencimiento.
            $cart['due_date'] = $document_date->addDays($expiration_days);

            //Actualizamos el estado
            $this->updateBackOrderAndMissingValueDocument($document->id);
            $stateRes = $this->documentUpdateState($document->id);

            if (!$stateRes) {
                DB::rollBack();
                return [
                    'data' => 'Error al actualizar el estado del pedido.',
                    'code' => 505
                ];
            }

            // Actualizamos el documento.
            Document::where('id', $cart['id'])->update([
                'vouchertype_id'          => $vouchersType->id,
                'in_progress'             => false,
                'consecutive'             => $consecutive,
                'total_value'             => $cart['total_value'],
                'total_value_brut'        => $cart['total_value_brut'],
                'observation'             => $observation,
                'prefix'                  => $vouchersType->prefix,
                'due_date'                => $cart['due_date'],
                'operationType_id'        => $model->operationType_id,
                'document_date'           => Carbon::parse(date('Y-m-d H:i:s')),
                'model_id'                => $model->id,
                'affects_prefix_document' => $affects_prefix_document,
                'affects_number_document' => $affects_number_document,
            ]);

            $document = Document::with(['warehouse', 'seller'])->find($cart['id']);

            // Confirmamos cambios.
            DB::commit();

        } catch (\Exception $e) {
            Log::error('Error al finalizar el pedido: ' . $e->getMessage() . 'Line: ' . $e->getLine() . ' File: ' . $e->getFile());
            DB::rollBack();
            return [
                'data' => 'Error al actualizar: ' . $e->getMessage() . 'Line: ' . $e->getLine() . ' File: ' . $e->getFile(),
                'code' => 400
            ];
        }


        // EMAIL
        try {
            if (config('app.send_emails')) {
                $recipients = [];

                if (filter_var($document->seller->email, FILTER_VALIDATE_EMAIL)) {
                    $recipients[] = $document->seller->email;
                }

                if (!is_null($cart['warehouse']) && !is_null($cart['warehouse']['branchoffice_id'])) {
                    $branchOffice = BranchOffice::select('email')->findOrFail($cart['warehouse']['branchoffice_id']);
                    if (filter_var($branchOffice->email, FILTER_VALIDATE_EMAIL)) {
                        $recipients[] = $branchOffice->email;
                    }
                }

                if (count($recipients) > 0) {
                    $file_name = $this->generatePdfOrder($this->pdf, $this->documentRepository, $document);
                    $document->filename = "/storage/pdf/" . $file_name;
                    Log::info('//---------------------- Enviando confirmación de pedido No. ' . $document->consecutive . '  a ' . json_encode($recipients));
                    $email = new NotificationOrderEmail($document->toArray(), $file_name);
                    Mail::to($recipients)->send($email);
                }
            }
        } catch (\Exception $e) {
            Log::error('Error enviar email: ' . $e->getMessage() . 'Line: ' . $e->getLine() . ' File: ' . $e->getFile());
        }

        return [
            'data' => $cart,
            'code' => 200
        ];
    }


    /**
     * Este metodo se encarga de devolver el documento
     * segun el ID.
     *
     * @author Kevin Galindo
     */
    public function show($id, $request)
    {
        $document = $this->documentRepository->getDocument($id);
        $document = $this->calculateTotalsDocument($document, $request);

        if (empty($document)) {
            return [
                'data' => 'Documento No encontrado',
                'code' => '404'
            ];
        }

        // foreach ($document->documentsProducts as $key => $documentProduct) {
        //     // Back Order
        //     $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request->toArray(), false);
        //     $documentProduct->back_order = $bo['backorder'];

        //     // IVA
        //     //$documentProduct->total_value_iva = $documentProduct->documentProductTaxes->where('tax_id', 1)->sum('value');
        // }

        return [
            'data' => $document,
            'code' => '200'
        ];
    }


    public function viewDocument($id, $request)
    {
        $taxesDoc = [];
        $document = $this->documentRepository->getDocument($id);
        $sale = $document->operationType->affectation == 2;

        if (empty($document)) {
            return [
                'data' => 'Documento No encontrado',
                'code' => '404'
            ];
        }

        foreach ($document->documentsProducts as $key => $documentProduct) {
            // Back Order
            // $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request->toArray(), false);
            // if ($bo) {
            //     $documentProduct->back_order = $bo['backorder'];
            // }

            // IVA
            $documentProduct->total_value_iva = $documentProduct->documentProductTaxes->where('tax_id', 1)->sum('value');

            // Inpuestos
            foreach ($documentProduct->documentProductTaxes as $key => $documentProductTax) {
                // Validamos si el tax ya existe en el arreglo.
                $key_tax = array_search(
                    $documentProductTax->tax_id,
                    array_column($taxesDoc, 'tax_id')
                );

                // Crear
                if ($key_tax === false) {
                    $taxesDoc[] = [
                        'tax_id'      => $documentProductTax->tax_id,
                        'value'       => $documentProductTax->value,
                        'percentage'  => $sale ? $documentProductTax->tax->percentage_purchase : $documentProductTax->tax->percentage_sale,
                        'base_value'  => $documentProductTax->base_value,
                        'description' => $documentProductTax->tax->description
                    ];
                } // Actualizar
                else {
                    $taxesDoc[$key_tax]['value'] += $documentProductTax->value;
                }
            }
        }

        $document['taxes'] = $taxesDoc;

        $document = $this->discountValueUnitDocumentProduct($document->toArray());

        return [
            'data' => $document,
            'code' => '200'
        ];
    }


    /**
     * Este metodo se encarga de cambiar el estado del
     * documento a "CERRADO"
     *
     * @author Kevin Galindo
     */
    public function closeDocument($id, $request)
    {
        $state = $request->state;

        try {

            DB::beginTransaction();

            $document = Document::find($id);
            $document->closed = $state;
            $document->save();

            DB::commit();

            return [
                'code' => '200',
                'data' => [
                    'message' => 'Documento Actualizado.'
                ]
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'code' => '500',
                'data' => [
                    'message' => 'Error creando el documento del documento base "' . $e->getMessage() . '"'
                ]
            ];
        }

    }

    /**
     * Este metodo se encarga de calcular
     * la disponibilidad
     *
     * @author Kevin Galindo
     */
    public function calculateAvailability($request)
    {
        // Seteamos las variables
        $branchoffice_warehouse_id = $request->branchoffice_warehouse_id;
        $products = [];

        $request->merge([
            'voucher_type' => 4,
            'model_id'     => 1
        ]);

        // Traemos los documentos
        $documents = $this->baseDocuments($request);

        try {
            // DB::beginTransaction();

            // Actualizamos la disponibilidad al stock antes de hacer cambios.
            DB::update('update inventories
                    set available = stock
                    where branchoffice_warehouse_id = ?', [$request->branchoffice_warehouse_id]);

            // Recorremos los documentos para rellenar el array de productos.
            foreach ($documents as $key => $document) {

                // Recorremos los productos de los documentos
                foreach ($document->documentsProducts as $key => $documetProduct) {

                    //Validamos si tiene cantidad pendiente
                    if ($documetProduct->back_order > 0) {

                        $key_product_array = array_search(
                            $documetProduct->product_id,
                            array_column($products, 'product_id')
                        );

                        // Agregamos el producto al array.
                        if ($key_product_array === false) {
                            $products[] = [
                                'product_id'                => $documetProduct->product_id,
                                'back_order'                => $documetProduct->back_order,
                                'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id
                            ];
                        } // Actualizamos.
                        else {
                            // Validamos que la bodega sea la misma
                            if ($products[$key_product_array]['branchoffice_warehouse_id'] == $document->branchoffice_warehouse_id) {
                                $products[$key_product_array]['back_order'] += $documetProduct->back_order;
                            } // En caso de ser diferente se crea.
                            else {
                                $products[] = [
                                    'product_id'                => $documetProduct->product_id,
                                    'back_order'                => $documetProduct->back_order,
                                    'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id
                                ];
                            }
                        }
                    }
                }
            }

            // Recorremos los productos y actualizamos la disponibilidad
            foreach ($products as $key => $product) {

                // Traemos el registro del inventario segun el product_id
                $inventory = Inventory::where('product_id', $product['product_id'])
                    ->where('branchoffice_warehouse_id', $product['branchoffice_warehouse_id'])
                    ->first();

                // Validamos si hay registro
                if ($inventory) {
                    // Calculamos la disponibilidad, stock - cantidad pendiente.
                    $available = $inventory->stock - $product['back_order'];
                    if ($available < 0) {
                        $available = 0;
                    }

                    // Se actualiza la disponibilidad.
                    $inventory->available = $available;
                    $inventory->save();
                } // Si no hay registro se crea
                else {
                    $available = 0 - $documetProduct->back_order;
                    Inventory::create([
                        'branchoffice_warehouse_id' => $product['branchoffice_warehouse_id'],
                        'product_id'                => $product['product_id'],
                        'stock'                     => 0,
                        'stock_values'              => 0,
                        'average_cost'              => 0,
                        'available'                 => $available,
                    ]);
                }
            }

            return true;

            // DB::commit();
        } catch (\Exception $e) {
            // DB::rollBack();
            return false;
        }

    }

    /**
     * Este Metodo calcula toda la disponibilidad
     * de las bodegas.
     */
    public function calculateAvailabilityAllWarehouse()
    {
        set_time_limit(0);

        $branch_office_warehouses = BranchOfficeWarehouse::where('warehouse_code', '01')->get();
        $model = Template::find('1');
        $data = [];

        foreach ($branch_office_warehouses as $key => $branch_office_warehouse) {
            // Seteamos las variables
            $products = [];
            $branchoffice_warehouse_id = $branch_office_warehouse->id; //279
            $branchoffice_id = $branch_office_warehouse->branchoffice_id; //305

            // Traemos el tipo de comprobante.
            $voucherType = VouchersType::where('code_voucher_type', '010')
                ->where('branchoffice_id', $branchoffice_id)
                ->first();

            // Traemos los documentos
            $documents = Document::where('vouchertype_id', $voucherType->id)
                ->where('in_progress', false)
                ->where('closed', false)
                ->where('branchoffice_id', $branchoffice_id)
                ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
                ->orderBy('id', 'ASC')
                ->get();

            // Validamos si ya fueron finalizados
            foreach ($documents as $key => $document) {

                // $request = [
                //     'document_id' => $document->id,
                //     'model_id'    => 1
                // ];

                // $documentsBackOrder = [];

                // foreach ($document->documentsProducts as $keyDp => $documentProduct) {
                //     if ($documentProduct->quantity > 0) {
                //         $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request, false);
                //         $documentsBackOrder[] = $bo;
                //         $document->documentsProducts[$keyDp]['back_order'] = $bo['backorder'];
                //     }
                // }


                $sumBackOrder = $document->backorder_quantity; //collect($documentsBackOrder)->sum('backorder');

                if ($sumBackOrder == 0) {
                    $documents[$key]['complete'] = true;
                } else {
                    $documents[$key]['complete'] = false;
                }

            }

            $documents = $documents->where('complete', false);

            // Validamos si el pedido esta retenido
            foreach ($documents as $key => $document) {

                // Esta variable guarda el motivo de la retencion.
                $motiveRejection = '';

                // Validamos si el pedido fue aprobado.
                if (!empty($document->status_id_authorized) && $document->status_id_authorized == 912) {
                    $documents[$key]['detained'] = false;
                    continue;
                }

                // Validamos si el pedido fue rechazado.
                if (!empty($document->status_id_authorized) && $document->status_id_authorized == 913) {
                    $documents[$key]['detained'] = true;
                    continue;
                }

                ## --FECHA DE VENCIMIENTO YA ESTE EXPIRADA-- ##
                // Traemos las facturas del cliente.
                $invoices = Document::select('document_date', 'due_date', 'operationType_id', 'contact_id', 'id')
                    ->where('contact_id', $document->contact_id)
                    ->where('operationType_id', 5)
                    ->has('receivable')
                    ->get();

                if ($invoices->where('facturaVencida', true)->count() > 0) {
                    $motiveRejection = 'Cartera Vencida';
                }

                ## --CUPO DISPONIBLE-- ##
                $totalWallet = 0;
                $totalQuotaCredit = 0;
                $availableQuota = 0;

                // Total Cartera
                $totalWallet = $invoices->sum(function ($item) {
                    return round($item->receivable[0]->invoice_balance, 0, PHP_ROUND_HALF_UP);
                });

                // Cupo de credito
                $totalQuotaCredit = $document->contact->customer->credit_quota;

                // Cupo disponible
                $availableQuota = $totalQuotaCredit - $totalWallet;

                if ($document->total_value > $availableQuota) {
                    $motiveRejection = empty($motiveRejection) ? 'Cupo insuficiente' : $motiveRejection . ' / Cupo insuficiente';
                }

                // validamos si el pedido tiene algun motivo de retencion.
                if (!empty($motiveRejection)) {

                    $documents[$key]['detained'] = true;

                } // No retenido
                else {
                    $documents[$key]['detained'] = false;
                }
            }

            // Traemos los que no estan retenidos.
            $documents = $documents->where('detained', false);

            try {
                DB::beginTransaction();

                // Actualizamos la disponibilidad al stock antes de hacer cambios.
                DB::update('update inventories
                set available = stock
                where branchoffice_warehouse_id = ?', [$branchoffice_warehouse_id]);

                $products = [];

                // Recorremos los documentos para rellenar el array de productos.
                foreach ($documents as $key => $document) {

                    // Recorremos los productos de los documentos
                    foreach ($document->documentsProducts as $key => $documetProduct) {

                        if ($documetProduct['backorder_quantity'] == 0) {
                            continue;
                        }

                        $key_product_array = array_search(
                            $documetProduct->product_id,
                            array_column($products, 'product_id')
                        );

                        // Agregamos el producto al array.
                        if ($key_product_array === false) {
                            $products[] = [
                                'product_id'                => $documetProduct->product_id,
                                'back_order'                => $documetProduct['backorder_quantity'],
                                'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id,
                            ];
                        } // Actualizamos.
                        else {
                            // Validamos que la bodega sea la misma
                            if ($products[$key_product_array]['branchoffice_warehouse_id'] == $document->branchoffice_warehouse_id) {
                                $products[$key_product_array]['back_order'] += $documetProduct['backorder_quantity'];
                            } // En caso de ser diferente se crea.
                            else {
                                $products[] = [
                                    'product_id'                => $documetProduct->product_id,
                                    'back_order'                => $documetProduct['backorder_quantity'],
                                    'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id,
                                ];
                            }
                        }

                    }
                }

                // Recorremos los productos y actualizamos la disponibilidad
                foreach ($products as $key => $product) {

                    // Traemos el registro del inventario segun el product_id
                    $inventory = Inventory::where('product_id', $product['product_id'])
                        ->where('branchoffice_warehouse_id', $product['branchoffice_warehouse_id'])
                        ->first();

                    // Validamos si hay registro
                    if ($inventory) {
                        // Calculamos la disponibilidad, stock - cantidad pendiente.
                        $available = $inventory->stock - $product['back_order'];
                        if ($available < 0) {
                            $available = 0;
                        }

                        // Se actualiza la disponibilidad.
                        $inventory->available = $available;
                        $inventory->save();
                    } // Si no hay registro se crea
                    else {
                        $available = 0 - $documetProduct->backorder_quantity;
                        Inventory::create([
                            'branchoffice_warehouse_id' => $product['branchoffice_warehouse_id'],
                            'product_id'                => $product['product_id'],
                            'stock'                     => 0,
                            'stock_values'              => 0,
                            'average_cost'              => 0,
                            'available'                 => $available,
                        ]);
                    }
                }

                DB::commit();

                $data[] = [
                    'sede'               => !empty($branch_office_warehouse->branchoffice) ? $branch_office_warehouse->branchoffice->name : 'N/A',
                    'bodega_id'          => $branch_office_warehouse->id,
                    'cantidad_productos' => count($products),
                    'cantidad_pedidos'   => count($documents)
                ];

            } catch (\Exception $e) {
                DB::rollBack();
                return 'Error:"' . $e->getMessage() . '"' . $e->getLine() . "File: " . $e->getFile();
            }
        }

        return $data;
    }


    /**
     * crear traslados
     *
     * @param Request $request
     * @return JsonResponse
     * @author Santiago Torres
     */
    public function createTransfer($request)
    {
        set_time_limit(0);
        try {
            DB::beginTransaction();

            if (isset($request->params["document_id"])) {

                $model = Template::find(34);

                $in_progress = true;
                $user_id_solicit_authorization = null;
                $date_solicit_authorization = null;

                // Validar Auhtorizaciones
                if ($model->authorizedUsers->count() > 0) {
                    $userAuth = $model->authorizedUsers->where('user_id', $request->params["contact_id"]["id"])->first();

                    if (!$userAuth) {
                        //necesita autorizacion
                        $in_progress = true;
                        $pending_authorization = true;
                        $user_id_solicit_authorization = $request->params["contact_id"]["id"];
                        $date_solicit_authorization = date('Y-m-d H:i:s');
                        $finalize_document = false;
                    } else {
                        $in_progress = false;
                        $pending_authorization = false;
                        $userAuth = false;
                        $finalize_document = true;
                    }

                }

                // actualizar el traslado
                $transfer_entrace = Document::where('id', $request->params["document_id"])
                    ->with('documentsProducts')->first();
                $transfer = Document:: where('id', $transfer_entrace->thread)
                    ->with('documentsProducts')->first();

                // $transfer->branchoffice_id                  =   $request->params["sede_salida"];
                // $transfer->branchoffice_warehouse_id        =   $request->params["bodega_salida"];
                $transfer->observation = $request->params["observation"];
                $transfer->pending_authorization = $pending_authorization;
                $transfer->in_progress = $in_progress;
                $transfer->save();

                //almacena tercero adicional
                if (isset($request->params["additional_contact_id"]) && !is_null($request->params["additional_contact_id"])) {
                    DocumentInformation:: create([
                        'document_id'                     => $transfer->id,
                        'additional_contact_id'           => $request->params["additional_contact_id"],
                        'additional_contact_warehouse_id' => $request->params["additional_contact_warehouse_id"]
                    ]);
                }

                // $transfer_entrace->branchoffice_id                  =   $request->params["sede_entrada"];
                // $transfer_entrace->branchoffice_warehouse_id        =   $request->params["bodega_entrada"];
                $transfer_entrace->observation = $request->params["observation"];
                $transfer_entrace->pending_authorization = $pending_authorization;
                $transfer_entrace->in_progress = $in_progress;
                $transfer_entrace->save();

                foreach ($request->product["products_transfer"] as $key => $products_transfer) {
                    if (isset($products_transfer["product"]["documents_product_id"])) {
                        $documents_products_transfer = DocumentProduct:: where('product_id', $products_transfer["product"]["id"])->where('document_id', $transfer->id)->first();
                        $documents_products_transfer_entrace = DocumentProduct:: where('product_id', $products_transfer["product"]["id"])->where('document_id', $transfer_entrace->id)->first();

                        // $documents_products_transfer->branchoffice_warehouse_id  =   $request->params["bodega_salida"];
                        $documents_products_transfer->quantity = $products_transfer["minimum_amount"];
                        $documents_products_transfer->save();
                        // $documents_products_transfer_entrace->branchoffice_warehouse_id = $request->params["bodega_entrada"];
                        $documents_products_transfer_entrace->quantity = $products_transfer["minimum_amount"];
                        $documents_products_transfer_entrace->save();
                    } else {
                        $validation = Inventory::where('product_id', $products_transfer["product"]["id"])
                            ->where('branchoffice_warehouse_id', $request->params["bodega_salida"])
                            ->first();
                        $product = Product:: find($products_transfer["product"]["id"]);
                        $documents_products_data = [
                            'document_id'               => $transfer->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $validation ? $validation->average_cost : 0,
                            'total_value_brut'          => $validation ? $validation->average_cost * $products_transfer["minimum_amount"] : 0,
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $request->params["bodega_salida"],
                            'operationType_id'          => 4
                        ];
                        $documents_products_transfer = DocumentProduct:: create($documents_products_data);

                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $documents_products_data["unit_value_before_taxes"],
                            'total_value_brut'          => $documents_products_data["unit_value_before_taxes"] * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $request->params["bodega_entrada"],
                            'operationType_id'          => 3
                        ];
                        $documents_products_transfer_entrace = DocumentProduct:: create($documents_products_data_entrace);

                    }
                    if ($finalize_document) {
                        $inva = $this->inventoryRepository->finalize($documents_products_transfer->id);
                        $inv = $this->inventoryRepository->finalize($documents_products_transfer_entrace->id);

                        $transfer->in_progress = false;
                        $transfer->save();

                        $transfer_entrace->in_progress = false;
                        $transfer_entrace->save();
                    }
                }


            } else {
                //autorizacion
                $model = Template::find(34);

                $in_progress = true;
                $user_id_solicit_authorization = null;
                $date_solicit_authorization = null;

                // Validar Auhtorizaciones
                if ($model->authorizedUsers->count() > 0) {
                    $userAuth = $model->authorizedUsers->where('user_id', $request->params["contact_id"]["id"])->first();

                    if (!$userAuth) {
                        //necesita autorizacion
                        $in_progress = true;
                        $pending_authorization = true;
                        $user_id_solicit_authorization = $request->params["contact_id"]["id"];
                        $date_solicit_authorization = date('Y-m-d H:i:s');
                        $finalize_document = false;
                    } else {
                        $pending_authorization = false;
                        $userAuth = false;
                        $finalize_document = true;
                    }
                } else {
                    $pending_authorization = false;
                    $userAuth = false;
                    $finalize_document = true;
                }


                $requestparamsbodega_salida = '';
                $from_service_request = false;
                if (isset($request->params["bodega_salida"])) {
                    $requestparamsbodega_salida = $request->params["bodega_salida"];
                } else {
                    if (isset($request->params["servicerequeststate"])) {
                        $from_service_request = true;
                        switch ($request->params["servicerequeststate"]) {
                            case 10:
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                                break;

                            case 11:
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                                break;

                            default :
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_salida"])->where('warehouse_code', '01')->first()->id;
                                break;
                        }
                    }
                }

                //Validar la existencia
                $productNotStock = [];

                foreach ($request->product['products_transfer'] as $key => $productTransfer) {
                    $stockProduct = $this->getStockProduct($productTransfer['product']['id'], $requestparamsbodega_salida);

                    if ($productTransfer['minimum_amount'] > $stockProduct) {
                        $productNotStock[] = [
                            'document_product_id' => null,
                            'product_id'          => $productTransfer['product']['id'],
                            'stock'               => $stockProduct
                        ];
                    }
                }

                if (count($productNotStock) > 0) {
                    return (object)[
                        'code'      => '404',
                        'completed' => false,
                        'data'      => (object)[
                            'message' => 'Existencia no Disponible:'
                        ]
                    ];
                }

                //actualizar bandera de la entrada por taslado
                if ($request->params["documentEntry"]) {
                    $documentEntry = Document::where('id', $request->params["documentEntry"])->first();
                    if ($documentEntry) {
                        $documentEntry->incorporated_document = true;
                        $documentEntry->save();
                    }
                }

                $branchOffice_code = BranchOffice::find($request->params["sede_salida"])->code;
                $subsciber_identification = Subscriber::first()->identification;
                $contact = Contact:: where('identification', $subsciber_identification)->first();
                $contact_warehouse = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code)->first()->id;

                $branchOffice_code_entrace = BranchOffice::find($request->params["sede_entrada"])->code;
                $contact_warehouse_entrace = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code_entrace)->first()->id;


                //se crea el documento traslado salida
                $consecutive = VouchersType::where('code_voucher_type', 100)->lockForUpdate()->first();
                $consecutive->consecutive_number = $consecutive->consecutive_number + 1;

                $exists = true;

                // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
                while ($exists) {
                    $aux = Document::where('consecutive', $consecutive->consecutive_number)
                        ->where('vouchertype_id', $consecutive->id)
                        ->where('in_progress', false)
                        ->exists();
                    if ($aux) {
                        ++$consecutive->consecutive_number;
                    } else {
                        $exists = false;
                    }
                }

                $consecutive->save();
                $this->validateBranchofficeTransfer($request->params["sede_salida"], $requestparamsbodega_salida);

                $data = [
                    'vouchertype_id'                => $consecutive->id,
                    'consecutive'                   => $consecutive->consecutive_number,
                    'user_id'                       => $request->params["contact_id"]["id"],
                    //'contact_id' => $request->params["contact_id"]["contact_id"],
                    'contact_id'                    => $contact->id,
                    'document_state'                => 1,
                    'in_progress'                   => $in_progress,
                    'branchoffice_id'               => $request->params["sede_salida"],
                    'branchoffice_warehouse_id'     => $requestparamsbodega_salida,
                    'operationType_id'              => 4,
                    "incorporated_document"         => false,
                    "observation"                   => $request->params["observation"],
                    "issue_date"                    => Carbon:: now(),
                    "warehouse_id"                  => $contact_warehouse,
                    'document_date'                 => date('Y-m-d H:i:s'),
                    'model_id'                      => 34,
                    'from_service_request'          => $from_service_request,
                    'pending_authorization'         => $pending_authorization,
                    'user_id_solicit_authorization' => $user_id_solicit_authorization,
                    'date_solicit_authorization'    => $date_solicit_authorization

                ];
                $transfer = Document::create($data);
                if (isset($request->params["additional_contact_id"]) && !is_null($request->params["additional_contact_id"])) {
                    DocumentInformation:: create([
                        'document_id'                     => $transfer->id,
                        'additional_contact_id'           => $request->params["additional_contact_id"],
                        'additional_contact_warehouse_id' => $request->params["additional_contact_warehouse_id"]
                    ]);
                }


                $requestparamsbodega_entrada = '';
                if (isset($request->params["bodega_entrada"])) {
                    $requestparamsbodega_entrada = $request->params["bodega_entrada"];
                } else {
                    switch ($request->params["servicerequeststate"]) {
                        case 5:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                            break;

                        case 10:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '04')->first()->id;
                            break;

                        case 11:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '11')->first()->id;
                            break;
                    }
                }
                $this->validateBranchofficeTransfer($request->params["sede_entrada"], $requestparamsbodega_entrada);

                //traslado entrada
                $data_entrace = [
                    'vouchertype_id'                => $consecutive->id,
                    'consecutive'                   => $consecutive->consecutive_number,
                    'user_id'                       => $request->params["contact_id"]["id"],
                    //'contact_id' => $request->params["contact_id"]["contact_id"],
                    'contact_id'                    => $contact->id,
                    'document_state'                => 1,
                    'in_progress'                   => $in_progress,
                    'branchoffice_id'               => $request->params["sede_entrada"],
                    'branchoffice_warehouse_id'     => $requestparamsbodega_entrada,
                    'thread'                        => $transfer->id,
                    'operationType_id'              => 3,
                    "incorporated_document"         => false,
                    "observation"                   => $request->params["observation"],
                    "issue_date"                    => Carbon:: now(),
                    "warehouse_id"                  => $contact_warehouse_entrace,
                    'document_date'                 => date('Y-m-d H:i:s'),
                    'model_id'                      => 35,
                    'from_service_request'          => $from_service_request,
                    'pending_authorization'         => $pending_authorization,
                    'user_id_solicit_authorization' => $user_id_solicit_authorization,
                    'date_solicit_authorization'    => $date_solicit_authorization
                ];

                $transfer_entrace = Document::create($data_entrace);
                $total_value = 0;
                foreach ($request->product["products_transfer"] as $key => $products_transfer) {
                    $validation = Inventory::where('product_id', $products_transfer["product"]["id"])
                        ->where('branchoffice_warehouse_id', $requestparamsbodega_salida)
                        ->first();
                    $product = Product:: find($products_transfer["product"]["id"]);

                    $documents_products_data = [
                        'document_id'               => $transfer->id,
                        'product_id'                => $products_transfer["product"]["id"],
                        'line_id'                   => $product->line_id,
                        'subline_id'                => $product->subline_id,
                        'brand_id'                  => $product->brand_id,
                        'quantity'                  => $products_transfer["minimum_amount"],
                        'unit_value_before_taxes'   => $validation ? $validation->average_cost : 0,
                        'total_value_brut'          => $validation ? $validation->average_cost * $products_transfer["minimum_amount"] : 0,
                        'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                        'branchoffice_warehouse_id' => $requestparamsbodega_salida,
                        'operationType_id'          => 4
                    ];

                    $total_value_brut_transfer = $validation->average_cost * $products_transfer["minimum_amount"];

                    $total_value += $documents_products_data['total_value_brut'];

                    $documents_products_transfer = DocumentProduct::create($documents_products_data);

                    if ($finalize_document) {
                        $inva = $this->inventoryRepository->finalize($documents_products_transfer->id);
                    }


                    $inventory_entrace = Inventory::where('product_id', $products_transfer["product"]["id"])
                        ->where('branchoffice_warehouse_id', $requestparamsbodega_entrada)
                        ->first();

                    if (isset($request->params["servicerequeststate"]) && $request->params["servicerequeststate"] == 10) {
                        $producto_chatarra = Product:: find($request->product_id);
                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $producto_chatarra->id,
                            'line_id'                   => $producto_chatarra->line_id,
                            'subline_id'                => $producto_chatarra->subline_id,
                            'brand_id'                  => $producto_chatarra->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $request->unit_value,
                            'total_value_brut'          => $request->unit_value * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'operationType_id'          => 3,
                            'cost_ref'                  => $total_value_brut_transfer,
                            'branchoffice_warehouse_id' => $requestparamsbodega_entrada,
                            'fixed_cost'                => true
                        ];
                    } else {
                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $documents_products_data["unit_value_before_taxes"],
                            'total_value_brut'          => $documents_products_data["unit_value_before_taxes"] * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $requestparamsbodega_entrada,
                            'operationType_id'          => 3
                        ];
                    }


                    $documents_products_transfer_entrace = DocumentProduct::create($documents_products_data_entrace);

                    if ($finalize_document) {
                        $inv = $this->inventoryRepository->finalize($documents_products_transfer_entrace->id);

                        $transfer->in_progress = false;
                        $transfer->save();

                        $transfer_entrace->in_progress = false;
                        $transfer_entrace->save();
                    }
                }

                //total traslado
                $transfer->total_value = $total_value;
                $transfer->save();

                $transfer_entrace->total_value = $total_value;
                $transfer_entrace->save();

            }

            if ($transfer) {
                if ($request->params["pdf"]) {
                    //crear pdf
                    $transfer_s = $this->documentRepository->searchTransfer($transfer->id);
                    $transfer->invoice_link = "/storage/pdf/" . $this->generatePdftransfer($transfer_s, true);
                    $transfer->save();
                    $transfer_entrace->invoice_link = $transfer->invoice_link;
                    $transfer_entrace->save();

                    $transfer->file_name = $transfer->invoice_link;

                    // Actualizar el estado de la solicitud de servicio
                    // if (!empty($request->params['servicerequest_id'])) {
                    //     $serviceRequestStatusHistory = ServiceRequestStatusHistory::find($request->params['servicerequest_id']);
                    //     dd($serviceRequestStatusHistory);
                    //     $serviceRequestStatusHistory->document_id = $transfer->id;
                    //     $serviceRequestStatusHistory->save();
                    // }


                }
            }
            DB:: commit();
            return (object)[
                'code' => '200',
                'data' => (object)[
                    'data'    => $transfer,
                    'message' => 'Se Ha creado Exítosamente'
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() . "\n"
                ]
            ];
        }

    }

    /**
     * validar bodegas en traslados
     *
     * @param Request $request
     * @author Santiago Torres
     */
    public function validateBranchofficeTransfer($branchoffice, $branchoffice_warehouse_id)
    {
        $branchoffice_warehouse = BranchOfficeWarehouse:: select('id', 'branchoffice_id')->find($branchoffice_warehouse_id);
        if ($branchoffice_warehouse->branchoffice_id != $branchoffice) {
            throw new \Exception("Ha ocurrido un error intentelo de nuevo mas tarde");
        }
    }

    /**
     * crear traslados rotacion garantias
     *
     * @param Request $request
     * @return JsonResponse
     * @author Santiago Torres
     */
    public function createTransferRot($request)
    {
        set_time_limit(0);
        try {
            DB::beginTransaction();

            if (isset($request->params["document_id"])) {

                $model = Template::where('code', 243)->first();

                $in_progress = true;
                $user_id_solicit_authorization = null;
                $date_solicit_authorization = null;

                // Validar Auhtorizaciones
                if ($model->authorizedUsers->count() > 0) {

                    $userAuth = $model->authorizedUsers->where('user_id', $request->params["contact_id"]["id"])->first();

                    if (!$userAuth) {
                        //necesita autorizacion
                        $in_progress = true;
                        $pending_authorization = true;
                        $user_id_solicit_authorization = $request->params["contact_id"]["id"];
                        $date_solicit_authorization = date('Y-m-d H:i:s');
                        $finalize_document = false;
                    } else {
                        $in_progress = false;
                        $pending_authorization = false;
                        $userAuth = false;
                        $finalize_document = true;
                    }

                }

                // actualizar el traslado
                $transfer_entrace = Document::where('id', $request->params["document_id"])
                    ->with('documentsProducts')->first();
                $transfer = Document:: where('id', $transfer_entrace->thread)
                    ->with('documentsProducts')->first();

                // $transfer->branchoffice_id                  =   $request->params["sede_salida"];
                // $transfer->branchoffice_warehouse_id        =   $request->params["bodega_salida"];
                $transfer->observation = $request->params["observation"];
                $transfer->pending_authorization = $pending_authorization;
                $transfer->in_progress = $in_progress;
                $transfer->save();

                if (isset($request->params["additional_contact_id"]) && !is_null($request->params["additional_contact_id"])) {
                    DocumentInformation:: create([
                        'document_id'                     => $transfer->id,
                        'additional_contact_id'           => $request->params["additional_contact_id"],
                        'additional_contact_warehouse_id' => $request->params["additional_contact_warehouse_id"]
                    ]);
                }

                // $transfer_entrace->branchoffice_id                  =   $request->params["sede_entrada"];
                // $transfer_entrace->branchoffice_warehouse_id        =   $request->params["bodega_entrada"];
                $transfer_entrace->observation = $request->params["observation"];
                $transfer_entrace->pending_authorization = $pending_authorization;
                $transfer_entrace->in_progress = $in_progress;
                $transfer_entrace->save();

                foreach ($request->product["products_transfer"] as $key => $products_transfer) {
                    if (isset($products_transfer["product"]["documents_product_id"])) {
                        $documents_products_transfer = DocumentProduct:: where('product_id', $products_transfer["product"]["id"])->where('document_id', $transfer->id)->first();
                        $documents_products_transfer_entrace = DocumentProduct:: where('product_id', $products_transfer["product"]["id"])->where('document_id', $transfer_entrace->id)->first();

                        // $documents_products_transfer->branchoffice_warehouse_id  =   $request->params["bodega_salida"];
                        $documents_products_transfer->quantity = $products_transfer["minimum_amount"];
                        $documents_products_transfer->save();
                        // $documents_products_transfer_entrace->branchoffice_warehouse_id = $request->params["bodega_entrada"];
                        $documents_products_transfer_entrace->quantity = $products_transfer["minimum_amount"];
                        $documents_products_transfer_entrace->save();
                    } else {
                        $validation = Inventory::where('product_id', $products_transfer["product"]["id"])
                            ->where('branchoffice_warehouse_id', $request->params["bodega_salida"])
                            ->first();
                        $product = Product:: find($products_transfer["product"]["id"]);
                        $documents_products_data = [
                            'document_id'               => $transfer->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $validation ? $validation->average_cost : 0,
                            'total_value_brut'          => $validation ? $validation->average_cost * $products_transfer["minimum_amount"] : 0,
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $request->params["bodega_salida"],
                            'operationType_id'          => 4
                        ];
                        $documents_products_transfer = DocumentProduct:: create($documents_products_data);

                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $documents_products_data["unit_value_before_taxes"],
                            'total_value_brut'          => $documents_products_data["unit_value_before_taxes"] * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $request->params["bodega_entrada"],
                            'operationType_id'          => 3
                        ];
                        $documents_products_transfer_entrace = DocumentProduct:: create($documents_products_data_entrace);

                    }
                    if ($finalize_document) {
                        $inva = $this->inventoryRepository->finalize($documents_products_transfer->id);
                        $inv = $this->inventoryRepository->finalize($documents_products_transfer_entrace->id);

                        $transfer->in_progress = false;
                        $transfer->save();

                        $transfer_entrace->in_progress = false;
                        $transfer_entrace->save();
                    }
                }


            } else {
                //autorizacion
                $model = Template::where('code', 243)->first();

                $in_progress = true;
                $user_id_solicit_authorization = null;
                $date_solicit_authorization = null;

                // Validar Auhtorizaciones
                if ($model->authorizedUsers->count() > 0) {
                    $userAuth = $model->authorizedUsers->where('user_id', $request->params["contact_id"]["id"])->first();

                    if (!$userAuth) {
                        //necesita autorizacion
                        $in_progress = true;
                        $pending_authorization = true;
                        $user_id_solicit_authorization = $request->params["contact_id"]["id"];
                        $date_solicit_authorization = date('Y-m-d H:i:s');
                        $finalize_document = false;
                    } else {
                        $pending_authorization = false;
                        $userAuth = false;
                        $finalize_document = true;
                    }
                } else {
                    $pending_authorization = false;
                    $userAuth = false;
                    $finalize_document = true;
                }


                $requestparamsbodega_salida = '';
                $from_service_request = false;
                if (isset($request->params["bodega_salida"])) {
                    $requestparamsbodega_salida = $request->params["bodega_salida"];
                } else {
                    if (isset($request->params["servicerequeststate"])) {
                        $from_service_request = true;
                        switch ($request->params["servicerequeststate"]) {
                            case 10:
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                                break;

                            case 11:
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                                break;

                            default :
                                $requestparamsbodega_salida = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_salida"])->where('warehouse_code', '01')->first()->id;
                                break;
                        }
                    }
                }

                //Validar la existencia
                $productNotStock = [];

                foreach ($request->product['products_transfer'] as $key => $productTransfer) {
                    $stockProduct = $this->getStockProduct($productTransfer['product']['id'], $requestparamsbodega_salida);

                    if ($productTransfer['minimum_amount'] > $stockProduct) {
                        $productNotStock[] = [
                            'document_product_id' => null,
                            'product_id'          => $productTransfer['product']['id'],
                            'stock'               => $stockProduct
                        ];
                    }
                }

                if (count($productNotStock) > 0) {
                    return (object)[
                        'code'      => '404',
                        'completed' => false,
                        'data'      => (object)[
                            'message' => 'Existencia no Disponible:'
                        ]
                    ];
                }

                //actualizar bandera de la entrada por taslado
                if ($request->params["documentEntry"]) {
                    $documentEntry = Document::where('id', $request->params["documentEntry"])->first();
                    if ($documentEntry) {
                        $documentEntry->incorporated_document = true;
                        $documentEntry->save();
                    }
                }

                $branchOffice_code = BranchOffice::find($request->params["sede_salida"])->code;
                $contact = Contact:: where('identification', '900338016')->first();
                $contact_warehouse = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code)->first()->id;

                $branchOffice_code_entrace = BranchOffice::find($request->params["sede_entrada"])->code;
                $contact_warehouse_entrace = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code_entrace)->first()->id;

                //se crea el documento traslado salida

                // listamos la bodega va a ir el traslado
                $code_branchOffice_warehouse = BranchOfficeWarehouse:: find($request->params["bodega_entrada"]);
                $code_voucher_type_exit = 243;
                $code_voucher_type_entrace = 143;
                if ($code_branchOffice_warehouse->warehouse_code == '17') {
                    $code_voucher_type_exit = 241;
                    $code_voucher_type_entrace = 141;
                }

                $consecutive = VouchersType::where('code_voucher_type', $code_voucher_type_exit)->where('branchoffice_id', $request->params["sede_entrada"])->lockForUpdate()->first();
                $consecutive->consecutive_number = $consecutive->consecutive_number + 1;

                $exists = true;

                // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
                while ($exists) {
                    $aux = Document::where('consecutive', $consecutive->consecutive_number)
                        ->where('vouchertype_id', $consecutive->id)
                        ->where('in_progress', false)
                        ->exists();
                    if ($aux) {
                        ++$consecutive->consecutive_number;
                    } else {
                        $exists = false;
                    }
                }


                $consecutive_entrace = VouchersType::where('code_voucher_type', $code_voucher_type_entrace)->where('branchoffice_id', $request->params["sede_entrada"])->lockForUpdate()->first();
                $consecutive_entrace->consecutive_number = $consecutive->consecutive_number;
                $consecutive_entrace->save();
                $consecutive->save();

                $model_transfer = Template::where('code', 243)->first();
                $model_transfer_entrace = Template::where('code', 143)->first();


                $this->validateBranchofficeTransfer($request->params["sede_salida"], $requestparamsbodega_salida);
                $data = [
                    'vouchertype_id'                => $consecutive->id,
                    'consecutive'                   => $consecutive->consecutive_number,
                    'user_id'                       => $request->params["contact_id"]["id"],
                    //'contact_id' => $request->params["contact_id"]["contact_id"],
                    'contact_id'                    => $contact->id,
                    'document_state'                => 1,
                    'in_progress'                   => $in_progress,
                    'branchoffice_id'               => $request->params["sede_salida"],
                    'branchoffice_warehouse_id'     => $requestparamsbodega_salida,
                    'operationType_id'              => 4,
                    "incorporated_document"         => false,
                    "observation"                   => $request->params["observation"],
                    "issue_date"                    => Carbon:: now(),
                    "warehouse_id"                  => $contact_warehouse,
                    'document_date'                 => date('Y-m-d H:i:s'),
                    'model_id'                      => $model_transfer->id, //modelo de la salida traslados rot gar
                    'from_service_request'          => $from_service_request,
                    'pending_authorization'         => $pending_authorization,
                    'user_id_solicit_authorization' => $user_id_solicit_authorization,
                    'date_solicit_authorization'    => $date_solicit_authorization

                ];
                $transfer = Document::create($data);
                if (isset($request->params["additional_contact_id"]) && !is_null($request->params["additional_contact_id"])) {
                    DocumentInformation:: create([
                        'document_id'                     => $transfer->id,
                        'additional_contact_id'           => $request->params["additional_contact_id"],
                        'additional_contact_warehouse_id' => $request->params["additional_contact_warehouse_id"]
                    ]);
                }

                $requestparamsbodega_entrada = '';
                if (isset($request->params["bodega_entrada"])) {
                    $requestparamsbodega_entrada = $request->params["bodega_entrada"];
                } else {
                    switch ($request->params["servicerequeststate"]) {
                        case 5:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '02')->first()->id;
                            break;

                        case 10:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '04')->first()->id;
                            break;

                        case 11:
                            $requestparamsbodega_entrada = BranchOfficeWarehouse:: where('branchoffice_id', $request->params["sede_entrada"])->where('warehouse_code', '11')->first()->id;
                            break;
                    }
                }
                //traslado entrada
                $this->validateBranchofficeTransfer($request->params["sede_entrada"], $requestparamsbodega_entrada);
                $data_entrace = [
                    'vouchertype_id'                => $consecutive_entrace->id,
                    'consecutive'                   => $consecutive_entrace->consecutive_number,
                    'user_id'                       => $request->params["contact_id"]["id"],
                    //'contact_id' => $request->params["contact_id"]["contact_id"],
                    'contact_id'                    => $contact->id,
                    'document_state'                => 1,
                    'in_progress'                   => $in_progress,
                    'branchoffice_id'               => $request->params["sede_entrada"],
                    'branchoffice_warehouse_id'     => $requestparamsbodega_entrada,
                    'thread'                        => $transfer->id,
                    'operationType_id'              => 3,
                    "incorporated_document"         => false,
                    "observation"                   => $request->params["observation"],
                    "issue_date"                    => Carbon:: now(),
                    "warehouse_id"                  => $contact_warehouse_entrace,
                    'document_date'                 => date('Y-m-d H:i:s'),
                    'model_id'                      => $model_transfer_entrace->id, //modelo de la salida traslados rot gar
                    'from_service_request'          => $from_service_request,
                    'pending_authorization'         => $pending_authorization,
                    'user_id_solicit_authorization' => $user_id_solicit_authorization,
                    'date_solicit_authorization'    => $date_solicit_authorization
                ];

                $transfer_entrace = Document::create($data_entrace);
                $total_value = 0;
                foreach ($request->product["products_transfer"] as $key => $products_transfer) {
                    $validation = Inventory::where('product_id', $products_transfer["product"]["id"])
                        ->where('branchoffice_warehouse_id', $requestparamsbodega_salida)
                        ->first();
                    $product = Product:: find($products_transfer["product"]["id"]);

                    $documents_products_data = [
                        'document_id'               => $transfer->id,
                        'product_id'                => $products_transfer["product"]["id"],
                        'line_id'                   => $product->line_id,
                        'subline_id'                => $product->subline_id,
                        'brand_id'                  => $product->brand_id,
                        'quantity'                  => $products_transfer["minimum_amount"],
                        'unit_value_before_taxes'   => $validation ? $validation->average_cost : 0,
                        'total_value_brut'          => $validation ? $validation->average_cost * $products_transfer["minimum_amount"] : 0,
                        'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                        'branchoffice_warehouse_id' => $requestparamsbodega_salida,
                        'operationType_id'          => 4
                    ];

                    $total_value_brut_transfer = $validation->average_cost * $products_transfer["minimum_amount"];

                    $total_value += $documents_products_data['total_value_brut'];

                    $documents_products_transfer = DocumentProduct::create($documents_products_data);

                    if ($finalize_document) {
                        $inva = $this->inventoryRepository->finalize($documents_products_transfer->id);
                    }


                    $inventory_entrace = Inventory::where('product_id', $products_transfer["product"]["id"])
                        ->where('branchoffice_warehouse_id', $requestparamsbodega_entrada)
                        ->first();

                    if (isset($request->params["servicerequeststate"]) && $request->params["servicerequeststate"] == 10) {
                        $producto_chatarra = Product:: find($request->product_id);
                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $producto_chatarra->id,
                            'line_id'                   => $producto_chatarra->line_id,
                            'subline_id'                => $producto_chatarra->subline_id,
                            'brand_id'                  => $producto_chatarra->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $request->unit_value,
                            'total_value_brut'          => $request->unit_value * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'operationType_id'          => 3,
                            'cost_ref'                  => $total_value_brut_transfer,
                            'branchoffice_warehouse_id' => $requestparamsbodega_entrada,
                            'fixed_cost'                => true
                        ];
                    } else {
                        $documents_products_data_entrace = [
                            'document_id'               => $transfer_entrace->id,
                            'product_id'                => $products_transfer["product"]["id"],
                            'line_id'                   => $product->line_id,
                            'subline_id'                => $product->subline_id,
                            'brand_id'                  => $product->brand_id,
                            'quantity'                  => $products_transfer["minimum_amount"],
                            'unit_value_before_taxes'   => $documents_products_data["unit_value_before_taxes"],
                            'total_value_brut'          => $documents_products_data["unit_value_before_taxes"] * $products_transfer["minimum_amount"],
                            'manage_inventory'          => Product::find($products_transfer["product"]["id"])->manage_inventory,
                            'branchoffice_warehouse_id' => $requestparamsbodega_entrada,
                            'operationType_id'          => 3
                        ];
                    }


                    $documents_products_transfer_entrace = DocumentProduct::create($documents_products_data_entrace);

                    if ($finalize_document) {
                        $inv = $this->inventoryRepository->finalize($documents_products_transfer_entrace->id);

                        $transfer->in_progress = false;
                        $transfer->save();

                        $transfer_entrace->in_progress = false;
                        $transfer_entrace->save();
                    }
                }

                //total traslado
                $transfer->total_value = $total_value;
                $transfer->save();

                $transfer_entrace->total_value = $total_value;
                $transfer_entrace->save();

            }
            if ($transfer) {
                if ($request->params["pdf"]) {
                    //crear pdf
                    $transfer_s = $this->documentRepository->searchTransfer($transfer->id);

                    $transfer->invoice_link = "/storage/pdf/" . $this->generatePdftransfer($transfer_s, true);
                    $transfer->save();
                    $transfer_entrace->invoice_link = $transfer->invoice_link;
                    $transfer_entrace->save();

                    $transfer->file_name = $transfer->invoice_link;

                    // Actualizar el estado de la solicitud de servicio
                    // if (!empty($request->params['servicerequest_id'])) {
                    //     $serviceRequestStatusHistory = ServiceRequestStatusHistory::find($request->params['servicerequest_id']);
                    //     dd($serviceRequestStatusHistory);
                    //     $serviceRequestStatusHistory->document_id = $transfer->id;
                    //     $serviceRequestStatusHistory->save();
                    // }


                }
            }
            DB:: commit();
            return (object)[
                'code' => '200',
                'data' => (object)[
                    'data'    => $transfer,
                    'message' => 'Se Ha creado Exítosamente'
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() . "\n" . $e->getLine() . $e->getFile()
                ]
            ];
        }

    }

    /**
     * Este metodo se encarga de cargar los documentos
     * de forma masiva.
     *
     * @author Kevin Galindo
     */
    public function loadDocumentMasive($request)
    {
        //Recibimos la información
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());
        $countLoad = 0;


        // Variables de errores
        $errorBranchOffice = [];
        $errorVouchersType = [];
        $errorProduct = [];
        $errorClient = [];
        $errorSeller = [];
        $codeVouchersType = '205';

        // Validamos que existan la informacion
        foreach ($fileData as $key => $item) {
            // PRODUCTOS
            if ($item['PRODUCTO'] != 0 && !Product::where('code', trim($item['PRODUCTO']))->exists()) {
                $keyErrorProduct = array_search($item['PRODUCTO'], array_column($errorProduct, 'codigo'));
                if (!is_numeric($keyErrorProduct)) {
                    $errorProduct[] = [
                        'codigo'  => $item['PRODUCTO'],
                        'mensaje' => 'No existe el PRODUCTO'
                    ];
                }
            }

            // SEDE
            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['COMPROBANTE'] . '%')->first();
            if ($item['COMPROBANTE'] != 0 && !$branchOffice) {
                $keyErrorBranchOffice = array_search($item['COMPROBANTE'], array_column($errorBranchOffice, 'codigo'));
                if (!is_numeric($keyErrorBranchOffice)) {
                    $errorBranchOffice[] = [
                        'codigo'  => $item['COMPROBANTE'],
                        'mensaje' => 'No existe la SEDE'
                    ];
                }
                continue;
            }

            // Comprobante
            if ($item['COMPROBANTE'] != "0") {

                $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                    ->where('code_voucher_type', $codeVouchersType)
                    ->first();

                if (!$vouchersType) {
                    $keyErrorVouchersType = array_search($item['COMPROBANTE'], array_column($errorVouchersType, 'codigo'));
                    if (!is_numeric($keyErrorVouchersType)) {
                        $errorVouchersType[] = [
                            'codigo'  => $item['COMPROBANTE'],
                            'id'      => $branchOffice->id,
                            'mensaje' => 'No existe TIPO DE COMPROBANTE ' . $codeVouchersType
                        ];
                    }
                    continue;
                }
            }

            // Cliente
            if ($item['PRODUCTO'] != 0 && !Contact::where('identification', trim($item['NIT']))->exists()) {
                $keyErrorClient = array_search($item['NIT'], array_column($errorClient, 'codigo'));
                if (!is_numeric($keyErrorClient)) {
                    $errorClient[] = [
                        'codigo'  => $item['NIT'],
                        'mensaje' => 'No existe el NIT del cliente'
                    ];
                }
            }

            // Cliente
            if ($item['PRODUCTO'] != 0 && !Contact::where('identification', trim($item['VENDEDOR']))->exists()) {
                $keyErrorSeller = array_search($item['VENDEDOR'], array_column($errorSeller, 'codigo'));
                if (!is_numeric($keyErrorSeller)) {
                    $errorSeller[] = [
                        'codigo'  => $item['VENDEDOR'],
                        'mensaje' => 'No existe el VENDEDOR del cliente'
                    ];
                }
            }
        }

        // Crear Documentos
        foreach ($fileData as $key => $item) {

            if ($item['PRODUCTO'] == "0") {
                continue;
            }

            $contact = Contact::where('identification', $item['NIT'])->first();
            $contactWarehouses = $contact->warehouses->where('code', '000')->first();

            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['COMPROBANTE'] . '%')->first();
            $branch_office_warehouses = $branchOffice->warehouses->where('warehouse_code', '01')->first();

            $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                ->where('code_voucher_type', $codeVouchersType)
                ->first();

            $newDocument = [
                'vouchertype_id' => $vouchersType->id,
                'consecutive'    => $item['NUMERO'],
                //'document_date' => '2020-06-05', //$item['FECHA'],
                'contact_id'     => $contact->id,

                'prefix'       => $vouchersType->prefix,
                'warehouse_id' => $contactWarehouses ? $contactWarehouses->id : null,

                'due_date'                  => null,
                'branchoffice_id'           => $branchOffice->id,
                'branchoffice_warehouse_id' => $branch_office_warehouses->id,
                'operationType_id'          => 7,
                'observation'               => 'load_masive_fac',
                'user_id'                   => 44
            ];

            // Validamos si ya existe el consecutivo
            if (!Document::where('consecutive', $newDocument['consecutive'])
                ->where('vouchertype_id', $newDocument['vouchertype_id'])
                ->exists()) {
                $this->documentRepository->create($newDocument);
            }


        }

        return response()->json('Registros creatos...', 200);
    }

    public function loadReceiptsPurchases($fileData, $request)
    {
        // Variables de errores
        $errorBranchOffice = [];
        $errorVouchersType = [];
        $errorProduct = [];
        $codeVouchersType = '105';

        // Validamos que existan la informacion
        foreach ($fileData as $key => $item) {
            // PRODUCTOS
            if ($item['PRODUCTO'] != 0 && !Product::where('code', trim($item['PRODUCTO']))->exists()) {
                $keyErrorProduct = array_search($item['PRODUCTO'], array_column($errorProduct, 'codigo'));
                if (!is_numeric($keyErrorProduct)) {
                    $errorProduct[] = [
                        'codigo'  => $item['PRODUCTO'],
                        'mensaje' => 'No existe el PRODUCTO'
                    ];
                }
            }


            // SEDE
            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['BODEGA'] . '%')->first();
            if ($item['BODEGA'] != 0 && !$branchOffice) {
                $keyErrorBranchOffice = array_search($item['BODEGA'], array_column($errorBranchOffice, 'codigo'));
                if (!is_numeric($keyErrorBranchOffice)) {
                    $errorBranchOffice[] = [
                        'codigo'  => $item['BODEGA'],
                        'mensaje' => 'No existe la SEDE'
                    ];
                }
                continue;
            }

            // Comprobante
            if ($item['BODEGA'] != "0") {

                $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                    ->where('code_voucher_type', $codeVouchersType)
                    ->first();

                if (!$vouchersType) {
                    $keyErrorVouchersType = array_search($item['BODEGA'], array_column($errorVouchersType, 'codigo'));
                    if (!is_numeric($keyErrorVouchersType)) {
                        $errorVouchersType[] = [
                            'codigo'  => $item['BODEGA'],
                            'id'      => $branchOffice->id,
                            'mensaje' => 'No existe TIPO DE COMPROBANTE ' . $codeVouchersType
                        ];
                    }
                    continue;
                }
            }
        }

        // Crear Documentos
        foreach ($fileData as $key => $item) {

            if ($item['BODEGA'] == "0") {
                continue;
            }

            $contact = Contact::where('identification', $item['NIT'])->first();
            $contactWarehouses = $contact->warehouses->where('code', '003')->first();

            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['BODEGA'] . '%')->first();
            $branch_office_warehouses = $branchOffice->warehouses->where('warehouse_code', '01')->first();

            $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                ->where('code_voucher_type', $codeVouchersType)
                ->first();

            $newDocument = [
                'vouchertype_id'            => $vouchersType->id,
                'consecutive'               => $item['NUMERO'],
                'document_date'             => $item['FECHA'],
                'contact_id'                => $contact->id,
                //'document_last_state'
                'prefix'                    => $vouchersType->prefix,
                'warehouse_id'              => $contactWarehouses->id,
                'affects_number_document'   => $item['FACTURAPROVEEDOR'],
                'due_date'                  => $item['FECHA_VENC'],
                'branchoffice_id'           => $branchOffice->id,
                'branchoffice_warehouse_id' => $branch_office_warehouses->id,
                'operationType_id'          => 7,
                'observation'               => 'load_masive',
                'user_id'                   => 44
            ];

            // Validamos si ya existe el consecutivo
            if (!Document::where('consecutive', $newDocument['consecutive'])
                ->where('vouchertype_id', $newDocument['vouchertype_id'])
                ->exists()) {
                $this->documentRepository->create($newDocument);
            }


        }

        // Crear productos
        foreach ($fileData as $key => $item) {

            if ($item['PRODUCTO'] == "0") {
                continue;
            }
            if ($item['BODEGA'] == "0") {
                continue;
            }

            $contact = Contact::where('identification', $item['NIT'])->first();
            $contactWarehouses = $contact->warehouses->where('code', '003')->first();

            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['BODEGA'] . '%')->first();
            $branch_office_warehouses = $branchOffice->warehouses->where('warehouse_code', '01')->first();

            $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                ->where('code_voucher_type', $codeVouchersType)
                ->first();

            $document = Document::where('consecutive', $item['NUMERO'])
                ->where('vouchertype_id', $vouchersType->id)
                ->first();

            $product = Product::where('code', trim($item['PRODUCTO']))->first();

            $newDocumentProduct = [
                'is_benefit'              => false,
                'quantity'                => $item['CANTIDAD'],
                'original_quantity'       => $item['CANTIDAD'],
                'model_id'                => 17,
                'seller_id'               => null,
                'promotion_id'            => null,
                'document_id'             => $document->id,
                'product_id'              => $product->id,
                'contact_id'              => $contact->id,
                'physical_unit'           => null,
                'unit_value_before_taxes' => $item['VALOR'],
                'discount_value'          => 0,
                'branchoffice_id'         => $branchOffice->id,
                'branchoffice_storage'    => $branchOffice->id,
                'validatePromotion'       => false
            ];


            $this->documentsProductsService->createDocumentProduct((array)$newDocumentProduct);
        }

        // Finalizar Documento
        foreach ($fileData as $key => $item) {

            if ($item['PRODUCTO'] == "0") {
                continue;
            }
            if ($item['BODEGA'] == "0") {
                continue;
            }

            $branchOffice = BranchOffice::where('name', 'ilike', '%' . $item['BODEGA'] . '%')->first();
            $branch_office_warehouses = $branchOffice->warehouses->where('warehouse_code', '01')->first();

            $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
                ->where('code_voucher_type', $codeVouchersType)
                ->first();

            $document = Document::where('consecutive', $item['NUMERO'])
                ->where('vouchertype_id', $vouchersType->id)
                ->first();


            $request->request->add([
                'document_id'               => $document->id,
                'seller_id'                 => null,
                'model_id'                  => 17,
                'branchoffice_warehouse_id' => $branch_office_warehouses->id,
                'consecutive'               => $item['NUMERO'],
                'observation'               => 'load_masive',
            ]);

            $this->finalizeDocument($request);

        }

        // Actualizamos 'affects_number_document'
        foreach ($fileData as $key => $item) {

            if ($item['PRODUCTO'] != "0") {
                continue;
            }
            if ($item['BODEGA'] != "0") {
                continue;
            }

            // $branchOffice = BranchOffice::where('name', 'ilike', '%'.$item['BODEGA'].'%')->first();

            // $vouchersType = VouchersType::where('branchoffice_id', $branchOffice->id)
            //                             ->where('code_voucher_type', $codeVouchersType)
            //                             ->first();

            $document = Document::where('consecutive', $item['NUMERO'])
                //->where('vouchertype_id', $vouchersType->id)
                ->first();


            $document->affects_number_document = $item['FACTURAPROVEEDOR'];
            $document->save();
        }
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $row = preg_replace('!\s+!', '', $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 0) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }


    /**
     * cusulta de documentos que afectan diponbilidad de un porducto
     *
     * @author santiago torres
     */
    public function getDocumentsAbailability($request)
    {
        $documents = $this->documentRepository->getDocumentsAbailability($request);
        return $documents;

    }

    public function getDespatchListInvoiced($invoiced = true, array $request = [])
    {
        // Variables
        $model_id = isset($request['model_id']) ? $request['model_id'] : 2;
        $model = Template::find($model_id);
        $data = [];

        // Filtros
        $filterText = isset($request['filter']) ? $request['filter'] : null;
        $product_id = isset($request['product_id']) ? $request['product_id'] : null;
        $despatch = isset($request['despatch']) ? $request['despatch'] : null;
        $search = isset($request['search']) ? $request['search'] : null;
        $filterDate = isset($request['date']) ? $request['date'] : null;
        $state = isset($request['state']) ? $request['state'] : null;
        $branchoffice_id = !empty($request['branchoffice_id']) ? $request['branchoffice_id'] : null;
        $closed = isset($request['closed']) ? $request['closed'] : false;
        $zone_id = isset($request['zone_id']) ? $request['zone_id'] : null;
        $selected_branchoffice_warehouse = isset($request['selected_branchoffice_warehouse']) ? $request['selected_branchoffice_warehouse'] : null;

        // Rango de fechas
        $fromDate = isset($request['fromDate']) ? $request['fromDate'] : null;
        $toDate = isset($request['toDate']) ? $request['toDate'] : null;


        // Traemos los tipos de comprobantes (documento / base)
        $vouchersType = VouchersType::where('code_voucher_type', $model->voucherType->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        $voucherTypeBase = VouchersType::where('code_voucher_type', $model->voucherType1->code_voucher_type)
            ->where('branchoffice_id', $branchoffice_id)
            ->first();

        //Realizamos la consulta
        $documents = Document::where('vouchertype_id', $vouchersType->id)
            ->where('in_progress', false)
            ->where('closed', $closed)
            ->with([
                'documentsProducts'    => function ($q) use ($selected_branchoffice_warehouse, $product_id) {
                    if (!empty($product_id)) {
                        $q->where('product_id', $product_id);
                    }
                    $q->with([
                        'product' => function ($qq) use ($selected_branchoffice_warehouse) {
                            $qq->with([
                                'line',
                                'inventory' => function ($query) use ($selected_branchoffice_warehouse) {
                                    $query->where('branchoffice_warehouse_id', $selected_branchoffice_warehouse);
                                }
                            ]);
                        },
                        'documentProductTaxes'
                    ]);
                },
                // 'invoices' => function ($q) use ($vouchersType) {
                //     $q->where('vouchertype_id', $vouchersType->id);
                //     $q->where('in_progress', false);
                //     $q->with([
                //         'documentsProducts' => function ($qq) {
                //             $qq->with(['product']);
                //         }
                //     ]);
                // },
                'warehouse'            => function ($q) {
                    $q->with(['zone']);
                },
                'documentsInformation' => function ($q) {
                    $q->with(['contact']);
                },
                'contact'              => function ($q) {
                    $q->with('customer');
                },
                'seller'               => function ($q) {
                    $q->select('id', 'identification', 'name', 'surname', 'address');
                },
                'vouchertype'
            ]);

        //filtro zona
        if ($zone_id) {
            $documents->WhereHas('warehouse', function ($q) use ($zone_id) {
                $q->WhereHas('zone', function ($q2) use ($zone_id) {
                    $q2->where('id', $zone_id);
                });
            });
        }

        // Validamos el filtro de texto
        if ($filterText) {
            $documents->where(function ($query) use ($filterText) {
                // Contact
                $query->orWhereHas('contact', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });

                // Seller
                $query->orWhereHas('seller', function ($query) use ($filterText) {
                    $query->where('identification', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('name', 'ilike', '%' . $filterText . '%');
                    $query->orWhere('surname', 'ilike', '%' . $filterText . '%');
                });
            });
        }

        // Validamos la SEDE
        if (!empty($branchoffice_id)) {
            $documents->where('branchoffice_id', $branchoffice_id);
        }

        // Validamos si hay algun filtro.
        if (!is_null($despatch)) {
            $documents->whereIn('id', $despatch);
        }

        if (!empty($search)) {
            $documents
                ->whereHas('contact', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('seller', function ($q) use ($search) {
                    $q->where('name', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('documentsProducts', function ($q) use ($search) {
                    $q->where('description', 'ilike', '%' . $search . '%');
                });
        }

        if (!empty($filterDate) && $filterDate != 'null') {
            $documents->where('document_date', '<=', $filterDate);
        }

        if (!empty($fromDate)) {
            $documents->whereBetween('due_date', [$fromDate, $toDate]);
        }

        // Validamos que estado debe devolver del pedido
        if (!empty($state) && $state == 'DESPACHADO') {
            $documents->whereHas('status', function ($q) {
                $q->where('code_parameter', '5');
            });
        } else {
            $documents->whereHas('status', function ($q) {
                $q->where('code_parameter', '!=', '5');
            });
        }

        // Traemos la informacion
        $documents = $documents->orderBy('id', 'ASC')->get();

        // Validamos
        foreach ($documents as $key => $document) {

            $documentsProductsBackOrder = [];

            // Traemos el BackOrder
            foreach ($document->documentsProducts as $key => $documentProduct) {
                $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request, false);
                $documentsProductsBackOrder[] = $bo;
                // $documentProduct->back_order = $bo['backorder'];
            }

            // Validar documentos repetidos
            $repeatQuantity = 0;
            $countDocumentProduct = count($document->documentsProducts);
            foreach ($data as $key => $documentR) {
                foreach ($documentR->documentsProducts as $key => $documentProductR) {
                    foreach ($document->documentsProducts as $key => $documentProduct) {

                        if ($document->vouchertype_id == $documentR->vouchertype_id &&
                            $document->thread == $documentR->thread &&
                            $documentProduct->back_order == $documentProductR->back_order &&
                            $documentProduct->created_at == $documentProductR->created_at &&
                            $documentProduct->product_id == $documentProductR->product_id) {
                            $repeatQuantity++;
                        }

                    }
                }
            }

            if ($repeatQuantity == $countDocumentProduct) continue;

            // validamos el BackOrder
            $count = 0;
            foreach ($documentsProductsBackOrder as $key => $documentProductBackOrder) {
                // if ($documentProductBackOrder['backorder'] > 0) {
                //     $count ++;
                // }
            }

            //Validamos que retornar
            if ($invoiced) {
                if ($count === 0) {
                    $data[] = $document;
                }
            } else {
                if ($count !== 0) {
                    $data[] = $document;
                }
            }
        }

        //Devolvemos la informacion
        return $data;
    }


    public function massiveOpeningBalances()
    {
        set_time_limit(0);

        //Recibimos la información
        // $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray(storage_path() . '/app/public/saldis iniciales.csv');

        //declaramos variables
        $does_not_exists = [];
        $new_products = [];
        $data = [];

        // revisamos la info del archivo
        foreach ($fileData as $key => $value) {
            //almacenamos el codeigo del producto
            $code = rtrim($value["REFERENCIA"]);
            //consultamos el producto
            $product = Product:: where('code', $code)->first();

            //validamos si no existe
            if (!$product) {

                //guardamos el codigo del producto
                $does_not_exists [] = $value["REFERENCIA"];

                // crear producto
                $new_product = Product:: create([
                    'code'        => trim($value["REFERENCIA"]),
                    'description' => trim($value["DESCRIPCION"]),
                    'line_id'     => 11,
                    'brand_id'    => 11,
                    'manage_inventory', true
                ]);

                $new_price_list = Price:: create([
                    'product_id'    => $new_product->id,
                    'price_list_id' => 924,
                    'price'         => 0
                ]);

            }

            //almacenamos los datos del csv
            $data[] = [
                'BODEGA'        => $value["BODEGA"],
                'UBICACION'     => $value["UBICACION"],
                'REFERENCIA'    => $value["REFERENCIA"],
                'DESCRIPCION'   => $value["DESCRIPCION"],
                'SALDOPRODUCTO' => $value["SALDOPRODUCTO"],
                'COSTO'         => $value["COSTO"],
            ];
        }

        //lista blanca sedes del csv con el respectivo id de la db
        $codesFileBranchoffices = [
            '0001 BODEGA PRINCIPAL         ' => '305',
            '0002 BODEGA CALI              ' => '306',
            '0003 BODEGA BUCARAMANGA       ' => '307',
            '0004 BODEGA BARRANQUILLA      ' => '308',
            '0005 BODEGA MEDELLIN          ' => '283',
            '0006 BODEGA VILLAVICENCIO     ' => '286',
            '0007 BODEGA IBAGUE            ' => '284',
            '0008 BODEGA MONTERIA          ' => '287',
            '0009 BODEGA YOPAL             ' => '288',
            '0010 BODEGA CUCUTA            ' => '285',

        ];

        // lista blanca bodegas
        $codesFileBranchofficesWarehouses = [
            'DISPONIBLES'         => '01',
            'DISPONIBLE'          => '01',
            'GARANTIAS'           => '03',
            'TRANSITO '           => '15',
            'RESERVAS '           => '06',
            'GARANTIAS GM'        => '14',
            'RECLAMOS PROVEDORES' => '08',
            'INACTIVO'            => '16',
            'ROTACIONES '         => '17',
            'ROTACIONES'          => '17',
            'SERVCIO TECNICO'     => '11',
            'SERVICIO TECNICO '   => '11',
            'AVERIAS'             => '09',
            'EXHIBIDORES'         => '10',
            'FV VIRTUAL'          => '15',
            'FV VIRTUAL '         => '15',
            'EXPORTACIONES'       => '12',
            'EXPORTACION'         => '12',
            'CHATARRA'            => '05',
            'RESERVA'             => '06',
            'RESERVA '            => '06',
            'RECLAMACION GM'      => '07',
        ];

        // creamos colleccion
        $collect = collect($data);

        // buscamos las sedes del documento
        $branchofficesData = $collect->unique('BODEGA');
        $branchofficesData->values()->all();


        //resopuesta
        $finalize = [];

        //recorremos las sedes
        foreach ($branchofficesData as $key => $branchoffice) {

            // buscamos en la colleccion los registros de la sade (en el csv la sede es la columna bodega)
            $dataLoasd = $collect->where('BODEGA', $branchoffice["BODEGA"]);

            $dataLoasd->all();

            $branchoffice_id_ = $codesFileBranchoffices[$branchoffice["BODEGA"]];

            // listamos las bodegas de la sede
            $branch_office_warehousesData = $dataLoasd->unique('UBICACION');

            $branch_office_warehousesData->values()->all();

            //recorremos las bodegas
            foreach ($branch_office_warehousesData as $key => $branch_office_warehouse) {


                $branchoffice_warehouse_id_ = BranchOfficeWarehouse:: where('warehouse_code', $codesFileBranchofficesWarehouses[$branch_office_warehouse["UBICACION"]])
                    ->where('branchoffice_id', $branchoffice_id_)
                    ->first();

                $datta = [];

                //recorremos la info de la sede
                foreach ($dataLoasd as $key => $value) {

                    //gardamos la info de la bodega (UBICACION) que sea igual a la de la iteración
                    if ($value["UBICACION"] == $branch_office_warehouse["UBICACION"]) {
                        $datta[] = [
                            'BODEGA'        => $value["BODEGA"],
                            'UBICACION'     => $value["UBICACION"],
                            'REFERENCIA'    => $value["REFERENCIA"],
                            'DESCRIPCION'   => $value["DESCRIPCION"],
                            'SALDOPRODUCTO' => $value["SALDOPRODUCTO"],
                            'COSTO'         => $value["COSTO"],
                        ];
                    }
                }


                //crear documento

                $vouchersType_ = VouchersType:: where('code_voucher_type', 150)->where('branchoffice_id', $branchoffice_id_)->first();

                $data_create_document = new Request();

                $data_create_document->merge(["model_id" => 3]);
                $data_create_document->merge(["prefix" => $vouchersType_->prefix]);
                $data_create_document->merge(["in_progress" => true]);
                $data_create_document->merge(["branchoffice_id" => $branchoffice_id_]);
                $data_create_document->merge(["documents_information" => ["price_list" => null]]);
                $data_create_document->merge(["branchoffice_warehouse_id" => $branchoffice_warehouse_id_->id]);
                $data_create_document->merge(["vouchertype_id" => $vouchersType_->id]);
                $contact_document = Contact:: where('identification', '900338016')->first();
                $data_create_document->merge(["contact_id" => $contact_document->id]);
                $data_create_document->merge(["user_id" => 44]);
                $data_create_document->merge(["document_date" => '2020-07-01 7:00:00']);


                // $data_create_document->request->add(["model_id" => $vouchersType_->id]); //pasarlo por get

                $document = $this->documentRepository->create($data_create_document->toArray());


                $data_create_document->merge(["seller_id" => null]);
                $data_create_document->merge(["document_id" => $document->id]);
                $data_create_document->merge(["promotion_id" => null]);

                foreach ($datta as $key => $productData) {
                    $product = Product:: where('code', trim($productData["REFERENCIA"]))->first();
                    if (trim($productData["SALDOPRODUCTO"]) != '') {
                        $data_create_document->merge(["product_id" => $product->id]);
                        $data_create_document->merge(["unit_value_before_taxes" => $productData["COSTO"]]);
                        $data_create_document->merge(["quantity" => $productData["SALDOPRODUCTO"]]);
                        $data_create_document->merge(["original_quantity" => $productData["SALDOPRODUCTO"]]);
                        $data_create_document->merge(["is_benefit" => false]);

                        $documentProduct = $this->documentsProductsService->createDocumentProduct($data_create_document->all());
                    }

                }


                //finalizar documento
                $data_create_document->merge(["ret_fue_prod_vr_limit" => 0]);
                $data_create_document->merge(["ret_fue_prod_percentage" => 0]);
                $data_create_document->merge(["ret_fue_prod_total" => 0]);
                $data_create_document->merge(["ret_fue_serv_vr_limit" => 0]);
                $data_create_document->merge(["ret_fue_serv_percentage" => 0]);
                $data_create_document->merge(["ret_fue_serv_total" => 0]);
                $data_create_document->merge(["ret_fue_total" => 0]);
                $data_create_document->merge(["ret_iva_prod_vr_limit" => 0]);
                $data_create_document->merge(["ret_iva_prod_percentage" => 0]);
                $data_create_document->merge(["ret_iva_prod_total" => 0]);
                $data_create_document->merge(["ret_iva_serv_vr_limit" => 0]);
                $data_create_document->merge(["ret_iva_serv_percentage" => 0]);
                $data_create_document->merge(["ret_iva_serv_total" => 0]);
                $data_create_document->merge(["ret_iva_total" => 0]);
                $data_create_document->merge(["ret_ica_prod_vr_limit" => 0]);
                $data_create_document->merge(["ret_ica_prod_percentage" => 0]);
                $data_create_document->merge(["ret_ica_prod_total" => 0]);
                $data_create_document->merge(["ret_ica_serv_vr_limit" => 0]);
                $data_create_document->merge(["ret_ica_serv_percentage" => 0]);
                $data_create_document->merge(["ret_ica_serv_total" => 0]);
                $data_create_document->merge(["ret_ica_total" => 0]);
                $data_create_document->merge(["documentProductsDifferences" => null]);
                $data_create_document->merge(["observation" => null]);
                $data_create_document->merge(["model_id" => 3]);

                $data_finalize = $this->finalizeDocument($data_create_document);

                if ($data_finalize->code != 200) {
                    dd($data_finalize);
                }

                $finalize [] = $data_finalize;//$this->finalizeDocument($data_create_document);
            }
        }
        return $finalize;
    }

    public function massiveOpeningBalancesColrecambios()
    {
        set_time_limit(0);
        try {
            DB::beginTransaction();

            //Recibimos la información
            $fileData = $this->csvToArray(storage_path() . '/app/public/saldis iniciales colrecambios.csv');

            //declaramos variables
            $does_not_exists = [];
            $new_products = [];
            $data = [];
            $line = Line::select('id')->where('line_code', '999')->first();

            // revisamos la info del archivo
            foreach ($fileData as $key => $value) {
                //almacenamos el codeigo del producto
                $code = rtrim($value["REFERENCIA"]);
                //consultamos el producto
                $product = Product:: where('previous_code_system', $code)->first();//cambiar a previous_code_system

                //validamos si no existe
                if (!$product) {
                    //guardamos el codigo del producto
                    $does_not_exists [] = $code;


                    // crear producto
                    $new_product = Product:: create([
                        'code'                 => $code,
                        'previous_code_system' => $code,//cambiar a previous_code_system
                        'description'          => trim($value["DESCRIPCION"]),
                        'line_id'              => $line->id,
                        'manage_inventory'     => true,
                    ]);
                    $new_products [] = $new_product->toArray();

                    $new_price_list = Price:: create([
                        'product_id'    => $new_product->id,
                        'price_list_id' => 924,
                        'price'         => 0
                    ]);

                }

                //almacenamos los datos del csv
                $data[] = [
                    'BODEGA'        => $value["BODEGA"],
                    'UBICACION'     => $value["UBICACION"],
                    'REFERENCIA'    => $value["REFERENCIA"],
                    'DESCRIPCION'   => $value["DESCRIPCION"],
                    'SALDOPRODUCTO' => $value["SALDOPRODUCTO"],
                    'COSTO'         => $value["COSTO"],
                ];
            }

            //lista blanca sedes del csv con el respectivo id de la db
            $codesFileBranchoffices = [
                'PRINCIPAL' => '5'
            ];

            // lista blanca bodegas con e code (NO el id) de la db
            $codesFileBranchofficesWarehouses = [
                'DISPONIBLE'    => '01',
                'NO DISPONIBLE' => '05'
            ];

            // creamos colleccion
            $collect = collect($data);

            // buscamos las sedes del documento
            $branchofficesData = $collect->unique('BODEGA');
            $branchofficesData->values()->all();


            //resopuesta
            $finalize = [];

            //recorremos las sedes
            foreach ($branchofficesData as $key => $branchoffice) {
                // buscamos en la colleccion los registros de la sade (en el csv la sede es la columna bodega)
                $dataLoasd = $collect->where('BODEGA', $branchoffice["BODEGA"]);

                $dataLoasd->all();

                $branchoffice_id_ = $codesFileBranchoffices[$branchoffice["BODEGA"]];

                // listamos las bodegas de la sede
                $branch_office_warehousesData = $dataLoasd->unique('UBICACION');
                $branch_office_warehousesData->values()->all();


                //recorremos las bodegas
                foreach ($branch_office_warehousesData as $key => $branch_office_warehouse) {


                    $branchoffice_warehouse_id_ = BranchOfficeWarehouse:: where('warehouse_code', $codesFileBranchofficesWarehouses[$branch_office_warehouse["UBICACION"]])
                        ->where('branchoffice_id', $branchoffice_id_)
                        ->first();

                    $datta = [];

                    //recorremos la info de la sede
                    foreach ($dataLoasd as $key => $value) {

                        //gardamos la info de la bodega (UBICACION) que sea igual a la de la iteración
                        if ($value["UBICACION"] == $branch_office_warehouse["UBICACION"]) {
                            $datta[] = [
                                'BODEGA'        => $value["BODEGA"],
                                'UBICACION'     => $value["UBICACION"],
                                'REFERENCIA'    => $value["REFERENCIA"],
                                'DESCRIPCION'   => $value["DESCRIPCION"],
                                'SALDOPRODUCTO' => $value["SALDOPRODUCTO"],
                                'COSTO'         => $value["COSTO"],
                            ];
                        }
                    }

                    //crear documento

                    $vouchersType_ = VouchersType:: where('code_voucher_type', 150)->where('branchoffice_id', $branchoffice_id_)->first();

                    $data_create_document = new Request();

                    $data_create_document->merge(["model_id" => 20]);
                    $data_create_document->merge(["prefix" => $vouchersType_->prefix]);
                    $data_create_document->merge(["in_progress" => true]);
                    $data_create_document->merge(["branchoffice_id" => $branchoffice_id_]);
                    $data_create_document->merge(["documents_information" => ["price_list" => null]]);
                    $data_create_document->merge(["branchoffice_warehouse_id" => $branchoffice_warehouse_id_->id]);
                    $data_create_document->merge(["vouchertype_id" => $vouchersType_->id]);
                    $contact_document = Contact:: where('identification', '830048991')->first();
                    $data_create_document->merge(["contact_id" => $contact_document->id]);
                    $data_create_document->merge(["user_id" => 44]);
                    $data_create_document->merge(["document_date" => '2020-12-31 07:00:00']);


                    // $data_create_document->request->add(["model_id" => $vouchersType_->id]); //pasarlo por get
                    $document = $this->documentRepository->create($data_create_document->toArray());

                    $data_create_document->merge(["seller_id" => null]);
                    $data_create_document->merge(["document_id" => $document->id]);
                    $data_create_document->merge(["promotion_id" => null]);

                    foreach ($datta as $key => $productData) {
                        $product = Product:: where('previous_code_system', trim($productData["REFERENCIA"]))->first();//cambiar a previous_code_system
                        if (trim($productData["SALDOPRODUCTO"]) != '') {
                            $data_create_document->merge(["product_id" => $product->id]);
                            $data_create_document->merge(["unit_value_before_taxes" => floatval(str_replace(',', '.', $productData["COSTO"]))]);
                            $data_create_document->merge(["quantity" => intval($productData["SALDOPRODUCTO"])]);
                            $data_create_document->merge(["original_quantity" => intval($productData["SALDOPRODUCTO"])]);
                            $data_create_document->merge(["is_benefit" => false]);

                            $this->documentsProductsService->createDocumentProduct($data_create_document);


                        } else {
                            Log::info(trim($productData["REFERENCIA"]));
                        }

                    }

                    //finalizar documento
                    $data_create_document->merge(["ret_fue_prod_vr_limit" => 0]);
                    $data_create_document->merge(["ret_fue_prod_percentage" => 0]);
                    $data_create_document->merge(["ret_fue_prod_total" => 0]);
                    $data_create_document->merge(["ret_fue_serv_vr_limit" => 0]);
                    $data_create_document->merge(["ret_fue_serv_percentage" => 0]);
                    $data_create_document->merge(["ret_fue_serv_total" => 0]);
                    $data_create_document->merge(["ret_fue_total" => 0]);
                    $data_create_document->merge(["ret_iva_prod_vr_limit" => 0]);
                    $data_create_document->merge(["ret_iva_prod_percentage" => 0]);
                    $data_create_document->merge(["ret_iva_prod_total" => 0]);
                    $data_create_document->merge(["ret_iva_serv_vr_limit" => 0]);
                    $data_create_document->merge(["ret_iva_serv_percentage" => 0]);
                    $data_create_document->merge(["ret_iva_serv_total" => 0]);
                    $data_create_document->merge(["ret_iva_total" => 0]);
                    $data_create_document->merge(["ret_ica_prod_vr_limit" => 0]);
                    $data_create_document->merge(["ret_ica_prod_percentage" => 0]);
                    $data_create_document->merge(["ret_ica_prod_total" => 0]);
                    $data_create_document->merge(["ret_ica_serv_vr_limit" => 0]);
                    $data_create_document->merge(["ret_ica_serv_percentage" => 0]);
                    $data_create_document->merge(["ret_ica_serv_total" => 0]);
                    $data_create_document->merge(["ret_ica_total" => 0]);
                    $data_create_document->merge(["documentProductsDifferences" => null]);
                    $data_create_document->merge(["observation" => null]);
                    $data_create_document->merge(["model_id" => 20]);
                    $data_create_document->merge(["document_date" => '2020-12-31 07:00:00']);

                    $user = User::find(44);
                    Auth::login($user);

                    $document->affects_document_id = $document->id;
                    $document->save();

                    $data_finalize = $this->finalizeDocument($data_create_document, true);
                    if ($data_finalize->code != 200) {
                        dd($data_finalize);
                    }

                    DocumentInformation::create([
                        'document_id'              => $document->id,
                        'document_previous_system' => true
                    ]);

                    $finalize [] = $data_finalize;//$this->finalizeDocument($data_create_document);
                }
            }
            DB::commit();
            return 'saldos iniciales cargados correctamente';
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            Log::info($e);
            return $e->getFile() . $e->getLine() . $e->getMessage();
        }

    }


    /**
     * Este metodo se encarga de corregir los documentyos
     */
    public function correctionDocuments($request)
    {
        // 1 = devoluciones
        // 5 = facturas
        set_time_limit(0);

        $cnt = 0;
        //Recibimos la información

        switch ($request->type) {
            case 'FECHA_VENCIMIENTO':
                return $this->recalculateDueDateDocument($request->operationType_id);
                break;
            case 'VALORES_DOC':
                return $this->recalculateDocument($request->operationType_id);
                break;
            case 'VALORES_DOC_RET':
                return $this->adjustRetention($request->operationType_id);
                break;
            case 'ADJUST_SPECIFIC_DOCUMENTS':
                return $this->adjustSpecificDocuments($request->operationType_id);
                break;
            case 'VALORES_DOC_ID':
                return $this->adjustValuestByDocumentId($request->document_id);
                break;
            case 'CARGAR_PAGO_DIAS':
                $file = $request->file('archivo_importar');
                $fileData = $this->csvToArray($file->getRealPath());

                foreach ($fileData as $key => $item) {
                    $contact = Contact::where('identification', $item['NIT'])->first();
                    if ($contact) {
                        $contact->pay_days = (int)$item['PAGO'];
                        $contact->save();
                        $cnt++;
                    }
                }

                return $cnt;

                break;
            case 'FECHA_DOCUMENTO':
                $file = $request->file('archivo_importar');
                $fileData = $this->csvToArray($file->getRealPath());

                return $this->adjustDocumentDate($fileData);

                break;
            case 'CORREGIR_IVA':
                return $this->recalculateIVA();
                break;
            case 'REASIGNAR_VENDEDOR_CLIENTE':
                $file = $request->file('archivo_importar');
                $fileData = $this->csvToArray($file->getRealPath());

                return $this->reAsignSellerToCustomer($fileData, $request);
                break;

            case 'CARGAR_PESO':
                $file = $request->file('archivo_importar');
                $fileData = $this->csvToArray($file->getRealPath());

                return $this->pesoProducts($fileData);
                break;

            case 'CORREGIR_RETENCIONES_PARCIAL':
                return $this->recalculateRetentionPartial($request->operationType_id);
                break;
            case 'DOCUMENTOS_VALORES_ERRONEOS':
                return $this->documentWrongValues($request->operationType_id);
                break;
            case 'DOCUMENTOS_VALORES_ERRONEOS_RETENCIONES':
                return $this->documentWrongValuesRetention($request->operationType_id);
                break;
            case 'DOCUMENTOS_CON_IVA_VALORES_EN_CERO':
                return $this->documentIvaWrongValues($request->operationType_id);
                break;
            case 'DEV_VALOR_MAYOR_FACTURA':
                return $this->documentDevGreaterThanTheFac($request);
                break;
            case 'DOCUMENTOS_QUITAR_RETENCIONES':
                return $this->documentRemoveRetention();
                break;

            case 'REASIGNAR_MOTIVO_DEV':
                $file = $request->file('archivo_importar');
                $fileData = $this->csvToArray($file->getRealPath());

                return $this->asignMotiveDevToDocument($fileData, $request);
                break;
        }

    }

    public function asignMotiveDevToDocument($fileData, $request)
    {
        $count = 0;
        foreach ($fileData as $key => $item) {
            $document = Document::find($item['ID']);
            $param = Parameter::where('paramtable_id', 18)->where('code_parameter', $item['CODIGODEVOLUCION'])->first();

            $document->handling_other_parameters_value = $param->id;
            $document->save();
            $count++;
        };

        return $count;
    }

    public function documentDevGreaterThanTheFac($request)
    {
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", 1)
            ->whereNotIn("model_id", [4, 44, 45])
            ->where("in_progress", false)
            ->where("closed", false)
            // ->where("id", 17598)
            ->where("document_date", '>=', '2020-10-01 00:00:00')
            ->get();

        $documentsError = [];

        foreach ($documents as $key => $document) {

            $thread = $document->document;

            if ($document->total_value > $thread->total_value) {
                $documentsError[] = [
                    'id'            => $document->id,
                    'consecutive'   => $document->consecutive,
                    'document_date' => $document->document_date,
                    'sede'          => $document->branchoffice->name,
                    'bodega'        => $document->branchofficeWarehouse->warehouse_description,
                    'modelo'        => $document->template->description,
                ];
            }
        }

        return [
            'total' => count($documentsError),
            'data'  => $documentsError
        ];
    }

    public function documentIvaWrongValues($operationType_id)
    {
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45])
            ->where("in_progress", false)
            ->where("closed", false)
            // ->where("id", 17598)
            // ->where("document_date", '>=', '2020-09-25 00:00:00')
            ->get();

        $documentsIvaWrongValues = [];

        foreach ($documents as $key => $document) {

            $model = Template::find($document->model_id);

            // Validamos si liquida impuestos.
            if ($model->pay_taxes) {
                foreach ($document->documentsProducts as $key => $dp) {

                    if ($dp->quantity <= 0) {
                        continue;
                    }

                    // Validamos que el producto maneje IVA
                    $manageIva = $dp->line->taxes->where('code', '10')->first() ? true : false;

                    if ($manageIva) {

                        // Validamos que exista registro del IVA
                        $saveIva = $dp->documentProductTaxes->where('tax_id', '1')->first() ? $dp->documentProductTaxes->where('tax_id', '1')->first() : false;

                        if ($saveIva) {
                            // Validamos que el valor del IVA sea mayor a 0
                            if ($saveIva->value <= 0) {
                                // dd($saveIva->toArray());
                                $documentsIvaWrongValues[] = $document->id;
                            }
                        } else {
                            $documentsIvaWrongValues[] = $document->id;
                        }
                    }
                }
            }

        }

        return [
            'total' => count($documentsIvaWrongValues),
            'data'  => $documentsIvaWrongValues
        ];
    }

    public function documentRemoveRetention()
    {
        $contact_id = 2500;
        $consecutives = [
            3372,
            3373,
            3374,
            3375,
            3376,
            3377,
            3378,
            3379,
            3380,
            3381,
            3382,
            3383,
            3384,
            3385,
            3386,
            3387,
            3388,
            3389,
            3390,
            3391,
            3392,
            3393,
            3394,
            3395,
            3396,
            3399,
            3400,
            3401,
            3402,
            3403,
            3404,
            3405,
            3406,
            3407,
            3408,
            3410,
            3411,
            3412,
            3413,
            3414,
            3415,
            3416,
            3417,
            3418,
            3419,
            3420,
            3421,
            3422,
        ];

        $documents = Document::where("in_progress", false)
            ->where("closed", false)
            ->where("contact_id", $contact_id)
            ->whereIn('consecutive', $consecutives)
            ->get();


        foreach ($documents as $key => $document) {
            Document::find($document->id)->update([
                # FUE
                // productos
                'ret_fue_prod_vr_limit'   => 0,
                'ret_fue_prod_percentage' => 0,
                'ret_fue_prod_total'      => 0,
                'ret_fue_base_value'      => 0,

                // servicios
                'ret_fue_serv_vr_limit'   => 0,
                'ret_fue_serv_percentage' => 0,
                'ret_fue_serv_total'      => 0,
                'ret_fue_serv_base_value' => 0,

                // total
                'ret_fue_total'           => 0,

                # IVA
                // productos
                'ret_iva_prod_vr_limit'   => 0,
                'ret_iva_prod_percentage' => 0,
                'ret_iva_prod_total'      => 0,
                'ret_iva_base_value'      => 0,

                // servicios
                'ret_iva_serv_vr_limit'   => 0,
                'ret_iva_serv_percentage' => 0,
                'ret_iva_serv_total'      => 0,
                'ret_iva_serv_base_value' => 0,

                // total
                'ret_iva_total'           => 0,

                # ICA
                // productos
                'ret_ica_prod_vr_limit'   => 0,
                'ret_ica_prod_percentage' => 0,
                'ret_ica_prod_total'      => 0,
                'ret_ica_base_value'      => 0,

                // servicios
                'ret_ica_serv_vr_limit'   => 0,
                'ret_ica_serv_percentage' => 0,
                'ret_ica_serv_total'      => 0,
                'ret_ica_serv_base_value' => 0,

                // total
                'ret_ica_total'           => 0,
            ]);

            $document->erp_id = null;
            $document->save();
        }


        return $documents->count();

    }

    public function adjustSpecificDocuments($operationType_id)
    {
        $docConsecutive = [
            1002,
            1003,
            1004,
            1005,
            1006,
            1007,
        ];

        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45])
            ->where("in_progress", false)
            ->where("closed", false)
            ->whereIn('consecutive', $docConsecutive)
            ->get();

        foreach ($documents as $key => $document) {

            foreach ($document->documentsProducts as $keyDp => $documentProduct) {

                $totalBrut = (int)$documentProduct->total_value_brut;
                $totalTaxes = 0;
                $taxes = $documentProduct->line->taxes;

                if ($taxes) {
                    foreach ($taxes as $keyTx => $tax) {
                        $percentage = (float)$tax->percentage_purchase;

                        $dataSave = [
                            "document_product_id" => $documentProduct->id,
                            "tax_id"              => $tax->id,
                            "base_value"          => $totalBrut,
                            "tax_percentage"      => $percentage,
                            "value"               => round(($totalBrut * $percentage) / 100)
                        ];


                        DocumentProductTax::updateOrCreate([
                            'document_product_id' => $dataSave['document_product_id'],
                            'tax_id'              => $dataSave['tax_id'],
                        ], $dataSave);
                    }
                }

                $totalTaxes = $documentProduct->documentProductTaxes->sum('value');
                $documentProduct->total_value = $totalBrut + $totalTaxes;
                $documentProduct->save();
            }

            $document->erp_id = null;
            $document->total_value = $document->documentsProducts->sum('total_value');
            $document->save();

        }


        return $documents->load([
            'documentsProducts'
        ]);
    }

    public function adjustValuestByDocumentId($document_id)
    {
        $document = Document::find($document_id);
        $totalBrutDoc = 0;
        $totalDoc = 0;

        $sale = $document->operationType->affectation == 2;

        $model = Template::find($document->model_id);

        foreach ($document->documentsProducts as $key => $documentProduct) {
            $totalBrut = round($documentProduct->unit_value_before_taxes * $documentProduct->quantity);
            $totalBrutWithDiscount = $totalBrut - $documentProduct->discount_value;
            $totalTaxes = 0;


            // Impuestos
            if ($model->pay_taxes) {
                if ($documentProduct->line->taxes->count() > 0) {
                    foreach ($documentProduct->line->taxes as $key => $tax) {

                        $taxPercentage = $sale ? $tax->percentage_sale : $tax->percentage_purchase;

                        $documentProductTax = [
                            'document_product_id' => $documentProduct->id,
                            'tax_id'              => $tax->id,
                            'base_value'          => $totalBrutWithDiscount,
                            'tax_percentage'      => $taxPercentage,
                            'value'               => round($totalBrutWithDiscount * $taxPercentage / 100)
                        ];

                        // Creamos el registro del impuesto
                        DocumentProductTax::updateOrCreate(
                            [
                                'document_product_id' => $documentProduct->id,
                                'tax_id'              => $tax->id
                            ],
                            $documentProductTax
                        );

                        // suma impuestos
                        $totalTaxes += $documentProductTax['value'];
                    }
                }
            } else {
                DocumentProductTax::where('document_product_id', $documentProduct->id)->delete();
            }

            if ($documentProduct->is_benefit) {
                $totalBrutWithDiscount = 0;
            }

            $documentProduct->total_value_brut = $totalBrutWithDiscount;
            $documentProduct->total_value = $totalBrutWithDiscount + $totalTaxes;
            $documentProduct->save();

            $totalBrutDoc += $documentProduct->total_value_brut;
            $totalDoc += $documentProduct->total_value;
        }

        // Totales documento
        $document->total_value_brut = $totalBrutDoc;
        $document->total_value = $totalDoc;
        $document->save();

        return $document;
    }

    public function documentWrongValuesRetention($operationType_id)
    {
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45, 46, 64])
            ->where("in_progress", false)
            ->where("closed", false)
            // ->where("id", 30085)
            ->where("document_date", '>=', '2020-09-01 00:00:00')
            ->get();

        foreach ($documents as $keyDoc => $document) {

            $sale = $document->operationType->affectation == 2;
            $model = Template::find($document->model_id);

            $totalBrutDoc = 0;
            $totalDoc = 0;

            $motive_wrong = [];

            foreach ($document->documentsProducts as $key => $documentProduct) {


            }

        }


        $dataReturn = $documents->where('wrong', true)->map(function ($item, $key) {
            if ($item->wrong) {
                return [
                    'id'           => $item->id,
                    'motive_wrong' => $item->motive_wrong,
                    'date'         => $item->document_date,
                ];
            }
        });

        return [
            'CANTIDAD' => $documents->where('wrong', true)->count(),
            'DATA'     => $dataReturn->values()
        ];
    }

    public function documentWrongValues($operationType_id)
    {
        set_time_limit(0);
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45, 46, 64])
            ->where("in_progress", false)
            ->where("closed", false)
            // ->where("id", 53097)
            ->where("document_date", '>=', '2020-12-09 00:00:00')
            ->get();

        $totalBrutDoc = 0;
        $totalDoc = 0;

        foreach ($documents as $keyDoc => $document) {

            $sale = $document->operationType->affectation == 2;
            $model = Template::find($document->model_id);

            $totalBrutDoc = 0;
            $totalDoc = 0;

            $motive_wrong = [];

            foreach ($document->documentsProducts as $key => $documentProduct) {
                if ($documentProduct->quantity === 0) {
                    continue;
                }
                $totalBrut = round($documentProduct->unit_value_before_taxes * $documentProduct->quantity);
                $totalBrutWithDiscount = $totalBrut - $documentProduct->discount_value;

                if ($documentProduct->discount_value === $totalBrut) {
                    $totalBrutWithDiscount = $totalBrut;
                }

                $totalTaxes = 0;
                $totalValue = 0;

                // Impuestos
                if ($model->pay_taxes) {
                    if ($documentProduct->line->taxes->count() > 0) {
                        foreach ($documentProduct->line->taxes as $key => $tax) {

                            $taxPercentage = $sale ? $tax->percentage_sale : $tax->percentage_purchase;

                            $documentProductTax = [
                                'document_product_id' => $documentProduct->id,
                                'tax_id'              => $tax->id,
                                'base_value'          => $totalBrutWithDiscount,
                                'tax_percentage'      => $taxPercentage,
                                'value'               => round(($totalBrutWithDiscount * $taxPercentage) / 100)
                            ];

                            // suma impuestos
                            $totalTaxes += $documentProductTax['value'];
                        }
                    }
                }

                if ($documentProduct->is_benefit) {
                    $totalBrutWithDiscount = 0;
                }

                $totalValue = $totalBrutWithDiscount + $totalTaxes;

                //----
                $tax_iva_percentage = 0;

                if ($documentProduct->is_benefit) {
                    $dp_total_value_brut_round = $totalBrutWithDiscount;
                    $dp_total_value_iva_round = $totalTaxes;
                } else if ($documentProduct->product->line->taxes->where('code', "10")->first()) {
                    if ($taxIva = $documentProduct->documentProductTaxes->where('tax_id', 1)->first()) {
                        $tax_iva_percentage = (int)$taxIva->tax_percentage;
                    }

                    if ($tax_iva_percentage != 0) {
                        // pasar a decimales
                        $tax_iva_percentage = $tax_iva_percentage / 100;

                        $dp_total_value_brut_round = round(($documentProduct->total_value / (1 + $tax_iva_percentage)) + 0.005, 2);
                        $dp_total_value_iva_round = round($documentProduct->total_value - $dp_total_value_brut_round, 2);

                    } else {
                        $dp_total_value_brut_round = $documentProduct->total_value_brut;
                        $dp_total_value_iva_round = 0;
                    }

                } else {
                    $dp_total_value_brut_round = $documentProduct->total_value_brut;
                    $dp_total_value_iva_round = 0;
                }

                $totalTaxes = $dp_total_value_iva_round;
                $totalBrutWithDiscount = $dp_total_value_brut_round;

                if ($documentProduct->is_benefit) {
                    if ($documentProduct->total_value_brut != 0) {
                        $document->wrong = true;
                    } else {
                        $document->wrong = false;
                    }
                } else {
                    if ($documentProduct->total_value_brut != $totalBrutWithDiscount) {
                        $document->wrong = true;
                        $motive_wrong[] = "dp_total_value_brut - " . $documentProduct->product->code;
                    }

                    if ($documentProduct->total_value != $totalValue) {
                        $document->wrong = true;
                        $motive_wrong[] = "dp_total_value - " . $documentProduct->product->code;
                    }
                }

                // Validamos el docuemnto
                $totalBrutDoc += $totalBrutWithDiscount;
                $totalDoc += $totalValue;

            }

            if (!$document->wrong) {
                if (strval($totalBrutDoc) !== strval($document->total_value_brut)) {
                    $document->wrong = true;
                    $motive_wrong[] = "total_value_brut";
                    // $this->adjustValuestByDocumentId($document->id);
                }

                if ($totalDoc != $document->total_value) {
                    $document->wrong = true;
                    $motive_wrong[] = "total_value";
                    // $this->adjustValuestByDocumentId($document->id);
                }
            }


            $document->motive_wrong = $motive_wrong;

        }


        $dataReturn = $documents->where('wrong', true)->map(function ($item, $key) {
            if ($item->wrong) {
                return [
                    'id'           => $item->id,
                    'motive_wrong' => $item->motive_wrong,
                    'date'         => $item->document_date,
                ];
            }
        });

        return [
            'CANTIDAD' => $documents->where('wrong', true)->count(),
            'DATA'     => $dataReturn->values()
        ];
    }

    public function recalculateRetentionPartial($operationType_id)
    {
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45])
            ->where("in_progress", false)
            ->where("closed", false)
            ->get();

        $docPartial = [];
        foreach ($documents as $key => $document) {
            // Cantidades de productos
            $quantityDocBase = $document->document->documentsProducts->sum('quantity');
            $quantityDoc = $document->documentsProducts->sum('quantity');

            // Cantidad de productos
            $quantityProductsDocBase = $document->document->documentsProducts->count();
            $quantityProductsDoc = $document->documentsProducts->count();

            $partial = false;

            if ($quantityProductsDocBase !== $quantityProductsDoc) {
                $partial = true;
            }

            if ($quantityDocBase !== $quantityDoc) {
                $partial = true;
            }

            // Validamos el documento parcial
            if ($partial) {
                $docPartial[] = $document;

                $warehouses = $document->contact->warehouses[0];
                $totalBrut = $document->total_value_brut;

                $document->ret_fue_prod_percentage = $operationType_id == 1 ? $document->document->ret_fue_prod_percentage : $warehouses->ret_fue_prod_percentage;
                $document->ret_ica_prod_percentage = $operationType_id == 1 ? $document->document->ret_ica_prod_percentage : $warehouses->ret_ica_prod_percentage;

                // FUE
                if ($operationType_id == 1 || $document->ret_fue_base_value >= $document->ret_fue_prod_vr_limit) {
                    $document->ret_fue_prod_total = round(($document->ret_fue_base_value * $warehouses->ret_fue_prod_percentage) / 100);
                    $document->ret_fue_total = round(($document->ret_fue_base_value * $warehouses->ret_fue_prod_percentage) / 100);
                }

                // ICA
                if ($operationType_id == 1 || $document->ret_ica_base_value >= $document->ret_ica_prod_vr_limit) {
                    $document->ret_ica_prod_total = round(($document->ret_ica_base_value * $warehouses->ret_ica_prod_percentage) / 100);
                    $document->ret_ica_total = round(($document->ret_ica_base_value * $warehouses->ret_ica_prod_percentage) / 100);
                }

                $document->erp_id = null;

                $document->save();
            }
        }

        return $docPartial;
    }

    public function reAsignSellerToCustomer($fileData, $request)
    {
        $seller = User::where("email", "ilike", "%" . trim($request->email) . "%")->first();
        $countData = 0;

        foreach ($fileData as $key => $item) {

            $contact = Contact::where('identification', trim($item['NIT']))->first();

            foreach ($contact->warehouses as $keyW => $warehouse) {
                if ($warehouse->seller_id != $seller->contact_id) {
                    $warehouse->seller_id = $seller->contact_id;
                    $warehouse->save();
                    $countData++;
                }
            }

        }

        return $countData;

    }

    public function pesoProducts($fileData)
    {
        foreach ($fileData as $key => $item) {
            $product = Product::where('code', trim($item['REFERENCIA']))->first();

            if ($product) {
                $product->weight = $item['PESO'];
                $product->save();
            }

        }

        return 'oki';
    }

    /**
     * Fechas de docuemntos
     */

    public function adjustDocumentDate($fileData)
    {
        $cont = 0;
        $contNot = [];
        foreach ($fileData as $key => $item) {
            $branchoffice = BranchOffice::where('code', '0' . $item['CODIGOSEDE'])->first();

            $vouchersType = VouchersType::where('code_voucher_type', $item['TIPODECOMPROBANTE'])
                ->where('branchoffice_id', $branchoffice->id)
                ->first();


            $document = Document::where('consecutive', trim($item['NUMERO']))
                ->where('branchoffice_id', $branchoffice->id)
                ->where('vouchertype_id', $vouchersType->id)
                ->where('in_progress', false)
                ->first();

            if ($document) {
                // Validamos la fecha
                $document_date = Carbon::createFromFormat('d/m/Y H:i:s', $item['FECHA'] . ' 08:00:00')->format('Y-m-d H:i:s');

                $dateRepeat = true;

                while ($dateRepeat) {
                    if (Document::where('document_date', $document_date)->where('id', '!=', $document->idate)->exists()) {
                        $document_date = Carbon::parse($document_date)
                            ->addSeconds(1)
                            ->format('Y-m-d H:i:s');
                    } else {
                        $dateRepeat = false;
                    }
                }

                $document->document_date = $document_date;
                $document->save();

                $cont++;

            } else {
                // $contNot[] = $item['NUMERO']." - ".$item['NOMBRESEDE'];
            }
        }

        dd([
            'documentos afectados' => $cont,
        ]);
        //consecutive  = 236 and branchoffice_id = 307 and in_progress  = false
    }

    /*
        Ajustar retenciones
    */
    public function adjustRetention($operationType_id)
    {
        // $contacts_id = [
        //     2821,
        //     2897,
        //     3219
        // ];
        // 1 = devoluciones
        // 5 = facturas
        $documents = Document::where("operationType_id", $operationType_id)
            ->whereNotIn("model_id", [4, 44, 45])
            ->where("in_progress", false)
            ->where("closed", false)
            ->where("document_date", '>=', '2020-08-01 00:00:00')
            // ->whereIn('contact_id', $contacts_id)
            ->get();

        $documentAfect = [];

        // Valor base ret
        // foreach ($documents as $key => $document) {
        //     if (empty($document->ret_fue_base_value)) {$document->ret_fue_base_value = $document->total_value_brut;}
        //     if (empty($document->ret_ica_base_value)) {$document->ret_fue_base_value = $document->total_value_brut;}
        //     if (empty($document->ret_fue_serv_base_value)) {$document->ret_fue_base_value = $document->total_value_brut;}
        //     if (empty($document->ret_ica_serv_base_value)) {$document->ret_fue_base_value = $document->total_value_brut;}

        //     $totalIva = 0;
        //     foreach ($document->documentsProducts as $key => $dp) {
        //         if ($tax = $dp->documentsProductsTaxes->where('tax_id', 1)->first()) {
        //             $totalIva += round($tax->value);
        //         }
        //     }

        //     if(empty($document->ret_iva_base_value)){$document->ret_iva_base_value  = $totalIva;}
        //     if(empty($document->ret_iva_serv_base_value)){$document->ret_iva_base_value  = $totalIva;}

        //     $document->save();
        // }

        foreach ($documents as $key => $document) {
            $warehouse = $document->warehouse;
            $totalBrut = $document->total_value_brut;

            // Porcentajes
            $document->ret_fue_prod_percentage = $operationType_id == 1 ? $document->document->ret_fue_prod_percentage : $warehouse->ret_fue_prod_percentage;
            $document->ret_ica_prod_percentage = $operationType_id == 1 ? $document->document->ret_ica_prod_percentage : $warehouse->ret_ica_prod_percentage;
            $document->ret_iva_prod_percentage = $operationType_id == 1 ? $document->document->ret_iva_prod_percentage : $warehouse->ret_iva_prod_percentage;


            $document->ret_fue_prod_vr_limit = $operationType_id == 1 ? $document->document->ret_fue_prod_vr_limit : $warehouse->ret_fue_prod_vr_limit;
            $document->ret_ica_prod_vr_limit = $operationType_id == 1 ? $document->document->ret_ica_prod_vr_limit : $warehouse->ret_ica_prod_vr_limit;
            $document->ret_iva_prod_vr_limit = $operationType_id == 1 ? $document->document->ret_iva_prod_vr_limit : $warehouse->ret_iva_prod_vr_limit;

            // FUE
            if ($operationType_id != 1 && $document->ret_fue_base_value >= $document->ret_fue_prod_vr_limit) {
                $document->ret_fue_prod_total = round(($document->ret_fue_base_value * $warehouse->ret_fue_prod_percentage) / 100);
                $document->ret_fue_total = round(($document->ret_fue_base_value * $warehouse->ret_fue_prod_percentage) / 100);

            } else if ($operationType_id == 1 && $document->document->ret_fue_base_value >= $document->document->ret_fue_prod_vr_limit) {
                $document->ret_fue_prod_total = round(($document->ret_fue_base_value * $document->ret_fue_prod_percentage) / 100);
                $document->ret_fue_total = round(($document->ret_fue_base_value * $document->ret_fue_prod_percentage) / 100);
            } else {
                $document->ret_fue_prod_total = 0;
                $document->ret_fue_total = 0;
            }

            // ICA
            if ($operationType_id != 1 && $document->ret_ica_base_value >= $document->ret_ica_prod_vr_limit) {
                $document->ret_ica_prod_total = round(($document->ret_ica_base_value * $warehouse->ret_ica_prod_percentage) / 100);
                $document->ret_ica_total = round(($document->ret_ica_base_value * $warehouse->ret_ica_prod_percentage) / 100);

            } else if ($operationType_id == 1 && $document->document->ret_ica_base_value >= $document->document->ret_ica_prod_vr_limit) {
                $document->ret_ica_prod_total = round(($document->ret_ica_base_value * $document->ret_ica_prod_percentage) / 100);
                $document->ret_ica_total = round(($document->ret_ica_base_value * $document->ret_ica_prod_percentage) / 100);
            } else {
                $document->ret_ica_prod_total = 0;
                $document->ret_ica_total = 0;
            }

            // IVA
            if ($operationType_id != 1 && $document->ret_iva_base_value >= $document->ret_iva_prod_vr_limit) {
                $document->ret_iva_prod_total = round(($document->ret_iva_base_value * $warehouse->ret_iva_prod_percentage) / 100);
                $document->ret_iva_total = round(($document->ret_iva_base_value * $warehouse->ret_iva_prod_percentage) / 100);

            } else if ($operationType_id == 1 && $document->document->ret_iva_base_value >= $document->document->ret_iva_prod_vr_limit) {
                $document->ret_iva_prod_total = round(($document->ret_iva_base_value * $document->ret_iva_prod_percentage) / 100);
                $document->ret_iva_total = round(($document->ret_iva_base_value * $document->ret_iva_prod_percentage) / 100);
            } else {
                $document->ret_iva_prod_total = 0;
                $document->ret_iva_total = 0;
            }


            // $document->ret_iva_prod_percentage = $warehouse->ret_iva_prod_percentage;
            // $document->ret_iva_serv_percentage = $warehouse->ret_iva_prod_percentage;
            $document->erp_id = null;

            $document->save();
        }

        // foreach ($documents as $key => $document) {

        //     $validation = true;

        //     // Completo
        //     if ($document->documentsProducts->sum("quantity") === $document->document->documentsProducts->sum("quantity")) {
        //         // ICA
        //         if ($document->ret_ica_base_value >= $document->ret_ica_prod_vr_limit) {
        //             if ($document->ret_ica_prod_total != $document->document->ret_ica_prod_total) {
        //                 $validation = false;
        //             }
        //         }
        //         // FUE
        //         if ($document->ret_fue_base_value >= $document->ret_fue_prod_vr_limit) {
        //             if ($document->ret_fue_prod_total != $document->document->ret_fue_prod_total) {
        //                 $validation = false;
        //             }
        //         }
        //         // IVA
        //         if ($document->ret_iva_base_value >= $document->ret_iva_prod_vr_limit) {
        //             if ($document->ret_iva_prod_total != $document->document->ret_iva_prod_total) {
        //                 $validation = false;
        //             }
        //         }
        //     }
        //     // Parcial
        //     else {

        //         if (
        //             $document->document->ret_fue_prod_total > 0 ||
        //             $document->document->ret_ica_prod_total > 0 ||
        //             $document->document->ret_iva_prod_total > 0
        //             ) {

        //                 $totalParcialFue = round(($document->ret_fue_base_value * $document->ret_fue_prod_percentage) / 100);
        //                 $totalParcialIca = round(($document->ret_ica_base_value * $document->ret_ica_prod_percentage) / 100);
        //                 $totalParcialIva = round(($document->ret_iva_base_value * $document->ret_iva_prod_percentage) / 100);

        //                 if (
        //                     $document->ret_fue_prod_total != $totalParcialFue ||
        //                     $document->ret_ica_prod_total != $totalParcialIca ||
        //                     $document->ret_iva_prod_total != $totalParcialIva
        //                 ) {
        //                     $validation = false;
        //                 }
        //         }

        //     }

        //     if (!$validation) {
        //         $documentAfect[] = $document;
        //     }


        // }

        return [
            'cantidad' => count($documentAfect),
            'data'     => $documentAfect
        ];
    }

    /*
        RECALCULAR DOCUEMNTOS
    */
    public function recalculateDocument($operationType_id)
    {

        $documents = Document::where("operationType_id", $operationType_id)
            ->where("in_progress", false)
            ->where("closed", false)
            ->get();

        foreach ($documents as $key => $document) {
            // Actualizar toales en documento
            $total_value = 0;
            $total_value_brut = 0;

            $documentsProducts = $document->documentsProducts->where("quantity", '>', '0');

            foreach ($documentsProducts as $dp) {
                if ($dp->quantity > 0) {
                    $total_value_brut += $dp->total_value_brut;
                    $total_value += $dp->total_value;
                }
            }

            // Actualizamos totales
            Document::find($document->id)->update([
                'total_value'      => round($total_value),
                'total_value_brut' => round($total_value_brut)
            ]);
        }


        return $documents;

    }

    /*
        RECALCULAR DOCUEMNTOS
    */
    public function recalculateDueDateDocument($operationType_id)
    {

        $documents = Document::where("operationType_id", $operationType_id)
            ->where("in_progress", false)
            //->whereNull("software_siigo")
            ->get();

        foreach ($documents as $key => $document) {
            if ($document->documentsProducts->count() == 0) {
                continue;
            }
            //Fecha de vencimiento
            $document_date = Carbon::parse($document->document_date); // ->format('Y-m-d H:i:s'); //Carbon::parse(date('Y-m-d H:i:s'));
            $expiration_days = 0;

            if (!empty($document->contact->pay_days)) {
                $expiration_days = $document->contact->pay_days;
            }

            $document->due_date = $document_date->addDays($expiration_days);
            $document->save();
        }

        return $documents->count();

    }

    /**
     * Calcular el IVA
     *
     * @author Kevin Galindo
     */
    public function recalculateIVA()
    {
        $documents = Document::where('in_progress', false)
            ->whereHas('documentsProducts', function ($query) {
                $query->whereHas('documentProductTaxes', function ($query) {
                    $query->where('tax_id', 1);
                    $query->where('value', 0);
                });
            })
            ->get();

        // IVA
        foreach ($documents as $key => $document) {
            foreach ($document->documentsProducts as $keyDp => $documentProduct) {
                // Seleccionamos los impuestos del producto
                $taxes = $documentProduct->product->line->taxes->where('id', 1);

                // Calcular TAXES
                if ($taxes->count() > 0) {

                    // Recorremos los impuestos
                    foreach ($taxes as $keyt => $tax) {

                        $valueTax = 0;
                        $taxPercentage = 0;

                        // Ventas
                        if (
                            $document->operationType->code == "206" ||
                            $document->operationType->code == "205" ||
                            $document->operationType->code == "155" ||
                            $document->operationType->code == "010" ||
                            $document->operationType->code == "156"
                        ) {
                            $taxPercentage = $tax->percentage_sale;
                        }

                        // Compras
                        if (
                            $document->operationType->code == "105" ||
                            $document->operationType->code == "020" ||
                            $document->operationType->code == "270" ||
                            $document->operationType->code == "015"
                        ) {
                            $taxPercentage = $tax->percentage_purchase;
                        }

                        // Calcula el valormpor el valor Bruto
                        if ($tax->base == 1) {
                            $valueTax = $documentProduct->total_value_brut * $taxPercentage / 100;
                        }

                        $taxesDocumentProduct = [
                            'document_product_id' => $documentProduct->id,
                            'tax_id'              => $tax->id,
                            'base_value'          => $documentProduct->total_value_brut,
                            'tax_percentage'      => $taxPercentage,
                            'value'               => round($valueTax)
                        ];

                        // Creamos el registro del impuesto
                        DocumentProductTax::updateOrCreate(
                            Arr::only($taxesDocumentProduct, ['document_product_id', 'tax_id']),
                            $taxesDocumentProduct
                        );
                    }
                }
            }
        }

        // CANTIDADES
        foreach ($documents as $key => $document) {
            foreach ($document->documentsProducts as $keyDp => $documentProduct) {
                $totalTaxes = $documentProduct->documentProductTaxes->sum('value');

                DocumentProduct::find($documentProduct->id)->update([
                    'total_value' => $documentProduct->total_value_brut + $totalTaxes
                ]);
            }
        }

        // CANTIDADES DE DOC
        foreach ($documents as $key => $document) {
            $total_value = $document->documentsProducts->sum('total_value');
            Document::find($document->id)->update([
                'total_value' => $total_value
            ]);
        }


        return $documents;

    }

    /**
     * Descargar todos los pedidos con su estado
     * de facturacion y estado de cartera.
     *
     * @author Kevin Galindo
     */
    public function downloadDocumentsByState($request)
    {
        $model_id = $request->model_id;
        $documents = [];
        $branchoffice_id = $request->branchoffice_id;

        $branchoffices = empty($branchoffice_id)
            ? BranchOffice::where('code', '!=', '01')->orderBy('name')->get()
            : BranchOffice::where('id', $branchoffice_id)->get();

        foreach ($branchoffices as $key => $branchoffice) {

            $documentBranchoffice = [];

            $request->request->add([
                'model_id'        => $model_id,
                'branchoffice_id' => $branchoffice->id,
                'paginate'        => 'false',
                'closed'          => false,
                'state_wallet'    => [
                    '03',
                    '05',
                    '09',
                    '07'
                ],
                'state_backorder' => [
                    '1',
                    '2',
                    '3'
                ]
            ]);

            $documentsAll = $this->listBaseDocuments($request);

            // NO RETENIDOS
            // $documentsNotRejected = $this->getOrderIfRejected(false, $request);

            // RETENIDOS
            // $documentsRetained    = $this->getOrderIfRejected(true, $request);

            // RECHAZADOS
            // $documentsRejected    = $this->getOrderByStatusAuthorized('RECHAZADOS', $request);

            // FACTURADOS
            // $documentsComplete   = $this->getDocumentInvoiced(true, $request->toArray());

            // CERRADOS
            $request->request->add([
                'state_backorder' => [
                    '4'
                ],
                'state_wallet'    => null
            ]);

            $documentsClose = $this->listBaseDocuments($request);

            $documentsMerge = array_merge($documentsAll->toArray(), $documentsClose->toArray());

            // Recorremos los pedidos RETENIDOS
            foreach ($documentsMerge as $key => $document) {
                $documentBranchoffice[] = [
                    'SEDE'                => $branchoffice->name,
                    'FECHA'               => $document['document_date'],
                    'TIPO DOCUMENTO'      => $document['vouchertype']['name_voucher_type'],
                    'NUMERO'              => $document['consecutive'],
                    'CLIENTE'             => $document['warehouse']['description'],
                    'VENDEDOR'            => $document['seller']['name'] . " " . (!empty($document['seller']['surname']) ? $document['seller']['surname'] : ''),
                    'VALOR TOTAL'         => $document['total_value'],
                    'ESTADO FACTURACION'  => $document['closed'] ? 'CERRADO' : $document['state_backorder']['name_parameter'],
                    'ESTADO AUTORIZACION' => $document['state_authorization_wallet_param']['name_parameter'],
                ];
            }

            $documents = array_merge($documents, collect($documentBranchoffice)->sortBy('FECHA')->values()->toArray());
        }

        return [
            'name_excel' => 'Estado_de_pedidos_' . date('Y-m-d H:i:s'),
            'data'       => $documents
        ];

    }

    public function updateDocCruce($request)
    {
        $document = Document::find($request->document_id);
        $request->request->add(['erp_id' => null]);
        $document->update(Arr::except($request->all(), [
            'document_id'
        ]));

        return $document;
    }

    public function documentUpdateState($document_id)
    {

        try {

            //DB::beginTransaction();

            // Traemos el documento
            $document = Document::find($document_id);

            // Seteamos las variables
            //$document->model_id = $model_id;

            $originDoc = $document;
            $adjustDoc = $document;
            $adjustDocState = true;

            do {
                // Traemos el back order del documento
                $backOrderDocument = $this->documentTotalBackOrder($adjustDoc, $originDoc->model_id);

                // Traemos el estado segun el backorder
                $statedoc = $this->getStateDocumentByBackOrder($backOrderDocument);

                // Actualizamos el estado
                Document::find($adjustDoc->id)->update([
                    'state_backorder' => $statedoc->id
                ]);

                // Validamos el Documento base
                if (!empty($adjustDoc->document) && $adjustDoc->document->id != $adjustDoc->id) {
                    $originDoc = $adjustDoc;
                    $adjustDoc = $adjustDoc->document;
                } else {
                    $adjustDocState = false;
                }

            } while ($adjustDocState);

            //DB::commit();
            return true;

        } catch (\Exception $e) {
            //DB::rollBack();

            return false;
        }
    }

    public function documentTotalBackOrder($document, $model_id)
    {
        $documentsBackOrder = [];

        // Traemos el BackOrder
        $calculateBackOrderRequest = [
            'document_id' => $document->id,
            'model_id'    => $model_id
        ];

        // foreach ($document->documentsProducts as $key => $documentProduct) {
        //     $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $calculateBackOrderRequest, false);
        //     $documentsBackOrder[] = $bo;
        // }

        $data = [
            'back_order_document_original' => $document->documentsProducts->sum('quantity'),
            'back_order_document_acttual'  => $document->backorder_quantity //collect($documentsBackOrder)->sum('backorder')
        ];

        return $data;
    }

    public function getStateDocumentByBackOrder($backOrderDocument)
    {
        $statedoc = null;

        if ($backOrderDocument['back_order_document_original'] == $backOrderDocument['back_order_document_acttual']) {
            // PENDIENTE
            $statedoc = Parameter::where('paramtable_id', 64)->where('code_parameter', '1')->first();
        } else
            if ($backOrderDocument['back_order_document_acttual'] > 0) {
                // 	PARCIAL
                $statedoc = Parameter::where('paramtable_id', 64)->where('code_parameter', '2')->first();

            } else
                if ($backOrderDocument['back_order_document_acttual'] <= 0) {
                    // FINALIZADO
                    $statedoc = Parameter::where('paramtable_id', 64)->where('code_parameter', '3')->first();
                } else {
                    $statedoc = null;
                }

        return $statedoc;
    }

    public function BuscardPTrash()
    {
        $documents = Document:: select('id', 'total_value', 'vouchertype_id', 'consecutive')
            ->with(['vouchertype'       => function ($query) {
                $query->whereIN('code_voucher_type', ['205', '207', '209']);
            },
                    'documentsProducts' => function ($query2) {
                        $query2->select('id', 'document_id', 'total_value');
                    }
            ])
            ->whereHas('vouchertype', function ($query) {
                $query->whereIN('code_voucher_type', ['205', '207', '209']);
            })
            ->where('in_progress', false)
            ->get();

        $documentsConEliminaciones = [];
        $listaFacturasaRevisar = [];

        foreach ($documents as $key => $document) {

            $total_doc_prod = 0;
            if (count($document->documentsProducts) == 0) {
                $documentsConEliminaciones [] = $document;

                $documents_products = DocumentProduct:: where('document_id', $document->id)
                    ->with(['product'])
                    ->withTrashed()->get();
                $products = [];
                foreach ($documents_products as $key => $document_product) {
                    if ($document_product->deleted_at != null) {
                        $products [] = $document_product->product->code . '----' . $document_product->product->description . '||||Eliminado';
                    } else {
                        $products [] = $document_product->product->code . '----' . $document_product->product->description;
                    }
                }

                $listaFacturasaRevisae[] = [
                    $document->vouchertype->name_voucher_type . '-' . $document->consecutive,
                    $products
                ];
            }
            foreach ($document->documentsProducts as $key => $documentProduct) {
                $total_doc_prod += $documentProduct->total_value;
            }

            if ($total_doc_prod < $document->total_value) {
                $documentsConEliminaciones [] = $document;

                $documents_products = DocumentProduct:: where('document_id', $document->id)
                    ->with(['product'])
                    ->withTrashed()->get();
                foreach ($documents_products as $key => $document_product) {
                    if ($document_product->deleted_at != null) {
                        $products [] = $document_product->product->code . '----' . $document_product->product->description . '||||Eliminado';
                    } else {
                        $products [] = $document_product->product->code . '----' . $document_product->product->description;
                    }
                }

                $listaFacturasaRevisae [] = [
                    $document->vouchertype->name_voucher_type . '-' . $document->consecutive,
                    $products
                ];
            }
        }

        return $listaFacturasaRevisae;
    }


    /**
     * Kevin Galindo
     *
     * @author Kevin Galindo
     */
    public function getDocumentByauthorize($request)
    {
        $user_id = $request->user_id;

        $documents = Document::where('pending_authorization', true)
            ->where('closed', false)
            ->whereHas('template', function ($query) use ($user_id) {
                $query->whereHas('authorizedUsers', function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                });
            })
            ->with([
                'contact',
                'seller',
                'vouchertype',
                'branchoffice:id,name',
                'userSolicitAuthorization',
                'branchofficeWarehouse'
            ])
            ->get();


        return $documents;
    }


    public function getHour0()
    {
        // Listar los docmentos con hora 0

        $documents = DB:: select("select id, consecutive , vouchertype_id , document_date from documents where date_part('hour', document_date ) = 0 order by document_date");

        $response = [];
        // recorro los documentos
        foreach ($documents as $key => $value) {
            //busco el documento
            $document = Document::find($value->id);

            if ($document) {
                $hour = $document->consecutive . '000000';

                $hour = substr($hour, 0, 6);

                $hours = substr($hour, 0, 2) < '23' ? substr($hour, 0, 2) : '23';
                $minutes = substr($hour, 2, 2) < '59' ? substr($hour, 2, 2) : '59';

                $spli = str_split($document->consecutive);

                $second = 0;
                foreach ($spli as $key => $number) {
                    $second += $number;
                }
                if ($second < 10) {
                    $second = '0' . $second;
                }

                $len = strlen($document->consecutive);
                $seconds = '0' . substr($document->consecutive, $len - 1, 1);


                // $seconds = substr($hour, 5, 1) < '59' ? substr($hour, 4, 2) : '59' ;

                $document->document_date = substr($document->document_date, 0, 10) . ' ' . $hours . ':' . $minutes . ':' . $seconds;

                $document_validate = Document:: where('document_date', $document->document_date)->first();

                // if ($document_validate) {
                //     if ($document_validate->consecutive > $document->consecutive) {
                //         $second=$second-1;
                //     }else{
                //         $second=$second+1;
                //     }
                //     if ($second < 10) {
                //         $second = '0'.$second;
                //     }
                //     $document->document_date= substr($document->document_date, 0, 10).' '.$hours.':'.$minutes.':'.$second;
                // }
                $response [] = $document;
                $document->save();
            }
        }
        return $response;
    }

    /**
     * @param $request
     * @param $user
     */
    public function getOrdersManagement($request, $user)
    {
        $documents = $this->documentRepository->getOrdersManagement($request, $user);

        $documents = $this->getDocumentWalletProblem($documents, '', false);

        /*$documents->map(function ($d) use ($request) {
            $countDocumentsProducts = $d->documentsProducts->count();
            $d->documentsProducts->map(function ($dp) use ($d) {
                $dp->quantityInvoiced = 0;
                $d->documentThread->map(function ($dt) use ($dp) {
                    if ($dp->quantityInvoiced < $dp->quantity) {
                        $dt->documentsProducts->map(function ($dpt) use ($dp) {
                            if ($dpt->product_id == $dp->product_id) {
                                $dp->quantityInvoiced += $dpt->quantity;
                            }
                        });
                    }
                });
            });

            $countDocumentsProductsInvoiced = $d->documentsProducts->filter(function ($dp) use ($d) {
                return $dp->quantityInvoiced >= $dp->quantity;
            })->count();

            if ($countDocumentsProductsInvoiced == $countDocumentsProducts) {
                $d->state = 'FACTURADO';
            } else if ($d->documentThread->count() > 0) {
                $d->state = 'FACTURADO PARCIAL';
            } else {
                $d->state = 'PENDIENTE';
            }

        });

        if (isset($request->filter_status) && $request->filter_status != '') {
            $documents = $documents->filter(function ($d) use ($request) {
                return $d->state == $request->filter_status;
            })->values();
        }*/

        if ($request['sort'] == 'ASC') {
            $documents = $documents->sortBy($request['sortBy'])->values()->all();
        } else {
            $documents = $documents->sortByDesc($request['sortBy'])->values()->all();
        }

        $paginated = $this->paginateCollections($documents, 100, $request->page);

        $paginated['total_value_orders'] = collect($documents)->sum('total_value_brut');

        return $paginated;
    }

    public function createCsvDespatch($data)
    {

        //Obtenemos los "Labels" para generar el archivo
        $fieldsCsvRow1 = [];

        //Declaramos las cabeceras de la respuesta para descargar el archivo.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $fieldsCsvRow3[] = 'Fecha';
        $fieldsCsvRow3[] = 'Numero Documento';
        $fieldsCsvRow3[] = 'Identificacion';
        $fieldsCsvRow3[] = 'cliente';
        $fieldsCsvRow3[] = 'direccion';
        $fieldsCsvRow3[] = 'Vr Total';
        $fieldsCsvRow3[] = 'Ciudad';
        // $fieldsCsvRow3[] = 'Zona';
        $fieldsCsvRow3[] = 'Transportador';
        $fieldsCsvRow3[] = 'Numero de Guia';
        $fieldsCsvRow3[] = 'Obs Despacho';


        $fieldsCsvRow4 = [];

        foreach ($data as $key => $document) {
            $fieldsCsvRow4[] = [
                $document->document_date,
                $document->prefix . '-' . $document->consecutive,
                $document->contact->identification,
                $document->contact->name . ' ' . $document->contact->surname,
                $document->contact->address,
                number_format($document->total_value_brut, 0, '', ''),
                !is_null($document->contact->city) ? $document->contact->city->city : null,
                // $document->warehouse->zone != null ? $document->warehouse->zone->name_parameter : null,
                $document->documentsInformation != null ? ($document->documentsInformation->contact != null ? $document->documentsInformation->contact->name . ' ' . $document->documentsInformation->contact->surname : null) : null,
                $document->documentsInformation != null ? ($document->documentsInformation->guide_number) : null,
                $document->documentsInformation != null ? ($document->observation) : null,
            ];
        }

        $column = [$fieldsCsvRow3];
        $callback = function () use ($column, $fieldsCsvRow4) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ($column as $row) {

                fputcsv($file, $row, ';');
            }

            foreach ($fieldsCsvRow4 as $key => $field) {
                fputcsv($file, $field, ';');
            }

            //Cerramos la escritura del archivo.
            fclose($file);
        };


        //Retornamos el archivo.
        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200
        ];

    }

    public function createCsvLabels($data, $request)
    {
        //Obtenemos los "Labels" para generar el archivo
        $fieldsCsvRow1 = [];

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $fieldsCsvRow3[] = 'Cod-cliente';
        $fieldsCsvRow3[] = 'Nombre-Cliente';
        $fieldsCsvRow3[] = 'Direccion';
        $fieldsCsvRow3[] = 'Ciudad';
        $fieldsCsvRow3[] = 'Telefono';
        $fieldsCsvRow3[] = 'Tipo-Doc';
        $fieldsCsvRow3[] = 'prefijo-numero';
        $fieldsCsvRow3[] = 'Numero de cajas';
        $fieldsCsvRow3[] = 'Link';


        $fieldsCsvRow4 = [];

        foreach ($data["data"] as $key => $document) {

            if ($document->operationType_id != 3 && $document->operationType_id != 4) {
                // $document_print = Document::find($document->id);
                // $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document_print, true);
                $file_name = "/storage/pdf/" . str_replace("/", "-", $document->vouchertype->name_voucher_type . ' No.' . $document->consecutive . '.pdf');
            } else {
                // $transfer_s_id = $document->operationType_id == 4 ? $document->id : $document->thread;
                // $transfer_s = $this->documentRepository->searchTransfer($transfer_s_id);
                // $file_name ="/storage/pdf/" . $this->generatePdftransfer($transfer_s, true);
                $file_name = "/storage/pdf/" . 'Transfer' . $document->consecutive . '.pdf';
            }

            for ($i = 1; $i <= $document->number_stickers; $i++) {
                $fieldsCsvRow4[] = [
                    $document->operationType_id != 3 && $document->operationType_id != 4 ? $document->contact->identification : $document->warehouse->code,
                    $document->warehouse->description,
                    $document->warehouse->address,
                    is_null($document->warehouse->city) ? null : $document->warehouse->city->city,
                    is_null($document->warehouse->telephone2) && trim($document->warehouse->telephone2) == "" ? $document->warehouse->telephone . '.' : $document->warehouse->telephone . ' - ' . $document->warehouse->telephone2,
                    $document->operationType_id != 3 && $document->operationType_id != 4 ? 'Factura' : 'Traslado',
                    is_null($document->prefix) ? $document->consecutive : trim($document->prefix) . ' - ' . $document->consecutive,
                    'Caja ' . $i . '/' . $document->number_stickers,
                    $request->link . $file_name,
                ];
            }

        }

        $column = [$fieldsCsvRow3];
        $callback = function () use ($column, $fieldsCsvRow4) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ($column as $row) {

                echo $this->rfccsv($row) . "\n";
            }

            foreach ($fieldsCsvRow4 as $key => $field) {
                echo $this->rfccsv($field) . "\n";
            }

            //Cerramos la escritura del archivo.
            fclose($file);
        };


        //Retornamos el archivo.
        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200
        ];

    }

    public function rfccsv($arr)
    {
        foreach ($arr as &$a) {
            $a = strval($a);
            if (strcspn($a, ",\"\r\n") < strlen($a)) $a = '"' . strtr($a, array('"' => '""')) . '"';
        }
        return implode(';', $arr);
    }

    public function createCsvPickin($data)
    {

        //Obtenemos los "Labels" para generar el archivo
        $fieldsCsvRow1 = [];

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $fieldsCsvRow3[] = 'Ubicacion';
        $fieldsCsvRow3[] = 'Codigo';
        $fieldsCsvRow3[] = 'Descripcion';
        $fieldsCsvRow3[] = 'Cantidad';
        $fieldsCsvRow3[] = 'Linea';

        $fieldsCsvRow4 = [];

        foreach ($data as $key => $documents) {
            foreach ($documents as $key => $document) {
                $fieldsCsvRow4[] = [
                    $document["location"],
                    $document["code"],
                    $document["description"],
                    $document["quantity"],
                    $document["line"],
                ];
            }
        }

        $column = [$fieldsCsvRow3];
        $callback = function () use ($column, $fieldsCsvRow4) {

            //Creamos un archivo temporal.
            $file = fopen('php://output', 'w');

            //Llenamos de informacion el archivo temporal.
            foreach ($column as $row) {

                fputcsv($file, $row, ';');
            }

            foreach ($fieldsCsvRow4 as $key => $field) {
                fputcsv($file, $field, ';');
            }

            //Cerramos la escritura del archivo.
            fclose($file);
        };


        //Retornamos el archivo.
        return [
            'headers' => $headers,
            'file'    => $callback,
            'status'  => 200
        ];

    }


    public function getTransfer($id)
    {
        $transfer_s = $this->documentRepository->searchTransfer($id);

        if ($transfer_s) {
            return (object)[
                'code'      => '200',
                'completed' => false,
                'data'      => (object)[
                    'data'    => $transfer_s,
                    'message' => ''
                ]
            ];
        } else {
            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'no existe el traslado'
                ]
            ];
        }
    }

    public function documentsMissingBackOrder($request)
    {
        set_time_limit(0);

        $productsMissingBackOrder = DB::select('select
        document_date as "FECHA_PEDIDO",
        document_time as "HORA_PEDIDO",
        consecutive as "NUM_PEDIDO",
        client_ident as "IDENT_CLIENTE",
        client_name as "NOMBRE_CLIENTE",
        seller_name as "VENDEDOR",
        product_cod as "COD_PRODUCTO",
        product_desc as "DESCRIP_PROD",
        back_order as "CANTIDAD_PEND_PEDIDO",
        existence as "EXISTENCIA",
        total_cant_req as "TOTAL_CANT_REQ",
        total_cant_falt as "TOTAL_CANT_FALTANTE"
        from fn_reporte_back_order(?)',
            [$request->branchoffice_id]);

        $productsMissingBackOrder = collect($productsMissingBackOrder)->where('TOTAL_CANT_FALTANTE', '>', 0)->values();

        $header = [
            'Fecha-Pedido',
            'Hora-Pedido',
            'Num-Pedido',
            'Ident-Cliente',
            'Nombre-Cliente',
            'Vendedor',
            'Cod-Producto',
            'Descrip-Prod',
            'Cant-Pend-Pedido',
            'Existencia',
            'Total Cant Req',
            'Total Cant Faltante',
        ];

        $headerDetail = [
            'Cod-Producto',
            'Descrip-Prod',
            'Existencia',
            'Total Cant Req',
            'Total Cant Faltante',
        ];

        $detailProduct = [];

        foreach ($productsMissingBackOrder as $key => $product) {
            $key_product_array = array_search(
                $product->COD_PRODUCTO,
                array_column($detailProduct, 'CODE')
            );

            // Agregamos el producto al array.
            if ($key_product_array === false) {
                $detailProduct[] = [
                    'CODE'                => $product->COD_PRODUCTO,
                    'DESCRIP_PROD'        => $product->DESCRIP_PROD,
                    'EXISTENCIA'          => $product->EXISTENCIA,
                    'TOTAL_CANT_REQ'      => $product->TOTAL_CANT_REQ,
                    'TOTAL_CANT_FALTANTE' => $product->TOTAL_CANT_FALTANTE,
                ];
            }

        }

        return [
            'header'       => $header,
            'data'         => $productsMissingBackOrder,
            'dataDetail'   => $detailProduct,
            'headerDetail' => $headerDetail
        ];
    }

    /**
     *
     */
    public function getDocumentsToAuthorize()
    {
        $user = Auth::user();

        return $this->documentRepository->getDocumentsToAuthorize($user->id);

    }


    public function createTransferMedellin($request)
    {
        try {
            DB::beginTransaction();

            //autorizacion
            $model = Template::find(34);


            $in_progress = true;
            $branchOffice_code = BranchOffice::find(283)->code;
            $contact = Contact:: where('identification', '900338016')->first();
            $contact_warehouse = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code)->first()->id;

            $branchOffice_code_entrace = BranchOffice::find(305)->code;
            $contact_warehouse_entrace = ContactWarehouse:: where('contact_id', $contact->id)->where('code', $branchOffice_code_entrace)->first()->id;

            //se crea el documento traslado salida
            $consecutive = VouchersType::where('code_voucher_type', 100)->lockForUpdate()->first();
            $consecutive->consecutive_number = $consecutive->consecutive_number + 1;

            $exists = true;

            // Validamos si existe el consecutivo y de ser asi se suma 1 hasta que no coincida.
            while ($exists) {
                $aux = Document::where('consecutive', $consecutive->consecutive_number)
                    ->where('vouchertype_id', $consecutive->id)
                    ->where('in_progress', false)
                    ->exists();
                if ($aux) {
                    ++$consecutive->consecutive_number;
                } else {
                    $exists = false;
                }
            }


            $consecutive->save();

            $data = [
                'vouchertype_id'            => 13,
                'consecutive'               => $consecutive->consecutive_number,
                // 'user_id'=> $request->params["contact_id"]["contact_id"],
                'contact_id'                => $contact->id,
                'document_state'            => 1,
                'in_progress'               => $in_progress,
                'branchoffice_id'           => 283,
                'branchoffice_warehouse_id' => 306,
                'operationType_id'          => 4,
                "incorporated_document"     => false,
                "observation"               => '',
                "issue_date"                => Carbon:: now(),
                "warehouse_id"              => $contact_warehouse,
                'document_date'             => date('Y-m-d H:i:s'),
                'model_id'                  => 34,
                'from_service_request'      => false,

            ];
            $transfer = Document::create($data);

            $requestparamsbodega_entrada = 278;

            //traslado entrada
            $data_entrace = [
                'vouchertype_id'            => 13,
                'consecutive'               => $consecutive->consecutive_number,
                // 'user_id' =>  $request->params["contact_id"]["contact_id"],
                'contact_id'                => $contact->id,
                'document_state'            => 1,
                'in_progress'               => $in_progress,
                'branchoffice_id'           => 305,
                'branchoffice_warehouse_id' => $requestparamsbodega_entrada,
                'thread'                    => $transfer->id,
                'operationType_id'          => 3,
                "incorporated_document"     => false,
                "observation"               => '',
                "issue_date"                => Carbon:: now(),
                "warehouse_id"              => $contact_warehouse_entrace,
                'document_date'             => date('Y-m-d H:i:s'),
                'model_id'                  => 35,
                'from_service_request'      => false,
            ];

            $transfer_entrace = Document::create($data_entrace);
            $total_value = 0;

            //traer productos
            $products = Inventory::where('branchoffice_warehouse_id', 306)->where('stock', '<>', 0)->with('product')->get();

            foreach ($products as $key => $product) {
                $documents_products_data = [
                    'document_id'             => $transfer->id,
                    'product_id'              => $product->product_id,
                    'line_id'                 => $product->product->line_id,
                    'subline_id'              => $product->product->subline_id,
                    'brand_id'                => $product->product->brand_id,
                    'quantity'                => $product->stock,
                    'unit_value_before_taxes' => $product->average_cost,
                    'total_value_brut'        => $product->stock * $product->average_cost,
                    'manage_inventory'        => Product::find($product->product_id)->manage_inventory,
                    'operationType_id'        => 4
                ];

                $total_value += $documents_products_data['total_value_brut'];

                $documents_products_transfer = DocumentProduct::create($documents_products_data);

                $inva = $this->inventoryRepository->finalize($documents_products_transfer->id);

                $documents_products_data_entrace = [
                    'document_id'             => $transfer_entrace->id,
                    'product_id'              => $product->product_id,
                    'line_id'                 => $product->product->line_id,
                    'subline_id'              => $product->product->subline_id,
                    'brand_id'                => $product->product->brand_id,
                    'quantity'                => $product->stock,
                    'unit_value_before_taxes' => $documents_products_data["unit_value_before_taxes"],
                    'total_value_brut'        => $product->stock * $product->average_cost,
                    'manage_inventory'        => Product::find($product->product_id)->manage_inventory,
                    'operationType_id'        => 3
                ];

                $documents_products_transfer_entrace = DocumentProduct::create($documents_products_data_entrace);
                $inv = $this->inventoryRepository->finalize($documents_products_transfer_entrace->id);

                $transfer->in_progress = false;
                $transfer->save();

                $transfer_entrace->in_progress = false;
                $transfer_entrace->save();
            }

            //total traslado
            $transfer->total_value = $total_value;
            $transfer->save();

            $transfer_entrace->total_value = $total_value;
            $transfer_entrace->save();


            if ($transfer) {
                //crear pdf
                $transfer_s = $this->documentRepository->searchTransfer($transfer->id);

                $transfer->invoice_link = "/storage/pdf/" . $this->generatePdftransfer($transfer_s, true);
                $transfer->save();
                $transfer_entrace->invoice_link = $transfer->invoice_link;
                $transfer_entrace->save();

                $transfer->file_name = $transfer->invoice_link;
            }
            DB:: commit();
            return (object)[
                'code' => '200',
                'data' => (object)[
                    'data'    => $transfer,
                    'message' => 'Se Ha creado Exítosamente'
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() . "\n"
                ]
            ];
        }

    }

    public function setReturnedDocument($id)
    {
        //obtenemos el docmento
        $document = $this->documentRepository->getDocument($id);
        //validamos si es devolucion
        if ($document->template->code == 155 || $document->template->code == 270) {
            //obtenemos el documento base (la factura)
            $base_document = $document->document;
            $state = null;


            //validamos si es igual el total de la devolucion y el de la factura
            if ($document->total_value_brut == $base_document->total_value_brut) {
                //se actualiza el campo returned_document con estado 1
                $state = 1;
            } else {
                // validamos si la facura tiene mas devoluciones
                $returns_ = $this->documentRepository->getReturnsFromBill($document->thread);
                $returns_total_value_brut = 0;
                foreach ($returns_ as $key => $document) {
                    $returns_total_value_brut += $document->total_value_brut;
                }

                // validamos si el total de las devoluciones es igual al de la factura
                if ($returns_total_value_brut == $base_document->total_value_brut) {
                    //se actualiza el campo returned_document con estado 3
                    $state = 3;
                } else {
                    //se actualiza el campo returned_document con estado 2
                    $state = 2;
                }
            }
            $this->documentRepository->retReturnedDocument($document->thread, $state);
        }
    }

    public function clearErp($request)
    {
        // $parameters = [
        //     array(1, 2, 3)
        // ];
        // $data = DB::select('select * from public.test_array(?)',$parameters);
        // return(object) [
        //     'code' => '200',
        //     'data' => (object) [
        //         'data'=>$data,
        //     ]
        // ];

        $list_documents = $this->documentRepository->searchDocumentForClearErp($request);
        if (count($list_documents) > 0) {
            try {
                DB::beginTransaction();
                $documents = [];
                foreach ($list_documents as $key => $document) {
                    $documents[] = $this->documentRepository->setErp_idNull($document->id);
                }
                DB:: commit();
                return (object)[
                    'code' => '200',
                    'data' => (object)[
                        'data'    => $documents,
                        'message' => 'se han Borrado los erp_id de los documentos'
                    ]
                ];
            } catch (\Exception $e) {
                DB::rollBack();
                return (object)[
                    'code' => '400',
                    'data' => (object)[
                        'data'    => null,
                        'message' => 'Error al Borrar los erp'
                    ]
                ];
            }
        } else {
            return (object)[
                'code' => '404',
                'data' => 'No se Encontraron Documentos'
            ];
        }
    }

    public function documentoMargenUtilidad($request)
    {

    }

    /**
     * Este metodo se encarga de anular un documento
     * segun su ID
     *
     * @author Kevin Galindo
     */
    public function cancelDocument($id, $request)
    {
        // Traemos el documento
        $document = Document::find($id);

        // Organizamos las columnas que se van a poner en 0 del documento.
        $setValuesDocument = [
            // Valores de totales.
            'total_value_brut'        => 0,
            'total_value'             => 0,
            'total_value_brut_us'     => 0,
            'total_value_us'          => 0,
            'trm_value'               => 0,


            // Retenciones
            'ret_fue_prod_vr_limit'   => 0,
            'ret_fue_prod_percentage' => 0,
            'ret_fue_prod_total'      => 0,
            'ret_fue_serv_vr_limit'   => 0,
            'ret_fue_serv_percentage' => 0,
            'ret_fue_serv_total'      => 0,
            'ret_fue_total'           => 0,
            'ret_iva_prod_vr_limit'   => 0,
            'ret_iva_prod_percentage' => 0,
            'ret_iva_prod_total'      => 0,
            'ret_iva_serv_vr_limit'   => 0,
            'ret_iva_serv_percentage' => 0,
            'ret_iva_serv_total'      => 0,
            'ret_iva_total'           => 0,
            'ret_ica_prod_vr_limit'   => 0,
            'ret_ica_prod_percentage' => 0,
            'ret_ica_prod_total'      => 0,
            'ret_ica_serv_vr_limit'   => 0,
            'ret_ica_serv_percentage' => 0,
            'ret_ica_serv_total'      => 0,
            'ret_ica_total'           => 0,

            // Bases de retenciones
            'ret_fue_base_value'      => 0,
            'ret_iva_base_value'      => 0,
            'ret_ica_base_value'      => 0,
            'ret_fue_serv_base_value' => 0,
            'ret_iva_serv_base_value' => 0,
            'ret_ica_serv_base_value' => 0,

            // Anulado
            'canceled'                => true,
            'canceled_user_id'        => $request->user_id,
            'canceled_date'           => date('Y-m-d H:i:s'),
        ];

        // Organizamos las columnas que se van a poner en 0 del producto del documento.
        $setValuesDocumentProduct = [
            'quantity'                => 0,
            'original_quantity'       => 0,
            'unit_value_before_taxes' => 0,
            'discount_value'          => 0,
            'total_value_brut'        => 0,
            'total_value'             => 0,
            'physical_unit'           => 0,
            'total_value_us'          => 0,
            'total_value_brut_us'     => 0,
            'discount_value_us'       => 0,
            'unit_value_list'         => 0,
        ];

        // Organizamos las columnas que se van a poner en 0 en los impuestos de los productos.
        $setValuesDocumentProductTax = [
            'base_value'     => 0,
            'tax_percentage' => 0,
            'value'          => 0
        ];

        try {

            DB::beginTransaction();

            // Seteamos los valores del documento
            foreach ($document->documentsProducts as $key => $dp) {
                $dp->update($setValuesDocumentProduct);

                // Actualizamos los  inpuestos
                foreach ($dp->documentsProductsTaxes as $keytx => $tax) {
                    $tax->update($setValuesDocumentProductTax);
                }
            }

            // Seteamos los valores del documento.
            $document->update($setValuesDocument);

            // Confirmar cambios.
            DB::commit();

            // Recontruir el documento.
            $resultRebuildInventory = $this->inventoryService->rebuildDocument($id);

            if ($resultRebuildInventory->code == '500') {
                return [
                    'code' => '500',
                    'data' => [
                        'message' => 'Error al recontruir el documento'
                    ]
                ];
            }

            // Generar el pdf.
            $document = Document::find($id);

            $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document, true);

            Document::find($document->id)->update([
                'invoice_link' => $file_name
            ]);

            $document->filename = $file_name;

            // Retirnar respuesta.
            return [
                'data' => $document,
                'code' => 200
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'code' => '500',
                'data' => [
                    'message' => 'Error: "' . $e->getMessage() . '" \n Line: ' . $e->getLine() . "\nFile: " . $e->getFile()
                ]
            ];
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function finalizeOrderApp(array $data)
    {
        try {
            DB::beginTransaction();

            $document = $this->documentRepository->create($data);

            foreach ($data['documents_products'] as $document_product) {

                $request = new Request();

                $document_product['document_id'] = $document->id;
                $document_product['seller_id'] = $document->seller_id;
                $document_product['model_id'] = $document->model_id;

                $request->merge($document_product);

                $this->documentsProductsService->createDocumentProduct($request->all());
            }

            DB::commit();

            return $this->documentRepository->getDocument($document->id, $data);
        } catch (Exception $exception) {
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $request
     * @return string
     * @throws \Exception
     */
    public function generateReportAuthorizedDocuments($request, $user)
    {
        $data = $this->documentRepository->getAuthorizedDocuments($request->dates, $user);

        if ($data->count() <= 0) {
            throw new \Exception('No se encontraron registros para este usuario en las fechas seleccionadas', '500');
        }

        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle('Autorización de documentos');

        $row = 2;

        foreach ($data as $line) {

            if ($line->pending_authorization) {
                if ($line->closed) {
                    $stateAuthorization = 'RECHAZADO';
                } else {
                    $stateAuthorization = 'PENDIENTE POR AUTORIZAR';
                }
            } else {
                $stateAuthorization = 'AUTORIZADO';
            }

            $letter = 'A';
            $sheet->setCellValue($letter++ . $row, $line->document_date);
            $sheet->setCellValue($letter++ . $row, $line->template->code . ' - ' . $line->template->description);
            $sheet->setCellValue($letter++ . $row, $line->contact->identification . ' - ' . $line->contact->name . ' ' . $line->contact->surname);
            $sheet->setCellValue($letter++ . $row, $line->warehouse->code . ' - ' . $line->warehouse->description);
            $sheet->setCellValue($letter++ . $row, $line->consecutive);
            $sheet->setCellValue($letter++ . $row, is_null($line->userSolicitAuthorization) ? '' : $line->userSolicitAuthorization->name . ' ' . $line->userSolicitAuthorization->surname);
            $sheet->setCellValue($letter++ . $row, $line->date_solicit_authorization);
            $sheet->setCellValue($letter++ . $row, ($stateAuthorization == 'AUTORIZADO' || $stateAuthorization == 'RECHAZADO' ? (is_null($line->userAuthorization) ? '' : $line->userAuthorization->name . ' ' . $line->userAuthorization->surname) : ''));
            $sheet->setCellValue($letter++ . $row, ($stateAuthorization == 'AUTORIZADO' ? $line->document_date : ($stateAuthorization == 'RECHAZADO' ? $line->updated_at : '')));
            $sheet->setCellValue($letter++ . $row, $stateAuthorization);
            $row++;
        }

        //Se establece el filtro para todas las columnas
        $sheet->setAutoFilter('A1:I' . --$row);

        $this->setStyles($sheet, $row);

        $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

        $fileName = storage_path("app/public/reports/") . 'Reporte de Autorizacion de documentos.xlsx';

        $writer->save($fileName);

        return 'Reporte de Autorizacion de documentos.xlsx';
    }

    /**
     * @param $sheet
     * @param $row
     */
    public function setStyles($sheet, $row)
    {
        $headers = [
            'FECHA',
            'MODELO',
            'TERCERO',
            'SUCURSAL',
            'NO. DOCUMENTO',
            'USUARIO QUE SOLICITA',
            'FECHA DE SOLICITUD',
            'USUARIO QUE AUTORIZA',
            'FECHA DE AUTORIZACIÓN O RECHAZO',
            'ESTADO'
        ];

        $styles = [
            'A' => [
                'width' => 20
            ],
            'B' => [
                'width' => 50
            ],
            'C' => [
                'width' => 60
            ],
            'D' => [
                'width' => 60
            ],
            'E' => [
                'width' => 20
            ],
            'F' => [
                'width' => 40
            ],
            'G' => [
                'width' => 20
            ],
            'H' => [
                'width' => 40
            ],
            'I' => [
                'width' => 20
            ],
            'J' => [
                'width' => 25
            ]
        ];

        $generalStyles = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'borders'   => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN
                ]
            ]
        ];

        $counter = 0;

        foreach ($styles as $letter => $style) {
            $sheet->setCellValue($letter . '1', $headers[$counter++]);
            $sheet->getStyle($letter . '1')->getAlignment()->setWrapText(true);
            $sheet->getStyle($letter . '1')->getFont()->setBold(true);
            $sheet->getStyle($letter . '1')->getFont()->getColor()->setARGB('FFFFFF');
            $sheet->getStyle($letter . '1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('6386D3');
            $sheet->getStyle($letter . '1')->getFill()->getEndColor()->setARGB('6386D3');
            $sheet->getColumnDimension($letter)->setWidth($style['width']);
            $sheet->getColumnDimension($letter)->setAutoSize(isset($style['autoSize']) ? $style['autoSize'] : false);
            $sheet->getStyle($letter . '2:' . $letter . $row)->getAlignment()->setWrapText(isset($style['wrapText']) ? $style['wrapText'] : false);
        }

        $sheet->getStyle('A2:A' . $row)->getNumberFormat()->setFormatCode(
            NumberFormat::FORMAT_DATE_DATETIME
        );
        $sheet->getStyle('A1:J' . $row)->applyFromArray($generalStyles);
    }

    /**
     * Método que emite ante la DIAN un documento finalizado previamente
     *
     * @param $document_id contiene el id del documento a emitir
     * @return mixed
     * @throws \Exception
     */
    public function sendFinishedDocument($document_id)
    {
        // Traemos la informacion del documento
        $document = $this->documentRepository->getDocument($document_id);

        // Validamos que traiga informacion
        if (empty($document)) {
            return (object)[
                'code' => '404',
                'data' => (object)[
                    'message' => 'Documento no encontrado'
                ]
            ];
        }

        try {
            DB::beginTransaction();

            // Actualizamos los valores de document_information.
            $document = $this->documentRepository->finalizeDocumentInformation($document);

            //Emitir factura electronica ante la DIAN
            $document = $this->electronicInvoice($document, false);

            $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document);

            Document::find($document->id)->update([
                'invoice_link' => $file_name
            ]);

            $document->filename = $file_name;

            DB::commit();

            $this->sendElectronicInvoice($document, false);

            return $document;
        } catch (\Exception $e) {
            DB::rollBack();

            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            throw new \Exception('Error en en la emisión del documento: ' . $e->getMessage(), 400);
        }
    }

    public function fixBackorder()
    {
        try {
            set_time_limit(0);
            ini_set('memory_limit', '-1');

            DB::beginTransaction();

            $pedidos = Document::where('in_progress', false)->where('operationType_id', 8)->where('closed', false)
                ->with(['documentsProducts',
                    'documentThread.documentsProducts',
                    'documentThread.documentThread.documentsProducts'
                ])->get();
            Log::info('cantidad pedidos' . count($pedidos));

            $fecha = Carbon::parse('2020-11-01');

            foreach ($pedidos as $key => $pedido) {
                $fecha_pedido = Carbon::parse($pedido->document_date);
                $backorder_total = 0;
                if ($fecha_pedido >= $fecha) {
                    foreach ($pedido->documentsProducts as $key => $documentproduct) {
                        $backorder_product = $documentproduct->quantity;


                        foreach ($pedido->documentThread as $key => $factura) {
                            if ($factura->in_progress) {
                                continue;
                            }
                            if ($pedido->id == $factura->id || $factura->operationType_id != 5) {
                                continue;
                            }

                            foreach ($factura->documentsProducts as $key => $f_dp) {
                                if ($documentproduct->product_id == $f_dp->product_id && $documentproduct->product_repeat_number == $f_dp->product_repeat_number) {
                                    $backorder_product -= $f_dp->quantity;
                                }

                                foreach ($factura->documentThread as $key => $devolucion) {
                                    if ($devolucion->in_progress) {
                                        continue;
                                    }
                                    if ($factura->id == $devolucion->id || $devolucion->operationType_id != 1) {
                                        continue;
                                    }
                                    foreach ($devolucion->documentsProducts as $key => $d_dp) {
                                        if ($documentproduct->product_id == $d_dp->product_id && $documentproduct->product_repeat_number == $d_dp->product_repeat_number) {
                                            $backorder_product += $d_dp->quantity;
                                        }
                                    }
                                }
                            }
                        }
                        $backorder_total += $backorder_product;
                        $document_updated = DocumentProduct::where('id', $documentproduct->id)
                            ->update([
                                'backorder_quantity' => $backorder_product,
                            ]);
                    }
                } else {
                    DocumentProduct::where('document_id', $pedido->id)
                        ->update([
                            'backorder_quantity' => 0,
                        ]);
                }
                $backorder_total = ($backorder_total < 0 ? 0 : $backorder_total);
                $pedido_act = Document::where('id', $pedido->id)
                    ->update([
                        'backorder_quantity' => $backorder_total,
                    ]);
                Log::info($key . '   =>   ' . $pedido->id . '   ' . $backorder_total);
            }


            $facturas = Document::where('in_progress', false)->where('operationType_id', 5)->where('closed', false)
                ->with(['documentsProducts',
                    'documentThread.documentsProducts',
                ])->get();
            Log::info('CANTIDAD FACTURAS ' . count($facturas));

            foreach ($facturas as $key => $factura) {
                $_backorder_total = 0;

                foreach ($factura->documentsProducts as $key => $f_d_p) {
                    $_backorder_product = $f_d_p->quantity;

                    foreach ($factura->documentThread as $key => $devolucion_) {
                        if ($devolucion_->in_progress) {
                            continue;
                        }
                        if ($devolucion->operationType_id != 1) {
                            continue;
                        }
                        if ($factura->id == $devolucion_->id) {
                            continue;
                        }
                        foreach ($devolucion_->documentsProducts as $key => $d_d_p) {
                            if ($f_d_p->product_id == $d_d_p->product_id && $f_d_p->product_repeat_number == $d_d_p->product_repeat_number) {
                                $_backorder_product -= $d_d_p->quantity;
                            }
                        }
                    }
                    $_backorder_total += $_backorder_product;
                    $document_updated = DocumentProduct::where('id', $f_d_p->id)
                        ->update([
                            'backorder_quantity' => $_backorder_product,
                        ]);
                }

                $_backorder_total = ($_backorder_total < 0 ? 0 : $_backorder_total);
                $fac_act = Document::where('id', $factura->id)
                    ->update([
                        'backorder_quantity' => $_backorder_total,
                    ]);
                // Log::info($key.'   =>   '. $factura->id .'   '.$backorder_total);

            }

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
        }

    }

    public function validateMinimalPriceDocumentProducts($document, $model)
    {
        $documentProductsError = [];

        if ($model->minimal_price) {
            foreach ($document->documentsProducts as $key => $documentProduct) {

                if ($documentProduct->is_benefit == true) {
                    continue;
                }

                $documentProduct = $this->documentProductRepository->addMinimalPriceDocumentProducts($documentProduct, ['model_id' => $model->id]);

                $minimalPrice = $documentProduct->minimalPrice['price'];

                if ($minimalPrice <= 0) {
                    continue;
                }

                $valueBrutUnit = round($documentProduct->total_value_brut / $documentProduct->quantity);

                if ($minimalPrice > $valueBrutUnit) {
                    // if (true) {
                    $documentProductsError[] = [
                        'document_product_id' => $documentProduct->id,
                        'minimalPrice'        => $documentProduct->minimalPrice,
                        'message'             => "Codigo: " . $documentProduct->product->code . " - Valor precio minimo: ",
                        'price'               => $minimalPrice
                    ];
                }
            }
        }

        return $documentProductsError;
    }

    public function finalizeAccountingDocument($request)
    {

        try {
            DB::beginTransaction();

            $document = Document:: find($request->document_id);

            foreach ($request->document_transactions as $key => $value) {
                // si la transaccion no tiene cuenta se omite
                if (is_null($value['account_id'])) continue;

                $account = Account :: find($value['account_id']);
                if ($account->manage_document_balances && $document->vouchertype->origin_document) {
                    $value['affects_document_id'] = $document->id;
                }

                if (!isset($value["id_document_transactions"])) {
                    if ($value["value"]!= 0) {
                        $request->document_transaction = $value;
                        $this->documentTransactionsRepository->store($request, $document);
                    }
                } else {
                    $request->document_transaction = $value;
                    $this->documentTransactionsRepository->update_($value["id_document_transactions"], $request, $document);
                }
            }

            $document->accounting_debit_value = $request->total_debit;
            $document->accounting_credit_value = $request->total_credit;

            $document->in_progress = false;
            $document->save();

            $this->vouchersTypeRepository->updateConsecutive($document->vouchertype_id, $document->consecutive);

            $accounting_document = $this->documentRepository->getAccountDocument($document->id);
            $file_name = "/storage/pdf/" . $this->generateAccountingDocument($accounting_document);
            $accounting_document->accounting_link = $file_name;

            DB::commit();

            $document_date = Carbon::parse($document->document_date);

            UpdateBalancesJob::dispatch($document_date->format('Ym'), $document->id)->onQueue('high');

            return (object)[
                'code' => '200',
                'data' => (object)[
                    'data'    => $accounting_document,
                    'message' => 'Finalizado'
                ]
            ];
        } catch (\Exception $e) {
            Log::error('------------- finalizacion documento contable ------------');
            Log::error($e->getMessage() . ' ' . $e->getLine() . " " . $e->getFile());

            DB::rollBack();

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Hubo un error al obtener documeto a modificar'
                ]
            ];
        }
    }

    public function UpdateAccountingDocument($request)
    {
        $document_update = Document:: find($request->document_id);

        $document_date = Carbon::parse($document_update->document_date);

        UpdateBalancesJob::dispatch($document_date->format('Ym'), $document_update->ref_update_doc , true)->onQueue('high');

        $model = $this->getModelForIdFromCache($request->template_id);

        //seteamos datos del formulario
        $document_update->contact_id = $request->contact_id;
        $document_update->branchoffice_id = $request->branchoffice_id;
        $document_update->observation = $request->observation;
        $document_update->observation_two = $request->observation2;
        // $document_update->document_date =  strlen($request->date) > 10 ? $request->date : $request->date.date(' H:i:s');
        $document_update->city = $request->city_id;
        $document_update->operationType_id = $model ? $model->operationType_id : null;
        $document_update->model_id = $request->template_id;
        $document_update->accounting_user_last_modification = $request->user_id;
        $document_update->accounting_date_last_modification = Carbon:: now();
        $document_update->center_cost_id = $request->center_cost;
        $document_update->projects_id = $request->projects_id;
        $document_update->seller_id = $request->seller_id;
        $document_update->affects_prefix_document = $request->affects_prefix_document;
        $document_update->affects_number_document = $request->affects_number_document;
        // $document_update->affects_date_document = $request->affects_date_document;
        $document_update->due_date = $request->due_date;
        $document_update->term_days = $request->term_days;

        $document_update->accounting_debit_value = $request->total_debit;
        $document_update->accounting_credit_value = $request->total_credit;

        $document_update->in_progress = false;
        $document_update->user_id_last_modification = $request->user_id;
        $document_update->date_last_modification = Carbon:: now();
        $document_update->save();

        $old_document = Document:: find($document_update->ref_update_doc);
        $old_document->in_progress = true;
        $old_document->save();


        $accounting_document = $this->documentRepository->getAccountDocument($document_update->id);
        $file_name = "/storage/pdf/" . $this->generateAccountingDocument($accounting_document);
        $accounting_document->accounting_link = $file_name;

        UpdateBalancesJob::dispatch($document_date->format('Ym'), $document_update->id)->onQueue('high');

        $this->validateLastDocumentSubscriber($document_update);

        return (object)[
            'code' => '200',
            'data' => (object)[
                'data'    => $accounting_document,
                'message' => 'Finalizado'
            ]
        ];
    }

    public function getSearchDocument($request)
    {
        $res = $this->documentRepository->getSearchDocument($request);
        $firm_date = FirmDate::first();

        if (!is_null($firm_date)) {
            foreach ($res as $key => $doc) {
                $doc->modify_document_firm_date = true;
                if ($doc->document_date < $firm_date->firm_date_general ) {
                    $doc->modify_document_firm_date =  false;
                }
            }
        }

        return $res;
    }

    public function validateLastDocumentSubscriber($document)
    {
        $user = JWTAuth::parseToken()->toUser();
        $subscriber = $user->subscriber;
        $document_subscriber = $subscriber->document;

        if (is_null($document_subscriber)) {
            $subscriber->document_id = $document->id;
        }else{
            if ($document->document_date < $document_subscriber->document_date) {
                $subscriber->document_id = $document->id;
            }
        }

        $subscriber->save();

    }

    public function editDocument($id)
    {
        try {
            DB::beginTransaction();

            $old_document = $this->documentRepository->getEditDocument($id);

            //productos
            foreach ($old_document['documents_products'] as $dp) {
                $dp['document_id'] = $old_document["new_document_id"];
                $documentProduct = DocumentProduct::create($dp);

                // impuestos
                foreach ($dp['document_product_taxes'] as $dpt) {
                    $dpt['document_product_id'] = $documentProduct->id;
                    DocumentProductTax::create($dpt);
                }
            }

            //docinf
            $old_document["documents_information"]["document_id"] = $old_document["new_document_id"];
            DocumentInformation:: create($old_document["documents_information"]);

            //formas de pago
            // foreach ($old_document["payment_methods"] as $key => $paymentMethod) {
            //     $paymentMethod['document_id'] = $old_document["new_document_id"];
            //     DocumentPaymentMethod::create($paymentMethod);
            // }

            // Archivos
            foreach ($old_document['files'] as $key => $file) {
                $file['document_id'] = $old_document["new_document_id"];
                DocumentFile::create($file);
            }

            // Inspeccion de vehiculo
            // foreach ($old_document['document_vehicle_inspection'] as $key => $item) {
            //     $item['document_id'] = $old_document["new_document_id"];
            //     DocumentVehicleInspection::create($item);
            // }

            // transacciones
            foreach ($old_document['accounting_transactions'] as $key => $item) {
                $item['document_id'] = $old_document["new_document_id"];
                DocumentTransactions::create($item);
            }

            // Vehiculo
            // if (isset($old_document['document_vehicle']) && isset($old_document['mileage'])) {
            //     $old_document['document_vehicle']['document_id'] = $old_document["new_document_id"];
            //     DocumentVehicle::create($old_document['document_vehicle']);
            // }
            DB::commit();

            $document = $this->documentRepository->getAccountingDocumentEdit($old_document["new_document_id"]);
            $document->in_progress = false;
            // dd($document->ToArray());


            //obtenemos el valor iva de manera informativa
            $document->total_quantity = 0;
            $document->total_discount = 0;
            $document->total_vr_brut = 0;
            $document->total_vr_iva = 0;
            $document->total_ = 0;
            $document->total_items = count($document->documentsProducts);
            foreach ($document->documentsProducts as $key => $document_product) {

                $document->total_quantity += $document_product->quantity;
                $document->total_discount += $document_product->discount_value;
                $document->total_vr_brut += $document_product->total_value_brut;
                $document->total_ += $document_product->total_value;
                $document->total_inventory_cost_value += $document_product->inventory_cost_value;

                $document_product->vr_iva = 0;
                foreach ($document_product->documentsProductsTaxes as $key => $document_product_taxes) {
                    $document_product->vr_iva = $document_product_taxes->tax_id == 1 ? $document_product_taxes->value : $document_product->vr_iva;
                    $document->total_vr_iva += $document_product->vr_iva;
                }
            }


            $document["modify_document_firm_date"] = true;
            foreach ($document->accounting_transactions as $key => $accounting_transaction) {
                // validamos  fecha firme en la cuenta
                $firm_date = FirmDate::where('account_id', $accounting_transaction->account_id)->first();

                if (!is_null($firm_date)) {
                    if ($document["document_date"] < $firm_date->firm_date_account ) {
                        $document["modify_document_firm_date"] =  false;
                    }
                }
            }

            return (object)[
                'code'      => '200',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'exito',
                    'data'    => $document
                ]
            ];

        } catch (\Exception $e) {
            Log::error('------------- modificación documento contable ------------');
            Log::error($e->getMessage() . ' ' . $e->getLine() . " " . $e->getFile());

            DB::rollBack();

            return (object)[
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Hubo un error al obtener documeto a modificar'
                ]
            ];
        }
    }

    public function searchDocument_($request)
	{
		$res = $this->documentRepository->searchDocument_($request);
        if (!is_null($res->data->data)) {
            $res->data->data = $this->validateFirmDateByAccount($res->data->data->toArray());
        }
        return $res;
	}

    public function validateFirmDateByAccount($document)
    {

        $document["modify_document_firm_date"] = true;
        foreach ($document["accounting_transactions"] as $key => $accounting_transaction) {
            // validamos  fecha firme en la cuenta
            $firm_date = FirmDate::where('account_id', $accounting_transaction["account_id"])->first();

            if (!is_null($firm_date)) {
                if ($document["document_date"] < $firm_date->firm_date_account ) {
                    $document["modify_document_firm_date"] =  false;
                }
            }
        }
        return $document;
    }

    public function retainAuthorizedDocuments($configAurora)
    {
        try {
            DB::beginTransaction();

            $limitDays = 5;
            $stateBackorder = [
                828,
                830
            ];

            $statusAuthorized = [
                912
            ];

            $documents = Document::whereHas('operationType', function ($query) {
                $query->where('code', '010');
            })
                ->where([
                    'in_progress' => false,
                    // 'branchoffice_id' => 305
                ])
                ->where(function ($query) {
                    $query->where('canceled', false);
                    $query->orWhereNull('canceled');
                })
                ->where(function ($query) {
                    $query->where('closed', false);
                    $query->orWhereNull('closed');
                })
                ->whereIn('state_backorder', $stateBackorder)
                ->whereIn('status_id_authorized', $statusAuthorized)
                ->get();

            $documentsRetain = [];

            // Valida si el documento debe ser retenido o no.
            foreach ($documents as $key => $document) {
                $dateAuthorized = Carbon::parse($document->date_authorized);
                $today = Carbon::now();
                $diffInDays = $dateAuthorized->diffInDays($today);

                if ($diffInDays >= $limitDays) {
                    // $document->observation_authorized = "retenido sistema";
                    $document->status_id_authorized = 914;
                    // $document->user_id_authorizes = null;
                    $document->save();
                    $documentsRetain[] = $document;
                }
            }

            // Enviar emails
            $emails = json_decode($configAurora->email);

            if (count($emails) > 0) {
                foreach ($emails as $key => $email) {

                    $user = User::where('email', $email)->first();

                    $mail = new RetainAuthorizedDocumentsMail($documentsRetain, $user ? $user->contact : null);
                    Mail::to($email)->send($mail);
                }
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return false;
        }
    }

    public function documentsThreadHistoryByDocumentId($request)
    {
        $documentMain = null;
        $documentsArray = collect();

        $documentSelected = Document::find($request->document_id);

        // Buscar el documento principal.
        $execute = true;
        while ($execute) {

            if (!$documentSelected->document || $documentSelected->id === $documentSelected->thread) {
                $execute = false;
                $documentMain = $documentSelected;
            }

            $documentSelected = $documentSelected->document;

        }

        $documentsArray = $documentsArray->merge([$documentMain]);

        // Buscamos los documento asociados
        $documentsThread = $documentMain->documentThread->where('in_progress', false);
        $documentsArray = $documentsArray->merge($documentsThread);

        foreach ($documentsThread as $key => $document) {
            $arr = $document->documentThread->where('in_progress', false);
            $documentsArray = $documentsArray->merge($arr);
        }


        $returnArray = [];
        foreach ($documentsArray as $key => $document) {
            $returnArray[] = [
                'id'                              => $document->id,
                'model_id'                        => $document->model_id,
                'template'                        => $document->template,
                'prefix'                          => $document->prefix,
                'consecutive'                     => $document->consecutive,
                'total_value'                     => $document->total_value,
                'file_name'                       => $document->invoice_link,
                'date'                            => $document->document_date,
                'voucherType'                     => $document->voucherType,
                'observation'                     => $document->observation,
                'handling_other_parameters_value' => $document->handlingOtherParametersValue,
                'user'                            => User::select('id', 'email', 'photo', 'contact_id', 'name')->with([
                    'contact' => function ($query) {
                        $query->select('id', 'name', 'surname', 'identification');
                    }
                ])->find($document->user_id),
            ];
        }


        return [
            'data' => collect($returnArray)->unique('id')->sortBy('id')->values(),
            'code' => 200
        ];

    }

    /**
     * @param $document
     * @param $model
     * @param $baseDocument
     * @param $request
     * @return mixed
     */
    public function validatedUpdateDocValues($document, $model, $baseDocument, $request)
    {
        $update_doc = $request->update_doc;

        // Buscamos y guardamos el resultado.
        $inventories = Inventory::whereIn('product_id', $document->documentsProducts->pluck('product_id'))->get();

        if (empty($update_doc)) {
            if (!$model->credit_note) {

                $affectation_inventory = null;

                if ($request->model_id) {
                    $model = $this->getModelForIdFromCache($request->model_id);
                    $affectation_inventory = $model->operationType->affectation;
                }
                // Actualizar cantidades
                $baseDocumentProducts = collect($baseDocument['documents_products']);

                foreach ($document->documentsProducts as $key => $document_product) {

                    $document_product->physical_unit = $document_product->quantity;

                    if ($document_product->backorder_quantity > 0) {
                        // Realizamos la busqueda al inventario del producto
                        $productInventory = $document_product->product->inventory->first();

                        if (isset($document_product->backorder_quantity)) {
                            // Si el "backorder_quantity" es mayor al "stock" en inventario se guarda en valor del "stock" en "cantidad"
                            if ($document_product->product->manage_inventory) {
                                switch ($affectation_inventory) {
                                    case 2:
                                        if (!isset($productInventory->stock)) {
                                            $document_product->quantity = 0;
                                            $document_product->physical_unit = 0;
                                        } else {
                                            if ($document_product->backorder_quantity > $productInventory->stock) {
                                                $document_product->quantity = $productInventory->stock;
                                                $document_product->physical_unit = $productInventory->stock;
                                            } else {
                                                $document_product->quantity = $document_product->backorder_quantity;
                                                $document_product->physical_unit = $document_product->backorder_quantity;
                                            }
                                        }
                                        break;
                                    case 1:
                                    case 3:
                                        $document_product->quantity = $document_product->backorder_quantity;
                                        $document_product->physical_unit = $document_product->backorder_quantity;
                                        break;
                                }
                            }
                        }
                    }

                    $dataUpdate = [
                        'total_value'      => $document_product->total_value,
                        'total_value_brut' => $document_product->total_value_brut,
                        'physical_unit'    => $document_product->physical_unit
                    ];

                    if ($update_doc) {
                        $dataUp = $baseDocumentProducts->where('product_id', $document_product->product_id)
                            ->where('product_repeat_number', $document_product->product_repeat_number)
                            ->first();

                        $document_product->quantity = $dataUp['quantity'];
                    }

                    $dataUpdate['quantity'] = $document_product->quantity;

                    $document_product->update($dataUpdate);

                    foreach ($document->document->documentsProducts as $key => $base_document_product) {
                        if ($document_product->product_id == $base_document_product->product_id) {
                            $document_product->base_quantity = $base_document_product->quantity;
                            $document_product->base_unit_value_before_taxes = round($base_document_product->unit_value_before_taxes);
                            $document_product->base_unit_value = round($base_document_product->unit_value_before_taxes);
                            break;
                        }
                    }

                    $document_product = $this->setDiscountValueUnitToDocumentProduct($document_product);

                    $document_product = $this->setInventoryToDocumentProduct($document_product, $document->branchoffice_warehouse_id, $inventories);
                }

            } else {
                foreach ($document->documentsProducts as $keydp => $document_product) {
                    foreach ($document_product->documentProductTaxes as $keyT => $documentProductTax) {
                        if ($documentProductTax->tax->code == '10') {
                            $document->documentsProducts[$keydp]->total_value_iva = $documentProductTax->value;
                            break;
                        }
                    }

                    $document_product = $this->setDiscountValueUnitToDocumentProduct($document_product);

                    $document_product = $this->setInventoryToDocumentProduct($document_product, $document->branchoffice_warehouse_id, $inventories);
                }
            }
        } else {

            $taxesDoc = [];

            $sale = $model->operationType->affectation == 2;

            foreach ($document->documentsProducts as $keydp => $document_product) {
                $document->documentsProducts[$keydp]['total_value_iva'] = null;

                foreach ($document_product->documentProductTaxes as $keyT => $documentProductTax) {
                    if ($documentProductTax->tax->code == '10') {
                        $document->documentsProducts[$keydp]->total_value_iva = $documentProductTax->value;
                    }

                    // Validamos si el tax ya existe en el arreglo.
                    $key_tax = array_search(
                        $documentProductTax->tax_id,
                        array_column($taxesDoc, 'tax_id')
                    );

                    // Crear
                    if ($key_tax === false) {
                        $taxesDoc[] = [
                            'tax_id'      => $documentProductTax->tax_id,
                            'value'       => $documentProductTax->value,
                            'percentage'  => $sale ? $documentProductTax->tax->percentage_purchase : $documentProductTax->tax->percentage_sale,
                            'base_value'  => $documentProductTax->base_value,
                            'description' => $documentProductTax->tax->description
                        ];
                    } // Actualizar
                    else {
                        $taxesDoc[$key_tax]['value'] += $documentProductTax->value;
                    }
                }

                $document_product = $this->setDiscountValueUnitToDocumentProduct($document_product);

                $document_product = $this->setInventoryToDocumentProduct($document_product, $document->branchoffice_warehouse_id, $inventories);
            }

            $document->taxes = $taxesDoc;
        }

        return $document;
    }

    //--------------------------- INICIA REESTRUCTURACIÓN DE LA CREACIÓN DE UN DOCUMENTO DESDE UN DOCUMENTO BASE ----------------------------- \\

    /**
     * Establecer valores y crear documento con base en otro
     *
     * @param Request $request Contiene los parámetros enviados en la petición Http
     * @param User $user Contiene la información del usuario autenticado
     * @return mixed $new_document que contiene el documento creado
     * @author Jhon García
     */
    public function createDocumentFromBase(Request $request, User $user)
    {
        // Variables
        $taxes = 0;
        $document_id = $request->document_id;
        $updating_document = $request->update_doc == 'true';

        $voucher_type = VouchersType::find($request->vouchertype_id);

        // Obtener modelo
        $model = $this->getModelForIdFromCache($request->model_id);

        $base_document = $this->getBaseDocumentForId($document_id, $model, $updating_document, $user, $request, $voucher_type);

        try {
            // Deshabilitar la auditoria temporalmente
            Document::disableAuditing();
            DocumentProduct::disableAuditing();
            DocumentProductTax::disableAuditing();
            DocumentPaymentMethod::disableAuditing();
            DocumentFile::disableAuditing();

            DB::beginTransaction();

            $except = ['id', 'erp_id', 'returned_document'];

            if (!$model->select_projects) {
                array_push($except, 'trm_value');
            }
            // Crear documento
            $new_document = Document::create(Arr::except($base_document, $except));

            // Copiar el document_information
            $new_document->documentsInformation()->create($base_document['documents_information']);

            // Validar si el modelo indica que hereda productos
            if ($model->inherit_products) {

                // Llamar método que se encarga de copiar los documents_products del documento base al nuevo documento
                $taxes = $this->createDocumentsProductsFromBase($base_document, $new_document, $model, $request, $updating_document);

                // Validar si el modelo indica que puede tener multiples documentos base
                if ($model->multiple_documents && $request->additional_documents){

                    $additional_documents_ids = explode(',', $request->additional_documents);

                    foreach ($additional_documents_ids as $additional_document_id) {

                        $additional_document = $this->getBaseDocumentForId($additional_document_id, $model, $updating_document, $user, $request, $voucher_type);

                        $taxes += $this->createDocumentsProductsFromBase($additional_document, $new_document, $model, $request, $updating_document);
                    }
                }

                $this->calculateRetentionsForDocument($new_document, $model);
            }

            // Copiar los métodos de pago
            $new_document->paymentMethods()->sync($base_document["payment_methods"]);

            // Copiar los archivos
            $new_document->files()->sync($base_document["files"]);

            DB::commit();

            $document = $this->documentRepository->getDocument($new_document->id, $request->all());
            $document->taxes = $taxes;
            $document->total_value_brut = $document->documentsProducts->sum('total_value_brut');
            $document->total_value = floor($document->documentsProducts->sum('total_value'));

            // Habilitar auditoria
            Document::enableAuditing();
            DocumentProduct::enableAuditing();
            DocumentProductTax::enableAuditing();
            DocumentPaymentMethod::enableAuditing();
            DocumentFile::enableAuditing();

            return [
                'code' => '201',
                'data' => $document
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e);

            // Habilitar auditoria
            Document::enableAuditing();
            DocumentProduct::enableAuditing();
            DocumentProductTax::enableAuditing();
            DocumentPaymentMethod::enableAuditing();
            DocumentFile::enableAuditing();

            return [
                'code'      => '500',
                'completed' => false,
                'data'      => (object)[
                    'message' => 'Error creando el documento del documento base "' . $e->getMessage() . '"' . $e->getLine() . "File: " . $e->getFile()
                ]
            ];
        }
    }

    /**
     * @param $document_id
     * @param $model
     * @param bool $updating_document
     * @param User $user
     * @param Request $request
     * @param $voucher_type
     * @return mixed
     */
    public function getBaseDocumentForId($document_id, $model, bool $updating_document, User $user, Request $request, $voucher_type)
    {
        // Obtener documento base
        $base_document = Document::where('id', $document_id)
            ->with(
                [
                    'documentsProducts.documentProductTaxes',
                    'documentsProducts.product.line.taxes',
                    'files',
                    'documentsInformation',
                    'contact',
                    'warehouse:id,seller_id',
                    'paymentMethods',
                    'operationType'
                ]
            )
            ->first()
            ->toArray();

        // Validar si se deben calcular los valores según el modelo
        if ($model->priceList && $model->priceList->code_parameter == '002') {
            $base_document['total_value'] = 0;
            $base_document['total_value_brut'] = 0;
            $base_document['ret_fue_prod_vr_limit'] = 0;
            $base_document['ret_fue_prod_percentage'] = 0;
            $base_document['ret_fue_prod_total'] = 0;
            $base_document['ret_fue_serv_vr_limit'] = 0;
            $base_document['ret_fue_serv_percentage'] = 0;
            $base_document['ret_fue_serv_total'] = 0;
            $base_document['ret_fue_total'] = 0;
            $base_document['ret_iva_prod_vr_limit'] = 0;
            $base_document['ret_iva_prod_percentage'] = 0;
            $base_document['ret_iva_prod_total'] = 0;
            $base_document['ret_iva_serv_vr_limit'] = 0;
            $base_document['ret_iva_serv_percentage'] = 0;
            $base_document['ret_iva_serv_total'] = 0;
            $base_document['ret_iva_total'] = 0;
            $base_document['ret_ica_prod_vr_limit'] = 0;
            $base_document['ret_ica_prod_percentage'] = 0;
            $base_document['ret_ica_prod_total'] = 0;
            $base_document['ret_ica_serv_vr_limit'] = 0;
            $base_document['ret_ica_serv_percentage'] = 0;
            $base_document['ret_ica_serv_total'] = 0;
            $base_document['ret_ica_total'] = 0;
        }

        // Sede por defecto según el modelo.
        if (!empty($model->branchoffice_id)) {
            $base_document['branchoffice_id'] = $model->branchoffice_id;
        }

        // Bodega por defecto según el modelo.
        if (!empty($model->branchoffice_warehouse_id)) {
            $branch_office_warehouse = $this->getBranchOfficeWarehouse($base_document['branchoffice_id'], $model->branchOfficeWarehouse->warehouse_code);

            if ($branch_office_warehouse) {
                $base_document['branchoffice_warehouse_id'] = $branch_office_warehouse->id;
            }
        }

        // Validar si es una actualización de documento
        if ($updating_document) {
            $base_document['ref_update_doc'] = $document_id;
        } else {
            $base_document['user_id'] = $user->id;
            $base_document['seller_id'] = $base_document['warehouse']['seller_id'];
            $base_document['term_days'] = $base_document['contact']['pay_days'];
            $base_document['document_date'] = date('Y-m-d H:i:s');
            $base_document['vouchertype_id'] = $request->vouchertype_id;
            $base_document['operationType_id'] = $model->operationType_id;
            $base_document['branchoffice_warehouse_id'] = $request->branchoffice_warehouse_id;
        }

        $base_document['thread'] = $document_id;
        $base_document['in_progress'] = true;
        $base_document['origin_document'] = $voucher_type->origin_document;

        return $base_document;
    }

    /**
     * Obtener los precios e iterar los documents_products de un documento base para copiarlos
     *
     * @param array $base_document Contiene la información del documento base
     * @param Template $model Contiene la información del modelo
     * @param Request $request Contiene los parámetros enviados en la petición
     * @return array Contiene la información de los documents_products creados
     * @author Jhon García
     */
    public function createDocumentsProductsFromBase($base_document, $new_document, $model, $request, $updating_document): array
    {
        // Obtener los precios de los productos existentes en el documento base
        $prices = Price::select('product_id', 'price')
            ->whereIn('product_id', Arr::pluck($base_document['documents_products'], 'product_id'))
            ->where('price_list_id', $model->price_list_id)
            ->where(function ($branch_office) use ($base_document) {
                $branch_office->where('branchoffice_id', $base_document['branchoffice_id'])
                    ->orWhereNull('branchoffice_id');
            })
            ->orderBy('branchoffice_id', 'DESC')
            ->get();


        // Obtener el inventario de los productos existentes en el documento base
        $inventories = Inventory::select('product_id', 'stock', 'branchoffice_warehouse_id')
            ->whereIn('product_id', Arr::pluck($base_document['documents_products'], 'product_id'))
            ->where(function ($branch_office) use ($base_document) {
                $branch_office->whereIn('branchoffice_warehouse_id', Arr::pluck($base_document['documents_products'], 'branchoffice_warehouse_id'))
                    ->orWhere('branchoffice_warehouse_id', $base_document['branchoffice_warehouse_id']);
            })
            ->get();

        $documents_products_taxes = [];

        if (!$updating_document && !$model->credit_note) {

            // Recorrer los documents_products del documento base
            foreach ($base_document['documents_products'] as $base_document_product) {

                $base_document_product['base_document_product_id'] = $base_document_product['id'];

                if ($base_document_product['backorder_quantity'] === 0) {
                    $new_document_product = Arr::except($base_document_product, ['discount_value_us', 'discount_value', 'total_value', 'total_value_brut']);

                    $new_document_product['quantity'] = 0;
                } else {
                    $new_document_product = $base_document_product;

                    $new_document_product['quantity'] = $base_document_product['quantity'];
                }

                $new_document_product['document_id'] = $new_document->id;

                // Precio de lista por sede
                $price = $prices->firstWhere('product_id', $new_document_product['product_id']);
                $new_document_product['unit_value_list'] = $price ? $price->price : 0;

                if ((!empty($model->branchoffice_change) && empty($base_document_product['branchoffice_warehouse_id'])) || empty($model->branchoffice_change)) {
                    $new_document_product['branchoffice_warehouse_id'] = $request->branchoffice_warehouse_id;
                }

                // Cantidad
                $new_document_product['quantity'] = $this->getQuantityForDocumentProduct($new_document, $new_document_product, $model, $inventories);
                $new_document_product['physical_unit'] = $new_document_product['quantity'];

                $new_document_product['operationType_id'] = $model->operationType_id;
                $new_document_product['total_value_brut'] = ($base_document_product['unit_value_before_taxes'] - $base_document_product['discount_value_unit']) * $new_document_product['quantity'];
                $new_document_product['model_id'] = $model->id;

                // Crear el document_product
                $document_product = DocumentProduct::create($new_document_product);

                $document_product_taxes = $this->createDocumentProductTaxesFromBase($base_document_product, $document_product, $new_document->operationType->code, $model);

                $new_document_product['total_value'] = collect($document_product_taxes)->sum('value') + $new_document_product['total_value_brut'];

                $document_product->update($new_document_product);

                $documents_products_taxes = array_merge($documents_products_taxes, $document_product_taxes);
            }
        } else {
            // Recorrer los documents_products del documento base
            foreach ($base_document['documents_products'] as $base_document_product) {

                $new_document_product = Arr::except($base_document_product, ['document_id']);

                $new_document_product['document_id'] = $new_document->id;

                $new_document_product['operationType_id'] = $model->operationType_id;
                $new_document_product['model_id'] = $model->id;

                // Crear el document_product
                $document_product = DocumentProduct::create($new_document_product);

                $document_product_taxes = $this->createDocumentProductTaxesFromBase($base_document_product, $document_product, $new_document->operationType->code, $model);

                $new_document_product['total_value'] = collect($document_product_taxes)->sum('value') + $new_document_product['total_value_brut'];

                $document_product->update($new_document_product);

                $documents_products_taxes = array_merge($documents_products_taxes, $document_product_taxes);
            }
        }

        return $this->calculateTotalTaxesDocument($documents_products_taxes);
    }

    /**
     * Calcular la cantidad de un document product
     *
     * @param Document $new_document Contiene la información del documento creado
     * @param array $document_product Contiene la información del document product
     * @param Template $model Contiene la información del modelo
     * @param Collection $inventories Contiene el inventario de los productos del documento base
     * @return int Contiene la cantidad a asignar
     * @author Jhon García
     */
    public function getQuantityForDocumentProduct($new_document, $document_product, $model, $inventories): int
    {
        // Obtener el tipo de afectación del inventario
        $affectation_inventory = $model->operationType->affectation;

        // Si el "backorder_quantity" es mayor al "stock" en inventario se guarda en valor del "stock" en "cantidad"
        if ($document_product['backorder_quantity'] >= 0 && $document_product['product']['manage_inventory']) {
            if ($affectation_inventory == 2) {

                $branch_office_warehouse_id = $document_product['branchoffice_warehouse_id'] ? $document_product['branchoffice_warehouse_id'] : $new_document->branchoffice_warehouse_id;

                // Obtener el inventario del producto
                $product_inventory = $inventories->where('product_id', $document_product['product_id'])
                    ->where('branchoffice_warehouse_id', $branch_office_warehouse_id)
                    ->first();

                if (!$product_inventory) {
                    return 0;
                } else if ($document_product['backorder_quantity'] > $product_inventory->stock) {
                    return (int)$product_inventory->stock;
                } else {
                    return (int)$document_product['backorder_quantity'];
                }
            } else {
                return (int)$document_product['backorder_quantity'];
            }
        }

        return (int)$document_product['quantity'];
    }

    /**
     * Crear los document_product_taxes de un document_product
     *
     * @param array $base_document_product Contiene la información document_product base
     * @param DocumentProduct $document_product Contiene la información del document_product
     * @param string $operation_type_code Contiene el código del tipo de operación del documento
     * @param Template $model Contiene la información del modelo
     * @return array Contiene el valor total de los impuestos asociados al document_product
     * @author Jhon García
     */
    public function createDocumentProductTaxesFromBase($base_document_product, $document_product, $operation_type_code, $model): array
    {
        $document_product_taxes = [];

        if ($model->pay_taxes) {

            // Obtener los impuestos de la linea
            $taxes = $base_document_product['product']['line']['taxes'];

            if (count($taxes)) {

                $iva_value = 0;

                // Validar si el precio del producto tiene el IVA incluido de acuerdo a la lista de precios
                $iva_included = $model->priceList && $model->priceList->paramtable->code_table === 'LDP' && $model->priceList->alphanum_data_second === 'SI';

                // Establecer el valor bruto
                if ($document_product->is_benefit) {
                    $total_value_brut = $document_product->unit_value_before_taxes * $document_product->quantity;
                } else {
                    $total_value_brut = $document_product->total_value_brut;
                }

                // Recorrer los impuestos
                foreach ($taxes as $tax) {

                    $tax_percentage = 0;
                    $value_tax = 0;

                    // Establecer el porcentaje del impuesto dependiendo del tipo de operación
                    if (in_array($operation_type_code, ['206', '205', '155', '010', '260', '156'])) { // Validar si el tipo de operación es Ventas
                        $tax_percentage = (float)$tax['percentage_sale'];
                    } else if (in_array($operation_type_code, ['105', '020', '270', '015', '271'])) { // Validar si el tipo de operación es Compras
                        $tax_percentage = (float)$tax['percentage_purchase'];
                    }

                    // Validar si el id del impuesto es 1 (IVA) y si el precio no tiene incluido el iva
                    if (($tax['id'] == 1 && !$iva_included)) {
                        $value_tax = round(($total_value_brut * $tax_percentage) / 100, 2);

                        // Validar si el atributo total_value_iva es diferente al valor calculado se asigna el primer valor
                        if (!is_null($document_product->total_value_iva) && $document_product->total_value_iva != $value_tax) {
                            $value_tax = $document_product->total_value_iva;
                        }

                        $iva_value = $value_tax;

                        // Validar si el impuesto no es 1 (IVA) y la base es el valor bruto
                    } else if ($tax['id'] != 1 && $tax['base'] == 1) {
                        $value_tax = floor(($total_value_brut * $tax_percentage) / 100);

                        // Validar si la base del impuesto es 2 (IVA) y el valor del IVA > 0
                    } else if ($tax['base'] == 2 && $iva_value > 0) {
                        $value_tax = floor(($iva_value * $tax_percentage) / 100);
                    }

                    // Validar si el valor del impuesto es mayor a 0, se crea el impuesto
                    if ($value_tax > 0) {

                        $document_product_tax = [
                            'id'             => $tax['id'],
                            'tax_id'         => $tax['id'],
                            'description'    => $tax['description'],
                            'code'           => $tax['code'],
                            'base'           => $tax['base'],
                            'base_value'     => $total_value_brut,
                            'minimum_base'   => 0,
                            'tax_percentage' => $tax_percentage,
                            'value'          => $value_tax
                        ];

                        array_push($document_product_taxes, $document_product_tax);
                    }
                }

                $document_product->documentProductTaxes()->sync($document_product_taxes);
            }
        }

        return $document_product_taxes;
    }

    /**
     * Calcular el total de impuestos por documento
     *
     * @param array $documents_products_taxes Contiene los documents_product_taxes del documento
     * @return array Contiene el total de impuestos por documento
     * @Jhon García
     */
    public function calculateTotalTaxesDocument(array $documents_products_taxes): array
    {
        $taxes = [];

        // Calcular los impuestos del documento
        foreach ($documents_products_taxes as $documents_products_tax) {

            $added = false;

            if (count($taxes)) {
                foreach ($taxes as $key => $tax) {
                    if ($documents_products_tax['code'] == $tax['code']) {
                        $taxes[$key]['value'] += $documents_products_tax['value'];
                        $added = true;
                        break;
                    }
                }
                if (!$added) {
                    array_push($taxes, $documents_products_tax);
                }
            } else {
                $documents_products_tax['percentage'] = $documents_products_tax['tax_percentage'];
                array_push($taxes, $documents_products_tax);
            }
        }

        return $taxes;
    }

    /**
     * Calcular las retenciones de un documento
     *
     * @param Document $document
     * @param Template $model
     * @return array
     * @author Jhon García
     */
    public function calculateRetentionsForDocument($document, $model)
    {
        // Validar si el documento es un devolucion.
        $is_devolution = false;

        // Códigos de tipos de operaciones que son devoluciones.
        $devolution_codes = ['155', '270', '225'];

        // Validamos si el modelo maneja retenciones. si no maneja se pone en 0 todos los valores.
        if ($model->retention_management === 0 || is_null($model->retention_management) || ($model->priceList && $model->priceList->code_parameter == '002')) {
            $document->update([
                // Retenciones en la fuente productos
                'ret_fue_prod_vr_limit'   => 0,
                'ret_fue_prod_percentage' => 0,
                'ret_fue_prod_total'      => 0,
                'ret_fue_base_value'      => 0,
                // Retenciones en la fuente servicios
                'ret_fue_serv_vr_limit'   => 0,
                'ret_fue_serv_percentage' => 0,
                'ret_fue_serv_total'      => 0,
                'ret_fue_serv_base_value' => 0,
                // Total retención en la fuente
                'ret_fue_total'           => 0,
                // ReteIVA en productos
                'ret_iva_prod_vr_limit'   => 0,
                'ret_iva_prod_percentage' => 0,
                'ret_iva_prod_total'      => 0,
                'ret_iva_base_value'      => 0,
                // ReteIVA en servicios
                'ret_iva_serv_vr_limit'   => 0,
                'ret_iva_serv_percentage' => 0,
                'ret_iva_serv_total'      => 0,
                'ret_iva_serv_base_value' => 0,
                // Total ReteIVA
                'ret_iva_total'           => 0,
                // ReteICA en productos
                'ret_ica_prod_vr_limit'   => 0,
                'ret_ica_prod_percentage' => 0,
                'ret_ica_prod_total'      => 0,
                'ret_ica_base_value'      => 0,
                // ReteICA en servicios
                'ret_ica_serv_vr_limit'   => 0,
                'ret_ica_serv_percentage' => 0,
                'ret_ica_serv_total'      => 0,
                'ret_ica_serv_base_value' => 0,
                // Total ReteICA
                'ret_ica_total'           => 0,
            ]);
        }

        $total_value_brut_services = 0;
        $total_iva_services = 0;
        $total_value_brut_products = 0;
        $total_iva_products = 0;

        $documents_products = DocumentProduct::select('id', 'document_id', 'line_id', 'total_value_brut')
            ->where('document_id', $document->id)
            ->with([
                'line:id,tipe',
                'documentProductTaxes:id,document_product_id,tax_id,value'
            ])
            ->get();

        // Actualizamos las bases de las retenciones.
        // Recorremos los productos del documento.
        foreach ($documents_products as $key => $document_product) {
            # Validamos el tipo del producto.

            // Servicio.
            if ($document_product->line->tipe == 2) {
                $total_value_brut_services += $document_product->total_value_brut;

                # Sumamos el Iva del servicio
                $total_iva_services += $document_product->documentProductTaxes->where('tax_id', 1)->sum('value');
            } // Producto.
            else {
                $total_value_brut_products += $document_product->total_value_brut;

                # Sumamos el Iva del producto.
                $total_iva_products += $document_product->documentProductTaxes->where('tax_id', 1)->sum('value');
            }
        }

        $document->ret_fue_base_value = $total_value_brut_products;
        $document->ret_ica_base_value = $total_value_brut_products;
        $document->ret_iva_base_value = $total_iva_products;

        $document->ret_fue_serv_base_value = $total_value_brut_services;
        $document->ret_ica_serv_base_value = $total_value_brut_services;
        $document->ret_iva_serv_base_value = $total_iva_services;

        // Traemos la sucursal.
        $contact_warehouse = $document->warehouse;

        # Validamos si el documento es una devolucion.
        $is_devolution = in_array($model->operationType->code, $devolution_codes);

        # Actualizamos vr limite y porcentajes.
        switch ($model->contact_type) {
            case 1:
                $document = $this->setConfigRetentionToDocument($document, $is_devolution, $contact_warehouse, $model);
                break;
            case 2:
                $document = $this->setConfigRetentionToDocumentProvider($document, $is_devolution, $contact_warehouse, $model);
                break;
        }

        $this->calculateValuesRetentionForDocument($document, $is_devolution);

        // Actualizamos
        $document->save();
    }

    /**
     * Este metodo se encarga de
     * validar los valores de las retenciones
     * de forma parcial o no.
     *
     * @author Kevin Galindo
     */
    public function calculateValuesRetentionForDocument(Document $document, $isDevolution)
    {
        // Si el documento es una devolucion se calcula de forma parcial.
        if ($isDevolution) {
            # FUE
            if ((float)$document->document->ret_fue_prod_total > 0) {
                $document->ret_fue_prod_total = round(((float)$document->ret_fue_base_value * (float)$document->ret_fue_prod_percentage) / 100);
            } else {
                $document->ret_fue_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_fue_serv_total > 0) {
                $document->ret_fue_serv_total = round(((float)$document->ret_fue_serv_base_value * (float)$document->ret_fue_serv_percentage) / 100);
            } else {
                $document->ret_fue_serv_total = 0;
            }

            // totales
            $document->ret_fue_total = $document->ret_fue_prod_total + $document->ret_fue_serv_total;

            # ICA
            // productos
            if ((float)$document->document->ret_ica_prod_total > 0) {
                $document->ret_ica_prod_total = round(((float)$document->ret_ica_base_value * (float)$document->ret_ica_prod_percentage) / 100);
            } else {
                $document->ret_ica_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_ica_serv_total > 0) {
                $document->ret_ica_serv_total = round(((float)$document->ret_ica_serv_base_value * (float)$document->ret_ica_serv_percentage) / 100);
            } else {
                $document->ret_ica_serv_total = 0;
            }

            // totales
            $document->ret_ica_total = $document->ret_ica_prod_total + $document->ret_ica_serv_total;

            # IVA
            // productos
            if ((float)$document->document->ret_iva_prod_total > 0) {
                $document->ret_iva_prod_total = round(((float)$document->ret_iva_base_value * (float)$document->ret_iva_prod_percentage) / 100);
            } else {
                $document->ret_iva_prod_total = 0;
            }

            // servicios
            if ((float)$document->document->ret_iva_serv_total > 0) {
                $document->ret_iva_serv_total = round(((float)$document->ret_iva_serv_base_value * (float)$document->ret_iva_serv_percentage) / 100);
            } else {
                $document->ret_iva_serv_total = 0;
            }

            // totales
            $document->ret_iva_total = $document->ret_iva_prod_total + $document->ret_iva_serv_total;
        } // Se calcula de forma NO parcial.
        else {
            # FUE
            // productos
            if ((float)$document->ret_fue_base_value > 0 && (float)$document->ret_fue_base_value >= (float)$document->ret_fue_prod_vr_limit) {
                $document->ret_fue_prod_total = round(((float)$document->ret_fue_base_value * (float)$document->ret_fue_prod_percentage) / 100);
            } else {
                $document->ret_fue_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_fue_serv_base_value > 0 && (float)$document->ret_fue_serv_base_value >= (float)$document->ret_fue_serv_vr_limit) {
                $document->ret_fue_serv_total = round(((float)$document->ret_fue_serv_base_value * (float)$document->ret_fue_serv_percentage) / 100);
            } else {
                $document->ret_fue_serv_total = 0;
            }

            // totales
            $document->ret_fue_total = $document->ret_fue_prod_total + $document->ret_fue_serv_total;

            # ICA
            // productos
            if ((float)$document->ret_ica_base_value > 0 && (float)$document->ret_ica_base_value >= (float)$document->ret_ica_prod_vr_limit) {
                $document->ret_ica_prod_total = round(((float)$document->ret_ica_base_value * (float)$document->ret_ica_prod_percentage) / 100);
            } else {
                $document->ret_ica_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_ica_serv_base_value > 0 && (float)$document->ret_ica_serv_base_value >= (float)$document->ret_ica_serv_vr_limit) {
                $document->ret_ica_serv_total = round(((float)$document->ret_ica_serv_base_value * (float)$document->ret_ica_serv_percentage) / 100);
            } else {
                $document->ret_ica_serv_total = 0;
            }

            // totales
            $document->ret_ica_total = $document->ret_ica_prod_total + $document->ret_ica_serv_total;

            # IVA

            // productos
            if ((float)$document->ret_iva_base_value > 0 && (float)$document->ret_iva_base_value >= (float)$document->ret_iva_prod_vr_limit) {
                $document->ret_iva_prod_total = round(((float)$document->ret_iva_base_value * (float)$document->ret_iva_prod_percentage) / 100);
            } else {
                $document->ret_iva_prod_total = 0;
            }

            // servicios
            if ((float)$document->ret_iva_serv_base_value > 0 && (float)$document->ret_iva_serv_base_value >= (float)$document->ret_iva_serv_vr_limit) {
                $document->ret_iva_serv_total = round(((float)$document->ret_iva_serv_base_value * (float)$document->ret_iva_serv_percentage) / 100);
            } else {
                $document->ret_iva_serv_total = 0;
            }

            // totales
            $document->ret_iva_total = $document->ret_iva_prod_total + $document->ret_iva_serv_total;
        }
    }

    //--------------------------- FIN REESTRUCTURACIÓN DE LA CREACIÓN DE UN DOCUMENTO DESDE UN DOCUMENTO BASE ----------------------------- \\
    public function createDocumentSoapWms()
    {
        $documents = $this->documentRepository->getDocumentsWms();
        $countDocuments = $documents->count();
        $respuestas = array();

        for ($s = 0; $s < $countDocuments; $s++) {

            //Verificar si es Agregar o Modificar
            if (!isset($documents[$s]['date_update_wsm'])) {
                $action = 'A';
            } else {
                $action = 'M';
            }

            if ($documents[$s]['select_send_wms'] == 1) {
                $type_document = 'ENTRADA';///Aviso de llegada cambiar ciudad a la de la sede
                $departamento = $documents[$s]['department_id_branchoffice'];
                $nombre_departamento = $documents[$s]['department_branchoffice'];
                $ciudad = $documents[$s]['city_code_branchoffice'];
                $nombre_ciudad = $documents[$s]['city_branchoffice'];
            } else {
                $type_document = 'SALIDA';
                $departamento = $documents[$s]['department_id_sucursal_contact'];
                $nombre_departamento = $documents[$s]['department_contact'];
                $ciudad = $documents[$s]['city_code_contact'];
                $nombre_ciudad = $documents[$s]['city_contact'];
            }

            $type_operation = $documents[$s]['type_operation'] == '205' ? 'FV' : $documents[$s]['prefix'];

            $type_warehouse = $documents[$s]['warehouse_code'];

            $fecha_document = $documents[$s]['document_date'];

            $array_documents_entries = array(
                'punto'                       => 'CEDI',
                'tipo_documento'              => $type_operation,
                'documento'                   => $type_operation."-".$documents[$s]['consecutive'],
                'fecha_documento'             => $fecha_document,
                'proveedor'                   => $documents[$s]['identification'],
                'contacto'                    => str_replace("&", "&amp;", $documents[$s]['complete_name']),
                'moneda'                      => 'COP',
                'valor_tasa_cambio'           => 0,
                'porcentaje_descuento_global' => 0,
                'observacion'                 => str_replace("&", '&amp;', $documents[$s]['observation']),
                'clasificador1'               => '',
                'punto_destino'               => '',
                'bodega_destino'              => '',
                'campo1'                      => $documents[$s]['id'],
                'campo2'                      => $type_document,
                'campo3'                      => $type_warehouse,
                'campo4'                      => '',
                'campo5'                      => '',
                'multiple_recepcion'          => 'S'
            );

            $documentsproducts = $this->documentRepository->getDocumentsProductsWms($documents[$s]['id']);
            $countProducts = $documentsproducts->count();
            $arreglo_producst_xml = "";

            if ($countProducts > 0) {
                for ($d = 0; $d < $countProducts; $d++) {

                    $code = isset($documentsproducts[$d]['code']) ? $documentsproducts[$d]['code'] : "";
                    $quantity = isset($documentsproducts[$d]['quantity']) ? $documentsproducts[$d]['quantity'] : "";
                    $value_unit = isset($documentsproducts[$d]['unit_value_before_taxes']) ? $documentsproducts[$d]['unit_value_before_taxes'] : "";
                    $date_entrie = isset($documentsproducts[$d]['document_date']) ? $documentsproducts[$d]['document_date'] : "";
                    $id_document_product = isset($documentsproducts[$d]['id']) ? $documentsproducts[$d]['id'] : "";

                    $array_articles_entries[$d] = array(
                        'articulo'                   => $code,
                        'cantidad_esperada'          => $quantity,
                        'valor_comercial'            => $value_unit,
                        'porcentaje_descuento_linea' => 0,
                        'valor_porcentaje_iva'       => 0,
                        'es_bonificado'              => 'N',
                        'fecha_entrada'              => $date_entrie,
                        'serie'                      => 'COP',
                        'lote'                       => '',
                        'documento1'                 => '',
                        'documento2'                 => '',
                        'estado_articulo'            => '',
                        'fecha_vencimiento'          => '',
                        'fecha_fabricacion'          => '',
                        'campo1'                     => $id_document_product,
                        'campo2'                     => '',
                        'campo3'                     => '',
                        'campo4'                     => '',
                        'campo5'                     => '',
                    );

                    $array_articles_out[$d] = array(
                        'secuencia'       => $id_document_product,
                        'articulo'        => $code,
                        'serie'           => '',
                        'lote'            => '',
                        'documento1'      => '',
                        'documento2'      => '',
                        'estado_articulo' => '',
                        'sscc'            => '',
                        'sscc_completo'   => 'N',
                        'cantidad'        => $quantity,
                        'valor_base'      => 1,
                        'valor_iva'       => 1,
                        'presentacion'    => '',
                        'descripcion'     => ''
                    );

                }

            } else {

                $array_articles_entries[0] = array(
                    'articulo'                   => "",
                    'cantidad_esperada'          => "",
                    'valor_comercial'            => "",
                    'porcentaje_descuento_linea' => 0,
                    'valor_porcentaje_iva'       => 0,
                    'es_bonificado'              => 'N',
                    'fecha_entrada'              => "",
                    'serie'                      => 'COP',
                    'lote'                       => '',
                    'documento1'                 => '',
                    'documento2'                 => '',
                    'estado_articulo'            => '',
                    'fecha_vencimiento'          => '',
                    'fecha_fabricacion'          => '',
                    'campo1'                     => '',
                    'campo2'                     => '',
                    'campo3'                     => '',
                    'campo4'                     => '',
                    'campo5'                     => '',
                );

                $array_articles_out[0] = array(
                    'secuencia'       => '',
                    'articulo'        => '',
                    'serie'           => '',
                    'lote'            => '',
                    'documento1'      => '',
                    'documento2'      => '',
                    'estado_articulo' => '',
                    'sscc' => '',
                    'sscc_completo' => 'N',
                    'cantidad' => '',
                    'valor_base' => '',
                    'valor_iva' => '',
                    'presentacion' => '',
                    'descripcion' => ''
                );
            }

            $array_details_entries = array("arreglo_detalles" => $array_articles_entries);

            $array_documents_out = array('arreglo_salidas' => array(
                'punto'                     => 'CEDI',
                'salida'                    => $type_operation."-".$documents[$s]['consecutive'],
                'identificacion'            => $documents[$s]['identification'],
                'nombre'                    => str_replace("&", '&amp;', $documents[$s]['complete_name']),
                'apellido'                 => '',
                'pais'                      => 169,
                'nombre_pais'               => 'Colombia',
                'departamento'              => $departamento,
                'nombre_departamento'       => $nombre_departamento,
                'ciudad'                    => $ciudad,
                'nombre_ciudad'             => $nombre_ciudad,
                'zona'                      => 'SIN',
                'telefono1'                 => $documents[$s]['telephone_contact'],
                'telefono2'                 => '',
                'email'                     => $documents[$s]['email_contact'],
                'contacto'                  => '',
                'tipo_salida'               => 'C',
                'direccion'                 => $documents[$s]['address_contact'],
                'fecha'                     => $fecha_document,
                'tipo_cliente'              => '',
                'clasificador1'             => '',
                'clasificador2'             => '',
                'punto_destino'             => '',
                'bodega_destino'            => '',
                'bodega_alistamiento'       => '',
                'sector_alistamiento'       => '',
                'area_alistamiento'         => '',
                'tipo_documento_referencia' => $type_operation,
                'documento_referencia'      => $documents[$s]['consecutive'],
                'prioridad'                 => 1,
                'fecha_envio'               => Carbon::parse(now())->format('Y-m-d'),
                'hora_envio'                => Carbon::parse(now())->format('H:m'),
                'observacion'               => '',
                'dato_adicional'            => '',
                'proveedor'                 => '',
                'campo1'                    => $documents[$s]['id'],
                'campo2'                    => $type_document,
                'campo3'                    => $type_warehouse,
                'campo4'                    => '',
                'campo5'                    => '',
                'operacion'                 => '',
                'divide_por'                => '',
                'maneja_sobreporte'         => 'M',
                'valor_declarado'           => 1,
                'valor_a_recaudar'          => 1,
                'carga_componentes'         => 0,
                "detalles"                  => $array_articles_out
            ));


            //$array_details_out = array("detalles"=>$array_articles_out);

            if ($documents[$s]['select_send_wms'] == 1) {
                $wsdl = env('SOAP_URLWMS') . 'documentos.php?wsdl';
                $wsdl_loc = env('SOAP_URLWMS') . 'documentos.php';

                $array_documents = array_merge($array_documents_entries, $array_details_entries);
            } else {
                $wsdl = env('SOAP_URLWMS') . 'salidas.php?wsdl';
                $wsdl_loc = env('SOAP_URLWMS') . 'salidas.php';

                $array_documents = array_merge($array_documents_out);
            }

            //dd($array_documents);


            //Log::channel('documentslog')->info("Cosumed Document Problem 2".$xml_document.$arreglo_producst_xml."</document>");


            //dd($xmlr);
            $context = stream_context_create([
                'ssl' => [
                    // set some SSL/TLS specific options
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true
                ]
            ]);
            //
            //dd($data);
            // Este es el webservice que vamos a consumir
            //dd(env('SOAP_PASSWORDWMS'));
            //$wsdl = env('SOAP_URLWMS').'documentos.php?wsdl';
            //$wsdl_loc = env('SOAP_URLWMS').'documentos.php';

            $cliente = new SoapClient($wsdl, [
                'location'       => $wsdl_loc,
                'encoding'       => 'UTF-8',
                'trace'          => 1,
                'exceptions'     => 1,
                'soap_version'   => 'SOAP_1_2',
                'keep_alive'     => false,
                'stream_context' => stream_context_create([
                    'ssl' => [
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true
                    ]
                ]),
                'login'          => env('SOAP_USERWMS'),
                'password'       => env('SOAP_PASSWORDWMS'),
            ]);

            // Consumimos el servicio llamando al método que necesitamos, en este caso
            //$resultado = $cliente->validarArticulos($arreglo_articulos);
            //var_dump($xmlr->fecha_documento);
            //dd($xmlr);
            //dd($array_documents);

            if ($documents[$s]['select_send_wms'] == 1) {
                $resultado = $cliente->CrearDocumentos($array_documents);
            } else {
                $resultado = $cliente->validarSalidas($array_documents);
            }
            //dd($documents);
            //dd(isset($resultado->estado)?$resultado->estado:$resultado->arreglo_respuestas->estado);
            //dd($resultado->arreglo_respuestas->mensaje);
            $state = isset($resultado->estado) ? $resultado->estado : $resultado->arreglo_respuestas->estado;
            $message = isset($resultado->mensaje) ? $resultado->mensaje : $resultado->arreglo_respuestas->mensaje;
            //dd(isset($resultado->mensaje));
            if ($state == 'NOOK' || $state == 'NO OK') {
                $product = $this->documentRepository->getUpdateDocumentsWmsError($documents[$s]['id'],$message);
                $respuestasnok = array(
                    'id'      => $documents[$s]['id'],
                    'message' => "Soup request Incorrect! " . $message,
                    'error'   => isset($resultado->estado) ? $resultado->estado : $resultado->arreglo_respuestas->estado,
                );
                array_push($respuestas, $respuestasnok);
            } else {
                $product = $this->documentRepository->getUpdateDocumentsWms($documents[$s]['id'], $action, isset($resultado->estado) ? $resultado->estado : $resultado->arreglo_respuestas->estado);

                $respuestasok = array(
                    'id'      => $documents[$s]['id'],
                    'message' => "Soup request Correct! " . $documents[$s]['consecutive'],
                    'error'   => isset($resultado->estado) ? $resultado->estado : $resultado->arreglo_respuestas->estado,
                );
                array_push($respuestas, $respuestasok);

            }

            $state = isset($resultado->estado) ? $resultado->estado : $resultado->arreglo_respuestas->estado;
            $message = isset($resultado->mensaje) ? $resultado->mensaje : $resultado->arreglo_respuestas->mensaje;
            //Log::channel('documentslog')->info("Cosumed Document ");
            //Log::channel('documentslog')->info("Respuest Web Service " . $state . " Message " . $message);

        }


        //Log::channel('documentslog')->info("Respuest Web Service Not information to load");
        // Respuesta si no hay informacion @Hom669
        return [
            'data' => $respuestas ? $respuestas : "Not information to load",
            'code' => 200,
        ];

    }

    public function createDocumentsTransfersWMS($request)
    {

        $v = \Validator::make($request->all(), [
            'id_user'                                  => 'required|integer',
            'branchoffice_warehouse_code_source'       => 'required|string',
            'branchoffice_warehouse_code_destination'  => 'required|string',
            'id_contact'                               => 'required|string',
            'product_array.*.cod_prod'                 => 'required|string',
            'product_array.*.quantity_total'           => 'required|integer|min:1',
            'product_array.*.lot_array.*.number_lot'   => 'string',
            'product_array.*.lot_array.*.quantity_lot' => 'integer|min:1',
        ], ['product_array.*.lot_array.*.quantity_lot.*' => 'Quantity Lot, It must be greater than 0, Integer And required :attribute',
            'product_array.*.lot_array.*.number_lot.*'   => 'Number Lot, It must be String And required :attribute',
            'product_array.*.quantity_total.*'           => 'Quantity Total, It must be greater than 0, Integer And required :attribute',
            'product_array.*.cod_prod.*'                 => 'Code Product, It must be String And required :attribute',
            'id_user.*'                                  => 'User Id, It must be Integer And required :attribute',
            'id_contact.*'                               => 'Contact Id, It must be String And required :attribute',
            'branchoffice_warehouse_code_source.*'       => 'Branch Office WareHouse Source, It must be String And required :attribute',
            'branchoffice_warehouse_code_destination.*'  => 'Branch Office WareHouse Destination, It must be String And required :attribute',
        ]);

        if ($v->fails()) {
            $errors = $v->errors();
            $resultExist = array(
                "data"  => $errors->all(),
                "state" => "error",
                "msg"   => "Errors Validate Information"
            );
            return $resultExist;
        } else {

            $createDocumentsExit = $this->documentRepository->createDocumentsWms($request);

            if (isset($createDocumentsExit['state'])) {
                //dd("AQUIII11");
                return $createDocumentsExit;
            } else {


                $firstKeyExit = array_key_first($createDocumentsExit['products_exit']);
                $firstKeyEntry = array_key_first($createDocumentsExit['products_entry']);
                $conProductsExit = array_key_last($createDocumentsExit['products_exit']);
                $conProductsEntry = array_key_last($createDocumentsExit['products_entry']);
                //dd($createDocumentsExit);

                for ($m = $firstKeyExit; $m <= $conProductsExit; $m++) {
                    //dd($createDocumentsExit['products_exit'][$m]);
                    $invExit = $this->inventoryRepository->finalize($createDocumentsExit['products_exit'][$m]['id_product'], false);
                }
                // dd($createDocumentsExit,$firstKeyExit, $conProductsExit);

                for ($m = $firstKeyEntry; $m <= $conProductsEntry; $m++) {
                    $invEntry = $this->inventoryRepository->finalize($createDocumentsExit['products_entry'][$m]['id_product'], false);
                }

                if (($invExit == null && $invEntry == null) || (isset($invExit) && isset($invEntry))) {
                    return $createDocumentsExit;
                }
            }
        }
        //$createDocumentsExit = $this->documentRepository->createDocumentsWms($id_branchOffice_source,$id_branchOfficeWarehouse_source_source,$request->product_array,"ENTRADA POR TRASLADOS",$request->id_contact);
        //dd($invExit,$invEntry);


    }

    public function createDocumentsMovementsWMS($request)
    {

        $v = \Validator::make($request->all(), [
            'type_document'                            => 'required|string',
            'id_user'                                  => 'required|integer',
            'branchoffice_warehouse_code'              => 'required|string',
            'id_contact'                               => 'required|string',
            'product_array.*.cod_prod'                 => 'required|string',
            'product_array.*.quantity_total'           => 'required|integer|min:1',
            'product_array.*.lot_array.*.number_lot'   => 'string',
            'product_array.*.lot_array.*.quantity_lot' => 'integer|min:1',
        ], ['product_array.*.lot_array.*.quantity_lot.*' => 'Quantity Lot, It must be greater than 0, Integer And required :attribute',
            'product_array.*.lot_array.*.number_lot.*'   => 'Number Lot, It must be String And required :attribute',
            'product_array.*.quantity_total.*'           => 'Quantity Total, It must be greater than 0, Integer And required :attribute',
            'product_array.*.cod_prod.*'                 => 'Code Product, It must be String And required :attribute',
            'type_document.*'                            => 'Type Document, It must be String And required :attribute',
            'id_user.*'                                  => 'User Id, It must be Integer And required :attribute',
            'id_contact.*'                               => 'Contact Id, It must be String And required :attribute',
            'branchoffice_warehouse_code.*'              => 'Branch Office WareHouse Source, It must be String And required :attribute',
        ]);

        if ($v->fails()) {
            $errors = $v->errors();
            $resultExist = array(
                "data"  => $errors->all(),
                "state" => "error",
                "msg"   => "Errors Validate Information"
            );
            return $resultExist;
        } else {

            $createDocumentsMovement = $this->documentRepository->createDocumentsMovementsWMS($request);
            //dd($createDocumentsMovement);
            if (isset($createDocumentsMovement['state'])) {
                //dd("AQUIII11");
                return $createDocumentsMovement;
            } else {

                //dd($createDocumentsMovement['state']);

                $firstKey = array_key_first($createDocumentsMovement['products_document']);
                $conProducts = array_key_last($createDocumentsMovement['products_document']);


                for ($m = $firstKey; $m <= $conProducts; $m++) {
                    $invDocument = $this->inventoryRepository->finalize($createDocumentsMovement['products_document'][$m]['id_product'], false);
                }


                if ($invDocument == null && $invDocument == null) {
                    return $createDocumentsMovement;
                }
            }
        }
        //$createDocumentsExit = $this->documentRepository->createDocumentsWms($id_branchOffice_source,$id_branchOfficeWarehouse_source_source,$request->product_array,"ENTRADA POR TRASLADOS",$request->id_contact);
        //dd($invExit,$invEntry);


    }

    /**
     * Este metodo se encarga de finalizar un documento de cruce
     *
     * @author Kevin Galindo
     */
    public function finalizeDocumentCrossBalance($request)
    {

        $voucherType_id = $request->voucherType_id;
        $document_id = $request->document_id;
        $model_id = $request->model_id;

        $model = Template::select(
            'id',
            'code',
            'branchoffice_id',
            'branchoffice_warehouse_id',
            'credit_note',
            'operationType_id',
            'purchase_difference_management',
            'consecutive_handling',
            'handling_other_parameters_state',
            'doc_cruce_management',
            'minimal_price'
        )
        ->with([
            'branchOfficeWarehouse',
            'operationType',
            'authorizedUsers',
            'minimalPriceParam'
        ])
        ->find($model_id);

        $document = Document::select(
            'id',
            'in_progress',
            'document_date',
            'contact_id',
            'due_date',
            'user_id_solicit_authorization',
            'date_solicit_authorization',
            'pending_authorization',
            'vouchertype_id',
            'branchoffice_id',
            'consecutive',
            'invoice_link',
            'operationType_id',
            'observation',
            'prefix',
            'model_id',
            'import_number',
            'seller_id',
            'handling_other_parameters_value',
            'crossing_prefix',
            'crossing_number_document',
            'crossing_date_document'
        )
        ->with([
            'documentsProducts',
            'contact',
            'vouchertype',
            'documentsInformation',
            'paymentMethods'
        ])
        ->find($document_id);

        try {
            DB::beginTransaction();

            $document->in_progress = false;

            $document = $this->documentSetDueDate($document);

            // Validamos el manejo del consecutivo segun el modelo
            if ($model) {
                if ($model->consecutive_handling === 1) {
                    $vouchersType = VouchersType::where('code_voucher_type', $document->vouchertype->code_voucher_type)
                    ->where('branchoffice_id', $document->branchoffice_id)
                    ->lockForUpdate()
                    ->first();
                } else {
                    $vouchersType = VouchersType::where('code_voucher_type', $document->vouchertype->code_voucher_type)
                    ->whereHas('branchoffice', function($query) {
                        $query->where('main', true);
                    })
                    ->lockForUpdate()
                    ->first();
                }
            } else {
                $vouchersType = VouchersType::where('id', $voucherType_id)->lockForUpdate()->first();
            }



            $document = $this->documentSetConsecutive($document, $vouchersType, null, false);
            $document = $this->documentSetAdditionalInformation($document, $vouchersType, $model, $request);
            $document = $this->documentRepository->finalizeDocument($document);

            // $file_name = "/storage/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document, true);

            // Document::find($document->id)->update([
            //     'invoice_link' => $file_name
            // ]);

            // $document->filename = $file_name;
            DB::commit();

            $return_document =  $this->documentRepository->getDocument($document->id, $request);
            // $return_document->filename = $file_name;

            $dataReturn = [
                'code' => '200',
                'completed' => false,
                'data' => $return_document
            ];

            return (object) $dataReturn;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());

            return (object) [
                'code' => '500',
                'completed' => false,
                'data' => (object) [
                    'message' => 'Error en la finalización del documento: ' . $e->getMessage() /*."\n".$e->getLine()."\n".$e->getFile()*/
                ]
            ];
        }
    }

    /**
     *
     * @author Kevin Galindo
     */
    public function createDocumentBalanceCrossing($request)
    {
        $document = $this->documentRepository->getDocumentBalanceCrossing($request);

        dd($document->id);
    }
}
