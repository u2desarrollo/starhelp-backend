<?php

namespace App\Services;

use App\Entities\BalanceForAccount;
use App\Repositories\BalanceForAccountRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class BalanceForAccountService
{
    private $balanceForAccountRepository;

    public function __construct(BalanceForAccountRepository $balanceForAccountRepository)
    {
        $this->balanceForAccountRepository = $balanceForAccountRepository;

    }

    public function balanceForAccount($request)
    {
        $balances_for_account = $this->balanceForAccountRepository->balanceForAccount($request);
        //dd($balances_for_account);
        return [
            'data' => $balances_for_account
        ];
    }

    public function stateFinanze($request)
    {
        $documents_transactions = $this->balanceForAccountRepository->stateFinanze($request);
        return [
            'data' => $documents_transactions
        ];
    }
}
