<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class ContactWarehouseService
{
    public function saveConfigurationRetention($request)
    {

        $data = $request->all();

        // Variables
        $warehouse_id = $request->warehouse_id;

        // Traemos y actualizamos la sucursal
        $contactWarehouse = ContactWarehouse::find($warehouse_id);

        if ($contactWarehouse) {
            // Retornamos la sucursales actualizadas
            $contactWarehouse->update(Arr::except($data, ['warehouse_id', 'token']));
            return [
                'data' => [
                    'message' => 'registro actualizado',
                    'data' => $contactWarehouse
                ], 
                'code' => 200
            ];

        } else {
            return ['data' => ['message' => 'registro no encontrado para actualizar'], 'code' => 404];
        }

    
    }
}
