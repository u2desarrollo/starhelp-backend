<?php

namespace App\Services;

use App\Entities\Contact;
use App\Entities\Template;
use App\Entities\ContactCustomer;
use App\Entities\ContactWarehouse;
use App\Repositories\TemplateAccountingRepository;
use App\Repositories\ParameterRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Arr;

// use App\Repositories
use Validator;

class TemplateAccountingService
{
    private $templateAccountingRepository;
    private $parameterRepository;

    public function __construct(TemplateAccountingRepository $templateAccountingRepository ,ParameterRepository $parameterRepository)
    {
        $this->templateAccountingRepository = $templateAccountingRepository;
        $this->parameterRepository = $parameterRepository;;
    }

    public function store($request)
    {
        // Valores
        $model_id = $request->model_id;
        $templateAccountings = collect($request->templateAccountings);

        // Variables
        $model = Template::find($model_id);
        $templateAccountingsActual = $model->templateAccountings;

        // Eliminamos los registros
        foreach ($templateAccountingsActual as $key => $taa) {

            $notInArray = $templateAccountings
            ->where('concept_id', $taa->concept_id)
            ->where('according_id', $taa->according_id)
            ->where('account_id', $taa->account_id)
            ->where('departure_square', $taa->departure_square)
            ->where('debit_credit', $taa->debit_credit)
            ->where('order_accounting', $taa->order_accounting)
            ->first();

            if (!$notInArray) {
                $taa->delete();
            }
        }

        // Agregamosa los nuevos registros
        foreach ($templateAccountings as $key => $ta) {
            
            // Validamos si ya existe
            $exists = $templateAccountingsActual
            ->where('concept_id', $ta['concept_id'])
            ->where('according_id', $ta['according_id'])
            ->where('account_id', $ta['account_id'])
            ->where('departure_square', $ta['departure_square'])
            ->where('debit_credit', $ta['debit_credit'])
            ->where('order_accounting', $ta['order_accounting'])
            ->first();

            if ( $exists ) continue;

            $this->templateAccountingRepository->create([
                'model_id' => $model_id,
                'concept_id' => $ta['concept_id'],
                'according_id' => $ta['according_id'],
                'account_id' => $ta['account_id'],
                'departure_square' => $ta['departure_square'],
                'debit_credit' => $ta['debit_credit'],
                'order_accounting' => $ta['order_accounting'],
            ]);

        }

        // Resivimos la info.
        return $model->templateAccountings;
    }
}
