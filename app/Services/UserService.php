<?php


namespace App\Services;


use App\Repositories\UserRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class UserService
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function generateReportVisits($request)
    {
        $sellers = $this->userRepository->sellersStatistics($request);

        if (count($sellers) > 0) {

            $spreadSheet = new Spreadsheet();
            $sheet = $spreadSheet->getActiveSheet();
            $row = 2;

            $style = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN
                    ]
                ]
            ];

            $headers = [
                "Vendedor",
                "Identificacion Cliente",
                "Cliente",
                "Direccion",
                "Cliente Prospecto",
                "Fecha",
                "Motivo",
                "Duracion",
                "Distancia",
                "Observacion",
            ];

            foreach ($sellers as $seller) {
                foreach ($seller->visits as $visit) {
                    $letter = 'A';
                    $latitudeFrom = $visit->latitude_checkin;
                    $longitudeFrom = $visit->longitude_checkin;
                    $latitudeTo = $visit->contactsWarehouse->latitude;
                    $longitudeTo = $visit->contactsWarehouse->length;

                    if ($latitudeTo && $longitudeTo) {
                        $distance = $this->calculateDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);
                    } else {
                        $distance = "Ninguna";
                    }

                    $sheet->setCellValue($letter++ . $row, $seller->name . ' ' . $seller->surname);
                    $sheet->setCellValue($letter++ . $row, $visit->contact->identification);
                    $sheet->setCellValue($letter++ . $row, $visit->contact->name . ' ' . $visit->contact->surname);
                    $sheet->setCellValue($letter++ . $row, $visit->contact->address);
                    $sheet->setCellValue($letter++ . $row, $visit->contact->customer->category_id == 24473 ? 'SI' : 'NO');
                    $sheet->setCellValue($letter++ . $row, $visit->checkin_date);
                    $sheet->setCellValue($letter++ . $row, $this->getMotiveVisit($visit));
                    $sheet->setCellValue($letter++ . $row, $visit->duration);
                    $sheet->setCellValue($letter++ . $row, $distance);
                    $sheet->setCellValue($letter++ . $row++, $visit->observation);
                }
            }

            $sheet->setAutoFilter('A1:J' . $row);

            $letter = 'A';
            foreach ($headers as $header) {
                $sheet->setCellValue($letter . '1', $header);
                $sheet->getStyle($letter . '1')->getFont()->setBold(true);
                $sheet->getColumnDimension($letter)->setAutoSize(true);
                $sheet->getStyle('A1:' . $letter++ . $row)->applyFromArray($style);
            }

            $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

            $fileName = storage_path("app/public/reports/") . 'Visitas vendedores.xlsx';

            $writer->save($fileName);

            return 'Visitas vendedores.xlsx';
        }
    }

    public function getMotiveVisit($visit)
    {
        $motive = "";
        if ($visit->sales_management) {
            $motive .= "Gestión de ventas";
        }

        if ($visit->sales_management && $visit->collection) {
            $motive .= "-";
        }

        if ($visit->collection) {
            $motive .= "Cobranza";
        }

        if (($visit->collection && $visit->training) ||
            ($visit->sales_management && !$visit->collection && $visit->training)
        ) {
            $motive .= "-";
        }

        if ($visit->training) {
            $motive .= "Capacitación";
        }

        return $motive;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param int $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function calculateDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}
