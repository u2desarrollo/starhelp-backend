<?php


namespace App\Services;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Service
{

    protected $array;
    protected $branchoffice;
    protected $branchoffices;
    protected $voucher_type;
    protected $voucher_types;
    protected $seller;
    protected $sellers;
    protected $contact;
    protected $contactWarehouse;
    protected $document;

    protected $year_month;
    protected $identification_contact;
    protected $code_contact_warehouse;
    protected $name_contact;
    protected $code_branchoffice;
    protected $code_voucher_type;
    protected $prefix;
    protected $consecutive;
    protected $transaction_type;
    protected $document_date;
    protected $issue_date;
    protected $due_date;
    protected $total_value;
    protected $invoice_balance;
    protected $identification_seller;
    protected $name_seller;
    protected $observation;
    protected $account_code;
    protected $erp_id_document;
    protected $erp_id_receivable;
    protected $account_code_puc;
    protected $payment_applied_erp;
    protected $detail;
    protected $description;
    protected $debits;
    protected $credits;
    protected $erp_id_transaction;
    protected $contra_key;

    public function __construct()
    {
        $this->setBranchoffices();
        $this->setVoucherTypes();
        $this->setSellers();
        $this->year_month = 0;
        $this->identification_contact = 1;
        $this->code_contact_warehouse = 2;
        $this->name_contact = 3;
    }

    public function setBranchoffices()
    {

        $this->branchoffices = collect(DB::table('branchoffices')
            ->whereNull('deleted_at')->get());
    }

    public function setVoucherTypes()
    {
        $this->voucher_types = collect(DB::table('voucherstypes')->get());
    }

    public function setSellers()
    {
        $this->sellers = collect(DB::table('contacts')->where([
            'is_employee' => true,
            'deleted_at' => null
        ])->get());
    }

    public function cleanSpaces()
    {
        // reccorro mi array y quito los espacios en blanco q pueda llegar a tener y caracteres especiales
        foreach ($this->array as $key => $value) {
            $this->array[$key] = trim($value);
        }
    }

    public function transformDate($date)
    {
        //sustraigo ciertos caracteres de el parametro y retorno un string con el formato deseado
        if ($date != '' || $date != null) {
            $year = substr($date, 0, 4);
            $month = substr($date, 4, 2);
            $day = substr($date, -2);
            $date_string = $year . '-' . $month . '-' . $day;
            $time = strtotime($date_string);
            $new_format = date('Y-m-d', $time);
            return $new_format;
        }
        return null;
    }

    public function transformYearMonth($date)
    {
        //sustraigo ciertos caracteres de el parametro y retorno un string con el formato deseado
        $year = substr($date, 0, 4);
        $month = substr($date, 4, 2);
        return $year . '-' . $month;
    }

    public function branchofficeTreatment()
    {
        //  creo un objeto de tipo  ranchOffice verifico su existencia,
        //  en caso de no existir creo uno con datos temporales
        if (is_null($this->branchoffice) || $this->branchoffice->code != $this->array[$this->code_branchoffice]) {

            $this->branchoffice = $this->branchoffices
                ->firstWhere('code', '=', $this->array[$this->code_branchoffice]);

            if (!$this->branchoffice) {

                $branch_office = [
                    'subscriber_id' => 1,
                    'name' => 'Bogotá',
                    'address' => 'Por definir',
                    'telephone' => 'Por definir',
                    'main' => false,
                    'city_id' => 148,
                    'branchoffice_type' => true,
                    'code' => $this->array[$this->code_branchoffice],
                    'deleted_at' => null
                ];

                DB::table('branchoffices')->insert($branch_office);

                $this->branchoffice = DB::table('branchoffices')->where(Arr::only($branch_office, ['code']))->first();

                $this->branchoffices->push($this->branchoffice);
            }
        }
    }

    public function voucherTratment()
    {
        //creo un objeto de tipó VouchersType y valido si existe en caso de no, lo creo
        // con datos temporales
        if (
            is_null($this->voucher_type)
            || $this->voucher_type->code_voucher_type != $this->array[$this->code_voucher_type]
            || $this->voucher_type->branchoffice_id != $this->branchoffice->id
        ) {

            $this->voucher_type = $this->voucher_types
                ->where('code_voucher_type', $this->array[$this->code_voucher_type])
                ->where('branchoffice_id', $this->branchoffice->id)
                ->first();

            if (!$this->voucher_type) {

                $voucher_type = [
                    'branchoffice_id' => $this->branchoffice->id,
                    'code_voucher_type' => $this->array[$this->code_voucher_type],
                    'name_voucher_type' => 'Por definir',
                    'short_name' => 'Por definir',
                    'electronic_bill' => false,
                    'consecutive_number' => 1,
                    'affectation' => 1,
                    'state' => true,
                ];

                DB::table('voucherstypes')->insert($voucher_type);

                $this->voucher_type = DB::table('voucherstypes')
                    ->where(Arr::only($voucher_type, ['code_voucher_type', 'branchoffice_id']))->first();
                $this->voucher_types->push($this->voucher_type);
            }
        }
    }

    public function sellersTreatment()
    {
        //creo un objeto de tipo seller valido su existencia, en caso de no existir se cre ,
        // como tambien se crea su registro en la tabla de user
        if (is_null($this->seller) || $this->seller->identification != $this->array[$this->identification_seller]) {

            $this->seller = $this->sellers
                ->firstWhere('identification', $this->array[$this->identification_seller]);

            if (!$this->seller) {

                $this->seller = null;

                $seller = [
                    'id' => null,
                    'identification' => $this->array[$this->identification_seller],
                    'subscriber_id' => 1,
                    'name' => $this->array[$this->name_seller],
                    'city_id' => 1,
                    'class_person' => 5,
                    'taxpayer_type' => 5,
                    'tax_regime' => 5,
                    'is_employee' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => null
                ];
//                $this->sellers->push($seller);
//                $this->seller = (object) $this->sellers
//                    ->firstWhere('identification', $this->array[$this->identification_seller]);

//                DB::table('contacts')->insert($seller);
//                $this->seller = DB::table('contacts')
//                    ->where(Arr::only($seller, ['identification', 'deleted_at']))->first();
//                $this->sellers->push($this->seller);
            }
        }
    }

    public function thirdPartyTreatment()
    {
        //creo un objeto de tipó contact y valido si existe en caso de no,
        //lo creo y creo su correspondiente ContactCustomer  y  retorno el contact
        if (is_null($this->contact) || $this->contact->identification != $this->array[$this->identification_contact]) {
            $contact = [
                'identification' => $this->array[$this->identification_contact],
                'subscriber_id' => 1,
                'name' => $this->array[$this->name_contact],
                'city_id' => 1,
                'class_person' => 5,
                'taxpayer_type' => 5,
                'tax_regime' => 5,
                'is_customer' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $this->contact = DB::table('contacts')
                ->where(Arr::only($contact, ['identification']))->first();

            if (!$this->contact) {
                DB::table('contacts')->insert($contact);
                $this->contact = DB::table('contacts')
                    ->where(Arr::only($contact, ['identification']))->first();

                    //  registro en contacts customers
                    DB::table('contacts_customers')->insert([
                        'contact_id'=>$this->contact->id,
                        'blocked'=>false
                    ]);
            }
        }
    }

    public function contactWarehouseTreatment(): void
    {
        if (is_null($this->contactWarehouse) || $this->contactWarehouse->contact_id != $this->contact->id || $this->contactWarehouse->code != $this->contact->id) {
            $contactC = [
                'contact_id' => $this->contact->id,
                'seller_id' => is_null($this->seller) ? null : $this->seller->id,
                'code' => strval($this->array[$this->code_contact_warehouse]),
                'description' => strval($this->contact->name) . ' ' . strval($this->contact->surname),
                'state' => true,
                'minimum_order_value' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $this->contactWarehouse = DB::table('contacts_warehouses')
                ->where(Arr::only($contactC, ['contact_id', 'code']))->first();

            if (!$this->contactWarehouse) {
                DB::table('contacts_warehouses')->insert($contactC);
                $this->contactWarehouse = DB::table('contacts_warehouses')
                    ->where(Arr::only($contactC, ['contact_id', 'code']))->first();
            }
        }
    }
}
