<?php

namespace App\Services;
use App\Entities\Parameter;
use App\Entities\CaseTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use Tymon\JWTAuth\Facades\JWTAuth;

class CaseService
{

    public function createCase($request) {
        $state = Parameter::whereHas('paramtable', function ($query) {
            $query->where('code_table', '500');
        })->where('code_parameter', '01')->first();
        $user = JWTAuth::parseToken()->toUser();
        
        $data = $request->all();
        $data['state_id'] = $state->id;
        $data['company_id'] = $user->company_id;
        $data['area_id'] = $user->area_id;
        $data['user_id'] = $user->id;


        $dataCreate = CaseTable::create($data);

        return [
            'data' => $dataCreate,
            'code' => 201
        ];
    }

}