<?php


namespace App\Services;


use App\Mail\NotificationErrorCronMail;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Entities\Document;
use App\Entities\ContactWarehouse;
use App\Entities\Contact;

class InventaryOperationService
{


    public function __construct()
    {
    }


    public function readFile()
    {
        set_time_limit(300);

        $error = false;
        $subject = 'ERROR EN EJECUCIÓN DE TAREAS CRON DE OPERACIONES DE INVENTARIO !!!';
        $text = '';

        $operationInventories = [];

        $sftp = Storage::disk('sftp');

        $namesFiles = $sftp->files('WEB/PENDING');

        //Recorre el array y obtiene los nombre solo de los que contengan INVERP
        foreach ($namesFiles as $value) {
            $part = explode('/', $value);
            if (substr($part[count($part) - 1], 0, 6) == 'INVERP') {
                array_push($operationInventories, $part[count($part) - 1]);
            }
        }

        //Cierra lo conexión sftp
        $sftp->getDriver()->getAdapter()->disconnect();

        if (sizeof($operationInventories)) {

            try {
                Log::info('//--------------- Archivos encontrados de OPERACIONES DE INVENTARIO en servidor SFTP: ' . implode(', ', $operationInventories));

                // Copia el archivo del disco SFTP al disco local
                foreach ($operationInventories as $value) {
                    $file = $sftp->get('WEB/PENDING/' . $value);
                    Storage::disk('local')->put('public/portfolio/' . $value, $file);
                    $sftp->delete('WEB/PENDING/' . $value);
                }

                // Cierra lo conexión sftp
                $sftp->getDriver()->getAdapter()->disconnect();

                foreach ($operationInventories as $v) {
                    $cant = 0;
                    try {
                        $file = fopen(storage_path('app/public/portfolio/' . $v), "r");
                        Log::info('//--------------- Procesando archivo de INVERP: ' . $v);
                        DB::beginTransaction();

                        while (!feof($file)) {
                            // Leyendo una linea
                            $line = fgets($file);
                            //sacando un array apartir de la linea extraida del archivo
                            $ids = explode("|", utf8_encode($line));
                            if ($cant > 0 && (count($ids) > 1)) {
                                if ($ids[0] == 'TER') {
                                    //consulta la sucursal
                                    $cW = DB::table('contacts_warehouses')->where('id', (int)$ids[1])->first();
                                    if ($cW) {
                                        DB::table('contacts')
                                            ->where('id', $cW->contact_id)->update(['erp_id' => $ids[2]]);
                                    }
                                } elseif ($ids[0] == 'DOC') {
                                    //actualiza erp_id del documento
                                    DB::table('documents')->where('id', (int)$ids[1])->update(['erp_id' => $ids[2]]);
                                }
                            }
                            $cant++;
                        }

                        fclose($file);

                        // elimino del servidor de colsaisa
                        DB::commit();
                        Log::info("//--------------- Archivo INVERP $v procesado con exito. Total registros: $cant");
                        Storage::disk('local')->delete('public/portfolio/' . $v);
                    } catch (\Exception $th) {
                        Log::info($th->getMessage());
                        DB::rollBack();
                    }
                }

                Log::info('//--------------- Archivos de OPERACIONES DE INVENTARIO procesados con éxito: ' . implode(', ', $operationInventories));

            } catch (FileNotFoundException $e) {
                $error = true;
                $text = 'NO SE HAN PODIDO PROCESAR LOS ARCHIVOS DE OPERACIONES DE INVENTARIO';
                Log::critical('//---------------- ' . $text);
                Log::critical($e->getMessage());
            }
        } else {
            $error = true;
            $text = 'NO SE ENCONTRARON ARCHIVOS DE OPERACIONES DE INVENTARIO EN EL SERVIDOR FTP';
            Log::critical('//---------------- ' . $text);
        }

        if ($error){
            $dayOfWeek = date('N');
            $lastDateSend = Cache::get('lastDateSendErrorCronInventoryOperation', now()->addMinutes(-61)->format('Y-m-d H:i:s'));
            $hour = date('H');
            $lastHour = 17;

            if ($dayOfWeek > 5){
                $lastHour = 12;
            }

            if ($hour >= 8 && $hour <= $lastHour && $dayOfWeek < 7 && Carbon::parse($lastDateSend)->diffInMinutes(now()) > 60) {
                $emails = config('app.emails_notification_error_cron');

                $emailsAsArray = explode(';', $emails);

                Mail::to($emailsAsArray)->send(new NotificationErrorCronMail($text, $subject));

                Cache::put('lastDateSendErrorCronInventoryOperation', now()->format('Y-m-d H:i:s'), 3600);
            }
        }
    }
}
