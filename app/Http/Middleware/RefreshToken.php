<?php

namespace App\Http\Middleware;

use App\Entities\User;
use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class RefreshToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        try {

            //Obtiene el token actual
            $token = JWTAuth::getToken();

            //Refresca el token
            $new_token = JWTAuth::refresh($token);

            //Almacena en base de datos el nuevo token
            User::where('id', Auth::user()->id)->update(['session_id' => $new_token]);

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        }

        $response = $next($request);

        //Agrega el nuevo token al encabezado de la respuesta
        $response->header('Authorization', $new_token);

        return $response;
    }

}
