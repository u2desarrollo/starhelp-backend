<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
/**
 * Middleware que permite verificar los permisos de un usuario autenticado
 * para permitirle el acceso a la petición en donde se implemente el middleware
 *
 * @param Illuminate\Http\Request $request
 * @param Closure $next
 * @return $next o error de autenticación
 */
class VerifyPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $user = auth()->user();
        $user = JWTAuth::parseToken()->authenticate();

        $permissionsMiddleware = array_slice(func_get_args(), 2) ?: [null]; // Genera un array conteniendo los permisos pasados por parametro en el Middleware
        $permissionsUser = $user->getUserPermissions()
            // Filtra los permisos del usuario para dejar en la colección solo los permisos que coincidan con los solicitados por el Middleware
            ->filter(function ($permission) use ($permissionsMiddleware) {
                return array_search($permission, $permissionsMiddleware) !== false;
            })->count();
        if ($permissionsUser > 0) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Acceso no autorizado',
            'errors' => ['No tiene acceso a la acción que intenta ejecutar']
        ], 403);
    }
}
