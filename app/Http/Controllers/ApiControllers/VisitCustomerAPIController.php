<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateVisitCustomerAPIRequest;
use App\Http\Requests\ApiRequests\UpdateVisitCustomerAPIRequest;
use Illuminate\Support\Facades\DB;
use App\Entities\VisitCustomer;
use App\Entities\SellerCoordinate;
use App\Repositories\VisitCustomerRepository;
use App\Repositories\DocumentRepository;
use App\Repositories\CashReceiptsRepository;
use App\Repositories\SellerCoordinateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Validator;
use Response;

/**
 * Class VisitCustomerController
 * @package App\Http\Controllers\ApiControllers
 */

class VisitCustomerAPIController extends AppBaseController
{
	/** @var  VisitCustomerRepository */
	private $visitCustomerRepository;
	private $documentRepository;
	private $cashReceiptsRepository;
	private $sellerCoordinateRepository;

	public function __construct(VisitCustomerRepository $visitCustomerRepo, DocumentRepository $documentRepository, CashReceiptsRepository $cashReceiptsRepository, SellerCoordinateRepository $sellerCoordinateRepository) {
        $this->documentRepository = $documentRepository;
		$this->visitCustomerRepository = $visitCustomerRepo;
		$this->cashReceiptsRepository = $cashReceiptsRepository;
		$this->sellerCoordinateRepository = $sellerCoordinateRepository;
	}

	public function index(Request $request) {
		$this->visitCustomerRepository->pushCriteria(new RequestCriteria($request));
		$this->visitCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
		$visitCustomers = $this->visitCustomerRepository->findSellers($request);

		return $this->sendResponse($visitCustomers->toArray(), 'Visits Customers retrieved successfully');
	}

	public function store(CreateVisitCustomerAPIRequest $request) {
        $input = $request->all();
		try {
			$visitCustomers = $this->visitCustomerRepository->create($input);
			return response()->json($visitCustomers,200);
		} catch (\Exception $e) {
			return response()->json(['error' => ' Se presentó un error al procesar la solicitud.' . $e], 500);
		}
	}

	public function checkout(UpdateVisitCustomerAPIRequest $request) {

		$input = $request->all();
		$input['checkout_ok'] = mb_convert_case($input['checkout_ok'], MB_CASE_UPPER);

		try {
			if ($input['checkout_ok'] != 'SI' && $input['checkout_ok'] != 'NO') {
				return response()->json(['error' => ' Estado del checkout inválido. Debe ser SI o NO.'], 400);
            }

            $visitCustomers = $this->visitCustomerRepository->checkout($input, $input['cod_segven']);
			return response()->json($visitCustomers);
		} catch (\Exception $e) {
			Log::info($e->getMessage());
			return response()->json(['error' => ' Se presentó un error al procesar la solicitud. '.$e],500);
		}
	}
	public function getMarkerCoordinate(Request $request){
        $markerCoordinates= $this->visitCustomerRepository->getMarkerCoordinate($request->all());
        return $this->sendResponse($markerCoordinates,'Marker coordinates retrieved successfully');
	}

	public function listDatesSeller(Request $request, $id)
    {
        $query = DB::table('visits_customers')->selectRaw('DATE(checkin_date) as date')->distinct()->where('seller_id', $id)->get();
        return $this->sendResponse($query,'Marker coordinates retrieved successfully');
			// $dates = array_map(function ($value) {return $value->date;}, $query->get()->toArray());
			// if ($request->wantsJson()) {
			//     return new SellerDateCollection($dates);
			// }
	}

	public function listCordinatesDateSeller(Request $request, $id, $date)
    {
		$visits = $this->visitCustomerRepository->searchVisitsSeller($id, $date);

		$contact = $this->sellerCoordinateRepository->getCoordinates($visits, $id, $date);
        // $contact = SellerCoordinate::where('seller_id', $id)->whereDate('datetime', $date)->orderBy('datetime')->get();

        return $this->sendResponse($contact,'Marker coordinates retrieved successfully');
	}

	public function currentLocation(Request $request, $id)
    {

		$contact = $this->sellerCoordinateRepository->currentLocation($id);
        return $this->sendResponse($contact,'Marker coordinates retrieved successfully');
	}
	public function listVisitsSeller(Request $request, $id, $date)
	{
		$visitCustomers = $this->visitCustomerRepository->searchVisitsSeller($id, $date);

		$visitCustomers = $this->documentRepository->searchDocumentsVisits($visitCustomers);

		$visitCustomers = $this->cashReceiptsRepository->searchcollectionVisits($visitCustomers);

        return $this->sendResponse($visitCustomers,'Marker visits retrieved successfully');
	}
}
