<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateLineAPIRequest;
use App\Http\Requests\ApiRequests\UpdateLineAPIRequest;
use App\Entities\Line;
use App\Entities\DataSheetLine;
use App\Repositories\LineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

//Repositories
use App\Repositories\DataSheetRepository;

/**
 * Class LineController
 * @package App\Http\Controllers\ApiControllers
 */

class LineAPIController extends AppBaseController
{
    /** @var  LineRepository */
    private $lineRepository;
    private $dataSheetRepository;

    public function __construct(LineRepository $lineRepo, DataSheetRepository $DataSheetRepo)
    {
        $this->lineRepository = $lineRepo;
        $this->dataSheetRepository = $DataSheetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     *
     */
    public function index(Request $request)
    {
        $this->lineRepository->pushCriteria(new RequestCriteria($request));
        $this->lineRepository->pushCriteria(new LimitOffsetCriteria($request));
        $lines = $this->lineRepository->findAll($request);

        return $this->sendResponse($lines->toArray(), 'Lines retrieved successfully');
    }

    /**
     * @param CreateLineAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/lines",
     *      summary="Store a newly created Line in storage",
     *      tags={"Line"},
     *      description="Store Line",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Line that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Line")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Line"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateLineAPIRequest $request)
    {
        $data_sheets = $request->data_sheets;
        $input = $request->except('data_sheets');  //$request->all();

        $lines = $this->lineRepository->create($input);

        //Actualizar la ficha de datos
        if ($data_sheets) {

            //Agregar las fichas
            foreach ($data_sheets as $key => $value) {
                DataSheetLine::create([
                    'data_sheet_id' => $value['id'],
                    'line_id' => $lines->id,
                    'required' => $value['required'] == false || $value['required'] == '' ? 0 : 1
                ]);
            }
        }

        return $this->sendResponse($lines, 'Line saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/lines/{id}",
     *      summary="Display the specified Line",
     *      tags={"Line"},
     *      description="Get Line",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Line",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Line"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Line $line */
        $line = $this->lineRepository->findForId($id);

        if (empty($line)) {
            return $this->sendError('Line not found');
        }

        return $this->sendResponse($line->toArray(), 'Line retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateLineAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/lines/{id}",
     *      summary="Update the specified Line in storage",
     *      tags={"Line"},
     *      description="Update Line",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Line",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Line that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Line")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Line"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateLineAPIRequest $request)
    {
        if ($request['act_prod_conf'] == 0) {
            $input = $request->except('prod_desc_config');
        } else {
            $input = $request->only('prod_desc_config');
        }

        /** @var Line $line */
        $line = $this->lineRepository->findWithoutFail($id);

        if (empty($line)) {
            return $this->sendError('Line not found');
        }

        $line = $this->lineRepository->update($input, $id);

        //Actualizar la ficha de datos
        if ($request->data_sheets) {

            //Eliminar la ficha de datos que no esten en la respuesta
            DataSheetLine::where('line_id', $id)
                ->whereNotIn('data_sheet_id', $request->input('data_sheets.*.id'))->delete();

            //Agregar las fichas de datos nuevas

            foreach ($request->data_sheets as $key => $value) {

                DataSheetLine::updateOrCreate(
                    [
                        'data_sheet_id' => $value['id'],
                        'line_id' => $id
                    ],
                    [
                        'data_sheet_id' => $value['id'],
                        'line_id' => $id,
                        'required' => $value['required'] == false || $value['required'] == '' ? 0 : 1,
                        'order'=>isset($value['order']) ? $value['order'] : null
                    ]
                );
            }
        }

        return $this->sendResponse($line->toArray(), 'Line updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/lines/{id}",
     *      summary="Remove the specified Line from storage",
     *      tags={"Line"},
     *      description="Delete Line",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Line",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Line $line */
        $line = $this->lineRepository->findWithoutFail($id);

        if (empty($line)) {
            return $this->sendError('Line not found');
        }

        $line->delete();

        return $this->sendResponse($id, 'Line deleted successfully');
    }
    public function getLines()
    {
        $lines = Line::all("id", "line_description");
        return $lines;
    }



    public function dataSheetForId($id)
    {
        $res = $this->dataSheetRepository->findForLineId($id);
        return response()->json($res, 200);
    }

    /**
     * Este metodo se encarga de devolver todos los valores de las
     * lineas, esto para utlizarlo en las listas desplegables u otros.
     *
     * @author kevin galindo
     */
    public function linesList()
    {
        $lineList = Line::select('id', 'line_code', 'line_description')->orderBy('line_code', 'asc')->get();
        return response()->json($lineList, 200);
    }
}