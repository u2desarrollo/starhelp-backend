<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateSubCategorieAPIRequest;
use App\Http\Requests\ApiRequests\UpdateSubCategorieAPIRequest;
use App\Entities\SubCategorie;
use App\Repositories\SubCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SubCategorieController
 * @package App\Http\Controllers\ApiControllers
 */

class SubCategorieAPIController extends AppBaseController
{
    /** @var  SubCategorieRepository */
    private $subCategorieRepository;

    public function __construct(SubCategorieRepository $subCategorieRepo)
    {
        $this->subCategorieRepository = $subCategorieRepo;
    }

    /**
     * Display a listing of the SubCategorie.
     * GET|HEAD /subCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->subCategorieRepository->pushCriteria(new RequestCriteria($request));
        $this->subCategorieRepository->pushCriteria(new LimitOffsetCriteria($request));
        $subCategories = $this->subCategorieRepository->findWhere(['categorie_id' => $request->categorie_id]);;

        return $this->sendResponse($subCategories->toArray(), 'Sub Categories retrieved successfully');
    }

    /**
     * Store a newly created SubCategorie in storage.
     * POST /subCategories
     *
     * @param CreateSubCategorieAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubCategorieAPIRequest $request)
    {
        $input = $request->all();

        $subCategories = $this->subCategorieRepository->create($input);

        return $this->sendResponse($subCategories->toArray(), 'Sub Categorie saved successfully');
    }

    /**
     * Display the specified SubCategorie.
     * GET|HEAD /subCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SubCategorie $subCategorie */
        $subCategorie = $this->subCategorieRepository->findWithoutFail($id);

        if (empty($subCategorie)) {
            return $this->sendError('Sub Categorie not found');
        }

        return $this->sendResponse($subCategorie->toArray(), 'Sub Categorie retrieved successfully');
    }

    /**
     * Update the specified SubCategorie in storage.
     * PUT/PATCH /subCategories/{id}
     *
     * @param  int $id
     * @param UpdateSubCategorieAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubCategorieAPIRequest $request)
    {
        $input = $request->all();

        /** @var SubCategorie $subCategorie */
        $subCategorie = $this->subCategorieRepository->findWithoutFail($id);

        if (empty($subCategorie)) {
            return $this->sendError('Sub Categorie not found');
        }

        $subCategorie = $this->subCategorieRepository->update($input, $id);

        return $this->sendResponse($subCategorie->toArray(), 'SubCategorie updated successfully');
    }

    /**
     * Remove the specified SubCategorie from storage.
     * DELETE /subCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SubCategorie $subCategorie */
        $subCategorie = $this->subCategorieRepository->findWithoutFail($id);

        if (empty($subCategorie)) {
            return $this->sendError('Sub Categorie not found');
        }

        $subCategorie->delete();

        return $this->sendResponse($id, 'Sub Categorie deleted successfully');
    }
    /**
     * lista las sublineas
     */
    public function listSubcategories(Request $request)
    {
        $this->subCategorieRepository->pushCriteria(new RequestCriteria($request));
        $this->subCategorieRepository->pushCriteria(new LimitOffsetCriteria($request));

        $subCategorie = $this->subCategorieRepository->findAll($request);

        return $this->sendResponse($subCategorie->toArray(), 'Subcategories retrieved successfully');
    }


    /**
     *
     * Retorna las subcategorias segun el id de la categorias
     *
     * @author Kevin Galindo
     */
    public function subCategoriesByCategorieId(Request $request)
    {
        $category_id = $request->category_id;
        if ($category_id=='' || $category_id==null) {
            return response()->json([
                'message' => 'Falta el id de la categoria',
                'code' => 400
            ], 400);
        }

        $subcategoriesList = SubCategorie::select("id", "subcategorie_code", "description")
            ->where("categorie_id", $category_id)
            ->orderBy('description', 'asc')
            ->get();
        return response()->json($subcategoriesList, 200);
    }
}