<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactProviderAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactProviderAPIRequest;
use App\Entities\ContactProvider;
use App\Repositories\ContactProviderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactProviderController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactProviderAPIController extends AppBaseController
{
    /** @var  ContactProviderRepository */
    private $contactProviderRepository;

    public function __construct(ContactProviderRepository $contactProviderRepo)
    {
        $this->contactProviderRepository = $contactProviderRepo;
    }

    /**
     * Display a listing of the ContactProvider.
     * GET|HEAD /contactProviders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contactProviderRepository->pushCriteria(new RequestCriteria($request));
        $this->contactProviderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactProviders = $this->contactProviderRepository->all();

        return $this->sendResponse($contactProviders->toArray(), 'Contact Providers retrieved successfully');
    }

    /**
     * Store a newly created ContactProvider in storage.
     * POST /contactProviders
     *
     * @param CreateContactProviderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateContactProviderAPIRequest $request)
    {
        $input = $request->all();

        $contactProviders = $this->contactProviderRepository->create($input);

        return $this->sendResponse($contactProviders->toArray(), 'Contact Provider saved successfully');
    }

    /**
     * Display the specified ContactProvider.
     * GET|HEAD /contactProviders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ContactProvider $contactProvider */
        $contactProvider = $this->contactProviderRepository->findWithoutFail($id);

        if (empty($contactProvider)) {
            return $this->sendError('Contact Provider not found');
        }

        return $this->sendResponse($contactProvider->toArray(), 'Contact Provider retrieved successfully');
    }

    /**
     * Update the specified ContactProvider in storage.
     * PUT/PATCH /contactProviders/{id}
     *
     * @param  int $id
     * @param UpdateContactProviderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactProviderAPIRequest $request)
    {
        $input = $request->all();

        /** @var ContactProvider $contactProvider */
        $contactProvider = $this->contactProviderRepository->findWithoutFail($id);

        if (empty($contactProvider)) {
            return $this->sendError('Contact Provider not found');
        }

        $contactProvider = $this->contactProviderRepository->update($input, $id);

        return $this->sendResponse($contactProvider->toArray(), 'ContactProvider updated successfully');
    }

    /**
     * Remove the specified ContactProvider from storage.
     * DELETE /contactProviders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ContactProvider $contactProvider */
        $contactProvider = $this->contactProviderRepository->findWithoutFail($id);

        if (empty($contactProvider)) {
            return $this->sendError('Contact Provider not found');
        }

        $contactProvider->delete();

        return $this->sendResponse($id, 'Contact Provider deleted successfully');
    }

    public function getConveyor()
    {
        return $this->contactProviderRepository->searchConveyor();
    }
}
