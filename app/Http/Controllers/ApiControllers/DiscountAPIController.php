<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DiscountRepository;

use App\Entities\Discount;


class DiscountAPIController extends AppBaseController
{

    private $discountRepository;

    public function __construct(DiscountRepository $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    public function get()
    {
        return $this->sendResponse($this->discountRepository->searchDiscounts(), 'Success');
    }

    public function store(Request $request)
    {
        return $this->discountRepository->createDiscount($request);
    }

    public function show($id)
    {
        return $this->discountRepository->searchDiscount($id);
    }

    public function update(Request $request)
    {
        return $this->discountRepository->updateDiscount($request);
    }

    public function delete($id)
    {
        return $this->discountRepository->deleteDiscount($id);
    }
}
