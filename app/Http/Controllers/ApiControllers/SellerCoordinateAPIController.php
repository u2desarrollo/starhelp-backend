<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateSellerCoordinateAPIRequest;
use App\Http\Requests\ApiRequests\UpdateSellerCoordinateAPIRequest;
use App\Entities\SellerCoordinate;
use App\Repositories\SellerCoordinateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SellerCoordinateController
 * @package App\Http\Controllers\ApiControllers
 */

class SellerCoordinateAPIController extends AppBaseController
{
	/** @var  SellerCoordinateRepository */
	private $sellerCoordinateRepository;

	public function __construct(SellerCoordinateRepository $sellerCoordinateRepo)
	{
		$this->sellerCoordinateRepository = $sellerCoordinateRepo;
	}

	public function store(CreateSellerCoordinateAPIRequest $request)
	{
		$input = $request->all();

		try {
			$sellerCoordinates = $this->sellerCoordinateRepository->create($input);
			return response()->json($sellerCoordinates);
		} catch (\Exception $e) {
			return response()->json(['error' => ' Se presentó un error al procesar la solicitud.' . $e], 500);
		}
    }

    public function getCoordinateSeller(Request $request) {
		$sellerCoordinate = $this->sellerCoordinateRepository->getCoordinateSeller($request->all());

		return $this->sendResponse($sellerCoordinate, 'Coordinate retrieved successfully');
	}
	public function getLineCoordinate(Request $request) {
		$lineCoordinate = $this->sellerCoordinateRepository->getLineCoordinate($request->all());

		return $this->sendResponse($lineCoordinate, 'Line retrieved successfully');
	}
}
