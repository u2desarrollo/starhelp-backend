<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Contact;
use App\Http\Requests\ApiRequests\CreateUserAPIRequest;
use App\Http\Requests\ApiRequests\UpdateUserAPIRequest;
use App\Entities\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use App\Traits\LoadImageTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Notifications\NewUserInvitation;
use Faker\Provider\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File as FileFacades;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\ApiControllers
 */
class UserAPIController extends AppBaseController
{

    use LoadImageTrait;

    /** @var  UserRepository */
    private $userRepository;
    private $userService;

    public function __construct(UserRepository $userRepo, UserService $userService)
    {
        $this->userRepository = $userRepo;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->findAll($request);

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     * @return Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $input = $request->all();

        // Validar Correo
        $validateEmail = User::where('email', strtolower($input['email']))->exists();

        if ($validateEmail) {
            return response()->json([
                'message' => 'El correo ' . strtolower($input['email']) . ' ya existe.'
            ], 400);
        }


        $input['photo'] = str_replace("/storage", "", isset($input['photo']) ? $input['photo'] : '/storage/contacts/default.png');

        $contact = Contact::find($input['contact_id']);

        if ($contact) {
            $input['password'] = $contact->identification;
        } else {
            $input['password'] = str_random(12);
        }

        $signature = $request->file('signature');
        if ($signature) {
            Storage::disk('local')->put('public/signature/' . $signature->getClientOriginalName(), file($signature));
            $input['signature'] = $signature->getClientOriginalName();
        }

        $input['clerk_id'] = 1;
        $users = $this->userRepository->create($input);

        return $this->sendResponse($users->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findForId($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->email_cash_receipts_app = explode(',', $user->email_cash_receipts_app);

        return $this->sendResponse($user, 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     * @return Response
     * @throws \Exception
     */
    public function update($id, Request $request)
    {
        $input = $request->all();


        // /** @var User $user */
        $user = $this->userRepository->findForId($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $input['photo'] = $request->photo->getClientOriginalName();
            Storage::disk('local')->put('public/users/' . $input['photo'], file($photo));
        } else {
            if ($request->photo == 'null') {
                $input['photo'] = null;
            } else {
                unset($input['photo']);
            }
        }

        if ($request->hasFile('signature')) {
            $signature = $request->file('signature');
            $input['signature'] = $request->signature->getClientOriginalName();
            Storage::disk('local')->put('public/signature/' . $input['signature'], file($signature));
        } else {
            if ($request->signature == 'null') {
                $input['signature'] = null;
            } else {
                unset($input['signature']);
            }

        }

        $contact = Contact::find($input['contact_id']);

        if ($contact) {
            $input['password'] = $contact->identification;
        } else {
            $input['password'] = str_random(12);
        }

        $input['clerk_id'] = 1;
        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }


    public function changePassword(Request $request)
    {
        $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|string|confirmed',
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return $this->sendError('User not found');
        }

        $user->password = $request->password;
        $user->password_changed = true;
        $user->save();
        return $this->sendResponse($user->toArray(), 'User password changed successfully');
    }

    public function sellersStats(Request $request)
    {
        try {
            $sellers = $this->userRepository->sellersStatistics($request);

            if (!$sellers) {
                return $this->sendError('Sellers not found');
            }

            return $this->sendResponse($sellers, 'Sellers retrieved successfully');
        } catch (\Exception $error) {
            return $error;
        }
    }

    public function reportSellersStats(Request $request)
    {
        try {
            $sellers = $this->userService->generateReportVisits($request);

            return Storage::download('/public/reports/Visitas vendedores.xlsx');
        } catch (\Exception $error) {
            return $error;
        }
    }


    public function setUserRole(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->getRoles()->sync($request->rol_id);
        return response()->json($user, 200);
    }

    /**
     * Traer el nombre de usuario.
     *
     * @return $users User
     * @author Kevin Galindo
     */
    public function getUsersUsername()
    {
        $users = $this->userRepository->getUsersUsername();
        return response()->json($users, 200);
    }
}
