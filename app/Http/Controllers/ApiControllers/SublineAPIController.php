<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateSublineAPIRequest;
use App\Http\Requests\ApiRequests\UpdateSublineAPIRequest;
use App\Entities\Subline;
use App\Repositories\SublineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SublineController
 * @package App\Http\Controllers\ApiControllers
 */

class SublineAPIController extends AppBaseController
{
    /** @var  SublineRepository */
    private $sublineRepository;

    public function __construct(SublineRepository $sublineRepo)
    {
        $this->sublineRepository = $sublineRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     *
     */
    public function index(Request $request)
    {
        //return response()->json(Subline::all()->where('subline_description', '!=', 'null'), 200);
        //$this->sublineRepository->pushCriteria(new RequestCriteria($request));
        //$this->sublineRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sublines = $this->sublineRepository->findWhere(['line_id' => $request->line_id]);
        $sublines = $this->sublineRepository->findAll($request);

        return $this->sendResponse($sublines->toArray(), 'Sublines retrieved successfully');
    }

    /**
     * @param CreateSublineAPIRequest $request
     * @return Response
     *
     *
     */
    public function store(CreateSublineAPIRequest $request)
    {
        $input = $request->all();

        $sublines = $this->sublineRepository->create($input);
        return $this->sendResponse($sublines, 'Subline saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function show($id)
    {
        /** @var Subline $subline */
        $subline = $this->sublineRepository->findWithoutFail($id);

        if (empty($subline)) {
            return $this->sendError('Subline not found');
        }

        return $this->sendResponse($subline->toArray(), 'Subline retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSublineAPIRequest $request
     * @return Response
     *
     *
     */
    public function update($id, UpdateSublineAPIRequest $request)
    {
        $input = $request->all();

        /** @var Subline $subline */
        $subline = $this->sublineRepository->findWithoutFail($id);

        if (empty($subline)) {
            return $this->sendError('Subline not found');
        }

        $subline = $this->sublineRepository->update($input, $id);

        return $this->sendResponse($subline->toArray(), 'Subline updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var Subline $subline */
        $subline = $this->sublineRepository->findWithoutFail($id);

        if (empty($subline)) {
            return $this->sendError('Subline not found');
        }

        $subline->delete();

        return $this->sendResponse($id, 'Subline deleted successfully');
    }

    /**
     * lista las sublineas
     */
    public function listSubline(Request $request)
    {
        $this->sublineRepository->pushCriteria(new RequestCriteria($request));
        $this->sublineRepository->pushCriteria(new LimitOffsetCriteria($request));

        $sublines = $this->sublineRepository->findAll($request);

        return $this->sendResponse($sublines->toArray(), 'Sublines retrieved successfully');
    }

    public function getSubLines($id)
    {
        $sublines = Subline::all("id", "line_id", "subline_description")->where("line_id", $id);
        return $sublines;
    }

    /**
     * Retorna las sublineas segun el id de
     * la linea
     *
     * @author kevin galindo
     */
    public function subLineByLineId(Request $request)
    {
        $line_id = $request->line_id;

        if ($line_id=='' || $line_id==null) {
            return response()->json([
                'message' => 'Falta el id de la linea',
                'code' => 400
            ], 400);
        }

        $sublinesList = Subline::select("id", "subline_code", "subline_description")
            ->where("line_id", $line_id)
            ->orderBy('subline_description', 'asc')
            ->get();
        return response()->json($sublinesList, 200);
    }
}
