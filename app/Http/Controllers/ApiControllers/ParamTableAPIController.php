<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateParamTableAPIRequest;
use App\Http\Requests\ApiRequests\UpdateParamTableAPIRequest;
use App\Entities\ParamTable;
use App\Entities\Parameter;

use App\Repositories\ParamTableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ParamTableController
 * @package App\Http\Controllers\ApiControllers
 */

class ParamTableAPIController extends AppBaseController
{
    /** @var  ParamTableRepository */
    private $paramTableRepository;

    public function __construct(ParamTableRepository $paramTableRepo)
    {
        $this->paramTableRepository = $paramTableRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/paramTables",
     *      summary="Get a listing of the ParamTables.",
     *      tags={"ParamTable"},
     *      description="Get all ParamTables",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ParamTable")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->paramTableRepository->pushCriteria(new RequestCriteria($request));
        $this->paramTableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $paramTables = $this->paramTableRepository->findAll($request);

        return $this->sendResponse($paramTables->toArray(), 'Param Tables retrieved successfully');
    }

    /**
     * @param CreateParamTableAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/paramTables",
     *      summary="Store a newly created ParamTable in storage",
     *      tags={"ParamTable"},
     *      description="Store ParamTable",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ParamTable that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ParamTable")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ParamTable"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateParamTableAPIRequest $request)
    {

        $input = $request->all();

        $paramTables = $this->paramTableRepository->create($input);

        return $this->sendResponse($paramTables->toArray(), 'Param Table saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/paramTables/{id}",
     *      summary="Display the specified ParamTable",
     *      tags={"ParamTable"},
     *      description="Get ParamTable",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ParamTable",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ParamTable"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ParamTable $paramTable */
        $paramTable = $this->paramTableRepository->findForId($id);

        if (empty($paramTable)) {
            return $this->sendError('Param Table not found');
        }

        return $this->sendResponse($paramTable->toArray(), 'Param Table retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateParamTableAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/paramTables/{id}",
     *      summary="Update the specified ParamTable in storage",
     *      tags={"ParamTable"},
     *      description="Update ParamTable",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ParamTable",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ParamTable that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ParamTable")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ParamTable"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateParamTableAPIRequest $request)
    {
        $input = $request->all();

        /** @var ParamTable $paramTable */
        $paramTable = $this->paramTableRepository->findWithoutFail($id);

        if (empty($paramTable)) {
            return $this->sendError('Param Table not found');
        }

        $paramTable = $this->paramTableRepository->update($input, $id);

        return $this->sendResponse($paramTable->toArray(), 'ParamTable updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/paramTables/{id}",
     *      summary="Remove the specified ParamTable from storage",
     *      tags={"ParamTable"},
     *      description="Delete ParamTable",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ParamTable",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ParamTable $paramTable */
        $paramTable = $this->paramTableRepository->findWithoutFail($id);

        if (empty($paramTable)) {
            return $this->sendError('Param Table not found');
        }

        $paramTable->delete();

        return $this->sendResponse($id, 'Param Table deleted successfully');
    }

    public function type_identification()
    {

        $typeIdentities = ParamTable::where('id',1)
                        ->with(['extraParameters' => function($query){
                            $query->select(['id','paramtable_id','name_parameter']);
                        }])
                        ->select(['id','name_table'])
                        ->get();

        return response()->json($typeIdentities);
    }

    public function listData()
    {
        $paramTable = ParamTable::all();
        return response()->json($paramTable, 200);
    }

    public function getContactCategories ($id)
    {
        $paramTableId = null;

        switch ($id) {
            case '1':
                $paramTableId = 5;
            break;
            case '2':
                $paramTableId = 7;
            break;
            case '3':
                $paramTableId = 8;
            break;
        }

        if ($paramTableId == null) {
            return response()->json('Parametro no valido', 400);
        }

        $parameters = Parameter::where('paramtable_id', $paramTableId)->get();

        return response()->json($parameters, 200);
    }
}
