<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateRequestProductAPIRequest;
use App\Http\Requests\ApiRequests\UpdateRequestProductAPIRequest;
use App\Entities\RequestProduct;
use App\Repositories\RequestProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RequestProductController
 * @package App\Http\Controllers\ApiControllers
 */

class RequestProductAPIController extends AppBaseController
{
    /** @var  RequestProductRepository */
    private $requestProductRepository;

    public function __construct(RequestProductRepository $requestProductRepo)
    {
        $this->requestProductRepository = $requestProductRepo;
    }

    /**
     * Display a listing of the RequestProduct.
     * GET|HEAD /requestProducts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->requestProductRepository->pushCriteria(new RequestCriteria($request));
        $this->requestProductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $requestProducts = $this->requestProductRepository->all();

        return $this->sendResponse($requestProducts->toArray(), 'Request Products retrieved successfully');
    }

    /**
     * Store a newly created RequestProduct in storage.
     * POST /requestProducts
     *
     * @param CreateRequestProductAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestProductAPIRequest $request)
    {
        $input = $request->all();
        //Llamo a la funcion del repositoria para crear las solicitudes
        $requestProduct = $this->requestProductRepository->createAll($input);

        return $this->sendResponse($requestProduct->toArray(), 'Request Product saved successfully');
    }

    /**
     * Display the specified RequestProduct.
     * GET|HEAD /requestProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RequestProduct $requestProduct */
        $requestProduct = $this->requestProductRepository->findWithoutFail($id);

        if (empty($requestProduct)) {
            return $this->sendError('Request Product not found');
        }

        return $this->sendResponse($requestProduct->toArray(), 'Request Product retrieved successfully');
    }

    /**
     * Update the specified RequestProduct in storage.
     * PUT/PATCH /requestProducts/{id}
     *
     * @param  int $id
     * @param UpdateRequestProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestProductAPIRequest $request)
    {
        $input = $request->all();

        /** @var RequestProduct $requestProduct */
        $requestProduct = $this->requestProductRepository->findWithoutFail($id);

        if (empty($requestProduct)) {
            return $this->sendError('Request Product not found');
        }

        $requestProduct = $this->requestProductRepository->update($input, $id);

        return $this->sendResponse($requestProduct->toArray(), 'RequestProduct updated successfully');
    }

    /**
     * Remove the specified RequestProduct from storage.
     * DELETE /requestProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RequestProduct $requestProduct */
        $requestProduct = $this->requestProductRepository->findWithoutFail($id);

        if (empty($requestProduct)) {
            return $this->sendError('Request Product not found');
        }

        $requestProduct->delete();

        return $this->sendResponse($id, 'Request Product deleted successfully');
    }
    public function getRequestType(){
        $requestTypes = $this->requestProductRepository->getRequestType();

        return $this->sendResponse($requestTypes->toArray(), 'Request Types retrieved successfully');
    }
    public function insertData(Request $request){
        // $input = $request->all();

        $requestProduct = $this->requestProductRepository->createData($request);

        return $this->sendResponse($requestProduct, 'Request Product saved successfully');
    }
}
