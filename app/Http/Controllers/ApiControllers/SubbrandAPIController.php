<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use App\Entities\Subbrand;
use Illuminate\Http\Request;
use App\Traits\LoadImageTrait;
use App\Repositories\SubbrandRepository;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateSubbrandAPIRequest;
use App\Http\Requests\ApiRequests\UpdateSubbrandAPIRequest;

/**
 * Class SubbrandController
 * @package App\Http\Controllers\ApiControllers
 */

class SubbrandAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  SubbrandRepository */
    private $subbrandRepository;

    public function __construct(SubbrandRepository $subbrandRepo)
    {
        $this->subbrandRepository = $subbrandRepo;
    }

    /**
     * Display a listing of the Subbrand.
     * GET|HEAD /subbrands
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->subbrandRepository->pushCriteria(new RequestCriteria($request));
        $this->subbrandRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$subbrands = $this->subbrandRepository->findWhere(['brand_id' => $request->brand_id]);

        $subbrands = $this->subbrandRepository->findAll($request);

        return $this->sendResponse($subbrands->toArray(), 'Subbrands retrieved successfully');
    }

    /**
     * Store a newly created Subbrand in storage.
     * POST /subbrands
     *
     * @param CreateSubbrandAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubbrandAPIRequest $request)
    {
        $input = $request->except('image');

        $input['image'] =  $this->saveImage($request->image, 'brands/subbrands');
        $input['data_sheet'] =  $this->saveImage($input['data_sheet'], 'brands/subbrands/dataSheets');

        $subbrands = $this->subbrandRepository->create($input);



        return $this->sendResponse($subbrands->toArray(), 'Subbrand saved successfully');
    }

    /**
     * Display the specified Subbrand.
     * GET|HEAD /subbrands/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Subbrand $subbrand */
        $subbrand = $this->subbrandRepository->findWithoutFail($id);

        if (empty($subbrand)) {
            return $this->sendError('Subbrand not found');
        }

        return $this->sendResponse($subbrand->toArray(), 'Subbrand retrieved successfully');
    }

    /**
     * Update the specified Subbrand in storage.
     * PUT/PATCH /subbrands/{id}
     *
     * @param  int $id
     * @param UpdateSubbrandAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubbrandAPIRequest $request)
    {
        $input = $request->except('image');

        /** @var Subbrand $subbrand */
        $subbrand = $this->subbrandRepository->findWithoutFail($id);

        if ($subbrand->image != $request->image) {
            $input['image'] =  $this->saveImage($request->image, 'brands/subbrands');
        }

        if ($input['data_sheet'] != $subbrand->data_sheet) {
            $input['data_sheet'] =  $this->saveImage($input['data_sheet'], 'brands/subbrands/dataSheets');
        }

        if (empty($subbrand)) {
            return $this->sendError('Subbrand not found');
        }

        $subbrand = $this->subbrandRepository->update($input, $id);

        return $this->sendResponse($subbrand->toArray(), 'Subbrand updated successfully');
    }

    /**
     * Remove the specified Subbrand from storage.
     * DELETE /subbrands/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Subbrand $subbrand */
        $subbrand = $this->subbrandRepository->findWithoutFail($id);

        if (empty($subbrand)) {
            return $this->sendError('Subbrand not found');
        }

        $subbrand->delete();

        return $this->sendResponse($id, 'Subbrand deleted successfully');
    }

    public function listSubbrand(Request $request)
    {
        $this->subbrandRepository->pushCriteria(new RequestCriteria($request));
        $this->subbrandRepository->pushCriteria(new LimitOffsetCriteria($request));

        $subbrands = $this->subbrandRepository->findAll($request);

        return $this->sendResponse($subbrands->toArray(), 'Sublines retrieved successfully');
    }

    public function getSubBrands($id)
    {
        $subbrands = Subbrand::all("id", "brand_id", "description")->where("brand_id", $id);
        return $subbrands;
    }
    /**
     * Este metodo devuelte las submarcas segun el id de la marca
     *
     * @author kevin galindo
     */
    public function subBrandByBrandId(Request $request)
    {
        $brand_id = $request->brand_id;

        if (!$brand_id) {
            return response()->json([
                'message' => 'Falta el id de la marca',
                'code' => 400
            ], 400);
        }

        $subbrandList = Subbrand::select("id", "subbrand_code", "description")
            ->where("brand_id", $brand_id)
            ->orderBy('description', 'asc')
            ->get();
        return response()->json($subbrandList, 200);
    }
}