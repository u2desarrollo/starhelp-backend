<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\CashReceiptsApp;
use App\Http\Requests\ApiRequests\CreateConsignmentAPIRequest;
use App\Entities\Consignment;
use App\Entities\User;
use App\Repositories\ConsignmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Traits\LoadImageTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * Class ConsignmentAPIController
 * @package App\Http\Controllers\ApiControllers
 */

class ConsignmentAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  ConsignmentRepository */
    private $ConsignmentRepository;

    public function __construct(ConsignmentRepository $ConsignmentRepo)
    {
        $this->ConsignmentRepository = $ConsignmentRepo;
    }

    public function store(CreateConsignmentAPIRequest $request)
    {

        $array = $request->all();
        $array1 = json_decode($array['recibosCaja']);

        if (count($array1) <= 0) {
            return response()->json('La consignación debe contener recibos de caja', 400);
        }


        try {
            $consignment = new Consignment();
            $user = User::where('contact_id', $request->iduser)->first();
            $consignment->bank_id = $request->banco;
            $consignment->consignment_number = $request->numeroConsignacion;
            $consignment->user_id = $user->id;
            if ($request->hasFile('image')) {

                $file_name = str_random() . "." . $request->image->extension();
                Storage::disk('colsaisa')->putFileAs('/consignments', $request->image, $file_name);
                $consignment->photo_consignment = $file_name;
            }
            $consignment->date_time = date('Y-m-d');

            $consignment->save();

            foreach ($array1 as $key => $value) {
                $c = CashReceiptsApp::findOrFail($value);
                $c->update(['consignment_id' => $consignment->id, 'id_erp' => null]);
            }
            //  retornar ids de los rc
            return response()->json($consignment, 200);
        } catch (\Exception $th) {
            return response()->json($th->getMessage() . ', error en el archivo ' . $th->getFile() . ', linea ' . $th->getLine(), 500);
        }
    }
}
