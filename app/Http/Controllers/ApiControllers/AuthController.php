<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\BranchOffice;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\Role;
use App\Entities\Subscriber;
use App\Entities\User;
use App\Http\Requests\ApiRequests\AuthRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{

    public function authenticate(AuthRequest $request)
    {

        $credentials = [
            'email' => strtolower($request->email),
            'password' => $request->password
        ];

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'cod' => 1,
                'status' => 'error',
                'message' => 'No podemos encontrar una cuenta con estas credenciales.'
            ], 401);
        }

        $user = User::select(
            'branchoffice_id', 
            'branchoffice_warehouse_id', 
            'contact_id', 
            'contact_warehouse_id', 
            'clerk_id', 
            'email', 
            'id', 
            'module_id', 
            'name', 
            'surname', 
            'password_changed', 
            'photo', 
            'subscriber_id', 
            'type',
            'area_id',
            'company_id'
            )
            ->where('id', Auth::user()->id)
            ->with([
                'area',
                'company',
                'branchOfficeWarehouse', 
                'modules', 
                'subscribers', 
                'getRoles', 
                'contact' => function ($query) {
                    $query->with('employee');
                }, 
                'clerk'
            ])
            ->first();

        $branch_offices = BranchOffice::select('id', 'name', 'main')
            ->where('subscriber_id', $user->subscriber_id)
            ->get();

        $branch_offices_warehouses = BranchOfficeWarehouse::select('id', 'warehouse_code', 'warehouse_description', 'branchoffice_id')
            ->whereIn('branchoffice_id', $branch_offices->pluck('id'))
            ->where('warehouse_status', '!=', '9')
            ->get();

        $roleAndPermisions = Role::with('getPermissions')->where('id', $user->getRoles[0]->id)->get();

        $subscriber = Subscriber::find($user->subscriber_id);

        try {

            if ($user->session_id != null || $user->session_id != "") {
                JWTAuth::setToken($user->session_id);
                JWTAuth::invalidate();
            }
        } catch (TokenInvalidException $e) {
        } catch (TokenExpiredException $e) {
        }

        $user->update(['session_id' => $token]);

        $user->token = $user->session_id;

        return response()->json([
            'status' => 'success',
            'data' => [
                'user' => $user->toArray(),
                'branch_offices' => $branch_offices->toArray(),
                'branch_offices_warehouses' => $branch_offices_warehouses->toArray(),
                'role_and_permisions' => $roleAndPermisions[0]->getPermissions,
                'subscriber' => $subscriber
            ]
        ]);
    }

    public function authenticateApp(Request $request)
    {
        //dd(bcrypt('Colombia2021*'));
        $credentials = [
            'email' => strtolower($request->email),
            'password' => $request->password
        ];

        if (!$request->has('email') || strlen($request->input('email')) < 1) {
            return response()->json([
                'error' => 'No envio el email.'
            ], 400);
        }

        if (!$request->has('password') || strlen($request->input('password')) < 0) {
            return response()->json([
                'error' => 'No envio la contrasenia.'
            ], 400);
        }

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'error' => 'Email o contrasenia incorrectos.'
            ], 403);
        }

        $user = DB::table('users')
            ->select('users.contact_id', 'users.name', 'users.surname', 'contacts_employees.distance_tolerance')
            ->join('contacts', 'contacts.id', '=', 'users.contact_id')
            ->leftjoin('contacts_employees', 'contacts_employees.contact_id', '=', 'contacts.id')
            ->where('users.id', Auth::user()->id)
            ->first();

        $response = [
            'iduser' => $user->contact_id,
            'name' => $user->name . ' ' . $user->surname,
            'token' => $token,
            //'distance_tolerance' => $user->distance_tolerance != null ? $user->distance_tolerance : 0,
            'business_url' => 'https://colrecambios.starcommerce.co/'
        ];

        return $response;
    }

    public function logout($id = null, Request $request)
    {
        $token = $request->header('Authorization');

        try {
            if ($id !== null) {

                $user = User::find($id);
                $user->update(['session_id' => NULL]);

                return response()->json([
                    'status' => 'success',
                    'message' => "El usuario se desconecto correctamente."
                ], 200);
            } else {

                $user = User::find(Auth::user()->id);
                $user->update(['session_id' => NULL]);
                JWTAuth::invalidate($token);

                return response()->json([
                    'status' => 'success',
                    'message' => "El usuario se desconecto correctamente."
                ], 200);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Error al cerrar sesion, porfavor intente de nuevo.' . '(' . $e->getMessage() . ')'
            ], 500);
        }
    }
}
