<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\User;
use App\Entities\Contact;
use App\Entities\BranchOffice;
use App\Http\Requests\ApiRequests\CreateContactWarehouseAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactWarehouseAPIRequest;
use App\Entities\ContactWarehouse;
use App\Repositories\ContactWarehouseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;
use Response;
use DB;

use App\Services\ContactWarehouseService;
use Illuminate\Support\Arr;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class ContactWarehouseController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactWarehouseAPIController extends AppBaseController
{
	/** @var  ContactWarehouseRepository */
	private $contactWarehouseRepository;
	private $contactWarehouseService;

	public function __construct(
		ContactWarehouseRepository $contactWarehouseRepo,
		ContactWarehouseService $contactWarehouseService
	) {
		$this->contactWarehouseRepository = $contactWarehouseRepo;
		$this->contactWarehouseService = $contactWarehouseService;
	}


	public function getWarehouses(Request $request)
	{
		$input = $request->toArray();
		$this->contactWarehouseRepository->pushCriteria(new RequestCriteria($request));
		$this->contactWarehouseRepository->pushCriteria(new LimitOffsetCriteria($request));
		$contactsWarehouses = ContactWarehouse::select('id', 'description')
			->where('description', 'ILIKE', '%' . $input[0] . '%')->get();
		return $this->sendResponse($contactsWarehouses, 'Contact Warehouses retrieved successfully');
	}

    /**
     * Store a newly created ContactWarehouse in storage.
     * POST /contactWarehouses
     *
     * @param CreateContactWarehouseAPIRequest $request
     *
     * @return Response
     * @throws ValidatorException
     */
	public function store(CreateContactWarehouseAPIRequest $request)
	{
		$input = $request->all();
		$contactWarehouses = $this->contactWarehouseRepository->create($input);

		return $this->sendResponse($contactWarehouses->toArray(), 'Contact Warehouse saved successfully');
	}

	/**
	 * Display the specified ContactWarehouse.
	 * GET|HEAD /contactWarehouses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */


	/**
	 * Update the specified ContactWarehouse in storage.
	 * PUT/PATCH /contactWarehouses/{id}
	 *
	 * @param  int $id
	 * @param UpdateContactWarehouseAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateContactWarehouseAPIRequest $request)
	{
		$input = $request->all();
		/** @var ContactWarehouse $contactWarehouse */
		$contactWarehouse = $this->contactWarehouseRepository->findWithoutFail($id);

		if (empty($contactWarehouse)) {
			return $this->sendError('Contact Warehouse not found');
		}

		$contactWarehouse = $this->contactWarehouseRepository->update($input, $id);

		//erp_id debe quedar nulo
		Contact :: where('id' , $request->contact_id)->update(['erp_id'=>null]);

		return $this->sendResponse($contactWarehouse, 'ContactWarehouse updated successfully');
	}

    /**
     * Remove the specified ContactWarehouse from storage.
     * DELETE /contactWarehouses/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     */
	public function destroy($id)
	{
		/** @var ContactWarehouse $contactWarehouse */
		$contactWarehouse = $this->contactWarehouseRepository->delete($id);

		if (!$contactWarehouse['status']) {
			return $this->sendError($contactWarehouse['message']);
		}

		return $this->sendResponse($id, 'Contact Warehouse deleted successfully');
	}


    /**
     * @param Request $request
     * @return mixed
     */
    public function findForEmployeeType(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $user = User::where('id', $user->id)->with(['getRoles'])->first();

        try {

            $warehouses = $this->contactWarehouseRepository->findCustomersByType($request, $user);
            $data = $warehouses->toArray();
            //$data = sort($data, 'description');
            //ddd($data);

            // Si los suario son de Duitama o Ibague se cambia si branchoffice a BOGOTA
            $redirectBranchoffice_id = [
                '288', // Duitama
                '284', // Ibague
            ];


            if (in_array($request->branchoffice_id, $redirectBranchoffice_id)) {
                // Traemo la SEDE de bogota.
                $branchOffice = BranchOffice::find(305);

                foreach ($data['data'] as $key => $warehouse) {
                    $data['data'][$key]['branchoffice_id'] = $brloganchOffice->id;
                    $data['data'][$key]['branchoffice'] = $branchOffice;
                }
            }

            return $this->sendResponse($data, 'Warehouses retrieved successfully');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError('Error al ejecutar la consulta', 404);
        }
    }

	public function findCustomersNotPaginate(Request $request)
	{
		$user = JWTAuth::parseToken()->toUser();

		$user = User::where('id', $user->id)->with(['getRoles'])->first();

		$warehouses = $this->contactWarehouseRepository->findCustomersNotPaginate($request, $user);

		return $this->sendResponse($warehouses, 'Warehouses retrieved successfully');
	}

	/**
	 * Display a listing of the ContactWarehouse.
	 * GET|HEAD /contactWarehouses
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{

		$contactsWarehouses = ContactWarehouse::with([
			'seller',
			'contact',
			'retFueAccount',
			'retIvaAccount',
			'retIcaAccount',
			'retFueProviderAccount',
			'retIvaProviderAccount',
			'retIcaProviderAccount',
			'retFueServAccount',
			'retIcaServAccount',
			'retIvaServAccount',
			'retFueServProviderAccount',
			'retIcaServProviderAccount',
			'retIvaServProviderAccount'
		])
		->where(['contact_id' => $request->contact_id])
		->get();
		return $this->sendResponse($contactsWarehouses, 'Contact Warehouses retrieved successfully');

		/*$contactsWarehouses = ContactWarehouse::when($request->typeUser == 'c', function ($query) use ($request) {
            $query->where(['contact_id' => $request->contact_id]);
        })
            ->when($request->typeUser == 'e' && $request->role == 'Vendedor', function ($query) use ($request) {
                $query->where(['seller_id' => $request->contact_id]);
            })
            ->get()->pluck('id');
        return $this->sendResponse($contactsWarehouses, 'Contact Warehouses retrieved successfully');*/
	}




	/**
	 * Display the specified ContactWarehouse.
	 * GET|HEAD /contactWarehouses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var ContactWarehouse $contactWarehouse */
		$contactWarehouse = $this->contactWarehouseRepository->searchBiId($id);

		if (empty($contactWarehouse)) {
			return $this->sendError('Contact Warehouse not found');
		}

		return $this->sendResponse($contactWarehouse->toArray(), 'Contact Warehouse retrieved successfully');
	}


	public function getByIdArray(Request $request)
	{
		$contactW = ContactWarehouse::when(count($request->warehouse) > 0, function ($query) use ($request) {
			$query->whereIn('id', $request->warehouse);
		})->get();
		return $contactW;
	}

	public function uploadExcel(Request $request)
	{
		$this->contactWarehouseRepository->pushCriteria(new RequestCriteria($request));
		$this->contactWarehouseRepository->pushCriteria(new LimitOffsetCriteria($request));
		$warehouses = $this->contactWarehouseRepository->uploadExcel($request);

		return $warehouses;
	}

	public function saveConfigurationRetention(Request $request)
	{
		$response = $this->contactWarehouseService->saveConfigurationRetention($request);
		return response()->json($response['data'], $response['code']);
	}

	public function getIdContactsWarehouses(Request $request)
	{
		$contactsWarehouses = ContactWarehouse::when($request->typeUser == 'c', function ($query) use ($request) {
			$query->where(['contact_id' => $request->contact_id]);
		})
			->when($request->typeUser == 'e' && $request->role == 'Vendedor', function ($query) use ($request) {
				$query->where(['seller_id' => $request->contact_id]);
			})
			->get()->pluck('id');
		return $this->sendResponse($contactsWarehouses, 'Contact Warehouses retrieved successfully');
	}

	/**
	 * busca correos para envial pdf
	 *
	 * @author santiago torres | kevin galindo
	 */
	public function searchmail(Request $request)
	{
		$mailbyidWarehouse = ContactWarehouse::select('id', 'email')->where('contact_id', $request->contact)->get();
		$mailbyidcontact = Contact::select('id', 'email')->where('id', $request->contact)->get();
		$mailsWarehouse = ContactWarehouse::select('id', 'email')->where('email', 'ILIKE', '%' . $request->filter . '%')->get();
		$mailscontact = Contact::select('id', 'email')->where('email', 'ILIKE', '%' . $request->filter . '%')->get();

		$mails = [];

		foreach ($mailbyidWarehouse as $key => $item) {
			if ($item->email) {
				$mails[] = $item;
			}
		}
		foreach ($mailbyidcontact as $key => $item) {
			if ($item->email) {
				$mails[] = $item;
			}
		}
		foreach ($mailsWarehouse as $key => $item) {
			if ($item->email) {
				$mails[] = $item;
			}
		}
		foreach ($mailscontact as $key => $item) {
			if ($item->email) {
				$mails[] = $item;
			}
		}
		return $this->sendResponse($mails, 'mails retrieved successfully');
	}

	/**
	 * actualizar el correo de un contact
	 *
	 * @author santiago torres | kevin galindo
	 */

	public function updateEmail(Request $request)
	{
		return $this->contactWarehouseRepository->updateeMail($request);
	}

	/**
	 * listar proveedores
	 *
	 * @santiago torres | kevin galindo
	 */
	public function getProviders(Request $request)
	{
		return $this->contactWarehouseRepository->searchProviders($request);;
	}

	/**
	 * Este metodfo se encarga de cargar los porcentaje de los
	 * terceros de forma masiva
	 *
	 * @author Kevin Galindo
	 */
	public function loadRetentionMasiveCsv(Request $request)
	{
		//Recibimos la información
		$file = $request->file('archivo_importar');
		$fileData = $this->csvToArray($file->getRealPath());
		$countLoad = 0;

		try {
			DB::beginTransaction();

			// Recorremos el arreglo y creamos los registros
			foreach ($fileData as $key => $value) {

				// Buscamos el cliente
				$contact = Contact::where('identification', trim($value['NIT']))->first();

				// Validamos si existe y triene sucursal
				if ($contact && $contact->warehouses) {
					// Recorremos las sucursales
					foreach ($contact->warehouses as $key => $warehouse) {

						// Validamos el tipo de retencion.
						if ($value['TIPO'] == 'ICA') {
							$warehouse->ret_ica_prod_percentage = $value['PORCENTAJE'];
						}

						if ($value['TIPO'] == 'IVA') {
							$warehouse->ret_iva_prod_percentage = $value['PORCENTAJE'];
						}

						if ($value['TIPO'] == 'FUE') {
							$warehouse->ret_fue_prod_percentage = $value['PORCENTAJE'];
						}

						// Guardamos cambios
						$warehouse->save();

						$countLoad++;
					}
				}
			}

			DB::commit();
		} catch (\Exception $e) {
			DB::rollBack();
			return 'Error: ' . $e->getMessage() . " File:" . $e->getFile() . " Line:" . $e->getLine();
		}

		return response()->json('Registros creatos, cantidad: ' . $countLoad, 200);
	}

	/**
	 * Esta funcion se encarga de pasar un archivo csv a un array
	 * segun el tipo de limitador.
	 *
	 * @return array
	 * @param string $filename
	 * @param string $delimiter
	 * @author Kevin Galindo
	 */
	function csvToArray($filename = '', $delimiter = ';')
	{
		//Valida si el archivo existe y si se puede abrir.
		if (!file_exists($filename) || !is_readable($filename)) return false;

		//Creamos las variables de la cabecera y la data que tendra el array.
		$header = [];
		$data = [];

		//Recorremos el archivo y acomodamos la data.
		if (($handle = fopen($filename, 'r')) !== false) {

			while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
				if (!$header) {
					// Elimina los caracteres no válidos u ocultos
					$row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
					$row = preg_replace('!\s+!', '',  $row);
					$header = $row;
				} else {
					$data[] = array_combine($header, $row);
				}
			}

			fclose($handle);
		}

		if (count($data[1]) <= 1) {
			return false;
		}

		//Retornamos la informacion.
		return $data;
	}


	public function massReassignSeller(Request $request)
	{
		try {
			//  cambio el erp_id del cliente de cada una de las bodegas
			$contactWarehouses = ContactWarehouse::where('seller_id', $request->incoming_seller)->with('contactt')->get();
			$contactWarehouses->map(function ($contactw, $key) {
				$contactw->contactt->erp_id = null;
				$contactw->contactt->save();
			});

			// cambio el vendedor a cada bodega
			ContactWarehouse::where('seller_id', $request->incoming_seller)
				->update(['seller_id' => $request->outgoing_seller]);

			return response()->json('actualización realizada correctamente', 200);
		} catch (\Throwable $th) {
			return $th->getMessage();
		}
	}
	public function getContactWarehouse(Request $request)
	{
		return response()->json($this->contactWarehouseRepository->getContactWarehouse($request), 200);
		return $this->contactWarehouseRepository->getContactWarehouse($request);
	}
}
