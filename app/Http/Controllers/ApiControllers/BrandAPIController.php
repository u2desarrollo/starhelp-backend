<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use App\Entities\Brand;
use Illuminate\Http\Request;
use App\Traits\LoadImageTrait;
use App\Repositories\BrandRepository;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateBrandAPIRequest;
use App\Http\Requests\ApiRequests\UpdateBrandAPIRequest;

/**
 * Class BrandController
 * @package App\Http\Controllers\ApiControllers
 */

class BrandAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  BrandRepository */
    private $brandRepository;

    public function __construct(BrandRepository $brandRepo)
    {
        $this->brandRepository = $brandRepo;
    }

    /**
     * Display a listing of the Brand.
     * GET|HEAD /brands
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->brandRepository->pushCriteria(new RequestCriteria($request));
        $this->brandRepository->pushCriteria(new LimitOffsetCriteria($request));
        $brands = $this->brandRepository->findAll($request);
        // $brands = $this->brandRepository->all();

        return $this->sendResponse($brands->toArray(), 'Brands retrieved successfully');
    }

    /**
     * Store a newly created Brand in storage.
     * POST /brands
     *
     * @param CreateBrandAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBrandAPIRequest $request)
    {
        $input = $request->except('image');

        if ($request['image'] != '') {

            $imagen = $this->saveImage($request->image, 'brands');

            $ext = $this->getExtension($imagen);

            $fileName = str_random() . '.' . $ext;

            $path = storage_path() . '/app/public/brands/thumbnails/' . $fileName;
            $thumbnail = Image::make(storage_path() . '/app/public/' . $imagen)
                ->resize(53, 42, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save($path);

            $input['thumbnails'] = $fileName;
            $input['image'] = $imagen;
        }

        $brands = $this->brandRepository->create($input);

        return $this->sendResponse($brands->toArray(), 'Brand saved successfully');
    }

    /**
     * Display the specified Brand.
     * GET|HEAD /brands/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Brand $brand */
        $brand = $this->brandRepository->findForId($id);

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        return $this->sendResponse($brand->toArray(), 'Brand retrieved successfully');
    }

    /**
     * Update the specified Brand in storage.
     * PUT/PATCH /brands/{id}
     *
     * @param  int $id
     * @param UpdateBrandAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBrandAPIRequest $request)
    {
        $input = $request->except('image');

        /** @var Brand $brand */
        $brand = $this->brandRepository->findForId($id);

        if ($brand->image != $request['image']) {
            $imagen = $this->saveImage($request->image, 'brands');

            $ext = $this->getExtension($imagen);

            $fileName = str_random() . '.' . $ext;
            $path = storage_path() . '/app/public/brands/thumbnails/' . $fileName;
            $thumbnail = Image::make(storage_path() . '/app/public/' . $imagen)
                ->resize(53, 42, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save($path);

            $input['thumbnails'] = $fileName;
            $input['image'] = $imagen;
        }

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        $brand = $this->brandRepository->update($input, $id);

        return $this->sendResponse($brand->toArray(), 'Brand updated successfully');
    }

    /**
     * Remove the specified Brand from storage.
     * DELETE /brands/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Brand $brand */
        $brand = $this->brandRepository->findWithoutFail($id);

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        $brand->delete();

        return $this->sendResponse($id, 'Brand deleted successfully');
    }
    public function getBrands()
    {
        $brands = Brand::all("id", "description");
        return $brands;
    }


    /**
     * Este metodo retorna todas las marcas sin agregar ningun filtro
     *
     * @author kevin galindo
     */
    public function brandList()
    {
        $brandList = Brand::select('id', 'brand_code', 'description')->orderBy('description', 'asc')->get();
        return response()->json($brandList, 200);
    }
}