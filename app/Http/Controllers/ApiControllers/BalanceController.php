<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateBalancesJob;
use App\Services\BalancesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    /**
     * @var BalancesService
     */
    private $balanceService;

    /**
     * BalanceController constructor.
     * @param BalancesService $balancesService
     */
    public function __construct(BalancesService $balancesService)
    {
        $this->balanceService = $balancesService;
    }

    /**
     * Actualizar saldos desde un año mes especifico
     *
     * @param Request $request Contiene el año mes desde el que se quieren actualizar los saldos
     * @return JsonResponse Contiene mensaje de respuesta
     * @author Jhon García
     */
    public function updateBalances(Request $request)
    {
        ini_set('max_execution_time', '1200');

        $user = auth()->user();

        UpdateBalancesJob::dispatch($request->year_month, null, false, $user->id)->onQueue('high');

        return response()->json('Actualización de saldos exitosa', 200);
    }

    public function searchWallet(Request $request)
    {
        return $wallet = $this->balanceService->searchWallet($request);
    }

    public function searchAccount(Request $request)
    {
        return $wallet = $this->balanceService->searchAccount($request);
    }

    public function getBalanceContact(Request $request,$id_account)
    {

        $contact_balance = $this->balanceService->getBalanceContact($request,$id_account);
        return $contact_balance;
    }

    public function getBalanceDocument(Request $request,$id_contact,$id_account)
    {

        $contact_balance = $this->balanceService->getBalanceDocument($request,$id_contact,$id_account);
        return $contact_balance;
    }

    public function getBalanceDocumentContactAccount(Request $request,$id_contact,$id_account)
    {

        $contact_balance = $this->balanceService->getBalanceDocumentContactAccount($request,$id_contact,$id_account);
        return $contact_balance;
    }

    public function getMovementsAccountContact(Request $request)
    {
        $response = $this->balanceService->getMovementsAccountContact($request);
        return response()->json([
            'data' => $response['data']
        ], 200);
    }
}
