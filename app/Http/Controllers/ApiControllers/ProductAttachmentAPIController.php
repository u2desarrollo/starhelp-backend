<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Product;
use App\Http\Requests\ApiRequests\CreateProductAttachmentAPIRequest;
use App\Http\Requests\ApiRequests\UpdateProductAttachmentAPIRequest;
use App\Entities\ProductAttachment;
use App\Http\Requests\CreateProductAttachmentImagesAPIRequest;
use App\Repositories\ProductAttachmentRepository;
use App\Services\ProductAttachmentService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Intervention\Image\Facades\Image;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;
use Response;

/**
 * Class ProductAttachmentController
 *
 * @package App\Http\Controllers\ApiControllers
 */
class ProductAttachmentAPIController extends AppBaseController
{
    /** @var  ProductAttachmentRepository */
    private $productAttachmentRepository;
    private $product_attachment_service;

    /**
     * ProductAttachmentAPIController constructor.
     *
     * @param ProductAttachmentRepository $productAttachmentRepo
     * @param ProductAttachmentService $product_attachment_service
     */
    public function __construct(ProductAttachmentRepository $productAttachmentRepo, ProductAttachmentService $product_attachment_service)
    {
        $this->productAttachmentRepository = $productAttachmentRepo;
        $this->product_attachment_service = $product_attachment_service;
    }

    /**
     * Display a listing of the ProductAttachment.
     * GET|HEAD /productAttachments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $productAttachments = $this->productAttachmentRepository->all();

        return $this->sendResponse($productAttachments->toArray(), 'Product Attachments retrieved successfully');
    }

    /**
     * Store a newly created ProductAttachment in storage.
     * POST /productAttachments
     *
     * @param CreateProductAttachmentAPIRequest $request
     * @return Response
     */
    public function store(CreateProductAttachmentAPIRequest $request)
    {
        $input = $request->all();

        $productAttachments = $this->productAttachmentRepository->create($input);

        return $this->sendResponse($productAttachments->toArray(), 'Product Attachment saved successfully');
    }

    /**
     * Display the specified ProductAttachment.
     * GET|HEAD /productAttachments/{id}
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductAttachment $productAttachment */
        $productAttachment = $this->productAttachmentRepository->findWithoutFail($id);

        if (empty($productAttachment)) {
            return $this->sendError('Product Attachment not found');
        }

        return $this->sendResponse($productAttachment->toArray(), 'Product Attachment retrieved successfully');
    }

    /**
     * Update the specified ProductAttachment in storage.
     * PUT/PATCH /productAttachments/{id}
     *
     * @param int $id
     * @param UpdateProductAttachmentAPIRequest $request
     * @return Response
     */
    public function update($id, UpdateProductAttachmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductAttachment $productAttachment */
        $productAttachment = $this->productAttachmentRepository->findWithoutFail($id);

        if (empty($productAttachment)) {
            return $this->sendError('Product Attachment not found');
        }

        $productAttachment = $this->productAttachmentRepository->update($input, $id);

        return $this->sendResponse($productAttachment->toArray(), 'ProductAttachment updated successfully');
    }

    /**
     * Remove the specified ProductAttachment from storage.
     * DELETE /productAttachments/{id}
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductAttachment $productAttachment */
        $productAttachment = $this->productAttachmentRepository->findWithoutFail($id);

        if (empty($productAttachment)) {
            return $this->sendError('Product Attachment not found');
        }

        $productAttachment->delete();

        return $this->sendResponse($id, 'Product Attachment deleted successfully');
    }

    /**
     * Guarda y redimensiona una imagen asociada a un producto
     * @param Request $request Contiene la imagen a almacenar
     * @param Product $product Contiene la información del producto
     * @return mixed Contiene el mensaje de respuesta
     * @author Jhon garcía
     */
    public function storeImages(Request $request, Product $product)
    {
        try {

            // Validar que la imagen cumpla las condiciones
            $validator = Validator::make($request->all(), [
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            // Validar si hay errores
            if ($validator->fails()) {
                $this->sendError('Error al amacenar imagen', 400);
            }

            // Llamar el método que se encarga de almacenar la imagen
            $product_attachment = $this->product_attachment_service->storeImages($request, $product->id);

            return $this->sendResponse($product_attachment, 'Product Attachment saved successfully');

        } catch (\Exception $exception) {
            return $this->sendError('Error al amacenar imagen', 400);
        }

    }

    /**
     * Obtiene las imagenes asociadas a un producto
     *
     * @param Product $product Contiene la información del producto
     * @return mixed Contiene las imagenes del producto
     * @author Jhon garcía
     */
    public function getImages(Product $product)
    {
        try {
            $product_attachments = $this->productAttachmentRepository->getImagesFromProductId($product);

            return $this->sendResponse($product_attachments->toArray(), 'Images obtained successfully');

        } catch (\Exception $exception) {
            return $this->sendError('Error getting images', 400);
        }

    }

    /**
     * Eliminar imagen
     *
     * @param ProductAttachment $product_attachment Contiene la información de la imagen a eliminar
     * @return mixed Contiene el mensaje de respuesta
     * @author Jhon garcía
     */
    public function deleteImage(ProductAttachment $product_attachment)
    {
        try {
            $product_attachment->delete();

            return $this->sendResponse(null, 'Image deleted successfully');

        } catch (\Exception $exception) {
            return $this->sendError('Error deleting image', 400);
        }

    }

    /**
     * Establecer imagen como principal
     *
     * @param ProductAttachment $product_attachment
     * @return mixed Contiene las imagenes del producto
     * @author Jhon garcía
     */
    public function setMainToImage(ProductAttachment $product_attachment)
    {
        try {
            $this->productAttachmentRepository->setMainToImage($product_attachment);

            return $this->sendResponse(null, 'Image updated successfully');

        } catch (\Exception $exception) {
            return $this->sendError('Error updating image', 400);
        }

    }
}
