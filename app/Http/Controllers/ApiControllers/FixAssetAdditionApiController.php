<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\FixAssetAdditionRepository;
// use App\Services\FixAssetAdditionService;

class FixAssetAdditionApiController extends Controller
{

    private $fixAssetAdditionRepository;
    // private $fixAssetAdditionService;

    public function __construct(
        FixAssetAdditionRepository $fixAssetAdditionRepository
        // FixAssetAdditionService $fixAssetAdditionService
    )
    {
        $this->fixAssetAdditionRepository = $fixAssetAdditionRepository;
        // $this->fixAssetAdditionService = $fixAssetAdditionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = $this->fixAssetAdditionRepository->findAll($request);
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->fixAssetAdditionRepository->create($request->all());
        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->fixAssetAdditionRepository->findById($id);
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->fixAssetAdditionRepository->update($request->all(), $id);
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fix = $this->fixAssetAdditionRepository->findById($id);
        $fix->delete();
        return response()->json($fix, 200);
    }
}
