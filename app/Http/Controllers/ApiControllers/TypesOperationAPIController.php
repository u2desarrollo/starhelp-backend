<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateTypesOperationAPIRequest;
use App\Http\Requests\ApiRequests\UpdateTypesOperationAPIRequest;
use App\Entities\TypesOperation;
use App\Repositories\TypesOperationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypesOperationController
 * @package App\Http\Controllers\ApiControllers
 */

class TypesOperationAPIController extends AppBaseController
{
    /** @var  TypesOperationRepository */
    private $typesOperationRepository;

    public function __construct(TypesOperationRepository $typesOperationRepo)
    {
        $this->typesOperationRepository = $typesOperationRepo;
    }

    /**
     * Display a listing of the TypesOperation.
     * GET|HEAD /typesOperations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typesOperations = $this->typesOperationRepository->findAll($request);
        return $this->sendResponse($typesOperations->toArray(), 'Types Operations retrieved successfully');
    }

    /**
     * Store a newly created TypesOperation in storage.
     * POST /typesOperations
     *
     * @param CreateTypesOperationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypesOperationAPIRequest $request)
    {

        $input = $request->all();

        $typesOperation = $this->typesOperationRepository->create($input);

        return $this->sendResponse($typesOperation->toArray(), 'Types Operation saved successfully');
    }

    /**
     * Display the specified TypesOperation.
     * GET|HEAD /typesOperations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypesOperation $typesOperation */
        $typesOperation = $this->typesOperationRepository->findWithoutFail($id);

        if (empty($typesOperation)) {
            return $this->sendError('Types Operation not found');
        }

        return $this->sendResponse($typesOperation->toArray(), 'Types Operation retrieved successfully');
    }

    /**
     * Update the specified TypesOperation in storage.
     * PUT/PATCH /typesOperations/{id}
     *
     * @param  int $id
     * @param UpdateTypesOperationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypesOperationAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypesOperation $typesOperation */
        $typesOperation = $this->typesOperationRepository->findWithoutFail($id);

        if (empty($typesOperation)) {
            return $this->sendError('Types Operation not found');
        }

        $typesOperation = $this->typesOperationRepository->update($input, $id);

        return $this->sendResponse($typesOperation->toArray(), 'TypesOperation updated successfully');
    }

    /**
     * Remove the specified TypesOperation from storage.
     * DELETE /typesOperations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypesOperation $typesOperation */
        $typesOperation = $this->typesOperationRepository->findById($id);

        if (empty($typesOperation)) {
            return $this->sendError('Types Operation not found');
        }

        $typesOperation->delete();

        return $this->sendResponse($id, 'Types Operation deleted successfully');
    }
}
