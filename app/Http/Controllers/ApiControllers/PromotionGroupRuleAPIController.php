<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePromotionGroupRuleAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePromotionGroupRuleAPIRequest;
use App\Entities\PromotionGroupRule;
use App\Repositories\PromotionGroupRuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromotionGroupRuleController
 * @package App\Http\Controllers\ApiControllers
 */

class PromotionGroupRuleAPIController extends AppBaseController
{
    /** @var  PromotionGroupRuleRepository */
    private $promotionGroupRuleRepository;

    public function __construct(PromotionGroupRuleRepository $promotionGroupRuleRepo)
    {
        $this->promotionGroupRuleRepository = $promotionGroupRuleRepo;
    }

    /**
     * Display a listing of the PromotionGroupRule.
     * GET|HEAD /promotionGroupRules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->promotionGroupRuleRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionGroupRuleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotionGroupRules = $this->promotionGroupRuleRepository->all();

        return $this->sendResponse($promotionGroupRules->toArray(), 'Promotion Group Rules retrieved successfully');
    }

    /**
     * Store a newly created PromotionGroupRule in storage.
     * POST /promotionGroupRules
     *
     * @param CreatePromotionGroupRuleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePromotionGroupRuleAPIRequest $request)
    {
        $input = $request->all();

        $promotionGroupRules = $this->promotionGroupRuleRepository->create($input);

        return $this->sendResponse($promotionGroupRules->toArray(), 'Promotion Group Rule saved successfully');
    }

    /**
     * Display the specified PromotionGroupRule.
     * GET|HEAD /promotionGroupRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PromotionGroupRule $promotionGroupRule */
        $promotionGroupRule = $this->promotionGroupRuleRepository->findWithoutFail($id);

        if (empty($promotionGroupRule)) {
            return $this->sendError('Promotion Group Rule not found');
        }

        return $this->sendResponse($promotionGroupRule->toArray(), 'Promotion Group Rule retrieved successfully');
    }

    /**
     * Update the specified PromotionGroupRule in storage.
     * PUT/PATCH /promotionGroupRules/{id}
     *
     * @param  int $id
     * @param UpdatePromotionGroupRuleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromotionGroupRuleAPIRequest $request)
    {
        $input = $request->all();

        /** @var PromotionGroupRule $promotionGroupRule */
        $promotionGroupRule = $this->promotionGroupRuleRepository->findWithoutFail($id);

        if (empty($promotionGroupRule)) {
            return $this->sendError('Promotion Group Rule not found');
        }

        $promotionGroupRule = $this->promotionGroupRuleRepository->update($input, $id);

        return $this->sendResponse($promotionGroupRule->toArray(), 'PromotionGroupRule updated successfully');
    }

    /**
     * Remove the specified PromotionGroupRule from storage.
     * DELETE /promotionGroupRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PromotionGroupRule $promotionGroupRule */
        $promotionGroupRule = $this->promotionGroupRuleRepository->findWithoutFail($id);

        if (empty($promotionGroupRule)) {
            return $this->sendError('Promotion Group Rule not found');
        }

        $promotionGroupRule->delete();

        return $this->sendResponse($id, 'Promotion Group Rule deleted successfully');
    }
}
