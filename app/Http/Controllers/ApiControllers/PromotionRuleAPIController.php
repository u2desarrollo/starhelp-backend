<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePromotionRuleAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePromotionRuleAPIRequest;
use App\Entities\PromotionRule;
use App\Repositories\PromotionRuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromotionRuleController
 * @package App\Http\Controllers\ApiControllers
 */

class PromotionRuleAPIController extends AppBaseController
{
    /** @var  PromotionRuleRepository */
    private $promotionRuleRepository;

    public function __construct(PromotionRuleRepository $promotionRuleRepo)
    {
        $this->promotionRuleRepository = $promotionRuleRepo;
    }

    /**
     * Display a listing of the PromotionRule.
     * GET|HEAD /promotionRules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->promotionRuleRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionRuleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotionRules = $this->promotionRuleRepository->all();

        return $this->sendResponse($promotionRules->toArray(), 'Promotion Rules retrieved successfully');
    }

    /**
     * Store a newly created PromotionRule in storage.
     * POST /promotionRules
     *
     * @param CreatePromotionRuleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePromotionRuleAPIRequest $request)
    {
        $input = $request->all();

        $promotionRules = $this->promotionRuleRepository->create($input);

        return $this->sendResponse($promotionRules->toArray(), 'Promotion Rule saved successfully');
    }

    /**
     * Display the specified PromotionRule.
     * GET|HEAD /promotionRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PromotionRule $promotionRule */
        $promotionRule = $this->promotionRuleRepository->findWithoutFail($id);

        if (empty($promotionRule)) {
            return $this->sendError('Promotion Rule not found');
        }

        return $this->sendResponse($promotionRule->toArray(), 'Promotion Rule retrieved successfully');
    }

    /**
     * Update the specified PromotionRule in storage.
     * PUT/PATCH /promotionRules/{id}
     *
     * @param  int $id
     * @param UpdatePromotionRuleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromotionRuleAPIRequest $request)
    {
        $input = $request->all();

        /** @var PromotionRule $promotionRule */
        $promotionRule = $this->promotionRuleRepository->findWithoutFail($id);

        if (empty($promotionRule)) {
            return $this->sendError('Promotion Rule not found');
        }

        $promotionRule = $this->promotionRuleRepository->update($input, $id);

        return $this->sendResponse($promotionRule->toArray(), 'PromotionRule updated successfully');
    }

    /**
     * Remove the specified PromotionRule from storage.
     * DELETE /promotionRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PromotionRule $promotionRule */
        $promotionRule = $this->promotionRuleRepository->findWithoutFail($id);

        if (empty($promotionRule)) {
            return $this->sendError('Promotion Rule not found');
        }

        $promotionRule->delete();

        return $this->sendResponse($id, 'Promotion Rule deleted successfully');
    }
}
