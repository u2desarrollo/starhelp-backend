<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

use App\Repositories\InventoryRepository;
use App\Repositories\InventoryxRepository;
use App\Repositories\BranchOfficeWarehouseRepository;
use App\Repositories\DocumentProductRepository;
use App\Repositories\DocumentRepository;

use App\Services\InventoryService;

/**
 * Class InventoryController
 * @package App\Http\Controllers\ApiControllers
 */

class InventoryxController extends AppBaseController
{
    /** @var  InventoryRepository */
    private $branchOfficeWarehouseRepository;
    private $inventoryxRepository;
    private $inventoryRepository;
    private $documentProductRepository;
    private $documentRepository;
    private $inventoryService;

    public function __construct(
        InventoryxRepository $inventoryxRepo,
        InventoryRepository $inventoryRepo,
        BranchOfficeWarehouseRepository $branchOfficeWarehouseRepo,
        DocumentProductRepository $DocumentProductRepository,
        DocumentRepository $documentRepository,
        InventoryService $inventoryService
    )
    {
            $this->inventoryxRepository = $inventoryxRepo;
            $this->inventoryRepository = $inventoryRepo;
            $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepo;
            $this->documentProductRepository = $DocumentProductRepository;
            $this->documentRepository = $documentRepository;
            $this->inventoryService = $inventoryService;
        }

    /**
     * 
     */
    public function fixValues(Request $request)
    {
        //limpiar la tabla inventoriesx
        DB::table('inventoriesx')->truncate();

        //lista de bodegas
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->allBranchOfficeWarehouse();

        //Obtener los valores stock y stock_values para inventoryx
        $branchOfficeWarehouses->map(function ($item, $key) use ($request, $branchOfficeWarehouses){
            $item->product_id = $request->product;
            $item->result = $this->documentProductRepository->getStockAndStockValues($request, $item->branchoffice_warehouse_id, $item->product_id);
            if ($item->result) {
                $item->stock = $item->result->stock_after_operation ;
                $item->stock_values = $item->result->stock_value_after_operation ;
                $item->average_cost = $item->stock_values/$item->stock;
                unset($item->result);
            }else{
                unset($branchOfficeWarehouses[$key]);
            }
        });

        //crea los registros de las bodegas en la tabla inventoryx
        $this->inventoryxRepository->create($branchOfficeWarehouses->toArray());
        
        //lista de documentos para reconstruir
        $documentsProductsReconstructor = $this->documentProductRepository->searchDocumentsProductsReconstructor($request);

        // valorizar los documents product y actualizar inventoryx
        // $inventoryx =$documentsProductsReconstructor->map(function($item, $key){
        //     return $this->inventoryxRepository->finalize($item->id);
        // });


        $inventoryx = [];
        foreach ($documentsProductsReconstructor as $key => $item) {
            $inventoryx []= $this->inventoryxRepository->finalize($item->id);
        }


        //actualizar inventory con la data de inventoryx

        $inventoryx =  $this->inventoryxRepository->getinventoryforupdate();
        //return $inventoryx;
        foreach ($inventoryx as $key => $value) {
            $data=[
                'branchoffice_warehouse_id'=>$value->branchoffice_warehouse_id,
                'product_id'=>$value->product_id,
                'stock'=>$value->stock,
                'stock_values'=>$value->stock_values,
                'average_cost'=>$value->average_cost,
            ];
            $this->inventoryRepository->updateInventoryFromInventoryx($data);
        }
        
    }

    /**
     * proceso para reconstruir el inventario de uno o mas productos
     * 
     * @author santiago torres
     */
    public function fixGlobalValues(Request $request)
    {
        set_time_limit(0);
        //limpiar la tabla inventoriesx

        // validamos si se en el request se envio un product
        // si no se envio se hace valoriza  la tabla inventory todos los productos en cero
        if (!$request->product) {
            // DB::table('inventories')->truncate();
            $set_values_inventory_0 =  $this->inventoryRepository->updateAllValuesTo0();
        }
        DB::table('inventoriesx')->truncate();

        //lista de bodegas
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->allBranchOfficeWarehouse();

        //productos que se reconstruira el inventario
        if (!$request->product) {
            $list_products= $this->documentProductRepository->getProductFromDocuments();
            $products=[];
            foreach ($list_products as $key => $product) {
                $products[]=$product->product_id;
            }
        }else{
            // Si se envia producto desde el request solo se reconstruye ese producto
            $products=[$request->product];
        }

        // se crea variable de con los documentos usados para los valores iniciales de inventoryx
        $valoresiniciales =[];
        $idDocumentsValues=[];
        

        // Si se envia fecha desde el request 
        //se toma el documento anterior a la fecha para crear valores iniciales en inventoryx  
        if ($request->date) {

            // recorremos el arrat de productos a recontruir
            foreach ($products as $key => $product) {
    
                //valuesByBranchoffice almacena los la info que se almacenara en inventoryx
                $valuesByBranchoffice =[];

                // recorremos la lista de bodegas
                foreach ($branchOfficeWarehouses as $key => $item) {

                    // consultamos los 
                    // valores iniciales
                    $valuesByBranchoffice [$key]=[
                        'result' => $this->documentRepository->getStockAndStockValues($request, $item->branchoffice_warehouse_id, $product)
                    ];

                    // si no hay valores para el producto en la bodega se elimina el item
                    if ($valuesByBranchoffice[$key]['result'] == null) {
                        unset($valuesByBranchoffice[$key]);
                    }else{
                        // $idDocumentsValues almacena los ids de los documentos que usamos para los valores iniciales
                        $idDocumentsValues[] = $valuesByBranchoffice[$key]['result']->id;

                        // se almacena info del productos que si tiene valores en la bodega bodega
                        $valuesByBranchoffice [$key]=[
                            "stock" => $valuesByBranchoffice[$key]['result']->documentsProducts[0]->stock_after_operation,
                            "stock_values" => $valuesByBranchoffice[$key]['result']->documentsProducts[0]->stock_value_after_operation,
                            "average_cost" => $valuesByBranchoffice[$key]['result']->documentsProducts[0]->stock_value_after_operation/$valuesByBranchoffice[$key]['result']->documentsProducts[0]->stock_after_operation,
                            'product_id' => $product,
                            'branchoffice_warehouse_id' => $item->branchoffice_warehouse_id,
                        ];
                    }
                }
                //crea los registros de las bodegas en la tabla inventoryx
                $valoresiniciales=$this->inventoryxRepository->create($valuesByBranchoffice); 
                
            }
        }

        //lista de documentos para reconstruir
        $documentsProductsReconstructor = $this->documentRepository->searchDocumentsProductsReconstructor($request, $products, $idDocumentsValues);

        //almacena la info que se va a reconstruir en inventoryx
        $inventoryxF = []; 

        // recorremos los documentos y calculamos los saldos en unidades y valores
        foreach ($documentsProductsReconstructor as $key => $document) {
            foreach ($document->documentsProducts as $key => $documentProduct) {
                $inventoryxF[] = $this->inventoryxRepository->finalize($documentProduct->id);
            }
        }

        // obtenemos la info de inventoryx para pasarla a inventory
        $inventoryx =  $this->inventoryxRepository->getinventoryforupdate();

        $inventory=[];
        //recorremos la info de inventoryx para almacenarla en inventory
        foreach ($inventoryx as $key => $value) {
            $inventory[]=$this->inventoryRepository->updateInventoryFromInventoryx($value);
        }

        
        return [
            'products'                          =>     $products,
            'branchOfficeWarehouses'            =>     $branchOfficeWarehouses,
            'valoresiniciales'                  =>     $valoresiniciales,
            'idDocumentsValues'                 =>     $idDocumentsValues,
            'documentsProductsReconstructor'    =>     $documentsProductsReconstructor,
            'inventoryxF'                       =>     $inventoryxF,
            'inventoryx'                        =>     count($inventoryx),
            'inventory'                         =>     count($inventory),
        ];
    }

    public function rebuildDocument($document_id)
    {
        $response = $this->inventoryService->rebuildDocument($document_id);
        return response()->json($response->data, $response->code);
    }
}
