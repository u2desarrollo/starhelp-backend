<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateCityAPIRequest;
use App\Http\Requests\ApiRequests\UpdateCityAPIRequest;
use App\Entities\City;
use App\Repositories\CityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CityController
 * @package App\Http\Controllers\ApiControllers
 */

class CityAPIController extends AppBaseController
{
    /** @var  CityRepository */
    private $cityRepository;

    public function __construct(CityRepository $cityRepo)
    {
        $this->cityRepository = $cityRepo;
    }

    /**
     * @param Request $request
     * @return Response

     */
    public function index(Request $request)
    {
        $this->cityRepository->pushCriteria(new RequestCriteria($request));
        $this->cityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ci = $this->cityRepository->all();

        if ($request->department_id) {
            $cities = $ci->filter(function ($value) use ($request){
                if ((integer) $value->department_id == $request->department_id){
                    return $value;
                }
            })->values();
        }else {
            $cities = $ci;
        }

        return $this->sendResponse($cities->toArray(), 'Cities retrieved successfully');
    }



    /**
     * @param int $id
     * @return Response

     */
    public function show($id)
    {
        /** @var City $city */
        $city = $this->cityRepository->findWithoutFail($id);

        if (empty($city)) {
            return $this->sendError('City not found');
        }

        return $this->sendResponse($city->toArray(), 'City retrieved successfully');
    }

    public function cities(Request $request)
    {
        return $this->sendResponse( $this->cityRepository->cities_departaments($request) , 'City retrieved successfully');
    }

}
