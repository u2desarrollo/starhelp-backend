<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\TemplateAccountingRepository;

use App\Services\TemplateAccountingService; 

use Response;

class TemplateAccountingApiController extends AppBaseController
{
    private $templateAccountingRepository;
    private $templateAccountingService;
    
    public function __construct(
        TemplateAccountingRepository $templateAccountingRepository,
        TemplateAccountingService $templateAccountingService
    )
    {
        $this->templateAccountingRepository = $templateAccountingRepository;
        $this->templateAccountingService = $templateAccountingService;
    }

    /**
     * lista de contabilidad.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $response = $this->templateAccountingRepository->findAll($request);
        return $this->sendResponse($response->toArray(), 'Account retrieved successfully');
    }

    /**
     *
     * Crear un registro.
     * 
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->templateAccountingService->store($request);
        return $this->sendResponse($response->toArray(), 'Account retrieved successfully');
    }
}
