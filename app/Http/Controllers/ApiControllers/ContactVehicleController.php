<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Auth\LoginController;
use App\Repositories\ContactVehicleRepository;
use App\Services\ContactVehicleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

/**
 * Class ContactVehicleController permite definir métodos para controlar las peticiones Http relacionadas al modelo ContactVehicle
 * @package App\Http\Controllers\ApiControllers
 * @author Jhon García
 */
class ContactVehicleController extends Controller
{
    /**
     * ContactVehicleController constructor.
     * @param ContactVehicleService $contactVehicleService
     * @param ContactVehicleRepository $contactVehicleRepository
     * @author Jhon García
     */
    public function __construct(ContactVehicleService $contactVehicleService, ContactVehicleRepository $contactVehicleRepository)
    {
        $this->service = $contactVehicleService;
        $this->repository = $contactVehicleRepository;
    }

    /**
     * Método que controla las peticiones Http de tipo GET para listar los vehículos asociados a terceros
     *
     * @param Request $request Contiene todos los posibles filtros a aplicar en la consulta
     * @return JsonResponse Con tiene el listado de vehículos
     * @author Jhon García
     */
    public function index(Request $request)
    {
        $vehicles = $this->repository->findAll($request->all());

        return response()->json([
            'message' => 'Vehicles retrieved successfully',
            'data' => $vehicles->toArray()
        ], 200);
    }
}
