<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateMenuAPIRequest;
use App\Http\Requests\ApiRequests\UpdateMenuAPIRequest;
use App\Entities\Menu;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;
use Response;

/**
 * Class MenuController
 * @package App\Http\Controllers\ApiControllers
 */

class MenuAPIController extends AppBaseController
{
    /** @var  MenuRepository */
    private $menuRepository;

    public function __construct(MenuRepository $menuRepo)
    {
        $this->menuRepository = $menuRepo;
    }

    /**
     * Display a listing of the Menu.
     * GET|HEAD /menus
     *
     * @return Response
     */
    public function index()
    {
        $menus = $this->menuRepository->findAll();

        return $this->sendResponse($menus->toArray(), 'Menus retrieved successfully');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $menus = $this->menuRepository->store($input);

        return $this->sendResponse($menus, 'Menu saved successfully');
    }

    /**
     * Display the specified Menu.
     * GET|HEAD /menus/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Menu $menu */
        $menu = $this->menuRepository->findWithoutFail($id);

        if (empty($menu)) {
            return $this->sendError('Menu not found');
        }

        return $this->sendResponse($menu->toArray(), 'Menu retrieved successfully');
    }

    /**
     * Update the specified Menu in storage.
     * PUT/PATCH /menus/{id}
     *
     * @param  int $id
     * @param UpdateMenuAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuAPIRequest $request)
    {
        $input = $request->all();

        /** @var Menu $menu */
        $menu = $this->menuRepository->findWithoutFail($id);

        if (empty($menu)) {
            return $this->sendError('Menu not found');
        }

        $menu = $this->menuRepository->update($input, $id);

        return $this->sendResponse($menu->toArray(), 'Menu updated successfully');
    }

    /**
     * Remove the specified Menu from storage.
     * DELETE /menus/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Menu $menu */
        $menu = $this->menuRepository->findWithoutFail($id);

        if (empty($menu)) {
            return $this->sendError('Menu not found');
        }

        $menu->delete();

        return $this->sendResponse($id, 'Menu deleted successfully');
    }

    public function createMenuVideoTutorial(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = $user = auth()->user()->id;

        $note = $this->menuRepository->createMenuVideoTutorial($input);
        return $this->sendResponse($note, 'Note video crated successfully');
    }

    public function updateMenuVideoTutorial(Request $request, $id)
    {
        $note = $this->menuRepository->updateMenuVideoTutorial($request->all(),  $id);
        return $this->sendResponse($note, 'Note video update successfully');
    }

    public function deleteMenuVideoTutorial(Request $request, $id)
    {
        $note = $this->menuRepository->deleteMenuVideoTutorial($request->all(),  $id);
        return $this->sendResponse($note, 'Note video deleted successfully');
    }

    public function findAllMenuVideoTutorial(Request $request)
    {
        $notes = $this->menuRepository->findAllMenuVideoTutorial($request);
        return response()->json($notes, 200);
    }
}
