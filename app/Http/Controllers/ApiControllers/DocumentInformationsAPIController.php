<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateDocumentInformationsAPIRequest;
use App\Http\Requests\ApiRequests\UpdateDocumentInformationsAPIRequest;
use App\Entities\DocumentInformations;
use App\Repositories\DocumentInformationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DocumentInformationsController
 * @package App\Http\Controllers\ApiControllers
 */

class DocumentInformationsAPIController extends AppBaseController
{
	/** @var  DocumentInformationsRepository */
	private $documentInformationsRepository;

	public function __construct(DocumentInformationsRepository $documentInformationsRepo)
	{
		$this->documentInformationsRepository = $documentInformationsRepo;
	}

	/**
	 * Display a listing of the DocumentInformations.
	 * GET|HEAD /documentInformations
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->documentInformationsRepository->pushCriteria(new RequestCriteria($request));
		$this->documentInformationsRepository->pushCriteria(new LimitOffsetCriteria($request));
		$documentInformations = $this->documentInformationsRepository->all();

		return $this->sendResponse($documentInformations->toArray(), 'Document Informations retrieved successfully');
	}

	/**
	 * Store a newly created DocumentInformations in storage.
	 * POST /documentInformations
	 *
	 * @param CreateDocumentInformationsAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDocumentInformationsAPIRequest $request)
	{
		$input = $request->all();

		$documentInformations = $this->documentInformationsRepository->create($input);

		return $this->sendResponse($documentInformations->toArray(), 'Document Informations saved successfully');
	}

	/**
	 * Display the specified DocumentInformations.
	 * GET|HEAD /documentInformations/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var DocumentInformations $documentInformations */
		$documentInformations = $this->documentInformationsRepository->findWithoutFail($id);

		if (empty($documentInformations)) {
			return $this->sendError('Document Informations not found');
		}

		return $this->sendResponse($documentInformations->toArray(), 'Document Informations retrieved successfully');
	}

	/**
	 * Update the specified DocumentInformations in storage.
	 * PUT/PATCH /documentInformations/{id}
	 *
	 * @param  int $id
	 * @param UpdateDocumentInformationsAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateDocumentInformationsAPIRequest $request)
	{
		$input = $request->all();

		/** @var DocumentInformations $documentInformations */
		$documentInformations = $this->documentInformationsRepository->findWithoutFail($id);

		if (empty($documentInformations)) {
			return $this->sendError('Document Informations not found');
		}

		$documentInformations = $this->documentInformationsRepository->update($input, $id);

		return $this->sendResponse($documentInformations->toArray(), 'DocumentInformations updated successfully');
	}

	/**
	 * Remove the specified DocumentInformations from storage.
	 * DELETE /documentInformations/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var DocumentInformations $documentInformations */
		$documentInformations = $this->documentInformationsRepository->findWithoutFail($id);

		if (empty($documentInformations)) {
			return $this->sendError('Document Informations not found');
		}

		$documentInformations->delete();

		return $this->sendResponse($id, 'Document Informations deleted successfully');
	}
}