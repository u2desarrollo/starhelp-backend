<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateBranchOfficeWarehouseAPIRequest;
use App\Http\Requests\ApiRequests\UpdateBranchOfficeWarehouseAPIRequest;
use App\Entities\BranchOfficeWarehouse;
use App\Repositories\BranchOfficeWarehouseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BranchOfficeWarehouseController
 * @package App\Http\Controllers\ApiControllers
 */
class BranchOfficeWarehouseAPIController extends AppBaseController
{
    /** @var  BranchOfficeWarehouseRepository */
    private $branchOfficeWarehouseRepository;

    public function __construct(BranchOfficeWarehouseRepository $branchOfficeWarehouseRepo)
    {
        $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     */
    public function index(Request $request)
    {
        $this->branchOfficeWarehouseRepository->pushCriteria(new RequestCriteria($request));
        $this->branchOfficeWarehouseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->findForBranchOffice($request);

        return $this->sendResponse($branchOfficeWarehouses, 'Branch Office Warehouses retrieved successfully');
    }

    /**
     * @param CreateBranchOfficeWarehouseAPIRequest $request
     * @return Response     *
     */
    public function store(CreateBranchOfficeWarehouseAPIRequest $request)
    {
        $input = $request->all();

        $branchOfficeWarehouses = $this->branchOfficeWarehouseRepository->create($input);

        return $this->sendResponse($branchOfficeWarehouses->toArray(), 'Branch Office Warehouse saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *

     */
    public function show($id)
    {
        /** @var BranchOfficeWarehouse $branchOfficeWarehouse */
        $branchOfficeWarehouse = $this->branchOfficeWarehouseRepository->findWithoutFail($id);


        if (empty($branchOfficeWarehouse)) {
            return $this->sendError('Branch Office Warehouse not found');
        }

        return $this->sendResponse($branchOfficeWarehouse->toArray(), 'Branch Office Warehouse retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBranchOfficeWarehouseAPIRequest $request
     * @return Response
     *
     */
    public function update($id, UpdateBranchOfficeWarehouseAPIRequest $request)
    {
        $input = $request->all();

        /** @var BranchOfficeWarehouse $branchOfficeWarehouse */
        $branchOfficeWarehouse = $this->branchOfficeWarehouseRepository->findWithoutFail($id);

        if (empty($branchOfficeWarehouse)) {
            return $this->sendError('Branch Office Warehouse not found');
        }

        $branchOfficeWarehouse = $this->branchOfficeWarehouseRepository->update($input, $id);

        return $this->sendResponse($branchOfficeWarehouse->toArray(), 'BranchOfficeWarehouse updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var BranchOfficeWarehouse $branchOfficeWarehouse */
        $branchOfficeWarehouse = $this->branchOfficeWarehouseRepository->findWithoutFail($id);

        if (empty($branchOfficeWarehouse)) {
            return $this->sendError('Branch Office Warehouse not found');
        }

        $branchOfficeWarehouse->delete();

        return $this->sendResponse($id, 'Branch Office Warehouse deleted successfully');
    }
    public function getBranchsWare(){
        $branchs_ware=$this->branchOfficeWarehouseRepository->getBranchsWare();
        if(empty($branchs_ware)){
            return $this->sendError('Branch Ware Office not found');
        }
        return $this->sendResponse($branchs_ware->toArray(), 'Branch Ware Office retrieved successfully');
    }
}
