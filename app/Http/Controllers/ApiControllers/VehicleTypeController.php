<?php

namespace App\Http\Controllers\ApiControllers;

use App\Repositories\VehicleTypeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class VehicleTypeController permite definir métodos para controlar las peticiones Http relacionadas al modelo VehicleType
 * @package App\Http\Controllers\ApiControllers
 * @author Jhon García
 */
class VehicleTypeController extends Controller
{

    public function __construct(VehicleTypeRepository $vehicleTypeRepository)
    {
        $this->repository = $vehicleTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $vehiclesTypes = $this->repository->findAll($request->toArray());

        return response()->json([
            'data' => $vehiclesTypes->toArray()
        ]);
    }
}
