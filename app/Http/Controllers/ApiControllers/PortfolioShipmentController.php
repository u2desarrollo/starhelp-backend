<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\PortfolioShipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $portfolioShipments=PortfolioShipment::orderBy('expiration_days_from','asc')->get();
        return response()->json($portfolioShipments,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            PortfolioShipment::create($request->toArray());
        return response()->json('configuracion creada correctamente',200);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(),500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          try {
            $portfolioShipment=PortfolioShipment::findOrFail($id);
            $portfolioShipment->update($request->toArray());
        return  response()->json($portfolioShipment,200) ;
          } catch (\Throwable $th) {
            return  response()->json($th->getMessage(),500) ;
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try {
        $portfolioShipment=PortfolioShipment::findOrFail($id);
        $portfolioShipment->delete();
        return  response()->json($portfolioShipment,200) ;
       } catch (\Throwable $th) {
           return  response()->json($th->getMessage(),500) ;
    }
}
}
