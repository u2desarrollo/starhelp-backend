<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateBannerB2bAPIRequest;
use App\Http\Requests\ApiRequests\UpdateBannerB2bAPIRequest;
use App\Entities\BannerB2b;
use App\Repositories\BannerB2bRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Traits\LoadImageTrait;

/**
 * Class BannerB2bController
 * @package App\Http\Controllers\ApiControllers
 */

class BannerB2bAPIController extends AppBaseController
{
	use LoadImageTrait;

	/** @var  BannerB2bRepository */
	private $bannerB2bRepository;

	public function __construct(BannerB2bRepository $bannerB2bRepo)
	{
        // $this->middleware(['role_or_permission:super-admin|create banners|edit banners|list banners|delete banners']);
		$this->bannerB2bRepository = $bannerB2bRepo;
	}

	/**
	 * Display a listing of the BannerB2b.
	 * GET|HEAD /bannerB2bs
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->bannerB2bRepository->pushCriteria(new RequestCriteria($request));
		$this->bannerB2bRepository->pushCriteria(new LimitOffsetCriteria($request));
		$bannerB2bs = $this->bannerB2bRepository->findAll($request);

		return $this->sendResponse($bannerB2bs->toArray(), 'Banner B2Bs retrieved successfully');
	}

	/**
	 * Store a newly created BannerB2b in storage.
	 * POST /bannerB2bs
	 *
	 * @param CreateBannerB2bAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateBannerB2bAPIRequest $request)
	{
		$input = $request->all();

		if (isset($input['big_banner'])) {
			$input['big_banner'] = $this->saveImage($input['big_banner'], 'banners-b2b/desktop');
		}
		if (isset($input['small_banner'])) {
			$input['small_banner'] = $this->saveImage($input['small_banner'], 'banners-b2b/mobile');
		}

		$bannerB2bs = $this->bannerB2bRepository->create($input);

		return $this->sendResponse($bannerB2bs->toArray(), 'Banner B2B saved successfully');
	}

	/**
	 * Display the specified BannerB2b.
	 * GET|HEAD /bannerB2bs/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var BannerB2b $bannerB2b */
		$bannerB2b = $this->bannerB2bRepository->findWithoutFail($id);

		if (empty($bannerB2b)) {
			return $this->sendError('Banner B2B not found');
		}

		return $this->sendResponse($bannerB2b->toArray(), 'Banner B2B retrieved successfully');
	}

	/**
	 * Update the specified BannerB2b in storage.
	 * PUT/PATCH /bannerB2bs/{id}
	 *
	 * @param  int $id
	 * @param UpdateBannerB2bAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateBannerB2bAPIRequest $request)
	{
		$input = $request->all();

		/** @var BannerB2b $bannerB2b */
		$bannerB2b = $this->bannerB2bRepository->findWithoutFail($id);

		if (empty($bannerB2b)) {
			return $this->sendError('Banner B2B not found');
		}

		if (isset($input['big_banner'])) {
			if ($input['big_banner'] != $bannerB2b->big_banner) {
				$input['big_banner'] = $this->saveImage($input['big_banner'], 'banners-b2b/desktop');
			} else {
				unset($input['big_banner']);
			}
		}

		if (isset($input['small_banner'])) {
			if ($input['small_banner'] != $bannerB2b->small_banner) {
				$input['small_banner'] = $this->saveImage($input['small_banner'], 'banners-b2b/desktop');
			} else {
				unset($input['small_banner']);
			}
		}

		$bannerB2b = $this->bannerB2bRepository->update($input, $id);

		return $this->sendResponse($bannerB2b->toArray(), 'BannerB2b updated successfully');
	}

	/**
	 * Remove the specified BannerB2b from storage.
	 * DELETE /bannerB2bs/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var BannerB2b $bannerB2b */
		$bannerB2b = $this->bannerB2bRepository->findWithoutFail($id);

		if (empty($bannerB2b)) {
			return $this->sendError('Banner B2B not found');
		}

		$bannerB2b->delete($id);

		return $this->sendResponse($id, 'Banner B2B deleted successfully');
    }


    /**
     *
     * obtener los banners segun el request
     */
    public function getBanners(Request $request){
        $banner = BannerB2b::where('parameter_id', '=',$request->idParameter)
        /*->wherehas('line', function ($query) use ($request){
			$query->where('line_code', $request->lines);
		}) */->get();

        return $banner; //$this->sendResponse($banner->toArray(), 'Parameters retrieved successf     ully');

    }
}
