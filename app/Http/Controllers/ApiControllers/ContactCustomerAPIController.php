<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactCustomerAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactCustomerAPIRequest;
use App\Entities\ContactCustomer;
use App\Repositories\ContactCustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactCustomerController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactCustomerAPIController extends AppBaseController
{
    /** @var  ContactCustomerRepository */
    private $contactCustomerRepository;

    public function __construct(ContactCustomerRepository $contactCustomerRepo)
    {
        $this->contactCustomerRepository = $contactCustomerRepo;
    }

    /**
     * Display a listing of the ContactCustomer.
     * GET|HEAD /contactCustomers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contactCustomerRepository->pushCriteria(new RequestCriteria($request));
        $this->contactCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactCustomers = $this->contactCustomerRepository->all();

        return $this->sendResponse($contactCustomers->toArray(), 'Contact Customers retrieved successfully');
    }

    /**
     * Store a newly created ContactCustomer in storage.
     * POST /contactCustomers
     *
     * @param CreateContactCustomerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateContactCustomerAPIRequest $request)
    {
        $input = $request->all();

        $contactCustomers = $this->contactCustomerRepository->create($input);

        return $this->sendResponse($contactCustomers->toArray(), 'Contact Customer saved successfully');
    }

    /**
     * Display the specified ContactCustomer.
     * GET|HEAD /contactCustomers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ContactCustomer $contactCustomer */
        $contactCustomer = $this->contactCustomerRepository->findWithoutFail($id);

        if (empty($contactCustomer)) {
            return $this->sendError('Contact Customer not found');
        }

        return $this->sendResponse($contactCustomer->toArray(), 'Contact Customer retrieved successfully');
    }

    /**
     * Update the specified ContactCustomer in storage.
     * PUT/PATCH /contactCustomers/{id}
     *
     * @param  int $id
     * @param UpdateContactCustomerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactCustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var ContactCustomer $contactCustomer */
        $contactCustomer = $this->contactCustomerRepository->findWithoutFail($id);

        if (empty($contactCustomer)) {
            return $this->sendError('Contact Customer not found');
        }

        $contactCustomer = $this->contactCustomerRepository->update($input, $id);

        return $this->sendResponse($contactCustomer->toArray(), 'ContactCustomer updated successfully');
    }

    /**
     * Remove the specified ContactCustomer from storage.
     * DELETE /contactCustomers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ContactCustomer $contactCustomer */
        $contactCustomer = $this->contactCustomerRepository->findWithoutFail($id);

        if (empty($contactCustomer)) {
            return $this->sendError('Contact Customer not found');
        }

        $contactCustomer->delete();

        return $this->sendResponse($id, 'Contact Customer deleted successfully');
    }

    public function getContactCategory($id)
    {
        $contactCustomer = $this->contactCustomerRepository->getContactCategory($id);
        return $this->sendResponse($contactCustomer, 'Contact Customer');
    }
}
