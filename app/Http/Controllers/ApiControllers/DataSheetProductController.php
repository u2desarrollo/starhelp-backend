<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\DataSheetProduct;
use Validator;

class DataSheetProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(DataSheetProduct::orderBy('id', 'desc')->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            //'data_sheet_product_id' => 'required | exists:data_sheet_products,id',
            'product_id' => 'required | exists:products,id',
            'value' => 'required'
        ]);

        if ($v->fails()) return response()->json(['message' => $v->errors(), 'code' => '400'], 400);

        $valueStore = json_decode($request->value, true);

        $v = Validator::make($valueStore, [
            'value' => 'required',
            'data_sheet_id' => 'required'
        ]);

        if ($v->fails()) return response()->json(['message' => $v->errors(), 'code' => '400'], 400);

        $dataSheetProduct = DataSheetProduct::where('product_id', $request->product_id)->first();
        if (empty($dataSheetProduct)) return response()->json(['message' => 'No se encontro registro de ficha de datos', 'code' => '400'], 400);

        foreach ($dataSheetProduct->value as $key => $value) {
            if ($valueStore['data_sheet_id'] == $value['data_sheet_id']) {
                return response()->json([
                    'message' => 'Este valor ya existe en la ficha de datos del producto',
                    'code' => '400'
                ], 400);
            }
        }

        $valueDataBase = $dataSheetProduct->value;
        array_push($valueDataBase, $valueStore);

        $dataSheetProduct->value = json_decode(json_encode($valueDataBase));
        $dataSheetProduct->save();

        return response()->json([
            'message' => 'Registro creado.',
            'data' => $dataSheetProduct->value
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DataSheetProduct::where('product_id', $id)->first();

        if (!$data) return response()->json(['message' => 'Not data found', 'code' => '404'], 404);

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'data_sheet_id' => 'required | exists:data_sheet,id',
            'value' => 'required'
        ]);

        if ($v->fails()) return response()->json(['message' => $v->errors(), 'code' => '400'], 400);

        $dataSheetProduct = DataSheetProduct::where('product_id', $id)->first();
        if (!$dataSheetProduct) return response()->json(['message' => 'Not data found.', 'code' => '404'], 404);

        $dataSheetUpdate = $dataSheetProduct->value;

        foreach ($dataSheetUpdate as $key => $value) {
            if ($value['data_sheet_id'] == $request->data_sheet_id) {
                $dataSheetUpdate[$key]['value'] = $request->value;
            }
        }

        $dataSheetProduct->value = json_decode(json_encode($dataSheetUpdate));
        $dataSheetProduct->save();

        return response()->json([
            'message' => 'Registro actualizado.',
            'data' => $dataSheetProduct
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'data_sheet_id' => 'required | exists:data_sheet,id'
        ]);

        if ($v->fails()) return response()->json(['message' => $v->errors(), 'code' => '400'], 400);

        $dataSheetProduct = DataSheetProduct::where('product_id', $id)->first();

        if (!$dataSheetProduct) return response()->json(['message' => 'Not data found.', 'code' => '404'], 404);

        $dataSheetDelete = $dataSheetProduct->value;
        $registerDestroy = [];

        foreach ($dataSheetDelete as $key => $value) {
            if ($value['data_sheet_id'] == $request->data_sheet_id) {
                array_push($registerDestroy, $dataSheetDelete[$key]);
                unset($dataSheetDelete[$key]);
            }
        }

        $dataSheetProduct->value = json_decode(json_encode($dataSheetDelete), true);
        $dataSheetProduct->save();

        return response()->json([
            'message' => 'Registro eliminado.',
            'data' => $registerDestroy
        ], 200);
    }
}