<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateTemplateAPIRequest;
use App\Http\Requests\ApiRequests\UpdateTemplateAPIRequest;
use App\Entities\Template;
use App\Entities\TemplateTransaction;
use App\Entities\VouchersType;
use App\Repositories\TemplateRepository;
use App\Traits\CacheTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Cache;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TemplateController
 *
 * @package App\Http\Controllers\ApiControllers
 */
class TemplateAPIController extends AppBaseController
{
    use CacheTrait;

    /** @var  TemplateRepository */
    private $templateRepository;

    public function __construct(TemplateRepository $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * Display a listing of the Template.
     * GET|HEAD /templates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $template = $this->templateRepository->findAll($request);

        return $this->sendResponse($template->toArray(), 'Template retrieved successfully');
    }

    /**
     * Store a newly created Template in storage.
     * POST /templates
     *
     * @param CreateTemplateAPIRequest $request
     * @return JsonResponse
     */
    public function store(CreateTemplateAPIRequest $request)
    {
        $input = $request->except('payment_methods_allowed_id', 'line_id', 'user_id');

        if (Template::where('code', $input['code'])->exists()) {
            return response()->json([
                'message' => 'El codigo ya existe',
                'status'  => 422
            ], 422);
        }

        $template = $this->templateRepository->createTemplate($input);

        if (($request['line_id']) != null) {
            $template->lines()->attach($request['line_id']);
        }

        if (($request['user_id']) != null) {
            $template->users()->attach($request['user_id']);
        }

        if (($request['payment_methods_allowed_id']) != null) {
            $template->parameters()->attach($request['payment_methods_allowed_id']);
        }

        if (Cache::has('model_' . $template->id)) {
            Cache::forget('model_' . $template->id);
        }

        return $this->sendResponse($template, 'Template saved successfully');
    }

    /**
     * Display the specified Template.
     * GET|HEAD /templates/{id}
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // Buscar el modelo
        $template = $this->templateRepository->findForId($id);

        // Verificar si contiene informacion
        if (empty($template)) return $this->sendError('Template not found');

        // Retornar la informacion
        return $this->sendResponse($template, 'Template retrieved successfully');
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function getModelForId($id, Request $request)
    {
        // Buscar el modelo
        $template = $this->getModelForIdFromCache($id)->toArray();

        // Verificar si contiene informacion
        if (empty($template)) return $this->sendError('Template not found');

        if ($request->branchoffice_id) {

            #CAMBIOMODELOPORDEFECTO
            if (!empty($template['branchoffice_id'])) {
                $request->branchoffice_id = $template['branchoffice_id'];
            }

            if ($template['consecutive_handling'] === 1) {
                $vouchersType = VouchersType::where('code_voucher_type', $template['voucher_type']['code_voucher_type'])
                    ->where('branchoffice_id', $request->branchoffice_id)
                    ->first();

            } else {
                $vouchersType = VouchersType::where('code_voucher_type', $template['voucher_type']['code_voucher_type'])
                    ->whereHas('branchoffice', function ($query) {
                        $query->where('main', true);
                    })
                    ->first();
            }

            $template['voucher_type_selected_branchoffice'] = $vouchersType->toArray();
        }

        // Retornar la informacion
        return $this->sendResponse($template, 'Template retrieved successfully');
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getModelForCode($code)
    {
        // Buscar el modelo
        $template = $this->templateRepository->findForCode($code);

        // Retornar la informacion
        return $this->sendResponse($template, 'Template retrieved successfully');
    }


    /**
     * Update the specified Template in storage.
     * PUT/PATCH /templates/{id}
     *
     * @param int $id
     * @param UpdateTemplateAPIRequest $request
     * @return JsonResponse
     */
    public function update($id, UpdateTemplateAPIRequest $request)
    {
        $input = $request->all();

        if (Template::where('code', $input['code'])->where('id', '!=', $id)->exists()) {
            return response()->json(['El codigo ya existe'], 422);
        }

        if (isset($input['description'])) {
            $input['description'] = strtoupper($input['description']);
        }

        $template = $this->templateRepository->update($input, $id);
        $this->saveTemplateTransaction($input['templateTransaction'], $id);

        if (Cache::has('model_' . $id)) {
            Cache::forget('model_' . $id);
        }

        return $this->sendResponse($template, 'Template updated successfully');
    }

    public function saveTemplateTransaction($templateTransaction, $template_id)
    {
        $templateTransaction = collect($templateTransaction);
        $newTransactions = $templateTransaction->where('new', true);
        $updateTransactions = $templateTransaction->where('new', '!=', true);

        // Eliminar las existentes
        TemplateTransaction::where('template_id', $template_id)
            ->whereNotIn('id', $updateTransactions->pluck('id')->toArray())
            ->delete();

        // Actualizar las existentes
        foreach ($updateTransactions as $key => $updateTransaction) {
            $transaction = TemplateTransaction::find($updateTransaction['id']);

            if ($transaction) {
                $transaction->update($updateTransaction);
            }
        }

        // Crear nuevas transacciones
        foreach ($newTransactions as $key => $newTransaction) {
            $newTransaction['template_id'] = $template_id;
            TemplateTransaction::create($newTransaction);
        }

        return $templateTransaction;

    }

    /**
     * Remove the specified Template from storage.
     * DELETE /templates/{id}
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Template $template */
        $template = $this->templateRepository->findWithoutFail($id);

        if (empty($template)) {
            return $this->sendError('Template not found');
        }
        $template->code = ' ';
        $template->save();
        $template->delete();

        return $this->sendResponse($id, 'Template deleted successfully');
    }
}
