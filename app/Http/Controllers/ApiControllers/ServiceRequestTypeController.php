<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ServiceRequestTypeRepository;

class ServiceRequestTypeController extends Controller
{
    protected $ServiceRequestTypeRepository;

    public function __construct(ServiceRequestTypeRepository $ServiceRequestTypeRepository)
    {
        $this->ServiceRequestTypeRepository = $ServiceRequestTypeRepository;
    }

    public function search()
    {
        return $this->ServiceRequestTypeRepository->search();
    }

    public function getStateByServiceRequestType(Request $request)
    {
        if (!$request->service_requests_type_id) {
            return response()->json([
                'message' => 'faltan argumentos'
            ], 400);
        }
        $res = $this->ServiceRequestTypeRepository->getStateByServiceRequestType($request);
        return response()->json($res, 200);
    }
}
