<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Prettus\Validator\Exceptions\ValidatorException;
use Response;

use App\Entities\Product;
use App\Entities\Line;
use Illuminate\Http\Request;
use App\Events\ProductUpdated;
use App\Traits\LoadImageTrait;
use App\Jobs\CalculatePricesJob;
use Illuminate\Support\Collection;
use App\Entities\ProductAttachment;
use App\Entities\ProductTechnnicalData;
use App\Entities\DataSheet;
use App\Entities\DataSheetLine;
use App\Entities\DataSheetProduct;
use App\Entities\Parameter;

use App\Services\CalculatePricesService;
//use App\Repositories\PromotionRepository;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateProductAPIRequest;
use App\Http\Requests\ApiRequests\UpdateProductAPIRequest;

//Repositories
use App\Repositories\ProductAttachmentRepository;
use App\Repositories\ProductTechnnicalDataRepository;
use App\Repositories\ProductRepository;
use App\Repositories\DocumentProductRepository;

//Services
use App\Services\ProductService;

/**
 * Class ProductController
 * @package App\Http\Controllers\ApiControllers
 */

class ProductAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  ProductRepository */
    private $productRepository;
    private $lineRepository;
    //private $promotionRepository;
    private $productTechnnicalDataRepository;
    private $productAttachmentRepository;
    private $documentProductRepository;

    //Services
    private $productService;


    public function __construct(
        ProductRepository $productRepo,/*PromotionRepository $promotionRepository,*/
        ProductTechnnicalDataRepository $productTechnnicalDataRepo,
        ProductAttachmentRepository $productAttachmentRepo,
        //Services
        DocumentProductRepository $documentProductRepository,
        ProductService $productService
    ) {
        $this->productRepository = $productRepo;
        //$this->promotionRepository = $promotionRepository;
        $this->productTechnnicalDataRepository = $productTechnnicalDataRepo;
        $this->productAttachmentRepository = $productAttachmentRepo;
        $this->documentProductRepository = $documentProductRepository;
        //Services
        $this->productService = $productService;
    }

    /**
     * Display a listing of the Product.
     * GET|HEAD /products
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $products = $this->productService->getProducts($request);
        return $this->sendResponse($products, 'Products retrieved successfully');
    }

    /**
     * Store a newly created Product in storage.
     * POST /products
     *
     * @param CreateProductAPIRequest $request
     * @return
     */
    public function store(CreateProductAPIRequest $request)
    {
        // Kevin Galindo
        $dataSheets = $request->data_sheets;

        $input = $request->except('data_sheets', 'products_attachments', 'products_technnical_data', 'line', 'subline', 'category', 'brand', 'type');

        Log::info($input);

        try {
            DB::beginTransaction();

            $product = Product::create($input);

            $technicalData = $request['products_technnical_data'];
            $technicalData['product_id'] = $product->id;
            if (isset($technicalData['link_data_sheet'])) {
                $technicalData['link_data_sheet'] = $this->saveImage($request['products_technnical_data']['link_data_sheet'], '/products/dataSheet');
            }

            $this->productTechnnicalDataRepository->create($technicalData);

            // Crear la ficha de datos
            if (!empty($dataSheets)) {
                // Crea los datos adicionales sobre el producto.
                DataSheetProduct::updateOrCreate(
                    [
                        'product_id' => $product->id,
                    ],
                    [
                        'product_id' => $product->id,
                        'value'      => json_decode(json_encode($dataSheets)),
                    ]
                );
            }
            DB::commit();

            return $this->sendResponse($product->toArray(), 'Product saved successfully');

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            DB::rollBack();
            return $this->sendError('Error en la creación del producto', 400);
        }

    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        /** @var Product $product */
        $product = $this->productService->getProduct($id, $request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product, 'Product retrieved successfully');
    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function getProductSimple($id, Request $request)
    {
        /** @var Product $product */
        $product = $this->productService->getProductSimple($id, $request);
        if (empty($product)) { return $this->sendError('Product not found'); }
        return $this->sendResponse($product, 'Product retrieved successfully');
    }

    /**
     * Update the specified Product in storage.
     * PUT/PATCH /products/{id}
     *
     * @param  int $id
     * @param UpdateProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAPIRequest $request)
    {

        // Kevin Galindo

        $dataSheets = [];
        foreach ($request->data_sheets as $key => $dataSheet) {
            $data_sheet_line = DataSheetLine :: where('data_sheet_id', $dataSheet["data_sheet_id"])->where('line_id', $request->line_id)->first();
            
            $dataSheet["order"] = !is_null($data_sheet_line) ? $data_sheet_line["order"] : null ;

            $dataSheets[]= $dataSheet;
        }
        
        $input = $request->except('products_attachments', 'products_technnical_data', 'line', 'subline', 'category', 'brand', 'type');

        $input['update_wsm']='false';

        /** @var Product $product */
        $product = Product::find($id);  //$this->productRepository->findForId($id, $request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product->update($input);
        //  $prod|uct_save = $this->productRepository->update($input, $id);

        $technicalData = $request['products_technnical_data'];
        if (isset($technicalData['link_data_sheet'])) {
            if ($product->productsTechnnicalData->link_data_sheet != $technicalData['link_data_sheet']) {
                $technicalData['link_data_sheet'] = $this->saveImage($request['products_technnical_data']['link_data_sheet'], '/products/dataSheet');
            } else {
                unset($technicalData['link_data_sheet']);
            }
        }
        // if(isset($technicalData['id'])){
        //     $product->productsTechnnicalData()->update($technicalData, $technicalData['id']);
        // } else {
        //     $technicalDat = new ProductTechnnicalData($technicalData);
        //     $product->productsTechnnicalData()->save($technicalDat);
        // }

        $attachment = $request['products_attachments'];
        if (isset($attachment['url'])) {
            if ($product->productsAttachments->url != $attachment['url']) {
                $attachment['url'] = $this->saveImage($request['products_attachments']['url'], '/products');
            } else {
                unset($attachment['url']);
            }
        }
        // if(isset($attachment['id'])){
        //     $product->productsAttachments()->update($attachment, $attachment['id']);
        // } else {
        //     $productAttachment = new ProductAttachment($attachment);
        //     $product->productsAttachments()->save($productAttachment);
        // }

        $respuestaId = Product::find($id); //$this->productRepository->findForId($id);
        $respuestaId->update_in_app = true;
        $respuestaId->save();

        // Actualizamos la ficha de datos
        if (!empty($dataSheets)) {
            // Crea los datos adicionales sobre el producto.
            DataSheetProduct::updateOrCreate(
                [
                    'product_id' => $product->id,
                ],
                [
                    'product_id' => $product->id,
                    'value' => json_decode(json_encode($dataSheets)),
                ]
            );
        }

        return $this->sendResponse($respuestaId, 'Product updated successfully');
    }

    /**
     * Remove the specified Product from storage.
     * DELETE /products/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product_has_movements = $this->documentProductRepository->validateMovements($id);
        if ($product_has_movements){
            return $this->sendError('El producto tiene movimientos');
        }else{
            $product->delete();
        }

        return $this->sendResponse($id, 'Product deleted successfully');
    }
    /** */
    public function findForCode($code)
    {
        $product = $this->productRepository->findByField(['code' => $code])->first();

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product->toArray(), 'Product retrieved successfully');
    }

    public function relacionados($id, Request $request)
    {
        /** @var Product $product */
        $product = $this->productRepository->related($id, $request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product->toArray(), 'Product retrieved successfully');
    }

    /*public function getPromotions(Request $request) {
		$products = $this->productRepository->productPromotions($request);

		return $this->sendResponse($products, 'Products retrieved successfully');
	}*/

    public function updateAvailability(Request $request)
    {
        $input = $request->all();
        $products = $this->productRepository->finishItem($input);
        return $this->sendResponse($products->toArray(), 'Product updated successfully');
    }

    /*public function setPromotionsForContactWarehouse($contact_warehouse_id) {
		CalculatePricesJob::dispatch($contact_warehouse_id);
	}*/

    public function calculatePriceForProductAndContactWarehouse($contact_warehouse_id, $product_id, Request $request)
    {
        /** @var Product $product */
        $product = $this->productRepository->findForId($product_id, $request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        //$objCalculatePrice = new CalculatePricesService($contact_warehouse_id);

        //$productWithPrice = $objCalculatePrice->calculateSingleProduct($product);

        return $this->sendResponse($product, 'Product with price retrieved successfully');
        //return $this->sendResponse($productWithPrice, 'Product with price retrieved successfully');

    }

    public function calculatePriceProductsAndContactWarehouse($contact_warehouse_id, Request $request)
    {

        $products = collect($request->all());

        $objCalculatePrice = new CalculatePricesService($contact_warehouse_id);

        $productsWithPrice = $objCalculatePrice->calculateMultipleProducts($products);

        return $this->sendResponse($productsWithPrice, 'Products with price retrieved successfully');
    }




    public function getProducts(Request $request)
    {
        $products = $this->productService->getProductsGrid($request);
        return $this->sendResponse($products, 'Product retrieved successfully');
    }

    public function uploadExcel(Request $request)
    {
        $this->productRepository->pushCriteria(new RequestCriteria($request));
        $this->productRepository->pushCriteria(new LimitOffsetCriteria($request));
        $products = $this->productRepository->uploadExcel($request);
    }

    /**
     * Esta funcion genera y retorna un archivo csv para importacion de productos.
     *
     * @return \Illuminate\Http\Response
     * @author Kevin Galindo
     */
    public function generateFileImport($line_id)
    {
        $respose = $this->productService->generateCsvForLine($line_id);
        return response()->stream($respose['file'], $respose['status'], $respose['headers']);
    }

    /**
     * Esta funcion se encarga de realizar la importacion de la informacion de productos.
     *
     * @return \Illuminate\Http\Response
     * @author Kevin Galindo
     */
    public function importFile(Request $request, $line_id)
    {
        $respose = $this->productService->importFileProduct($request, $line_id);

        return response()->json([
            'message' => $respose['message'],
            'code' => $respose['code']
        ], $respose['code']);
    }

    /**
     * Obtiene productos a partir de un texto y criterios de clasificación
     *
     * @return array
     */
    public function productByClasification(Request $request)
    {
        $products = $this->productService->productByClasification($request);
        return $this->sendResponse($products, 'Products retrieved successfully');
    }

    /**
     * Esta funcion se encarga de realizar la importacion de la informacion de productos.
     *
     * @return \Illuminate\Http\Response
     * @author Kevin Galindo
     */
    public function dataSheetProduct($id)
    {
        $product = Product::find($id)->load('data_sheets');

        if ($product->data_sheets == null) {
            return response()->json([
                'message' => 'No se han encontrado ficha de datos',
                'code' => '404'
            ], 404);
        }

        $dataSheets = $product->data_sheets->value;

        foreach ($dataSheets as $key => $value) {

            if (!isset($value["order"])) {
                $data_sheet_line = DataSheetLine :: where('line_id', $product->line_id)->where('data_sheet_id', $value['data_sheet_id'])->first();
                $dataSheets[$key]["order"] = !is_null($data_sheet_line) ? $data_sheet_line->order : null;
            }
            $dataSheets[$key]["order"] = is_null($dataSheets[$key]["order"]) ? null :  intval($dataSheets[$key]["order"]);

            $detail = DataSheet::find($value['data_sheet_id']);

            if (!$detail) {
                unset($dataSheets[$key]);
                continue;
            }
            $dataSheets[$key]['data_sheet_product_id'] = $product->data_sheets->id;
            $dataSheets[$key]['data_sheet_detail'] = $detail;

            if (!empty($detail->paramtable_id)) {
                $parameter = Parameter::select('id', 'code_parameter', 'name_parameter')
                    ->where('paramtable_id', $detail->paramtable_id)->get();
                $dataSheets[$key]['data_sheet_detail']['parameters'] = $parameter;
            }
        }


        $dataSheets = collect($dataSheets);

        $sorted = $dataSheets->sortBy('order');

        $sorted->values()->all();

        return response()->json($sorted->values(), 200);
    }

    /**
     * Este metodo se encarga de devolver la informacion
     * sobre todos los productos.
     *
     * @author kevin galindo
     */
    public function listProductsTable(Request $request)
    {
        $perPage = $request->perPage ? $request->perPage : 10;
        $search = $request->search;

        $filter_lines     = $request->filter_lines;
        $filter_sublines  = $request->filter_sublines;
        $filter_brands    = $request->filter_brands;
        $filter_subbrands = $request->filter_subbrands;
        $filter_update_wsm = $request->update_wsm;

        $products = Product::select('description', 'code', 'id', 'brand_id', 'line_id', 'subline_id')
                            ->selectRaw("CASE WHEN update_wsm = true THEN 'Act WMS' ELSE 'Pend Act WMS' END as update_wsm_tag")
                            ->with(['brand', 'line', 'subline']);
        if ($search) {

            $arraySearch = explode(' ',$search);

            // Buscardor flexio
            if (count($arraySearch) > 0) {
                foreach ($arraySearch as $key => $value) {
                    $products->where(function($query) use ($value) {

                        // Filtro datasheets
                        $query->orWhereHas('data_sheets', function ($query) use ($value) {
                            $query->where('value', 'ilike', '%'.$value.'%');
                        });

                        $query->orWhere('code', 'ilike', '%' . $value . '%');

                        $query->orWhere('description', 'ilike', '%' . $value . '%');

                        // MARCA
                        $query->orWhereHas('brand', function ($query)  use ($value) {
                            $query->where('description', 'ilike', '%' . $value . '%');
                        });

                        // LINEA
                        $query->orWhereHas('line', function ($query)  use ($value) {
                            $query->where('line_description', 'ilike', '%' . $value . '%');
                        });

                        // SUBLINEA
                        $query->orWhereHas('subline', function ($query)  use ($value) {
                            $query->where('subline_description', 'ilike', '%' . $value . '%');
                        });
                    });
                }
            }

        }

        if ($filter_lines && is_array($filter_lines)) {
            $products->whereHas('line', function($query) use ($filter_lines) {
                $query->whereIn('id',$filter_lines);
            });
        }

        if ($filter_sublines && is_array($filter_sublines)) {
            $products->whereHas('subline', function($query) use ($filter_sublines) {
                $query->whereIn('id',$filter_sublines);
            });
        }

        if ($filter_brands && is_array($filter_brands)) {
            $products->whereHas('brand', function($query) use ($filter_brands) {
                $query->whereIn('id',$filter_brands);
            });
        }

        if ($filter_subbrands && is_array($filter_subbrands)) {
            $products->whereHas('subbrand', function($query) use ($filter_subbrands) {
                $query->whereIn('id',$filter_subbrands);
            });
        }
        if ($filter_update_wsm) {
            $products->where('update_wsm','!=',$filter_update_wsm);
        }
        return response()->json($products->orderBy('code', 'asc')->paginate($perPage), 200);
    }

    public function generateReport(){

        $path_report = $this->productService->generateReport();

        return Storage::download('/public/reports/'.$path_report);


    }

    /**
     * @param Request $request
     */
    public function getPromotionsForProduct(Request $request)
    {
        /** @var Product $product */
        $product = $this->productService->getProduct($request->product_id, $request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product, 'Product retrieved successfully');
    }

    public function getProductByCode(Request $request)
    {
        $response = $this->productService->getProductByCode($request);
        return response()->json($response->data, $response->code);
    }

    public function repositionProducts(Request $request) {
        $product = $this->productRepository->repositionProducts($request);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product->toArray(), 'Product retrieved successfully');
    }

    /**
     * Este método se encarga de devolver la información del catalogo de productos para la app.
     *
     * @param Request $request Contiene los filtros de búsqueda a aplicar en la consulta
     * @return JsonResponse Contiene el listado de productos
     * @author Kevin Galindo
     */
    public function appProductCatalog(Request $request)
    {
        $products = $this->productService->appProductCatalog($request);

        return response()->json($products['data'], $products['code']);
    }

    /**
     * Este método se encarga de devolver la información del inventario de productos para la app.
     *
     * @param Request $request
     * @return JsonResponse
     * @author Kevin Galindo
     */
    public function appListInventoryProduct(Request $request)
    {
        $products = $this->productRepository->appListInventoryProduct($request);

        return response()->json($products, 200);
    }

    /**
     * Este metodo se encarga de devolver la lista de precio
     * de los productos
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function appListPriceProductByCustomer(Request $request)
    {
        $res = $this->productService->appListPriceProductByCustomer($request);
        return response()->json($res['data'], $res['code']);
    }

    /**
     * Este metodo retorna la imagen del producto
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function getImageProduct(Request $request)
    {
        if (!$request->imagen) {
            return response()->json('falta parametro.', 404);
        }

        $imgArray = explode("/", $request->imagen);
        $pathImage = 'brands/';
        $fileName = $imgArray[count($imgArray)-1];
        $file = $pathImage.$fileName;

        $exists = \Storage::disk('public')->exists($file);

        if ($exists) {
            return Storage::disk('public')->get($file, $fileName);
        } else {
            return response()->json('Imagen no encontrada.', 404);
        }
    }

    /**
     * Este metodo retorna array() con respuesta SOAP Enviados con exito y sin exito a WMS  o Si no se Encuentran Registros esto se hace por lotes de 200 Registros.
     *
     * @param product Id del producto si es uno a uno el consumo
     * @param action Accion que realizara el consumo 'A' Agregar o 'M' Modificar
     * En el caso que sea Masivo no es necesario ni enviar $product ni $action
     * @return Response Con toda los productos Actualizados
     * @author Haider Oviedo @Hom669
     */
    public function getProductsWms($product='',$action='A')
    {
        ini_set('max_execution_time', '1200');
        $res = $this->productService->createSoapWmsMasive($product,$action);
        //dd($res);
        return response()->json($res['data']);

    }
}

