<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateLineTaxAPIRequest;
use App\Http\Requests\ApiRequests\UpdateLineTaxAPIRequest;
use App\Entities\LineTax;
use App\Repositories\LineTaxRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LineTaxAPIController extends AppBaseController
{
	/** @var  lineTaxRepository */
	private $lineTaxRepository;

	public function __construct(LineTaxRepository $lineTaxRepo)
	{
		$this->lineTaxRepository = $lineTaxRepo;
	}

	/**
	 * Display a listing of the Tax.
	 * GET|HEAD /taxes
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->lineTaxRepository->pushCriteria(new RequestCriteria($request));
		$this->lineTaxRepository->pushCriteria(new LimitOffsetCriteria($request));
		$taxes = $this->lineTaxRepository->findForLineId($request);

		return $this->sendResponse($taxes->toArray(), 'Taxes retrieved successfully');
	}

	/**
	 * Store a newly created Tax in storage.
	 * POST /taxes
	 *
	 * @param CreateLineTaxAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateLineTaxAPIRequest $request)
	{
		$input = $request->all();

		$taxes = $this->lineTaxRepository->create($input);

		return $this->sendResponse($taxes, 'Tax saved successfully');
	}

	/**
	 * Display the specified Tax.
	 * GET|HEAD /taxes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Tax $tax */
		$tax = $this->lineTaxRepository->findWithoutFail($id);

		if (empty($tax)) {
			return $this->sendError('Tax not found');
		}

		return $this->sendResponse($tax->toArray(), 'Tax retrieved successfully');
	}

	/**
	 * Update the specified Tax in storage.
	 * PUT/PATCH /taxes/{id}
	 *
	 * @param  int $id
	 * @param UpdateLineTaxAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateLineTaxAPIRequest $request)
	{
		$input = $request->all();

		/** @var Tax $tax */
		$tax = $this->lineTaxRepository->findWithoutFail($id);

		if (empty($tax)) {
			return $this->sendError('Tax not found');
		}

		$tax = $this->lineTaxRepository->update($input, $id);

		return $this->sendResponse($tax->toArray(), 'Tax updated successfully');
	}

	/**
	 * Remove the specified Tax from storage.
	 * DELETE /taxes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Tax $tax */
		$tax = $this->lineTaxRepository->findWithoutFail($id);

		if (empty($tax)) {
			return $this->sendError('Tax not found');
		}

		$tax->delete();

		return $this->sendResponse($id, 'Tax deleted successfully');
	}
}
