<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\JobDoc;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobsDocsApiController extends Controller
{
    public function download($id)
    {
        $jobDoc = JobDoc::findOrFail($id);

        if (empty($jobDoc)) {
            return $this->sendError('Document not found');
        }

        $path = config('app.path_files_jobs') . $jobDoc->path_document;


        return Storage::disk('s3')->download($path , $jobDoc->path_document, []);
    }

    public function show($id)
    {

        $jobDoc = JobDoc::findOrFail($id);

        if (empty($jobDoc)) {
            return $this->sendError('Document not found');
        }

        $array = explode('.', $jobDoc->path_document);
        $extension = array_pop($array);

        $extensionsUrl = ['pdf', 'png', 'jpg', 'jpeg', 'gif'];

        $url = Storage::disk('s3')->temporaryUrl(config('app.path_files_jobs') . $jobDoc->path_document, now()->addMinutes(5));

        if (in_array($extension, $extensionsUrl)) {
            return $url;
        }

        return urlencode($url);
    }
}
