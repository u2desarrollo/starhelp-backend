<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Contact;
use App\Entities\DocumentFile;
use App\ElectronicInvoice\Processor;
use App\Http\Requests\SendMailRequest;
use App\Mail\SendMail;
use App\Repositories\DocumentProductRepository;
use App\Repositories\InventoryRepository;
use App\Traits\CacheTrait;
use Illuminate\Http\Request;
use App\Traits\GeneratePdfTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Repositories\DocumentRepository;
use App\Repositories\ContactWarehouseRepository;
use Illuminate\Support\Facades\Validator;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateDocumentAPIRequest;
use App\Http\Requests\ApiRequests\UpdateDocumentAPIRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;

// Controllers
use App\Http\Controllers\AppBaseController;

// Services
use App\Services\OrdersDetailSyncService;
use App\Services\OrdersSyncService;
use App\Services\ProductsSyncService;
use App\Services\DocumentsService;
use App\Services\DocumentAccountingService;

// Entities
use App\Entities\Pdf;
use App\Entities\Document;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\Template;
use App\Services\DocumentsBalanceService;
use Carbon\Carbon;
use Illuminate\Container\Container;

use Illuminate\Support\Facades\DB;

// Other
use Response;

/**
 * Class DocumentController
 * @package App\Http\Controllers\ApiControllers
 */
class DocumentAPIController extends AppBaseController
{
    use GeneratePdfTrait;
    use CacheTrait;

    /** @var  DocumentRepository */
    private $documentRepository;
    private $documentProductRepository;
    private $inventoryRepository;
    private $documentsBalanceService;
    protected $pdf;
    private $contactWarehouseRepository;
    private $documentAccountingService;

    protected $documentsService;

    public function __construct(
        Pdf $pdf,
        DocumentRepository $documentRepository,
        DocumentProductRepository $documentProductRepository,
        InventoryRepository $inventoryRepository,
        DocumentsService $documentsService,
        DocumentsBalanceService $documentsBalanceService,
        DocumentAccountingService $documentAccountingService,
        ContactWarehouseRepository $contactWarehouseRepository
    ) {
        $this->pdf = $pdf;
        $this->documentRepository = $documentRepository;
        $this->documentProductRepository = $documentProductRepository;
        $this->inventoryRepository = $inventoryRepository;
        $this->documentsService = $documentsService;
        $this->documentsBalanceService = $documentsBalanceService;
        $this->contactWarehouseRepository = $contactWarehouseRepository;
        $this->documentAccountingService = $documentAccountingService;
    }

    /**
     * Display a listing of the Document.
     * GET|HEAD /documents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $this->documentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $documents = $this->documentRepository->all();

        return $this->sendResponse($documents->toArray(), 'Documents retrieved successfully');
    }

    public function excelDespatch(Request $request)
    {
        $res = $this->documentsService->getDespatchInvoiced($request);

        $response = $this->documentsService->createCsvDespatch($res['data']);

        return response()->stream($response['file'], $response['status'], $response['headers']);
    }

    /**
     * Store a newly created Document in storage.
     * POST /documents
     *
     * @param CreateDocumentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentAPIRequest $request)
    {
        $user = JWTAuth::parseToken()->toUser();
        $input = $request->all();
        $input['user_id'] = $user->id;

        // Buscamos el modelo #CAMBIOMODELOPORDEFECTO
        $model = $this->getModelForIdFromCache($request->model_id);

        if ($model) {
            if (!empty($model->branchoffice_id)) {
                $input['branchoffice_id'] = $model->branchoffice_id;
            }

            if (!empty($model->branchoffice_warehouse_id)) {
                $branchOfficeWarehouse = $this->getBranchOfficeWarehouse($input['branchoffice_id'],  $model->branchOfficeWarehouse->warehouse_code);

                if ($branchOfficeWarehouse) {
                    $input['branchoffice_warehouse_id'] = $branchOfficeWarehouse->id;
                }
            }
        }

        $documents = $this->documentRepository->create($input);

        return $this->sendResponse($documents->toArray(), 'Document saved successfully');
    }

    /**
     * Display the specified Document.
     * GET|HEAD /documents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {

        $response = $this->documentsService->show($id, $request);
        return response()->json($response['data'], $response['code']);
    }

    public function viewDocument($id, Request $request)
    {

        $response = $this->documentsService->viewDocument($id, $request);
        return response()->json($response['data'], $response['code']);
    }



    /**
     * Update the specified Document in storage.
     * PUT/PATCH /documents/{id}
     *
     * @param  int $id
     * @param UpdateDocumentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Document $document */
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            return $this->sendError('Document not found');
        }

        $document = $this->documentRepository->update($input, $id);

        return $this->sendResponse($document->toArray(), 'Document updated successfully');
    }

    /**
     * Este metodo se encarga de actualizar
     * la informacion general del documento.
     *
     * @author Kevin Galindo
     */
    public function updateGeneralDataDocument($id, Request $request)
    {
        $data = $this->documentsService->updateGeneralDataDocument($id, $request);
        return response()->json($data['data'], $data['code']);
    }

    /**
     * Remove the specified Document from storage.
     * DELETE /documents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Document $document */
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            return $this->sendError('Document not found');
        }

        $document->delete();

        return $this->sendResponse($id, 'Document deleted successfully');
    }

    /**
     * @param $contact
     * @param $warehouse
     * @param $user
     * @return mixed
     */
    public function checkCart($contact, $warehouse, $user, Request $request)
    {
        $document = $this->documentsService->checkCart($contact, $warehouse, $user, $request);

        if (empty($document)) {
            return $this->sendError('Document not found');
        }

        return $this->sendResponse($document, 'Document retrieved successfully');
    }

    /**
     * Finalizar carrito
     *
     * @param Request $request Contiene los parámetros para la finalización del carrito de compras
     * @return JsonResponse Contiene la información del carrito finalizado
     * @author Jhon García
     */
    public function completeCart(Request $request)
    {
        $cart = $this->documentsService->completeCart($request);

        return response()->json($cart['data'], $cart['code']);
    }

    /**
     *
     */
    public function tracking(Request $request)
    {
        $documents = $this->documentRepository->tracking($request);

        if (empty($documents)) {
            return $this->sendError('Document not found');
        }

        $documents = $this->documentsService->getDocumentWalletProblem($documents, null, false, true);

        return $this->sendResponse($documents->toArray(), 'Document retrieved successfully');
    }

    public function ordersCustomers(Request $request)
    {
        $id = $request->iduser;

        try {
            $validator = Validator::make($request->all(), [
                'iduser' => 'required|integer'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => ' No envió el usuario.'], 400);
            }

            $visitCustomers = $this->documentRepository->orders($id);
            return response()->json($visitCustomers);
        } catch (\Exception $e) {
            return response()->json(['error' => ' Vendedor no existente.'], 500);
        }
    }

    public function getDocuments(Request $request)
    {
        $documentsData = $this->documentRepository->getDocuments($request);
        if (empty($documentsData)) {
            return $this->sendError("Document not found");
        }
        return $this->sendResponse($documentsData->toArray(), 'Document retrieved successfully');
    }

    /**
     * Obtiene los documentos base
     *
     * @param Request $request Contiene los filtros a aplicar en la consulta
     * @return object
     */
    public function baseDocuments(Request $request)
    {
        $v = Validator::make($request->all(), [
            'model_id'        => 'required',
            'branchoffice_id' => 'required',
        ]);

        if ($v->fails()) return (object)['data' => $v->errors(), 'code' => 400];

        $documents = $this->documentsService->listBaseDocuments($request);

        return $this->sendResponse($documents, 'Base Documents retrieved successfully');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function documentsRetained(Request $request)
    {
        if (isset($request->myActivity) && $request->myActivity === "true") {
            $documents = $this->documentsService->myActivityListBaseDocuments($request);
        } else {
            $documents = $this->documentsService->listBaseDocumentsRetained($request);
        }
        return $this->sendResponse($documents, 'Base Documents retrieved successfully');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createDocumentFromBase(Request $request)
    {
        ini_set('memory_limit', '2024M');

        $v = Validator::make($request->all(), [
            'document_id'               => 'required',
            'vouchertype_id'            => 'required',
            'model_id'                  => 'required',
            'branchoffice_warehouse_id' => 'required',
        ]);

        if ($v->fails()) {
            return ['data' => $v->errors(), 'code' => 400];
        }

        $user = JWTAuth::parseToken()->toUser();

        $response = $this->documentsService->createDocumentFromBase($request, $user);

        return response()->json($response['data'], $response['code']);
    }

    /**
     * Obtiene las facturas
     *
     * @param int $request->idcliente Código del cliente
     * @return array
     */
    public function getInvoices(Request $request)
    {

        //\Log::info(json_encode($request->all()).'   request');
        $validator = Validator::make($request->all(), [
            'idcliente' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["error" => " Campo idcliente requerido."], 400);
        }



        $documents = $this->documentRepository->getInvoices($request->idcliente);

        // if (count($documents) > 0) {
        //\Log::info(json_encode($documents).'   siii');
        return response()->json($documents, 200);
        //   }

        //return response()->json([], 200);
        //return $this->sendError(' No se encontraron facturas para el usuario con id '. $request->idcliente);
    }

    /**
     * Obtiene los recibos de caja
     */
    public function getCashReceipts(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'iduser' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => 'Campo iduser es requerido'], 400);
        }
        $documents = $this->documentRepository->getCashReceipts($request->iduser);
        if (count($documents) > 0) {
            return response()->json($documents, 200);
        }
        return response()->json([], 200);
    }

    public function getAllCashReceipts(Request $request)
    {


        if (!isset($request->iduser) || $request->iduser == null) {
            return response()->json(['error' => 'El campo iduser es obligatorio'], 400);
        }

        if ($request->dateRange[0] == null || $request->dateRange[1] == null) {
            return response()->json(['error' => 'El campo dateRange  es obligatorio'], 400);
        }

        $documents = $this->documentRepository->getCashReceiptsAll($request->iduser, $request->idWarehouse, $request->dateRange);
        if (count($documents) > 0) {
            return response()->json($documents, 200);
        }
        return response()->json([], 200);
    }

    /**

     * @param $thread_id
     * @return mixed
     */
    public function getDocumentsForThreadId($thread_id)
    {
        $documents = $this->documentRepository->getDocumentsForThreadId($thread_id);
        return $this->sendResponse($documents, 'Documents retrieved successfully');
    }

    public function finalizeDocument(Request $request)
    {
        $response = $this->documentsService->finalizeDocument($request);
        return response()->json($response->data, $response->code);
    }

    public function finalizeUpdateDocument(Request $request)
    {
        $response = $this->documentsService->finalizeUpdateDocument($request);
        return response()->json($response->data, $response->code);
    }

    public function getInvoiceReceipt(Request $request)
    {
        if (!isset($request->iduser) || $request->iduser == null) {
            return response()->json(['error' => 'El campo iduser es obligatorio'], 400);
        }

         if (!isset($request->rc) || $request->rc == null) {
             return response()->json(['error' => 'El campo rc es obligatorio'], 400);
         }

        return response()->json($this->documentRepository->getInvoiceReceipt($request->toArray()), 200);
    }

    /**
     * Este metodo se encarga de validar los pedidos
     * que cumplan con ciertas condiciones, y dependiendo
     * del $status retorna cierta informacion.
     *
     * @param Request $request
     * @param string $status
     * @return \Illuminate\Http\JsonResponse
     * @author Kevin Galindo
     */
    public function getListOrder($status, Request $request)
    {
        // Se guarda el tipo de estado para realizar la busqueda. (mayusculas)
        $status = strtoupper($status);

        // Se valida el tipo de estado para devolver la informacion.
        switch ($status) {
            case 'RETENIDOS':
            case 'FACTURAR':
                $res = $this->documentsService->getOrderRejected($status, $request);
                break;

            case 'AUTORIZADOS':
            case 'RECHAZADOS':
                $res = $this->documentsService->getOrderByStatusAuthorized($status, $request);
                break;

            default:
                $res = [
                    'data' => [
                        'message' => 'Filtro no valido'
                    ],
                    'code' => '400'
                ];
                break;
        }

        foreach ($res['data'] as $key => $value) {
            $contact_id = isset($value->contact_id) ? $value->contact_id : $value['contact_id'];
            $total_receivable = 0;
            $receivable = $this->documentsBalanceService->getTrackingByContactId($contact_id);

            foreach ($receivable['data'] as $key => $v) {
                $total_receivable += $v->receivable[0]->invoice_balance;
            }

            if (isset($value->total_receivable)) {
                $value->total_receivable = $total_receivable;
            } else {
                $value['total_receivable'] = $total_receivable;
            }
        }

        // Se retorna la respuesta.
        return response()->json($res['data'], $res['code']);
    }

    /**
     *
     * Este metodo se encarga de actualizar
     * la informacion de un pedido al ser aprobado
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Kevin Galindo
     */
    public function changeStatusOrder(Request $request)
    {
        $res = $this->documentsService->changeStatusOrder($request);
        return response()->json($res['data'], $res['code']);
    }

    /*

     * Este metodo se encarga de retener un pedido
     * que ya haya sido autorizado o rechazado
     *
     * @param Request $request
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function retainOrder($id, Request $request)
    {
        $res = $this->documentsService->retainOrderById($id, $request);
        return response()->json($res['data'], $res['code']);
    }

    /**
     *
     * @author Santiago Torres | Kevin Galindo
     */
    public function validateOrderStatus()
    {
        $res = $this->documentsService->getDespatchInvoiced();
        return response()->json($res['data'], $res['code']);
    }

    /**
     *
     * @author Kevin Galindo
     */
    public function despatchInvoiceList(Request $request)
    {
        $res = $this->documentsService->getDespatchInvoiced($request);
        return response()->json($res['data'], $res['code']);
    }

    /**
     * Este metodo se encarga de actualizar la factura
     * que se esta despachando.
     *
     * @param $id
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceInformation($id, Request $request)
    {
        $res = $this->documentsService->updateDespatchInvoiceInformation($id, $request);
        return response()->json($res['data'], $res['code']);
    }

    /**
     * Este metodo se encarga de cambiar el estado de
     * la factura a despachado.
     *
     * @param $id
     * @param Request $request
     * @return array
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceStatus($id, Request $request)
    {
        $res = $this->documentsService->updateDespatchInvoiceStatus($id, $request);
        return response()->json($res['data'], $res['code']);
    }

    public function searchDocument_(Request $request)
	{
		$res = $this->documentsService->searchDocument_($request);
        return response()->json($res->data, $res->code);
	}

    /**
     * Este metodo se encarga de cambiar el estado de
     * la factura a despachado de forma masiva.
     *
     * @param Request $request
     * @return JsonResponse
     * @author Kevin Galindo
     */
    public function updateDespatchInvoiceStatusMasive(Request $request)
    {
        $res = $this->documentsService->updateDespatchInvoiceStatusMasive($request);
        return response()->json($res['data'], $res['code']);
    }

    /**
     *
     * crear traslados
     *
     * @param Request $request
     * @return JsonResponse
     * @author Santiago Torres
     */
    public function createTransfer(Request $request)
    {
        $response = $this->documentsService->createTransfer($request);
        return response()->json($response->data, $response->code);
    }

    /**
     *
     * crear traslados
     *
     * @param Request $request
     * @return JsonResponse
     * @author Santiago Torres
     */
    public function createTransferRot(Request $request)
    {
        $response = $this->documentsService->createTransferRot($request);
        return response()->json($response->data, $response->code);
    }

    /**
     *
     * Este metodo se encarga de buscar todos los
     * documentos segun su modelo y otros filtros
     *
     * @author Kevin Galindo
     */

    public function searchDocument(Request $request)
    {
        $res = $this->documentsService->getSearchDocument($request);
        return $res;
    }

    /**
     * Consulta para traer las entradas por traslado de una bodega
     * @param $params sede y bodega para realizar la consulta
     * @author Santiago Torres
     */
    public function getTransferEntry(Request $request)
    {
        return $this->sendResponse($this->documentRepository->searchTransferEntry($request), 'si');
    }

    /**
     *
     *
     */
    public function generateFile()
    {
        // return [
        return $this->documentRepository->getInfoForFile();
        // $this->documentRepository->getInfoForFile(),
        // $this->contactWarehouseRepository->getInfoForFile()];
    }

    /**
     *
     * Este metodo se encarga de calcular los valores
     * de un documento
     *
     * @author Kevin Galindo
     */
    public function recalculateTotalsDocument(Request $request)
    {
        $document = (object) $request->document;
        $document->documentsProducts = [];

        foreach ($document->documents_products as $key => $item) {
            $document->documentsProducts[] = (object) $item;
        }

        $res = $this->documentsService->calculateTotalsDocument($document, $request);

        $res->documents_products = $res->documentsProducts;

        unset($res->documentsProducts);

        return response()->json($res, 200);
    }

    /**
     * envial el pdf por correo
     *
     * @param SendMailRequest $request
     * @return string
     * @throws \Exception
     * @author santiago torres | kevin galindo
     */

    public function sendmail(SendMailRequest $request)
    {
        $data = $request->all();
        $user = JWTAuth::parseToken()->toUser();

        $data['from'] = $user->email;
        $data['nameFrom'] = $user->name . ' ' . $user->surname;

        foreach ($data['to'] as $to) {
            if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('Correo no válido', 422);
            }
        }

        Mail::to($data['to'])->send(new SendMail($data));
        return response()->json('Mensaje enviado exitosamente');
    }

    /**
     * Este metodo se encarga de cambiar el estado del
     * documento a "CERRADO"
     *
     * @author Kevin Galindo
     */
    public function closeDocument($id, Request $request)
    {
        $data = $this->documentsService->closeDocument($id, $request);
        return response()->json($data['data'], $data['code']);
    }

    /**
     * Este metodo se encarga de calcular
     * la disponibilidad
     *
     * @author Kevin Galindo
     */
    public function calculateAvailability(Request $request)
    {
        $data = $this->documentsService->calculateAvailability($request);
        return response()->json($data);
    }

    /**
     * Este metodo se encarga de cargar los documentos
     * de forma masiva.
     *
     * @author Kevin Galindo
     */
    public function loadDocumentMasive(Request $request)
    {
        $data = $this->documentsService->loadDocumentMasive($request);
        return response()->json($data);
    }

    public function clearErp(Request $request)
    {
        $response = $this->documentsService->clearErp($request);
        return response()->json($response->data, $response->code);
    }

    public function aaaaaaaa(Request $request)
    {
        return 'ao';
    }

    /**
     * consulta para obtener los documentos que afectan
     * la disponibilidad de un producto
     *
     * @author santiago torres
     */
    public function getDocumentsAbailability(Request $request)
    {
        $documents = $this->documentsService->getDocumentsAbailability($request);
        return $this->sendResponse($documents, 'Base Documents retrieved successfully');
    }


    /*
    * saldos iniciales masivos
    */
    public function massiveOpeningBalances(Request $request)
    {
        set_time_limit(0);
        $res = $this->documentsService->massiveOpeningBalances($request);
        return response()->json($res, 200);
    }

    /**
     *
     * Este metodo se encarga de corregir los documentyos
     */
    public function correctionDocuments(Request $request)
    {
        $res = $this->documentsService->correctionDocuments($request);
        return response()->json($res, 200);
    }

    /**
     * Este metodo se encarga de descargar la informacion
     * con los estados de los documentos
     *
     * @author Kevin Galindo
     */
    public function downloadDocumentsByState(Request $request)
    {
        $res = $this->documentsService->downloadDocumentsByState($request);
        return response()->json($res, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sellerOrders(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $documents = $this->documentsService->getOrdersManagement($request, $user);

        return $this->sendResponse($documents, 'Documents retrieved successfully');
    }

    public function updateDocCruce(Request $request)
    {
        $res = $this->documentsService->updateDocCruce($request);
        return response()->json($res, 200);
    }

    public function setReturnedDocument($id)
    {
        return $this->documentsService->setReturnedDocument($id);
    }

    public function test(Request $request)
    {

        /*$res = $this->documentsService->listBaseDocuments($request);
        return response()->json($res, 200);


        set_time_limit(0);

        $documents = Document::where('in_progress', false)->orderBy('id')->get();


        foreach ($documents as $key => $document) {
            $request->request->add([
                'document_id' => $document->id,
                'model_id' => $document->model_id,
            ]);

            $res = $this->documentsService->documentUpdateState($request->document_id, $request);
        }

        return [];*/

        $res = $this->documentsService->documentUpdateState($request->document_id);
        return response()->json($res, 200);
    }

    public function buscrarDP()
    {
        return $this->documentsService->BuscardPTrash();
    }

    public function getDocumentByauthorize(Request $request)
    {
        $res = $this->documentsService->getDocumentByauthorize($request);
        return response()->json($res, 200);
    }

    public function search(Request $request)
    {
        return $this->documentsService->getHour0();
    }

    public function testt($id)
    {
        // return "testt";
        $response = $this->documentsService->getTransfer($id);
        return response()->json($response->data, $response->code);
    }

    public function getTransfer($id)
    {
        return 'si';
        $response = $this->documentsService->getTransfer($id);
        return response()->json($response->data, $response->code);
    }

    public function documentsMissingBackOrder(Request $request)
    {
        $response = $this->documentsService->documentsMissingBackOrder($request);
        return response()->json($response, 200);
    }


    public function getDocumentsToAuthorize()
    {
        return $this->sendResponse($this->documentsService->getDocumentsToAuthorize(), '');
    }

    public function transferMedellin(Request $request)
    {
        $response = $this->documentsService->createTransferMedellin($request);
        return response()->json($response, 200);
    }

    public function printLabels(Request $request)
    {
        $res = $this->documentsService->getDespatchInvoiced($request);
        $response = $this->documentsService->createCsvLabels($res, $request);
        return response()->stream($response['file'], $response['status'], $response['headers']);
    }

    /**
     * Genera pdf de pickin
     *
     * @author Santiago
     */
    public function excelPickin(Request $date)
    {

        if ($date->despatch) {
            $date->merge(['perPage' => count($date->despatch)]);
        }
        //obtener info de las facturas para despachar
        $res = $this->documentsService->getDespatchInvoicedList($date);

        //organiza la informacion necesaria para el reporte
        $pickin = $this->documentRepository->listProductsPickin($res["data"]);
        //return $pickin;

        $pickinorderbylocation = [];
        foreach ($pickin as $key => $value) {

            $collection = collect($value);

            $orderbylocation = $collection->sortBy('location')->values()->all();

            $pickinorderbylocation[$key] = $orderbylocation;
        }


        $response = $this->documentsService->createCsvPickin($pickinorderbylocation);
        return response()->stream($response['file'], $response['status'], $response['headers']);
    }


    public function detailDocument($id, Request $request)
    {
        $document = $this->documentRepository->detailDocument($id, $request);
        return $document;
    }

    /**
     *
     */
    public function AccountingDocument(Document $document)
    {
        $this->documentAccountingService->accountingInventoryOperationsByDocument($document);
    }

    /**
     * Este metodo se utiliza para calcular y
     * actualizar los valores del documento, ya sean
     * totales o retenciones.
     *
     * @author Kevin Galindo
     */
    public function updateDocumentValues(Request $request)
    {
        ini_set('memory_limit', '2048M');

        $data = $this->documentsService->updateDocumentValues($request);
        return response()->json($data['data'], $data['code']);
    }

    /**
     * Este metodo se utiliza para calcular y
     * actualizar las retenciones del documento.
     *
     * @author Kevin Galindo
     */
    public function updateDocumentValuesRetention(Request $request)
    {
        $data = $this->documentsService->updateDocumentValuesRetention($request->document_id, $request);
        return response()->json($data['data'], $data['code']);
    }



    public function testClearErp(Request $request)
    {
        $response = $this->documentsService->clearErp($request);
        // return response()->json($response->data, $response->code);
        return 'ok';
    }

    public function auditDocumentForId($id, Request $request)
    {

        $documentsProducts = collect();
        $productsTaxes = collect();
        $auditsDocument =  collect();
        $auditsDocumentsProducts = collect();
        $auditsDocumentsProductsTaxes = collect();


        // obtengo el documento
        $document = $this->detailDocument($id, $request);
        // verifico que exista un documento
        if ($document->isNotEmpty()) {

            //recorro mi documento
            $document->each(function ($doc, $key) use ($documentsProducts, $productsTaxes) {

                //  obtengo los id de los productos asociados
                $doc->documentsProducts->each(function ($documentproduct, $key) use ($documentsProducts, $productsTaxes) {
                    $documentsProducts->push($documentproduct->id);

                    //  obtengo el id del impuesto para cada producto
                    $documentproduct->documentsProductsTaxes->each(function ($documentProductTax, $key) use ($documentsProducts, $productsTaxes) {
                        $productsTaxes->push($documentProductTax->id);
                    });
                });
            });
        }

        // obtengo auditorias del documento
        $auditsDocument->push($this->documentRepository->auditsDocument($id));

        // obtengo auditorias de los productos asociados al documento
        foreach ($documentsProducts as $key => $documentProduct) {
            $auditsDocumentsProducts->push($this->documentRepository->auditsDocumentProducts($documentProduct));
        }

        // obtengo auditorias de los impuestos asociados al producto
        foreach ($productsTaxes as $key => $documentProductTax) {
            $auditsDocumentsProductsTaxes->push($this->documentRepository->auditsDocumentProductsTax($documentProductTax));
        }

        foreach ($auditsDocument->collapse() as $key => $ad) {
            $documentsProducts = [];
            $min_created_at = Carbon::parse($ad->created_at)->addMinutes(-15);
            $max_created_at = Carbon::parse($ad->created_at)->addMinutes(15);

            //  organizo documents products
            foreach ($auditsDocumentsProducts->collapse() as $key => $adp) {
                // \Log::info(gettype($adp->evento));
                $created_at_document_product = Carbon::parse($adp->created_at);
                foreach ($auditsDocumentsProductsTaxes->collapse() as $key => $adpt) {
                    $created_at_document_product_tax = Carbon::parse($adpt->created_at);


                     if (
                         $created_at_document_product_tax->between($min_created_at, $max_created_at) &&
                         $ad->evento == $adpt->evento &&
                         $ad->user_full_name == $adpt->user_full_name
                         && $adp->auditable_id == $adpt->document_product_id
                     ) {

                        $adp->new_values = str_replace('}', ',', ($adp->new_values));
                        $adp->new_values .=   str_replace('{','',$adpt->new_values) ;
                        $adp->old_values = str_replace('}', ',', ($adp->old_values));
                        $adp->old_values .=   str_replace('{','',$adpt->old_values) ;
                    }
                }

                if (
                    $created_at_document_product->between($min_created_at, $max_created_at) &&
                    $ad->evento == $adp->evento &&
                    $ad->user_full_name == $adp->user_full_name
                ) {
                    $documentsProducts[] = $adp;
                }
            }

            $ad->documentsProducts = $documentsProducts;
        }

        return response()->json([
            'auditsDocument' => $auditsDocument,
        ], 200);
    }


    /**
     *
     */
    public function documentoMargenUtilidad(Request $request)
    {
        $data = $this->documentsService->documentoMargenUtilidad($request);
        return response()->json($data['data'], $data['code']);
    }

    /**
     * Este metodo se encarga de anular un documento
     * segun su ID
     *
     * @author Kevin Galindo
     */
    public function cancelDocument($id, Request $request)
    {
        $data = $this->documentsService->cancelDocument($id, $request);
        return response()->json($data['data'], $data['code']);
    }

    public function updateBackOrderAndMissingValueDocument(Request $request)
    {
        $data = $this->documentsService->updateBackOrderAndMissingValueDocument($request->document_id, $request->model_id);
        return response()->json($data, 200);
    }

    /**
     * Controla las peticiones Http para generar el reporte de documentos autorizados
     *
     * @param Request $request
     * @return mixed
     * @author Jhon García
     */
    public function generateReportAuthorizedDocuments(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $path_report = $this->documentsService->generateReportAuthorizedDocuments($request, $user);

        if ($path_report != ''){
        return Storage::download('/public/reports/'.$path_report);
        }
    }

    public function documentDownloadFile($file_id)
    {
        $documentFile = DocumentFile::find($file_id);
        if ($documentFile) {
            return Storage::download("/public/$documentFile->path", $documentFile->name);
        }
    }

    public function getFinishedDocumentsPendingToSend(){
        $response = $this->documentRepository->getFinishedDocumentsPendingToSend();

        return response()->json($response->toArray(), 200);
    }

    /**
     * finaliza documentos contables
     */
    public function finalizeAccountingDocument(Request $request)
    {
        $response = $this->documentsService->finalizeAccountingDocument($request);
        return response()->json($response->data, $response->code);
    }

    /**
     *
     */
    public function editDocument($id)
    {
        $response = $this->documentsService->editDocument($id);
        return response()->json($response->data, $response->code);
    }

    /**
     *
     *
     */
    public function UpdateAccountingDocument(Request $request)
    {
        $response = $this->documentsService->UpdateAccountingDocument($request);
        return response()->json($response->data, $response->code);
    }

    /**
     * @param Document $document
     * @return JsonResponse
     * @throws \Exception
     */
    public function sendFinishedDocument(Document $document){

        $response = $this->documentsService->sendFinishedDocument($document->id);

        return response()->json($response->toArray(), 200);
    }



    public function getItemizedCashReceipts(Request $request)
    {
        if (!isset($request->iduser) || $request->iduser == null) {
            return response()->json(['error' => 'El campo iduser es obligatorio'], 400);
        }

        // if (!isset($request->rc) || $request->rc == null) {
        //     return response()->json(['error' => 'El campo rc es obligatorio'], 400);
        // }

        return response()->json($this->documentRepository->getItemizedCashReceipts($request->toArray()), 200);
    }

    public function getReportSales()
    {
        $sales = collect(DB::select('select * from public.fn_reportsales() where total_value_brut_first_month > 0 or total_value_brut_second_month > 0 or total_value_brut_third_month > 0 or total_value_brut_fourth_month > 0 or total_value_brut_fifth_month > 0 or total_value_brut_sixth_month > 0;'));

        $sales->map(function($sale){

            $sale->total_value_brut_first_month = (int) $sale->total_value_brut_first_month;
            $sale->total_value_brut_second_month = (int) $sale->total_value_brut_second_month;
            $sale->total_value_brut_third_month = (int) $sale->total_value_brut_third_month;
            $sale->total_value_brut_fourth_month = (int) $sale->total_value_brut_fourth_month;
            $sale->total_value_brut_fifth_month = (int) $sale->total_value_brut_fifth_month;
            $sale->total_value_brut_sixth_month = (int) $sale->total_value_brut_sixth_month;

            $sale->total_cost_value_first_month = (int) $sale->total_cost_value_first_month;
            $sale->total_cost_value_second_month = (int) $sale->total_cost_value_second_month;
            $sale->total_cost_value_third_month = (int) $sale->total_cost_value_third_month;
            $sale->total_cost_value_fourth_month = (int) $sale->total_cost_value_fourth_month;
            $sale->total_cost_value_fifth_month = (int) $sale->total_cost_value_fifth_month;
            $sale->total_cost_value_sixth_month = (int) $sale->total_cost_value_sixth_month;

            $sale->total_utility_first_month = (int) ($sale->total_value_brut_first_month - $sale->total_cost_value_first_month);
            $sale->total_utility_second_month = (int) ($sale->total_value_brut_second_month - $sale->total_cost_value_second_month);
            $sale->total_utility_third_month = (int) ($sale->total_value_brut_third_month - $sale->total_cost_value_third_month);
            $sale->total_utility_fourth_month = (int) ($sale->total_value_brut_fourth_month - $sale->total_cost_value_fourth_month);
            $sale->total_utility_fifth_month = (int) ($sale->total_value_brut_fifth_month - $sale->total_cost_value_fifth_month);
            $sale->total_utility_sixth_month = (int) ($sale->total_value_brut_sixth_month - $sale->total_cost_value_sixth_month);
        });

        return $sales;
    }

    public function getReportSalesProducts(Request $request)
    {
        $contact = Contact::select('id')
            ->selectRaw("CONCAT(name, ' ', surname) as name")
            ->whereRaw("TRIM(CONCAT(name, ' ', surname)) ILIKE ?", [ $request->contact_id . '%'])
            ->first();

        $sales = collect(DB::select('select * from public.fn_reportsales_products(?) where quantity_first_month > 0 or quantity_second_month > 0 or quantity_third_month > 0 or quantity_fourth_month > 0 or quantity_fifth_month > 0 or quantity_sixth_month > 0;', [$contact->id]));

        $sales->map(function($sale){
            $sale->quantity_first_month = (int) $sale->quantity_first_month;
            $sale->quantity_second_month = (int) $sale->quantity_second_month;
            $sale->quantity_third_month = (int) $sale->quantity_third_month;
            $sale->quantity_fourth_month = (int) $sale->quantity_fourth_month;
            $sale->quantity_fifth_month = (int) $sale->quantity_fifth_month;
            $sale->quantity_sixth_month = (int) $sale->quantity_sixth_month;

            $sale->total_value_brut_first_month = (int) $sale->total_value_brut_first_month;
            $sale->total_value_brut_second_month = (int) $sale->total_value_brut_second_month;
            $sale->total_value_brut_third_month = (int) $sale->total_value_brut_third_month;
            $sale->total_value_brut_fourth_month = (int) $sale->total_value_brut_fourth_month;
            $sale->total_value_brut_fifth_month = (int) $sale->total_value_brut_fifth_month;
            $sale->total_value_brut_sixth_month = (int) $sale->total_value_brut_sixth_month;

            $sale->total_cost_value_first_month = (int) $sale->total_cost_value_first_month;
            $sale->total_cost_value_second_month = (int) $sale->total_cost_value_second_month;
            $sale->total_cost_value_third_month = (int) $sale->total_cost_value_third_month;
            $sale->total_cost_value_fourth_month = (int) $sale->total_cost_value_fourth_month;
            $sale->total_cost_value_fifth_month = (int) $sale->total_cost_value_fifth_month;
            $sale->total_cost_value_sixth_month = (int) $sale->total_cost_value_sixth_month;

            $sale->total_utility_first_month = (int) ($sale->total_value_brut_first_month - $sale->total_cost_value_first_month);
            $sale->total_utility_second_month = (int) ($sale->total_value_brut_second_month - $sale->total_cost_value_second_month);
            $sale->total_utility_third_month = (int) ($sale->total_value_brut_third_month - $sale->total_cost_value_third_month);
            $sale->total_utility_fourth_month = (int) ($sale->total_value_brut_fourth_month - $sale->total_cost_value_fourth_month);
            $sale->total_utility_fifth_month = (int) ($sale->total_value_brut_fifth_month - $sale->total_cost_value_fifth_month);
            $sale->total_utility_sixth_month = (int) ($sale->total_value_brut_sixth_month - $sale->total_cost_value_sixth_month);
        });

        return response()->json([
            'contact' => $contact,
            'sales' => $sales
        ]);
    }

    /**
     * Este metodo se encarga de
     * devolver el historial de un documento por su id
     *
     * @author Kevin Galindo
     */
    public function documentsThreadHistoryByDocumentId(Request $request)
    {
        $response = $this->documentsService->documentsThreadHistoryByDocumentId($request);
        return response()->json([
            'documents' => $response['data']
        ], 200);
    }

    public function getDocumentsWms()
    {
        ini_set('max_execution_time', '1200');
        $res = $this->documentsService->createDocumentSoapWms();
        return response()->json($res['data']);
    }

    public function recalculateAccountingDocument($id)
    {
        $response = $this->documentsService->recalculateAccountingDocument($id);
        return response()->json([
            $response
        ], 200);
    }

    public function transferBranchoffice(Request $request){

        $response = $this->documentsService->createDocumentsTransfersWMS($request);
		//dd($response);
        return response()->json([ 'infowms' => $response]);
    }

    public function movementsWms(Request $request){

        $response = $this->documentsService->createDocumentsMovementsWMS($request);
		//dd($response);
        return response()->json([ 'infowms' => $response]);
    }

    /**
     * Este metodo se encarga de realizar la contabilidad
     * a varios documentos segon los filtros.
     *
     * @author Kevin Galindo
     */
    public function accountingInventoryOperations(Request $request)
    {
        set_time_limit(0);
        $response = $this->documentAccountingService->accountingInventoryOperations($request);
        return response()->json($response['message'], $response['code']);
    }

    /**
     * Este metodo se encarga de realizar la contabilidad
     * a varios documentos segon los filtros.
     *
     * @author Kevin Galindo
     */
    public function accountingInventoryOperationsCountDocuments(Request $request)
    {
        $response = $this->documentAccountingService->accountingInventoryOperationsCountDocuments($request);
        return response()->json([
            'documents' => $response
        ], 200);
    }

    public function createDocumentBalanceCrossing(Request $request)
    {
        $document = $this->documentsService->createDocumentBalanceCrossing($request);
        return $this->sendResponse($document, 'Document retrieved successfully');
    }

}
