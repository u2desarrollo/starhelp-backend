<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\CaseRepository;
use App\Services\CaseService;


class CaseApiController extends Controller
{

    private $caseRepository;
    private $caseService;

    public function __construct(
        CaseRepository $caseRepository,
        CaseService $caseService

    )
    {
        $this->caseRepository = $caseRepository;
        $this->caseService = $caseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = $this->caseRepository->findAll($request);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->caseService->createCase($request);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $response = $this->caseRepository->findById($id, $request);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
