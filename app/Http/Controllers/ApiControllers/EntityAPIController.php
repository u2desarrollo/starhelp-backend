<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateEntityAPIRequest;
use App\Http\Requests\ApiRequests\UpdateEntityAPIRequest;
use App\Entities\Entity;
use App\Repositories\EntityRepository;
use App\Repositories\EntityWareHouseRepository;
use App\Traits\LoadImageTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EntityController
 * @package App\Http\Controllers\ApiControllers
 */
class EntityAPIController extends AppBaseController
{

    use LoadImageTrait;

    /** @var  EntityRepository */
    private $entityRepository;
    private $entityWareHouseRepository;

    public function __construct(EntityRepository $entityRepo, EntityWareHouseRepository $entityWareHouseRepo)
    {
        $this->entityRepository = $entityRepo;
        $this->entityWareHouseRepository = $entityWareHouseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/entities",
     *      summary="Get a listing of the Entities.",
     *      tags={"Entity"},
     *      description="Get all Entities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Entity")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {

        $this->entityRepository->pushCriteria(new RequestCriteria($request));
        $this->entityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $entities = $this->entityRepository->findAll($request);

        return $this->sendResponse($entities->toArray(), 'Entities retrieved successfully');
    }

    /**
     * @param CreateEntityAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/entities",
     *      summary="Store a newly created Entity in storage",
     *      tags={"Entity"},
     *      description="Store Entity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Entity that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Entity")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEntityAPIRequest $request)
    {

        try {

            DB::beginTransaction();

            $input = $request->except('image_customer', 'image_provider', 'image_employee', 'warehouses');

            $input['image_customer'] = $this->saveImage($request->image_customer, 'entities/customers');
            $input['image_provider'] = $this->saveImage($request->image_provider, 'entities/providers');
            $input['image_employee'] = $this->saveImage($request->image_employee, 'entities/employees');

            $entities = $this->entityRepository->create($input);

            $warehouses = $request->warehouses;

            foreach ($warehouses as $warehouse) {
                $warehouse['entity_id'] = $entities->id;
                $this->entityWareHouseRepository->create($warehouse);
            }

            DB::commit();

            return $this->sendResponse($entities->toArray(), 'Entity saved successfully');

        } catch (\Exception $e) {

            DB::rollBack();

            return response($e->getMessage(), 500);

        }

    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/entities/{id}",
     *      summary="Display the specified Entity",
     *      tags={"Entity"},
     *      description="Get Entity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Entity $entity */
        $entity = $this->entityRepository->findWithoutFail($id);

        if (empty($entity)) {
            return $this->sendError('Entity not found');
        }

        return $this->sendResponse($entity->toArray(), 'Entity retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEntityAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/entities/{id}",
     *      summary="Update the specified Entity in storage",
     *      tags={"Entity"},
     *      description="Update Entity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Entity that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Entity")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEntityAPIRequest $request)
    {

        try {

            DB::beginTransaction();

            $input = $request->except('image_customer', 'image_provider', 'image_employee', 'warehouses');

//            if (isset($request->image_customer)) {
            $input['image_customer'] = $this->saveImage($request->image_customer, 'entities/customers');
//            }
//            if (isset($request->image_provider)) {
            $input['image_provider'] = $this->saveImage($request->image_provider, 'entities/providers');
//            }
//            if (isset($request->image_employee)) {
            $input['image_employee'] = $this->saveImage($request->image_employee, 'entities/employees');
//            }

            /** @var Entity $entity */
            $entity = $this->entityRepository->findWithoutFail($id);

            if (empty($entity)) {
                return $this->sendError('Entity not found');
            }

            $entity = $this->entityRepository->update($input, $id);

            $warehouses = $request->warehouses;

            foreach ($warehouses as $warehouse) {
                $this->entityWareHouseRepository->create($warehouse);
            }


            DB::commit();

            return $this->sendResponse($entity->toArray(), 'Entity updated successfully');

        } catch (\Exception $e) {

            DB::rollBack();

            return response($e->getMessage(), 500);

        }


    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/entities/{id}",
     *      summary="Remove the specified Entity from storage",
     *      tags={"Entity"},
     *      description="Delete Entity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Entity $entity */
        $entity = $this->entityRepository->findWithoutFail($id);

        if (empty($entity)) {
            return $this->sendError('Entity not found');
        }

        $entity->delete();

        return $this->sendResponse($id, 'Entity deleted successfully');
    }

    public function validateImages()
    {

    }
}
