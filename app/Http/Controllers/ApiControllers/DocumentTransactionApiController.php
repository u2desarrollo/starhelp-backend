<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DocumentTransactionsRepository;
use App\Services\DocumentTransactionsService;


class DocumentTransactionApiController extends AppBaseController
{

    private $documentTransactionsService;
    private $documentTransactionsRepository;
    //
    public function __construct(DocumentTransactionsRepository $documentTransactionsRepository, DocumentTransactionsService $documentTransactionsService)
    {
        $this->documentTransactionsRepository = $documentTransactionsRepository;
        $this->documentTransactionsService = $documentTransactionsService;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->documentTransactionsService->store($request);
        return response()->json($response->data, $response->code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->documentTransactionsRepository->destroy($id);
        return response()->json($response->data, $response->code);
    }

    public function getTransactionsFromDocument($id)
    {
        $response = $this->documentTransactionsRepository->getTransactionsFromDocument($id);
        return response()->json($response->data, $response->code);
    }

    
    /**
     * @author Santiago Torres
     * 
     * Consulta libro auxiliar
     */
    public function auxiliaryBook(Request $request)
    {
        $response = $this->documentTransactionsService->auxiliaryBook($request);
        return response()->json([
            'data' => $response['data']
        ], 200);
    }

    /**
     * @author Haider Oviedo
     * 
     * Consulta Reporte Comision 
     */
    public function reportCommissions(Request $request)
    {
        set_time_limit(3000);
        $response = $this->documentTransactionsService->reportCommissions($request);
        return response()->json([
            'data' => $response['data']
        ], 200);
    }

    /**
     * lista de transacciones de cartera
     * @author Santiago Torres
     */
    public function getTransactionsWallet($id, Request $request)
    {
        $response = $this->documentTransactionsService->getTransactionsWallet($id, $request);
        return response()->json([
            'data' => $response
        ], 200);
    }

    /**
     * @author Santiago Torres
     * 
     * Consulta libro auxiliar por tercero
     */
    public function auxiliaryBookContact(Request $request)
    {
        $response = $this->documentTransactionsService->auxiliaryBookContact($request);
        return response()->json([
            'data' => $response['data']
        ], 200);
    }
}
