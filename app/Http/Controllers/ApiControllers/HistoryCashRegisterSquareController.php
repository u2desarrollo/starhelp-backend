<?php

namespace App\Http\Controllers\ApiControllers;


use App\Entities\HistoryCashRegisterSquare;
use App\Http\Controllers\AppBaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Response;
use JWTAuth;

/**
 * Class CityController
 * @package App\Http\Controllers\ApiControllers
 */

class HistoryCashRegisterSquareController extends AppBaseController
{

    /**
     * Listado del historial del reporte cuadre de caja
     * 
     * @author Kevin Galindo
     */
    public function index(Request $request)
    {
        $data = HistoryCashRegisterSquare::when($request->dateFrom, function($query) use ($request) {
            $query->where('dateFrom', '>=', $request->dateFrom.' 00:00:00');
        })
        ->orderBy('dateFrom')
        ->get();

        return response()->json($data, 200);
    }

    /**
     * Guardar historial del reporte cuadre de caja
     * 
     * @author Kevin Galindo
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), HistoryCashRegisterSquare::$rules);

        if ($validator->fails()) {
            return response()->json(['error' => 'No se creo el cuadre de caja.'], 400);
        }

        $input = $request->all();
        $user = JWTAuth::parseToken()->toUser();

        $input['dateFrom'] = $input['dateFrom']." ".$input['timeFrom'];
        $input['dateTo'] = $input['dateTo']." ".$input['timeTo'];
        $input['user_id'] = $user->id;

        $data = HistoryCashRegisterSquare::create($input);

        return response()->json($data, 200);
    }

    /**
     * Actualizar un historial del reporte cuadre de caja
     * 
     * @author Kevin Galindo
     */
    public function update(Request $request, $id)
    {
        $historyCashRegisterSquare = HistoryCashRegisterSquare::find($id);

        if (!$historyCashRegisterSquare) {
            return response()->json(['error' => 'No se encontro el cuadre de caja.'], 404);
        }

        $validator = Validator::make($request->all(), HistoryCashRegisterSquare::$rules);

        if ($validator->fails()) {
            return response()->json(['error' => 'No se creo el cuadre de caja.'], 400);
        }

        $input = $request->all();
        $user = JWTAuth::parseToken()->toUser();

        $input['dateFrom'] = $input['dateFrom']." ".$input['timeFrom'];
        $input['dateTo'] = $input['dateTo']." ".$input['timeTo'];
        $input['user_id'] = $user->id;

        $historyCashRegisterSquare->update($input);

        return response()->json([
            'message' => 'Registro actualizado.'
        ], 200);
    }

    /**
     * Elimina un historial del reporte cuadre de caja
     * 
     * @author Kevin Galindo
     */
    public function destroy(Request $request, $id)
    {
        $historyCashRegisterSquare = HistoryCashRegisterSquare::find($id);

        if (!$historyCashRegisterSquare) {
            return response()->json(['error' => 'No se encontro el cuadre de caja.'], 404);
        }

        $historyCashRegisterSquare->delete();

        return response()->json($historyCashRegisterSquare, 200);
    }

}
