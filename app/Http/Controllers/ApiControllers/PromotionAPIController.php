<?php

namespace App\Http\Controllers\ApiControllers;


use App\Http\Requests\ApiRequests\CreateTaxAPIRequest;
use App\Http\Requests\ApiRequests\UpdateTaxAPIRequest;
use App\Entities\Tax;
use App\Jobs\CalculatePromotionsJob;
use App\Repositories\TaxRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PromotionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PromotionAPIController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $promotionRepository;

    public function __construct(PromotionRepository $promotionRepo)
    {
        $this->promotionRepository = $promotionRepo;
    }

    public function index(Request $request)
    {
        $promotions = $this->promotionRepository->findAll($request);

        return $this->sendResponse($promotions, 'Taxes retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->promotionRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotion = $this->promotionRepository->createPromotion($request);

//        CalculatePromotionsJob::dispatch();

        return $this->sendResponse($promotion, 'Taxes retrieved successfully');
    }


    public function show($id, Request $request)
    {
        // $this->promotionRepository->pushCriteria(new RequestCriteria($request));
        // $this->promotionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotions = $this->promotionRepository->findForId($id, $request);

        return $this->sendResponse($promotions, 'Taxes retrieved successfully');
    }

    public function getProductByParam($param)
    {
        $products = $this->promotionRepository->getProductByParam($param);
        return $this->sendResponse($products, 'Taxes retrieved successfully');

    }

    public function update(Request $request, $id)
    {
        $promotions = $this->promotionRepository->updatePromotion($request, $id);

//        CalculatePromotionsJob::dispatch();

        return $this->sendResponse($promotions, 'Taxes retrieved successfully');
    }

    public function destroy($id)
    {
        $promotion = $this->promotionRepository->deletePromotion($id);

//        CalculatePromotionsJob::dispatch();
        return $this->sendResponse($promotion, 'Taxes retrieved successfully');
    }

    /**
     * Obtiene las promociones activas a la fecha
     *
     * @return array
     */
    public function activePromotions(Request $request)
    {
        $promotions = $this->promotionRepository->activePromotions($request);

        $promotions->map(function ($item, $key) {
            $item->comply = false;
            $item->specific_product = null;
            return $item;
        });

        return $this->sendResponse($promotions, 'Promotions retrieved successfully');
    }

    /**
     *
     * Obtiene la imagen del producto
     *
     * @author Kevin Galindo
     */
    public function getImage($promotion_id)
    {

        $promotion = $this->promotionRepository->find($promotion_id);

        if (empty($promotion)) {
            return $this->sendError('Promotion not found');
        }

        $file = Storage::disk('local')->get('/promotions/' . $promotion->image);
        return new Response($file, 200);
    }

    public function findByIdForViewAndEdit($id, Request $request)
    {
        $promotions = $this->promotionRepository->findByIdForViewAndEdit($id, $request);
        return $this->sendResponse($promotions, 'Taxes retrieved successfully');
    }

    public function updatePromotions(Request $request)
    {
        CalculatePromotionsJob::dispatch();
    }
}
