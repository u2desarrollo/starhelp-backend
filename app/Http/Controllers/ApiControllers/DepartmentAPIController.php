<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateDepartmentAPIRequest;
use App\Http\Requests\ApiRequests\UpdateDepartmentAPIRequest;
use App\Entities\Department;
use App\Repositories\DepartmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DepartmentController
 * @package App\Http\Controllers\ApiControllers
 */

class DepartmentAPIController extends AppBaseController
{
    /** @var  DepartmentRepository */
    private $departmentRepository;

    public function __construct(DepartmentRepository $departmentRepo)
    {
        $this->departmentRepository = $departmentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->departmentRepository->pushCriteria(new RequestCriteria($request));
        $this->departmentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $departments = $this->departmentRepository->getDepartment($request);

        return $this->sendResponse($departments->toArray(), 'Departments retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var Department $department */
        $department = $this->departmentRepository->findWithoutFail($id);

        if (empty($department)) {
            return $this->sendError('Department not found');
        }

        return $this->sendResponse($department->toArray(), 'Department retrieved successfully');
    }
}
