<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateCustomerPromotionAPIRequest;
use App\Http\Requests\ApiRequests\UpdateCustomerPromotionAPIRequest;
use App\Entities\CustomerPromotion;
use App\Repositories\CustomerPromotionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CustomerPromotionController
 * @package App\Http\Controllers\ApiControllers
 */

class CustomerPromotionAPIController extends AppBaseController
{
    /** @var  CustomerPromotionRepository */
    private $customerPromotionRepository;

    public function __construct(CustomerPromotionRepository $customerPromotionRepo)
    {
        $this->customerPromotionRepository = $customerPromotionRepo;
    }

    /**
     * Display a listing of the CustomerPromotion.
     * GET|HEAD /customerPromotions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerPromotionRepository->pushCriteria(new RequestCriteria($request));
        $this->customerPromotionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $customerPromotions = $this->customerPromotionRepository->all();

        return $this->sendResponse($customerPromotions->toArray(), 'Customer Promotions retrieved successfully');
    }

    /**
     * Store a newly created CustomerPromotion in storage.
     * POST /customerPromotions
     *
     * @param CreateCustomerPromotionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerPromotionAPIRequest $request)
    {
        $input = $request->all();

        $customerPromotions = $this->customerPromotionRepository->create($input);

        return $this->sendResponse($customerPromotions->toArray(), 'Customer Promotion saved successfully');
    }

    /**
     * Display the specified CustomerPromotion.
     * GET|HEAD /customerPromotions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerPromotion $customerPromotion */
        $customerPromotion = $this->customerPromotionRepository->findWithoutFail($id);

        if (empty($customerPromotion)) {
            return $this->sendError('Customer Promotion not found');
        }

        return $this->sendResponse($customerPromotion->toArray(), 'Customer Promotion retrieved successfully');
    }

    /**
     * Update the specified CustomerPromotion in storage.
     * PUT/PATCH /customerPromotions/{id}
     *
     * @param  int $id
     * @param UpdateCustomerPromotionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerPromotionAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerPromotion $customerPromotion */
        $customerPromotion = $this->customerPromotionRepository->findWithoutFail($id);

        if (empty($customerPromotion)) {
            return $this->sendError('Customer Promotion not found');
        }

        $customerPromotion = $this->customerPromotionRepository->update($input, $id);

        return $this->sendResponse($customerPromotion->toArray(), 'CustomerPromotion updated successfully');
    }

    /**
     * Remove the specified CustomerPromotion from storage.
     * DELETE /customerPromotions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerPromotion $customerPromotion */
        $customerPromotion = $this->customerPromotionRepository->findWithoutFail($id);

        if (empty($customerPromotion)) {
            return $this->sendError('Customer Promotion not found');
        }

        $customerPromotion->delete();

        return $this->sendResponse($id, 'Customer Promotion deleted successfully');
    }
}
