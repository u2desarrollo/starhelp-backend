<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\DataSheetRepository;

//Services
use App\Services\DataSheetService;

//Entities
use App\Entities\Line;
use App\Entities\DataSheet;
use App\Entities\DataSheetLine;
use App\Entities\Parameter;

//Otros
use Validator;
use Illuminate\Support\Facades\DB;




class DataSheetController extends Controller
{
    protected $dataSheetRepository;
    protected $dataSheetService;

    /**
     * Constructor.
     *
     */
    public function __construct(
        DataSheetRepository $dataSheetRepo,
        DataSheetService $dataSheetServ
    ) {
        $this->dataSheetRepository = $dataSheetRepo;
        $this->dataSheetService = $dataSheetServ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->perPage ? $request->perPage : 10;
        $search = $request->search ? $request->search : null;
        $paginate = $request->paginate == 'true' ? true : false;

        $dataSheets = DataSheet::select('id', 'internal_name', 'visible_name', 'type_value', 'tooltip', 'paramtable_id')
        ->with(['type_value_name']);

        if ($search != null) {
            $dataSheets->where('internal_name', 'ilike', '%' . $search . '%')
            ->orWhere('visible_name', 'ilike', '%' . $search . '%')
            // TIPO
            ->orWhereHas('type_value_name', function ($query)  use ($search) {
                $query->where('name_parameter', 'ilike', '%' . $search . '%');
            })
            ->orderBy('internal_name', 'asc');
        } else {
            $dataSheets->orderBy('internal_name', 'asc');
        }
        
        if ($paginate) {
            $dataSheets = $dataSheets->paginate($perPage);
        }else {
            $dataSheets = $dataSheets->get();
        }

        return response()->json($dataSheets, 200);


        /*$dataSheets = $this->dataSheetRepository
            ->with('type_value_name')
            ->all();
        return response()->json($dataSheets, 200);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Guardamos la informacion
        $res = $this->dataSheetService->createDataSheet($request->all());

        //Retornamos un mensaje.
        return response()->json($res, $res['code'] ? $res['code'] : 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Buscamos el registro
        $data = $this->dataSheetRepository->findForId($id);

        //Validamos si exite y lo retornamos
        if ($data) return response()->json($data, 200);

        //En caso de que no exista retornamos un mensaje de error
        return response()->json(['message' => 'No se encontro el registro'], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Guardamos la informacion
        $res = $this->dataSheetService->updateDataSheet($request->all(), $id);

        //Retornamos un mensaje.
        return response()->json($res, $res['code'] ? $res['code'] : 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Buscamos el registro
        $dataSheet = $this->dataSheetRepository->findForId($id);

        //Validamos si exite y lo retornamos
        if (!$dataSheet) return response()->json(['message' => 'No se encontro el registro'], 400);

        //Eliminamos el registro
        $dataSheet->delete();

        return response()->json([
            'message' => 'Eliminado.',
            'data' => $dataSheet
        ], 200);
    }

    public function addDataSheetToLine(Request $request)
    {
        //Validamos la informacion
        $v = Validator::make($request->all(), [
            'line_id' => 'required|integer',
            'data_sheet_id' => 'required|integer',
            //'required' => 'required|integer'
        ]);

        //Validamos si ya existe el registro
        $register = DataSheetLine::where('line_id', $request->line_id)
            ->where('data_sheet_id', $request->data_sheet_id)
            ->count();

        if ($register > 0) {
            return response()->json(['message' => 'El registro ya existe..'], 400);
        }

        //En caso de no cumplir con las el "Validator" se retorna un mensaje de error.
        if ($v->fails()) return response()->json($v->errors(), 400);

        //Creamos el registro
        $dataSheetLine = DataSheetLine::create($request->all());

        //Retornamos un mensaje de exito
        return response()->json(['message' => 'Creado correctamente.', 'data' => $dataSheetLine], 201);
    }

    public function removeDataSheetToLine($id)
    {
        $dataSheetLine = DataSheetLine::find($id);

        //Validamos si exite y lo retornamos
        if (!$dataSheetLine) return response()->json(['message' => 'No se encontro el registro'], 400);

        //Eliminamos el registro
        $dataSheetLine->delete();

        return response()->json([
            'message' => 'Eliminado.',
            'data' => $dataSheetLine
        ], 200);
    }

    public function updateRequiredDataSheetToLine(Request $request, $id)
    {
        //Validamos la informacion
        $v = Validator::make($request->all(), [
            'required' => 'required|integer'
        ]);

        //En caso de no cumplir con las el "Validator" se retorna un mensaje de error.
        if ($v->fails()) return response()->json($v->errors(), 400);

        $dataSheetLine = DataSheetLine::find($id);

        $dataSheetLine->required = $request->required;
        $dataSheetLine->save();

        return response()->json([
            'message' => 'Registro Actualizado.',
            'data' => $dataSheetLine
        ], 200);
    }

    /**
     * Este metodo se encarga de devolver
     * los datos de la ficha de datos segun la linea
     *
     * @author kevin galindo
     */
    public function getDataSheetSelect($id)
    {
        $dataSheet = DB::select('select ds.*
                                from data_sheet ds
                                left join data_sheet_lines dsl on dsl.data_sheet_id = ds.id and dsl.line_id = ?
                                where dsl.id is not null', [$id]);

        foreach ($dataSheet as $key => $value) {
            if (!empty($value->paramtable_id)) {
                $parameter = Parameter::select('id', 'code_parameter', 'name_parameter')->where('paramtable_id', $value->paramtable_id)->get();
                $value->parameters = $parameter;
            }
        }

        return response()->json($dataSheet, 200);
    }

    public function getTypeValueDataSheet()
    {
        $parameter = Parameter::where('paramtable_id', 75)->get();
        return response()->json($parameter, 200);
    }

    /**
     * Este metodo valida si la ficha de dato ya existe.
     *
     * @author kevin galindo
     */
    public function checkExistDataSheet(Request $request)
    {
        $search = $request->search;
        if (!$search) {
            return response()->json([
                'message' => 'falta parametro "Buscar"',
                'code' => 400
            ], 400);
        }

        $dataSheet = DataSheet::where('visible_name', '=', $search)->first();
        if (!$dataSheet) {
            return response()->json([
                'message' => 'No exite una ficha de datos "' . $search . '"',
                'code' => 404
            ], 404);
        }

        return response()->json($dataSheet, 200);
    }

    public function listDataSheets()
    {
        $dataSheet = DataSheet::select('id', 'internal_name', 'visible_name', 'type_value', 'tooltip', 'paramtable_id')
            ->orderBy('visible_name', 'asc')
            ->get();

        return response()->json($dataSheet, 200);
    }

    /**
     * Este metodo se encarga de  cargar 
     * los atributos de forma masiva 
     * 
     * @author Kevin Galindo
     */
    public function loadNameDataSheetMasive(Request $request)
    {
        //Guardamos la informacion
        $res = $this->dataSheetService->loadNameDataSheetMasive($request);

        //Retornamos un mensaje.
        return response()->json($res, $res['code'] ? $res['code'] : 200);
    }
}