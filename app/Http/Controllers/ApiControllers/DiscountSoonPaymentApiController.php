<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DiscountSoonPaymentRepository;

class DiscountSoonPaymentApiController extends AppBaseController
{
    private $discountSoonPaymentRepository;

    public function __construct(DiscountSoonPaymentRepository $discountSoonPaymentRepository)
    {
        $this->discountSoonPaymentRepository = $discountSoonPaymentRepository;
    }

    public function store(Request $request)
    {
        return $this->discountSoonPaymentRepository->createDiscount($request);
    }

    public function get()
    {
        return $this->sendResponse($this->discountSoonPaymentRepository->searchDiscounts(), 'Success');
    }
    public function delete($id)
    {
        return $this->discountSoonPaymentRepository->deleteDiscount($id);
    }
    public function show($id)
    {
        return $this->discountSoonPaymentRepository->searchDiscount($id);
    }
    public function update(Request $request)
    {
        return $this->discountSoonPaymentRepository->updateDiscount($request);
    }
}
