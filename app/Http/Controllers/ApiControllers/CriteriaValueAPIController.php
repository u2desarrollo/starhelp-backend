<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateCriteriaValueAPIRequest;
use App\Http\Requests\ApiRequests\UpdateCriteriaValueAPIRequest;
use App\Entities\CriteriaValue;
use App\Repositories\CriteriaValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CriteriaValueController
 * @package App\Http\Controllers\ApiControllers
 */

class CriteriaValueAPIController extends AppBaseController
{
    /** @var  CriteriaValueRepository */
    private $criteriaValueRepository;

    public function __construct(CriteriaValueRepository $criteriaValueRepo)
    {
        $this->criteriaValueRepository = $criteriaValueRepo;
    }

    /**
     * Display a listing of the CriteriaValue.
     * GET|HEAD /criteriaValue
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->criteriaValueRepository->pushCriteria(new RequestCriteria($request));
        $this->criteriaValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $criteriaValue = $this->criteriaValueRepository->all();

        return $this->sendResponse($criteriaValue->toArray(), 'Criteria Value retrieved successfully');
    }

    /**
     * Store a newly created CriteriaValue in storage.
     * POST /criteriaValue
     *
     * @param CreateCriteriaValueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCriteriaValueAPIRequest $request)
    {
        $input = $request->all();

        $criteriaValue = $this->criteriaValueRepository->create($input);

        return $this->sendResponse($criteriaValue->toArray(), 'Criteria Value saved successfully');
    }

    /**
     * Display the specified CriteriaValue.
     * GET|HEAD /criteriaValue/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CriteriaValue $criteriaValue */
        $criteriaValue = $this->criteriaValueRepository->findWithoutFail($id);

        if (empty($criteriaValue)) {
            return $this->sendError('Criteria Value not found');
        }

        return $this->sendResponse($criteriaValue->toArray(), 'Criteria Value retrieved successfully');
    }

    /**
     * Update the specified CriteriaValue in storage.
     * PUT/PATCH /criteriaValue/{id}
     *
     * @param  int $id
     * @param UpdateCriteriaValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCriteriaValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var CriteriaValue $criteriaValue */
        $criteriaValue = $this->criteriaValueRepository->findWithoutFail($id);

        if (empty($criteriaValue)) {
            return $this->sendError('Criteria Value not found');
        }

        $criteriaValue = $this->criteriaValueRepository->update($input, $id);

        return $this->sendResponse($criteriaValue->toArray(), 'CriteriaValue updated successfully');
    }

    /**
     * Remove the specified CriteriaValue from storage.
     * DELETE /criteriaValue/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CriteriaValue $criteriaValue */
        $criteriaValue = $this->criteriaValueRepository->findWithoutFail($id);

        if (empty($criteriaValue)) {
            return $this->sendError('Criteria Value not found');
        }

        $criteriaValue->delete();

        return $this->sendResponse($id, 'Criteria Value deleted successfully');
    }
}
