<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\CashReceiptsApp;
use App\Entities\Contact;
use App\Entities\ContactWarehouse;
use App\Entities\ContributionInvoicesApp;
use App\Entities\Document;
use App\Entities\PaymentMethodsApp;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCashReceiptRequest;
use App\Repositories\CashReceiptsRepository;
use App\Repositories\DocumentRepository;
use App\Traits\GeneratePdfTrait;
use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class CashReceiptsAppController extends AppBaseController
{
    use GeneratePdfTrait;

    private $cashReceiptsRepository;

    public function __construct(CashReceiptsRepository $cashReceiptsRepository)
    {
        $this->cashReceiptsRepository = $cashReceiptsRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (!isset($request->iduser) || $request->iduser == null) {
            return response()->json(['error' => 'El campo iduser es obligatorio'], 400);
        }
        if (!isset($request->formasPago) || count(json_decode($request->formasPago)) <= 0) {
            return response()->json(['error' => 'El campo formasPago es obligatorio'], 400);
        }
        if (!isset($request->facturas) || count(json_decode($request->facturas)) <= 0) {
            return response()->json(['error' => 'El campo facturas es obligatorio'], 400);
        }
        if (!isset($request->bodega_id) || $request->bodega_id == null) {
            return response()->json(['error' => 'El campo bodega_id es obligatorio'], 400);
        }

        try {
            DB::beginTransaction();

            $formasPago = json_decode($request->formasPago);
            $maxConsecutive = CashReceiptsApp::max('consecutive');
            $cashReceiptsApp = CashReceiptsApp::create([
                'observations' => $request->observaciones,
                'creation_date' => date('Y-m-d H:i:s'),
                'warehouse_id' => $request->bodega_id,
                'contact_id' => $request->iduser,
                'consecutive' => $maxConsecutive == null ? 1000 : $maxConsecutive + 1,
            ]);

            foreach ($formasPago as $key) {

                $payWay = 849;

                if ($key->tipo == 'Cheque') {
                    $payWay = 850;
                } elseif ($key->tipo == 'Transferencia') {
                    $payWay = 851;
                }

                PaymentMethodsApp::create([
                    'cash_receipts_app_id' => $cashReceiptsApp->id,
                    'parameter_id' => $payWay,
                    'total_paid' => (int)$key->valor,
                    'bank' => $key->banco,
                    'check_number' => $key->numeroCheque,
                ]);
            }

            $facturas = json_decode($request->facturas);
            foreach ($facturas as $key) {
                $r = null;
                $dRepository = new DocumentRepository(new Container);
                if ($key->id != 0) {
                    $r = $dRepository->getInvoices(null, $key->id);
                }

                $contributionInvoicesApp = new ContributionInvoicesApp();
                $contributionInvoicesApp->cash_receipts_app_id = $cashReceiptsApp->id;
                $contributionInvoicesApp->document_id = $key->id != 0 ? $key->id : null;
                $contributionInvoicesApp->total_paid = $key->valorConsignar;
                $contributionInvoicesApp->invoice_document = $key->saldo;

                //   ***   Retenciones

                $contributionInvoicesApp->rete_fte = $key->rete_fte;
                $contributionInvoicesApp->rete_iva = $key->rete_iva;
                $contributionInvoicesApp->rete_ica = $key->rete_ica;


                if ($r) {
                    foreach ($r as $k => $value) {
                        if ($value->descuentoPP > 0 && $value->saldo == $key->valorConsignar) {
                            $contributionInvoicesApp->value_discount_prompt_payment = $value->descuentoPP;
                            $contributionInvoicesApp->invoice_days = $value->days_doc;
                        }
                    }
                }
                $contributionInvoicesApp->save();
            }


            DB::commit();
            // //  debo de eiminar el ultimo rc creado que posea
            // // que posea los mismos datos
            //$maxConsecutive = CashReceiptsApp::max('consecutive');
            $date = now()->addSeconds(-10)->format('Y-m-d H:i:s');

            $CashReceiptsCreated = CashReceiptsApp::with('contributionInvoicesApp')
                ->where('warehouse_id', $request->bodega_id)
                ->where('contact_id', $request->iduser)
                ->where('created_at', '>', $date)
                ->orderBy('id', 'desc')->get();

            if (count($CashReceiptsCreated) > 1) {
                CashReceiptsApp::find($CashReceiptsCreated[0]->id)->delete();

                return response()->json([
                    'id' => $CashReceiptsCreated[1]->id,
                    'consecutive' => 'RC-' . $CashReceiptsCreated[1]->consecutive
                ], 200);
            } else {
                $this->generatePdfCashReceipt(['rc' => $cashReceiptsApp->id, 'iduser' => $cashReceiptsApp->contact_id]);
                return response()->json([
                    'id' => $cashReceiptsApp->id,
                    'consecutive' => 'RC-' . $cashReceiptsApp->consecutive
                ], 200);
            }
        } catch (\Throwable $th) {

            DB::rollBack();
            \Log::info($th->getMessage());
            return response()->json($th, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getInvoiceReceipt($iduser, $rc)
    {
        return $iduser . '  -  ' . $rc;
    }


    public function mio()
    {


        return $cash = CashReceiptsApp::whereNull('consignment_id')->has(
            'contributionInvoicesApp.document'
        )->with(['contributionInvoicesApp.document' => function ($query) {
            $query->with(['warehouse.seller', 'contact.users', 'contact.city']);
        }])->has('paymentMethodsApp')->with('paymentMethodsApp')
            ->get()->groupBy('warehouse_id')->toArray();

        $clientes = collect();
        //recojo los clientes qye hay en la consulta
        foreach ($cash as $keyy => $value) {
            $contact = Contact::with(['warehouses' => function ($query) use ($keyy) {
                $query->select('contact_id', 'id')->where('id', $keyy);
            }])->first();

            // apartado del cliente
            if (!$clientes->contains($contact->id)) {

                // linea cliente
                $cliente = 'CLIENTE|' . $contact->identification . '|' . $contact->check_digit . '|' . $contact->name . '|' .
                    $contact->surname . '|' . $contact->address . '|' . $contact->cell_phone . '|' .
                    $contact->email . '|' . $contact->city_id . '|' . $contact->city . '|' . $contact->id;

                $clientes->push($contact->id);
                $clientes = $clientes->unique()->values();
            }


            foreach ($value as $key) {
                foreach ($key['contribution_invoices_app'] as $v) {


                    ///apartado para sucursales

                    $contactsWarehouses = Contact::with(['warehouses' => function ($query) use ($keyy) {
                        $query->select('contact_id', 'id')->where('id', $keyy);
                    }])->first();

                    if ($contactsWarehouses->warehouses->contains($keyy)) {
                        // linea de sucursal
                        $bodega = 'SUC-CLIENTE|' . $v['document']['warehouse']['code'] . '|' . $v['document']['warehouse']['description'] . '|' .
                            $v['document']['warehouse']['description'] . '|' . $v['document']['warehouse']['address'] . '|' . $v['document']['warehouse']['telephone'] . '|' .
                            $v['document']['warehouse']['email'] . '|' . $v['document']['warehouse']['seller']['identification'] . '|' .
                            $v['document']['warehouse']['seller']['name'] . '|' . $v['document']['warehouse']['seller']['surname'] . '|' . $v['document']['warehouse']['id'];


                        $vTotal = 0;

                        //obtengo el valor  para cada rc
                        foreach ($key['contribution_invoices_app'] as $element) {
                            return $element;
                        }


                        //recibo de caja
                        $fechaComoEntero = strtotime($key['creation_date']);
                        return $rCaja = 'RECAJAPP' . '|' . $key['consecutive'] . '|' . date("Y", $fechaComoEntero) . '-' . date("m", $fechaComoEntero) . '-' . date("d", $fechaComoEntero)
                            . '|' . trim(date("H", $fechaComoEntero) . ':' . date("i", $fechaComoEntero) . ':' . date("s", $fechaComoEntero)) . '|' . $key['consecutive'] . '|' . $vTotal;
                    }
                }
            }
            unset($cash[$keyy]);
        }
        return $clientes;
    }


    /**
     * Controlas las peticiiones Http de tipo GET para listar los recibos de caja filtrados por un rango de fechas
     *
     * @param Request $request Contiene los posibles filtros a aplicar en la consulta
     * @return mixed $cashReceipts Contiene el listado de recibos de caja
     * @author Jhon García
     */
    public function getCashReceiptsApp(Request $request)
    {
        $cashReceipts = $this->cashReceiptsRepository->getCashReceiptsApp($request);

        return $this->sendResponse($cashReceipts, 'Recibos de caja listados exitosamente');
    }

    public function downloadPdf(Request $request)
    {
        try {
            $rc = CashReceiptsApp::find($request->id);
            $path = 'pdf/';
            $fileName = 'Rec-Caja RC-' . $rc->consecutive . '.pdf';
            $file = $path.$fileName;

            if (!Storage::disk('public')->exists($file)) {
                $this->generatePdfCashReceipt(['rc' => $rc->id, 'iduser' => $rc->contact_id]);
            }

            return Storage::disk('public')->download($file, $fileName, [
                'Content-Type' => 'application/pdf',
            ]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }
}
