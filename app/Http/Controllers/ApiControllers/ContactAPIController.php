<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\ContactCustomer;
use App\Entities\ContactDocument;
use App\Entities\ContactClerk;
use App\Entities\ContactEmployee;
use App\Entities\ContactOther;
use App\Entities\ContactProvider;
use App\Entities\ContactWarehouse;
use App\Entities\Contact;
use App\Entities\Inventory;
use App\Entities\Parameter;
use App\Entities\Price;
use App\Entities\Product;
use App\Entities\Receivable;
use App\Entities\Document;
use App\Entities\Subscriber;
use App\Repositories\ContactRepository;
use App\Http\Requests\ApiRequests\CreateContactAPIRequest;
use App\Http\Requests\ApiRequests\CreateClientAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactAPIRequest;
use App\Http\Requests\ApiRequests\CreateContactBasicRequest;
use App\Services\BalancesService;
use App\Traits\LoadImageTrait;
use App\Entities\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Traits\CollectionsTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use stdClass;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Fill;

use App\Services\DocumentsBalanceService;

/**
 * Class ContactController
 *
 * @package App\Http\Controllers\ApiControllers
 */
class ContactAPIController extends AppBaseController
{
    use LoadImageTrait;
    use CollectionsTrait;

    /** @var  ContactRepository */
    private $contactRepository;
    private $documentsBalanceService;
    private $balances_service;

    public function __construct(ContactRepository $contactRepo,
                                DocumentsBalanceService $documentsBalanceService,
                                BalancesService $balances_service
    )
    {
        $this->contactRepository = $contactRepo;
        $this->documentsBalanceService = $documentsBalanceService;
        $this->balances_service = $balances_service;
    }


    /**
     * Display a listing of the Contact.
     * GET|HEAD /contacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $contacts = $this->contactRepository->findAll($request);

        return $this->sendResponse($contacts->toArray(), 'Contacts retrieved successfully');
    }

    public function getContactForType(Request $request)
    {

        $contacts = $this->contactRepository->getContactForType($request);

        return $this->sendResponse($contacts->toArray(), 'Contacts retrieved successfully');
    }

    /**
     * Store a newly created Contact in storage.
     * POST /contacts
     *
     * @param CreateContactAPIRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->except('city');

        $contact_request = $request->except(['id', 'city', 'customer', 'employee', 'lines', 'provider', 'subscriber', 'identificationt', 'warehouses', 'email_electronic_invoice']);
        DB::beginTransaction();

        try {

            $contact = DB::table('contacts')->where('identification', $request->identification)->first();

            if (empty($contact_request['birthdate'])) {
                $contact_request['birthdate'] = date('Y-m-d');
            }

            if ($contact) {
                if ($contact->deleted_at != null) {
                    $contact_request['deleted_at'] = null;
                }
                DB::table('contacts')->where('identification', $contact->identification)
                    ->update($contact_request);
                $contact = Contact::where('identification', $request->identification)->first();
            } else {
                $contact = $this->contactRepository->create($contact_request);
            }

            if ($contact->is_other) {
                $contact_other = new ContactOther($request->contact_other);
                $contact->contactOther()->save($contact_other);
            }

            $contact->lines()->attach($input['lines']);

            $this->storeCustomer($contact, $input);

            $this->storeProvider($contact, $input);

            $this->storeEmployee($contact, $input);

            $this->storeWarehouse($contact, $input);

            DB::commit();
            $contactResponse = $this->contactRepository->findForId($contact->id);
            return $this->sendResponse($contactResponse->toArray(), 'Contact saved successfully');
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
            return response('Error: ' . $e->getMessage(), 500);
        }
    }

    public function storeCustomer($contact, $input)
    {
        // Evalua si se debe crear el tercero como CLIENTE
        if ($input['is_customer'] == 1) {
            $customer = new ContactCustomer($input['customer']);
            if (isset($input['customer']['logo']) && $input['customer']['logo'] != '') {
                $customer['logo'] = $this->saveImage($customer['logo'], 'contacts/customers');
            }
            $contact->customer()->save($customer);
        }
    }

    public function storeProvider($contact, $input)
    {
        // Evalua si se debe crear el tercero como PROVEEDOR
        if ($input['is_provider'] == 1) {
            $provider = new ContactProvider($input['provider']);
            if (isset($input['provider']['logo']) && $input['provider']['logo'] != '') {
                $provider['logo'] = $this->saveImage($provider['logo'], 'contacts/providers');
            }
            $contact->provider()->save($provider);
        }
    }

    public function storeEmployee($contact, $input)
    {
        // Evalua si se debe crear el tercero como PROVEEDOR
        if ($input['is_employee'] == 1) {
            $employee = new ContactEmployee($input['employee']);
            if (isset($input['employee']['photo']) && $input['employee']['photo'] != '') {
                $employee['photo'] = $this->saveImage($employee['photo'], 'contacts/employees');
            }
            $contact->employee()->save($employee);
        }
    }

    public function storeWarehouse($contact, $input)
    {
        $contactWarehouse = [
            'contact_id'    => $contact->id,
            'code'          => '000',
            'description'   => $contact->name . ' ' . $contact->surname,
            'address'       => $contact->address,
            'telephone'     => $contact->main_telephone,
            'email'         => $contact->email,
            'city_id'       => $contact->city_id,
            'creation_date' => date('Y-m-d')
        ];

        $contactWarehouseBd = ContactWarehouse::where(Arr::only($contactWarehouse, ['contact_id', 'code']))->first();

        if (!$contactWarehouseBd) {
            ContactWarehouse::create($contactWarehouse);
        } else {
            ContactWarehouse::where(Arr::only($contactWarehouse, ['contact_id', 'code']))->update($contactWarehouse);
        }
    }

    /**
     * Display the specified Contact.
     * GET|HEAD /contacts/{id}
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var Contact $contact */
        $contact = $this->contactRepository->findForId($id);

        if (empty($contact)) {
            return $this->sendError('Contact not found');
        }

        return $this->sendResponse($contact->toArray(), 'Contact retrieved successfully');
    }

    /**
     * Update the specified Contact in storage.
     * PUT/PATCH /contacts/{id}
     *
     * @param int $id
     * @param UpdateContactAPIRequest $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->except('city');

        $contact_request = $request->except(['city', 'customer', 'employee', 'lines', 'provider', 'subscriber', 'warehouses', 'identificationt']);

        DB::beginTransaction();
        try {

            $contact = $this->contactRepository->findForId($id);

            if (empty($contact)) {
                return $this->sendError('Contact not found');
            }

            $contact->lines()->sync($input['lines']);

            $this->updateCustomer($contact, $input);

            $this->updateProvider($contact, $input);

            $this->updateEmployee($contact, $input);

            $this->updateUser($contact_request, $contact);

            $clerks = $contact_request['clerks'];

            if ($contact->is_other) {
                $contact->contactOther()->updateOrCreate([], $request->contact_other);
            }

            unset($contact_request['clerks']);

            $contact = $this->contactRepository->update($contact_request, $id);

            foreach ($clerks as $key => $clerk) {
                ContactClerk::find($clerk['id'])->update($clerk);
            }

            DB::commit();
            $contactResponse = $this->contactRepository->findForId($id);
            return $this->sendResponse($contactResponse->toArray(), 'Contact updated successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return response('Error: ' . $e->getMessage(), 500);
        }
    }

    /**
     * @param array $request
     * @param Contact $contact
     * @throws Exception
     */
    public function updateUser($request, $contact)
    {
        $user = User::where('contact_id', $contact->id)
            ->withTrashed()
            ->first();

        if ($user) {
            if (!empty($contact->email) && empty($request['email'])) {
                throw new Exception('El tercero tiene asociado un usuario, por lo cual no puede eliminar el email', 500);
            } else {
                $other_user = User::where(Arr::only($request, ['email']))
                    ->where('id', '!=', $user->id)
                    ->withTrashed()
                    ->first();

                if ($other_user) {
                    throw new Exception('No es posible actualizar el usuario debido a que la dirección de correo está asociada a otro usuario', 500);
                } else {
                    $user->fill(Arr::only($request, ['name', 'surname', 'email']))->save();
                }

            }
        }
    }

    public function updateCustomer($contact, $input)
    {

        //------------------------------ CLIENTES --------------------------//
        if ($input['is_customer'] == 1) {

            $customer = $input['customer'];

            if (isset($customer['logo'])) {
                if ($customer['logo'] != '' && $customer['logo'] != null) {
                    if ($contact->customer->logo != $customer['logo']) {
                        $customer['logo'] = $this->saveImage($customer['logo'], 'contacts/customers');
                    } else {
                        unset($customer['logo']);
                    }
                } else {
                    unset($customer['logo']);
                }
            }

            if (isset($customer['id'])) {
                $contact->customer()->update(Arr::except($customer, ['id']), $customer['id']);
            } else {
                $cust = new ContactCustomer($customer);
                $cust->save();
                $contact->customer()->save($cust);
            }
        }
    }

    //CreateClientAPIRequest $request
    public function create_new_client(CreateClientAPIRequest $request)
    {
        //-------------------------- CREATE NEW CLIENT -------------------------//
        $input = $request->all();

        $input['subscriber_id'] = 1;
        $input['class_person'] = 5;
        $input['taxpayer_type'] = 14;
        $input['tax_regime'] = 11;

        $input['picture'] = $this->saveImage($input['picture'], 'picture_clients');

        // $users = $this->contactRepository->create($input);
        $createClient = $this->contactRepository->createNewClient($input);

        return $createClient;
    }

    public function check_new_client($idClient)
    {
        $customer = ContactWarehouse::select('contacts_warehouses.id', 'description', 'credit_quota', 'price_list_id', 'contact_id', 'seller_id', 'pay_condition', 'send_point', 'telephone', 'minimum_order_value', 'branchoffice_id')
            ->where('state', true)
            ->where('contacts_warehouses.id', $idClient)
            ->with([
                'branchoffice',
                'contact',
                'contact.customer',
                'contact.employee',
                'contact.provider',
                'seller',
                'seller.employee',
                'line',
                'line.line'
            ])
            ->firstOrFail();

        return response()->json($customer);
    }

    public function updateProvider($contact, $input)
    {

        //------------------------------ PROVEEDORES --------------------------//
        if ($input['is_provider'] == 1) {

            $provider = $input['provider'];

            if (isset($provider['logo'])) {
                if ($provider['logo'] != '' && $provider['logo'] != null) {
                    if ($contact->provider->logo != $provider['logo']) {
                        $provider['logo'] = $this->saveImage($provider['logo'], 'contacts/providers');
                    } else {
                        unset($provider['logo']);
                    }
                } else {
                    unset($provider['logo']);
                }
            }

            if (isset($provider['id'])) {
                $contact->provider()->update(Arr::except($provider, ['id']), $provider['id']);
            } else {
                $prov = new ContactProvider($provider);
                $contact->provider()->save($prov);
            }
        }
    }

    public function updateEmployee($contact, $input)
    {
        //------------------------------ EMPLEADOS --------------------------//
        if ($input['is_employee'] == 1) {

            $employee = $input['employee'];

            if (isset($employee['photo'])) {
                if ($employee['photo'] != '' && $employee['photo'] != null) {
                    if ($contact->employee->photo != $employee['photo']) {
                        $employee['photo'] = $this->saveImage($employee['photo'], 'contacts/employees');
                    } else {
                        unset($employee['photo']);
                    }
                } else {
                    unset($employee['photo']);
                }
            }

            if (isset($employee['id'])) {
                $contact->employee()->update(Arr::except($employee, ['id']), $employee['id']);
            } else {
                $empl = new ContactEmployee($employee);
                $contact->employee()->save($empl);
            }
        }
    }


    public function updateDocuments($contact, $documents)
    {
        //------------------------------ DOCUMENTOS --------------------------//
        foreach ($documents as $document) {
            $doc = new ContactDocument($document);
            if (isset($document['action'])) {
                if ($document['action'] == 'c') {
                    $doc['path'] = $this->saveImage($doc['path'], 'contacts/documents');
                    $contact->documents()->save($doc);
                } elseif ($document['action'] == 'u') {
                    $docUpdate = ContactDocument::find($document['id']);
                    if ($document['path'] != $docUpdate->path) {
                        $document['path'] = $this->saveImage($document['path'], 'contacts/documents');
                    }
                    $docUpdate->fill($document)->save();
                } elseif ($document['action'] == 'd') {
                    ContactDocument::find($document['id'])->delete();
                }
            }
        }
    }


    /**
     * Remove the specified Contact from storage.
     * DELETE /contacts/{id}
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Contact $contact */
        $contact = $this->contactRepository->delete($id);

        if (!$contact['status']) {
            return $this->sendError($contact['message']);
        }

        return $this->sendResponse($id, 'Contact deleted successfully');
    }


    public function findForIdentification($identification)
    {
        $contact = $this->contactRepository->with(['customer', 'provider', 'employee', 'warehouses', 'city'])->findByField(['identification' => $identification])->first();

        if (empty($contact)) {
            return $this->sendError('Contact not found');
        }

        return $this->sendResponse($contact->toArray(), 'Contact retrieved successfully');
    }

    public function findForEmployeeType(Request $request)
    {
        $customers = $this->contactRepository->findCustomersByType($request);
        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }

    public function findForSellerId(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'iduser' => 'required|integer'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => 'No envió el código del vendedor.'], 400);
            }

            $customers = $this->contactRepository->findCustomersBySellerId($request->iduser);

            /*foreach ($customers as $key => $cw) {
                $contactWallet = $this->documentsBalanceService->getTrackingByContactId($cw->contact_id);
                // $cw->contact_wallet = $contactWallet['data'];

                $invoiceBalanceTotal = 0;
                foreach ($contactWallet['data'] as $key => $wallet) {
                    $invoiceBalanceTotal += $wallet->receivable[0]->invoice_balance;

                    $date1 = Carbon::parse($wallet->document_date);
                    $cw->wallet_overdue = $date1->diffInDays() > 60;
                }

                $cw->total_receivables = $invoiceBalanceTotal;

                //invoice_balance
                $cw->total_available = $cw->credit_quota - $cw->total_receivables;
            }*/

            if ($customers != null) {
                return response()->json($customers->toArray());
            }
            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => ' Se presentó un error al procesar la solicitud.' . $e], 500);
        }
    }


    public function sellersStats()
    {
        //$this->contactRepository->pushCriteria(new RequestCriteria($request));
        //$this->contactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contacts = $this->contactRepository->sellersStatistics();

        return $this->sendResponse($contacts->toArray(), 'Sellers retrieved successfully');

        /*$algo = [
            0 => ['seller' => 1,
            'num_visits' => 20,
            'num_orders' => 34,
            'effectiveness' => '60%',
            'orders_value' => 5000,
            'sells_value' => 8500
        ]];

        return $this->sendResponse($algo, 'success');*/
    }

    public function client_debt($diasMora)
    {
        $fullData = [];

        $i = 0;

        $clientesMorosos = Receivable::select('id', 'invoice_id', 'original_invoice_value', 'invoice_balance')
            ->with(['document' => function ($query) {
                $query->where('vouchertype_id', 4);
                $query->select(['id', 'contact_id', 'voucher_date', 'consecutive', 'vouchertype_id']);
                $query->with(['contact' => function ($query) {
                    $query->select(['id', 'identification', 'name', 'main_telephone', 'cell_phone']);
                }]);
            }])
            ->get();

        foreach ($clientesMorosos as $clienteMoroso) {
            $diasCalculados = $this->diasDeMora($clienteMoroso->document->voucher_date);

            if ($diasCalculados >= $diasMora) {
                $arreglo = [
                    'ident'     => $clienteMoroso->document->contact->identification,
                    'rsocial'   => $clienteMoroso->document->contact->name,
                    'telephone' => $clienteMoroso->document->contact->main_telephone,
                    'cellphone' => $clienteMoroso->document->contact->cell_phone,
                    'dmora'     => $diasCalculados,
                    'vmora'     => number_format($clienteMoroso->invoice_balance, 0, '', ','),
                    'tcartera'  => number_format($clienteMoroso->invoice_balance, 0, '', ','),
                ];

                $fullData[$i] = new \ArrayObject($arreglo);
            }

            $i += 1;
        }

        return $fullData;
    }

    private function diasDeMora($fecha)
    {
        // $this->diasDeMora("2018-07-03 11:00:00");

        $date = $fecha;
        $date = Carbon::parse($date);
        $now = Carbon::now();
        $diasMora = $date->diffInDays($now);

        return $diasMora;
    }

    public function uploadExcel(Request $request)
    {
        $this->contactRepository->pushCriteria(new RequestCriteria($request));
        $this->contactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contacts = $this->contactRepository->uploadExcel($request);

        return $contacts;
    }

    public function getContacts()
    {
        $contacts = $this->contactRepository->getContacts();
        return $this->sendResponse($contacts->toArray(), 'Contacts retrieved successfully');
    }

    /**Obtiene contactos por la categoria */
    public function contactsByCategory(Request $request)
    {
        $contacts = $this->contactRepository->findByCategory($request);
        return $this->sendResponse($contacts->toArray(), 'Contacts retrieved successfully');
    }

    public function getContactsSelect()
    {
        $contacts = $this->contactRepository->getContactsSelect();
        return $this->sendResponse($contacts->toArray(), 'Contacts retrieved successfully');
    }


    public function prospectiveCustomer(CreateClientAPIRequest $request)
    {
        $input = $request->all();
        $seller = JWTAuth::parseToken()->toUser();

        if ($input['identification'] == null || $input['identification'] == '') {
            $input['identification'] = str_random();
        }

        try {
            DB::beginTransaction();
            if (!empty($input['photo'])) {
                $input['photo'] = $this->saveImage($input['photo'], 'customer-establishment');
            }

            $prospective = $this->contactRepository->createProspectiveCustomer($input, $seller);
            DB::commit();
            return $this->sendResponse($prospective, 'Succesfully created prospective customer');
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json($error->getMessage());
        }
    }

    public function cargaInformacion()
    {
        set_time_limit(0);

        try {

            DB::beginTransaction();

            /*$this->borrarDatos();
            Log::info('Borro datos');*/

            /*$this->cargarTerceros();
            Log::info('Cargo terceros');*/

            /*$this->cargarProveedores();
            Log::info('Cargo proveedores');*/

            /*$this->actualizaUsuarios();
            Log::info('Cargo usuarios');*/
            $this->actualizaTerceros();


            // $this->nuevosTerceros2020();
            // Log::info('Nuevos terceros 2020');

            // $this->actualizaSucursales();
            // Log::info('Sucursales');

            DB::commit();
        } catch (Exception $e) {
            Log::info($e->getMessage() . ' in the line ' . $e->getLine());
            DB::rollBack();
        }
    }

    public function borrarDatos()
    {

        DB::select('TRUNCATE TABLE contacts RESTART IDENTITY CASCADE');
    }

    public function cargarTerceros()
    {

        $index = [
            'identificationSeller'      => 21,
            'nameSeller'                => 22,
            'surnameSeller'             => 23,
            'emailSeller'               => 24,
            'cellphoneSeller'           => 25,
            'branchofficeSeller'        => 26,
            'identificationContact'     => 0,
            'checkDigitContact'         => 3,
            'businessNameContact'       => 5,
            'firstNameContact'          => 6,
            'secondNameContact'         => 7,
            'surnameContact'            => 8,
            'secondSurnameContact'      => 9,
            'addressContact'            => 13,
            'mainTelephoneContact'      => 14,
            'secondaryTelephoneContact' => 15,
            'cellphoneContact'          => 16,
            'emailContact'              => 17,
            'creditQuotaContact'        => 19,
            'expirationDaysContact'     => 20,
            'cityContact'               => 26
        ];

        $file = fopen(storage_path('app/public/terceros.csv'), "r");

        $cities = DB::table('cities')->get();
        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $foundCity = false;

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    similar_text($branchoffice->name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                //Consulta si existe el vendedor

                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);
                    $seller = Contact::where('identification', $array[$index['identificationSeller']])->first();
                    //Si no existe lo crea
                    if (!$seller) {
                        $seller = Contact::create([
                            'subscriber_id'       => 1,
                            'identification'      => $array[$index['identificationSeller']],
                            'identification_type' => 2,
                            'name'                => $array[$index['nameSeller']],
                            'surname'             => $array[$index['surnameSeller']],
                            'cell_phone'          => $array[$index['cellphoneSeller']],
                            'email'               => $array[$index['emailSeller']],
                            'is_employee'         => true
                        ]);

                        $contact_employee = ContactEmployee::where('contact_id', $seller->id)->first();

                        if (!$contact_employee) {
                            ContactEmployee::create([
                                'contact_id'  => $seller->id,
                                'position_id' => 937
                            ]);
                        }

                        $user = User::where('contact_id', $seller->id)->first();

                        if (!$user) {
                            $user = User::where('email', $array[$index['emailSeller']])->first();

                            if (!$user) {

                                $user = User::create([
                                    'contact_id'                => $seller->id,
                                    'identification_type'       => 2,
                                    'name'                      => $array[$index['nameSeller']],
                                    'surname'                   => $array[$index['surnameSeller']],
                                    'email'                     => $array[$index['emailSeller']],
                                    'password'                  => $array[$index['identificationSeller']],
                                    'photo'                     => '/contacts/employees/default.jpg',
                                    'module_id'                 => 3,
                                    'branchoffice_id'           => $branchoffice_id,
                                    'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                                    'subscriber_id'             => 1,
                                    'type'                      => 'e',
                                    'password_changed'          => true
                                ]);
                            }

                            $user->subscribers()->attach([1]);
                            $user->modules()->attach([3]);
                            $user->getRoles()->sync([7]);
                        }
                    }
                }

                //Consulta si existe el tercero
                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);
                $contact = Contact::where('identification', $array[$index['identificationContact']])->first();


                if ($array[$index['firstNameContact']] == '') {
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                } else {
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];;
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];;
                }

                //Si no existe lo crea
                if (!$contact) {
                    $contact = Contact::create([
                        'subscriber_id'       => 1,
                        'identification'      => $array[$index['identificationContact']],
                        'check_digit'         => $array[$index['checkDigitContact']],
                        'name'                => $name,
                        'surname'             => $surname,
                        'address'             => $array[$index['addressContact']],
                        'main_telephone'      => $array[$index['mainTelephoneContact']],
                        'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                        'cell_phone'          => $array[$index['cellphoneContact']],
                        'email'               => $array[$index['emailContact']],
                        'is_customer'         => true
                    ]);

                    $contact_customer = ContactCustomer::where('contact_id', $contact->id)->first();
                    $array[$index['creditQuotaContact']] = str_replace('.', '', $array[$index['creditQuotaContact']]);
                    $array[$index['creditQuotaContact']] = str_replace(',', '.', $array[$index['creditQuotaContact']]);

                    if (!$contact_customer) {
                        ContactCustomer::create([
                            'contact_id'      => $contact->id,
                            'credit_quota'    => (int)$array[$index['creditQuotaContact']],
                            'expiration_days' => $array[$index['expirationDaysContact']]
                        ]);
                    }

                    $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => '000'])->first();

                    if (!$contact_warehouse) {

                        $city_id = null;

                        foreach ($cities as $city) {
                            similar_text($city->city, $array[$index['cityContact']], $percent);
                            $percent = round($percent, 0);
                            if ($percent >= 50) {
                                $city_id = $city->id;
                                break;
                            }
                        }

                        $contact_warehouse = ContactWarehouse::create([
                            'contact_id'      => $contact->id,
                            'code'            => '000',
                            'description'     => $name,
                            'address'         => $array[$index['addressContact']],
                            'telephone'       => $array[$index['mainTelephoneContact']],
                            'city_id'         => $city_id,
                            'seller_id'       => isset($seller) && $seller ? $seller->id : null,
                            'branchoffice_id' => $branchoffice_id
                        ]);
                    }
                }
            }
        }
    }

    public function asignaVendedoresATerceros()
    {

        $index = [
            'identificationSeller'  => 2,
            'nameSeller'            => 3,
            'surnameSeller'         => 4,
            'emailSeller'           => 1,
            'identificationContact' => 0
        ];

        $file = fopen(storage_path('app/public/vendedoresasucursales.csv'), "r");

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                //Consulta si existe el vendedor

                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);
                    $seller = Contact::where('identification', $array[$index['identificationSeller']])->first();
                    if ($seller) {
                        $user = User::where([
                            'email' => $array[$index['emailSeller']]
                        ])->first();

                        if ($user) {
                            $user->contact_id = $seller->id;
                            $user->save();
                        }

                        //Consulta si existe el tercero
                        $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);
                        $contact = Contact::where('identification', $array[$index['identificationContact']])->first();

                        if ($contact) {

                            $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => '000'])->first();

                            if ($contact_warehouse) {

                                $contact_warehouse->seller_id = $seller->id;
                                $contact_warehouse->save();
                            }
                        }
                    }
                }
            }
        }
    }

    public function arreglaTerceros()
    {

        $index = [
            'identificationContact' => 0,
            'businessNameContact'   => 5,
            'firstNameContact'      => 6,
            'secondNameContact'     => 7,
            'surnameContact'        => 8,
            'secondSurnameContact'  => 9,
        ];

        $file = fopen(storage_path('app/public/terceros.csv'), "r");

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                if (empty($array[$index['firstNameContact']]) || trim($array[$index['firstNameContact']]) == '') {
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                } else {
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];
                }

                $contact = Contact::where('identification', $array[$index['identificationContact']])->first();

                Contact::where('identification', $array[$index['identificationContact']])->update([
                    'name'    => $name,
                    'surname' => $surname
                ]);

                ContactWarehouse::where('contact_id', $contact->id)->update([
                    'description' => $name . ' ' . $surname
                ]);
            }
        }
    }

    public function cargarProveedores()
    {

        $index = [
            'identificationContact'     => 0,
            'checkDigitContact'         => 1,
            'businessNameContact'       => 2,
            'addressContact'            => 3,
            'faxContact'                => 5,
            'mainTelephoneContact'      => 6,
            'secondaryTelephoneContact' => 7,
            'cellphoneContact'          => 8,
            'cityContact'               => 4
        ];

        $file = fopen(storage_path('app/public/proveedores.csv'), "r");

        $cities = DB::table('cities')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                //Consulta si existe el tercero
                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);
                $contact = Contact::where('identification', $array[$index['identificationContact']])->first();
                //Si no existe lo crea
                if (!$contact) {
                    $contact = Contact::create([
                        'subscriber_id'       => 1,
                        'identification'      => $array[$index['identificationContact']],
                        'check_digit'         => $array[$index['checkDigitContact']],
                        'name'                => $array[$index['businessNameContact']],
                        'address'             => $array[$index['addressContact']],
                        'main_telephone'      => $array[$index['mainTelephoneContact']],
                        'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                        'cell_phone'          => $array[$index['cellphoneContact']],
                        'fax'                 => $array[$index['faxContact']],
                        'is_provider'         => true
                    ]);
                } else {
                    $contact->is_provider = true;
                    $contact->save();
                }


                //Consulta si existe el proveedor
                $contact_provider = ContactProvider::where('contact_id', $contact->id)->first();

                if (!$contact_provider) {
                    ContactCustomer::create([
                        'contact_id' => $contact->id,
                    ]);
                }

                $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => '000'])->first();

                if (!$contact_warehouse) {

                    $city_id = null;

                    foreach ($cities as $city) {
                        similar_text($city->city, $array[$index['cityContact']], $percent);
                        $percent = round($percent, 0);
                        if ($percent >= 40) {
                            $city_id = $city->id;
                            break;
                        }
                    }

                    ContactWarehouse::create([
                        'contact_id'  => $contact->id,
                        'code'        => '000',
                        'description' => $array[$index['businessNameContact']],
                        'address'     => $array[$index['addressContact']],
                        'telephone'   => $array[$index['mainTelephoneContact']],
                        'city_id'     => $city_id,
                    ]);
                }
            }
        }
    }

    public function cargaPrecios()
    {

        $file = fopen(storage_path('app/public/precios.csv'), "r");

        $countPrices = 0;

        $price_list_001 = Parameter::where(['paramtable_id' => 14, 'code_parameter' => '001'])->first();

        //$price_list_002 = Parameter::where(['paramtable_id' => 14, 'code_parameter' => '002'])->first();

        try {

            DB::beginTransaction();

            DB::select('TRUNCATE TABLE prices');

            while (!feof($file)) {

                $line = fgets($file);
                $array = explode(";", $line);

                if (isset($array[1])) {

                    for ($i = 0; $i < sizeof($array); $i++) {
                        $array[$i] = trim($array[$i]);
                    }

                    if (!empty($array[0])) {

                        $product = Product::where('code', $array[0])->first();

                        $array[3] = str_replace('.', '', $array[3]);
                        $array[3] = str_replace(',', '.', $array[3]);

                        if ($product) {
                            Price::create([
                                'product_id'      => $product->id,
                                'price_list_id'   => $price_list_001->id,
                                'branchoffice_id' => null,
                                'price'           => $array[3],
                                'previous_price'  => $array[3]
                            ]);
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            Log::info($e->getMessage() . ' in the line ' . $e->getLine());
            DB::rollBack();
        }
    }

    public function actualizaUsuarios()
    {

        $index = [
            'identificationSeller' => 0,
            'nameSeller'           => 1,
            'emailSeller'          => 5,
            'branchofficeSeller'   => 6
        ];

        $file = fopen(storage_path('app/public/usuarios.csv'), "r");

        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if ($array[0]) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    similar_text($branchoffice->name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                //Consulta si existe el tercero

                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);
                    $contact = Contact::where('identification', $array[$index['identificationSeller']])->first();
                    //Si no existe lo crea
                    if (!$contact) {
                        $contact = Contact::create([
                            'subscriber_id'       => 1,
                            'identification'      => $array[$index['identificationSeller']],
                            'identification_type' => 2,
                            'name'                => $array[$index['nameSeller']],
                            'email'               => $array[$index['emailSeller']],
                            'is_employee'         => true
                        ]);
                    }

                    $contact_employee = ContactEmployee::where('contact_id', $contact->id)->first();

                    if (!$contact_employee) {
                        ContactEmployee::create([
                            'contact_id'  => $contact->id,
                            'position_id' => 937
                        ]);
                    }

                    $user = User::where('contact_id', $contact->id)
                        ->orWhereRaw('lower(email) = ?', [strtolower($array[$index['emailSeller']])])
                        ->first();

                    if (!$user) {

                        $user = User::create([
                            'contact_id'                => $contact->id,
                            'identification_type'       => 2,
                            'name'                      => $array[$index['nameSeller']],
                            'email'                     => strtolower($array[$index['emailSeller']]),
                            'password'                  => $array[$index['identificationSeller']],
                            'photo'                     => '/contacts/employees/default.jpg',
                            'module_id'                 => 3,
                            'branchoffice_id'           => $branchoffice_id,
                            'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                            'subscriber_id'             => 1,
                            'type'                      => 'e',
                            'password_changed'          => true
                        ]);
                        $user->subscribers()->attach([1]);
                        $user->modules()->attach([3]);
                        $user->getRoles()->sync([7]);
                    }
                }
            }
        }
    }

    public function cargaUsuarios()
    {

        $index = [
            'identificationSeller' => 0,
            'nameSeller'           => 1,
            'emailSeller'          => 5,
            'branchofficeSeller'   => 6
        ];

        $file = fopen(storage_path('app/public/usuarios.csv'), "r");

        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if ($array[0]) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    similar_text($branchoffice->name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                //Consulta si existe el tercero

                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);
                    $contact = Contact::where('identification', $array[$index['identificationSeller']])->first();
                    //Si no existe lo crea
                    if (!$contact) {
                        $contact = Contact::create([
                            'subscriber_id'       => 1,
                            'identification'      => $array[$index['identificationSeller']],
                            'identification_type' => 2,
                            'name'                => $array[$index['nameSeller']],
                            'email'               => $array[$index['emailSeller']],
                            'is_employee'         => true
                        ]);
                    }

                    $contact_employee = ContactEmployee::where('contact_id', $contact->id)->first();

                    if (!$contact_employee) {
                        ContactEmployee::create([
                            'contact_id'  => $contact->id,
                            'position_id' => 937
                        ]);
                    }

                    $user = User::where('contact_id', $contact->id)->first();

                    if (!$user) {
                        $user = User::/*where('contact_id', $contact->id)
                            ->orW*/ whereRaw('lower(email) = ?', [strtolower($array[$index['emailSeller']])])
                            ->first();

                        if (!$user) {

                            $user = User::create([
                                'contact_id'                => $contact->id,
                                'identification_type'       => 2,
                                'name'                      => $array[$index['nameSeller']],
                                'email'                     => strtolower($array[$index['emailSeller']]),
                                'password'                  => $array[$index['identificationSeller']],
                                'photo'                     => '/contacts/employees/default.jpg',
                                'module_id'                 => 3,
                                'branchoffice_id'           => $branchoffice_id,
                                'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                                'subscriber_id'             => 1,
                                'type'                      => 'e',
                                'password_changed'          => true
                            ]);
                        } else {
                            $user->contact_id = $contact->id;
                            $user->branchoffice_id = $branchoffice_id;
                            $user->branchoffice_warehouse_id = $branchoffice_warehouse_id;
                            $user->password = $array[$index['identificationSeller']];
                            $user->email = strtolower($array[$index['emailSeller']]);
                            $user->name = $array[$index['nameSeller']];

                            $user->save();
                        }

                        $user->subscribers()->attach([1]);
                        $user->modules()->attach([3]);
                        $user->getRoles()->sync([7]);
                    }
                }
            }
        }
    }

    public function actualizaTerceros()
    {

        // $index = [
        //     'identificationContact' => 0,
        //     'checkDigitContact' => 1,
        //     'businessNameContact' => 2,
        //     'firstNameContact' => 3,
        //     'secondNameContact' => 4,
        //     'surnameContact' => 5,
        //     'secondSurnameContact' => 6,
        //     'addressContact' => 7,
        //     'mainTelephoneContact' => 8,
        //     'secondaryTelephoneContact' => 9,
        //     'cellphoneContact' => 10,
        //     'emailContact' => 11,
        //     'creditQuotaContact' => 12,
        //     'expirationDaysContact' => 13,
        //     'emailSeller' => 14,
        //     'identificationSeller' => 15,
        //     'nameSeller' => 16,
        //     'surnameSeller' => 17,
        //     'cellphoneSeller' => 18,
        //     'branchofficeSeller' => 19,
        //     'cityContact' => 19
        // ];
        $index = [
            'identificationContact'     => 1,
            'codeWarehouse'             => 2,
            'physicalFileNumber'        => 3,
            'yearRut'                   => 4,
            'checkDigitContact'         => 5,
            'activeClient'              => 6,
            'businessNameContact'       => 7,
            'firstNameContact'          => 8,
            'secondNameContact'         => 9,
            'surnameContact'            => 10,
            'secondSurnameContact'      => 11,
            'classPerson'               => 12,
            'taxRegime'                 => 13,
            'addressContact'            => 14,
            'mainTelephoneContact'      => 15,
            'secondaryTelephoneContact' => 16,
            'cellphoneContact'          => 17,
            'emailContact'              => 18,
            'dateOpening'               => 19,
            'creditQuotaContact'        => 20,
            'payDay'                    => 21,
            'emailSeller'               => 23,
            'identificationSeller'      => 24,
            'nameSeller'                => 25,
            'surnameSeller'             => 26,
            'cellphoneSeller'           => 27,
            'branchofficeSeller'        => 28,
        ];

        $file = fopen(storage_path('app/public/actualizacionterceros.csv'), "r");

        $cities = DB::table('cities')->get();
        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $line = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $line);
            $line = preg_replace('!\s+!', '', $line);
            $array = explode(";", $line);

            if (isset($array[1]) && $array[$index['activeClient']] != 'NO') {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    $branchoffice_name = str_replace("SEDE ", "", $branchoffice->name);
                    similar_text($branchoffice_name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                $seller_id = null;

                //Consulta si existe el vendedor
                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);

                    $array_seller = [
                        'subscriber_id'       => 1,
                        'identification'      => $array[$index['identificationSeller']],
                        'identification_type' => 2,
                        'name'                => strtolower($array[$index['nameSeller']]),
                        'surname'             => $array[$index['surnameSeller']],
                        'cell_phone'          => $array[$index['cellphoneSeller']],
                        'email'               => $array[$index['emailSeller']],
                        'is_employee'         => true
                    ];

                    $seller = Contact::updateOrCreate(Arr::only($array_seller, ['identification']), $array_seller);

                    $array_contact_employee = [
                        'contact_id'  => $seller->id,
                        'position_id' => 937
                    ];

                    ContactEmployee::updateOrCreate(Arr::only($array_contact_employee, ['contact_id']), $array_contact_employee);

                    $array_user = [
                        'contact_id'                => $seller->id,
                        'identification_type'       => 2,
                        'name'                      => $array[$index['nameSeller']],
                        'surname'                   => $array[$index['surnameSeller']],
                        'email'                     => strtolower($array[$index['emailSeller']]),
                        'password'                  => $array[$index['identificationSeller']],
                        'photo'                     => '/contacts/employees/default.jpg',
                        'module_id'                 => 3,
                        'branchoffice_id'           => $branchoffice_id,
                        'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                        'subscriber_id'             => 1,
                        'type'                      => 'e',
                        'password_changed'          => true
                    ];

                    User::updateOrCreate(Arr::only($array_user, ['email']), $array_user);

                    $seller_id = $seller->id;

                    /*$user->subscribers()->attach([1]);
                    $user->modules()->attach([3]);
                    $user->getRoles()->sync([7]);*/
                }

                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);

                if ($array[$index['firstNameContact']] == '') {
                    $identification_type = 1;
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                } else {
                    $identification_type = 2;
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];
                }

                // Clase de persona
                $classPerson = Parameter::where('name_parameter', 'ilike', '%' . $array[$index['classPerson']] . '%')
                    ->where('paramtable_id', 2)
                    ->first();

                if (!$classPerson) {
                    $classPerson = Parameter::create([
                        'paramtable_id'  => 2,
                        'code_parameter' => '000',
                        'name_parameter' => $array[$index['classPerson']]
                    ]);
                }

                // Tipo contribuytente
                $taxpayerType = Parameter::where('name_parameter', 'ilike', '%' . $array[$index['taxRegime']] . '%')
                    ->where('paramtable_id', 4)
                    ->first();

                if (!$taxpayerType) {
                    $taxpayerType = Parameter::create([
                        'paramtable_id'  => 2,
                        'code_parameter' => '000',
                        'name_parameter' => $array[$index['taxRegime']]
                    ]);
                }

                //works correctly
                if (trim($array[$index['dateOpening']]) == '00/01/1900') {
                    $array[$index['dateOpening']] = null;
                }

                if (trim($array[$index['checkDigitContact']]) == '#N/D') {
                    $array[$index['checkDigitContact']] = null;
                }

                $array_contact = [
                    'subscriber_id'       => 1,
                    'identification_type' => $identification_type,
                    'identification'      => $array[$index['identificationContact']],
                    'check_digit'         => $array[$index['checkDigitContact']],
                    'name'                => $name,
                    'surname'             => $surname,
                    'address'             => $array[$index['addressContact']],
                    'main_telephone'      => $array[$index['mainTelephoneContact']],
                    'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                    'cell_phone'          => $array[$index['cellphoneContact']],
                    'email'               => strtolower($array[$index['emailContact']]),
                    'is_customer'         => true,

                    'physical_file_number' => $array[$index['physicalFileNumber']],
                    'class_person'         => $classPerson->id,
                    'taxpayer_type'        => $taxpayerType->id,
                    'pay_day'              => $array[$index['payDay']],
                ];

                if ($array[$index['dateOpening']] != null) {
                    $array_contact['date_opening'] = $array[$index['dateOpening']];
                }

                $contact = Contact::updateOrCreate(Arr::only($array_contact, ['identification']), $array_contact);

                $array_contact_customer = [
                    'contact_id'   => $contact->id,
                    'credit_quota' => (int)$array[$index['creditQuotaContact']],
                    //'expiration_days' => $array[$index['expirationDaysContact']]
                ];

                ContactCustomer::updateOrCreate(Arr::only($array_contact_customer, ['contact_id']), $array_contact_customer);

                $city_id = null;

                foreach ($cities as $city) {
                    similar_text($city->city, $array[$index['cityContact']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 50) {
                        $city_id = $city->id;
                        break;
                    }
                }

                $array_contact_warehouse = [
                    'contact_id'      => $contact->id,
                    'code'            => str_pad($array[$index['codeWarehouse']], 3, "0", STR_PAD_LEFT),
                    'description'     => $name,
                    'address'         => $array[$index['addressContact']],
                    'telephone'       => $array[$index['mainTelephoneContact']],
                    'city_id'         => $city_id,
                    'seller_id'       => $seller_id,
                    'credit_quota'    => (int)$array[$index['creditQuotaContact']],
                    'branchoffice_id' => $branchoffice_id
                ];

                ContactWarehouse::updateOrCreate(Arr::only($array_contact_warehouse, ['contact_id', 'code']), $array_contact_warehouse);
                echo ++$countContacts . "\n";
            }
        }
    }

    public function actualizaSucursales()
    {

        $index = [
            'identificationContact' => 0,
            'codeWarehouse'         => 1,
            'nameWarehouse'         => 2,
            'emailSeller'           => 4
        ];

        $file = fopen(storage_path('app/public/sucursales.csv'), "r");

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                //Consulta si existe el usuario vendedors
                if ($array[$index['emailSeller']] !== '') {

                    $user = User::where('email', strtolower($array[$index['emailSeller']]))->first();

                    $contact = Contact::where('identification', $array[$index['identificationContact']])->first();

                    if ($contact) {

                        $city_id = null;

                        $array_contact_warehouse = [
                            'contact_id'      => $contact->id,
                            'code'            => str_pad($array[$index['codeWarehouse']], 3, "0", STR_PAD_LEFT),
                            'description'     => $array[$index['nameWarehouse']],
                            'city_id'         => null,
                            'seller_id'       => $user ? $user->contact_id : null,
                            'credit_quota'    => 0,
                            'branchoffice_id' => null,
                            'updated_at'      => date('Y-m-d H:i:s'),
                            'created_at'      => date('Y-m-d H:i:s')
                        ];

                        ContactWarehouse::updateOrCreate(Arr::only($array_contact_warehouse, ['contact_id', 'code']), $array_contact_warehouse);
                        echo ++$countContacts . "\n";
                    }
                }
            }
        }
    }

    public function nuevosTerceros2020()
    {

        $index = [
            'identificationContact'     => 0,
            'checkDigitContact'         => 1,
            'businessNameContact'       => 2,
            'firstNameContact'          => 3,
            'secondNameContact'         => 4,
            'surnameContact'            => 5,
            'secondSurnameContact'      => 6,
            'addressContact'            => 7,
            'mainTelephoneContact'      => 8,
            'secondaryTelephoneContact' => 9,
            'cellphoneContact'          => 10,
            'emailContact'              => 11,
            'creditQuotaContact'        => 12,
            'expirationDaysContact'     => 13
        ];

        $file = fopen(storage_path('app/public/terceros2020.csv'), "r");

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);

                if ($array[$index['firstNameContact']] == '') {
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                    $identification_type = 1;
                } else {
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];
                    $identification_type = 2;
                }

                $array_contact = [
                    'subscriber_id'       => 1,
                    'identification_type' => $identification_type,
                    'identification'      => $array[$index['identificationContact']],
                    'check_digit'         => $array[$index['checkDigitContact']],
                    'name'                => $name,
                    'surname'             => $surname,
                    'address'             => $array[$index['addressContact']],
                    'main_telephone'      => $array[$index['mainTelephoneContact']],
                    'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                    'cell_phone'          => $array[$index['cellphoneContact']],
                    'email'               => strtolower($array[$index['emailContact']]),
                    'is_customer'         => true,
                    'updated_at'          => date('Y-m-d H:i:s'),
                    'created_at'          => date('Y-m-d H:i:s')
                ];

                $contact = Contact::updateOrCreate(Arr::only($array_contact, ['identification']), $array_contact);

                $array_contact_customer = [
                    'contact_id'      => $contact->id,
                    'credit_quota'    => (int)$array[$index['creditQuotaContact']],
                    'expiration_days' => $array[$index['expirationDaysContact']]
                ];

                ContactCustomer::updateOrCreate(Arr::only($array_contact_customer, ['contact_id']), $array_contact_customer);

                echo ++$countContacts . "\n";
            }
        }
    }


    public function updateContacts(Request $request)
    {
        set_time_limit(0);

        //Recibimos la información
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());
        $countLoad = 0;
        $countLoadContact = 0;

        $contactWarehouses = ContactWarehouse::all();

        // eliminamos los que no tengan vendedor asignado o activo.
        foreach ($contactWarehouses as $key => $contactWarehouse) {

            $userExist = User::where('contact_id', $contactWarehouse->seller_id)->exists();

            if (!$userExist) {
                $contactWarehouse->delete();
                $countLoad++;
            }
        }

        // Actualizamos los terceros
        foreach ($fileData as $key => $item) {

            // Traemos los clientes
            $contact = Contact::where('identification', 'ilike', '%' . $item['NIT/CC'] . '%')->first();

            // Traemos la sucursal
            $contactWarehouse = ContactWarehouse::where('contact_id', $contact->id)
                ->where('code', '00' . $item['SUCURSAL'])
                ->first();

            if ($contactWarehouse) {
                $user = User::where('email', 'ilike', '%' . $item['CORREO VENDEDOR'] . '%')->first();

                if ($user) {
                    if ($contactWarehouse->seller_id != $user->contact_id) {
                        $contactWarehouse->seller_id = $user->contact_id;
                        $contactWarehouse->save();
                        $countLoadContact++;
                    }
                }
            }
        }

        return [
            'tercero sin vendedor' => $countLoad,
            'Actualizado tercero'  => $countLoadContact,
        ];
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 1) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }


    function getContactReport(Request $request)
    {

        $request['typeUser'] = json_decode($request['typeUser']);
        $contacts = DB::select(
            'select  *  from public.reportusers(?,?,?);',
            [$request['typeUser']->client, $request['typeUser']->provider, $request['typeUser']->employee]
        );

        $header = [];
        $body = [];

        //  ***   ENCABEZADO   ***

        foreach ($request->mainThird as $key => $contactMain) {
            $contactMain = json_decode($contactMain);
            $header[] = $contactMain->label;
        }
        if ($request->thirdBranch) {
            foreach ($request->thirdBranch as $key => $thirdBranch) {
                $thirdBranch = json_decode($thirdBranch);
                if ($thirdBranch->label == 'Info Retenciones') {
                    $header[] = '% Ret Fte';
                    $header[] = '% Ret Iva';
                    $header[] = '% Ret Ica';
                } elseif ($thirdBranch->label == 'Vendedor') {
                    $header[] = 'Cod-Vendedor';
                    $header[] = 'Nom-Vendedor';
                } elseif ($thirdBranch->label == 'Identificación') {
                    $header[] = 'Cod-Sucursal';
                } else {
                    $header[] = $thirdBranch->label;
                }
            }
        }
        // array_push($body, $header);

        //  *****    FIN    ******

        //  ****   CUERPO ARCHIVO    ****
        if (count($contacts) > 0) {

            foreach ($contacts as $key => $contact) {

                // SECCION TERCERO PRiNCIPAL
                $data = [];
                foreach ($request->mainThird as $key => $contactMain) {

                    $contactMain = json_decode($contactMain);

                    $value = $contactMain->value;

                    // fecha de creacion, quitamos la hora
                    if ($value == 'created_at') {
                        $date = substr($contact->$value, 0, 10);
                        $data[] = $date;
                    }

                    if ($value == 'aut_rep_cent_riesgo') {

                        $data[] = $contact->$value == 1 ? 'SI' : 'NO';
                    }

                    if ($value == 'available_quota') {

                        $data[] = number_format($contact->$value);
                    }

                    if ($value == 'credit_quota') {

                        $data[] = number_format($contact->$value);
                    }


                    if ($value == 'email') {

                        $data[] = $contact->email_warehouse != '' ? $contact->email_warehouse : $contact->$value;
                    }

                    if ($value == 'identification') {
                        $data[] = preg_replace('([^0-9-])', '', $contact->identification);
                    }

                    if ($value != 'created_at' && $value != 'aut_rep_cent_riesgo' &&
                        $value != 'available_quota' && $value != 'credit_quota' && $value != 'email'
                        && $value != 'identification') {
                        $data[] = $contact->$value;
                    }
                }

                // SECCION TERCERO SUCURSAL-CLIENTES

                if ($request->thirdBranch && !$request['typeUser']->provider == true) {

                    //para clientes
                    if ($request['typeUser']->client == true) {

                        $dataw = null;
                        $dataw = $data;
                        foreach ($request->thirdBranch as $key => $thirdBranch) {
                            $thirdBranch = json_decode($thirdBranch);

                            $value = $thirdBranch->value;
                            // fecha de creacion, quitamos la hora
                            if ($value == 'creation_date') {
                                $date = substr($contact->$value, 0, 10);
                                $dataw[] = $date;
                            } elseif ($value == 'visit_checkin_date') {
                                $date = substr($contact->$value, 0, 10);
                                $dataw[] = $date;
                            } elseif ($value == 'Info Retenciones') {
                                $dataw[] = $contact->ret_fue_prod_percentage;
                                $dataw[] = $contact->ret_iva_prod_percentage;
                                $dataw[] = $contact->ret_ica_prod_percentage;
                            } elseif ($value == 'seller') {
                                $dataw[] = trim($contact->identification_seller);
                                $dataw[] = $contact->seller;

                            } elseif ($value == 'code') {
                                $dataw[] = $contact->code;
                            } else {
                                $dataw[] = $contact->$value;
                            }
                        }
                        array_push($body, $dataw);
                    }

                    // para vendedores
                    if ($request['typeUser']->employee == true) {
                        $dataw = [];
                        $dataw = $data;
                        foreach ($request->thirdBranch as $key => $thirdBranch) {
                            $thirdBranch = json_decode($thirdBranch);

                            $value = $thirdBranch->value;
                            if ($value == 'creation_date') {
                                $date = substr($contact->$value, 0, 10);
                                $dataw[] = $date;
                            } elseif ($value == 'Info Retenciones') {
                                $dataw[] = $contact->ret_fue_prod_percentage;
                                $dataw[] = $contact->ret_iva_prod_percentage;
                                $dataw[] = $contact->ret_ica_prod_percentage;
                            } else {

                                $dataw[] = $contact->$value;
                            }
                        }


                        array_push($body, $dataw);
                    }
                } else {
                    array_push($body, $data);
                }

                //  SECCION SUCURSAL -VENDEDORES
            }
        } else {
            array_push($body, $data);
        }


        //  ***   fin   ******
        $date = date('Y-m-d');
        $pathFile = 'public/CSVUsers/INFORME-TERCEROS-' . $date . '.csv';

        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle('INFORME-TERCEROS-' . $date);

        $letter = 'A';
        $row = 2;
        $months_security = 2;

        foreach ($body as $line) {
            $letter = 'A';
            foreach ($line as $key => $value) {
                $total_letters = $letter;
                // echo $letter++ . '-' . $row. '  '. $value . '=========================';
                $sheet->setCellValue($letter++ . $row, $value);
            }
            $row++;
        }

        $row--;

        $sheet->setAutoFilter('A1:' . $total_letters . $row);

        $this->setStyles($sheet, $row, $total_letters, $header);

        $writer = IOFactory::createWriter($spreadSheet, "Xlsx");

        $fileName = storage_path("app/public/CSVUsers/") . 'INFORME-TERCEROS-' . $date . '.xlsx';
        $writer->save($fileName);


        return Storage::download('/public/CSVUsers/INFORME-TERCEROS-' . $date . '.xlsx');
    }

    public function setStyles($sheet, $row, $total_letters, $headers)
    {
        $letter_ = 'A';
        $styles = [];
        for ($i = 0; $i < count($headers); $i++) {

            $styles[$letter_] = ['width' => 20];

            $letter_++;
        }


        $generalStyles = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'borders'   => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN
                ]
            ]
        ];

        $counter = 0;

        foreach ($styles as $letter => $style) {
            $sheet->setCellValue($letter . '1', $headers[$counter++]);
            $sheet->getStyle($letter . '1')->getAlignment()->setWrapText(true);
            $sheet->getStyle($letter . '1')->getFont()->setBold(true);
            $sheet->getStyle($letter . '1')->getFont()->getColor()->setARGB('FFFFFF');
            $sheet->getStyle($letter . '1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('6386D3');
            $sheet->getStyle($letter . '1')->getFill()->getEndColor()->setARGB('6386D3');
            $sheet->getColumnDimension($letter)->setWidth($style['width']);
            $sheet->getColumnDimension($letter)->setAutoSize(isset($style['autoSize']) ? $style['autoSize'] : false);
            $sheet->getStyle($letter . '2:' . $letter . $row)->getAlignment()->setWrapText(isset($style['wrapText']) ? $style['wrapText'] : false);
        }
        $sheet->getStyle('A1:' . $total_letters . $row)->applyFromArray($generalStyles);
    }

    public function readFile($pathFile, $body)
    {
        Storage::disk('local')->put($pathFile, null);
        $file = fopen(storage_path('app/' . $pathFile), "w");

        foreach ($body as $line) {
            fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fputcsv($file, $line, ";");
        }
        rewind($file);
        fclose($file);
    }


    public function getUsers(Request $request)
    {
        $users = Contact::orderBy('name', 'asc')->whereHas('users')
            ->with(['users', 'users.getRoles'])
            ->when($request->name, function ($query) use ($request) {
                $query->where(function ($subquery) use ($request) {
                    $subquery->where('name', 'ILIKE', '%' . $request->name . '%')->orWhere('surname', 'ILIKE', '%' . $request->name . '%');
                });
            })->get();

        $users->map(function ($u) {
            $u->user_id = $u->users[0]->id;
            $u->rol_id = isset($u->users[0]->getRoles[0]) ? $u->users[0]->getRoles[0]->id : null;
        });

        $users = $this->paginateCollections($users->toArray(), $request->perPage, $request->page);


        return response()->json($users, 200);
    }


    public function getClients(Request $request)
    {

        return Contact::where('is_customer', true)->where(function ($query) use ($request) {
            $query->where('name', 'ILIKE', '%' . $request->nameClient . '%')->OrWhere('surname', 'ILIKE', '%' . $request->nameClient . '%')->OrWhere('identification', 'ILIKE', '%' . $request->nameClient . '%');
        })->get();
    }

    /**
     * Obtener los clientes asociados a un vendedor
     *
     * @param Request $request Contiene los filtros a aplicar en la consulta
     * @return JsonResponse Contiene un array con los clientes
     * @author Jhon García
     */
    public function appListCustomerBySeller(Request $request)
    {
        if (!$request->seller_id) {
            return response()->json([
                'error' => [
                    'message' => 'Faltan parametros. (seller_id)'
                ]
            ], 400);
        }

        $contacts_warehouses = ContactWarehouse::select(
            'contacts_warehouses.id',
            'contacts_warehouses.contact_id',
            'contacts_warehouses.branchoffice_id',
            'cc.credit_quota',
            'cc.percentage_discount',
            'c.name AS tercer_nom_ter',
            'c.tradename',
            'c.identification',
            'contacts_warehouses.address AS clibod_direccion',
            'contacts_warehouses.telephone AS clibod_telefono',
            'cities.city'
        )
            ->with('lastSalesProducts:contact_warehouse_id,product_id,last_sale_date,last_sale_quantity,last_sale_unit_value,suggested')
            ->selectRaw("CAST(c.id AS varchar) AS tercer_cod_ter")
            ->selectRaw("CAST(contacts_warehouses.id AS varchar) AS clibod_cod_bodega")
            ->selectRaw("CONCAT(c.identification,' - ',contacts_warehouses.code,' - ', contacts_warehouses.description ) AS clibod_descripcion")
            ->selectRaw("CASE WHEN contacts_warehouses.latitude = '-' OR contacts_warehouses.latitude is null THEN '0' ELSE contacts_warehouses.latitude END AS clibod_latitud")
            ->selectRaw("CASE WHEN contacts_warehouses.length = '-' OR contacts_warehouses.length is null THEN '0' ELSE contacts_warehouses.length END AS clibod_longitud")
            ->join('contacts AS c', 'c.id', '=', 'contacts_warehouses.contact_id')
            ->join('contacts_customers AS cc', 'cc.contact_id', '=', 'c.id')
            ->leftJoin('cities', 'contacts_warehouses.city_id', '=', 'cities.id')
            ->where('contacts_warehouses.state', true)
            ->whereHas('contact', function ($query) {
                $query->whereHas('customer', function ($query) {
                    $query->where('category_id', '!=', '24473');
                    $query->orWhere('category_id', null);
                });
            })
            ->where('contacts_warehouses.seller_id', $request->seller_id)
            ->orderby('c.name', 'ASC')
            ->get();

        if ($contacts_warehouses->count() <= 0) {
            return response()->json([
                'error' => [
                    'message' => 'No se ha encontrado informacion.'
                ]
            ], 404);
        }

        $branch_offices_warehouses = BranchOfficeWarehouse::select('id', 'branchoffice_id')
            ->where('warehouse_code', '01')
            ->get();

        $date_from = now()->addDays(-1)->format('Y-m-d') . ' ' . ' 00:00:00';
        $date_up = now()->format('Y-m-d') . ' ' . ' 23:59:59';

        // Consultar pedidos de los últimos 2 días
        $orders = Document::select('id')
            ->whereBetween('document_date', [$date_from, $date_up])
            ->whereHas('warehouse', function ($warehouse) use ($request) {
                $warehouse->where('seller_id', $request->seller_id);
            })
            ->whereHas('vouchertype', function ($query) {
                $query->where('code_voucher_type', '010');
            })
            ->where('in_progress', false)
            ->get();

        $inventories = Inventory::select('inventories.id', 'branchoffice_warehouse_id', 'product_id')
            ->join('branchoffice_warehouses as bw', 'bw.id', '=', 'inventories.branchoffice_warehouse_id')
            ->whereIn('bw.branchoffice_id', $contacts_warehouses->pluck('branchoffice_id')->toArray())
            ->where('bw.warehouse_code', '01')
            ->where('available', '>', '0')
            ->get();

        $new_receivable = true;

        // Validación si es COLSAISA
        if (Subscriber::where('identification', '900338016')->exists()) {
            $new_receivable = false;
        }

        $year_month = now()->format('Ym');

        // Cartera del cliente
        foreach ($contacts_warehouses as $key => $contact_warehouse) {

            $contact_warehouse->total_receivables = 0;
            $contact_warehouse->wallet_overdue = false;
            $contact_warehouse->contact_wallet = [];

            if ($new_receivable) {
                $contactWallet = $this->balances_service->searchWalletByContactId($year_month, $contact_warehouse->contact_id, 24604);

                if (!!count($contactWallet)) {
                    $contact_warehouse->total_receivables = $contactWallet[0]->final_balance;
                }
            } else {
                $contactWallet = $this->documentsBalanceService->getTrackingByContactId($contact_warehouse->contact_id);

                $contact_warehouse->contact_wallet = $contactWallet['data'];

                $invoiceBalanceTotal = 0;

                foreach ($contactWallet['data'] as $key => $wallet) {
                    $invoiceBalanceTotal += $wallet->receivable[0]->invoice_balance;

                    $date1 = Carbon::parse($wallet->document_date);
                    $contact_warehouse->wallet_overdue = $date1->diffInDays() > 60;
                }

                $contact_warehouse->total_receivables = $invoiceBalanceTotal;
            }

            //invoice_balance
            $contact_warehouse->total_available = $contact_warehouse->credit_quota - $contact_warehouse->total_receivables;

            $suggested = !!$contact_warehouse->lastSalesProducts->firstWhere('suggested', true);

            if ($suggested) {

                $suggested = !$orders->firstWhere('warehouse_id', $contact_warehouse->id);

                if ($suggested) {

                    $branch_office_warehouse = $branch_offices_warehouses->firstWhere('branchoffice_id', $contact_warehouse->branchoffice_id);

                    $products_suggested = $contact_warehouse->lastSalesProducts->where('suggested', true);

                    $inventory = $inventories->where('branchoffice_warehouse_id', $branch_office_warehouse->id)
                        ->whereIn('product_id', $products_suggested->pluck('product_id')->toArray())
                        ->first();

                    $suggested = !!$inventory;
                }
            }

            $contact_warehouse->suggested = $suggested;

            $part = explode('/', $contact_warehouse->tradename);
            $cityDirection = $contact_warehouse->city != '' && $contact_warehouse->city != null ? $contact_warehouse->city : '';
            if ($contact_warehouse->clibod_direccion != '' && $contact_warehouse->clibod_direccion != null) {
                $cityDirection .= $cityDirection != null ? '-' . $contact_warehouse->clibod_direccion : ' ' . $contact_warehouse->clibod_direccion;
            }

            if ($contact_warehouse->tradename != '' || $contact_warehouse->tradename != null) {

                $contact_warehouse->clibod_descripcion .= count($part) == 2 ? ' /' . $part[1] . ' ' . $cityDirection
                    : ' /' . $part[0] . ' ' . $cityDirection;
            } else {
                $contact_warehouse->clibod_descripcion .= ' ' . $cityDirection;
            }
        }

        return response()->json($contacts_warehouses, 200);
    }

    /**
     * Crear tercero con información básica
     *
     * @param CreateContactBasicRequest $request Contiene la información del tercero a crear
     * @return JsonResponse Contiene el mensaje de respuesta y la información del tercero creado
     * @author Jhon García
     */
    public function storeBasicAsCustomer(CreateContactBasicRequest $request)
    {
        $contact = $this->contactRepository->storeBasicAsCustomer($request->all());

        return response()->json([
            'message' => 'Tercero creado exitosamente',
            'data'    => $contact->toArray()
        ], 200);
    }

    /**
     * Obtener los terceros trasnportadores
     *
     * @return JsonResponse Contiene los terceros transportadores
     * @author Jhon García
     */
    public function getConveyors()
    {
        $conveyors = $this->contactRepository->getConveyors();

        return response()->json([
            'message' => 'Transportadoras obtenidas exitosamente',
            'data'    => $conveyors->toArray()
        ], 200);
    }
}
