<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use App\Entities\Parameter;
use Illuminate\Http\Request;
use App\Traits\LoadImageTrait;
use App\Repositories\ParameterRepository;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateParameterAPIRequest;
use App\Http\Requests\ApiRequests\UpdateParameterAPIRequest;

/**
 * Class ParameterController
 * @package App\Http\Controllers\ApiControllers
 */

class ParameterAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  ParameterRepository */
    private $parameterRepository;

    public function __construct(ParameterRepository $parameterRepo)
    {
        $this->parameterRepository = $parameterRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     *
     */
    public function index(Request $request)
    {
        if (isset($request->paramtable_id) && !empty($request->paramtable_id)) {
            $parameters = $this->parameterRepository
                ->where(['paramtable_id' => $request->paramtable_id])
                ->with(['accountingAccount', 'line', 'sub_line', 'city', 'branchoffice_warehouse' => function ($query) {
                    $query->with(['branchoffice']);
                }])
                ->orderBy('code_parameter', 'ASC')
                ->get();
        }else if (isset($request->code_table) && !empty($request->code_table)) {
            $parameters = $this->parameterRepository
                ->whereHas('paramtable', function ($paramtable) use ($request) {
                    $paramtable->where('code_table', $request->code_table);
                })
                ->orderBy('code_parameter', 'ASC')
                ->get();
        }

        return $this->sendResponse($parameters->toArray(), 'Parameters retrieved successfully');
    }

    /**
     * @param CreateParameterAPIRequest $request
     * @return Response
     *
     *
     */
    public function store(CreateParameterAPIRequest $request)
    {
        $input = $request->except('image');

        $input['image'] =  $this->saveImage($request->image, 'parameters');
        $parameters = $this->parameterRepository->create($input);

        return $this->sendResponse($parameters->toArray(), 'Parameter saved successfully');
    }

    /**
     * @param int $id
     * @return Response

     */
    public function show($id)
    {
        /** @var Parameter $parameter */
        $parameter = $this->parameterRepository->findWithoutFail($id);

        if (empty($parameter)) {
            return $this->sendError('Parameter not found');
        }

        return $this->sendResponse($parameter->toArray(), 'Parameter retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateParameterAPIRequest $request
     * @return Response
     *
     *
     */
    public function update($id, UpdateParameterAPIRequest $request)
    {
        $input = $request->except('image', 'line', 'sub_line', 'city', 'branchoffice_warehouse');

        /** @var Parameter $parameter */
        $parameter = $this->parameterRepository->findWithoutFail($id);

        if ($parameter->image != $request->image) {
            $input['image'] =  $this->saveImage($request->image, 'parameters');
        }


        if (empty($parameter)) {
            return $this->sendError('Parameter not found');
        }
        $parameter = $this->parameterRepository->update($input, $id);

        return $this->sendResponse($parameter->toArray(), 'Parameter updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/parameters/{id}",
     *      summary="Remove the specified Parameter from storage",
     *      tags={"Parameter"},
     *      description="Delete Parameter",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Parameter",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Parameter $parameter */
        $parameter = $this->parameterRepository->findWithoutFail($id);

        if (empty($parameter)) {
            return $this->sendError('Parameter not found');
        }

        $parameter->delete();

        return $this->sendResponse($id, 'Parameter deleted successfully');
    }

    public function listParameter(Request $request)
    {
        $this->parameterRepository->pushCriteria(new RequestCriteria($request));
        $this->parameterRepository->pushCriteria(new LimitOffsetCriteria($request));

        $parameters = $this->parameterRepository->findForParamTable($request);

        return $this->sendResponse($parameters->toArray(), 'Parameters retrieved successfully');
    }

    public function getPriceList()
    {
        $list_price = $this->parameterRepository->getPriceList();
        return $this->sendResponse($list_price->toArray(), 'Parameters retrieved successfully');;
    }
    public function getTypePayment()
    {
        $typePayment = $this->parameterRepository->getTypePayment();
        return $this->sendResponse($typePayment->toArray(), 'Parameters retrieved successfully');
    }
    public function getTypeNegotiationPortfolio()
    {
        $type_negotiation_portfolio = $this->parameterRepository->getTypeNegotiationPortfolio();
        return $this->sendResponse($type_negotiation_portfolio->toArray(), 'Parameters retrieved successfully');
    }
    public function getParameterCategory(Request $request)
    {
        $group = $this->parameterRepository->getParameterCategory($request);
        return $this->sendResponse($group->toArray(), 'Category retrieved successfully');
    }

    /**
     * Obtiene los estados de los documentos
     *
     * @return array
     */
    public function documentsStatus()
    {
        $status = $this->parameterRepository->documentsStatus();
        return $this->sendResponse($status, 'Documents status retrieved successfully');
    }


    public function getBancos()
    {
        $parameters = Parameter::where('paramtable_id', 72)->select('id', 'name_parameter as nombre')->get();
        if (count($parameters) > 0) {

            return response()->json($parameters, 200);
        }
        return $this->sendError([], 200);
    }

    public function paramsByTable($tableparam_id)
    {
        $parameters = Parameter::select('id', 'paramtable_id', 'code_parameter', 'name_parameter')
            ->where('paramtable_id', $tableparam_id)->get();
        return response()->json($parameters, 200);
    }

    public function optionsAurora($tableparam_id, Request $request)
    {
        $parameters = Parameter::select('id', 'paramtable_id', 'code_parameter', 'name_parameter')
            ->where('paramtable_id', $tableparam_id)
            ->where('code_parameter', 'ilike', $request->filter)
            ->get();
        return response()->json($parameters, 200);
    }
}
