<?php

namespace App\Http\Controllers\ApiControllers;

// Services
use App\Repositories\ServiceRequestRepository;
use App\Services\ServiceRequestService;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

class ServiceRequestApiController extends AppBaseController
{

    protected $serviceRequestService;
    protected $serviceRequestRepository;

    function __construct(ServiceRequestService $serviceRequestService, 
                        ServiceRequestRepository $serviceRequestRepository)
    {
        $this->serviceRequestService = $serviceRequestService;
        $this->serviceRequestRepository = $serviceRequestRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $res = $this->serviceRequestService->getServiceRequestWithHistoryStatus($request);
        return response()->json($res, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @author santiago torres | kevin galingo
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'service_requests_types_id'=> $request->type,
            'product_id'=> $request->product,
            'contact_id'=> $request->contact,
            'contacts_warehouse_id'=> $request->contactsWarehouse,
            'seller_id'=> $request->seller,
            'lot'=>$request->lot,
            'due_date'=>$request->due_date,
            'observation'=>$request->observation,
            'unit_value'=>$request->unit_value,
            'branchoffice_id'=>$request->branchoffice_id,
            'number_batteries'=>$request->number_batteries,
        ];

        $response = $this->serviceRequestRepository->store($data, $request);
        return response()->json($response, $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * 
     * Este metodo se encarga de actualizar el estado 
     * para la solicitud de servicio.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function updateState(Request $request)
    {
        $res = $this->serviceRequestService->updateState($request);
        return response()->json($res, $res['code']);
    }

    /*
     *crea documento para solicitudes de servicio
     * 
     * @author santiago torres | Kevin galindo
     */

    public function createDocument(Request $request)
    {
        //crear documento
        $response =$this->serviceRequestService->createDocument($request);
        if ($response->code == 500) {
            dd('fallo la creación del doc');
        }
        return response()->json($response->data, $response->code);
    }

    public function excelDespatch()
    {
        return 'si';
    }
}
