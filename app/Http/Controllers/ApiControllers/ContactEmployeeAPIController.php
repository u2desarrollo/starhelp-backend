<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactEmployeeAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactEmployeeAPIRequest;
use App\Entities\ContactEmployee;
use App\Repositories\ContactEmployeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactEmployeeController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactEmployeeAPIController extends AppBaseController
{
    /** @var  ContactEmployeeRepository */
    private $contactEmployeeRepository;

    public function __construct(ContactEmployeeRepository $contactEmployeeRepo)
    {
        $this->contactEmployeeRepository = $contactEmployeeRepo;
    }

    /**
     * Display a listing of the ContactEmployee.
     * GET|HEAD /contactEmployees
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contactEmployeeRepository->pushCriteria(new RequestCriteria($request));
        $this->contactEmployeeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactEmployees = $this->contactEmployeeRepository->all();

        return $this->sendResponse($contactEmployees->toArray(), 'Contact Employees retrieved successfully');
    }

    /**
     * Store a newly created ContactEmployee in storage.
     * POST /contactEmployees
     *
     * @param CreateContactEmployeeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateContactEmployeeAPIRequest $request)
    {
        $input = $request->all();

        $contactEmployees = $this->contactEmployeeRepository->create($input);

        return $this->sendResponse($contactEmployees->toArray(), 'Contact Employee saved successfully');
    }

    /**
     * Display the specified ContactEmployee.
     * GET|HEAD /contactEmployees/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ContactEmployee $contactEmployee */
        $contactEmployee = $this->contactEmployeeRepository->findWithoutFail($id);

        if (empty($contactEmployee)) {
            return $this->sendError('Contact Employee not found');
        }

        return $this->sendResponse($contactEmployee->toArray(), 'Contact Employee retrieved successfully');
    }

    /**
     * Update the specified ContactEmployee in storage.
     * PUT/PATCH /contactEmployees/{id}
     *
     * @param  int $id
     * @param UpdateContactEmployeeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactEmployeeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ContactEmployee $contactEmployee */
        $contactEmployee = $this->contactEmployeeRepository->findWithoutFail($id);

        if (empty($contactEmployee)) {
            return $this->sendError('Contact Employee not found');
        }

        $contactEmployee = $this->contactEmployeeRepository->update($input, $id);

        return $this->sendResponse($contactEmployee->toArray(), 'ContactEmployee updated successfully');
    }

    /**
     * Remove the specified ContactEmployee from storage.
     * DELETE /contactEmployees/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ContactEmployee $contactEmployee */
        $contactEmployee = $this->contactEmployeeRepository->findWithoutFail($id);

        if (empty($contactEmployee)) {
            return $this->sendError('Contact Employee not found');
        }

        $contactEmployee->delete();

        return $this->sendResponse($id, 'Contact Employee deleted successfully');
    }

    public function seller(Request $request)
    {
        return $this->sendResponse($this->contactEmployeeRepository->getSellers($request), 'Contact Employees retrieved successfully');
    }
}
