<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\FixAssetRepository;
use App\Services\FixAssetService;

class FixAssetApiController extends Controller
{

    private $fixAssetRepository;
    private $fixAssetService;

    public function __construct(FixAssetRepository $fixAssetRepository, FixAssetService $fixAssetService)
    {
        $this->fixAssetRepository = $fixAssetRepository;
        $this->fixAssetService = $fixAssetService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = $this->fixAssetRepository->findAll($request);
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->fixAssetRepository->create($request->all());
        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->fixAssetRepository->findById($id);
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->fixAssetRepository->update($request->all(), $id);
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fix = $this->fixAssetRepository->findById($id);
        $fix->delete();
        return response()->json($fix, 200);
    }

    public function getLastConsecutiveSuggested()
    {
        $fixConsecutive = $this->fixAssetRepository->getLastConsecutiveSuggested();
        return response()->json($fixConsecutive, 200);

    }

    public function loadFixedAssetsFile(Request $request)
    {
        $response = $this->fixAssetService->loadFixedAssetsFile($request);
        return response()->json($response['data'], $response['code']);
    }

    public function downloadFixedAssets(Request $request)
    {
        $response = $this->fixAssetService->downloadFixedAssets($request);
        return response()->stream($response['file'], $response['status'], $response['headers']);
    }

    public function createDepreciationMonthly(Request $request, $id)
    {
        $response = $this->fixAssetService->createDepreciationMonthly($request, $id);
        return response()->json($response['data'], $response['code']);
    }

    public function getDepreciationMonthly(Request $request, $id)
    {
        $response = $this->fixAssetService->getDepreciationMonthly($request, $id);
        return response()->json($response['data'], $response['code']);
    }

    public function voucherAssetsFixedDepreciation(Request $request, $yearMonth)
    {
        $response = $this->fixAssetService->voucherAssetsFixedDepreciation($request, $yearMonth);
        return response()->json($response['data'], $response['code']);
    }

    public function getVoucherAssetsFixedDepreciation(Request $request)
    {
        $response = $this->fixAssetService->getVoucherAssetsFixedDepreciation($request);
        return response()->json($response['data'], $response['code']);
    }

    public function regenerateVoucherAssetsFixedDepreciation(Request $request, $document_id)
    {
        $response = $this->fixAssetService->regenerateVoucherAssetsFixedDepreciation($request, $document_id);
        return response()->json($response['data'], $response['code']);
    }

    public function regenerateExcelVoucherAssetsFixed(Request $request, $document_id)
    {
        $response = $this->fixAssetService->regenerateExcelVoucherAssetsFixed($request, $document_id);
        return response()->json($response['data'], $response['code']);
    }

    public function createMasiveDepreciationMonthly(Request $request)
    {
        $response = $this->fixAssetService->createMasiveDepreciationMonthly($request);
        return response()->json($response, 200);
    }
}
