<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\AuroraRepository;
use App\Services\AuroraService;
use Response;

class AuroraApiController extends AppBaseController
{
    private $auroraRepository;
    private $auroraService;
    //
    public function __construct(AuroraRepository $auroraRepository, AuroraService $auroraService)
    {
        $this->auroraRepository = $auroraRepository;
        $this->auroraService = $auroraService;
    }


    public function index(Request $request)
    {
        $aurora = $this->auroraRepository->findAll($request);
        return $this->sendResponse($aurora->toArray(), 'Aurora retrieved successfully');
    }

    public function store(Request $request)
    {
        $response = $this->auroraService->createProcess($request);
        return response()->json($response->data, $response->code);
    }

    public function show($id)
    {
        // $response = $this->accountRepository->findAccountById($id);
        // return response()->json($response->data, $response->code);
    }

    public function update($id, Request $request)
    {
        // $response = $this->accountService->updateAccount($id, $request);
        // return response()->json($response->data, $response->code);
    }

    public function destroy($id)
    {
        $response = $this->auroraService->deleteAccount($id);
        return response()->json($response->data, $response->code);
    }
}
