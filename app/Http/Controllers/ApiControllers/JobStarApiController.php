<?php

namespace App\Http\Controllers\ApiControllers;

use App\Services\JobStarService;
use App\Repositories\JobStarRepository;

use App\Http\Requests\ApiRequests\CreateJobStarApiControllerRequest;
use App\Http\Requests\ApiRequests\UpdateJobStarApiControllerRequest;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;

use Illuminate\Http\Request;
use Response;

class JobStarApiController extends AppBaseController
{
    private $jobStarService;
    private $jobStarRepository;

    public function __construct(JobStarService $jobStarService, JobStarRepository $jobStarRepository)
    {
        $this->jobStarService = $jobStarService;
        $this->jobStarRepository = $jobStarRepository;
    }

    /**
     * lista de jobs.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function index(Request $request)
    {
        $jobStars = $this->jobStarRepository->findAll($request);
        return $this->sendResponse($jobStars, 'JobStars retrieved successfully');
    }

    /**
     * Crear un job.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function store(CreateJobStarApiControllerRequest $request)
    {
        $jobStar = $this->jobStarService->create($request);
        return $this->sendResponse($jobStar, 'JobStar created successfully');
    }

    /**
     * Traer un job.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function show($id)
    {
        $jobStar = $this->jobStarRepository->findById($id);
        return $this->sendResponse($jobStar, 'JobStar retrieved successfully');
    }

    /**
     * Actualizar un job.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function update(UpdateJobStarApiControllerRequest $request, $id)
    {
        $jobStar = $this->jobStarRepository->update($request->all(), $id);
        return $this->sendResponse($jobStar, 'JobStar updated successfully');
    }

    /**
     * Cerrar un job.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function destroy($id)
    {
        $jobStar = $this->jobStarRepository->close($id);

        if ($jobStar) {
            return $this->sendResponse($jobStar, 'JobStar closed successfully');
        }

        return response()->json('No hay registro...', 404);
    }

    /**
     * Agregar 1 o varios archivos a un job.
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function addFilesToJobStar(Request $request, $id)
    {
        $response = $this->jobStarService->addFilesToJobStar($request, $id);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Eliminar un archivo de la tarea
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function deleteFileToJobStar(Request $request, $id)
    {
        $response = $this->jobStarService->deleteFileToJobStar($request, $id);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Agregar los miembros aun job de forma masiva
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function addMembers(Request $request, $id)
    {
        $response = $this->jobStarRepository->addMembers($id, $request->members);
        return response()->json($response['data'], $response['code']);
    }

    /**
     * Agregar los miembros aun job de forma masiva
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function getJobsStarByUser(Request $request)
    {
        $jobsStar = $this->jobStarRepository->findAllByUser($request);
        return $this->sendResponse($jobsStar, 'JobStar updated successfully');
    }

    /**
     * Agregar la interaccion al job
     *
     * @param Request $request
     * @return Response
     * @author Kevin Galindo
     */
    public function addInteraction(Request $request, $id)
    {
        $response = $this->jobStarService->addInteraction($request, $id);
        return response()->json($response['data'], $response['code']);
    }
}
