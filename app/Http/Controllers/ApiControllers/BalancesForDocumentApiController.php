<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BalancesForDocumentRepository;
use App\Services\BalancesForDocumentService;
use App\Services\DocumentsService;
use App\Entities\ContactWarehouse;
use Response;

class BalancesForDocumentApiController extends AppBaseController
{
    private $balancesForDocumentRepository;
    private $balancesForDocumentService;
    private $documentService;
    //
    public function __construct(BalancesForDocumentRepository $balancesForDocumentRepository, 
                                BalancesForDocumentService $balancesForDocumentService,
                                DocumentsService $documentService)
    {
        $this->balancesForDocumentRepository = $balancesForDocumentRepository;
        $this->balancesForDocumentService = $balancesForDocumentService;
        $this->documentService = $documentService;
    }

    public function fetchFavorBalanceContact(Request $request)
    {
        $balancesForDocuments = $this->balancesForDocumentRepository->fetchFavorBalanceContact($request);
        $balancesForDocuments = $this->balancesForDocumentService->calculateBalanceContact($request, $balancesForDocuments->toArray());
        return $this->sendResponse($balancesForDocuments, 'BalancesForDocument retrieved successfully');
    }

    public function fetchPendingBalanceContact(Request $request)
    {
        $balancesForDocuments = $this->balancesForDocumentRepository->fetchPendingBalanceContact($request);
        $balancesForDocuments = $this->balancesForDocumentService->calculateBalanceContact($request, $balancesForDocuments->toArray());
        return $this->sendResponse($balancesForDocuments, 'BalancesForDocument retrieved successfully');
    }

    public function balancesForDocumentsDefaultCrossBalances()
    {
        $balancesForDocuments = $this->balancesForDocumentRepository->balancesForDocumentsDefaultCrossBalances();
        return $this->sendResponse($balancesForDocuments->toArray(), 'BalancesForDocument retrieved successfully');
    }

    public function getAllContactWarehouse(Request $request)
    {
        $contactWarehouse =  ContactWarehouse::when($request->filterText ,function ($query) use ($request) {
            $query->orWhere('description', 'ILIKE', '%' . $request->filterText . '%');
            $query->orWhere('code', 'ILIKE', '%' . $request->filterText . '%');
        })
        ->when($request->contact_id, function ($query) use ($request) {
            $query->where('contact_id', $request->contact_id);
        })
        ->with([
            'contact'
        ])
        ->get();

        return $this->sendResponse($contactWarehouse, 'successfully');

    }

    public function createDocumentByCrossBalance(Request $request)
    {
        $response = $this->balancesForDocumentService->createDocumentByCrossBalance($request);
        return $this->sendResponse($response, 'successfully');
    }

    public function createTransactionsByDocumentCrossBalance(Request $request)
    {
        $response = $this->balancesForDocumentService->createTransactionsByDocumentCrossBalance($request);
        return response()->json($response['data'], $response['code']);
    }

    public function deleteDocumentTransactionsBySequence(Request $request)
    {
        $response = $this->balancesForDocumentService->deleteDocumentTransactionsBySequence($request);
        return response()->json($response, 200);
    }

    public function checkDocumentInitiatedByAnotherUser(Request $request)
    {
        $response = $this->balancesForDocumentService->checkDocumentInitiatedByAnotherUser($request);
        return response()->json($response, 200);
    }

    public function finalizeDocumentCrossBalance(Request $request)
    {
        $response = $this->documentService->finalizeDocumentCrossBalance($request);
        return response()->json($response, $response->code);
    }

    public function getDocumentCrossBalance(Request $request)
    {
        $response = $this->balancesForDocumentService->getDocumentCrossBalance($request->id);
        return response()->json($response, 200);
    }
}
