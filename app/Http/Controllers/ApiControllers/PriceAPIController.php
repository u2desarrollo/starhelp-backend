<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePriceAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePriceAPIRequest;
use App\Entities\Price;
use App\Entities\Product;
use App\Entities\Parameter;
use App\Entities\BranchOffice;
use App\Repositories\PriceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

/**
 * Class PriceController
 * @package App\Http\Controllers\ApiControllers
 */

class PriceAPIController extends AppBaseController
{
    /** @var  PriceRepository */
    private $priceRepository;

    public function __construct(PriceRepository $priceRepo)
    {
        $this->priceRepository = $priceRepo;
    }

    /**
     * Display a listing of the Price.
     * GET|HEAD /prices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->priceRepository->pushCriteria(new RequestCriteria($request));
        $this->priceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $prices = $this->priceRepository->all();

        return $this->sendResponse($prices->toArray(), 'Prices retrieved successfully');
    }

    /**
     * Store a newly created Price in storage.
     * POST /prices
     *
     * @param CreatePriceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePriceAPIRequest $request)
    {
        $input = $request->all();

        $prices = $this->priceRepository->create($input);

        return $this->sendResponse($prices->toArray(), 'Price saved successfully');
    }

    /**
     * Display the specified Price.
     * GET|HEAD /prices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Price $price */
        $price = $this->priceRepository->findWithoutFail($id);

        if (empty($price)) {
            return $this->sendError('Price not found');
        }

        return $this->sendResponse($price->toArray(), 'Price retrieved successfully');
    }

    /**
     * Update the specified Price in storage.
     * PUT/PATCH /prices/{id}
     *
     * @param  int $id
     * @param UpdatePriceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriceAPIRequest $request)
    {
        $input = $request->except(["branch_office","parameter","product"]);

        $price = $this->priceRepository->updatePrice($input, $id);

        return $this->sendResponse($price->toArray(), 'Price updated successfully');
    }

    /**
     * Remove the specified Price from storage.
     * DELETE /prices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Price $price */
        $price = $this->priceRepository->findWithoutFail($id);

        if (empty($price)) {
            return $this->sendError('Price not found');
        }

        $price->delete();

        return $this->sendResponse($id, 'Price deleted successfully');
    }
    /* Obtiene los precios y los manda a la ruta api */
    public function getPrice(Request $request){

        $prices = $this->priceRepository->getPricesGrid($request);

        if(empty($prices)){
            return $this->sendError("Price not found");
        }

        return $this->sendResponse($prices,'Price retrieved successfully');
    }

    /**
     * Actualiza precios
     *
     * @param Request $request Contiene un array con los precios a actualizar
     * @return array Contiene los precios actualizados
     * @author Jhon García
     */
    public function updatedPriceArray(Request $request)
    {
        $prices = $this->priceRepository->updatedPriceArray($request);

        return $this->sendResponse($prices, 'Price updated successfully');
    }

    public function updatedPriceArrayExcel(Request $request){
        $prices=$this->priceRepository->updatedPriceArrayExcel($request);

        return $this->sendResponse($prices,'Price updated successfully');
    }

    public function formatPriceArrayExcel(Request $request){
        $response=$this->priceRepository->formatPriceArrayExcel($request);
        return response()->json($response, $response['code']);
    }

    public function updateprice($documentProduct)
    {
        return $this->priceRepository->updatePriceModel($documentProduct);
    }

    // CARGAR PRICE
    public function loadPricetoCsv(Request $request)
    {
        set_time_limit(0);

        //Recibimos la información
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());
        $countLoad = 0;
        $productNotExist = [];

        // Recorremos el arreglo y creamos los registros
        foreach ($fileData as $key => $value) {

            // Validamos si tiene lista de precios espesificada en el archivo
            if (!empty($value['CODIGO_LISTA'])) {
                // Traemos la lista de precios
                $listPrice = Parameter::where('code_parameter', $value['CODIGO_LISTA'])->where('paramtable_id', 14)->first();

                // En caso de que no exista seguimos con el siguiente registro.
                if (!$listPrice) {
                    continue;
                }

            } else {
                // Traemos la lista de precios
                $listPrice = Parameter::where('code_parameter', '001')->where('paramtable_id', 14)->first();

                // En caso de que no exista seguimos con el siguiente registro.
                if (!$listPrice) {
                    continue;
                }
            }

            if (empty($value['PRECIO'])) {
                continue;
            }

            $product = Product::where('code', trim($value['REFERENCIA']))->first();

            if ($product) {

                if (isset($value['SEDE']) && !empty($value['SEDE']) ) {
                    $branchOffice = BranchOffice::where('name', $value['SEDE'])->first();

                    if ($branchOffice) {
                        // actualizar previous_price
                        $price_ = Price :: where('product_id' , $product->id)
                        ->where('price_list_id' , $listPrice->id)
                        ->where('branchoffice_id' , $branchOffice->id)
                        ->first();

                        if ($price_) {
                            $price_->previous_price = $price_->price;
                            $price_->save();
                        }

                        Price::updateOrCreate([
                            'product_id' => $product->id,
                            'price_list_id' => $listPrice->id,
                            'branchoffice_id' => $branchOffice->id,
                        ],[
                            'product_id' => $product->id,
                            'price_list_id' => $listPrice->id,
                            'branchoffice_id' => $branchOffice->id,
                            'price' => $value['PRECIO'],
                            'include_iva' => false,
                            // 'previous_price' => $value['PRECIO'],
                        ]);
                        $countLoad ++;
                    }
                } else {
                    $price_ = Price :: where('product_id' , $product->id)
                        ->where('price_list_id' , $listPrice->id)
                        ->whereNull('branchoffice_id')
                        ->first();

                    if ($price_){
                        $price_->previous_price = $price_->price;
                        $price_->save();
                    }

                    Price::updateOrCreate([
                        'product_id' => $product->id,
                        'price_list_id' => $listPrice->id,
                        'branchoffice_id' => null,
                    ],[
                        'product_id' => $product->id,
                        'price_list_id' => $listPrice->id,
                        'branchoffice_id' => null,
                        'price' => $value['PRECIO'],
                        'include_iva' => false,
                        // 'previous_price' => $value['PRECIO'],
                    ]);
                    $countLoad ++;
                }


            } else {
                $productNotExist[] = $value['REFERENCIA'];
            }

        }

        return response()->json([
            'Registros creatos, cantidad: '.$countLoad,
            'Registros NO creatos, cantidad: '.count($productNotExist),
            'data' => $productNotExist
        ], 200);
    }

     /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @return array
     * @param string $filename
     * @param string $delimiter
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',  $row);
                    $row = preg_replace('!\s+!', '',  $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 1) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

}
