<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateMenuSubscriberAPIRequest;
use App\Http\Requests\ApiRequests\UpdateMenuSubscriberAPIRequest;
use App\Entities\MenuSubscriber;
use App\Repositories\MenuSubscriberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MenuSubscriberController
 * @package App\Http\Controllers\ApiControllers
 */

class MenuSubscriberAPIController extends AppBaseController
{
    /** @var  MenuSubscriberRepository */
    private $menuSubscriberRepository;

    public function __construct(MenuSubscriberRepository $menuSubscriberRepo)
    {
        $this->menuSubscriberRepository = $menuSubscriberRepo;
    }

    /**
     * Display a listing of the MenuSubscriber.
     * GET|HEAD /menuSubscribers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $menuSubscribers = $this->menuSubscriberRepository->findAllForSubscribersAndModule($request->subscriber_id);

        return $this->sendResponse($menuSubscribers->toArray(), 'Menu Subscribers retrieved successfully');
    }

    /**
     * Store a newly created MenuSubscriber in storage.
     * POST /menuSubscribers
     *
     * @param CreateMenuSubscriberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuSubscriberAPIRequest $request)
    {

        return $request;
        $input = $request->all();

        $menuSubscribers = $this->menuSubscriberRepository->create($input);

        return $this->sendResponse($menuSubscribers->toArray(), 'Menu Subscriber saved successfully');
    }

    /**
     * Display the specified MenuSubscriber.
     * GET|HEAD /menuSubscribers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MenuSubscriber $menuSubscriber */
        $menuSubscriber = $this->menuSubscriberRepository->findWithoutFail($id);

        if (empty($menuSubscriber)) {
            return $this->sendError('Menu Subscriber not found');
        }

        return $this->sendResponse($menuSubscriber->toArray(), 'Menu Subscriber retrieved successfully');
    }

    /**
     * Update the specified MenuSubscriber in storage.
     * PUT/PATCH /menuSubscribers/{id}
     *
     * @param  int $id
     * @param UpdateMenuSubscriberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuSubscriberAPIRequest $request)
    {

        $input = $request->all();

        $menuSubscriber = $this->menuSubscriberRepository->updateAll($input, $id);

        $menuUpdated = $this->menuSubscriberRepository->findAllForSubscribersAndModule($id);

        return $this->sendResponse($menuUpdated->toArray(), 'MenuSubscriber updated successfully');
    }

    /**
     * Remove the specified MenuSubscriber from storage.
     * DELETE /menuSubscribers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MenuSubscriber $menuSubscriber */
        $menuSubscriber = $this->menuSubscriberRepository->findWithoutFail($id);

        if (empty($menuSubscriber)) {
            return $this->sendError('Menu Subscriber not found');
        }

        $menuSubscriber->delete();

        return $this->sendResponse($id, 'Menu Subscriber deleted successfully');
    }
}
