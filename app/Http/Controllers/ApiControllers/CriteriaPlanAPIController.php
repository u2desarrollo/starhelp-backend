<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateCriteriaPlanAPIRequest;
use App\Http\Requests\ApiRequests\UpdateCriteriaPlanAPIRequest;
use App\Entities\CriteriaPlan;
use App\Repositories\CriteriaPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CriteriaPlanController
 * @package App\Http\Controllers\ApiControllers
 */

class CriteriaPlanAPIController extends AppBaseController
{
    /** @var  CriteriaPlanRepository */
    private $criteriaPlanRepository;

    public function __construct(CriteriaPlanRepository $criteriaPlanRepo)
    {
        $this->criteriaPlanRepository = $criteriaPlanRepo;
    }

    /**
     * Display a listing of the CriteriaPlan.
     * GET|HEAD /criteriaPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->criteriaPlanRepository->pushCriteria(new RequestCriteria($request));
        $this->criteriaPlanRepository->pushCriteria(new LimitOffsetCriteria($request));
        $criteriaPlans = $this->criteriaPlanRepository->all();

        return $this->sendResponse($criteriaPlans->toArray(), 'Criteria Plans retrieved successfully');
    }

    /**
     * Store a newly created CriteriaPlan in storage.
     * POST /criteriaPlans
     *
     * @param CreateCriteriaPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCriteriaPlanAPIRequest $request)
    {
        $input = $request->all();

        $criteriaPlans = $this->criteriaPlanRepository->create($input);

        return $this->sendResponse($criteriaPlans->toArray(), 'Criteria Plan saved successfully');
    }

    /**
     * Display the specified CriteriaPlan.
     * GET|HEAD /criteriaPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CriteriaPlan $criteriaPlan */
        $criteriaPlan = $this->criteriaPlanRepository->findWithoutFail($id);

        if (empty($criteriaPlan)) {
            return $this->sendError('Criteria Plan not found');
        }

        return $this->sendResponse($criteriaPlan->toArray(), 'Criteria Plan retrieved successfully');
    }

    /**
     * Update the specified CriteriaPlan in storage.
     * PUT/PATCH /criteriaPlans/{id}
     *
     * @param  int $id
     * @param UpdateCriteriaPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCriteriaPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var CriteriaPlan $criteriaPlan */
        $criteriaPlan = $this->criteriaPlanRepository->findWithoutFail($id);

        if (empty($criteriaPlan)) {
            return $this->sendError('Criteria Plan not found');
        }

        $criteriaPlan = $this->criteriaPlanRepository->update($input, $id);

        return $this->sendResponse($criteriaPlan->toArray(), 'CriteriaPlan updated successfully');
    }

    /**
     * Remove the specified CriteriaPlan from storage.
     * DELETE /criteriaPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CriteriaPlan $criteriaPlan */
        $criteriaPlan = $this->criteriaPlanRepository->findWithoutFail($id);

        if (empty($criteriaPlan)) {
            return $this->sendError('Criteria Plan not found');
        }

        $criteriaPlan->delete();

        return $this->sendResponse($id, 'Criteria Plan deleted successfully');
    }
}
