<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePriceContactAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePriceContactAPIRequest;
use App\Entities\PriceContact;
use App\Repositories\PriceContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PriceContactController
 * @package App\Http\Controllers\ApiControllers
 */

class PriceContactAPIController extends AppBaseController
{
    /** @var  PriceContactRepository */
    private $priceContactRepository;

    public function __construct(PriceContactRepository $priceContactRepo)
    {
        $this->priceContactRepository = $priceContactRepo;
    }

    /**
     * Display a listing of the PriceContact.
     * GET|HEAD /priceContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->priceContactRepository->pushCriteria(new RequestCriteria($request));
        $this->priceContactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $priceContacts = $this->priceContactRepository->all();

        return $this->sendResponse($priceContacts->toArray(), 'Price Contacts retrieved successfully');
    }

    /**
     * Store a newly created PriceContact in storage.
     * POST /priceContacts
     *
     * @param CreatePriceContactAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePriceContactAPIRequest $request)
    {
        $input = $request->all();

        $priceContacts = $this->priceContactRepository->create($input);

        return $this->sendResponse($priceContacts->toArray(), 'Price Contact saved successfully');
    }

    /**
     * Display the specified PriceContact.
     * GET|HEAD /priceContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PriceContact $priceContact */
        $priceContact = $this->priceContactRepository->findWithoutFail($id);

        if (empty($priceContact)) {
            return $this->sendError('Price Contact not found');
        }

        return $this->sendResponse($priceContact->toArray(), 'Price Contact retrieved successfully');
    }

    /**
     * Update the specified PriceContact in storage.
     * PUT/PATCH /priceContacts/{id}
     *
     * @param  int $id
     * @param UpdatePriceContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriceContactAPIRequest $request)
    {
        $input = $request->all();

        /** @var PriceContact $priceContact */
        $priceContact = $this->priceContactRepository->findWithoutFail($id);

        if (empty($priceContact)) {
            return $this->sendError('Price Contact not found');
        }

        $priceContact = $this->priceContactRepository->update($input, $id);

        return $this->sendResponse($priceContact->toArray(), 'PriceContact updated successfully');
    }

    /**
     * Remove the specified PriceContact from storage.
     * DELETE /priceContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PriceContact $priceContact */
        $priceContact = $this->priceContactRepository->findWithoutFail($id);

        if (empty($priceContact)) {
            return $this->sendError('Price Contact not found');
        }

        $priceContact->delete();

        return $this->sendResponse($id, 'Price Contact deleted successfully');
    }
    public function getPriceContact(Request $request){
        $priceContact = $this->priceContactRepository->getPriceCOntact($request);
        if (empty($priceContact)) {
            return $this->sendError('Price Contact not found');
        }
        return $this->sendResponse($priceContact->toArray(), 'Price Contact retrieved successfully');
    }
}
