<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateRelatedProductAPIRequest;
use App\Http\Requests\ApiRequests\UpdateRelatedProductAPIRequest;
use App\Entities\RelatedProduct;
use App\Repositories\RelatedProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RelatedProductController
 * @package App\Http\Controllers\ApiControllers
 */

class RelatedProductAPIController extends AppBaseController
{
    /** @var  RelatedProductRepository */
    private $relatedProductRepository;

    public function __construct(RelatedProductRepository $relatedProductRepo)
    {
        $this->relatedProductRepository = $relatedProductRepo;
    }

    /**
     * Display a listing of the RelatedProduct.
     * GET|HEAD /relatedProducts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->relatedProductRepository->pushCriteria(new RequestCriteria($request));
        $this->relatedProductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $relatedProducts = $this->relatedProductRepository->all();

        return $this->sendResponse($relatedProducts->toArray(), 'Related Products retrieved successfully');
    }

    /**
     * Store a newly created RelatedProduct in storage.
     * POST /relatedProducts
     *
     * @param CreateRelatedProductAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRelatedProductAPIRequest $request)
    {
        $input = $request->all();

        $relatedProducts = $this->relatedProductRepository->create($input);

        return $this->sendResponse($relatedProducts->toArray(), 'Related Product saved successfully');
    }

    /**
     * Display the specified RelatedProduct.
     * GET|HEAD /relatedProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RelatedProduct $relatedProduct */
        $relatedProduct = $this->relatedProductRepository->findWithoutFail($id);

        if (empty($relatedProduct)) {
            return $this->sendError('Related Product not found');
        }

        return $this->sendResponse($relatedProduct->toArray(), 'Related Product retrieved successfully');
    }

    /**
     * Update the specified RelatedProduct in storage.
     * PUT/PATCH /relatedProducts/{id}
     *
     * @param  int $id
     * @param UpdateRelatedProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRelatedProductAPIRequest $request)
    {
        $input = $request->all();

        /** @var RelatedProduct $relatedProduct */
        $relatedProduct = $this->relatedProductRepository->findWithoutFail($id);

        if (empty($relatedProduct)) {
            return $this->sendError('Related Product not found');
        }

        $relatedProduct = $this->relatedProductRepository->update($input, $id);

        return $this->sendResponse($relatedProduct->toArray(), 'RelatedProduct updated successfully');
    }

    /**
     * Remove the specified RelatedProduct from storage.
     * DELETE /relatedProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RelatedProduct $relatedProduct */
        $relatedProduct = $this->relatedProductRepository->findWithoutFail($id);

        if (empty($relatedProduct)) {
            return $this->sendError('Related Product not found');
        }

        $relatedProduct->delete();

        return $this->sendResponse($id, 'Related Product deleted successfully');
    }
}
