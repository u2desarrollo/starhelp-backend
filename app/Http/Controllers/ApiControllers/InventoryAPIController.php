<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\ContactWarehouse;
use App\Entities\Inventory;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\BranchOffice;
use App\Entities\Product;
use App\Http\Requests\ApiRequests\AvailabilityProductsRequest;
use App\Http\Requests\ApiRequests\CreateInventoryAPIRequest;
use App\Http\Requests\ApiRequests\UpdateInventoryAPIRequest;
use App\Repositories\InventoryRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\In;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;


use App\Services\InventoryService;

/**
 * Class InventoryController
 *
 * @package App\Http\Controllers\ApiControllers
 */
class InventoryAPIController extends AppBaseController
{
    /** @var  InventoryRepository */
    private $inventoryRepository;
    private $inventoryService;

    public function __construct(InventoryRepository $inventoryRepo, InventoryService $inventoryService)
    {
        $this->inventoryRepository = $inventoryRepo;
        $this->inventoryService = $inventoryService;
    }

    /**
     * Display a listing of the Inventory.
     * GET|HEAD /inventories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inventoryRepository->pushCriteria(new RequestCriteria($request));
        $this->inventoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inventories = $this->inventoryRepository->findAll($request);

        return $this->sendResponse($inventories->toArray(), 'Inventories retrieved successfully');
    }

    /**
     * Store a newly created Inventory in storage.
     * POST /inventories
     *
     * @param CreateInventoryAPIRequest $request
     * @return Response
     */
    public function store(CreateInventoryAPIRequest $request)
    {
        $input = $request->all();

        $inventories = $this->inventoryRepository->create($input);

        return $this->sendResponse($inventories->toArray(), 'Inventory saved successfully');
    }

    /**
     * Display the specified Inventory.
     * GET|HEAD /inventories/{id}
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var Inventory $inventory */
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            return $this->sendError('Inventory not found');
        }

        return $this->sendResponse($inventory->toArray(), 'Inventory retrieved successfully');
    }

    /**
     * Update the specified Inventory in storage.
     * PUT/PATCH /inventories/{id}
     *
     * @param int $id
     * @param UpdateInventoryAPIRequest $request
     * @return Response
     */
    public function update($id, UpdateInventoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Inventory $inventory */
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            return $this->sendError('Inventory not found');
        }

        $inventory = $this->inventoryRepository->update($input, $id);

        return $this->sendResponse($inventory->toArray(), 'Inventory updated successfully');
    }

    /**
     * Remove the specified Inventory from storage.
     * DELETE /inventories/{id}
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Inventory $inventory */
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            return $this->sendError('Inventory not found');
        }

        $inventory->delete();

        return $this->sendResponse($id, 'Inventory deleted successfully');
    }


    /**
     * Metodo que maneja las peticiones http para consultar la existencia de un producto en una bodega especifica
     *
     * @param Request $request
     * @return mixed
     */
    public function getExistenceForProductAndBranchofficeWarehouse(Request $request)
    {

        /** @var Inventory $inventory */
        $inventory = $this->inventoryRepository->getExistenceForProductAndBranchofficeWarehouse($request->product_id, $request->branchoffice_warehouse_id);

        if (empty($inventory)) {
            return $this->sendError('Inventory not found');
        }

        return $this->sendResponse($inventory, 'Inventory retrieved successfully');

    }


    /**
     * consulta productos por sede
     *
     * @author santiago torres
     */
    public function gProducts(Request $request)
    {
        $products = $this->inventoryRepository->getProducts($request->branchoffice_warehouse_id);

        return $this->sendResponse($products, 'Product retrieved successfully');
    }

    public function fin($documentProduct)
    {
        $Finalize = $this->inventoryRepository->finalize($documentProduct);
        return $Finalize;
    }

    /**
     *
     */
    public function available(Request $request)
    {
        $respnse = $this->inventoryService->calculateAvailability($request);
        if ($respnse) {
            return 'ok';
        } else {
            return 'NO';
        }
    }

    public function updatelocation(Request $request)
    {
        return $this->inventoryRepository->updateLocation($request);
    }

    /**
     * @author santiago Torres
     * disponibilidad
     */
    public function availability(Request $request)
    {
        return $this->inventoryRepository->availability($request);
    }

    /*
    * Este metodo se encarga de cargar las ubicaciones
    * de forma masiva por medio de un archivo CSV
    *
    * @author Kevin Galindo
    */
    public function loadLocationCsv(Request $request)
    {
        //Recibimos la información
        $file = $request->file('archivo_importar');
        $fileData = $this->csvToArray($file->getRealPath());
        $countLoad = 0;

        // Recorremos el arreglo y creamos los registros
        foreach ($fileData as $key => $value) {

            $dranchOffice = BranchOffice::where('name', 'ilike', '%' . $value['SEDE'] . '%')->first();
            if ($dranchOffice) {
                $branchOfficeWarehouse = BranchOfficeWarehouse::where('warehouse_code', '01')->where('branchoffice_id', $dranchOffice->id)->first();

                if ($branchOfficeWarehouse) {
                    // Buscamos el producto
                    if (empty($value['REFERENCIA'])) {
                        $product = Product::where('description', trim($value['DESCRIPCION']))->first();
                    } else {
                        $product = Product::where('code', trim($value['REFERENCIA']))->first();
                    }
                    // Validamos si exite producto.
                    if ($product) {

                        $inventory = Inventory::where('product_id', $product->id)
                            ->where('branchoffice_warehouse_id', $branchOfficeWarehouse->id)
                            ->first();

                        if ($inventory) {
                            $inventory->location = $value['UBICACION'];
                            $inventory->save();
                            $countLoad++;
                        } else {
                            Inventory::create([
                                'branchoffice_warehouse_id' => $branchOfficeWarehouse->id,
                                'product_id'                => $product->id,
                                'stock'                     => 0,
                                'stock_values'              => 0,
                                'average_cost'              => 0,
                                'available'                 => 0,
                                'location'                  => $value['UBICACION']
                            ]);
                            $countLoad++;
                        }

                    }
                }
            }

        }

        return response()->json('Registros creatos, cantidad: ' . $countLoad, 200);
    }

    /**
     * Esta funcion se encarga de pasar un archivo csv a un array
     * segun el tipo de limitador.
     *
     * @param string $filename
     * @param string $delimiter
     * @return array
     * @author Kevin Galindo
     */
    function csvToArray($filename = '', $delimiter = ';')
    {
        //Valida si el archivo existe y si se puede abrir.
        if (!file_exists($filename) || !is_readable($filename)) return false;

        //Creamos las variables de la cabecera y la data que tendra el array.
        $header = [];
        $data = [];

        //Recorremos el archivo y acomodamos la data.
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    // Elimina los caracteres no válidos u ocultos
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        if (count($data[1]) <= 1) {
            return false;
        }

        //Retornamos la informacion.
        return $data;
    }

    /**
     * Este metodo se encarga de retornal
     * las sucursales disponibles de un producto
     * con la disponibilidad en ellos
     *
     * @author Kevin Galindo
     */
    public function branchOfficesWarehousesAvailableByProduct($product_id, Request $request)
    {
        $branchOffices = BranchOffice::all();
        $productInventory = Inventory::where('product_id', $product_id)->get();
        $branchOfficesWithAvailable = [];

        foreach ($branchOffices as $key => $branchOffice) {
            $available = 0;
            $warehousesAvailable = $branchOffice->warehouses->where('warehouse_code', '01')->first();

            if (!isset($warehousesAvailable->id)) {
                continue;
            }

            $productInventory = Inventory::where('product_id', $product_id)
                ->where('branchoffice_warehouse_id', $warehousesAvailable->id)
                ->first();

            if ($productInventory) {
                $available = $productInventory->available;
            }

            $branchOfficesWithAvailable[] = [
                'branchoffice'              => $branchOffice,
                'branchoffice_warehouse_id' => $warehousesAvailable,
                'available'                 => $available,
                'inventory'                 => $productInventory
            ];


        }
        $product = Product::find($product_id);

        return response()->json([
            'product'                => $product,
            'branchOfficesAvailable' => $branchOfficesWithAvailable
        ], 200);
    }

    /**
     * consulta para obtener disponibilidad en las sedes
     *
     * @author santiago torres
     */

    public function branchOfficesAvailability($product_id)
    {
        return $this->inventoryRepository->branchOfficesAvailability($product_id);
    }

    public function generateReport(Request $request)
    {

        $path_report = $this->inventoryService->generateReport($request);

        return Storage::download('/public/reports/' . $path_report);


    }

    public function searchdupli()
    {
        return $this->inventoryRepository->searchdupli();
    }

    public function getReportRepositioningAssistant()
    {
        $fileName = $this->inventoryService->generateReportRepositioningAssistant();

        return Storage::download('/public/reports/Sugerido de reposicion.xlsx');

    }

    /**
     * Obtener la disponibilidad de productos en una bodega especifica
     *
     * @param AvailabilityProductsRequest $request Contiene el id de la sucursal del cliente y los ids de los productos a consultar
     * @return JsonResponse Contiene un array con la disponbilidad de los productos
     * @author Jhon García
     */
    public function getAvailabilityAndPricesForProducts(AvailabilityProductsRequest $request)
    {
        // Obtener la disponibilidad de los productos en la bodega asociada al cliente
        $availabilities = $this->inventoryService->getAvailabilityAndPricesForProducts($request);

        return response()->json($availabilities, 200);
    }

}
