<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use Illuminate\Http\Request;
use App\Traits\LoadImageTrait;
use App\Entities\Configuration;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ConfigurationRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Http\Requests\ApiRequests\CreateConfigurationAPIRequest;
use App\Http\Requests\ApiRequests\UpdateConfigurationAPIRequest;

/**
 * Class ConfigurationController
 * @package App\Http\Controllers\ApiControllers
 */

class ConfigurationAPIController extends AppBaseController
{
    use LoadImageTrait;

    /** @var  ConfigurationRepository */
    private $configurationRepository;

    public function __construct(ConfigurationRepository $configurationRepo)
    {
        $this->configurationRepository = $configurationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->configurationRepository->pushCriteria(new RequestCriteria($request));
        $this->configurationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $configurations = $this->configurationRepository->all();

        return $this->sendResponse($configurations->toArray(), 'Configurations retrieved successfully');
    }

    /**
     * @param CreateConfigurationAPIRequest $request
     * @return Response
     */
    public function store(CreateConfigurationAPIRequest $request)
    {
        $input = $request->all();

        $configurations = $this->configurationRepository->create($input);

        return $this->sendResponse($configurations->toArray(), 'Configuration saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     */
    public function show($id)
    {
        /** @var Configuration $configuration */
        $configuration = $this->configurationRepository->findByField('suscriber_id', $id)->first();

        if (empty($configuration)) {
            return $this->sendError('Configuration not found');
        }

        return $this->sendResponse($configuration->toArray(), 'Configuration retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateConfigurationAPIRequest $request
     * @return Response
     */
    public function update($id, UpdateConfigurationAPIRequest $request)
    {
        // dd($request->all());

        $input = $request->except('first_logo');
        $input['first_logo'] =  $this->saveImage($request->first_logo, 'subscriberLogo');

       // $input['first_logo'] = $fileName;
       // dd($input);

       // $input = $request->all();

        /** @var Configuration $configuration */
        $configuration = $this->configurationRepository->findWithoutFail($id);

        if (empty($configuration)) {
            return $this->sendError('Configuration not found');
        }

        $configuration = $this->configurationRepository->update($input, $id);

        return $this->sendResponse($configuration->toArray(), 'Configuration updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var Configuration $configuration */
        $configuration = $this->configurationRepository->findWithoutFail($id);

        if (empty($configuration)) {
            return $this->sendError('Configuration not found');
        }

        $configuration->delete();

        return $this->sendResponse($id, 'Configuration deleted successfully');
    }
}
