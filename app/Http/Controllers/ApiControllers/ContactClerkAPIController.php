<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactClerkAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactClerkAPIRequest;
use App\Entities\ContactClerk;
use App\Repositories\ContactClerkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactClerkController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactClerkAPIController extends AppBaseController
{
    /** @var  ContactClerkRepository */
    private $contactClerkRepository;

    public function __construct(ContactClerkRepository $contactClerkRepo)
    {
        $this->contactClerkRepository = $contactClerkRepo;
    }

    /**
     * Display a listing of the ContactClerk.
     * GET|HEAD /contactClerks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contactClerkRepository->pushCriteria(new RequestCriteria($request));
        $this->contactClerkRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactClerks = $this->contactClerkRepository->with('city')->findWhere(['contact_id' => $request->contact_id]);

        return $this->sendResponse($contactClerks->toArray(), 'Contact Clerks retrieved successfully');
    }

    /**
     * Store a newly created ContactClerk in storage.
     * POST /contactClerks
     *
     * @param CreateContactClerkAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateContactClerkAPIRequest $request)
    {
        $input = $request->except('city');

        $contactClerks = $this->contactClerkRepository->create($input);

        return $this->sendResponse($contactClerks->toArray(), 'Contact Clerk saved successfully');
    }

    /**
     * Display the specified ContactClerk.
     * GET|HEAD /contactClerks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ContactClerk $contactClerk */
        $contactClerk = $this->contactClerkRepository->findWithoutFail($id);

        if (empty($contactClerk)) {
            return $this->sendError('Contact Clerk not found');
        }

        return $this->sendResponse($contactClerk->toArray(), 'Contact Clerk retrieved successfully');
    }

    /**
     * Update the specified ContactClerk in storage.
     * PUT/PATCH /contactClerks/{id}
     *
     * @param  int $id
     * @param UpdateContactClerkAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactClerkAPIRequest $request)
    {
        $input = $request->except('city');

        /** @var ContactClerk $contactClerk */
        $contactClerk = $this->contactClerkRepository->findWithoutFail($id);

        if (empty($contactClerk)) {
            return $this->sendError('Contact Clerk not found');
        }

        $contactClerk = $this->contactClerkRepository->update($input, $id);

        return $this->sendResponse($contactClerk->toArray(), 'ContactClerk updated successfully');
    }

    /**
     * Remove the specified ContactClerk from storage.
     * DELETE /contactClerks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ContactClerk $contactClerk */
        $contactClerk = $this->contactClerkRepository->findWithoutFail($id);

        if (empty($contactClerk)) {
            return $this->sendError('Contact Clerk not found');
        }

        $contactClerk->delete();

        return $this->sendResponse($id, 'Contact Clerk deleted successfully');
    }
}
