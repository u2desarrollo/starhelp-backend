<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use Illuminate\Http\Request;
use App\Entities\BranchOffice;
use App\Entities\BranchOfficeWarehouse;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BranchOfficeRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Repositories\BranchOfficeWarehouseRepository;
use App\Http\Requests\ApiRequests\CreateBranchOfficeAPIRequest;
use App\Http\Requests\ApiRequests\UpdateBranchOfficeAPIRequest;

/**
 * Class BranchOfficeController
 * @package App\Http\Controllers\ApiControllers
 */

class BranchOfficeAPIController extends AppBaseController
{
    /** @var  BranchOfficeRepository */
    private $branchOfficeRepository;
    private $branchOfficeWarehouseRepository;

    public function __construct(BranchOfficeRepository $branchOfficeRepo, BranchOfficeWarehouseRepository $branchOfficeWarehouseRepo)
    {
        // $this->middleware(['role_or_permission:super-admin|super admin']);
        $this->branchOfficeRepository = $branchOfficeRepo;
        $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     */
    public function index(Request $request)
    {
        $this->branchOfficeRepository->pushCriteria(new RequestCriteria($request));
        $this->branchOfficeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $branchOffices = $this->branchOfficeRepository->findAll($request);
        // $branchOffices = $this->branchOfficeRepository->all();

        return $this->sendResponse($branchOffices->toArray(), 'Branch Offices retrieved successfully');
    }

    /**
     * @param CreateBranchOfficeAPIRequest $request
     * @return Response
     *
     *
     */
    public function store(CreateBranchOfficeAPIRequest $request)
    {
        $input = $request->except('warehouses', 'city');

        // Validamos la sede principal
        if ($request->main == true) {
            $res =  BranchOffice::withTrashed()->where('main', '1')->where('city_id', $request->city_id)->update(['main' => false]);
        }

        $branchOffice = $this->branchOfficeRepository->create($input);

        if($input['branchoffice_type'] == 1){
            $warehouses = $request->warehouses;
            foreach ($warehouses as $warehouse) {
                if ($warehouse['action'] == 'c') {
                    $wh = new BranchOfficeWarehouse($warehouse);
                    $branchOffice->warehouses()->save($wh);
                }
            }
        }
        return $this->sendResponse($branchOffice->toArray(), 'Branch Office saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function show($id)
    {
        /** @var BranchOffice $branchOffice */
        $branchOffice = $this->branchOfficeRepository->findForId($id);

        if (empty($branchOffice)) {
            return $this->sendError('Branch Office not found');
        }

        return $this->sendResponse($branchOffice->toArray(), 'Branch Office retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBranchOfficeAPIRequest $request
     * @return Response
     *
     *
     */
    public function update($id, UpdateBranchOfficeAPIRequest $request)
    {
        $input = $request->except('warehouses', 'city');

        // Validamos la sede principal
        if ($request->main == true) {
            $res =  BranchOffice::withTrashed()->where('main', '1')->where('city_id', $request->city_id)->update(['main' => false]);
        }

        /** @var BranchOffice $branchOffice */
        $branchOffice = $this->branchOfficeRepository->findForId($id);

        if (empty($branchOffice)) {
            return $this->sendError('Branch Office not found');
        }
        $branchOffice = $this->branchOfficeRepository->update($input, $id);

        // Actualizar Bodega

        $warehouses = $request->warehouses;
        foreach ($warehouses as $warehouse) {
            $bwh = new BranchOfficeWarehouse($warehouse);
            if(isset($warehouse['action'])){
                if($warehouse['action'] == 'c'){
                    $branchOffice->warehouses()->save($bwh);
                } elseif ($warehouse['action'] == 'u') {
                    $warehouse['branchOffice_id'] = $branchOffice->id;
                    BranchOfficeWarehouse::find($warehouse['id'])->fill($warehouse)->save();
                } elseif ($warehouse['action'] == 'd') {
                    BranchOfficeWarehouse::find($warehouse['id'])->delete();
                }
            }
        }
        $branchoff = $this->branchOfficeRepository->findForId($id);
        return $this->sendResponse($branchoff->toArray(), 'BranchOffice updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var BranchOffice $branchOffice */
        $branchOffice = $this->branchOfficeRepository->findWithoutFail($id);

        if (empty($branchOffice)) {
            return $this->sendError('Branch Office not found');
        }

        $branchOffice->delete();

        return $this->sendResponse($id, 'Branch Office deleted successfully');
    }
    public function getBranchs(){
        $branchOffice= $this->branchOfficeRepository->getBranchs();
        if (empty($branchOffice)) {
            return $this->sendError('Branch Office not found');
        }
        return $this->sendResponse($branchOffice->toArray(),'BranchOffice successfully');
    }
}
