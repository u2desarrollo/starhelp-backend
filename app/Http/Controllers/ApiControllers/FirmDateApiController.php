<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\FirmDateRepository;
use App\Services\FirmDateService;

class FirmDateApiController extends Controller
{

    private $firmDateRepository;
    private $firmDateService;

    public function __construct(FirmDateRepository $firmDateRepository, FirmDateService $firmDateService)
    {
        $this->firmDateRepository = $firmDateRepository;
        $this->firmDateService = $firmDateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = $this->firmDateRepository->findAll($request);
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->firmDateRepository->create($request->except(['account']));
        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->firmDateRepository->findById($id);
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->firmDateRepository->update($request->all(), $id);
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fix = $this->firmDateRepository->findById($id);
        $fix->delete();
        return response()->json($fix, 200);
    }
}
