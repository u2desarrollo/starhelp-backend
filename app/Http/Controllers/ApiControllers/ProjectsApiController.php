<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ProjectsRepository;
use App\Services\ProjectsService;


class ProjectsApiController extends AppBaseController
{
    private $projectsService;
    private $projectsRepository;
    //
    public function __construct(ProjectsRepository $projectsRepository, ProjectsService $projectsService)
    {
        $this->projectsRepository = $projectsRepository;
        $this->projectsService = $projectsService;

    }

    /**
     * metodo para listar negocios Activos
     */
    public function activeProjects(Request $request)
    {
        $projects = $this->projectsRepository->activeProjects($request);
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }
    
    public function index(Request $request)
    {
        $projects = $this->projectsRepository->findAll($request);
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }

    public function store(Request $request)
    {
        $projects = $this->projectsRepository->create($request->all());
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }

    public function show($id)
    {
        $projects = $this->projectsRepository->findById($id);
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }

    public function update(Request $request, $id)
    {
        $projects = $this->projectsRepository->update($id, $request->all());
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }

    public function destroy($id)
    {
        $projects = $this->projectsRepository->delete($id);
        return $this->sendResponse($projects, 'projects retrieved successfully');
    }
}
