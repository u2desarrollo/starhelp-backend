<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\AccountRepository;
use App\Services\AccountService;
use Response;

class AccountApiController extends AppBaseController
{
    private $accountRepository;
    private $accountService;
    //
    public function __construct(AccountRepository $accountRepository, AccountService $accountService)
    {
        $this->accountRepository = $accountRepository;
        $this->accountService = $accountService;
    }

    /**
     * lista de cuentas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accounts = $this->accountRepository->findAll($request);
        return $this->sendResponse($accounts->toArray(), 'Account retrieved successfully');
    }

    public function store(Request $request)
    {
        $response = $this->accountService->createAccount($request);
        return response()->json($response->data, $response->code);
    }

    public function show($id)
    {
        $response = $this->accountRepository->findAccountById($id);
        return response()->json($response->data, $response->code);
    }

    public function update($id, Request $request)
    {
        $response = $this->accountService->updateAccount($id, $request);
        return response()->json($response->data, $response->code);
    }

    public function destroy($id)
    {
        $response = $this->accountService->deleteAccount($id);
        return response()->json($response->data, $response->code);
    }

    public function accountsFromRoot($id)
    {
        $accounts = $this->accountRepository->accountsFromRoot($id);
        return $this->sendResponse($accounts->toArray(), 'Account retrieved successfully');
    }

    public function accountsDefaultCrossBalances()
    {
        $accounts = $this->accountRepository->accountsDefaultCrossBalances();
        return $this->sendResponse($accounts->toArray(), 'Account retrieved successfully');
    }
}
