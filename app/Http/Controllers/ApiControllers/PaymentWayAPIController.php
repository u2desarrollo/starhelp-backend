<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePaymentWayAPIRequest;
use App\Entities\PaymentWay;
use App\Repositories\PaymentWayRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PaymentWayAPIController
 * @package App\Http\Controllers\ApiControllers
 */

class PaymentWayAPIController extends AppBaseController
{
	/** @var  PaymentWayRepository */
	private $PaymentWayRepository;

	public function __construct(PaymentWayRepository $PaymentWayRepo)
	{
		$this->PaymentWayRepository = $PaymentWayRepo;
	}

	public function store(CreatePaymentWayAPIRequest $request)
	{
        $input = [];
        $input['iduser']=$request->iduser;
       $request= json_decode($request->formasPago);
       $input['formasPago']=$request;


		try {
			$PaymentWays = $this->PaymentWayRepository->create($input);
			return response()->json($PaymentWays);
		} catch (\Exception $e) {
			return response()->json(['error' => ' Se presentó un error al procesar la solicitud.' . $e], 500);
		}
	}
}
