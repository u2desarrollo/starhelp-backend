<?php

namespace App\Http\Controllers\ApiControllers;


use App\Entities\ContactWarehouse;
use App\Entities\Template;
use App\Entities\VouchersType;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderFromAppRequest;
use App\Repositories\DocumentRepository;
use App\Services\DocumentsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class DocumentAppAPIController extends Controller
{

    private $documentService;
    private $documentRepository;

    /**
     * DocumentAppAPIController constructor.
     * @param DocumentsService $documentsService
     */
    public function __construct(DocumentsService $documentsService, DocumentRepository $documentRepository)
    {
        $this->documentService = $documentsService;
        $this->documentRepository = $documentRepository;
    }


    /**
     * Controla las peticiones para crear un pedido del el app
     *
     * @param CreateOrderFromAppRequest $request Contiene la información del pedido
     * @return JsonResponse Contiene el id del pedido creado
     * @author Jhon García
     */
    public function finalizeOrderApp(CreateOrderFromAppRequest $request)
    {
        Log::info('Finalizando orden de venta desde el App');

        try {
            $contact_warehouse = ContactWarehouse::find($request->warehouse_id);
            $model = Template::where ('code', '010')->first();
            $voucher_type = VouchersType::where ('code_voucher_type', '010')
                ->where('branchoffice_id', $contact_warehouse->branchoffice_id)
                ->first();

            // Obtener el usuario que está haciendo la petición
            $user = JWTAuth::parseToken()->toUser();

            // Establecer los valores por defecto
            $defaultValues = [
                'user_id' => $user->id,
                'seller_id' => $user->contact_id,
                'vouchertype_id' => $voucher_type->id,
                'model_id' => $model->id
            ];

            // Combinar el request con los valores por defecto
            $request = $request->merge($defaultValues);

            // Crear y obtener el documento del carrito
            $documentCart = $this->documentService->finalizeOrderApp($request->all());

            $request = $request->merge(['id' => $documentCart->id]);

            $order = $this->documentService->completeCart($request);

            if (isset($order['data']) && isset($order['data']['id'])){

                $document = $this->documentRepository->getDocument($order['data']['id']);

                Log::info('Orden de venta finalizada, con consecutivo: ' . $document->consecutive);

                return response()->json([
                    'message' => 'Orden de venta finalizada exitosamente',
                    'id' => $document->consecutive
                ], 200);
            }else{
                throw new Exception('Se presentó un error al procesar la petición', 500);
            }

        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json($exception->getMessage(), $exception->getCode());
        }

    }
}
