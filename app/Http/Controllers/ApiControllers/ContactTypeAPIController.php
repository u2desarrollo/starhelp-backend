<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactTypeAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactTypeAPIRequest;
use App\Entities\ContactType;
use App\Repositories\ContactTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactTypeController
 * @package App\Http\Controllers\ApiControllers
 */

class ContactTypeAPIController extends AppBaseController
{
    /** @var  ContactTypeRepository */
    private $contactTypeRepository;

    public function __construct(ContactTypeRepository $contactTypeRepo)
    {
        $this->contactTypeRepository = $contactTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/contactTypes",
     *      summary="Get a listing of the ContactTypes.",
     *      tags={"ContactType"},
     *      description="Get all ContactTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ContactType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->contactTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->contactTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactTypes = $this->contactTypeRepository->all();

        return $this->sendResponse($contactTypes->toArray(), 'Contact Types retrieved successfully');
    }

    /**
     * @param CreateContactTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/contactTypes",
     *      summary="Store a newly created ContactType in storage",
     *      tags={"ContactType"},
     *      description="Store ContactType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ContactType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ContactType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateContactTypeAPIRequest $request)
    {
        $input = $request->all();

        $contactTypes = $this->contactTypeRepository->create($input);

        return $this->sendResponse($contactTypes->toArray(), 'Contact Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/contactTypes/{id}",
     *      summary="Display the specified ContactType",
     *      tags={"ContactType"},
     *      description="Get ContactType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ContactType $contactType */
        $contactType = $this->contactTypeRepository->findWithoutFail($id);

        if (empty($contactType)) {
            return $this->sendError('Contact Type not found');
        }

        return $this->sendResponse($contactType->toArray(), 'Contact Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateContactTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/contactTypes/{id}",
     *      summary="Update the specified ContactType in storage",
     *      tags={"ContactType"},
     *      description="Update ContactType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ContactType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ContactType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateContactTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ContactType $contactType */
        $contactType = $this->contactTypeRepository->findWithoutFail($id);

        if (empty($contactType)) {
            return $this->sendError('Contact Type not found');
        }

        $contactType = $this->contactTypeRepository->update($input, $id);

        return $this->sendResponse($contactType->toArray(), 'ContactType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/contactTypes/{id}",
     *      summary="Remove the specified ContactType from storage",
     *      tags={"ContactType"},
     *      description="Delete ContactType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ContactType $contactType */
        $contactType = $this->contactTypeRepository->findWithoutFail($id);

        if (empty($contactType)) {
            return $this->sendError('Contact Type not found');
        }

        $contactType->delete();

        return $this->sendResponse($id, 'Contact Type deleted successfully');
    }
}
