<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePriceListPromotionAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePriceListPromotionAPIRequest;
use App\Entities\PriceListPromotion;
use App\Repositories\PriceListPromotionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PriceListPromotionController
 * @package App\Http\Controllers\ApiControllers
 */

class PriceListPromotionAPIController extends AppBaseController
{
    /** @var  PriceListPromotionRepository */
    private $priceListPromotionRepository;

    public function __construct(PriceListPromotionRepository $priceListPromotionRepo)
    {
        $this->priceListPromotionRepository = $priceListPromotionRepo;
    }

    /**
     * Display a listing of the PriceListPromotion.
     * GET|HEAD /priceListPromotions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->priceListPromotionRepository->pushCriteria(new RequestCriteria($request));
        $this->priceListPromotionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $priceListPromotions = $this->priceListPromotionRepository->all();

        return $this->sendResponse($priceListPromotions->toArray(), 'Price List Promotions retrieved successfully');
    }

    /**
     * Store a newly created PriceListPromotion in storage.
     * POST /priceListPromotions
     *
     * @param CreatePriceListPromotionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePriceListPromotionAPIRequest $request)
    {
        $input = $request->all();

        $priceListPromotions = $this->priceListPromotionRepository->create($input);

        return $this->sendResponse($priceListPromotions->toArray(), 'Price List Promotion saved successfully');
    }

    /**
     * Display the specified PriceListPromotion.
     * GET|HEAD /priceListPromotions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PriceListPromotion $priceListPromotion */
        $priceListPromotion = $this->priceListPromotionRepository->findWithoutFail($id);

        if (empty($priceListPromotion)) {
            return $this->sendError('Price List Promotion not found');
        }

        return $this->sendResponse($priceListPromotion->toArray(), 'Price List Promotion retrieved successfully');
    }

    /**
     * Update the specified PriceListPromotion in storage.
     * PUT/PATCH /priceListPromotions/{id}
     *
     * @param  int $id
     * @param UpdatePriceListPromotionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriceListPromotionAPIRequest $request)
    {
        $input = $request->all();

        /** @var PriceListPromotion $priceListPromotion */
        $priceListPromotion = $this->priceListPromotionRepository->findWithoutFail($id);

        if (empty($priceListPromotion)) {
            return $this->sendError('Price List Promotion not found');
        }

        $priceListPromotion = $this->priceListPromotionRepository->update($input, $id);

        return $this->sendResponse($priceListPromotion->toArray(), 'PriceListPromotion updated successfully');
    }

    /**
     * Remove the specified PriceListPromotion from storage.
     * DELETE /priceListPromotions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PriceListPromotion $priceListPromotion */
        $priceListPromotion = $this->priceListPromotionRepository->findWithoutFail($id);

        if (empty($priceListPromotion)) {
            return $this->sendError('Price List Promotion not found');
        }

        $priceListPromotion->delete();

        return $this->sendResponse($id, 'Price List Promotion deleted successfully');
    }
}
