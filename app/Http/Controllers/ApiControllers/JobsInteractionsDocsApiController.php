<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\JobInteractionDoc;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobsInteractionsDocsApiController extends Controller
{
    public function download($id)
    {
        $jobInteractionDoc = JobInteractionDoc::findOrFail($id);

        if (empty($jobInteractionDoc)) {
            return $this->sendError('Document not found');
        }

        $path = config('app.path_files_jobs_interactions')  . $jobInteractionDoc->path_document;


        return Storage::disk('s3')->download($path , $jobInteractionDoc->path_document, []);
    }

    public function show($id)
    {
        $jobInteractionDoc = JobInteractionDoc::findOrFail($id);

        if (empty($jobInteractionDoc)) {
            return $this->sendError('Document not found');
        }

        $array = explode('.', $jobInteractionDoc->path_document);
        $extension = array_pop($array);

        $extensionsUrl = ['pdf', 'png', 'jpg', 'jpeg', 'gif'];

        $url = Storage::disk('s3')->temporaryUrl(config('app.path_files_jobs_interactions') . $jobInteractionDoc->path_document, now()->addMinutes(5));

        if (in_array($extension, $extensionsUrl)) {
            return $url;
        }

        return urlencode($url);
    }
}
