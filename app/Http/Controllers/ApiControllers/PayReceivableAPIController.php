<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreatePayReceivableAPIRequest;
use App\Http\Requests\ApiRequests\UpdatePayReceivableAPIRequest;
use App\Entities\PayReceivable;
use App\Repositories\PayReceivableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PayReceivableController
 * @package App\Http\Controllers\ApiControllers
 */

class PayReceivableAPIController extends AppBaseController
{
    /** @var  PayReceivableRepository */
    private $payReceivableRepository;

    public function __construct(PayReceivableRepository $payReceivableRepo)
    {
        $this->payReceivableRepository = $payReceivableRepo;
    }

    /**
     * Display a listing of the PayReceivable.
     * GET|HEAD /payReceivables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->payReceivableRepository->pushCriteria(new RequestCriteria($request));
        $this->payReceivableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $payReceivables = $this->payReceivableRepository->all();

        return $this->sendResponse($payReceivables->toArray(), 'Pay Receivables retrieved successfully');
    }

    /**
     * Store a newly created PayReceivable in storage.
     * POST /payReceivables
     *
     * @param CreatePayReceivableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePayReceivableAPIRequest $request)
    {
        $input = $request->all();

        $payReceivables = $this->payReceivableRepository->create($input);

        return $this->sendResponse($payReceivables->toArray(), 'Pay Receivable saved successfully');
    }

    /**
     * Display the specified PayReceivable.
     * GET|HEAD /payReceivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PayReceivable $payReceivable */
        $payReceivable = $this->payReceivableRepository->findWithoutFail($id);

        if (empty($payReceivable)) {
            return $this->sendError('Pay Receivable not found');
        }

        return $this->sendResponse($payReceivable->toArray(), 'Pay Receivable retrieved successfully');
    }

    /**
     * Update the specified PayReceivable in storage.
     * PUT/PATCH /payReceivables/{id}
     *
     * @param  int $id
     * @param UpdatePayReceivableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePayReceivableAPIRequest $request)
    {
        $input = $request->all();

        /** @var PayReceivable $payReceivable */
        $payReceivable = $this->payReceivableRepository->findWithoutFail($id);

        if (empty($payReceivable)) {
            return $this->sendError('Pay Receivable not found');
        }

        $payReceivable = $this->payReceivableRepository->update($input, $id);

        return $this->sendResponse($payReceivable->toArray(), 'PayReceivable updated successfully');
    }

    /**
     * Remove the specified PayReceivable from storage.
     * DELETE /payReceivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PayReceivable $payReceivable */
        $payReceivable = $this->payReceivableRepository->findWithoutFail($id);

        if (empty($payReceivable)) {
            return $this->sendError('Pay Receivable not found');
        }

        $payReceivable->delete();

        return $this->sendResponse($id, 'Pay Receivable deleted successfully');
    }
    /**
     *
     */

    public function postConsignment(){

    }
}
