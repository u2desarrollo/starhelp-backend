<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateDocumentProductAPIRequest;
use App\Http\Requests\ApiRequests\UpdateDocumentProductAPIRequest;
use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\VouchersType;

use App\Services\DocumentsProductsService;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Repositories\DocumentProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Storage;
use Response;

/**
 * Class DocumentProductController
 * @package App\Http\Controllers\ApiControllers
 */

class DocumentProductAPIController extends AppBaseController
{
	/** @var  DocumentProductRepository */
	private $documentProductRepository;
	private $documentsProductsService;

	public function __construct(DocumentProductRepository $documentProductRepo, DocumentsProductsService $documentsProductsService)
	{
		$this->documentProductRepository = $documentProductRepo;
		$this->documentsProductsService = $documentsProductsService;
	}

	/**
	 * Display a listing of the DocumentProduct.
	 * GET|HEAD /documentProducts
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->documentProductRepository->pushCriteria(new RequestCriteria($request));
		$this->documentProductRepository->pushCriteria(new LimitOffsetCriteria($request));
		$documentProducts = $this->documentProductRepository->all();

		return $this->sendResponse($documentProducts->toArray(), 'Document Products retrieved successfully');
	}

	public function fixCostDevs()
	{
		return $documentProducts = $this->documentsProductsService->fixCostDevs();

	}

	public function getProductsImport($document_id, Request $request)
	{
		$documentProducts = $this->documentProductRepository->getProductsImport($document_id, $request);
		return $this->sendResponse($documentProducts, 'Document Products retrieved successfully');
	}

	/**
	 * Store a newly created DocumentProduct in storage.
	 * POST /documentProducts
	 *
	 * @param CreateDocumentProductAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDocumentProductAPIRequest $request)
	{
		// Crear
		$documentProducts = $this->documentsProductsService->createDocumentProduct($request->all());
		return $this->sendResponse($documentProducts->toArray(), 'Document Product saved successfully');
	}

	/**
	 * Display the specified DocumentProduct.
	 * GET|HEAD /documentProducts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var DocumentProduct $documentProduct */
		$documentProduct = $this->documentProductRepository->findWithoutFail($id);

		if (empty($documentProduct)) {
			return $this->sendError('Document Product not found');
		}

		return $this->sendResponse($documentProduct->toArray(), 'Document Product retrieved successfully');
	}

	/**
	 * Update the specified DocumentProduct in storage.
	 * PUT/PATCH /documentProducts/{id}
	 *
	 * @param  int $id
	 * @param UpdateDocumentProductAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateDocumentProductAPIRequest $request)
	{
		$input = $request->all();

		/** @var DocumentProduct $documentProduct */
		$documentProduct = $this->documentProductRepository->findWithoutFail($id);

		if (empty($documentProduct)) {
			return $this->sendError('Document Product not found');
		}

		$documentProduct = $this->documentProductRepository->update($input, $id);

		return $this->sendResponse($documentProduct->toArray(), 'DocumentProduct updated successfully');
	}

	/**
	 * Remove the specified DocumentProduct from storage.
	 * DELETE /documentProducts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		/** @var DocumentProduct $documentProduct */
		$documentProduct = $this->documentProductRepository->findWithoutFail($id);

		if (empty($documentProduct)) {
			return $this->sendError('Document Product not found');
		}

		$documentProduct->total_value = 0;
		$documentProduct->total_value_brut = 0;
		$documentProduct->original_quantity = 0;
		$documentProduct->quantity = 0;
		$documentProduct->save();

		if ($request->forceDelete) {
			DocumentProductTax::where('document_product_id', $documentProduct->id)->forceDelete();
			$documentProduct->forceDelete();
		} else {
			$documentProduct->delete();
		}


		return $this->sendResponse($documentProduct, 'Document Product deleted successfully');
	}

    /**
     * @param $document_id
     * @param Request $request
     * @return JsonResponse
     * @author Kevin Galindo
     */
    public function uploadFileProductsToDocument($document_id, Request $request)
    {
        $response = $this->documentsProductsService->uploadFileProductsToDocumentService($document_id, $request);

        return response()->json([
            'message' => $response['message'],
            'code'    => $response['code']
        ], $response['code']);
    }

	/**
     *
     * Retorna la traza del documento
     *
     * @author Kevin Galindo
     */
    public function traceDocumentProduct($document_product_id ,Request $request)
    {
        $res = $this->documentsProductsService->calculateBackOrder($document_product_id, $request->toArray(),true);
        return response()->json($res, 200);
    }
	/*
	 * consulta para kardex
	 * @author Santiago Torres
	 */

	public function getKardex(Request $request)
	{
		$kardex = $this->documentProductRepository->searchKardex($request);

		return response()->json([ 'data' => $kardex, 'code' => 200]);
	}


    public function createOperationsReport(Request $request)
    {
		set_time_limit(0);

		$documentProducts = $this->documentProductRepository->getInfoForOperationsReport($request);
		if ($request->detailed_by_product && $request->detailed_by_product == 1) {
			$respose = $this->documentsProductsService->createCsv($documentProducts, $request);
			return Storage::download($respose);
		}
		if ($request->detailed_by_product && $request->detailed_by_product == 2) {

			$by_document = [];
			foreach ($documentProducts as $key => $documentProduct) {
				$by_document[$documentProduct->id] []= $documentProduct;
			}

			$respose = $this->documentsProductsService->createCsvByDocument($by_document, $request);
			return Storage::download($respose);
		}

	}

	public function reporNegative()
	{
		$documents_products =  $respose = $this->documentsProductsService->reporNegative();

		$respose = $this->documentsProductsService->createCsvNegat($documents_products);

		return response()->stream($respose['file'], $respose['status'], $respose['headers']);
	}

	public function zeroReturns()
	{
		return count($this->documentsProductsService->zeroReturns());
	}

	public function fixCost(Request $request)
	{
		$dpsfix = $this->documentsProductsService->fixCost($request);
		return $dpsfix;
	}
	public function updateDocumentProductsValues(Request $request)
	{
        ini_set('memory_limit', '2048M');

		$response = $this->documentsProductsService->updateDocumentProductsValues($request);
		return response()->json([ 'data' => $response['data'], 'code' => $response['code']]);
	}

	public function updateDocumentProductWms(Request $request)
	{

		$response = $this->documentsProductsService->verifyDocumentProductWM($request);
		//dd($response);
        return response()->json([ 'infowms' => $response]);
	}

}
