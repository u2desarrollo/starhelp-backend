<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Entities\BalancesForAccount;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BalanceForAccountRepository;
use App\Services\BalanceForAccountService;

class BalancesForAccountApiController extends AppBaseController
{
    /*Funcion para generar reporte de Balance de Prueba
        Filtros: Año-Mes -> BalancesForAccount.year_month
        Cuenta Desde -> BalancesForAccount.account_id
        Cuenta Hasta -> BalancesForAccount.account_id
        Nivel -> Account.level
    */
    private $balanceForAccountService;
    private $balanceForAccountRepository;

    public function __construct(BalanceForAccountRepository $balanceForAccountRepository, BalanceForAccountService $balanceForAccountService)
    {
        $this->balanceForAccountRepository = $balanceForAccountRepository;
        $this->balanceForAccountService = $balanceForAccountService;

    }

    function getReportTrialBalance(Request $request){
        $response = $this->balanceForAccountService->balanceForAccount($request);
        return response()->json([
            'data' => $response['data']
        ], 200);

    }

    public function stateFinanze(Request $request)
    {
        $response = $this->balanceForAccountService->stateFinanze($request);
        return response()->json([
            'data' => $response['data']
        ], 200);
    }

}
