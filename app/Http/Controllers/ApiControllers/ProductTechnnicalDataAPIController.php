<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateProductTechnnicalDataAPIRequest;
use App\Http\Requests\ApiRequests\UpdateProductTechnnicalDataAPIRequest;
use App\Entities\ProductTechnnicalData;
use App\Repositories\ProductTechnnicalDataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProductTechnnicalDataController
 * @package App\Http\Controllers\ApiControllers
 */

class ProductTechnnicalDataAPIController extends AppBaseController
{
    /** @var  ProductTechnnicalDataRepository */
    private $productTechnnicalDataRepository;

    public function __construct(ProductTechnnicalDataRepository $productTechnnicalDataRepo)
    {
        $this->productTechnnicalDataRepository = $productTechnnicalDataRepo;
    }

    /**
     * Display a listing of the ProductTechnnicalData.
     * GET|HEAD /productTechnnicalDatas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productTechnnicalDataRepository->pushCriteria(new RequestCriteria($request));
        $this->productTechnnicalDataRepository->pushCriteria(new LimitOffsetCriteria($request));
        $productTechnnicalDatas = $this->productTechnnicalDataRepository->all();

        return $this->sendResponse($productTechnnicalDatas->toArray(), 'Product Technnical Datas retrieved successfully');
    }

    /**
     * Store a newly created ProductTechnnicalData in storage.
     * POST /productTechnnicalDatas
     *
     * @param CreateProductTechnnicalDataAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductTechnnicalDataAPIRequest $request)
    {
        $input = $request->all();

        $productTechnnicalDatas = $this->productTechnnicalDataRepository->create($input);

        return $this->sendResponse($productTechnnicalDatas->toArray(), 'Product Technnical Data saved successfully');
    }

    /**
     * Display the specified ProductTechnnicalData.
     * GET|HEAD /productTechnnicalDatas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductTechnnicalData $productTechnnicalData */
        $productTechnnicalData = $this->productTechnnicalDataRepository->findWithoutFail($id);

        if (empty($productTechnnicalData)) {
            return $this->sendError('Product Technnical Data not found');
        }

        return $this->sendResponse($productTechnnicalData->toArray(), 'Product Technnical Data retrieved successfully');
    }

    /**
     * Update the specified ProductTechnnicalData in storage.
     * PUT/PATCH /productTechnnicalDatas/{id}
     *
     * @param  int $id
     * @param UpdateProductTechnnicalDataAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductTechnnicalDataAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductTechnnicalData $productTechnnicalData */
        $productTechnnicalData = $this->productTechnnicalDataRepository->findWithoutFail($id);

        if (empty($productTechnnicalData)) {
            return $this->sendError('Product Technnical Data not found');
        }

        $productTechnnicalData = $this->productTechnnicalDataRepository->update($input, $id);

        return $this->sendResponse($productTechnnicalData->toArray(), 'ProductTechnnicalData updated successfully');
    }

    /**
     * Remove the specified ProductTechnnicalData from storage.
     * DELETE /productTechnnicalDatas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductTechnnicalData $productTechnnicalData */
        $productTechnnicalData = $this->productTechnnicalDataRepository->findWithoutFail($id);

        if (empty($productTechnnicalData)) {
            return $this->sendError('Product Technnical Data not found');
        }

        $productTechnnicalData->delete();

        return $this->sendResponse($id, 'Product Technnical Data deleted successfully');
    }
}
