<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateEntityWareHouseAPIRequest;
use App\Http\Requests\ApiRequests\UpdateEntityWareHouseAPIRequest;
use App\Entities\EntityWareHouse;
use App\Repositories\EntityWareHouseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EntityWareHouseController
 * @package App\Http\Controllers\ApiControllers
 */

class EntityWareHouseAPIController extends AppBaseController
{
    /** @var  EntityWareHouseRepository */
    private $entityWareHouseRepository;

    public function __construct(EntityWareHouseRepository $entityWareHouseRepo)
    {
        $this->entityWareHouseRepository = $entityWareHouseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/entityWareHouses",
     *      summary="Get a listing of the EntityWareHouses.",
     *      tags={"EntityWareHouse"},
     *      description="Get all EntityWareHouses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/EntityWareHouse")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->entityWareHouseRepository->pushCriteria(new RequestCriteria($request));
        $this->entityWareHouseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $entityWareHouses = $this->entityWareHouseRepository->all();

        return $this->sendResponse($entityWareHouses->toArray(), 'Entity Ware Houses retrieved successfully');
    }

    /**
     * @param CreateEntityWareHouseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/entityWareHouses",
     *      summary="Store a newly created EntityWareHouse in storage",
     *      tags={"EntityWareHouse"},
     *      description="Store EntityWareHouse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EntityWareHouse that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EntityWareHouse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EntityWareHouse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEntityWareHouseAPIRequest $request)
    {
        $input = $request->all();
        
        $entityWareHouses = $this->entityWareHouseRepository->create($input);

        return $this->sendResponse($entityWareHouses->toArray(), 'Entity Ware House saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/entityWareHouses/{id}",
     *      summary="Display the specified EntityWareHouse",
     *      tags={"EntityWareHouse"},
     *      description="Get EntityWareHouse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EntityWareHouse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EntityWareHouse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var EntityWareHouse $entityWareHouse */
        $entityWareHouse = $this->entityWareHouseRepository->findWithoutFail($id);

        if (empty($entityWareHouse)) {
            return $this->sendError('Entity Ware House not found');
        }

        return $this->sendResponse($entityWareHouse->toArray(), 'Entity Ware House retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEntityWareHouseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/entityWareHouses/{id}",
     *      summary="Update the specified EntityWareHouse in storage",
     *      tags={"EntityWareHouse"},
     *      description="Update EntityWareHouse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EntityWareHouse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EntityWareHouse that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EntityWareHouse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EntityWareHouse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEntityWareHouseAPIRequest $request)
    {
        $input = $request->all();

        /** @var EntityWareHouse $entityWareHouse */
        $entityWareHouse = $this->entityWareHouseRepository->findWithoutFail($id);

        if (empty($entityWareHouse)) {
            return $this->sendError('Entity Ware House not found');
        }

        $entityWareHouse = $this->entityWareHouseRepository->update($input, $id);

        return $this->sendResponse($entityWareHouse->toArray(), 'EntityWareHouse updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/entityWareHouses/{id}",
     *      summary="Remove the specified EntityWareHouse from storage",
     *      tags={"EntityWareHouse"},
     *      description="Delete EntityWareHouse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EntityWareHouse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var EntityWareHouse $entityWareHouse */
        $entityWareHouse = $this->entityWareHouseRepository->findWithoutFail($id);

        if (empty($entityWareHouse)) {
            return $this->sendError('Entity Ware House not found');
        }

        $entityWareHouse->delete();

        return $this->sendResponse($id, 'Entity Ware House deleted successfully');
    }
}
