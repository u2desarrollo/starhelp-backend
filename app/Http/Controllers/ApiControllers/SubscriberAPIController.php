<?php

namespace App\Http\Controllers\ApiControllers;

use Response;
use App\Entities\Subscriber;
use Illuminate\Http\Request;
use App\Traits\LoadImageTrait;
use App\Entities\Configuration;
use App\Entities\BranchOfficeWarehouse;
use App\Repositories\SubscriberRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BranchOfficeRepository;
use App\Repositories\ConfigurationRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Repositories\BranchOfficeWarehouseRepository;
use App\Http\Requests\ApiRequests\CreateSubscriberAPIRequest;
use App\Http\Requests\ApiRequests\UpdateSubscriberAPIRequest;
use App\Entities\BranchOffice;

/**
 * Class SubscriberController
 * @package App\Http\Controllers\ApiControllers
 */

class SubscriberAPIController extends AppBaseController {
    use LoadImageTrait;

    /** @var  SubscriberRepository */
    private $subscriberRepository;
    private $configurationRepository;
    private $branchOfficeRepository;
    private $branchOfficeWarehouseRepository;

    public function __construct(SubscriberRepository $subscriberRepo, ConfigurationRepository $configurationRepo, BranchOfficeRepository $branchOfficeRepository, BranchOfficeWarehouseRepository $branchOfficeWarehouseRepository)
    {
        $this->subscriberRepository = $subscriberRepo;
        $this->configurationRepository = $configurationRepo;
        $this->branchOfficeRepository =  $branchOfficeRepository;
        $this->branchOfficeWarehouseRepository = $branchOfficeWarehouseRepository;

    }

    /**
     * @param Request $request
     * @return Response
     *
     *
     */
    public function index(Request $request)
    {
        $this->subscriberRepository->pushCriteria(new RequestCriteria($request));
        $this->subscriberRepository->pushCriteria(new LimitOffsetCriteria($request));
        $subscribers = $this->subscriberRepository->findAll($request);

        return $this->sendResponse($subscribers->toArray(), 'Subscribers retrieved successfully');
    }

    /**
     * @param CreateSubscriberAPIRequest $request
     * @return Response
     *
     */
    public function store(CreateSubscriberAPIRequest $request)
    {
        $input = $request->except('logo','city','branch_office');
        $branchOffices = $request['branch_office'];

        $input['logo'] =  $this->saveImage($request->logo, 'subscriberLogo');
        $subscriber = $this->subscriberRepository->create($input);

        $configuration = $input['configuration'];
        $configuration['subscriber_id'] = $subscriber->id;
        if(isset($configuration['first_logo'])) {
            $configuration['first_logo'] = $this->saveImage($input['configuration']['first_logo'], 'subscriberLogo/configuration');
        }
        $this->configurationRepository->create($configuration);

        $branchOffice = BranchOffice::create([
            'subscriber_id' => $subscriber->id,
            'name' => $branchOffices['name'],
            'address' => $branchOffices['address'],
            'telephone' => $branchOffices['telephone'],
            'fax' => $branchOffices['fax'],
            'main' => $branchOffices['main'],
            'city_id' => $branchOffices['city_id'],
            'latitude' => $branchOffices['latitude'],
            'length' => $branchOffices['length'],
            'branchoffice_type' => $branchOffices['branchoffice_type'],
        ]);

        $warehouse = BranchOfficeWarehouse::create([
            'branchoffice_id' => $branchOffice->id,
            'warehouse_code' => '1',
            'warehouse_description' => 'Bodega 1',
            'update_inventory' => 0,
            'exits_no_exist' => 0,
            'negative_balances' => 0,
            'warehouse_status' => 1,
            'last_date_physical_inventory' => today(),
            'number_physical_inventor' => '1',
        ]);
        return $this->sendResponse($subscriber->toArray(), 'Subscriber saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function show($id)
    {
        /** @var Subscriber $subscriber */
        $subscriber = $this->subscriberRepository->findForId($id);

        if (empty($subscriber)) {
            return $this->sendError('Suscriptor no encontrado');
        }
        return $this->sendResponse($subscriber->toArray(), 'Subscriber retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSubscriberAPIRequest $request
     * @return Response
     *
     *
     */
    public function update($id, UpdateSubscriberAPIRequest $request)
    {
        $input = $request->except('logo');
        /** @var Subscriber $subscriber */
        $subscriber = $this->subscriberRepository->findForId($id);

        if($subscriber->logo != $request['logo']){
            $input['logo'] =  $this->saveImage($request['logo'], 'subscriberLogo');
        }

        if (empty($subscriber)) {
            return $this->sendError('Subscriber not found');
        }

        $subscriber_save = $this->subscriberRepository->update($input, $id);

        $configuration = $input['configuration'];
        if(isset($configuration['first_logo'])) {
            if($subscriber->configuration->first_logo != $configuration['first_logo'] ) {
                $configuration['first_logo'] = $this->saveImage($input['configuration']['first_logo'], 'subscriberLogo/configuration');
            } else {
                unset($configuration['first_logo']);
            }
        }

        if (isset($configuration['id'])) {
            $subscriber->configuration()->update($configuration, $configuration['id']);
        } else {
            $config = new Configuration($configuration);
            $subscriber->configuration()->save($config);
        }

        $respuestaId = $this->subscriberRepository->findForId($id);
        return $this->sendResponse($respuestaId, 'Subscriber updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var Subscriber $subscriber */
        $subscriber = $this->subscriberRepository->findWithoutFail($id);

        if (empty($subscriber)) {
            return $this->sendError('Subscriber not found');
        }

        $subscriber->delete();

        return $this->sendResponse($id, 'Subscriber deleted successfully');
    }

}
