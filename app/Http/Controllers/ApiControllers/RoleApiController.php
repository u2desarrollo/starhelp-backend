<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Parameter;
use App\Entities\Permission;
use App\Entities\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPUnit\Framework\MockObject\Matcher\Parameters;

class RoleApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('getPermissions')->get();
        return response()->json($roles, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $role = new Role();
        $role->role = $request->name;
        $role->created_by = auth()->user()->id;
        $role->save();
        $role->getPermissions()->sync($request->permissions);
        $role->save();
        return response()->json($role, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::where('id', $id)->with('getPermissions')->first();
        $collection = [];
        foreach ($role->getPermissions as $key => $value) {
            $collection[] = $value->id;
        }
        $role->getPermissions = $collection;
        return response()->json($role, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::where('id', $id)->first();
        $role->role = $request->name;
        $role->getPermissions()->sync($request->permissions);
        $role->save();
        return response()->json($role, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = Role::find($id);
        $rol->delete();
        return response()->json($rol, 200);
    }

    public function getPermissions()
    {
        $permissions = Permission::orderByRaw('category_role_id, permission ASC')->get();
        $categories = Parameter::select('id', 'name_parameter')
            ->whereHas('paramtable', function ($paramtable) {
                $paramtable->where('code_table', 'CRIU2');
            })
            ->orderBy('code_parameter', 'ASC')
            ->get();

        return response()->json([
            'categories' => $categories,
            'permissions' => $permissions
        ], 200);
    }
}
