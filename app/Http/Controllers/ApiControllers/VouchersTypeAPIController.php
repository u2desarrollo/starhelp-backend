<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateVouchersTypeAPIRequest;
use App\Http\Requests\ApiRequests\UpdateVouchersTypeAPIRequest;
use App\Entities\VouchersType;
use App\Repositories\VouchersTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VouchersTypeController
 * @package App\Http\Controllers\ApiControllers
 */

class VouchersTypeAPIController extends AppBaseController
{
    /** @var  VouchersTypeRepository */
    private $vouchersTypeRepository;

    public function __construct(VouchersTypeRepository $vouchersTypeRepo)
    {
        $this->vouchersTypeRepository = $vouchersTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     *
     */
    public function index(Request $request)
    {
        $this->vouchersTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->vouchersTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $vouchersTypes = $this->vouchersTypeRepository->findAll($request);

        return $this->sendResponse($vouchersTypes->toArray(), 'Vouchers Types retrieved successfully');
    }

    /**
     * @param CreateVouchersTypeAPIRequest $request
     * @return Response
     *
     *
     */
    public function store(CreateVouchersTypeAPIRequest $request)
    {
        $input = $request->all();

        $exits_code = VouchersType::where('code_voucher_type', $input['code_voucher_type'])
                                    ->where('branchoffice_id', $input['branchoffice_id'])->exists();
        if ($exits_code) {
            return response()->json(['message' => 'El codigo del tipo de comprobante ya existe.'], 400);
        }

        $vouchersTypes = $this->vouchersTypeRepository->createVouchersType($input);

        return $this->sendResponse($vouchersTypes->toArray(), 'Vouchers Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function show($id)
    {
        /** @var VouchersType $vouchersType */
        $vouchersType = $this->vouchersTypeRepository->findForId($id);

        if (empty($vouchersType)) {
            return $this->sendError('Vouchers Type not found');
        }

        return $this->sendResponse($vouchersType->toArray(), 'Vouchers Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateVouchersTypeAPIRequest $request
     * @return Response
     *
     *
     */
    public function update($id, UpdateVouchersTypeAPIRequest $request)
    {
        // if($request['only_consecutive'] == 0){
        //     $input = $request->except('consecutive_number');
        // } else {
        //     $input = $request->only('consecutive_number');
        // }
        $input = $request->all();
        /** @var VouchersType $vouchersType */
        $vouchersType = $this->vouchersTypeRepository->findForId($id);

        if (empty($vouchersType)) {
            return $this->sendError('Vouchers Type not found');
        }

        $exits_code = VouchersType::where('code_voucher_type', $input['code_voucher_type'])
                                    ->where('branchoffice_id', $input['branchoffice_id'])
                                    ->where('id', '!=', $id)
                                    ->exists();
        if ($exits_code) {
            return response()->json(['message' => 'El codigo del tipo de comprobante ya existe.'], 400);
        }

        $vouchersType = $this->vouchersTypeRepository->update($input, $id);

        return $this->sendResponse($vouchersType->toArray(), 'VouchersType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     *
     */
    public function destroy($id)
    {
        /** @var VouchersType $vouchersType */
        $vouchersType = $this->vouchersTypeRepository->findWithoutFail($id);

        if (empty($vouchersType)) {
            return $this->sendError('Vouchers Type not found');
        }

        $vouchersType->delete();

        return $this->sendResponse($id, 'Vouchers Type deleted successfully');
    }
    public function getVouchersType(){
        $vouchersType=$this->vouchersTypeRepository->getVouchersType();
        if (empty($vouchersType)) {
            return $this->sendError('Vouchers Type not found');
        }
        return $this->sendResponse($vouchersType->toArray(), 'VouchersType successfully');
    }

    /**
     * consulta el proximo consecutivo
     * 
     * @author santiago torres
     */

    public function getConsecutive($vouchertype_id)
    {
        $vouchers_type = $this->vouchersTypeRepository->getConsecutive($vouchertype_id);
        $vouchers_type->consecutive_number = $vouchers_type->consecutive_number+1;
        
        return $vouchers_type;
    }

    public function getVoucherType(Request $request)
    {
        return $this->vouchersTypeRepository->getVoucherType($request);
    }

    public function getConsecutiveByCode($code, $branchoffice_id)
    {
        $vouchers_type = $this->vouchersTypeRepository->getConsecutiveByCode($code, $branchoffice_id);
        $vouchers_type->consecutive_number = $vouchers_type->consecutive_number+1;
        return $vouchers_type;
    }
}
