<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateRequestHeaderAPIRequest;
use App\Http\Requests\ApiRequests\UpdateRequestHeaderAPIRequest;
use App\Entities\RequestHeader;
use App\Repositories\RequestHeaderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RequestHeaderController
 * @package App\Http\Controllers\ApiControllers
 */

class RequestHeaderAPIController extends AppBaseController
{
    /** @var  RequestHeaderRepository */
    private $requestHeaderRepository;

    public function __construct(RequestHeaderRepository $requestHeaderRepo)
    {
        $this->requestHeaderRepository = $requestHeaderRepo;
    }

    /**
     * Display a listing of the RequestHeader.
     * GET|HEAD /requestHeaders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->requestHeaderRepository->pushCriteria(new RequestCriteria($request));
        $this->requestHeaderRepository->pushCriteria(new LimitOffsetCriteria($request));
        //Llamo la funcion del repositorio para la consulta con el filtro
        $requestHeaders = $this->requestHeaderRepository->getRequestHeaders($request);

        return $this->sendResponse($requestHeaders, 'Request Headers retrieved successfully');
    }

    /**
     * Store a newly created RequestHeader in storage.
     * POST /requestHeaders
     *
     * @param CreateRequestHeaderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestHeaderAPIRequest $request)
    {
        $input = $request->all();

        $requestHeader = $this->requestHeaderRepository->create($input);

        return $this->sendResponse($requestHeader->toArray(), 'Request Header saved successfully');
    }

    /**
     * Display the specified RequestHeader.
     * GET|HEAD /requestHeaders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RequestHeader $requestHeader */
        $requestHeader = $this->requestHeaderRepository->findBiId($id);

        if (empty($requestHeader)) {
            return $this->sendError('Request Header not found');
        }

        return $this->sendResponse($requestHeader, 'Request Header retrieved successfully');
    }

    /**
     * Update the specified RequestHeader in storage.
     * PUT/PATCH /requestHeaders/{id}
     *
     * @param  int $id
     * @param UpdateRequestHeaderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var RequestHeader $requestHeader */
        $requestHeader = $this->requestHeaderRepository->update($input, $id);

        if (empty($requestHeader)) {
            return $this->sendError('Request Header not found');
        }

        return $this->sendResponse($requestHeader->toArray(), 'RequestHeader updated successfully');
    }

    /**
     * Remove the specified RequestHeader from storage.
     * DELETE /requestHeaders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RequestHeader $requestHeader */
        $requestHeader = $this->requestHeaderRepository->findWithoutFail($id);

        if (empty($requestHeader)) {
            return $this->sendError('Request Header not found');
        }

        $requestHeader->delete();

        return $this->sendResponse($id, 'Request Header deleted successfully');
    }
}
