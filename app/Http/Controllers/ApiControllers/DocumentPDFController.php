<?php

namespace App\Http\Controllers\ApiControllers;

use App\Entities\Document;
use App\Entities\PdfSale;
use App\Entities\Pdf;
use App\Entities\despatchPdf;
use App\Entities\PdfDocumentsBalance;
use App\Entities\PickinPdf;
use App\Repositories\DocumentRepository;
use App\Http\Controllers\Controller;
use App\Services\DocumentsService;

use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;
use App\Traits\GeneratePdfTrait;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class DocumentPDFController extends Controller
{

    use GeneratePdfTrait;

    protected $pdf;
    protected $despatchPdf;
    protected $PickinPdf;
    private $documentRepository;
    protected $documentsService;

    public function __construct(Pdf $pdf, DocumentRepository $documentRepo, despatchPdf $despatchPdf, PickinPdf $PickinPdf, DocumentsService $documentsService)
    {
        $this->pdf = $pdf;
        $this->despatchPdf = $despatchPdf;
        $this->PickinPdf = $PickinPdf;
        $this->documentRepository = $documentRepo;
        $this->documentsService = $documentsService;
    }

    public function createPdf($id)
    {
        $document = $this->documentRepository->getDocument($id);
        if (empty($document)) {
            return $this->sendError('Document not found');
        } else {

            //return $document;
            $fileName = '/storage/pdf/' . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document);

            return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
        }
    }

    /**
     * Genera pdf de facrturas
     *
     * @author Santiago
     */
    public function despatch(Request $date)
    {
        if ($date->despatch) {
            $date->merge(['perPage'=>count($date->despatch)]);
        }
        //obtener info de las facturas para despachar
        $res = $this->documentsService->getDespatchInvoicedList($date);
        //organiza la informacion necesaria para el reporte
        $despatchs = $this->documentRepository->listInfoForDespatch($res["data"]);

        //se ordenan las facturas por zonas
        $despatchsProtractor = array();
        foreach ($despatchs as $key => $despatch) {
            $despatchsProtractor[$despatch["protractor"]] [] = $despatch;
        }

        $fileName = storage_path() . '/app/public/pdf/' . $this->generatePdfdespatch($this->despatchPdf, $despatchsProtractor);
        $headers = [
            'Content-Type' => 'application/pdf',
        ];
        return response()->download($fileName, 'bills.pdf', $headers);
        //return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
    }

    /**
     * Genera pdf de pickin
     *
     * @author Santiago
     */


    public function pickin(Request $date)
    {
        if ($date->despatch) {
            $date->merge(['perPage'=>count($date->despatch)]);
        }
        //obtener info de las facturas para despachar
        $res = $this->documentsService->getDespatchInvoicedList($date);

        //organiza la informacion necesaria para el reporte
        $pickin = $this->documentRepository->listProductsPickin($res["data"]);
        //return $pickin;

        $pickinorderbylocation = [];
        foreach ($pickin as $key => $value) {

            $collection = collect($value);

            $orderbylocation = $collection->sortBy('location')->values()->all();

            $pickinorderbylocation[$key] = $orderbylocation;
        }

        $fileName = storage_path() . '/app/public/pdf/' . $this->generatePdfpickin($this->PickinPdf, $pickinorderbylocation);


        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($fileName, 'pickin.pdf', $headers);
        //return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
    }

    public function createPdfSale()
    {
        $fileName = '/storage/pdf/' . $this->generatePdfSale($pdfsale = new Pdfsale);

        return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
    }

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function transfer($id)
    {
        $transfer = $this->documentRepository->searchTransfer($id);

        //calcular peso
        $transfer->weight = 0;
        foreach ($transfer->documentsProducts as $key => $dp) {
            $transfer->weight += (int) $dp->product->weight;
        }

        $fileName = storage_path() . '/app/public/pdf/' . $this->generatePdftransfer($transfer);

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
        return response()->download($fileName, 'Traslado.pdf', $headers);
    }

    /**
     * @param Document $document
     * @return BinaryFileResponse
     */
    public function downloadPdfTransfer(Document $document)
    {
        $transfer = $this->documentRepository->searchTransfer($document->id);

        //calcular peso
        $transfer->weight = 0;
        foreach ($transfer->documentsProducts as $key => $dp) {
            $transfer->weight += (int) $dp->product->weight;
        }

        $fileName = storage_path() . '/app/public/pdf/' . $this->generatePdftransfer($transfer);

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($fileName, 'Traslado.pdf', $headers);
    }


    // public function generatePDFCashReceipts(Request $date)
    // {
    //     //obtener info de las facturas para despachar
    //     $res = $this->documentsService->getDespatchInvoicedList($date);

    //     //organiza la informacion necesaria para el reporte
    //     $pickin = $this->documentRepository->listProductsPickin($res["data"]);
    //     //return $pickin;

    //     $pickinorderbylocation = [];
    //     foreach ($pickin as $key => $value) {

    //         $collection = collect($value);

    //         $orderbylocation = $collection->sortBy('location')->values()->all();

    //         $pickinorderbylocation[$key] = $orderbylocation;
    //     }

    //     $fileName = storage_path() . '/app/public/pdf/' . $this->generatePDFCashReceipts($this->PickinPdf, $pickinorderbylocation);


    //     $headers = [
    //         'Content-Type' => 'application/pdf',
    //     ];

    //     return response()->download($fileName, 'pickin.pdf', $headers);
    //     //return $this->sendResponse(['pdf' => $fileName], 'Document retrieved successfully');
    // }

    /**
     * @param Document $document
     * @return BinaryFileResponse
     */
    public function downloadPdf(Document $document)
    {
        $documentFound = $this->documentRepository->getDocument($document->id);

        $path = storage_path('app/public/pdf/');

        $fileName = $this->generatePdfOrder($this->pdf, $this->documentRepository, $documentFound);

        $file = $path . $fileName;
        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, $fileName, $headers);

    }




     public function pdfDocumentsBalance(Request $request)
     {
        $subscriber_id = Auth::user()->subscriber_id;         
        if($subscriber_id == 1){
            $fileName =  $this->generatePdfReceivable($this->pdf,$this->documentRepository,$request);
        }else if($subscriber_id == 3){
            $fileName =  $this->generatePdfReceivableCol($this->pdf,$this->documentRepository,$request);
        }

         $headers = [
             'Content-Type' => 'application/pdf',
         ];
         return response()->json($fileName,200, $headers);


     }

     public function pdfPreLegal(Request $request)
     {
        $subscriber_id = Auth::user()->subscriber_id;         
        if($subscriber_id == 1){
            $fileName =  $this->generateFilePrelegal($this->pdf,$this->documentRepository,$request);
        }else if($subscriber_id == 3){
            $fileName =  $this->generateFilePrelegalCol($this->pdf,$this->documentRepository,$request);
        }
         
         $headers = [
             'Content-Type' => 'application/pdf',
         ];
         return response()->json($fileName,200, $headers);


     }
     

     public function pdfNegativeReport(Request $request)
     {

        
        $subscriber_id = Auth::user()->subscriber_id;         
        if($subscriber_id == 1){
            $fileName =  $this->generateNegativeFile($this->pdf,$this->documentRepository,$request);
        }else if($subscriber_id == 3){
            $fileName =  $this->generateNegativeFileCol($this->pdf,$this->documentRepository,$request);
        }
         $headers = [
             'Content-Type' => 'application/pdf',
         ];
         return response()->json($fileName,200, $headers);


     }

    public function quareCashRegisterPdf(Request $request)
    {
        $fileName =  $this->generateSquareCashRegisterPdf($this->pdf, $this->documentRepository, $request);
        return response()->json($fileName,200);
    }

    /**
     * pdf contable
     */
    public function accountingPDf($id)
    {
        $accounting_document = $this->documentRepository->getAccountDocument($id);
        $file_name = "/storage/pdf/" . $this->generateAccountingDocument($accounting_document);
        return response()->json($file_name,200);
    }
    
    public function alertEmployeesWithoutActivityPdf($data)
    {
        return $this->generateAlertEmployeesWithoutActivityPdf($this->pdf,  $data);
    }

    public function downloadAccountingPdf($id)
    {

        $path = storage_path('app/public/pdf/');

        $accounting_document = $this->documentRepository->getAccountDocument($id);
        $fileName = $this->generateAccountingDocument($accounting_document);

        $file = $path . $fileName;

        $headers = [
            'Content-Type' => 'application/pdf',
        ];



        return response()->download($file, $fileName, $headers);
    }

}
