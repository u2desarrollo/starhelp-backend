<?php

namespace App\Http\Controllers\ApiControllers;

// Entities

use App\Entities\Contact;
use App\Entities\Document;
use App\Entities\Receivable;

// Repositories
use App\Repositories\ReceivableRepository;
use App\Repositories\DocumentsBalanceRepository;
use Prettus\Repository\Criteria\RequestCriteria;

// Service
use App\Services\DocumentsBalanceService;

// otros
use App\Http\Requests\ApiRequests\CreateReceivableAPIRequest;
use App\Http\Requests\ApiRequests\UpdateReceivableAPIRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\ApiRequests\CreateDocumentsBalanceAPIRequest;
use App\Http\Requests\ApiRequests\UpdateDocumentsBalanceAPIRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;


/**
 * Class ReceivableController
 * @package App\Http\Controllers\ApiControllers
 */

class DocumentsBalanceAPIController extends AppBaseController
{
    /** @var  ReceivableRepository */
    private $documentsBalanceRepository;
    private $documentsBalanceService;

    public function __construct(
        DocumentsBalanceRepository $receivableRepo,
        DocumentsBalanceService $documentsBalanceService
    ) {
        $this->documentsBalanceRepository = $receivableRepo;
        $this->documentsBalanceService = $documentsBalanceService;
    }

    /**
     * Display a listing of the Receivable.
     * GET|HEAD /receivables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->documentsBalanceRepository->pushCriteria(new RequestCriteria($request));
        $this->documentsBalanceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receivables = $this->documentsBalanceRepository->tracking($request);
        return response()->json($receivables, 200);
        //return $this->sendResponse($receivables->toArray(), 'Receivables retrieved successfully');
    }

    /**
     * Store a newly created Receivable in storage.
     * POST /receivables
     *
     * @param CreateReceivableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentsBalanceAPIRequest $request)
    {
        $input = $request->all();

        $receivables = $this->documentsBalanceRepository->create($input);

        return $this->sendResponse($receivables->toArray(), 'Receivable saved successfully');
    }

    /**
     * Display the specified Receivable.
     * GET|HEAD /receivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $res = $this->documentsBalanceService->getTrackingByContactId($id);
        return response()->json($res['data'], $res['code']);
    }

    /**
     * Update the specified Receivable in storage.
     * PUT/PATCH /receivables/{id}
     *
     * @param  int $id
     * @param UpdateReceivableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentsBalanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Receivable $receivable */
        $receivable = $this->documentsBalanceRepository->findWithoutFail($id);

        if (empty($receivable)) {
            return $this->sendError('Receivable not found');
        }

        $receivable = $this->documentsBalanceRepository->update($input, $id);

        return $this->sendResponse($receivable->toArray(), 'Receivable updated successfully');
    }

    /**
     * Remove the specified Receivable from storage.
     * DELETE /receivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Receivable $receivable */
        $receivable = $this->documentsBalanceRepository->findWithoutFail($id);

        if (empty($receivable)) {
            return $this->sendError('Receivable not found');
        }

        $receivable->delete();

        return $this->sendResponse($id, 'Receivable deleted successfully');
    }

    /**
     * @author Santiago
     */

    public function despatch()
    {
        return $this->documentsBalanceRepository->dispatch();
    }

    /**
     *
     * Este metodo se encarga de retornar la cantidad total
     * de la cartera del cliente.
     *
     * @author Kevin Galindo
     */
    public function getTotalWalletByContactId()
    {
        $res = $this->documentsBalanceService->getTotalWalletByContactId(6523);
        return $res;
    }


    public function readFile($pathFile, $body)
    {
        Storage::disk('local')->put($pathFile, null);
        $file = fopen(storage_path('app/' . $pathFile), "w");

        foreach ($body as $line) {

            fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fputcsv($file, $line, ";");
        }
        rewind($file);
        fclose($file);
    }



    public function getReceivableCSV($id, Request $request)
    {
        $documents =   $documents = Document::select(
            'id',
            'vouchertype_id',
            'document_date',
            'issue_date',
            'due_date',
            'total_value',
            'consecutive',
            'prefix',
            'thread',
            'branchoffice_id'

        )
            ->with([

                'receivable' => function ($query) {
                    $query->where('invoice_balance', '>', 0);
                },
                'transactions' => function ($query) {
                    $query->orderBy('document_date', 'asc')->orderBy('id', 'asc')->with([
                        'document' => function ($query) {
                            $query->with([
                                'vouchertype', 'contributionInvoicesApp.cashReceiptsApp'
                            ]);
                        }
                    ]);
                },
                'vouchertype',
                'documentsProducts'

            ])
            ->whereHas('receivable', function ($query) {
                $query->where('invoice_balance', '>', 0);
            })
            ->where('operationType_id', 5)
            ->where('contact_id', $id)
            ->get();

        $contact = Contact::findOrFail($id);
        $body = [
            [
                'Estado de Cuenta ' . date('Y-m-d H:m:s'),
                $contact ? $contact->name . ' ' . $contact->surname . '-' .
                    $contact->identification : ''
            ],
            ['Fecha-Doc', 'Fecha-Venc', 'Tipo-Doc', 'Numero', 'Doc-Aplic', 'Cargos', 'Abonos', 'Saldo']
        ];

        $bodyCredits = [];
        $debit = 0;

        if (count($documents) > 0) {


            //  monto las lineas correspondientes a facturas
            foreach ($documents as $key => $element) {

                $lineDocument = [];
                $lineDocument[] = Carbon::parse($element->document_date)->format('Y/m/d');
                $lineDocument[] = Carbon::parse($element->due_date)->format('Y-m-d');
                $lineDocument[] = $element->vouchertype->name_voucher_type;
                $element->prefix=str_replace(' ','',$element->prefix);
                $lineDocument[] = $element->prefix != null && $element->prefix != ''?  $element->prefix. '-' . $element->consecutive : $element->consecutive;
                $lineDocument[] = $element->prefix != null && $element->prefix != '' ? $element->prefix. '-' . $element->consecutive : $element->consecutive;
                $lineDocument[] = $element->total_value;
                $lineDocument[] = '';



                foreach ($element->transactions as $key => $transaction) {
                    if ($transaction->transaction_type == "D") {
                        //voy sumando los debitos para llegar al total
                        $debit += $transaction->transaction_value;
                        //  el valor de la  factura es su propio credito mas el de las anteriores
                        $lineDocument[] = $debit;
                        // saco las facturas  que tienen creditos aplicados aparte
                        // para mas tarde empezar a restar al debito
                    } elseif ($transaction->transaction_type == "C" && $transaction->payment_applied_erp == 1) {
                        $bodyCredits[] = $element;
                    }
                }

                array_push($body, $lineDocument);
            }

            //  lineas correspondientes a los creditos aplicados

            foreach ($bodyCredits as $key => $bodyC) {
                $lineCredit=[];
                foreach ($bodyC->transactions as $key => $transaction) {
                    if ($transaction->transaction_type == "C" && $transaction->payment_applied_erp == 1) {
                        $lineCredit[] = $transaction->document_date;
                        $lineCredit[] = '';
                        $lineCredit[] = $transaction->voucher_type;
                        $lineCredit[] =  $transaction->voucher_type.'-'.$transaction->document_number;
                        $lineCredit[] = $bodyC->prefix != null && $bodyC->prefix != '' ? $bodyC->prefix. '-' . $bodyC->consecutive : $bodyC->consecutive;
                        $lineCredit[] = '';
                        $lineCredit[] = $transaction->transaction_value;
                        $debit -= $transaction->transaction_value;
                        $lineCredit[] = $debit;
                        array_push($body, $lineCredit);
                    }
                }
            }

            $pathFile = 'public/CSVUsers/cartera.csv';
            $this->readFile($pathFile, $body);

            return Storage::download('/public/CSVUsers/cartera.csv');
        } else {
            return response()->json('El cliente no tiene Cartera', 200);
        }
    }
}
