<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Requests\ApiRequests\CreateContactDocumentAPIRequest;
use App\Http\Requests\ApiRequests\UpdateContactDocumentAPIRequest;
use App\Entities\ContactDocument;
use App\Repositories\ContactDocumentRepository;
use App\Traits\LoadImageTrait;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;
use Response;

/**
 * Class ContactDocumentController
 * @package App\Http\Controllers\ApiControllers
 */
class ContactDocumentAPIController extends AppBaseController
{

    use LoadImageTrait;

    /** @var  ContactDocumentRepository */
    private $contactDocumentRepository;

    public function __construct(ContactDocumentRepository $contactDocumentRepo)
    {
        $this->contactDocumentRepository = $contactDocumentRepo;
    }

    /**
     * Display a listing of the ContactDocument.
     * GET|HEAD /contactDocuments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $contactDocuments = $this->contactDocumentRepository->orderBy('created_at', 'DESC')
            ->with('parameter')
            ->findWhere(['contact_id' => $request['contact_id']]);

        return $this->sendResponse($contactDocuments->toArray(), 'Contact Documents retrieved successfully');
    }

    /**
     * Almacena un archivo en el servicio S3 de Amazon
     * POST /contactDocuments
     *
     * @param CreateContactDocumentAPIRequest $request
     *
     * @return Response
     * @throws FileNotFoundException
     * @throws ValidatorException
     * @author Jhon García
     */
    public function store(CreateContactDocumentAPIRequest $request)
    {
        $loadFileLocal = false;

        //Obtiene un uuid para asignarlo como nombre al archivo
        $fileName = Str::uuid() . "." . $request->file->getClientOriginalExtension();

        try {

            //Agrega el fileName al request
            $request->merge(['path' => $fileName]);

            //Guarda el archivo en el disco local temporalmente
            $request->file('file')->storeAs('public/contacts/documents/', $fileName);
            $loadFileLocal = true;

            //Obtiene el archivo nuevamente
            $file = Storage::disk('public')->get('contacts/documents/' . $fileName);

            $path = config('app.path_files_contacts') . $request->contact_id . '/' . $fileName;

            //Lo guarda en el servicio S3 de Amazon
            Storage::disk('s3')->put($path, $file);

            //Guardamos el registro en base de datos
            $contactDocuments = $this->contactDocumentRepository->create($request->all());

            //Eliminamos el archivo del disco local
            Storage::disk('public')->delete('contacts/documents/' . $fileName);

            return $this->sendResponse($contactDocuments->toArray(), 'Contact Document saved successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            if ($loadFileLocal) {
                //Eliminamos el archivo del disco local
                Storage::disk('public')->delete('contacts/documents/' . $fileName);
            }
            return $this->sendResponse($exception->getMessage(), 'Error al cargar documento');
        }

    }

    /**
     * Display the specified ContactDocument.
     * GET|HEAD /contactDocuments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function download($id)
    {
        $contactDocument = $this->contactDocumentRepository->findWithoutFail($id);

        if (empty($contactDocument)) {
            return $this->sendError('Contact Document not found');
        }

        $path = config('app.path_files_contacts') . $contactDocument->contact_id . '/' . $contactDocument->path;

        return Storage::disk('s3')->download($path , $contactDocument->path, []);
    }

    public function show($id)
    {
        $contactDocument = $this->contactDocumentRepository->findWithoutFail($id);

        if (empty($contactDocument)) {
            return $this->sendError('Contact Document not found');
        }

        $array = explode('.', $contactDocument->path);
        $extension = array_pop($array);

        $extensionsUrl = ['pdf', 'png', 'jpg', 'jpeg', 'gif'];

        $path = config('app.path_files_contacts') . $contactDocument->contact_id . '/' . $contactDocument->path;

        $url = Storage::disk('s3')->temporaryUrl($path, now()->addMinutes(5));

        if (in_array($extension, $extensionsUrl)) {
            return $url;
        }

        return urlencode($url);
    }

    /**
     * Update the specified ContactDocument in storage.
     * PUT/PATCH /contactDocuments/{id}
     *
     * @param int $id
     * @param UpdateContactDocumentAPIRequest $request
     *
     * @return Response
     * @throws ValidatorException
     */
    public function update($id, UpdateContactDocumentAPIRequest $request)
    {
        $input = $request->all();

        $contactDocument = $this->contactDocumentRepository->findWithoutFail($id);

        if (empty($contactDocument)) {
            return $this->sendError('Contact Document not found');
        }

        /* if ($input['path'] != $contactDocument->path) {
             $input['path'] = $this->saveImage($input['path'], 'contacts/documents');
         }*/

        $contactDocument = $this->contactDocumentRepository->update(Arr::except($input, ['path']), $id);

        return $this->sendResponse($contactDocument->toArray(), 'ContactDocument updated successfully');
    }

    /**
     * Remove the specified ContactDocument from storage.
     * DELETE /contactDocuments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ContactDocument $contactDocument */
        $contactDocument = $this->contactDocumentRepository->findWithoutFail($id);

        if (empty($contactDocument)) {
            return $this->sendError('Contact Document not found');
        }

        $contactDocument->delete();

        return $this->sendResponse($id, 'Contact Document deleted successfully');
    }
}
