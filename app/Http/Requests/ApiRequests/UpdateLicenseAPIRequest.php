<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\License;
use InfyOm\Generator\Request\APIRequest;

class UpdateLicenseAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('licencias');
        return [
            'license_code' => "required|unique:licenses,license_code,{$id},id,deleted_at,NULL"
        ];
    }

    public function attributes(){
        return [
            'license_code' => 'Código licencia'
        ];
    }

}
