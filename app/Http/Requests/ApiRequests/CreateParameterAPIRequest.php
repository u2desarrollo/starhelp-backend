<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Parameter;
use Illuminate\Http\Request;
use InfyOm\Generator\Request\APIRequest;

class CreateParameterAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->paramtable_id;
        return Parameter::rulesCreate($id);
    }

    public function attributes(){
        return  [
            'code_parameter' => 'Código'
        ];
    }
}
