<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\VisitCustomer;
use InfyOm\Generator\Request\APIRequest;

class CreateVisitCustomerAPIRequest extends APIRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'iduser' => 'required|integer',
			'cod_bodega' => 'required|integer',
			'cod_ter' => 'required|integer',
			'latitud_checkin' => 'required',
            'longitud_checkin' => 'required',
            'cobranza' => 'required',
            'capacitacion' => 'required',
			'gestion' => 'required',
		];
	}

	public function messages() {
		return [
			'iduser.required' => ' No envió el código del vendedor.',
			'cod_bodega.required' => ' No envió el código de la sucursal (bodega).',
			'cod_ter.required' => ' No envio el codigo del cliente.',
			'latitud_checkin.required' => ' No envió la latitud.',
            'longitud_checkin.required' => ' No envió la longitud.',

            'cobranza.required' => 'Campo cobranza requerido.',
            'capacitacion.required' => 'Capacitacion cobranza requerido.',
			'gestion.required' => 'Campo gestion requerido.'
		];
	}
}
