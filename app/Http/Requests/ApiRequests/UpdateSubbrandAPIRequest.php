<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Subbrand;
use Illuminate\Http\Request;
use InfyOm\Generator\Request\APIRequest;

class UpdateSubbrandAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $brand_id = $request->brand_id;
        $id = $this->route('submarca');
        return Subbrand::rulesUpdate($brand_id, $id);
    }

    public function attributes(){
        return  [
            'subbrand_code' => 'Código'
        ];
    }


}
