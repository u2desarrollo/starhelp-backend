<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Consignment;
use InfyOm\Generator\Request\APIRequest;

class CreateConsignmentAPIRequest extends APIRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return Consignment::$rules;
    }

    public function messages()
    {
     return   [
         'iduser.required'=>'el id del usuario es requerido',
         'banco.required'=>'el id del banco es requerido',
         'numeroConsignacion.required'=>'El campo numeroConsignacion es requerido',
         'recibosCaja.required'=>'El campo recibosCaja es requerido',

     ];
    }
}
