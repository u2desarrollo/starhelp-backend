<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\VisitCustomer;
use InfyOm\Generator\Request\APIRequest;

class UpdateVisitCustomerAPIRequest extends APIRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'latitud_checkout' => 'required',
			'longitud_checkout' => 'required',
			'checkout_ok' => 'required|string',
			'iduser' => 'required|string',
			'cod_segven' => 'required|string'
		];
	}

	public function messages()
	{
		return [
			'latitud_checkout.required' => ' No envió la latitud.',
			'longitud_checkout.required' => ' No envió la longitud.',
			'checkout_ok.required' => ' No envió el estado del checkout.',
			'iduser.required' => ' No envió el id del vendedor.',
			'cod_segven.required' => ' No envió el id del checkin.'
		];
	}
}
