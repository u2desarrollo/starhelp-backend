<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Brand;
use InfyOm\Generator\Request\APIRequest;

class UpdateBrandAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('marca');
        return [
            'brand_code' => "required|unique:brands,brand_code,{$id},id,deleted_at,NULL"
        ];
    }

    public function attributes(){
        return [
            'brand_code' => 'Código'
        ];
    }
}
