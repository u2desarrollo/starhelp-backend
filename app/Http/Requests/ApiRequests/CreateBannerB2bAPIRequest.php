<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\BannerB2b;
use InfyOm\Generator\Request\APIRequest;

class CreateBannerB2bAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return BannerB2b::$rules;
    }
}
