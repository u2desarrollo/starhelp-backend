<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\ParamTable;
use InfyOm\Generator\Request\APIRequest;

class CreateParamTableAPIRequest extends APIRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return  [
            'code_table' => "required|unique:paramtables,code_table,NULL, deleted_at,deleted_at,NULL"
        ];
    }

    public function attributes(){
        return  [
            'code_table' => 'Código'
        ];
    }
}
