<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Subbrand;
use Illuminate\Http\Request;
use InfyOm\Generator\Request\APIRequest;

class CreateSubbrandAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        $id = $request->brand_id;
        return Subbrand::rulesCreate($id);
    }

    public function attributes(){
        return  [
            'subbrand_code' => 'Código'
        ];
    }
}
