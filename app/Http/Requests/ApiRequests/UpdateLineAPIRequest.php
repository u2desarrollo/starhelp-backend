<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Line;
use InfyOm\Generator\Request\APIRequest;

class UpdateLineAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = $this->route('linea');
        return [
            'line_code' => "required|unique:lines,line_code,{$id},id,deleted_at,NULL|max:15",
        ];
    }

    public function attributes(){
        return  [
            'line_code' => 'Código de la linea'
        ];
    }
}
