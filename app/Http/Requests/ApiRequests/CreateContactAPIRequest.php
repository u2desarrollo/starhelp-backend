<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Contact;
use InfyOm\Generator\Request\APIRequest;

class CreateContactAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Contact::$rulesCreate;
    }

    public function attributes()
    {
        return [
            'identification' => 'Identificación',
            'identification_type' => 'Tipo de identificación',
            'name' => 'Nombres / Razón social',
            'city_id' => 'Ciudad',
            'class_person' => 'Clase de persona',
            'taxpayer_type' => 'Tipo de contribuyente',
            'tax_regime' => 'Régimen contributivo'
        ];
    }
}
