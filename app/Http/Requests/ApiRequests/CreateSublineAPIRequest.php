<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Subline;
use Illuminate\Http\Request;
use InfyOm\Generator\Request\APIRequest;

class CreateSublineAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->line_id;
        return Subline::rulesCreate($id);
    }

    public function attributes(){
        return  [
            'subline_code' => 'Código'
        ];
    }
}
