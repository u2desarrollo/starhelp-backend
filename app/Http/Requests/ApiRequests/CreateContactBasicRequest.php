<?php

namespace App\Http\Requests\ApiRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateContactBasicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscriber_id' => 'required|integer',
            'identification_type' => 'required|integer',
            'identification' => 'required|string|max:15',
            'check_digit' => 'nullable|integer',
            'name' => 'required|string|max:100',
            'surname' => 'nullable|string|max:100',
            'department_id' => 'nullable|integer',
            'city_id' => 'nullable|integer',
            'address' => 'nullable|string|max:50',
            'main_telephone' => 'nullable|string|max:30',
            'cell_phone' => 'nullable|string|max:30',
            'email' => 'nullable|email',
            'is_customer' => 'required',
            'vehicle' => 'array'

        ];
    }
}
