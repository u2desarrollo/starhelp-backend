<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\SellerCoordinate;
use InfyOm\Generator\Request\APIRequest;

class CreateSellerCoordinateAPIRequest extends APIRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return SellerCoordinate::$rules;
    }


    public function messages()
    {
        return [
            'iduser.required' => 'El :attribute es obligatorio.',
            'latitud.required' => 'la :attribute es obligatoria.',
            'longitud.required' => 'la :attribute es obligatoria.'
        ];
    }

    public function attributes()
    {
        return [
        'iduser' => 'id del vendedor',
        'latitud' => 'latitud',
        'longitud' => 'longitud'
    ];
    }

}
