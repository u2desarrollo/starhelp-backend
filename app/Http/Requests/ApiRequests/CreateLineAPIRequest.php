<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Line;
use InfyOm\Generator\Request\APIRequest;

class CreateLineAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Line::$rulesCreate;
    }

    public function attributes(){
        return  [
            'line_code' => 'Código de la linea'
        ];
    }


    
public function messages()
{
    return [
        'line_code.required' => 'El :attribute es obligatorio.',
        'price.unique' => 'El ya se encuentra registrado',
    ];
}

}
