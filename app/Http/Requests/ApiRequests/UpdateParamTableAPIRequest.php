<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\ParamTable;
use InfyOm\Generator\Request\APIRequest;

class UpdateParamTableAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('tablas_parametro');
        return [
            'code_table' => "required|unique:paramtables,code_table,{$id},id,deleted_at,NULL"
        ];
    }

    public function attributes(){
        return [
            'code_table' => 'Código'
        ];
    }
}
