<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\CriteriaPlan;
use InfyOm\Generator\Request\APIRequest;

class UpdateCriteriaPlanAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return CriteriaPlan::$rules;
    }
}
