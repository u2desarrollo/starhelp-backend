<?php

namespace App\Http\Requests\ApiRequests;

use Illuminate\Foundation\Http\FormRequest;

class AvailabilityProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productos'      => 'required|array|min:1',
            'productos.*'    => 'required|int|exists:products,id',
            'clibod_cod_bod' => 'required|int|exists:contacts_warehouses,id'
        ];
    }
}
