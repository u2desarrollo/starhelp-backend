<?php

namespace App\Http\Requests\ApiRequests;

use App\Entities\Parameter;
use InfyOm\Generator\Request\APIRequest;
use Illuminate\Http\Request;

class UpdateParameterAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $paramtable_id = $request->paramtable_id;
        $id = $this->route('parametro');
        return Parameter::rulesUpdate($paramtable_id, $id);
    }

    public function attributes(){
        return  [
            'code_parameter' => 'Código'
        ];
    }
}
