<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use InfyOm\Generator\Request\APIRequest;

class CreateCashReceiptRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iduser'=>'required',
            'formasPago'=>'required',
            'bodega_id'=>'required',
            'facturas'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'iduser.required'=>'El campo iduser es obligatorio',
            'formasPago.required'=>'El campo formasPago es obligatorio',
            'bodega_id.required'=>'El campo bodega_id es obligatorio',
            'facturas.required'=>'El campo facturas es obligatorio'
        ];
    }
}
