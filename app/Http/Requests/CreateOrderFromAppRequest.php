<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderFromAppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_id'                                   => 'required|integer|exists:contacts,id',
            'warehouse_id'                                 => 'required|integer|exists:contacts_warehouses,id',
            'observation'                                  => 'nullable|string|min:5|max:300',
            'documents_products'                           => 'required|array|min:1',
            'documents_products.*.product_id'              => 'required|integer|exists:products,id',
            'documents_products.*.quantity'                => 'required|integer|min:1',
            'documents_products.*.discount_value'          => 'nullable|integer|min:1',
            'documents_products.*.unit_value_before_taxes' => 'nullable|integer|min:1',
        ];
    }
}
