<?php

namespace App\Console;

use App\Entities\Subscriber;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $hour = date('H');
        $dayOfWeek = date('N');
        $minute = (int)date('i');

        $startHour = 7;
        $endHour = 12;

        if ($dayOfWeek < 6) {
            $endHour = 18;
        }

        if ($hour >= $startHour && $hour <= $endHour && $minute > 5 && $minute < 55 && Subscriber::where('identification', '900338016')->exists()) {

            $minute_from_cache = Cache::get('minute');

            if ($minute != $minute_from_cache) {
                Log::info('');
                Cache::put('minute', $minute, 5);
            }

            if (config("app.cron_start")) {
                $schedule->command('validate:dates')->everyFifteenMinutes();
                $schedule->command('sync:colsaisa')->everyTenMinutes();
                $schedule->command('caculate:available')->everyFiveMinutes();
                $schedule->command('execute:aurora')->everyFiveMinutes();
            } else {
                Log::warning('Cron inactivo. Para activarlo, establezca la variable CRON_START en verdadero en el archivo .env');
            }
        }

        $schedule->command('clean:audits')->dailyAt('22:15');
        $schedule->command('db:backup')->dailyAt('01:00');
        $schedule->command('calculate:promotions')->dailyAt('03:00');
        $schedule->command('wms:sendproducts')->everyMinute();
        $schedule->command('wms:senddocuments')->everyMinute();
        $schedule->command('update:balances')->dailyAt('02:00');
        $schedule->command('load:last_sales_for_contact_warehouse')->dailyAt('04:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
