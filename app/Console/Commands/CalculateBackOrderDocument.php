<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentsService;


class CalculateBackOrderDocument extends Command
{
    protected $documentsService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'document:backOrder {document_id?} {--dateMonth=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $documentsService)
    {
        $this->documentsService = $documentsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('document_id')) {
            return $this->documentsService->updateBackOrderAndMissingValueDocument($this->argument('document_id'));
            echo "--- FIN ---";
        }

        $this->documentsService->updateBackOrderAllDocuments([
            'dateMonth' => $this->option('dateMonth'),
        ]);
    }
}
