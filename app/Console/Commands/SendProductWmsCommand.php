<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ProductService;
use Illuminate\Support\Facades\Log;
class SendProductWmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wms:sendproducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command send product wms';
    private $productService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProductService $productService)
    {
        parent::__construct();
        $this->productService = $productService;

    }

    /**
     * Execute the console command.s
     *
     * @return mixed
     */
    public function handle()
    {
        //Log::channel('productslog')->info("Init Consumed WebService WMS Products");
        $this->productService->createSoapWmsMasive();
        //Log::channel('productslog')->info("Final Consumed WebService WMS Products");
    }
}
