<?php

namespace App\Console\Commands;

use App\Repositories\LineRepository;
use App\Repositories\ProductRepository;
use App\Repositories\PromotionRepository;
use App\Services\ProductService;
use Illuminate\Console\Command;
use Illuminate\Container\Container;

class CalculatePromotionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:promotions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lineRepository = new LineRepository(new Container());
        $promotionRepository = new PromotionRepository(new Container());
        $productRepository = new ProductRepository(new Container());


        $productService = new ProductService($lineRepository, $promotionRepository, $productRepository);

        $productService->calculatePromotions();
    }
}
