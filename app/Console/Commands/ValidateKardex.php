<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentsProductsService;

class ValidateKardex extends Command
{
	private $documentsProductsService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:kardex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsProductsService $documentsProductsService)
    {
        parent::__construct();
		$this->documentsProductsService = $documentsProductsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$kardex = $this->documentsProductsService->validateKardex();
    }
}
