<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use Illuminate\Http\Request;
use App\Services\InventoryService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class globalRebuild extends Command
{
    private $inventoryService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:fixGlobalValues {argument1=false} {argument2=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( InventoryService $inventoryService)
    {
        parent::__construct();
        $this->inventoryService = $inventoryService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            $params =new Request();
            
            $params->merge(["date" => $this->argument('argument1') != "false" ? $this->argument('argument1') : null ]);
            $params->merge(["product" => $this->argument('argument2') != "false" ? $this->argument('argument2') : null ]);
    
            $respnse = $this->inventoryService->fixGlobalValues($params);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info('---------------- Error Reconstruccion ----------------');
            Log::info($e);
            Log::info('------------- fin Error Reconstruccion ---------------');
        }

    }
}
