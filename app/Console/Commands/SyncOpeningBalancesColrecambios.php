<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentsService;

class SyncOpeningBalancesColrecambios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:massiveOpeningBalancesColrecambios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $DocumentsService)
    {
        parent::__construct();
        $this->documentsService= $DocumentsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->documentsService->massiveOpeningBalancesColrecambios();
    }
}
