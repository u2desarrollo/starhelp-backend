<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\DocumentsService;
use App\Http\Controllers\ApiControllers\DocumentPDFController;
use App\Entities\Document;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RetainAuthorizedDocuments extends Command
{
    protected $documentsService;
    protected $documentPDFController;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'documento:retener-autorizado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retiene los documentos que han sido autorizados y no se han realizado un proceso con ellos entre ciertos dias habiles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $documentsService, DocumentPDFController $documentPDFController)
    {
        $this->documentsService = $documentsService;
        $this->documentPDFController = $documentPDFController;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            DB::beginTransaction();
            
            $data = $this->documentsService->retainAuthorizedDocuments();
            dd(count($data));

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getFile() . $e->getLine() . $e->getMessage());
        }
        
    }
}
