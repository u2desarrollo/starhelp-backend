<?php

namespace App\Console\Commands;

use App\Entities\Template;
use App\Services\DocumentAccountingService;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReaccountDocumentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:reaccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var DocumentAccountingService
     */
    public $document_accounting_service;

    /**
     * Create a new command instance.
     *
     * @param DocumentAccountingService $document_accounting_service
     */
    public function __construct(DocumentAccountingService $document_accounting_service)
    {
        $this->document_accounting_service = $document_accounting_service;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $models = Template::select('id', 'code')
            ->whereIn('code', ['105', '270', '205', '206', '155', '107'])
            ->get();


        foreach ($models as $model) {
            echo 'Contabilizando documentos del modelo ' . $model->code;

            $request = new Request();

            $parameters = [
                'model_id'        => $model->id,
                'branchoffice_id' => 5,
                'filterDate'      => ['2021-01-01', '2021-08-31']
            ];

            $request->merge($parameters);

            $this->document_accounting_service->accountingInventoryOperations($request);
        }

    }
}
