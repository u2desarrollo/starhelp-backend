<?php

namespace App\Console\Commands;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactEmployee;
use App\Entities\ContactWarehouse;
use App\Entities\Menu;
use App\Entities\Permission;
use App\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MakeMenuColsaisa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:menu-colsaisa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $menu = [
            [
                'description' => "Inicio",
                'url' => "/",
                'icon' => "ti-home",
                'second_level' => []
            ],
            [
                'description' => "Administración Starcommerce",
                'icon' => "fa fa-fw fa-star",
                'second_level' => [
                    [
                        'description' => "Impuestos",
                        'url' => "/impuestos",
                        'icon' => "fa fa-fw fa-briefcase",
                        'permission' => "Impuestos"
                    ],
                    [
                        'description' => "Promociones",
                        'url' => "/promociones",
                        'icon' => "fa fa-fw fa-gift",
                        'permission' => "Promociones"
                    ],
                    [
                        'description' => "Roles y Permisos",
                        'url' => "/roles-permisos",
                        'icon' => "fa fa-fw fa-users",
                        'permission' => "Roles y Permisos"
                    ],
                    [
                        'description' => "Informe Automatico Cartera",
                        'url' => "/envio-cartera",
                        'icon' => "fa fa-fw fa-shopping-bag",
                        'permission' => "Informe Automatico Cartera"
                    ],
                    [
                        'description' => "Licencias",
                        'url' => "/licencias",
                        'icon' => "ti-desktop",
                        'permission' => "Licencias"
                    ],
                    [
                        'description' => "Suscriptores",
                        'url' => "/suscriptores",
                        'icon' => "fa fa-fw fa-building-o",
                        'permission' => "Suscriptores"
                    ],
                    [
                        'description' => "Sedes",
                        'url' => "/sedes",
                        'icon' => "fa fa-fw fa-hospital-o",
                        'permission' => "Sedes"
                    ],
                    [
                        'description' => "Tablas de parámetros",
                        'url' => "/tablas-parametros",
                        'icon' => "ti-info-alt",
                        'permission' => "Tablas de parámetro"
                    ],
                    [
                        'description' => "Tipos de comprobantes",
                        'url' => "/tipos-de-comprobantes",
                        'icon' => "ti-receipt",
                        'permission' => "Tipos de comprobantes"
                    ],
                    [
                        'description' => "Terceros",
                        'url' => "/terceros",
                        'icon' => "fa fa-users",
                        'permission' => "Terceros"
                    ],
                    [
                        'description' => "Lineas",
                        'url' => "/lineas",
                        'icon' => "ti-control-shuffle",
                        'permission' => "Lineas"
                    ],
                    [
                        'description' => "Marcas",
                        'url' => "/marcas",
                        'icon' => "fa fa-fw fa-shield",
                        'permission' => "Marcas"
                    ],
                    [
                        'description' => "Categorias",
                        'url' => "/categorias",
                        'icon' => "ti-layout-list-thumb",
                        'permission' => "Categorias"
                    ],
                    [
                        'description' => "Usuarios",
                        'url' => "/usuarios",
                        'icon' => "fa fa-user",
                        'permission' => "Usuarios"
                    ],
                    [
                        'description' => "Atributos Producto",
                        'url' => "/ficha-de-datos",
                        'icon' => "ti-agenda",
                        'permission' => "Atributos Producto"
                    ],
                    [
                        'description' => "Productos",
                        'url' => "/productos",
                        'icon' => "ti-archive",
                        'permission' => "Productos"
                    ],
                    [
                        'description' => "Configuración menú",
                        'url' => "/configuracion-menu",
                        'icon' => "ti-menu-alt",
                        'permission' => "Configuración menú"
                    ],
                    [
                        'description' => "Mant Lista Precios",
                        'url' => "/precios",
                        'icon' => "ti-money",
                        'permission' => "Mantenimiento Lista Precios"
                    ],
                    [
                        'description' => "Documentos",
                        'url' => "/documentos",
                        'icon' => "ti-agenda",
                        'permission' => "Documentos"
                    ],
                    [
                        'description' => "Tipos de operaciones",
                        'url' => "/operaciones",
                        'icon' => "ti-bar-chart",
                        'permission' => "Tipos de operaciones"
                    ],
                    [
                        'description' => "Modelos",
                        'url' => "/modelos",
                        'icon' => "ti-settings",
                        'permission' => "Modelos"
                    ],
                    [
                        'description' => "Formas de pago",
                        'url' => "/formas-pago",
                        'icon' => "ti-money",
                        'permission' => "Formas de pago"
                    ],
                    [
                        'description' => "Solicitudes de servicio",
                        'url' => "/solicitud-encabezado",
                        'icon' => "ti-settings",
                        'permission' => "Solicitudes de servicio"
                    ],
                    [
                        'description' => "Facturacion",
                        'url' => "/documentos-inventario/1",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Despachos",
                        'url' => "/despachos",
                        'icon' => "ti-package",
                        'permission' => "Despachos"
                    ],
                    [
                        'description' => "Pedidos Retenidos",
                        'url' => "/pedidos-retenidos",
                        'icon' => "ti-na",
                        'permission' => "Pedidos Retenidos"
                    ],
                    [
                        'description' => "Sugerido de Compra",
                        'url' => "/documentos-inventario/16",
                        'icon' => "ti-file",
                        'permission' => "Solicitudes Ordenes de compra"
                    ],
                    [
                        'description' => "Facturacion Directa",
                        'url' => "/documentos-inventario/5",
                        'icon' => "ti-file",
                        'permission' => "Facturacion Directa"
                    ],
                    [
                        'description' => "Ordenes de compra",
                        'url' => "/documentos-inventario/23",
                        'icon' => "ti-file",
                        'permission' => "Ordenes de compra"
                    ],
                    [
                        'description' => "Entradas por compras",
                        'url' => "/documentos-inventario/17",
                        'icon' => "ti-file",
                        'permission' => "Entradas por compras"
                    ],
                    [
                        'description' => "Devolucion de venta",
                        'url' => "/documentos-inventario/21",
                        'icon' => "ti-file",
                        'permission' => "Devolucion de venta"
                    ],
                    [
                        'description' => "Devolucion de compra",
                        'url' => "/documentos-inventario/22",
                        'icon' => "ti-file",
                        'permission' => "Devolucion de compra"
                    ],
                    [
                        'description' => "Entrada saldos iniciales",
                        'url' => "/documentos-inventario/3",
                        'icon' => "ti-file",
                        'permission' => "Entrada saldos iniciales"
                    ],
                    [
                        'description' => "Ingreso de chatarra",
                        'url' => "/documentos-inventario/24",
                        'icon' => "ti-file",
                        'permission' => "Ingreso de chatarra"
                    ],
                    [
                        'description' => "Borrar erp",
                        'url' => "/borrar-erp",
                        'icon' => "ti-exchange-vertical",
                        'permission' => "BorrarErp"
                    ],
                    [
                        'description' => "Traslados",
                        'url' => "/traslado",
                        'icon' => "ti-exchange-vertical",
                        'permission' => "Traslados"
                    ],
                    [
                        'description' => "Comprobantes Varios",
                        'url' => "/comprobantes",
                        'icon' => "ti-exchange-vertical",
                        'permission' => "Traslados"
                    ],
                    [
                        'description' => "Plan de Cuentas",
                        'url' => "/cuentas",
                        'icon' => "ti-agenda",
                        'permission' => "PlanCuentas"
                        // 'permission'=> "Cuentas",
                    ],
                    [
                        'description' => "Aurora",
                        'url' => "/Aurora",
                        'icon' => "ti-timer",
                        'permission' => "aurora"
                    ],
                    [
                        'description' => "Traslados Rotacion, Garantías",
                        'url' => "/traslado-rot",
                        'icon' => "ti-exchange-vertical",
                        'permission' => "Traslados"
                    ],
                    [
                        'description' => "Kardex",
                        'url' => "/kardex",
                        'icon' => "ti-exchange-vertical",
                        'permission' => "Kardex"
                    ],
                    [
                        'description' => "Descuentos",
                        'url' => "/Descuentos",
                        'icon' => "ti-angle-down",
                        'permission' => "Descuentos"
                    ],
                    [
                        'description' => "Descuentos pronto pago",
                        'url' => "/Descuentos-Pronto-Pago",
                        'icon' => "ti-angle-down",
                        'permission' => "Descuentos pronto pago"
                    ],
                    [
                        'description' => "Informe Operaciones Rango Fecha",
                        'url' => "/Informe-Operaciones-Rango-Fecha",
                        'icon' => "ti-align-justify",
                        'permission' => "Informe Operaciones Rango Fecha"
                    ],
                    [
                        'description' => "Consulta Disponibilidad",
                        'url' => "/consulta-disponibilidad",
                        'icon' => "ti-align-justify",
                        'permission' => "Consulta Disponibilidad"
                    ],
                    [
                        'description' => "Reconstructor",
                        'url' => "/reconstructor-global",
                        'icon' => "ti-align-justify",
                        'permission' => "Reconstructor"
                    ],

                    [
                        'description' => "Salida Por Ajuste",
                        'url' => "/documentos-inventario/37",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Entrada Por Ajuste",
                        'url' => "/documentos-inventario/36",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Facturacion Ibague (Desde Bogota)",
                        'url' => "/documentos-inventario/38",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Facturacion Duitama (Desde Bogota)",
                        'url' => "/documentos-inventario/39",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Salida Muestra Comercial",
                        'url' => "/documentos-inventario/42",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Devol Facturacion Ibague (Desde Bogota)",
                        'url' => "/documentos-inventario/40",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Devol Facturacion Duitama (Desde Bogota)",
                        'url' => "/documentos-inventario/41",
                        'icon' => "ti-file",
                        'permission' => "Facturacion"
                    ],
                    [
                        'description' => "Devoluciones Anteriores U2",
                        'url' => "/documentos-inventario/4",
                        'icon' => "ti-file",
                        'permission' => "Devoluciones Anteriores U2"
                    ],
                    [
                        'description' => "Devol Fact Anterior Ibague (Desde BTA)",
                        'url' => "/documentos-inventario/44",
                        'icon' => "ti-file",
                        'permission' => "Devol Fact Anterior Ibague (Desde BTA)"
                    ],
                    [
                        'description' => "Devol Fact Anterior Duitama (Desde BTA)",
                        'url' => "/documentos-inventario/45",
                        'icon' => "ti-file",
                        'permission' => "Devol Fact Anterior Duitama (Desde BTA)"
                    ],
                    [
                        'description' => "Compra Chatarra (efe)",
                        'url' => "/documentos-inventario/27",
                        'icon' => "ti-file",
                        'permission' => "Compra Chatarra (efe)"
                    ],
                    [
                        'description' => "Compra Chatarra Cruce",
                        'url' => "/documentos-inventario/25",
                        'icon' => "ti-file",
                        'permission' => "Compra Chatarra Cruce"
                    ],
                    [
                        'description' => "Buscar Documentos",
                        'url' => "/buscar-documentos",
                        'icon' => "ti-search",
                        'permission' => "Buscar Documentos"
                    ],
                    [
                        'description' => "Informe Terceros",
                        'url' => "/informe-usuarios",
                        'icon' => "fa  fa-address-book-o",
                        'permission' => "Informe Terceros"
                    ],
                    [
                        'description' => "Factura Exportacion",
                        'url' => "/documentos-inventario/46",
                        'icon' => "ti-file",
                        'permission' => "Factura Exportacion"
                    ],
                    [
                        'description' => "Entrada por Importacion",
                        'url' => "/documentos-inventario/50",
                        'icon' => "ti-file",
                        'permission' => "Entradas por compras"
                    ],
                    [
                        'description' => "Informe de documentos autorizados",
                        'url' => "/autorizacion-de-documentos",
                        'icon' => "ti-file",
                        'permission' => "Informe de documentos autorizados"
                    ],
                    [
                        'description' => "Salida para Chatarra (Tras)",
                        'url' => "/documentos-inventario/52",
                        'icon' => "ti-file",
                        'permission' => "Salida de Chatarra (Tras)"
                    ],
                    [
                        'description' => "Entrada de Chatarra (Tras)",
                        'url' => "/documentos-inventario/53",
                        'icon' => "ti-file",
                        'permission' => "Entrada de Chatarra (Tras)"
                    ],
                    [
                        'description' => "Facturacion Siniestros",
                        'url' => "/documentos-inventario/54",
                        'icon' => "ti-file",
                        'permission' => "Facturacion Siniestros"
                    ],
                    [
                        'description' => "Devolucion En Compras Ant U2",
                        'url' => "/documentos-inventario/55",
                        'icon' => "ti-file",
                        'permission' => "Devolucion En Compras Ant U2"
                    ],
                    [
                        'description' => "Estado Cuenta",
                        'url' => "/estado-cuenta",
                        'icon' => "ti-file",
                        'permission' => "Estado Cuenta"
                    ],
                    [
                        'description' => "Salida Consumo Interno",
                        'url' => "/documentos-inventario/56",
                        'icon' => "ti-file",
                        'permission' => "Salida Consumo Interno"
                    ],
                    [
                        'description' => "Salida Servicio Tecnico/Cliente",
                        'url' => "/documentos-inventario/59",
                        'icon' => "ti-file",
                        'permission' => "Salida Servicio Tecnico/Cliente"
                    ],
                    [
                        'description' => "Facturacion b/manga (desde bogota)",
                        'url' => "/documentos-inventario/60",
                        'icon' => "ti-file",
                        'permission' => "Facturacion b/manga (desde bogota)"
                    ],
                    [
                        'description' => "Devol ventas b/manga (desde bogota)",
                        'url' => "/documentos-inventario/61",
                        'icon' => "ti-file",
                        'permission' => "Devol ventas b/manga (desde) bogota)"
                    ],
                    [
                        'description' => "Devol nota credito (valores)",
                        'url' => "/documentos-inventario/64",
                        'icon' => "ti-file",
                        'permission' => "Devol nota credito (valores)"
                    ],
                    [
                        'description' => "Reasignación Vendedor",
                        'url' => "/Reasignacion-Vendedor",
                        'icon' => "fa fa-pencil",
                        'permission' => "Reasignacion Vendedor"
                    ],
                    [
                        'description' => "FACTURACION V/CENCIO (DESDE BOGOTA)",
                        'url' => "/documentos-inventario/65",
                        'icon' => "ti-file",
                        'permission' => "FACTURACION V/CENCIO (DESDE BOGOTA)"
                    ],
                    [
                        'description' => "DEVOL FACT V/CENCIO  (DESDE BTA)",
                        'url' => "/documentos-inventario/66",
                        'icon' => "ti-file",
                        'permission' => "DEVOL FACT V/CENCIO  (DESDE BTA)"
                    ],
                    [
                        'description' => "NOTA CREDITO IBAGUE DESDE BOGOTA",
                        'url' => "/documentos-inventario/67",
                        'icon' => "ti-file",
                        'permission' => "NOTA CREDITO IBAGUE DESDE BOGOTA"
                    ],
                    [
                        'description' => "NOTA CREDITO DUITAMA DESDE BOGOTA",
                        'url' => "/documentos-inventario/69",
                        'icon' => "ti-file",
                        'permission' => "NOTA CREDITO DUITAMA DESDE BOGOTA"
                    ],
                    [
                        'description' => "NOTA CREDITO V/CENCIO DESDE BOGOTA",
                        'url' => "/documentos-inventario/70",
                        'icon' => "ti-file",
                        'permission' => "NOTA CREDITO V/CENCIO DESDE BOGOTA"
                    ],
                    [
                        'description' => "NOTA CREDITO B/MANGA DESDE BOGOTA",
                        'url' => "/documentos-inventario/71",
                        'icon' => "ti-file",
                        'permission' => "NOTA CREDITO B/MANGA DESDE BOGOTA"
                    ],
                    [
                        'description' => "DEVOL FACT ANTERIOR V/CENCIO DESDE BOG",
                        'url' => "/documentos-inventario/72",
                        'icon' => "ti-file",
                        'permission' => "DEVOL FACT ANTERIOR V/CENCIO DESDE BOG"
                    ],
                    [
                        'description' => "DEVOL FACT ANTERIOR B/MANGA DESDE BOG",
                        'url' => "/documentos-inventario/73",
                        'icon' => "ti-file",
                        'permission' => "DEVOL FACT ANTERIOR B/MANGA DESDE BOG"
                    ],
                    [
                        'description' => "NOTA CREDITO COMPRAS VRS",
                        'url' => "/documentos-inventario/74",
                        'icon' => "ti-file",
                        'permission' => "NOTA CREDITO COMPRAS VRS"
                    ],
                    [
                        'description' => "Envío de documentos finalizados",
                        'url' => "/envio-de-documentos-finalizados",
                        'icon' => "ti-send",
                        'permission' => "Envío de documentos finalizados"
                    ],
                    [
                        'description' => "SALIDA POR DISPOSICION",
                        'url' => "/documentos-inventario/83",
                        'icon' => "ti-file",
                        'permission' => "SALIDA POR DISPOSICION"
                    ],
                    [
                        'description' => "NOTA DEBITO",
                        'url' => "/documentos-inventario/78",
                        'icon' => "ti-file",
                        'permission' => "NOTA DEBITO"
                    ],
                    [
                        'description' => "FACT PEDIDOS CONTINGENCIA",
                        'url' => "/documentos-inventario/76",
                        'icon' => "ti-file",
                        'permission' => "FACT PEDIDOS CONTINGENCIA"
                    ],
                    [
                        'description' => "FACT DIRECTA CONTINGENCIA",
                        'url' => "/documentos-inventario/77",
                        'icon' => "ti-file",
                        'permission' => "FACT DIRECTA CONTINGENCIA"
                    ],
                    [
                        'description' => "FACT CONTINGENCIA IBAGUE DESDE BOGOTA",
                        'url' => "/documentos-inventario/79",
                        'icon' => "ti-file",
                        'permission' => "FACT CONTINGENCIA IBAGUE DESDE BOGOTA"
                    ],
                    [
                        'description' => "FACT CONTINGENCIA DUITAMA DESDE BOGOTA",
                        'url' => "/documentos-inventario/80",
                        'icon' => "ti-file",
                        'permission' => "FACT CONTINGENCIA DUITAMA DESDE BOGOTA"
                    ],
                    [
                        'description' => "FACT CONTINGENCIA B/MANGA DESDE BOGOTA",
                        'url' => "/documentos-inventario/81",
                        'icon' => "ti-file",
                        'permission' => "FACT CONTINGENCIA B/MANGA DESDE BOGOTA"
                    ],
                    [
                        'description' => "FACT CONTINGENCIA V/CENCIO DESDE BOGOTA",
                        'url' => "/documentos-inventario/82",
                        'icon' => "ti-file",
                        'permission' => "FACT CONTINGENCIA V/CENCIO DESDE BOGOTA"
                    ],
                    [
                        'description' => "DEVOL FACTURACION SINIESTRO",
                        'url' => "/documentos-inventario/85",
                        'icon' => "ti-file",
                        'permission' => "DEVOL FACTURACION SINIESTRO"
                    ],
                    [
                        'description' => "Analytrix Ventas Ultimo Semestre",
                        'url' => "/reporte",
                        'icon' => "ti-file",
                        'permission' => "Analytrix Ventas Ultimo Semestre"
                    ],
                    [
                        'description' => "Cierre mensual",
                        'url' => "/cierre-mensual",
                        'icon' => "ti-check-box"
                    ],
                    [
                        'description' => "SALIDA POR DETERIORO",
                        'url' => "/documentos-inventario/84",
                        'icon' => "ti-file",
                        'permission' => "SALIDA POR DETERIORO"
                    ],
                    [
                        'description' => "CUADRE DE CAJA",
                        'url' => "/cuadre-de-caja",
                        'icon' => "ti-file",
                        'permission' => "CUADRE DE CAJA"
                    ]
                ]
            ],
            [
                'description' => "B2B",
                'url' => "",
                'icon' => "ti-shopping-cart-full",
                'second_level' => [
                    [
                        'description' => "Banners",
                        'url' => "/banners",
                        'icon' => "ti-image",
                        'permission' => "Banners"
                    ],
                    [
                        'description' => "Portal clientes",
                        'url' => "/ecommerce",
                        'icon' => "ti-layout",
                        'permission' => "Portal clientes"
                    ],
                    [
                        'description' => "Formulario Cliente Prospecto",
                        'url' => "/cliente-prospecto",
                        'icon' => "ti-pencil-alt",
                        'permission' => "Formulario Cliente Prospecto"
                    ],
                    [
                        'description' => "Gestion De Cobranza",
                        'url' => "/gestion-de-cobranzas",
                        'icon' => "ti-eye",
                        'permission' => "Gestion De Cobranza"
                    ],
                    [
                        'description' => "Gestión de pedidos",
                        'url' => "/b2b/pedidos",
                        'icon' => "ti-eye",
                        'permission' => "Gestión de pedidos"
                    ],
                    [
                        'description' => "Recibos de caja",
                        'url' => "/b2b/promociones",
                        'icon' => "ti-eye",
                        'permission' => "Recibos de caja"
                    ],
                    [
                        'description' => "Ruta vendedor",
                        'url' => "/ruta-vendedor",
                        'icon' => "ti-map-alt",
                        'permission' => "Ruta vendedor"
                    ],
                    [
                        'description' => "Visitas vendedor",
                        'url' => "/seguimiento-vendedores",
                        'icon' => "ti-location-pin",
                        'permission' => "Visitas vendedor"
                    ]
                ]
            ],
            [
                'description' => "Informes",
                'url' => "",
                'icon' => "ti-layout-grid4",
                'second_level' => [
                    [
                        'description' => "Inventario valorizado",
                        'url' => "/informe-inventario",
                        'icon' => "ti-money",
                        'permission' => "Inventario valorizado"
                    ],
                    [
                        'description' => "Sugerido de Reposición",
                        'url' => "/sugerido-de-reposicion",
                        'icon' => "ti-reload",
                        'permission' => "Sugerido de Reposición"
                    ],
                    [
                        'description' => "Business Intelligence",
                        'url' => "/business_intelligence",
                        'icon' => "ti-bar-chart-alt",
                        'permission' => "Business Intelligence"
                    ]
                ]
            ]
        ];

        try {
            DB::beginTransaction();

            DB::select('TRUNCATE TABLE menus RESTART IDENTITY;');

            $order_first_level = 1;

            foreach ($menu as $first_level) {
                $first_level['order'] = $order_first_level++;
                $first_level['level'] = 1;
                $first_level['module_id'] = 3;

                if (isset($first_level['permission'])) {
                    $permission_id = $this->getPermission($first_level['permission']);
                    $first_level['permission_id'] = $permission_id;
                }

                $first_level_create = Menu::create($first_level);

                if (isset($first_level['second_level'])) {

                    $order_second_level = 1;
                    foreach ($first_level['second_level'] as $second_level) {
                        $second_level['order'] = $order_second_level++;
                        $second_level['menu_id'] = $first_level_create->id;
                        $second_level['level'] = 2;
                        $second_level['module_id'] = 3;

                        if (isset($second_level['permission'])) {
                            $permission_id = $this->getPermission($second_level['permission']);
                            $second_level['permission_id'] = $permission_id;
                        }
                        Menu::create($second_level);
                    }
                }
            }

            DB::commit();
            
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();
        }
    }

    public function getPermission($permission)
    {
        $p = Permission::where('permission', $permission)->first();

        if ($p){
            return $p->id;
        }

        return null;
    }
}
