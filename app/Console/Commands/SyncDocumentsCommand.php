<?php

namespace App\Console\Commands;

use App\Entities\Account;
use App\Entities\DocumentsBalance;
use App\Services\ServiceToCharge;
use App\Services\SyncDocumentsService;
use App\Services\SyncTransactionsService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SyncDocumentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $service_to_charge = new SyncDocumentsService();

        if (config('app.test_cron')){
            $service_to_charge->syncData(null);
        }else {

            $filesBalance = collect();

            // ******* posibilidad de cambio    ****
            $sftp = Storage::disk('sftp');

            $namesFiles = $sftp->files('WEB/PENDING');

            //decido a que Array deben de ir cada nombre de archivo dependiendo del mismo
            foreach ($namesFiles as $key => $value) {
                $part = explode('/', $value);
                if (substr($part[count($part) - 1], 0, 6) == 'CARERP') {
                    $filesBalance->push($part[count($part) - 1]);
                }
            }

            Log::info('Archivos encontrados de catera en el servidor SFTP' . json_encode($filesBalance->toArray()));

            // carga de tablas documents y documents_balance
            foreach ($filesBalance as $key => $i) {
                $file = $sftp->get('WEB/PENDING/' . $i);
                Storage::disk('local')->put('public/portfolio/' . $i, $file);
                $sftp->delete('WEB/PENDING/' . $i);
            }

            //Cierra lo conexión sftp
            $sftp->getDriver()->getAdapter()->disconnect();


            foreach ($filesBalance as $key => $v) {
                try {
                    DB::beginTransaction();
                    $service_to_charge->syncData($v);
                    DB::commit();
                } catch (\Exception $th) {
                    Log::error($th);
                    DB::rollBack();
                }
            }

            Log::info('Sincronización de cartera finalizado con exito');
        }

    }
}
