<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentsService;

class DeleteNotPreviousDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:notPreviousDocuments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $documentsService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $documentsService)
    {
        parent::__construct();
        $this->documentsService = $documentsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd($this->documentsService->deleteNotPreviousDocuments());
    }
}
