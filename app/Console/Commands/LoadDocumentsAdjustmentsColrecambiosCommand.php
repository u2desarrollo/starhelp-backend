<?php

namespace App\Console\Commands;

use App\Entities\Document;
use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\TypesOperation;
use App\Entities\VouchersType;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadDocumentsAdjustmentsColrecambiosCommand extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:adjustments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     * @author Jhon García
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('Ajustes de inventario');
        $validator = true;
        $products_not_found = [];
        $contacts_not_found = [];
        $contacts_warehouses_not_found = [];

        $row = 2;

        DB::beginTransaction();

        while ($validator) {

            // Obtiene el número de identificación del tercero
            $identification = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($identification)) {

                $product_code = trim($this->sheet->getCell('A' . $row)->getValue());

                if ($product_code) {

                    $quantity = trim($this->sheet->getCell('H' . $row)->getValue());

                    $model_id = Template::where('code', '150')->first()->id;
                    $operationType_id = TypesOperation::where('code', '150')->first()->id;
                    $vouchertype_id = VouchersType::where('code_voucher_type', '150')->first()->id;

                    $adjustment_type = strtolower(trim($this->sheet->getCell('M' . $row)->getValue()));

                    $output = strpos($adjustment_type, 'salida');

                    if ($quantity < 0 || (is_int($output) && $output >= 0)) {
                        $model_id = Template::where('code', '260')->first()->id;
                        $operationType_id = TypesOperation::where('code', '260')->first()->id;
                        $vouchertype_id = VouchersType::where('code_voucher_type', '260')->first()->id;
                    }

                    $document_date_from_excel = trim($this->sheet->getCell('C' . $row)->getValue());

                    if (is_numeric($document_date_from_excel)){
                        $document_date_as_string = Date::excelToDateTimeObject($document_date_from_excel)->format('Y-m-d H:i:s');
                    }else{
                        $document_date_as_string = $document_date_from_excel;
                    }

                    $document_date = Carbon::parse($document_date_as_string);

                    $data_document = [
                        'prefix'                    => trim($this->sheet->getCell('D' . $row)->getValue()),
                        'consecutive'               => trim($this->sheet->getCell('E' . $row)->getValue()),
                        'branchoffice_id'           => 5,
                        'branchoffice_warehouse_id' => 1,
                        'vouchertype_id'            => $vouchertype_id,
                        'model_id'                  => $model_id,
                        'operationType_id'          => $operationType_id,
                        'issue_date'                => $document_date->format('Y-m-d'),
                        'document_date'             => $document_date->format('Y-m-d H:i:s'),
                        'document_last_state'       => $document_date->format('Y-m-d'),
                        'due_date'                  => $document_date->format('Y-m-d'),
                        'contact_id'                => 389,
                        'warehouse_id'              => 1524,
                        'seller_id'                 => null,
                        'user_id'                   => 44,
                        'observation'               => trim($this->sheet->getCell('N' . $row)->getValue()),
                        'in_progress'               => false,
                        'origin_document'           => true
                    ];

                    $document = $this->getDocument($data_document);

                    $this->createDocumentInformation($document);

                    $product = Product::where('previous_code_system', $product_code)->first();

                    if ($product) {
                        $unit_value_before_taxes = abs(trim($this->sheet->getCell('I' . $row)->getValue()));
                        $total_value_brut = abs(trim($this->sheet->getCell('K' . $row)->getValue()));
                        $total_value = $total_value_brut;

                        $data_document_product = [
                            'document_id'               => $document->id,
                            'product_id'                => $product->id,
                            'line_id'                   => $product->line_id,
                            'brand_id'                  => $product->brand_id,
                            'seller_id'                 => null,
                            'description'               => $product->description,
                            'manage_inventory'          => $product->manage_inventory,
                            'branchoffice_warehouse_id' => 1,
                            'operationType_id'          => $operationType_id,
                            'quantity'                  => abs($quantity),
                            'unit_value_before_taxes'   => $unit_value_before_taxes,
                            'discount_value'            => 0,
                            'total_value_brut'          => abs($total_value_brut),
                            'total_value'               => abs($total_value)
                        ];

                        DocumentProduct::create($data_document_product);

                        $document->thread = $document->id;
                        $document->affects_document_id = $document->id;
                        $document->affects_prefix_document = $document->prefix;
                        $document->affects_number_document = $document->consecutive;
                        $document->affects_date_document = $document->document_date;

                        $document->total_value_brut += $total_value_brut;
                        $document->total_value += $total_value;

                        $document->save();
                    }
                }
            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        Log::info('// ------------------------------------- Adjustments');
        if (count($contacts_not_found) || count($contacts_warehouses_not_found) || count($products_not_found)) {
            Log::info(collect($contacts_not_found)->unique()->toArray());
            Log::info(collect($contacts_warehouses_not_found)->unique()->toArray());
            Log::info(collect($products_not_found)->unique()->toArray());
        }

        DB::commit();
    }
}
