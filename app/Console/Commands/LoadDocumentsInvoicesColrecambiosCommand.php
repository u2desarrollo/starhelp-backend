<?php

namespace App\Console\Commands;

use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\TypesOperation;
use App\Entities\VouchersType;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadDocumentsInvoicesColrecambiosCommand extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     * @author Jhon García
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('Facturas');

        $validator = true;
        $products_not_found = [];
        $contacts_not_found = [];
        $contacts_warehouses_not_found = [];

        $row = 2;

        DB::beginTransaction();

        $voucher_type_id = VouchersType::where('code_voucher_type', '205')->first()->id;
        $model_id = Template::where('code', '205')->first()->id;
        $operation_type_id = TypesOperation::where('code', '205')->first()->id;

        while ($validator) {

            // Obtiene el número de identificación del tercero
            $identification = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($identification)) {

                // Consultar tercero por numero de identificación
                $contact = $this->getContact($identification);

                if (!$contact) {
                    array_push($contacts_not_found, $identification);
                } else {

                    $contact_warehouse = $this->getContactWarehouse($contact->id);

                    if (!$contact_warehouse) {
                        array_push($contacts_warehouses_not_found, $identification);
                    } else {

                        // Obtiene el número de identificación del vendedor
                        $identification_seller = trim($this->sheet->getCell('J' . $row)->getValue());

                        // Consultar vendedor por numero de identificación
                        $seller = $this->getSeller($identification_seller, trim($this->sheet->getCell('K' . $row)->getValue()));

                        $document_date_from_excel = trim($this->sheet->getCell('D' . $row)->getValue());

                        if (is_numeric($document_date_from_excel)){
                            $document_date_as_string = Date::excelToDateTimeObject($document_date_from_excel)->format('Y-m-d H:i:s');
                        }else{
                            $document_date_as_string = $document_date_from_excel;
                        }

                        $document_date = Carbon::parse($document_date_as_string);

                        $due_date_from_excel = trim($this->sheet->getCell('H' . $row)->getValue());

                        if (is_numeric($due_date_from_excel)){
                            $due_date_as_string = Date::excelToDateTimeObject($due_date_from_excel)->format('Y-m-d');
                        }else{
                            $due_date_as_string = $due_date_from_excel;
                        }

                        $due_date = Carbon::parse($due_date_as_string);

                        $data_document = [
                            'prefix'                    => trim($this->sheet->getCell('E' . $row)->getValue()),
                            'consecutive'               => trim($this->sheet->getCell('F' . $row)->getValue()),
                            'branchoffice_id'           => 5,
                            'branchoffice_warehouse_id' => 1,
                            'vouchertype_id'            => $voucher_type_id,
                            'model_id'                  => $model_id,
                            'operationType_id'          => $operation_type_id,
                            'issue_date'                => $document_date->format('Y-m-d'),
                            'document_date'             => $document_date->format('Y-m-d H:i:s'),
                            'document_last_state'       => $document_date->format('Y-m-d'),
                            'due_date'                  => $due_date->format('Y-m-d'),
                            'contact_id'                => $contact->id,
                            'warehouse_id'              => $contact_warehouse->id,
                            'seller_id'                 => $seller->id,
                            'user_id'                   => 44,
                            'observation'               => trim($this->sheet->getCell('I' . $row)->getValue()),
                            'in_progress'               => false,
                            'origin_document'           => true
                        ];

                        $document = $this->getDocument($data_document);

                        $this->createDocumentInformation($document);

                        $product_code = trim($this->sheet->getCell('L' . $row)->getValue());

                        $product = Product::where('previous_code_system', $product_code)->first();

                        if (!$product) {
                            array_push($products_not_found, $product_code);
                        } else {

                            $unit_value_before_taxes = trim($this->sheet->getCell('O' . $row)->getValue());
                            $total_value_brut = trim($this->sheet->getCell('Q' . $row)->getValue());
                            $iva_value = trim($this->sheet->getCell('R' . $row)->getValue());
                            $total_value = $total_value_brut + $iva_value;

                            $data_document_product = [
                                'document_id'               => $document->id,
                                'product_id'                => $product->id,
                                'description'               => $product->description,
                                'line_id'                   => $product->line_id,
                                'brand_id'                  => $product->brand_id,
                                'seller_id'                 => $seller->id,
                                'manage_inventory'          => $product->manage_inventory,
                                'branchoffice_warehouse_id' => 1,
                                'operationType_id'          => $operation_type_id,
                                'quantity'                  => abs(trim($this->sheet->getCell('M' . $row)->getValue())),
                                'unit_value_before_taxes'   => $unit_value_before_taxes,
                                'discount_value'            => trim($this->sheet->getCell('P' . $row)->getValue()),
                                'total_value_brut'          => $total_value_brut,
                                'total_value'               => $total_value
                            ];

                            $document_product = DocumentProduct::create($data_document_product);

                            DocumentProductTax::create([
                                'document_product_id' => $document_product->id,
                                'tax_id'              => 1,
                                'base_value'          => $total_value_brut,
                                'tax_percentage'      => 19,
                                'value'               => $iva_value
                            ]);

                            $document->thread = $document->id;
                            $document->affects_document_id = $document->id;
                            $document->affects_prefix_document = $document->prefix;
                            $document->affects_number_document = $document->consecutive;
                            $document->affects_date_document = $document->document_date;

                            $document->total_value_brut += $total_value_brut;
                            $document->total_value += $total_value;

                            $document->save();
                        }
                    }
                }
            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        Log::info('// ------------------------------------- Invoices');
        if (count($contacts_not_found) || count($contacts_warehouses_not_found) || count($products_not_found)) {
            Log::info(collect($contacts_not_found)->unique()->toArray());
            Log::info(collect($contacts_warehouses_not_found)->unique()->toArray());
            Log::info(collect($products_not_found)->unique()->toArray());
        }

        DB::commit();
    }
}
