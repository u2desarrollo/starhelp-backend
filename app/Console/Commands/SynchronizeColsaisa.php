<?php

namespace App\Console\Commands;

use App\Repositories\ContactWarehouseRepository;
use App\Repositories\DocumentRepository;
use App\Services\Cartera;
use App\Services\CarteraService;
use App\Services\CashReceiptsAppliedService;
use App\Services\CashReceiptsService;
use App\Services\InventaryOperationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SynchronizeColsaisa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:colsaisa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var ContactWarehouseRepository
     */
    private $ContactWarehouseRepository;

    /**
     * Create a new command instance.
     *
     * @param DocumentRepository $documentRepository
     * @param ContactWarehouseRepository $ContactWarehouseRepository
     */
    public function __construct(DocumentRepository $documentRepository, ContactWarehouseRepository $ContactWarehouseRepository)
    {
        parent::__construct();
        $this->documentRepository = $documentRepository;
        $this->ContactWarehouseRepository = $ContactWarehouseRepository;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(600);

        $lockSync = Cache::get('lockSyncColsaisa', false);

        if (!$lockSync) {
            Cache::put('lockSyncColsaisa', true, 60);
            Log::info('//--------------------------- CRON SYNC COLSAISA RUNNING --------------------------- \\\\');
            $invoiceService = new CarteraService();
            $invoiceService->InitPersistDDBB();

            // recibos de caja app
            $flatCashReceiptsService = new CashReceiptsService();
            $flatCashReceiptsService->createFlatCashReceipts();

            // actualizacion de erp_id recibos de caja
            $cashReceiptsApp = new CashReceiptsAppliedService();
            $cashReceiptsApp->InitPersistDDBB();

            // operaciones de inventario
            $flatfile = new CashReceiptsService();
            $flatfile->createFlat($this->documentRepository->getInfoForFile(), $this->ContactWarehouseRepository->getInfoForFile(), $this->documentRepository->gettransfersForFile());

            //  actualizacion  erp_id  inventario
            $a = new InventaryOperationService();
            $a->readFile();

            Cache::put('lockSyncColsaisa', false, 60);
            Cache::put('lastDateSyncAll', now()->format('Y-m-d H:i:s'), 3600);
        }
    }
}
