<?php

namespace App\Console\Commands;

use App\Services\BalancesService;
use Illuminate\Console\Command;

class LoadDocumentUpdateBalancesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:update_balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var BalancesService
     */
    public $balances_service;

    /**
     * Create a new command instance.
     *
     * @param BalancesService $balances_service
     */
    public function __construct(BalancesService $balances_service)
    {
        $this->balances_service = $balances_service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->balances_service->updateBalances('2018-01-01', null, false, null);
    }
}
