<?php

namespace App\Console\Commands;

use App\Entities\DocumentProduct;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\TypesOperation;
use App\Entities\VouchersType;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadDocumentsTransfersColrecambiosCommand extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:transfers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     * @author Jhon García
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('Traslados');
        $validator = true;
        $products_not_found = [];
        $contacts_not_found = [];
        $contacts_warehouses_not_found = [];

        $row = 2;

        DB::beginTransaction();

        $vouchertype_id = VouchersType::where('code_voucher_type', '100')->first()->id;
        $output_model_id = Template::where('code', '240')->first()->id;
        $output_operation_type_id = TypesOperation::where('code', '240')->first()->id;
        $input_model_id = Template::where('code', '140')->first()->id;
        $input_operation_type_id = TypesOperation::where('code', '140')->first()->id;

        while ($validator) {

            $product_code = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($product_code)) {

                $document_date_from_excel = trim($this->sheet->getCell('E' . $row)->getValue());

                if (is_numeric($document_date_from_excel)){
                    $document_date_as_string = Date::excelToDateTimeObject($document_date_from_excel)->format('Y-m-d H:i:s');
                }else{
                    $document_date_as_string = $document_date_from_excel;
                }

                $document_date = Carbon::parse($document_date_as_string);

                $branchoffice_warehouse = strtolower(trim($this->sheet->getCell('L' . $row)->getValue()));

                $origin_branchoffice_warehouse_id = 1;
                $destination_branchoffice_warehouse_id = 2;

                $not_avaliable = strpos($branchoffice_warehouse, 'no disponible');

                if (is_int($not_avaliable) && $not_avaliable >= 0){
                    $origin_branchoffice_warehouse_id = 2;
                    $destination_branchoffice_warehouse_id = 1;
                }

                $data_document = [
                    'prefix'                    => trim($this->sheet->getCell('C' . $row)->getValue()),
                    'consecutive'               => trim($this->sheet->getCell('D' . $row)->getValue()),
                    'branchoffice_id'           => 5,
                    'branchoffice_warehouse_id' => $origin_branchoffice_warehouse_id,
                    'vouchertype_id'            => $vouchertype_id,
                    'model_id'                  => $output_model_id,
                    'operationType_id'          => $output_operation_type_id,
                    'issue_date'                => $document_date->format('Y-m-d'),
                    'document_date'             => $document_date->format('Y-m-d H:i:s'),
                    'document_last_state'       => $document_date->format('Y-m-d'),
                    'due_date'                  => $document_date->format('Y-m-d'),
                    'contact_id'                => 389,
                    'warehouse_id'              => 1524,
                    'seller_id'                 => null,
                    'user_id'                   => 44,
                    'observation'               => trim($this->sheet->getCell('N' . $row)->getValue()),
                    'in_progress'               => false,
                    'origin_document'           => true
                ];

                $document = $this->getDocument($data_document);

                $this->createDocumentInformation($document);

                $product = Product::where('previous_code_system', $product_code)->first();

                if (!$product) {
                    array_push($products_not_found, $product_code);
                } else {

                    $unit_value_before_taxes = abs(trim($this->sheet->getCell('I' . $row)->getValue()));
                    $total_value_brut = abs(trim($this->sheet->getCell('K' . $row)->getValue()));
                    $total_value = $total_value_brut;

                    $data_document_product = [
                        'document_id'               => $document->id,
                        'product_id'                => $product->id,
                        'line_id'                   => $product->line_id,
                        'brand_id'                  => $product->brand_id,
                        'seller_id'                 => null,
                        'description'               => $product->description,
                        'manage_inventory'          => $product->manage_inventory,
                        'branchoffice_warehouse_id' => $origin_branchoffice_warehouse_id,
                        'operationType_id'          => $output_operation_type_id,
                        'quantity'                  => abs(trim($this->sheet->getCell('H' . $row)->getValue())),
                        'unit_value_before_taxes'   => $unit_value_before_taxes,
                        'discount_value'            => 0,
                        'total_value_brut'          => $total_value_brut,
                        'total_value'               => $total_value
                    ];

                    DocumentProduct::create($data_document_product);

                    $document->thread = $document->id;
                    $document->affects_document_id = $document->id;
                    $document->affects_prefix_document = $document->prefix;
                    $document->affects_number_document = $document->consecutive;
                    $document->affects_date_document = $document->document_date;

                    $document->total_value_brut += $total_value_brut;
                    $document->total_value += $total_value;

                    $document->save();

                    $data_document['branchoffice_warehouse_id'] = $destination_branchoffice_warehouse_id;
                    $data_document['model_id'] = $input_model_id;
                    $data_document['operationType_id'] = $input_operation_type_id;
                    $data_document['origin_document'] = false;

                    $document_two = $this->getDocument($data_document);

                    $this->createDocumentInformation($document_two);

                    $data_document_product['document_id'] = $document_two->id;
                    $data_document_product['branchoffice_warehouse_id'] = $destination_branchoffice_warehouse_id;
                    $data_document_product['operationType_id'] = $input_operation_type_id;

                    DocumentProduct::create($data_document_product);

                    $document_two->thread = $document->id;
                    $document_two->affects_document_id = $document->id;
                    $document_two->affects_prefix_document = $document->prefix;
                    $document_two->affects_number_document = $document->consecutive;
                    $document_two->affects_date_document = $document->document_date;

                    $document_two->total_value_brut += $total_value_brut;
                    $document_two->total_value += $total_value;

                    $document_two->save();
                }
            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        Log::info('// ------------------------------------- Transfers');

        if (count($contacts_not_found) || count($contacts_warehouses_not_found) || count($products_not_found)) {
            Log::info(collect($contacts_not_found)->unique()->toArray());
            Log::info(collect($contacts_warehouses_not_found)->unique()->toArray());
            Log::info(collect($products_not_found)->unique()->toArray());
        }

        DB::commit();
    }
}
