<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\DocumentsService;
use App\Entities\Document;


class DocumentUpdateStateMass extends Command
{
    protected $documentsService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'document:updateState';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando actualiza el estado de todos los docuemntos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $documentsService)
    {
        $this->documentsService = $documentsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $documents = Document::where('in_progress', false)->orderBy('id')->get();
        echo "Total Docs: ".$documents->count()."\n";


        foreach ($documents as $key => $document) {
            $res = $this->documentsService->documentUpdateState($document->id);
            echo $key."\n";
        }

        echo "------ FINALIZO -------";
    }
}
