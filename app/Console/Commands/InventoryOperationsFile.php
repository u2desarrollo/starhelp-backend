<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\DocumentRepository;
use App\Repositories\ContactWarehouseRepository;
use App\Services\CashReceiptsService;

class InventoryOperationsFile extends Command
{
    private $documentRepository;
    private $ContactWarehouseRepository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:inventoryoperationsfile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentRepository $documentRepository, ContactWarehouseRepository $ContactWarehouseRepository)
    {
        parent::__construct();
        $this->documentRepository = $documentRepository;
        $this->ContactWarehouseRepository  = $ContactWarehouseRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       try {

        $flatfile  = new CashReceiptsService();
        $flatfile->createFlat($this->documentRepository->getInfoForFile(), $this->ContactWarehouseRepository->getInfoForFile(), $this->documentRepository->gettransfersForFile());
        \Log::info('proceso inventory exitoso');

       } catch (\Throwable $th) {
            \Log::info($th);
       }
    }
}
