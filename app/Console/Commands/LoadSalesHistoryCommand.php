<?php

namespace App\Console\Commands;

use App\Entities\Document;
use App\Entities\DocumentProduct;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class LoadSalesHistoryCommand
 * @package App\Console\Commands
 */
class LoadSalesHistoryCommand extends Command
{

    private $spreadsheet;
    private $sheet;
    private $documents;
    private $row;
    private $contact;
    private $contactWarehouse;
    private $product;
    private $branchOfficeWarehouse;
    private $added;
    private $month;
    private $consecutive;
    private $rowsNotFounded;
    private $rowsFounded;
    private $counterDocumentsProducts;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     * @author Jhon García
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet();
    }

    public function setDocuments()
    {
        $this->documents = collect();
    }

    /**
     * @throws Exception
     * @author Jhon García
     */
    public function setSpreadSheet()
    {
        /*$reader = IOFactory::createReader("Xlsx");
        $this->spreadsheet = $reader->load(storage_path() . "/app/public/ventas.xlsx");*/
    }

    /**
     * @param int $index
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @author Jhon García
     */
    public function setSheet(int $index)
    {
        $this->sheet = $this->spreadsheet->getSheet($index);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function handle()
    {
        $this->consecutive = 1;

        try {
            DB::beginTransaction();
            $this->setData();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function setData()
    {
        for ($index = 0; $index <= 5; $index++) {

            $this->setDocuments();
            $this->setSheet($index);
            $validator = true;
            $this->row = 2;
            $this->month = $index + 1;
            $this->rowsFounded = 0;
            $this->rowsNotFounded = 0;

            Log::info('//---------- Hoja ' . $index);

            while ($validator) {

                // Obtiene el número de identificación del tercero
                $identification = trim($this->sheet->getCell('A' . $this->row)->getValue());

                // Obtiene el código de de la sucursal del tercero
                $code = trim($this->sheet->getCell('B' . $this->row)->getValue());

                // Obtiene la referencia del producto
                $reference = trim($this->sheet->getCell('H' . $this->row)->getValue());

                // Obtiene la referencia del producto
                $quantity = trim($this->sheet->getCell('I' . $this->row)->getValue());

                if (!empty($identification)) {

                    if ($quantity != 0 && $reference != '0') {
                        // Establece el id del contact
                        $this->setContact($identification);

                        // Establece el id del contactWarehouse
                        $this->setContactWarehouse($code);

                        // Establece el id del branchOfficeWarehouse
                        $this->setBranchOfficeWarehouse();

                        // Establece el id del producto
                        $this->setProduct($reference);

                        if ($this->validateData()) {
                            $this->add();
                        }
                    }
                } else {
                    $validator = false;
                }

                $this->row++;
            }

            $this->saveData();

            Log::info('//---------- Encontrados ' . $this->rowsFounded);
            Log::info('//---------- No encontrados ' . $this->rowsNotFounded);
        }
    }


    public function saveData()
    {

        $this->counterDocumentsProducts = 0;

        $this->documents->map(function ($document) {
            $documentCreated = Document::create((array)$document);
            $document->documents_products->map(function ($documentProduct) use ($documentCreated) {
                $documentProduct->document_id = $documentCreated->id;
                DocumentProduct::create((array)$documentProduct);
                ++$this->counterDocumentsProducts;
            });
        });

    }

    /**
     * @param string $identification
     * @return void
     */
    public function setContact(string $identification): void
    {
        $this->contact = DB::table('contacts')
            ->where('identification', $identification)
            ->first();

        if (!$this->contact) {
            $contact = [
                'subscriber_id' => 1,
                'identification_type' => 1,
                'identification' => $identification,
                'name' => $this->getDefinitiveName(),
                'deleted_at' => date('Y-m-d H:i:s')
            ];

            DB::table('contacts')->insert($contact);

            $this->setContact($identification);
        }
    }

    /**
     * @param string $code
     * @return void
     */
    public function setContactWarehouse(string $code): void
    {
        $definitiveCode = str_pad($code, 3, '0', STR_PAD_LEFT);

        $this->contactWarehouse = DB::table('contacts_warehouses')
            ->select('id', 'branchoffice_id')
            ->where('code', $definitiveCode)
            ->where('contact_id', $this->contact->id)
            ->first();

        if (!$this->contactWarehouse) {
            $contactWarehouse = [
                'contact_id' => $this->contact->id,
                'code' => $definitiveCode,
                'description' => $this->getDefinitiveName(),
                'state' => 0,
                'deleted_at' => date('Y-m-d H:i:s')
            ];

            DB::table('contacts_warehouses')->insert($contactWarehouse);

            $this->setContact($code);
        }

    }

    public function getDefinitiveName()
    {
        $name = $this->sheet->getCell('C' . $this->row)->getValue();

        $nameAsArray = explode('-', $name);

        unset($nameAsArray[0]);
        unset($nameAsArray[1]);

        return implode($nameAsArray);
    }

    /**
     * @param string $reference
     * @return void
     */
    public function setProduct(string $reference): void
    {
        $this->product = DB::table('products')
            ->where('code', $reference)
            ->first();

        if (!$this->product) {

            $lineAndBrand = $this->sheet->getCell('D' . $this->row)->getValue();

            $lineAndBrandAsArray = explode('-', $lineAndBrand);

            $brand_id = null;
            $line_id = $lineAndBrandAsArray[0];

            if (isset($lineAndBrandAsArray[1])) {
                $brand_id = $lineAndBrandAsArray[1];
            }

            $product = [
                'code' => $this->sheet->getCell('H' . $this->row)->getValue(),
                'description' => $this->sheet->getCell('G' . $this->row)->getValue(),
                'line_id' => $line_id,
                'brand_id' => $brand_id,
                'state' => 0,
                'deleted_at' => date('Y-m-d H:i:s')
            ];

            DB::table('products')->insert($product);

            $this->setProduct($reference);
        }
    }

    /**
     * @return void
     */
    public function setBranchOfficeWarehouse(): void
    {
        if (is_null($this->contactWarehouse) || is_null($this->contactWarehouse->branchoffice_id)) {
            $this->branchOfficeWarehouse = (object)['id' => 279];
        } else {
            $this->branchOfficeWarehouse = DB::table('branchoffice_warehouses')
                ->select('id')
                ->where('branchoffice_id', $this->contactWarehouse->branchoffice_id)
                ->where('warehouse_code', '01')
                ->first();
        }
    }


    /**
     * @return bool
     */
    public function validateData(): bool
    {
        $validate = $this->contact && $this->contactWarehouse && $this->product && $this->branchOfficeWarehouse;

        if ($validate) {
            $this->rowsFounded++;
        } else {
            $this->rowsNotFounded++;
        }

        return $validate;
    }

    /**
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function add()
    {
        $document = $this->buildArrayDocument();

        $documentProduct = $this->buildArrayDocumentProduct();

        $this->addDocumentProduct($documentProduct);

        if (!$this->added) {
            $this->documents->push($document);
            $this->addDocumentProduct($documentProduct);
        }
    }

    /**
     * @param $documentProduct
     * @return mixed
     */
    public function addDocumentProduct($documentProduct)
    {
        $this->added = false;
        $this->documents->map(function ($doc) use ($documentProduct) {
            if ($doc->contact_id == $this->contact->id && $doc->warehouse_id == $this->contactWarehouse->id) {
                $this->added = true;
                if (isset($doc->documents_products)) {
                    $doc->documents_products->push($documentProduct);
                } else {
                    $documentsProductsCollect = collect();
                    $documentsProductsCollect->push($documentProduct);
                    $doc->documents_products = $documentsProductsCollect;
                    $doc->total_value += $documentProduct->total_value;
                    $doc->total_value_brut += $documentProduct->total_value_brut;
                }
            }
        });
    }

    /**
     * @return object
     */
    public function buildArrayDocument()
    {
        return (object)[
            'vouchertype_id' => 490,
            'model_id' => 68,
            'prefix' => 'HISV',
            'branchoffice_id' => $this->contactWarehouse->branchoffice_id,
            'branchoffice_warehouse_id' => $this->branchOfficeWarehouse->id,
            'consecutive' => $this->consecutive++,
            'document_date' => '2020-0' . $this->month . '-01 08:00:00',
            'contact_id' => $this->contact->id,
            'warehouse_id' => $this->contactWarehouse->id,
            'document_last_state' => 1,
            'issue_date' => '2020-0' . $this->month . '-01',
            'in_progress' => false,
            'operationType_id' => 5,
            'total_value' => 0,
            'total_value_brut' => 0,
            'observation' => 'Cargue de historial de ventas'
        ];
    }

    /**
     * @return object
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function buildArrayDocumentProduct()
    {
        $quantity = $this->sheet->getCell('I' . $this->row)->getValue();
        $total_value = $this->sheet->getCell('J' . $this->row)->getValue();
        $total_value_brut = $this->sheet->getCell('J' . $this->row)->getValue();
        $inventory_cost_value = $this->sheet->getCell('K' . $this->row)->getValue();
        $is_benefit = $quantity > 0 && $total_value == 0;
        $unit_value_before_taxes = $total_value / $quantity;
        $unit_value_after_taxes = $total_value / $quantity;

        return (object)[
            'document_id' => null,
            'product_id' => $this->product->id,
            'quantity' => $this->sheet->getCell('I' . $this->row)->getValue(),
            'operationType_id' => 5,
            'line_id' => $this->product->line_id,
            'subline_id' => $this->product->subline_id,
            'brand_id' => $this->product->brand_id,
            'subbrand_id' => $this->product->subbrand_id,
            'is_benefit' => $is_benefit,
            'inventory_cost_value' => $inventory_cost_value,
            'unit_value_before_taxes' => $unit_value_before_taxes,
            'unit_value_afert_taxes' => $unit_value_after_taxes,
            'total_value' => $total_value,
            'total_value_brut' => $total_value_brut,
            'observation' => 'Cargue de historial de ventas'
        ];
    }


}
