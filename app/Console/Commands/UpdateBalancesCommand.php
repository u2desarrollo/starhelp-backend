<?php

namespace App\Console\Commands;

use App\Entities\Document;
use App\Entities\Subscriber;
use App\Services\BalancesService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateBalancesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var BalancesService
     */
    public $balances_service;

    /**
     * Create a new command instance.
     *
     * @param BalancesService $balances_service
     */
    public function __construct(BalancesService $balances_service)
    {
        $this->balances_service = $balances_service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriber = Subscriber::select('id', 'document_id')->first();

        if ($subscriber && !empty($subscriber->document_id)) {

            $document = Document::find($subscriber->document_id);

            $document_date = Carbon::parse($document->year_month)->startOfMonth();

            $current_date = now()->startOfMonth();

            if ($current_date->diffInMonths($document_date) >= 2) {
                $year_month = $document_date->format('Ym');
            } else {
                $year_month = $current_date->addMonths(-2)->format('Ym');
            }

            $this->balances_service->updateBalances($year_month, null, false, null);

            $subscriber->document_id = null;

            $subscriber->last_date_update_balances = date('Y-m-d H:i:s');

            $subscriber->save();
        }
    }
}
