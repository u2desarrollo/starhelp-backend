<?php

namespace App\Console\Commands;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactEmployee;
use App\Entities\ContactWarehouse;
use App\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class MakeContactsSellersAndUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:contacts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $index = [
            'identificationSeller' => 15,
            'nameSeller' => 16,
            'surnameSeller' => 17,
            'emailSeller' => 14,
            'cellphoneSeller' => 18,
            'branchofficeSeller' => 19,
            'identificationContact' => 0,
            'checkDigitContact' => 1,
            'businessNameContact' => 2,
            'firstNameContact' => 3,
            'secondNameContact' => 4,
            'surnameContact' => 5,
            'secondSurnameContact' => 6,
            'addressContact' => 7,
            'mainTelephoneContact' => 8,
            'secondaryTelephoneContact' => 9,
            'cellphoneContact' => 10,
            'emailContact' => 11,
            'creditQuotaContact' => 12,
            'expirationDaysContact' => 13,
            'cityContact' => 19
        ];

        $file = fopen(storage_path('app/public/actualizacionterceros.csv'), "r");

        $cities = DB::table('cities')->get();
        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    $branchoffice_name = str_replace("SEDE ", "", $branchoffice->name);
                    similar_text($branchoffice_name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                //Consulta si existe el vendedor

                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);

                    $array_seller = [
                        'subscriber_id' => 1,
                        'identification' => $array[$index['identificationSeller']],
                        'identification_type' => 2,
                        'name' => $array[$index['nameSeller']],
                        'surname' => $array[$index['surnameSeller']],
                        'cell_phone' => $array[$index['cellphoneSeller']],
                        'email' => $array[$index['emailSeller']],
                        'is_employee' => true
                    ];

                    $seller = Contact::updateOrCreate(Arr::only($array_seller, ['identification']), $array_seller);

                    $array_contact_employee = [
                        'contact_id' => $seller->id,
                        'position_id' => 937
                    ];

                    ContactEmployee::updateOrCreate(Arr::only($array_contact_employee, ['contact_id']), $array_contact_employee);

                    $array_user = [
                        'contact_id' => $seller->id,
                        'identification_type' => 2,
                        'name' => $array[$index['nameSeller']],
                        'surname' => $array[$index['surnameSeller']],
                        'email' => $array[$index['emailSeller']],
                        'password' => $array[$index['identificationSeller']],
                        'photo' => '/contacts/employees/default.jpg',
                        'module_id' => 3,
                        'branchoffice_id' => $branchoffice_id,
                        'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                        'subscriber_id' => 1,
                        'type' => 'e',
                        'password_changed' => true
                    ];

                    $user = User::updateOrCreate(Arr::only($array_user, ['email', 'contact_id']), $array_user);

                    $user->subscribers()->attach([1]);
                    $user->modules()->attach([3]);
                    $user->getRoles()->sync([7]);

                }

                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);

                if ($array[$index['firstNameContact']] == '') {
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                } else {
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];
                }

                $array_contact = [
                    'subscriber_id' => 1,
                    'identification' => $array[$index['identificationContact']],
                    'check_digit' => $array[$index['checkDigitContact']],
                    'name' => $name,
                    'surname' => $surname,
                    'address' => $array[$index['addressContact']],
                    'main_telephone' => $array[$index['mainTelephoneContact']],
                    'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                    'cell_phone' => $array[$index['cellphoneContact']],
                    'email' => $array[$index['emailContact']],
                    'is_customer' => true
                ];

                $contact = Contact::updateOrCreate(Arr::only($array_contact, ['identification']), $array_contact);

                $array_contact_customer = [
                    'contact_id' => $contact->id,
                    'credit_quota' => (int)$array[$index['creditQuotaContact']],
                    'expiration_days' => $array[$index['expirationDaysContact']]
                ];

                ContactCustomer::updateOrCreate(Arr::only($array_contact_customer, ['contact_id']), $array_contact_customer);

                $city_id = null;

                foreach ($cities as $city) {
                    similar_text($city->city, $array[$index['cityContact']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 50) {
                        $city_id = $city->id;
                        break;
                    }
                }

                $array_contact_warehouse = [
                    'contact_id' => $contact->id,
                    'code' => '000',
                    'description' => $name,
                    'address' => $array[$index['addressContact']],
                    'telephone' => $array[$index['mainTelephoneContact']],
                    'city_id' => $city_id,
                    'seller_id' => isset($seller) && $seller ? $seller->id : null,
                    'credit_quota' => (int)$array[$index['creditQuotaContact']],
                    'branchoffice_id' => $branchoffice_id
                ];

                ContactWarehouse::updateOrCreate(Arr::only($array_contact_warehouse, ['contact_id', 'code']), $array_contact_warehouse);
                echo ++$countContacts . "\n";

            }


        }
    }
}
