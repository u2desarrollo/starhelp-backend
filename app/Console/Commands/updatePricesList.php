<?php

namespace App\Console\Commands;

use App\Entities\Price;
use App\Entities\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class updatePricesList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:pricesList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products=Product::select("id","code","description","line_id","subline_id","brand_id","subbrand_id","quantity_available")
        ->with([
            'price' => function($query){
               $query->where('price_list_id', 948);
            },
            'prices' => function ($query) {
                $query->with(['parameter']);
            }])->get();


            $file = fopen(storage_path('app/public/portfolio/PRECIO PISO.csv'), "r");
            $cant=0;
            try {
                DB::beginTransaction();
                while (!feof($file)) {
                    // Leyendo una linea
                    $line = fgets($file);
                    //sacando un array apartir de la linea extraida del archivo
                    $array = explode(";", utf8_encode($line));

                    if ($cant > 0 && (count($array) > 1)) {
                        $products->map(function($product,$key) use ($array){
                             if($product->code==$array[0]){
                                 $floor_price=str_replace('$','',$array[1]);
                                 $floor_price=str_replace('.','',$floor_price);
                                $price=Price::where('id',$product->price->id)->update(['floor_price'=>(int)$floor_price]);
                             }


                       });
                    }
                    $cant++;
                }
                DB::commit();
                \Log::info('Actualizacion de lista de precios correcta');

            } catch (\Throwable $th) {
                DB::rollBack();
            }




    }
}
