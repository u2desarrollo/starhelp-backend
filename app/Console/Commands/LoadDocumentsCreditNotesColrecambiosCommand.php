<?php

namespace App\Console\Commands;

use App\Entities\Document;
use App\Entities\DocumentProduct;
use App\Entities\DocumentProductTax;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\TypesOperation;
use App\Entities\VouchersType;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadDocumentsCreditNotesColrecambiosCommand extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:credit_notes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     * @author Jhon García
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('Notas Credito');

        $validator = true;
        $products_not_found = [];
        $contacts_not_found = [];
        $contacts_warehouses_not_found = [];

        $row = 2;

        DB::beginTransaction();

        $base_document_voucher_type_id = VouchersType::where('code_voucher_type', '205')->first()->id;
        $base_document_model_id = Template::where('code', '205')->first()->id;
        $base_document_operation_type_id = TypesOperation::where('code', '205')->first()->id;

        $voucher_type_id = VouchersType::where('code_voucher_type', '155')->first()->id;
        $model_id = Template::where('code', '155')->first()->id;
        $operation_type_id = TypesOperation::where('code', '155')->first()->id;

        while ($validator) {

            // Obtiene el número de identificación del tercero
            $identification = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($identification)) {

                // Consultar tercero por numero de identificación
                $contact = $this->getContact($identification);

                if (!$contact) {
                    array_push($contacts_not_found, $identification);
                } else {

                    $contact_warehouse = $this->getContactWarehouse($contact->id);

                    if (!$contact_warehouse) {
                        array_push($contacts_warehouses_not_found, $identification);
                    } else {

                        // Obtiene el número de identificación del vendedor
                        $identification_seller = trim($this->sheet->getCell('K' . $row)->getValue());

                        $seller_id = null;

                        if (!empty($identification_seller)){
                            // Consultar vendedor por numero de identificación
                            $seller = $this->getSeller($identification_seller, trim($this->sheet->getCell('L' . $row)->getValue()));
                            $seller_id = $seller->id;
                        }

                        $document_date_from_excel = trim($this->sheet->getCell('H' . $row)->getValue());

                        if (is_numeric($document_date_from_excel)){
                            $document_date_as_string = Date::excelToDateTimeObject($document_date_from_excel)->format('Y-m-d H:i:s');
                        }else{
                            $document_date_as_string = $document_date_from_excel;
                        }

                        $document_date = Carbon::parse($document_date_as_string);

                        $due_date_from_excel = trim($this->sheet->getCell('G' . $row)->getValue());

                        if (is_numeric($due_date_from_excel)){
                            $due_date_as_string = Date::excelToDateTimeObject($due_date_from_excel)->format('Y-m-d');
                        }else{
                            $due_date_as_string = $due_date_from_excel;
                        }

                        $due_date = Carbon::parse($due_date_as_string);

                        $data_base_document = [
                            'prefix'                    => trim($this->sheet->getCell('I' . $row)->getValue()),
                            'consecutive'               => (int)trim($this->sheet->getCell('J' . $row)->getValue()),
                            'branchoffice_id'           => 5,
                            'branchoffice_warehouse_id' => 1,
                            'vouchertype_id'            => $base_document_voucher_type_id,
                            'model_id'                  => $base_document_model_id,
                            'operationType_id'          => $base_document_operation_type_id,
                            'issue_date'                => '2020-12-31',
                            'document_date'             => '2020-12-31 11:59:00',
                            'document_last_state'       => '2020-12-31',
                            'due_date'                  => '2020-12-31',
                            'contact_id'                => $contact->id,
                            'warehouse_id'              => $contact_warehouse->id,
                            'seller_id'                 => $seller_id,
                            'user_id'                   => 44,
                            'observation'               => 'Factura de venta Año 2020***',
                            'in_progress'               => false,
                            'origin_document'           => true
                        ];

                        $base_document = Document::select('id', 'total_value', 'total_value_brut')
                            ->where(Arr::only($data_base_document, ['prefix', 'consecutive', 'vouchertype_id']))
                            ->first();

                        if (!$base_document) {
                            $base_document = Document::create($data_base_document);

                            $base_document->affects_document_id = $base_document->id;
                            $base_document->affects_prefix_document = $base_document->prefix;
                            $base_document->affects_number_document = $base_document->consecutive;
                            $base_document->affects_date_document = $base_document->document_date;

                            $base_document->save();
                        }

                        $data_document = [
                            'prefix'                    => trim($this->sheet->getCell('D' . $row)->getValue()),
                            'consecutive'               => trim($this->sheet->getCell('E' . $row)->getValue()),
                            'branchoffice_id'           => 5,
                            'branchoffice_warehouse_id' => 1,
                            'vouchertype_id'            => $voucher_type_id,
                            'model_id'                  => $model_id,
                            'operationType_id'          => $operation_type_id,
                            'issue_date'                => $document_date->format('Y-m-d'),
                            'document_date'             => $document_date->format('Y-m-d H:i:s'),
                            'document_last_state'       => $document_date->format('Y-m-d'),
                            'due_date'                  => $due_date->format('Y-m-d'),
                            'contact_id'                => $contact->id,
                            'warehouse_id'              => $contact_warehouse->id,
                            'seller_id'                 => $seller_id,
                            'user_id'                   => 44,
                            'thread'                    => $base_document->id,
                            'observation'               => trim($this->sheet->getCell('Y' . $row)->getValue()),
                            'in_progress'               => false,
                            'origin_document'           => false,
                            'affects_document_id'       => $base_document->id,
                            'affects_prefix_document'   => $base_document->prefix,
                            'affects_number_document'   => $base_document->consecutive,
                            'affects_date_document'     => $base_document->document_date
                        ];

                        $document = $this->getDocument($data_document);

                        $this->createDocumentInformation($document);

                        $product_code = trim($this->sheet->getCell('M' . $row)->getValue());

                        $product = Product::where('previous_code_system', $product_code)->first();

                        if (!$product) {
                            array_push($products_not_found, $product_code);
                        } else {

                            $unit_value_before_taxes = abs(trim($this->sheet->getCell('P' . $row)->getValue()));
                            $total_value_brut = abs(trim($this->sheet->getCell('R' . $row)->getValue()));
                            $iva_value = abs(trim($this->sheet->getCell('S' . $row)->getValue()));
                            $total_value = $total_value_brut + $iva_value;

                            $data_document_product = [
                                'document_id'               => $document->id,
                                'product_id'                => $product->id,
                                'line_id'                   => $product->line_id,
                                'brand_id'                  => $product->brand_id,
                                'seller_id'                 => $seller_id,
                                'description'               => $product->description,
                                'manage_inventory'          => $product->manage_inventory,
                                'branchoffice_warehouse_id' => 1,
                                'operationType_id'          => $operation_type_id,
                                'quantity'                  => abs((int)trim($this->sheet->getCell('N' . $row)->getValue())),
                                'unit_value_before_taxes'   => $unit_value_before_taxes,
                                'discount_value'            => trim($this->sheet->getCell('Q' . $row)->getValue()),
                                'total_value_brut'          => $total_value_brut,
                                'total_value'               => $total_value
                            ];

                            $document_product = DocumentProduct::create($data_document_product);

                            DocumentProductTax::create([
                                'document_product_id' => $document_product->id,
                                'tax_id'              => 1,
                                'base_value'          => $total_value_brut,
                                'tax_percentage'      => 19,
                                'value'               => $iva_value
                            ]);

                            $document->total_value_brut += $total_value_brut;
                            $document->total_value += $total_value;

                            $document->save();
                        }
                    }
                }
            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        Log::info('// ------------------------------------- Credit Notes');
        if (count($contacts_not_found) || count($contacts_warehouses_not_found) || count($products_not_found)) {
            Log::info(collect($contacts_not_found)->unique()->toArray());
            Log::info(collect($contacts_warehouses_not_found)->unique()->toArray());
            Log::info(collect($products_not_found)->unique()->toArray());
        }

        DB::commit();
    }
}
