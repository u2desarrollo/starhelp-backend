<?php

namespace App\Console\Commands;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\City;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactEmployee;
use App\Entities\ContactProvider;
use App\Entities\ContactWarehouse;
use App\Entities\Parameter;
use App\Entities\User;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadProvidersColrecambios extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:providers_colrecambios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var false|string
     */
    private $startTime;

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     */
    public function __construct()
    {
        $this->setSpreadSheet('proveedores.xlsx');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('BASE');
        $validator = true;
        $row = 2;

        DB::beginTransaction();

        while ($validator) {

            // Obtiene el número de identificación del tercero
            $identification = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($identification)) {

                $first_surname = strtoupper(trim($this->sheet->getCell('B' . $row)->getValue()));
                $second_surname = strtoupper(trim($this->sheet->getCell('C' . $row)->getValue()));
                $first_name = strtoupper(trim($this->sheet->getCell('D' . $row)->getValue()));
                $second_name = strtoupper(trim($this->sheet->getCell('E' . $row)->getValue()));
                $name = strtoupper(trim($this->sheet->getCell('F' . $row)->getValue()));
                $taxpayer_type = strtoupper(trim($this->sheet->getCell('L' . $row)->getValue()));
                $address = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',trim($this->sheet->getCell('G' . $row)->getValue()));
                $main_telephone = trim($this->sheet->getCell('N' . $row)->getValue());
                $email = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', strtolower(trim($this->sheet->getCell('M' . $row)->getValue())));
                $code_city = trim($this->sheet->getCell('I' . $row)->getValue()) . trim($this->sheet->getCell('H' . $row)->getValue());
                $tax_regime = trim($this->sheet->getCell('O' . $row)->getValue());
                $fiscal_responsibility = trim($this->sheet->getCell('P' . $row)->getValue());

                //Consulta si existe el tercero
                $identification = str_replace('.', '', $identification);
                $contact = Contact::where('identification', $identification)->first();

                $city_id = null;

                if (!empty($code_city)) {
                    $city = City::whereRaw('concat(city_code, department_id) = ?', [$code_city])->first();
                    if ($city) {
                        $city_id = $city->id;
                    }
                }

                $fiscal_responsibility_id = null;

                if (!empty($fiscal_responsibility)) {
                    if ($fiscal_responsibility == 'NO') {
                        $fiscal_responsibility_id = 24568;
                    }
                }

                $tax_regime_id = null;

                if (!empty($tax_regime)) {
                    if ($tax_regime == 'S') {
                        $tax_regime_id = 16;
                    } else if ($tax_regime == 'C') {
                        $tax_regime_id = 11;
                    }
                }

                $taxpayer_type_id = null;

                if (!empty($taxpayer_type)) {
                    if ($tax_regime == 'PERSONA NATURAL') {
                        $taxpayer_type_id = 14;
                    } else if ($taxpayer_type == 'JURIDICA') {
                        $taxpayer_type_id = 13;
                    }
                }

                //Si no existe lo crea
                if (!$contact) {
                    $contact = Contact::create([
                        'subscriber_id'            => 3,
                        'identification'           => $identification,
                        'identification_type'      => empty($first_surname) ? 1 : 2,
                        'name'                     => empty($first_surname) ? $name : $first_name . ' ' . $second_name,
                        'surname'                  => empty($first_surname) ? '' : $first_surname . ' ' . $second_surname,
                        'tax_regime'               => $tax_regime_id,
                        'taxpayer_type'            => $taxpayer_type_id,
                        'fiscal_responsibility_id' => $fiscal_responsibility_id,
                        'address'                  => $address,
                        'main_telephone'           => $main_telephone,
                        'city_id'                  => $city_id,
                        'email'                    => $email,
                        'is_provider'              => true
                    ]);
                } else {
                    $contact->subscriber_id = 3;
                    $contact->identification_type = empty($first_surname) ? 1 : 2;
                    $contact->name = empty($first_surname) ? $name : $first_name . ' ' . $second_name;
                    $contact->surname = empty($first_surname) ? '' : $first_surname . ' ' . $second_surname;
                    $contact->tax_regime = $tax_regime_id;
                    $contact->taxpayer_type = $taxpayer_type_id;
                    $contact->fiscal_responsibility_id = $fiscal_responsibility_id;
                    $contact->address = $address;
                    $contact->main_telephone = $main_telephone;
                    $contact->city_id = $city_id;
                    $contact->email = $email;
                    $contact->is_provider = true;
                    $contact->save();
                }


                //Consulta si existe el cliente
                $contact_customer = ContactProvider::where('contact_id', $contact->id)->first();

                if (!$contact_customer) {
                    ContactCustomer::create([
                        'contact_id' => $contact->id
                    ]);
                }

                $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => '000'])->first();

                if (!$contact_warehouse) {

                    ContactWarehouse::create([
                        'contact_id'  => $contact->id,
                        'code'        => '000',
                        'description' => empty($first_surname) ? $name : $first_name . ' ' . $second_name . ' ' . $first_surname . ' ' . $second_surname,
                        'email'       => $email,
                        'address'     => $address,
                        'telephone'   => $main_telephone,
                        'city_id'     => $city_id
                    ]);
                } else {
                    $contact_warehouse->description = empty($first_surname) ? $name : $first_name . ' ' . $second_name . ' ' . $first_surname . ' ' . $second_surname;
                    $contact_warehouse->email = $email;
                    $contact_warehouse->address = $address;
                    $contact_warehouse->telephone = $main_telephone;
                    $contact_warehouse->city_id = $city_id;
                    $contact_warehouse->save();
                }

            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        DB::commit();

        echo "FINALIZO CARGA DE CLIENTES";
    }
}
