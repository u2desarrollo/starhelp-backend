<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\VouchersType;
use App\Entities\Document;
use App\Entities\Inventory;
use App\Services\DocumentsProductsService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class CalculateAvailabilityAllWarehouse extends Command
{
    protected $documentsProductsService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caculate:available';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param DocumentsProductsService $documentsProductsService
     */
    public function __construct(DocumentsProductsService $documentsProductsService)
    {
        $this->documentsProductsService = $documentsProductsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);

        Log::info('//--------------- Calculando disponibilidad');

        $branchOfficeWarehouses = BranchOfficeWarehouse::where('warehouse_code', '01')
            // ->where('branchoffice_id', 286)
            ->get();

        $data = [];

        foreach ($branchOfficeWarehouses as $key => $branchOfficeWarehouse) {
            // Seteamos las variables
            $products = [];
            $branchoffice_warehouse_id = $branchOfficeWarehouse->id; //279
            $branchoffice_id = $branchOfficeWarehouse->branchoffice_id; //305

            // Traemos el tipo de comprobante.
            $voucherType = VouchersType::where('code_voucher_type', '010')
                ->where('branchoffice_id', $branchoffice_id)
                ->first();


            $sede = !empty($branchOfficeWarehouse->branchoffice) ? $branchOfficeWarehouse->branchoffice->name : 'N/A';
            echo "---------------------------------------------- \n";
            echo "SEDE: $sede \n";

            // Traemos los documentos
            $documents = Document::where('vouchertype_id', $voucherType->id)
                ->where('in_progress', false)
                ->where('closed', false)
                ->where(function ($query) {
                    $query->whereIn('state_authorization_wallet', [24493, 24494]);// Autorizado sistema
                    $query->orWhereNull('state_authorization_wallet'); //Autorizado cartera
                })
                ->whereNotIn('state_backorder', ['831'])
                ->where('branchoffice_id', $branchoffice_id)
                ->where('branchoffice_warehouse_id', $branchoffice_warehouse_id)
                ->whereNotIn('state_backorder', ['831'])
                ->orderBy('id', 'ASC')
                ->get();

            // Validamos si ya fueron finalizados
            foreach ($documents as $key => $document) {

                $request = [
                    'document_id' => $document->id,
                    'model_id'    => 1
                ];

                $documentsBackOrder = [];

                // foreach ($document->documentsProducts as $keyDp => $documentProduct) {
                //     if ($documentProduct->quantity > 0) {
                //         $bo = $this->documentsProductsService->calculateBackOrder($documentProduct->id, $request, false);
                //         $documentsBackOrder[] = $bo;
                //         $document->documentsProducts[$keyDp]['back_order'] = $bo['backorder'];
                //     }
                // }


                // $sumBackOrder = collect($documentsBackOrder)->sum('backorder');

                if ($document->backorder_quantity == 0) {
                    $documents[$key]['complete'] = true;
                } else {
                    $documents[$key]['complete'] = false;
                }

            }

            $documents = $documents->where('complete', false);

            // Validamos si el pedido esta retenido
            foreach ($documents as $key => $document) {

                // Esta variable guarda el motivo de la retencion.
                $motiveRejection = '';

                // Validamos si el pedido fue aprobado.
                if (!empty($document->status_id_authorized) && $document->status_id_authorized == 912) {
                    $documents[$key]['detained'] = false;
                    continue;
                }

                // Validamos si el pedido fue rechazado.
                if (!empty($document->status_id_authorized) && $document->status_id_authorized == 913) {
                    $documents[$key]['detained'] = true;
                    continue;
                }

                ## --FECHA DE VENCIMIENTO YA ESTE EXPIRADA-- ##
                // Traemos las facturas del cliente.
                $invoices = Document::select('document_date', 'due_date', 'operationType_id', 'contact_id', 'id')
                    ->where('contact_id', $document->contact_id)
                    ->where('operationType_id', 5)
                    ->has('receivable')
                    ->get();

                if ($invoices->where('facturaVencida', true)->count() > 0) {
                    $motiveRejection = 'Cartera Vencida';
                }

                ## --CUPO DISPONIBLE-- ##
                $totalWallet = 0;
                $totalQuotaCredit = 0;
                $availableQuota = 0;

                // Total Cartera
                $totalWallet = $invoices->sum(function ($item) {
                    return round($item->receivable[0]->invoice_balance, 0, PHP_ROUND_HALF_UP);
                });

                // Cupo de credito
                $totalQuotaCredit = $document->contact->customer->credit_quota;

                // Cupo disponible
                $availableQuota = $totalQuotaCredit - $totalWallet;

                if ($document->total_value > $availableQuota) {
                    $motiveRejection = empty($motiveRejection) ? 'Cupo insuficiente' : $motiveRejection . ' / Cupo insuficiente';
                }

                // validamos si el pedido tiene algun motivo de retencion.
                if (!empty($motiveRejection)) {

                    $documents[$key]['detained'] = true;

                } // No retenido
                else {
                    $documents[$key]['detained'] = false;
                }
            }

            // Traemos los que no estan retenidos.
            $documents = $documents->where('detained', false);

            //-- MENSAJE
            echo "PEDIDOS SIN PROBLEMAS: " . $documents->count() . " \n";

            try {
                DB::beginTransaction();

                // Actualizamos la disponibilidad al stock antes de hacer cambios.
                DB::update('update inventories
                set available = stock
                where branchoffice_warehouse_id = ?', [$branchoffice_warehouse_id]);

                $products = [];

                // Recorremos los documentos para rellenar el array de productos.
                foreach ($documents as $key => $document) {

                    // Recorremos los productos de los documentos
                    foreach ($document->documentsProducts as $key => $documetProduct) {
                        if ($documetProduct['backorder_quantity'] == 0) {
                            continue;
                        }

                        $key_product_array = array_search(
                            $documetProduct->product_id,
                            array_column($products, 'product_id')
                        );

                        // Agregamos el producto al array.
                        if ($key_product_array === false) {
                            $products[] = [
                                'product_id'                => $documetProduct->product_id,
                                'back_order'                => $documetProduct['backorder_quantity'],
                                'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id,
                            ];
                        } // Actualizamos.
                        else {
                            // Validamos que la bodega sea la misma
                            if ($products[$key_product_array]['branchoffice_warehouse_id'] == $document->branchoffice_warehouse_id) {
                                $products[$key_product_array]['back_order'] += $documetProduct['backorder_quantity'];
                            } // En caso de ser diferente se crea.
                            else {
                                $products[] = [
                                    'product_id'                => $documetProduct->product_id,
                                    'back_order'                => $documetProduct['backorder_quantity'],
                                    'branchoffice_warehouse_id' => $document->branchoffice_warehouse_id,
                                ];
                            }
                        }

                    }
                }

                // Recorremos los productos y actualizamos la disponibilidad
                foreach ($products as $key => $product) {

                    // Traemos el registro del inventario segun el product_id
                    $inventory = Inventory::where('product_id', $product['product_id'])
                        ->where('branchoffice_warehouse_id', $product['branchoffice_warehouse_id'])
                        ->first();

                    // Validamos si hay registro
                    if ($inventory) {
                        // Calculamos la disponibilidad, stock - cantidad pendiente.
                        $available = $inventory->stock - $product['back_order'];
                        if ($available <= 0) {
                            $available = 0;
                        }

                        // Se actualiza la disponibilidad.
                        $inventory->available = $available;
                        $inventory->save();
                    } // Si no hay registro se crea
                    else {
                        $available = 0 - $documetProduct->back_order;
                        Inventory::create([
                            'branchoffice_warehouse_id' => $product['branchoffice_warehouse_id'],
                            'product_id'                => $product['product_id'],
                            'stock'                     => 0,
                            'stock_values'              => 0,
                            'average_cost'              => 0,
                            'available'                 => $available,
                        ]);
                    }
                }

                DB::commit();

                echo "CANTIDAD PRODUCTOS AFECTADOS: " . count($products) . " \n";

            } catch (\Exception $e) {
                DB::rollBack();
                return 'Error:"' . $e->getMessage() . '"' . $e->getLine() . "File: " . $e->getFile();
            }
        }

        Log::info('//--------------- Finalizando calculo de disponibilidad');

        return $data;
    }
}
