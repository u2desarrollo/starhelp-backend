<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadAllDocumentsColrecambiosCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load_documents:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Truncar tablas
        DB::select('TRUNCATE TABLE public.balances_for_accounts RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.balances_for_contacts RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.balances_for_documents RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.documents RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.document_transactions RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.documents_informations RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.documents_products RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.inventories RESTART IDENTITY CASCADE;');
        DB::select('TRUNCATE TABLE public.inventoriesx RESTART IDENTITY CASCADE;');

        echo 'Tablas truncadas';

        $this->call('load_documents:invoices_old'); // Cargar documentos anteriores a 2021
        $this->call('create:massiveOpeningBalancesColrecambios'); // Cargar saldos iniciales de inventario
        $this->call('load:opening_balances'); // Cargar saldos iniciales contables
        $this->call('load_documents:invoices'); // Cargar facturas
        $this->call('load_documents:gift_invoices'); // Cargar facturas obsequio
        $this->call('load_documents:credit_notes'); // Cargar notas credito
        $this->call('load_documents:outputs'); // Cargar salidas
        $this->call('load_documents:inputs'); // Cargar entradas
        $this->call('load_documents:transfers'); // Cargar traslados
        $this->call('load_documents:adjustments'); // Cargar Ajustes
        $this->call('load:movements'); // Cargar movimientos contables
        $this->call('sync:fixGlobalValues'); // Reconstruir inventario
        $this->call('load_documents:reaccount'); // Recontabilizar documentos
        $this->call('load_documents:update_balances'); // Actualizar saldos
    }
}
