<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\AuroraService;

class AuroraCommand extends Command
{
    protected $auroraService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:aurora';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AuroraService $auroraService)
    {
        $this->auroraService = $auroraService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->auroraService->aurora();
    }
}
