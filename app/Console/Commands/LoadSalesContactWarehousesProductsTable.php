<?php

namespace App\Console\Commands;

use App\Entities\SalesContactWarehousesProduct;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LoadSalesContactWarehousesProductsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:last_sales_for_contact_warehouse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Obtener las ventas del ultimo día
        $sales = DB::select("select * from public.sales_contact_warehouses_products()");

        try {
            DB::beginTransaction();

            // Recorrer ventas
            foreach ($sales as $key => $sale) {
                $sale = (array)$sale;

                // Consultar el registro en base de datis
                $sale_contact_warehouse_product = SalesContactWarehousesProduct::where(Arr::only($sale, ['contact_warehouse_id', 'product_id']))
                    ->first();

                $last_sale_date = Carbon::parse($sale['last_sale_date']);

                $sale['last_sale_days'] = now()->diffInDays($last_sale_date);

                // Si no existe se crea el registro
                if (!$sale_contact_warehouse_product) {
                    $sale['date_one'] = $sale['last_sale_date'];
                    $sale['days_sale'] = now()->diffInDays($last_sale_date) - 3;

                    $sale_contact_warehouse_product = SalesContactWarehousesProduct::create($sale);
                } else { // Si existe el registro se actualiza

                    // Validar si la fecha de ultima venta es diferente a la que está registrada
                    if ($sale['last_sale_date'] != $sale_contact_warehouse_product->last_sale_date) {

                        $date_one = Carbon::parse($sale_contact_warehouse_product->date_one);

                        // Validar si la fecha 2 es null
                        if (is_null($sale_contact_warehouse_product->date_two)) {
                            $sale['date_two'] = $sale['last_sale_date'];
                            $sale['days_sale'] = $last_sale_date->diffInDays($date_one) - 3;
                        } else {

                            $date_two = Carbon::parse($sale_contact_warehouse_product->date_two);

                            // Validar si la fecha 3 no es null entonces pasamos de la fecha 3 a la fecha 2 y de la fecha 2 a la 1
                            if (!is_null($sale_contact_warehouse_product->date_three)) {
                                $sale['date_one'] = $sale_contact_warehouse_product->date_two;
                                $sale['date_two'] = $sale_contact_warehouse_product->date_three;
                            }

                            $sale['date_three'] = $sale['last_sale_date'];

                            // Guardamos las diferencia en días entre fechas 1-2, 2-3
                            $diff_in_days_between_one_and_two = $date_two->diffInDays($date_one);
                            $diff_in_days_between_two_and_three = $last_sale_date->diffInDays($date_two);

                            // Establecer los días de venta con la menor diferencia entre fechas
                            if ($diff_in_days_between_one_and_two <= $diff_in_days_between_two_and_three) {
                                $sale['days_sale'] = $diff_in_days_between_one_and_two - 3;
                            } else {
                                $sale['days_sale'] = $diff_in_days_between_two_and_three - 3;
                            }
                        }

                        $sale['suggested'] = $sale['last_sale_days'] >= $sale['days_sale'] && $sale['last_sale_days'] < ($sale['days_sale'] * 1.5);
                    } else {
                        $sale['last_sale_quantity'] = $sale_contact_warehouse_product->last_sale_quantity;
                    }
                }

                $sale_contact_warehouse_product->update($sale);

            }

            DB::commit();
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
        }
    }
}
