<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentTransactionsService;

class setDocumentTransactions extends Command
{
    private $documentTransactionsService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:matchs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentTransactionsService $documentTransactionsService)
    {
        parent::__construct();
        $this->documentTransactionsService = $documentTransactionsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd($this->documentTransactionsService->setTransactionsFromCsv());
    }
}
