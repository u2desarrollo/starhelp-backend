<?php

namespace App\Console\Commands;

use App\Entities\User;
use App\Notifications\DataBaseBackupNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\PostgreSql;


class DataBaseBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup de la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connections =  Config::get('database.connections');

        $connection_default =  Config::get('database.default');

        $configuration = $connections[$connection_default];

        $diskBackup = Storage::disk('backup');

        $path = $diskBackup->getDriver()->getAdapter()->getPathPrefix();

        $fileName = Carbon::now()->format('YmdHis') . ".sql.gz";

        PostgreSql::create()
            ->setHost(addslashes($configuration['host']))
            ->setDbName(addslashes($configuration['database']))
            ->setUserName(addslashes($configuration['username']))
            ->setPassword(addslashes($configuration['password']))
            ->useCompressor(new GzipCompressor())
            ->dumpToFile($path . $fileName);


        $backup = $diskBackup->get($fileName);
        Storage::disk('s3')->put(config('app.path_backup_db') . $fileName, $backup);
        $diskBackup->delete($fileName);

        $emails = explode(';', config('app.emails_notification_backup'));

        if (count($emails)) {
            $users = User::whereIn('email', $emails)->get();
            if ($users->count()) {
                Notification::send($users, new DataBaseBackupNotification($fileName));
            }
        }

    }
}
