<?php

namespace App\Console\Commands;

use App\Entities\BranchOfficeWarehouse;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Entities\ContactCustomer;
use App\Entities\ContactDocument;
use App\Entities\ContactEmployee;
use App\Entities\ContactProvider;
use App\Entities\ContactWarehouse;
use App\Entities\Contact;
use App\Entities\User;
use App\Entities\Parameter;
use Illuminate\Support\Arr;


class UpdateContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:contact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var false|string
     */
    private $startTime;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {

            DB::beginTransaction();
            $this->startTime = date('Y-m-d H:i:s');

            $this->deleteData();
            $this->updateCustomers();
            $this->updateProviders();

//            DB::rollBack();
            DB::commit();
        } catch (\Exception $e) {
            echo $e->getMessage() . ' in the line ' . $e->getLine();
            Log::info($e->getMessage() . ' in the line ' . $e->getLine());
            DB::rollBack();
        }

        echo "FINALIZO CARGA/ACTUALIZACION DE TERCEROS";

    }

    public function updateProviders()
    {

        $index = [
            'identificationContact' => 0,
            'checkDigitContact' => 1,
            'businessNameContact' => 2,
            'addressContact' => 3,
            'cityContact' => 4,
            'faxContact' => 5,
            'mainTelephoneContact' => 6,
            'secondaryTelephoneContact' => 7,
            'cellphoneContact' => 8
        ];

        $file = fopen(storage_path('app/public/proveedores.csv'), "r");

        $cities = DB::table('cities')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
            $array = explode(";", $line);

            if (isset($array[1])) {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                //Consulta si existe el tercero
                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);
                $contact = Contact::where('identification', $array[$index['identificationContact']])->first();
                //Si no existe lo crea
                if (!$contact) {
                    $contact = Contact::create([
                        'subscriber_id' => 1,
                        'identification_type' => 2,
                        'identification' => $array[$index['identificationContact']],
                        'check_digit' => $array[$index['checkDigitContact']],
                        'name' => $array[$index['businessNameContact']],
                        'address' => $array[$index['addressContact']],
                        'main_telephone' => $array[$index['mainTelephoneContact']],
                        'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                        'cell_phone' => $array[$index['cellphoneContact']],
                        'fax' => $array[$index['faxContact']],
                        'is_provider' => true
                    ]);
                } else {
                    $contact->is_provider = true;
                    $contact->save();
                }


                //Consulta si existe el proveedor
                $contact_provider = ContactProvider::where('contact_id', $contact->id)->first();

                if (!$contact_provider) {
                    ContactCustomer::create([
                        'contact_id' => $contact->id,
                    ]);
                }

                $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => '000'])->first();

                if (!$contact_warehouse) {

                    $city_id = null;

                    foreach ($cities as $city) {
                        similar_text($city->city, $array[$index['cityContact']], $percent);
                        $percent = round($percent, 0);
                        if ($percent >= 30) {
                            $city_id = $city->id;
                            break;
                        }
                    }

                    ContactWarehouse::create([
                        'contact_id' => $contact->id,
                        'code' => '000',
                        'description' => $array[$index['businessNameContact']],
                        'address' => $array[$index['addressContact']],
                        'telephone' => $array[$index['mainTelephoneContact']],
                        'city_id' => $city_id,
                    ]);
                }


            }

            echo "Proveedores: " . ++$countContacts . "\n";

        }

    }

    /**
     * @return false|float
     */
    public function updateCustomers()
    {
        $index = [
            'identificationContact' => 0,
            'codeWarehouse' => 1,
            'physicalFileNumber' => 2,
            'yearRut' => 3,
            'checkDigitContact' => 4,
            'activeClient' => 5,
            'businessNameContact' => 6,
            'firstNameContact' => 7,
            'secondNameContact' => 8,
            'surnameContact' => 9,
            'secondSurnameContact' => 10,
            'classPerson' => 11,
            'taxRegime' => 12,
            'addressContact' => 13,
            'mainTelephoneContact' => 14,
            'secondaryTelephoneContact' => 15,
            'cellphoneContact' => 16,
            'emailContact' => 17,
            'dateOpening' => 18,
            'creditQuotaContact' => 19,
            'payDay' => 20,
            'emailSeller' => 22,
            'identificationSeller' => 23,
            'nameSeller' => 24,
            'surnameSeller' => 25,
            'cellphoneSeller' => 26,
            'branchofficeSeller' => 27,
            'tradenameContact' => 28,
        ];

        $file = fopen(storage_path('app/public/actualizacionterceros.csv'), "r");

        $cities = DB::table('cities')->get();
        $branchoffices = DB::table('branchoffices')->get();

        $countContacts = 0;

        while (!feof($file)) {

            $line = fgets($file);
           /* $line = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $line);
            $line = preg_replace('!\s+!', '', $line);*/
            $array = explode(";", $line);
            if (isset($array[1]) && $array[$index['activeClient']] != 'NO') {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $array[$i] = trim(utf8_encode($array[$i]));
                }

                $branchoffice_id = null;

                foreach ($branchoffices as $branchoffice) {
                    $branchoffice_name = str_replace("SEDE ", "", $branchoffice->name);
                    similar_text($branchoffice_name, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 60) {
                        $branchoffice_id = $branchoffice->id;
                        break;
                    }
                }

                $branchoffice_warehouse_id = null;

                if (!is_null($branchoffice_id)) {
                    $branchoffice_warehouse = BranchOfficeWarehouse::where('branchoffice_id', $branchoffice_id)->orderBy('warehouse_code', 'ASC')->first();
                    $branchoffice_warehouse_id = $branchoffice_warehouse->id;
                }

                $seller_id = null;

                //Consulta si existe el vendedor
                if ($array[$index['identificationSeller']] !== '') {
                    $array[$index['identificationSeller']] = str_replace('.', '', $array[$index['identificationSeller']]);

                    $array_seller = [
                        'subscriber_id' => 1,
                        'identification' => $array[$index['identificationSeller']],
                        'identification_type' => 2,
                        'name' => strtolower($array[$index['nameSeller']]),
                        'surname' => $array[$index['surnameSeller']],
                        'cell_phone' => $array[$index['cellphoneSeller']],
                        'email' => $array[$index['emailSeller']],
                        'is_employee' => true
                    ];

                    $seller = Contact::updateOrCreate(Arr::only($array_seller, ['identification']), $array_seller);

                    $array_contact_employee = [
                        'contact_id' => $seller->id,
                        'position_id' => 937
                    ];

                    ContactEmployee::updateOrCreate(Arr::only($array_contact_employee, ['contact_id']), $array_contact_employee);

                    $array_user = [
                        'contact_id' => $seller->id,
                        'identification_type' => 2,
                        'name' => $array[$index['nameSeller']],
                        'surname' => $array[$index['surnameSeller']],
                        'email' => strtolower($array[$index['emailSeller']]),
                        'password' => $array[$index['identificationSeller']],
                        'photo' => '/contacts/employees/default.jpg',
                        'module_id' => 3,
                        'branchoffice_id' => $branchoffice_id,
                        'branchoffice_warehouse_id' => $branchoffice_warehouse_id,
                        'subscriber_id' => 1,
                        'type' => 'e',
                        'password_changed' => true
                    ];

                    User::updateOrCreate(Arr::only($array_user, ['email']), $array_user);

                    $seller_id = $seller->id;

                }

                $array[$index['identificationContact']] = str_replace('.', '', $array[$index['identificationContact']]);

                if ($array[$index['firstNameContact']] == '') {
                    $identification_type = 1;
                    $name = $array[$index['businessNameContact']];
                    $surname = '';
                } else {
                    $identification_type = 2;
                    $name = $array[$index['firstNameContact']] . ' ' . $array[$index['secondNameContact']];
                    $surname = $array[$index['surnameContact']] . ' ' . $array[$index['secondSurnameContact']];
                }

                // Clase de persona
                $classPerson = Parameter::where('name_parameter', 'ilike', '%' . $array[$index['classPerson']] . '%')
                    ->where('paramtable_id', 2)
                    ->first();

                if (!$classPerson) {
                    $classPerson = Parameter::create([
                        'paramtable_id' => 2,
                        'code_parameter' => '000',
                        'name_parameter' => $array[$index['classPerson']]
                    ]);
                }

                // Tipo contribuytente
                $taxpayerType = Parameter::where('name_parameter', 'ilike', '%' . $array[$index['taxRegime']] . '%')
                    ->where('paramtable_id', 4)
                    ->first();

                if (!$taxpayerType) {
                    $taxpayerType = Parameter::create([
                        'paramtable_id' => 2,
                        'code_parameter' => '000',
                        'name_parameter' => $array[$index['taxRegime']]
                    ]);
                }

                //works correctly
                if (trim($array[$index['dateOpening']]) == '00/01/1900') {
                    $array[$index['dateOpening']] = null;
                }

                if (trim($array[$index['checkDigitContact']]) == '#N/D') {
                    $array[$index['checkDigitContact']] = null;
                }

                $array_contact = [
                    'subscriber_id' => 1,
                    'identification_type' => $identification_type,
                    'identification' => $array[$index['identificationContact']],
                    'check_digit' => $array[$index['checkDigitContact']],
                    'name' => $name,
                    'surname' => $surname,
                    'address' => $array[$index['addressContact']],
                    'main_telephone' => $array[$index['mainTelephoneContact']],
                    'secondary_telephone' => $array[$index['secondaryTelephoneContact']],
                    'cell_phone' => $array[$index['cellphoneContact']],
                    'email' => strtolower($array[$index['emailContact']]),
                    'is_customer' => true,

                    'physical_file_number' => $array[$index['physicalFileNumber']],
                    'class_person' => $classPerson->id,
                    'taxpayer_type' => $taxpayerType->id,
                    'pay_day' => $array[$index['payDay']],
                    'tradename' => $array[$index['tradenameContact']]
                ];

                if ($array[$index['dateOpening']] != null) {
                    $array_contact['date_opening'] = $array[$index['dateOpening']];
                }

                $contact = Contact::updateOrCreate(Arr::only($array_contact, ['identification']), $array_contact);

                $array[$index['creditQuotaContact']] = str_replace('.', '', $array[$index['creditQuotaContact']]);
                $array[$index['creditQuotaContact']] = str_replace(',', '', $array[$index['creditQuotaContact']]);

                $array_contact_customer = [
                    'contact_id' => $contact->id,
                    'credit_quota' => (int)$array[$index['creditQuotaContact']]  ,
                    //'expiration_days' => $array[$index['expirationDaysContact']]
                ];

                ContactCustomer::updateOrCreate(Arr::only($array_contact_customer, ['contact_id']), $array_contact_customer);

                $city_id = null;

                foreach ($cities as $city) {
                    similar_text($city->city, $array[$index['branchofficeSeller']], $percent);
                    $percent = round($percent, 0);
                    if ($percent >= 50) {
                        $city_id = $city->id;
                        break;
                    }
                }

                $array_contact_warehouse = [
                    'contact_id' => $contact->id,
                    'code' => str_pad($array[$index['codeWarehouse']], 3, "0", STR_PAD_LEFT),
                    'description' => $array[$index['businessNameContact']],
                    'address' => $array[$index['addressContact']],
                    'telephone' => $array[$index['mainTelephoneContact']],
                    'city_id' => $city_id,
                    'seller_id' => $seller_id,
                    'credit_quota' => (int)$array[$index['creditQuotaContact']],
                    'branchoffice_id' => $branchoffice_id
                ];

                ContactWarehouse::updateOrCreate(Arr::only($array_contact_warehouse, ['contact_id', 'code']), $array_contact_warehouse);
                echo "Clientes: " . ++$countContacts . "\n";

            }

        }

    }

    private function deleteData()
    {

        /*$tables = [
            'contacts_warehouses',
            'contacts_providers',
            'contacts_customers',
            'contacts_employees',
            'contacts',
        ];*/

        $this->dropConstraints();

        $this->truncateTables();

       /* foreach ($tables as $table) {
            DB::table($table)
                ->whereNull('updated_at')
                ->orWhere('updated_at', '<', $this->startTime)
                ->delete();
        }*/

        $this->addConstraints();
    }

    public function dropConstraints(): void
    {
        DB::select('ALTER TABLE public.documents_products_lots DROP CONSTRAINT documents_products_lots_documents_products_id_foreign;');
        DB::select('ALTER TABLE public.documents_products_taxes DROP CONSTRAINT documents_products_taxes_document_product_id_foreign;');
        DB::select('ALTER TABLE public.cash_receipts_consigned DROP CONSTRAINT cash_receipts_consigned_document_id_foreign;');
        DB::select('ALTER TABLE public.contribution_invoices_app DROP CONSTRAINT contribution_invoices_app_document_id_foreign;');
        DB::select('ALTER TABLE public.contribution_invoices_app DROP CONSTRAINT contribution_invoices_app_cash_receipts_app_id_foreign;');
        DB::select('ALTER TABLE public.documents_balance DROP CONSTRAINT documents_balance_document_id_foreign;');
        DB::select('ALTER TABLE public.documents_transactions DROP CONSTRAINT documents_transactions_document_id_foreign;');
        DB::select('ALTER TABLE public.receivables DROP CONSTRAINT receivables_invoice_id_foreign;');
        DB::select('ALTER TABLE public.pay_receivables DROP CONSTRAINT pay_receivables_receivable_id_foreign;');
        DB::select('ALTER TABLE public.service_requests_status_history DROP CONSTRAINT service_requests_status_history_service_requests_id_foreign;');
        DB::select('ALTER TABLE public.payment_methods_app DROP CONSTRAINT payment_methods_app_cash_receipts_app_id_foreign;');
        DB::select('ALTER TABLE public.contacts_warehouses DROP CONSTRAINT contacts_warehouses_seller_id_foreign;');
        DB::select('ALTER TABLE public.contacts_warehouses_lines DROP CONSTRAINT contacts_warehouses_lines_contact_warehouse_id_foreign;');
        DB::select('ALTER TABLE public.documents_informations DROP CONSTRAINT documents_informations_delivery_establishment_foreign;');
        DB::select('ALTER TABLE public.request_headers DROP CONSTRAINT request_headers_contacts_warehouse_id_foreign;');
        DB::select('ALTER TABLE public.request_products DROP CONSTRAINT request_products_request_header_id_foreign;');
        DB::select('ALTER TABLE public.visits_customers DROP CONSTRAINT visits_customers_contact_warehouse_id_foreign;');
        DB::select('ALTER TABLE public.brands DROP CONSTRAINT brands_provider_id_foreign;');
        DB::select('ALTER TABLE public.brands DROP CONSTRAINT brands_maker_id_foreign;');
        DB::select('ALTER TABLE public.cash_receipts_app DROP CONSTRAINT cash_receipts_app_contact_id_foreign;');
        DB::select('ALTER TABLE public.contact_line DROP CONSTRAINT contact_line_contact_id_foreign;');
        DB::select('ALTER TABLE public.contacts_clerks DROP CONSTRAINT contacts_clerks_contact_id_foreign;');

        DB::select('ALTER TABLE public.contacts_warehouses DROP CONSTRAINT contacts_warehouses_contact_id_foreign;');
        DB::select('ALTER TABLE public.contacts_providers DROP CONSTRAINT contacts_providers_contact_id_foreign;');
        DB::select('ALTER TABLE public.contacts_customers DROP CONSTRAINT contacts_customers_contact_id_foreign;');
        DB::select('ALTER TABLE public.contacts_employees DROP CONSTRAINT contacts_employees_contact_id_foreign;');
        DB::select('ALTER TABLE public.contacts_documents DROP CONSTRAINT contacts_documents_contact_id_foreign;');
        DB::select('ALTER TABLE public.documents DROP CONSTRAINT documents_seller_id_foreign;');
        DB::select('ALTER TABLE public.documents DROP CONSTRAINT documents_fk;');
        DB::select('ALTER TABLE public.documents_informations DROP CONSTRAINT documents_informations_additional_contact_foreign;');
        DB::select('ALTER TABLE public.documents_informations DROP CONSTRAINT documents_informations_domiciliary_foreign;');
        DB::select('ALTER TABLE public.documents_informations DROP CONSTRAINT documents_informations_protractor_foreign;');
        DB::select('ALTER TABLE public.documents_products DROP CONSTRAINT documents_products_technician_foreign;');
        DB::select('ALTER TABLE public.payment_methods DROP CONSTRAINT payment_methods_contact_id_foreign;');
        DB::select('ALTER TABLE public.price_contacts DROP CONSTRAINT price_contacts_contact_id_foreign;');
        DB::select('ALTER TABLE public.products DROP CONSTRAINT products_contact_id_foreign;');
        DB::select('ALTER TABLE public.request_headers DROP CONSTRAINT request_headers_contact_id_foreign;');
        DB::select('ALTER TABLE public.seller_coordinates DROP CONSTRAINT seller_coordinates_seller_id_foreign;');
        DB::select('ALTER TABLE public.service_requests DROP CONSTRAINT service_requests_contact_id_foreign;');
        DB::select('ALTER TABLE public.service_requests DROP CONSTRAINT service_requests_seller_id_foreign;');
        DB::select('ALTER TABLE public.templates DROP CONSTRAINT templates_contact_id_foreign;');
        DB::select('ALTER TABLE public.visits_customers DROP CONSTRAINT visits_customers_contact_id_foreign;');
        DB::select('ALTER TABLE public.visits_customers DROP CONSTRAINT visits_customers_seller_id_foreign;');
    }

    private function truncateTables(): void
    {
        DB::table('contacts_clerks')->truncate();
        DB::table('contact_line')->truncate();
        DB::table('request_products')->truncate();
        DB::table('price_contacts')->truncate();
        DB::table('request_headers')->truncate();
        DB::table('visits_customers')->truncate();
        DB::table('documents_informations')->truncate();
        DB::table('contacts_warehouses_lines')->truncate();
        DB::table('contacts_warehouses')->truncate();
        DB::table('contacts_providers')->truncate();
        DB::table('contacts_customers')->truncate();
        DB::table('contacts_employees')->truncate();
        DB::table('contacts')->truncate();
        DB::table('payment_methods')->truncate();
        DB::table('payment_methods_app')->truncate();
        DB::table('contribution_invoices_app')->truncate();
        DB::table('cash_receipts_app')->truncate();
        DB::table('service_requests_status_history')->truncate();
        DB::table('service_requests')->truncate();
        DB::table('seller_coordinates')->truncate();
        DB::table('documents_products_taxes')->truncate();
        DB::table('documents_products_lots')->truncate();
        DB::table('documents_products')->truncate();
        DB::table('cash_receipts_consigned')->truncate();
        DB::table('documents_balance')->truncate();
        DB::table('documents_transactions')->truncate();
        DB::table('receivables')->truncate();
        DB::table('pay_receivables')->truncate();
        DB::table('documents')->truncate();
    }

    private function addConstraints(): void
    {
        DB::select('ALTER TABLE public.documents_products_lots ADD CONSTRAINT documents_products_lots_documents_products_id_foreign FOREIGN KEY (documents_products_id) REFERENCES public.documents_products(id);');
        DB::select('ALTER TABLE public.documents_products_taxes ADD CONSTRAINT documents_products_taxes_document_product_id_foreign FOREIGN KEY (document_product_id) REFERENCES public.documents_products(id);');
        DB::select('ALTER TABLE public.cash_receipts_consigned ADD CONSTRAINT cash_receipts_consigned_document_id_foreign FOREIGN KEY (document_id) REFERENCES public.documents(id);');
        DB::select('ALTER TABLE public.contribution_invoices_app ADD CONSTRAINT contribution_invoices_app_document_id_foreign FOREIGN KEY (document_id) REFERENCES public.documents(id);');
        DB::select('ALTER TABLE public.contribution_invoices_app ADD CONSTRAINT contribution_invoices_app_cash_receipts_app_id_foreign FOREIGN KEY (cash_receipts_app_id) REFERENCES public.cash_receipts_app(id);');
        DB::select('ALTER TABLE public.documents_balance ADD CONSTRAINT documents_balance_document_id_foreign FOREIGN KEY (document_id) REFERENCES public.documents(id);');
        DB::select('ALTER TABLE public.documents_transactions ADD CONSTRAINT documents_transactions_document_id_foreign FOREIGN KEY (document_id) REFERENCES public.documents(id);');
        DB::select('ALTER TABLE public.receivables ADD CONSTRAINT receivables_invoice_id_foreign FOREIGN KEY (invoice_id) REFERENCES public.documents(id);');
        DB::select('ALTER TABLE public.pay_receivables ADD CONSTRAINT pay_receivables_receivable_id_foreign FOREIGN KEY (receivable_id) REFERENCES public.receivables(id);');
        DB::select('ALTER TABLE public.service_requests_status_history ADD CONSTRAINT service_requests_status_history_service_requests_id_foreign FOREIGN KEY (service_requests_id) REFERENCES public.service_requests(id);');
        DB::select('ALTER TABLE public.payment_methods_app ADD CONSTRAINT payment_methods_app_cash_receipts_app_id_foreign FOREIGN KEY (cash_receipts_app_id) REFERENCES public.cash_receipts_app(id);');
        DB::select('ALTER TABLE public.contacts_warehouses ADD CONSTRAINT contacts_warehouses_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_warehouses_lines ADD CONSTRAINT contacts_warehouses_lines_contact_warehouse_id_foreign FOREIGN KEY (contact_warehouse_id) REFERENCES public.contacts_warehouses(id);');
        DB::select('ALTER TABLE public.documents_informations ADD CONSTRAINT documents_informations_delivery_establishment_foreign FOREIGN KEY (delivery_establishment) REFERENCES public.contacts_warehouses(id);');
        DB::select('ALTER TABLE public.request_headers ADD CONSTRAINT request_headers_contacts_warehouse_id_foreign FOREIGN KEY (contacts_warehouse_id) REFERENCES public.contacts_warehouses(id);');
        DB::select('ALTER TABLE public.request_products ADD CONSTRAINT request_products_request_header_id_foreign FOREIGN KEY (request_header_id) REFERENCES public.request_headers(id);');
        DB::select('ALTER TABLE public.visits_customers ADD CONSTRAINT visits_customers_contact_warehouse_id_foreign FOREIGN KEY (contact_warehouse_id) REFERENCES public.contacts_warehouses(id);');
        DB::select('ALTER TABLE public.brands ADD CONSTRAINT brands_provider_id_foreign FOREIGN KEY (provider_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.brands ADD CONSTRAINT brands_maker_id_foreign FOREIGN KEY (maker_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.cash_receipts_app ADD CONSTRAINT cash_receipts_app_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contact_line ADD CONSTRAINT contact_line_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_clerks ADD CONSTRAINT contacts_clerks_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');

        DB::select('ALTER TABLE public.contacts_warehouses ADD CONSTRAINT contacts_warehouses_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_providers ADD CONSTRAINT contacts_providers_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_customers ADD CONSTRAINT contacts_customers_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_employees ADD CONSTRAINT contacts_employees_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.contacts_documents ADD CONSTRAINT contacts_documents_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents ADD CONSTRAINT documents_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents ADD CONSTRAINT documents_fk FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents_informations ADD CONSTRAINT documents_informations_additional_contact_foreign FOREIGN KEY (additional_contact) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents_informations ADD CONSTRAINT documents_informations_domiciliary_foreign FOREIGN KEY (domiciliary) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents_informations ADD CONSTRAINT documents_informations_protractor_foreign FOREIGN KEY (protractor) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.documents_products ADD CONSTRAINT documents_products_technician_foreign FOREIGN KEY (technician­_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.payment_methods ADD CONSTRAINT payment_methods_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.price_contacts ADD CONSTRAINT price_contacts_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.products ADD CONSTRAINT products_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.request_headers ADD CONSTRAINT request_headers_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.seller_coordinates ADD CONSTRAINT seller_coordinates_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.service_requests ADD CONSTRAINT service_requests_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.service_requests ADD CONSTRAINT service_requests_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.templates ADD CONSTRAINT templates_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.visits_customers ADD CONSTRAINT visits_customers_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.contacts(id);');
        DB::select('ALTER TABLE public.visits_customers ADD CONSTRAINT visits_customers_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.contacts(id);');
    }

}
