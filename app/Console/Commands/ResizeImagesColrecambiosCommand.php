<?php

namespace App\Console\Commands;

use App\Entities\Product;
use App\Entities\ProductAttachment;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ResizeImagesColrecambiosCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:images_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws FileNotFoundException
     */
    public function handle()
    {
        $private_disk = Storage::disk('private');
        $public_disk = Storage::disk('public');
        $products_not_found = [];
        $without_folder = [];
        $replacements = [
            ' (piunto)',
            ' (punto)',
            '(punto)',
            ' punto',
            'punto',
        ];

        $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];

        $paths = $private_disk->allFiles('products');

        try {
            DB::beginTransaction();

            DB::select('TRUNCATE TABLE public.products_attachments RESTART IDENTITY CASCADE;');

            $counter = 0;
            $counter_saved = 0;
            $counter_they_are_not_images = 0;

            Log::info('Total archivos: ' . count($paths));

            foreach ($paths as $key => $path) {

                $parts_path = explode('/', $path);

                $file_name = array_pop($parts_path);

                $parts_file_name = explode('.', $file_name);

                $extension = array_pop($parts_file_name);

                if (in_array($extension, $extensions)) {
                    if (count($parts_path) >= 1) {

                        $length_parts_path = count($parts_path);

                        $product_code = strtolower($parts_path[$length_parts_path - 1]);

                        foreach ($replacements as $replacement) {
                            $product_code = str_replace($replacement, "", $product_code);
                        }

                        $product = Product::select('id')
                            ->whereRaw('LOWER(code) = ?', [trim($product_code)])
                            ->with('productsAttachments:id,product_id')
                            ->first();

                        if ($product) {

                           $image = $private_disk->get($path);

                            $public_disk->put('products/web/' . $file_name, $image);

                            $resized_image = Image::make(storage_path('/app/private/') . $path)
                                ->resize(250, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                })->encode('jpg', 75);;

                            $resized_image->save(storage_path('/app/public/products/app/' . $file_name));

                            ProductAttachment::create([
                                'product_id'  => $product->id,
                                'description' => '',
                                'url'         => $file_name,
                                'type'        => 'i',
                                'main'        => !$product->productsAttachments->count()
                            ]);

                            $counter_saved++;
                        } else {
//                            array_push($products_not_found, '"' . strtoupper($product_code) . '"');
                            array_push($products_not_found, '"' . strtoupper($path) . '"');
                        }
                    } else {
                        array_push($without_folder, $path);
                    }
                } else {
                    $counter_they_are_not_images++;
                }

                $counter++;

                if ($counter % 100 == 0) {
                    Log::info($counter . ' imágenes procesadas.');
                }
            }

            Log::info('Total imágenes guardadas: ' . $counter_saved);
            Log::info('Total de imágenes que no existen: ' . count($products_not_found));
            Log::info('Total archivos que no tienen carpeta: ' . count($without_folder));
            Log::info('Total archivos que no son imágenes: ' . $counter_they_are_not_images);

            $products_not_found = collect($products_not_found)->unique()->toArray();

            Log::info('Referencias que no existen: '. count($products_not_found));
            Log::info(implode(", \n", $products_not_found));

            DB::commit();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }
    }


}
