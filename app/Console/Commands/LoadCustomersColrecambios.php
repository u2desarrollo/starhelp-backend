<?php

namespace App\Console\Commands;

use App\Entities\City;
use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\ContactEmployee;
use App\Entities\ContactProvider;
use App\Entities\ContactWarehouse;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoadCustomersColrecambios extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:customers_colrecambios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var false|string
     */
    private $startTime;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setSpreadSheet('clientes Final.xlsx');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('BASE');
        $validator = true;
        $contacts = [];
        $row = 2;

        DB::beginTransaction();

        while ($validator) {

            // Obtiene el número de identificación del tercero
            $identification = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($identification)) {

                $check_digit = trim($this->sheet->getCell('B' . $row)->getValue());
                $first_surname = strtoupper(trim($this->sheet->getCell('C' . $row)->getValue()));
                $second_surname = strtoupper(trim($this->sheet->getCell('D' . $row)->getValue()));
                $first_name = strtoupper(trim($this->sheet->getCell('E' . $row)->getValue()));
                $second_name = strtoupper(trim($this->sheet->getCell('F' . $row)->getValue()));
                $taxpayer_type = strtoupper(trim($this->sheet->getCell('G' . $row)->getValue()));
                $name = strtoupper(trim($this->sheet->getCell('H' . $row)->getValue()));
                $tradename = strtoupper(trim($this->sheet->getCell('I' . $row)->getValue()));
                $address = trim($this->sheet->getCell('K' . $row)->getValue());
                $main_telephone = trim($this->sheet->getCell('M' . $row)->getValue());
                $cell_phone = trim($this->sheet->getCell('N' . $row)->getValue());
                $email = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', strtolower(trim($this->sheet->getCell('O' . $row)->getValue())));
                $email_electronic_invoice = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', strtolower(trim($this->sheet->getCell('P' . $row)->getValue())));
                $code_city = trim($this->sheet->getCell('Q' . $row)->getValue());
                $discount = trim($this->sheet->getCell('T' . $row)->getValue());
                $credit_quota = trim($this->sheet->getCell('U' . $row)->getValue());
                $date_opening = trim($this->sheet->getCell('V' . $row)->getValue());
                $expiration_days = (int)trim($this->sheet->getCell('W' . $row)->getValue());
                $observations = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($this->sheet->getCell('X' . $row)->getValue()));
                $tax_regime = trim($this->sheet->getCell('Y' . $row)->getValue());
                $fiscal_responsibility = trim($this->sheet->getCell('AE' . $row)->getValue());
                $identification_seller = trim($this->sheet->getCell('AC' . $row)->getValue());
                $name_seller = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', strtoupper(trim($this->sheet->getCell('AC' . $row)->getValue())));
                $code_ciiu = strtoupper(trim($this->sheet->getCell('AD' . $row)->getValue()));

                $date_opening = Date::excelToDateTimeObject($date_opening)->format('Y-m-d');

                //Consulta si existe el tercero
                $identification = str_replace('.', '', $identification);
                $contact = Contact::where('identification', $identification)->first();
                $code_contact_warehouse = '000';

                $city_id = null;

                if (!empty($code_city)) {
                    $city = City::whereRaw('concat(department_id, city_code) = ?', [$code_city])
                        ->orWhereRaw('concat(city_code, department_id) = ?', [$code_city])
                        ->first();
                    if ($city) {
                        $city_id = $city->id;
                    }
                }

                if (!in_array($identification, $contacts)) {

                    array_push($contacts, $identification);

                    $fiscal_responsibility_id = null;

                    if (!empty($fiscal_responsibility)) {
//                    if ($fiscal_responsibility == 'NO') {
                        $fiscal_responsibility_id = 24568;
//                    }
                    }

                    $tax_regime_id = null;

                    if (!empty($tax_regime)) {
                        if ($tax_regime == 'S') {
                            $tax_regime_id = 16;
                        } else if ($tax_regime == 'C') {
                            $tax_regime_id = 11;
                        }
                    }

                    $taxpayer_type_id = null;

                    if (!empty($taxpayer_type)) {
                        if ($tax_regime == 'NATURAL') {
                            $taxpayer_type_id = 14;
                        } else if ($taxpayer_type == 'JURIDICA') {
                            $taxpayer_type_id = 13;
                        }
                    }

                    //Si no existe lo crea
                    if (!$contact) {
                        $contact = Contact::create([
                            'subscriber_id'            => 3,
                            'identification'           => $identification,
                            'check_digit'              => $check_digit,
                            'identification_type'      => empty($first_surname) ? 1 : 2,
                            'name'                     => empty($first_surname) ? $name : $first_name . ' ' . $second_name,
                            'surname'                  => empty($first_surname) ? '' : $first_surname . ' ' . $second_surname,
                            'tradename'                => $tradename,
                            'tax_regime'               => $tax_regime_id,
                            'taxpayer_type'            => $taxpayer_type_id,
                            'fiscal_responsibility_id' => $fiscal_responsibility_id,
                            'address'                  => $address,
                            'main_telephone'           => $main_telephone,
                            'cell_phone'               => $cell_phone,
                            'date_opening'             => $date_opening,
                            'city_id'                  => $city_id,
                            'email'                    => $email,
                            'email_electronic_invoice' => $email_electronic_invoice,
                            'code_ciiu'                => $code_ciiu,
                            'observations'             => $observations,
                            'is_customer'              => true
                        ]);
                    } else {
                        $contact->subscriber_id = 3;
                        $contact->check_digit = $check_digit;
                        $contact->identification_type = empty($first_surname) ? 1 : 2;
                        $contact->name = empty($first_surname) ? $name : $first_name . ' ' . $second_name;
                        $contact->surname = empty($first_surname) ? '' : $first_surname . ' ' . $second_surname;
                        $contact->tradename = $tradename;
                        $contact->tax_regime = $tax_regime_id;
                        $contact->taxpayer_type = $taxpayer_type_id;
                        $contact->fiscal_responsibility_id = $fiscal_responsibility_id;
                        $contact->address = $address;
                        $contact->main_telephone = $main_telephone;
                        $contact->cell_phone = $cell_phone;
                        $contact->date_opening = $date_opening;
                        $contact->city_id = $city_id;
                        $contact->email = $email;
                        $contact->email_electronic_invoice = $email_electronic_invoice;
                        $contact->code_ciiu = $code_ciiu;
                        $contact->observations = $observations;
                        $contact->is_customer = true;
                        $contact->save();
                    }


                    //Consulta si existe el cliente
                    $contact_customer = ContactCustomer::where('contact_id', $contact->id)->first();

                    if (!$contact_customer) {
                        ContactCustomer::create([
                            'contact_id'          => $contact->id,
                            'credit_quota'        => $credit_quota,
                            'expiration_days'     => $expiration_days,
                            'percentage_discount' => $discount
                        ]);
                    } else {
                        $contact_customer->credit_quota = $credit_quota;
                        $contact_customer->expiration_days = $expiration_days;
                        $contact_customer->percentage_discount = $discount;
                        $contact_customer->save();
                    }
                }else {
                    $last_contact_warehouse = ContactWarehouse::select('code')
                        ->where('contact_id', $contact->id)
                        ->orderBy('code', 'DESC')
                        ->first();

                    if ($last_contact_warehouse) {
                        $code_as_int = (int)$last_contact_warehouse->code + 1;
                        $code_contact_warehouse = str_pad($code_as_int, 3, "0", STR_PAD_LEFT);
                    }
                }

                $contact_warehouse = ContactWarehouse::where(['contact_id' => $contact->id, 'code' => $code_contact_warehouse])->first();

                $seller_id = null;

                if (!empty($identification_seller)) {

                    //Consulta si existe el vendedor
                    $identification_seller = str_replace('.', '', $identification_seller);
                    $seller = Contact::where('identification', $identification_seller)->first();

                    //Si no existe lo crea
                    if (!$seller) {
                        $seller = Contact::create([
                            'subscriber_id'       => 3,
                            'identification'      => $identification_seller,
                            'identification_type' => 2,
                            'name'                => $name_seller,
                            'is_employee'         => true
                        ]);

                        ContactEmployee::create([
                            'contact_id' => $seller->id,
                        ]);
                    }

                    $seller_id = $seller->id;
                }

                if (!$contact_warehouse) {
                    ContactWarehouse::create([
                        'contact_id'          => $contact->id,
                        'code'                => $code_contact_warehouse,
                        'description'         => !empty($tradename) ? $tradename : (empty($first_surname) ? $name : $first_name . ' ' . $second_name . ' ' . $first_surname . ' ' . $second_surname),
                        'email'               => $email,
                        'address'             => $address,
                        'telephone'           => $main_telephone,
                        'cellphone'           => $cell_phone,
                        'credit_quota'        => $credit_quota,
                        'pay_condition'       => $expiration_days,
                        'city_id'             => $city_id,
                        'seller_id'           => $seller_id,
                        'discount_percentage' => $discount,
                        'branchoffice_id'     => 5
                    ]);
                } else {
                    $contact_warehouse->description = !empty($tradename) ? $tradename : (empty($first_surname) ? $name : $first_name . ' ' . $second_name . ' ' . $first_surname . ' ' . $second_surname);
                    $contact_warehouse->email = $email;
                    $contact_warehouse->address = $address;
                    $contact_warehouse->telephone = $main_telephone;
                    $contact_warehouse->cellphone = $cell_phone;
                    $contact_warehouse->credit_quota = $credit_quota;
                    $contact_warehouse->pay_condition = $expiration_days;
                    $contact_warehouse->city_id = $city_id;
                    $contact_warehouse->seller_id = $seller_id;
                    $contact_warehouse->discount_percentage = $discount;
                    $contact_warehouse->branchoffice_id = 5;
                    $contact_warehouse->save();
                }

            } else {
                $validator = false;
            }

            $row++;

            echo $row . "\n";
        }

        DB::commit();

        echo "FINALIZO CARGA DE CLIENTES";
    }


}
