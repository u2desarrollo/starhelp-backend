<?php

namespace App\Console\Commands;

use App\Services\InventaryOperationService;
use Illuminate\Console\Command;

class InventoryOperation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:InventoryOperation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $a = new InventaryOperationService();
            $a->readFile();
            \Log::info('fin , proceso exitoso de sincronizacion de operaciones de inventario');
        } catch (\Throwable $th) {
            \Log::info($th);
        }
    }
}
