<?php

namespace App\Console\Commands;

use App\Entities\DocumentTransaction;
use App\Services\SyncDocumentsService;
use App\Services\SyncTransactionsService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SyncTransactionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $instance = new SyncTransactionsService();

        if (config('app.test_cron')) {
            $instance->syncData(null);
        } else {
            $filesTransactions = collect();

            // ******* posibilidad de cambio    ****
            $sftp = Storage::disk('sftp');

            $namesFiles = $sftp->files('WEB/PENDING');

            //decido a que Array deben de ir cada nombre de archivo dependiendo del mismo
            foreach ($namesFiles as $key => $value) {
                $part = explode('/', $value);
                if (substr($part[count($part) - 1], 0, 6) == 'TRAERP') {
                    $filesTransactions->push($part[count($part) - 1]);
                }
            }

            Log::info('Archivos encontrados de transacciones en el servidor SFTP' . json_encode($filesTransactions->toArray()));

            //documents_transactions
            foreach ($filesTransactions as $key => $elm) {
                $file = $sftp->get('WEB/PENDING/' . $elm);
                Storage::disk('local')->put('public/portfolio/' . $elm, $file);
                $sftp->delete('WEB/PENDING/' . $elm);
            }

            //Cierra lo conexión sftp
            $sftp->getDriver()->getAdapter()->disconnect();


            foreach ($filesTransactions as $key => $element) {
                try {
                    DB::beginTransaction();
                    $instance->syncData($element);
                    DB::commit();
                } catch (\Exception $th) {
                    \Log::error($th->getTrace());
                    DB::rollBack();
                }
            }

            Log::info('Sincronización de transacciones finalizado con exito');
        }


    }
}
