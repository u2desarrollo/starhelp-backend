<?php

namespace App\Console\Commands;

use App\Entities\Contact;
use App\Entities\ContactWarehouse;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class UpdateCitiesContactsAndContactsWarehouses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = fopen(storage_path('app/public/ciudadesterceros.csv'), "r");

        $cities = DB::select('select c.id, c.city, d.department from cities c inner join departments d on d.id = c.department_id ');

        $countCities = 0;

        while (!feof($file)) {

            $line = fgets($file);

            $array = explode(";", $line);

            $array[0] = str_replace('.', '', $array[0]);

            $city_id = null;

            if (isset($array[1])) {


                foreach ($cities as $city) {
                    $city_and_department = explode('-', $array[2]);

                    similar_text($city->city, trim($city_and_department[0]), $percent_city);
                    $percent_city = round($percent_city, 0);

                    similar_text($city->department, trim($city_and_department[1]), $percent_department);
                    $percent_department = round($percent_department, 0);
                    if ($percent_city >= 70 && $percent_department >= 50) {
                        $city_id = $city->id;
                        echo $countCities++ . " " . $city->city . " " . $city_and_department[0] . " " . $city->department . " " . $city_and_department[1] . "\n";
                        break;
                    }
                }

                if (!is_null($city_id)) {

                    $array_contact = [
                        'identification' => trim($array[0]),
                        'city_id' => $city_id
                    ];

                    Contact::where(Arr::only($array_contact, ['identification']))->update($array_contact);

                    $contact = Contact::where(Arr::only($array_contact, ['identification']))->first();

                    if ($contact) {
                        $array_contact_warehouse = [
                            'contact_id' => $contact->id,
                            'code' => str_pad($array[1], 3, "0", STR_PAD_LEFT),
                            'city_id' => $city_id
                        ];

                        ContactWarehouse::where(Arr::only($array_contact_warehouse, ['contact_id', 'code']))
                            ->update($array_contact_warehouse);
                    }
                }
            }
        }
    }
}
