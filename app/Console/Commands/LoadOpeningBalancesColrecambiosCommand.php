<?php

namespace App\Console\Commands;

use App\Entities\Account;
use App\Entities\Document;
use App\Entities\DocumentProduct;
use App\Entities\DocumentTransaction;
use App\Entities\DocumentTransactions;
use App\Entities\Product;
use App\Entities\Template;
use App\Entities\TypesOperation;
use App\Entities\VouchersType;
use App\Traits\LoadDocumentsColrecambiosTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class LoadOpeningBalancesColrecambiosCommand extends Command
{
    use LoadDocumentsColrecambiosTrait;

    private $spreadsheet;
    private $sheet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:opening_balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->setSpreadSheet('saldos_iniciales.xlsx');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setSheet('FORMATO OK');
        $validator = true;
        $accounts_not_found = [];
        $types_documents_not_found = [];
        $contacts_not_found = [];
        $contacts_warehouses_not_found = [];

        $voucher_types = [
            'ajuste contable'       => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '455')->first()->id,
                'model_id'         => null,
                'operationType_id' => null
            ],
            'anticipo'              => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '486')->first()->id,
                'model_id'         => Template::where('code', '486')->first()->id,
                'operationType_id' => null
            ],
            'comprobante de egreso' => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '410')->first()->id,
                'model_id'         => null,
                'operationType_id' => null
            ],
            'doc. de nomina'        => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '430')->first()->id,
                'model_id'         => null,
                'operationType_id' => null
            ],
            'factura de compra'     => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '105')->first()->id,
                'model_id'         => Template::where('code', '105')->first()->id,
                'operationType_id' => TypesOperation::where('code', '105')->first()->id
            ],
            'factura de venta'      => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '205')->first()->id,
                'model_id'         => Template::where('code', '205')->first()->id,
                'operationType_id' => TypesOperation::where('code', '205')->first()->id
            ],
            'nota bancaria'         => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '450')->first()->id,
                'model_id'         => null,
                'operationType_id' => null
            ],
            'nota credito'          => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '155')->first()->id,
                'model_id'         => Template::where('code', '155')->first()->id,
                'operationType_id' => TypesOperation::where('code', '155')->first()->id
            ],
            'recibo de caja'        => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '310')->first()->id,
                'model_id'         => Template::where('code', '310')->first()->id,
                'operationType_id' => null
            ],
            'saldo inicial'         => [
                'vouchertype_id'   => VouchersType::where('code_voucher_type', '500')->first()->id,
                'model_id'         => null,
                'operationType_id' => null
            ]
        ];

        $row = 3;

        DB::beginTransaction();

        while ($validator) {

            echo $row . "\n";

            $account_code = trim($this->sheet->getCell('A' . $row)->getValue());

            if (!empty($account_code)) {

                // ------------------- VARIABLES -------------------//
                $contact_id = 389;
                $seller_id = null;
                $contact_warehouse_id = 1524;
                $document_date = Carbon::parse('2020-12-31');
                $due_date = Carbon::parse('2020-12-31');
                $account = Account::where('code', $account_code)->first();
                $document_date_as_string = trim($this->sheet->getCell('I' . $row)->getValue());
                $due_date_as_string = trim($this->sheet->getCell('J' . $row)->getValue());
                $identification = trim($this->sheet->getCell('C' . $row)->getValue());
                $name = trim($this->sheet->getCell('D' . $row)->getValue());
                $identification_seller = trim($this->sheet->getCell('L' . $row)->getValue());
                $name_seller = trim($this->sheet->getCell('M' . $row)->getValue());
                $consecutive = trim($this->sheet->getCell('F' . $row)->getValue());
                $document_type = strtolower(trim($this->sheet->getCell('E' . $row)->getValue()));
                $operation_value = (float)trim($this->sheet->getCell('N' . $row)->getValue()) + (float)trim($this->sheet->getCell('O' . $row)->getValue());
                $transaction_type = (float)trim($this->sheet->getCell('N' . $row)->getValue()) != 0 ? 'D' : 'C';

                if (!empty($identification_seller)) {
                    $seller = $this->getSeller($identification_seller, $name_seller);
                    $seller_id = $seller->id;
                }

                // Validar si existe el tipo de documento
                if (!isset($voucher_types[$document_type])) {
                    array_push($types_documents_not_found, $document_type);
                    $row++;
                    continue;
                }

                // Validar si existe la cuenta
                if (!$account) {
                    array_push($accounts_not_found, $account_code);
                    $row++;
                    continue;
                }

                // Validar si el tipo de identificación del tercero no está vacío
                if (!empty($identification)) {
                    // Consultar tercero por numero de identificación
                    $contact = $this->getContact($identification);

                    if (!$contact) {
                        array_push($contacts_not_found, $identification . ' ' . $name);
                        $row++;
                        continue;
                    }

                    $contact_id = $contact->id;

                    $contact_warehouse_id = null;
                    $contact_warehouse = $this->getContactWarehouse($contact->id);

                    if ($contact_warehouse) {
                        $contact_warehouse_id = $contact_warehouse->id;
                    }
                }

                // Validar si la fecha no está vacía
                if (!empty($document_date_as_string)) {
                    $document_date = Carbon::parse($document_date_as_string);
                    $due_date = Carbon::parse($due_date_as_string);
                }

                // Validar el tipo de documento para dar manejo al consecutivo
                if ($document_type == 'factura de venta') {
                    $consecutive = trim($this->sheet->getCell('H' . $row)->getValue());
                } else if ($document_type == 'saldo inicial') {
                    $consecutive = 1;
                }

                // Combinar el array con la información del producto
                $data_document = array_merge($voucher_types[$document_type], [
                    'prefix'                    => trim($this->sheet->getCell('G' . $row)->getValue()),
                    'consecutive'               => $consecutive,
                    'branchoffice_id'           => 5,
                    'branchoffice_warehouse_id' => 1,
                    'issue_date'                => $document_date->format('Y-m-d'),
                    'document_date'             => $document_date->format('Y-m-d H:i:s'),
                    'document_last_state'       => $document_date->format('Y-m-d'),
                    'due_date'                  => $due_date->format('Y-m-d'),
                    'contact_id'                => $contact_id,
                    'warehouse_id'              => $contact_warehouse_id,
                    'seller_id'                 => $seller_id,
                    'user_id'                   => 44,
                    'observation'               => trim($this->sheet->getCell('K' . $row)->getValue()),
                    'in_progress'               => false,
                    'origin_document'           => true
                ]);

                // Consultar el documento
                $document = Document::where(Arr::only($data_document, ['prefix', 'consecutive', 'vouchertype_id', 'in_progress']))
                    ->first();

                if (!$document) {
                    // Validar el tipo de documento para dar manejo al consecutivo
                    if ($document_type == 'factura de venta') {
                        $data_document['model_id'] = Template::where('code', '018')->first()->id;
                    }

                    $document = Document::create($data_document);

                    $document->thread = $document->id;
                    $document->affects_document_id = $document->id;
                    $document->affects_prefix_document = $document->prefix;
                    $document->affects_number_document = $document->consecutive;
                    $document->affects_date_document = $document->document_date;

                    $document->save();
                }

                // Validar si existe el document_information
                $this->createDocumentInformation($document);

                DocumentTransactions::create([
                    'affects_document_id' => $document->affects_document_id,
                    'account_id'          => $account->id,
                    'document_id'         => $document->id,
                    'contact_id'          => $contact_id,
                    'year_month'          => $document_date->format('Ym'),
                    'operation_value'     => $operation_value,
                    'transactions_type'   => $transaction_type,
                    'origin_document'     => $document->origin_document
                ]);

            } else {
                $validator = false;
            }

            $row++;
        }

        Log::info('// ------------------------------------- Opening Balances');
        if (count($contacts_not_found) || count($contacts_warehouses_not_found) || count($accounts_not_found) || count($types_documents_not_found)) {
            Log::info('$contacts_not_found');
            $unique_contacts = collect($contacts_not_found)->unique()->toArray();
            Log::info(implode("\n", $unique_contacts));
//            Log::info(collect($contacts_not_found)->unique()->toArray());
            Log::info('$contacts_warehouses_not_found');
            Log::info(collect($contacts_warehouses_not_found)->unique()->toArray());
            Log::info('$accounts_not_found');
            Log::info(collect($accounts_not_found)->unique()->toArray());
            Log::info('$types_documents_not_found');
            Log::info(collect($types_documents_not_found)->unique()->toArray());
//            DB::rollBack();
        } /*else {*/
        DB::commit();
//        }

    }
}
