<?php

namespace App\Console\Commands;

use App\Mail\NotificationErrorCronMail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ValidateLastDatesSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastDate = Cache::get('lastDateSyncAll', now()->addMinutes(-11)->format('Y-m-d H:i:s'));

        $dayOfWeek = date('N');
        $hour = date('H');
        $lastHour = 17;

        if ($dayOfWeek > 5){
            $lastHour = 12;
        }

        if ($hour >= 9 && $hour <= $lastHour && $dayOfWeek < 7 && Carbon::parse($lastDate)->diffInMinutes(now()) > 30) {

            $subject = "ERROR EN EJECUCIÓN DE TAREAS CRON !!!";
            $text = "EL CRON DE CARTERA, RECIBOS DE CAJA Y OPERACIONES DE INVENTARIO NO SE HA EJECUTADO EN LOS ÚLTIMOS 30 MINUTOS";

            $emails = config('app.emails_notification_error_cron');

            $emailsAsArray = explode(';', $emails);

            Mail::to($emailsAsArray)->send(new NotificationErrorCronMail($text, $subject));

        }else if ($hour >= 9 && $hour <= $lastHour && $dayOfWeek < 7 && Carbon::parse($lastDate)->diffInMinutes(now()) >= 10){
            $lockSync = Cache::get('lockSyncColsaisa', false);

            if ($lockSync){
                Cache::put('lockSyncColsaisa', false, 600);
            }
        }
    }
}
