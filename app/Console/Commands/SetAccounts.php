<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\AccountService;

class SetAccounts extends Command
{
    private $accountService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cargar las cuentas desde un csv a la tabla acoounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AccountService $accountService)
    {
        parent::__construct();
        $this->accountService = $accountService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd($this->accountService->setAcoounts());
    }
}
