<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;

class UpdateImagesProductsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'images:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the images of the products';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$path_images = storage_path('app/public/products/');

		$images = dir($path_images);

		$count = 0;
		$notFound = [];

		try {
			DB::beginTransaction();

			while (($file = $images->read()) !== false) {
				$code = explode('.', $file);
				$product = DB::table('products')->where('code', $code[0])->first();

				if ($product) {
					DB::table('products_attachments')
					->updateOrInsert(['product_id' => $product->id], [
						'product_id' => $product->id,
						'main' => true,
						'url' => '/products/' . $file,
						'type' => 'i'
					]);

					$count++;
				} else {
					$notFound[] = $file;
				}
			}

			DB::commit();

			Log::info("$count imagenes actualizadas");

		} catch (\Exception $e) {
			DB::rollBack();
			Log::error('Error actualizando imagenes de productos '. $e->getMessage() . ' in the line ' . $e->getLine());
		}
	}
}
