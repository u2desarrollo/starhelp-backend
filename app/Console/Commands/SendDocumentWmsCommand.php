<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DocumentsService;
use Illuminate\Support\Facades\Log;

class SendDocumentWmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wms:senddocuments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command send documents entries to WMS';

    private $productDocument;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DocumentsService $productDocument)
    {
        parent::__construct();
        $this->productDocument = $productDocument;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('documentslog')->info("Init Consumed WebService WMS Documents Entries");
        $this->productDocument->createDocumentSoapWms();
        Log::channel('documentslog')->info("Final Consumed WebService WMS Documents Entries");
    }
}
