<?php

namespace App\Traits;
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 13/02/2019
 * Time: 3:31 PM
 */

use Illuminate\Support\Collection as Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


trait CollectionsTrait{

    public function paginateCollections($items, $perPage = 15, $page = null, $options = [])
    {
        $data = array_chunk($items, $perPage);

        $paginate = [];

        $paginate['data'] = isset($data[$page-1]) ? $data[$page-1] : [];
        $paginate['current_page'] = $page;
        $paginate['from'] = ($page - 1) * $perPage + 1;
        $paginate['last_page'] = count($data);
        $paginate['per_page'] = $perPage;
        $paginate['to'] = $page * $perPage;
        $paginate['total'] = count($items);

        return $paginate;
    }

}
