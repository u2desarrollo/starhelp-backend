<?php

namespace App\Traits;

use App\Entities\BranchOfficeWarehouse;
use App\Entities\Contact;
use App\Entities\ContactEmployee;
use App\Entities\ContactWarehouse;
use App\Entities\Document;
use App\Entities\DocumentInformation;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use PHPExcel_IOFactory;
use PhpOffice\PhpSpreadsheet\IOFactory;

trait LoadDocumentsColrecambiosTrait
{
    /**
     * @param string $file
     * @author Jhon García
     */
    public function setSpreadSheet($file = 'documentos_enero_agosto Final.xlsx')
    {
        /*try {
            $reader = IOFactory::createReaderForFile(storage_path() . "/app/public/" . $file);
            $this->spreadsheet = $reader->load(storage_path() . "/app/public/" . $file);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }*/
    }

    /**
     * @param $name_sheet
     * @author Jhon García
     */
    public function setSheet($name_sheet)
    {
        $this->sheet = $this->spreadsheet->getSheetByName($name_sheet);
    }

    /**
     * @param $identification
     * @return mixed
     */
    public function getContact($identification)
    {
        return Contact::select('id')
            ->where('identification', $identification)
            ->withTrashed()
            ->first();
    }

    /**
     * @param $identification_seller
     * @param $name
     * @return mixed
     */
    public function getSeller($identification_seller, $name)
    {
        // Consultar vendedor por numero de identificación
        $seller = $this->getContact($identification_seller);

        if (!$seller) {
            $seller = Contact::create([
                'subscriber_id'       => 3,
                'identification_type' => 1,
                'identification'      => $identification_seller,
                'name'                => $name,
                'is_employee'         => true
            ]);

            ContactEmployee::create(['contact_id' => $seller->id]);
        }

        return $seller;
    }

    /**
     * @param $data_document
     * @param bool $include_operation_type
     * @return mixed
     */
    private function getDocument($data_document, $include_operation_type = true)
    {
        $only = ['prefix', 'consecutive', 'vouchertype_id', 'in_progress'];

        if ($include_operation_type) {
            $only = ['prefix', 'consecutive', 'vouchertype_id', 'operationType_id'];
        }

        $document = Document::select('id', 'total_value', 'total_value_brut', 'origin_document')
            ->where(Arr::only($data_document, $only))
            ->first();

        if (!$document) {
            $document = Document::create($data_document);
        } else {
            $document->fill($data_document);
            $document->save();
        }

        return $document;
    }

    /**
     * @param $contact_id
     * @return mixed
     */
    private function getContactWarehouse($contact_id)
    {
        return ContactWarehouse::where('contact_id', $contact_id)
            ->orderBy('code', 'ASC')
            ->withTrashed()
            ->first();
    }

    /**
     * @param $document
     */
    public function createDocumentInformation($document): void
    {
        $document_information = DocumentInformation::where('document_id', $document->id)->first();

        if (!$document_information) {
            DocumentInformation::create([
                'document_id'              => $document->id,
                'document_previous_system' => true
            ]);
        } else {
            $document_information->document_previous_system = true;
            $document_information->save();
        }
    }

}
