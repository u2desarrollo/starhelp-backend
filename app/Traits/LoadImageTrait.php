<?php

namespace App\Traits;
use Illuminate\Support\Str;
/**
 *
 */
trait LoadImageTrait
{
    public function saveImage($image, $folder)
    {

        if ($image == null || $image == '') return '';

        $exploded = explode(',', $image);
        $decoded = base64_decode($exploded[1]);

        $extension = $this->getExtension($exploded[0]);

        if ($extension == '') {
            return $extension;
        }

        $fileName = Str::random(50) . '.' . $extension;

        $ruta = storage_path() . '/app/public/' . $folder . '/' . $fileName;

        file_put_contents($ruta, $decoded);

        return $folder . '/' . $fileName;
    }

    private function getExtension($file)
    {

        $extension = '';

        $options = [
            'msword' => 'doc',
            'pdf' => 'pdf',
            '.document' => 'docx',
            'ms-excel' => 'xls',
            '.sheet' => 'xlsx',
            'plain' => 'txt',
            'png' => 'png',
            'jpeg' => 'jpg',
            'jpg' => 'jpg',
            'gif' => 'gif'
        ];

        foreach ($options as $key => $value) {
            if (str_contains($file, $key)) {
                $extension = $value;
                break;
            }
        }

        return $extension;

    }





}

