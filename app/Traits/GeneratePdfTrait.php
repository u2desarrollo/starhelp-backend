<?php

namespace App\Traits;

use App\Entities\SquareCashRegisterPdf;
use App\Repositories\DocumentRepository;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\CashReceiptsPdf;
use App\Entities\Contact;
use App\Entities\Pdf;
use App\Entities\PdfColrecambios;
use App\Entities\PdfOyt;
use App\Entities\PickinPdf;
use App\Entities\AccountingDocumentPDF;
use App\Entities\despatchPdf;
use App\Entities\PdfDocumentsBalance;
use App\Entities\PdfDocumentsBalanceCol;
use App\Entities\PdfNegativeFile;
use App\Entities\PdfNegativeFileCol;
use App\Entities\TransferPdf;
use App\Entities\Subscriber;
use App\Mail\SendMailCashReceipt;
use App\Repositories\DocumentProductRepository;
use App\Repositories\InventoryRepository;
use App\Services\DocumentsService;
use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

use stdClass;

/**
 *
 */
trait GeneratePdfTrait
{

	/**
	 * pdf traslado
	 * @author santiago Torres
	 */
	public function generatePdftransfer($transfer, $view = false)
	{
		// dd($transfer->toArray());
		$pdf = new TransferPdf('p', 'mm', 'letter');

		$pdf->PDFDocument = $transfer;

		$pdf->SetTopMargin(37);
		$pdf->AddPage();
		$pdf->SetFont('Arial', 'B', 6);

		$pdf->SetAutoPageBreak(true, 35);


		$pdf->setY(45);
		$pdf->setX(43);
		$pdf->Cell(30, 4, utf8_decode('Origen'), 1, 0, 'C', false);
		$pdf->setX(135);
		$pdf->Cell(30, 4, utf8_decode('Destino'), 1, 0, 'C', false);
		$pdf->ln(4);

		$pdf->SetFont('Arial', '', 6);
		$pdf->setX(43);
		$pdf->Cell(30, 4, utf8_decode('Sede: ' . $transfer->document->branchoffice->name), 0, 0, 'L', false);
		$pdf->setX(135);
		$pdf->Cell(30, 4, utf8_decode('Sede: ' . $transfer->branchoffice->name), 0, 0, 'L', false);
		$pdf->ln(4);
		$pdf->setX(43);
		$pdf->Cell(30, 4, utf8_decode('Bodega: ' . $transfer->document->branchofficeWarehouse->branchoffice->name .' - '. $transfer->document->branchofficeWarehouse->warehouse_description), 0, 0, 'L', false);
		$pdf->setX(135);
		$pdf->Cell(30, 4, utf8_decode('Bodega: ' . $transfer->branchofficeWarehouse->branchoffice->name .' - '.$transfer->branchofficeWarehouse->warehouse_description), 0, 0, 'L', false);


        $di = $transfer->document->documentsInformation != null ? $transfer->document->documentsInformation->toArray() : null;
		$tercero = null;
		if (isset($di["additional_contact"])) {
			$tercero= is_null($di) ? null : $di["additional_contact"]["name"]. ' ' . $di["additional_contact"]["surname"];
		}

		if (!is_null($tercero)) {
			$pdf->ln(8);
			$pdf->setX(43);
			$pdf->Cell(30, 4, utf8_decode('Tercero: ' . $tercero), 0, 0, 'L', false);
			if (isset($di["additional_contact_warehouse"])) {
				if ($di['additional_contact_warehouse'] != null) {
					$pdf->ln(4);
					$pdf->setX(43);
					$pdf->Cell(30, 4, utf8_decode('Direccion: ' . $di['additional_contact_warehouse']["address"]), 0, 0, 'L', false);
					$pdf->ln(4);
					$pdf->setX(43);
					$pdf->Cell(30, 4, utf8_decode('Telefono: ' .  $di['additional_contact_warehouse']["telephone"]), 0, 0, 'L', false);
				}
			}
		}

		$pdf->SetTextColor(255, 255, 255);

		$pdf->setY(76);
		$pdf->Image(storage_path() . '/app/public/gradient_bar.png', $pdf->getX(), $pdf->getY(), 189, 8);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(63, 8, utf8_decode('Código'), 1, 0, 'L', false);
		$pdf->Cell(63, 8, utf8_decode('Descripción'), 1, 0, 'L', false);
		$pdf->Cell(63, 8, utf8_decode('Cantidad'), 1, 0, 'L', false);
		$pdf->ln(8);
		$pdf->SetFont('Arial', '', 6);

		$pdf->SetWidths([63, 63, 63]);
		$pdf->SetAligns(['L', 'L', 'L']);
		$pdf->SetTextColor(0, 0, 0);

		$total_value = 0;
		foreach ($transfer->document->documentsProducts as $key => $product) {
			$total_value += $product->inventory_cost_value;
			$pdf->Row([
				utf8_decode($product->product->code),
				utf8_decode($product->product->description),
				utf8_decode($product->quantity),
			]);
		}
		// dd($transfer->weight);
		$pdf->ln(8);
		$pdf->Cell(20, 4, utf8_decode('Peso: ' . $transfer->weight), 1, 0, 'L', false);

		// if ($transfer->total_value) {
		// 	// $transfer->total_value = number_format($transfer->total_value, 0, '', '.');
		// 	$pdf->Cell(30, 4, utf8_decode('Costo Total: ' . $transfer->total_value), 1, 0, 'L', false);
		// } else {
		// $total_value = number_format($total_value, 0, '', '.');
		$pdf->Cell(30, 4, utf8_decode('Costo Total: ' . $total_value), 1, 0, 'L', false);
		// }

		$pdf->ln(8);

		$pdf->SetFillColor(2, 5, 56, 1);
		$pdf->SetTextColor(255, 255, 255);
		$pdf->SetX(10);
		$pdf->Cell(0, 4, utf8_decode('Observacion'), 0, 0, 'L', true);
		$pdf->ln(4);

		$pdf->setX(10);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->MultiCell(0, 4, utf8_decode($transfer->observation), 'BLR', 'L');

		$pdf->SetFont('Arial', '', 9);
		if (!is_null($transfer->user)) {
			$pdf->text(10, $pdf->gety()+5, utf8_decode( 'Usuario Elaboración: '.$transfer->user->name. ' ' . $transfer->user->surname));
		}

		$fileName = 'Transfer' . $transfer->document->consecutive . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	/**
	 * Pdf pickin
	 * @author Santiago Torres
	 */
	public function generatePdfCashReceipt(array $CashR)
	{


		$pdf = new CashReceiptsPdf('p', 'mm', 'letter');

		$pdf->AddPage();
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->SetAutoPageBreak(true, 10);

		$pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13, 10, 35, 20);
		$pdf->Cell(60);


		$documentReposiyory = new  DocumentRepository(new Container);
		$fuente = 0;
		$iva = 0;
		$ica = 0;
		$cashReceipt = $documentReposiyory->getInvoiceReceipt($CashR);




		$pdf->SetTextColor(54, 60, 132);
		$pdf->SetFontSize(20);

		$pref = explode(' ', $cashReceipt['id']);
		$pdf->Write(15, utf8_decode('Recibo de Caja ' . $pref[0]));
		$pdf->ln();
		$pdf->SetXY(85, 28);
		$pdf->SetFontSize(12);
		$pdf->Write(-5, utf8_decode('(Duplicado Electrónico)'));
		$pdf->ln(4);

		$pdf->SetDrawColor(54, 60, 132);
		$pdf->SetLineWidth(0.5);
		$pdf->RoundedRect(20, 45, 160, 15, 3.5);
		$pdf->SetXY(30, 50);
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Write(0, utf8_decode('Cliente:    ' . $cashReceipt['cliente']));
		$pdf->SetXY(120, 50);
		$pdf->Write(0, utf8_decode('Ident:  ' . $cashReceipt['iden']));
		$pdf->SetXY(30, 55);
		$pdf->Write(0, utf8_decode('Asesor:    ' . $cashReceipt['vendedor']));
		$pdf->SetXY(120, 55);
		$pdf->Write(0, utf8_decode('Fecha:  ' . $cashReceipt['fecha']));
		// $pdf->SetXY(30, 55);


		//      *********     facturas    *******
		$pdf->SetXY(85, 75);
		$pdf->SetTextColor(54, 60, 132);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Write(10, utf8_decode('Detalle Facturas'));


		$pdf->SetDrawColor(54, 60, 132);
		$pdf->RoundedRect(20, 85, 160, 8, 2.5);
		$pdf->SetXY(30, 89);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Write(0, utf8_decode('Factura  '));
		$pdf->SetXY(60, 89);
		$pdf->Write(0, utf8_decode('Fecha-Fac  '));
		$pdf->SetXY(90, 89);
		$pdf->Write(0, utf8_decode('Dcto PP  '));
		$pdf->SetXY(125, 89);
		$pdf->Write(0, utf8_decode('Saldo  '));
		$pdf->SetXY(155, 89);
		$pdf->Write(0, utf8_decode('Pago  '));

		$pdf->ln();


		$pdf->SetXY(20, 96);
		foreach ($cashReceipt['facturas'] as $f) {
			$fuente += $f->rete_fte;
			$iva += $f->rete_iva;
			$ica += $f->rete_ica;
			$pdf->SetX(20);
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(32, 4, $f->numeroFactura, 0, '', 'C');
			$pdf->Cell(32, 4,  $f->fecha, 0, '', 'C');
			$pdf->Cell(32, 4, number_format($f->dctpp), 0, '', 'C');
			$pdf->Cell(32, 4, '$ ' . number_format($f->saldo), 0, '', 'C');
			$pdf->Cell(32, 4, '$ ' . number_format($f->valorConsignar), 0, '', 'C');
			$pdf->Ln();
		}


		//      *********     fin    *******


		// 			  //      *********     FORMAS DE PAGO    *******
		$pdf->SetXY(85, $pdf->GetY() + 20);
		$pdf->SetTextColor(54, 60, 132);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Write(10, utf8_decode('Formas de Pago'));


		$pdf->SetDrawColor(54, 60, 132);
		$pdf->RoundedRect(20, $pdf->GetY() + 10, 160, 8, 2.5);
		$pdf->SetXY(30, $pdf->GetY() + 15);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Write(0, utf8_decode('Forma de pago  '));
		$pdf->SetXY(75, $pdf->GetY());
		$pdf->Write(0, utf8_decode('Valor  '));
		$pdf->SetXY(110, $pdf->GetY());
		$pdf->Write(0, utf8_decode('Entidad  '));
		$pdf->SetXY(150, $pdf->GetY());
		$pdf->Write(0, utf8_decode('Num-ref  '));

		$pdf->ln();
		$pdf->SetXY(20, $pdf->getY() + 5);
		foreach ($cashReceipt['formasPago'] as $key => $value) {
			$pdf->SetX(20);
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(40, 4, $value->tipo, 0, '', 'C');
			$pdf->Cell(40, 4, '$ ' . number_format($value->valor), 0, '', 'C');
			$pdf->Cell(40, 4, $value->entidad, 0, '', 'C');
			$pdf->Cell(40, 4,  $value->numRef, 0, '', 'C');
			$pdf->Ln();
		}


		// // 	   //      *********     fin    *******


		//      *********     RETENCIONES    *******
		if ($fuente > 0 || $iva > 0 || $ica > 0) {

			$pdf->SetXY(80, $pdf->GetY() + 20);
			$pdf->SetTextColor(54, 60, 132);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Write(10, utf8_decode('Retenciones Aplicadas'));


			$pdf->SetDrawColor(54, 60, 132);
			$pdf->RoundedRect(20, $pdf->GetY() + 10, 160, 8, 2.5);
			$pdf->SetXY(40, $pdf->GetY() + 15);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->Write(0, utf8_decode('Ret-Fte  '));
			$pdf->SetXY(93, $pdf->GetY());
			$pdf->Write(0, utf8_decode('Ret-Iva  '));
			$pdf->SetXY(145, $pdf->GetY());
			$pdf->Write(0, utf8_decode('Ret-Ica  '));
			$pdf->Ln();
			$pdf->SetXY(20, $pdf->getY() + 5);
			$pdf->SetFont('Arial', '', 9);




			$pdf->setX(20);
			$pdf->Cell(53, 4, '$' . number_format($fuente), 0, '', 'C');
			$pdf->Cell(53, 4, '$ ' . number_format($iva), 0, '', 'C');
			$pdf->Cell(53, 4, '$ ' . number_format($ica), 0, '', 'C');
			// 	$pdf->Ln();
		}


		// //  //      *********     fin    *******



		//  ******     PARTE FINAL DOCUMENTP       ***********

		$pdf->SetXY(80, $pdf->GetY() + 20);
		$pdf->SetTextColor(54, 60, 132);
		$pdf->SetFont('Arial', 'B', 16);
		$pdf->Write(10, utf8_decode('Estimado Cliente:'));
		$pdf->ln();
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetTextColor(0, 0, 0);
		$html = 'Su pago lo podra ver reflejado en el portal de clientes <a href="http://www.fpdf.org">www.colsaisa.com</a> en las proximas 48 horas.' . "\r\n" .
			'Recuerde que los descuentos pronto pago solo son aplicados si usted se encuentra al dia en cartera.';
		$pdf->WriteHTML(utf8_encode($html));
		$pdf->ln(2);
		$pdf->SetXY(60, $pdf->GetY() + 5);
		$pdf->SetTextColor(54, 60, 132);
		$pdf->SetFont('Arial', 'B', 16);
		$pdf->Write(10, utf8_decode('Gracias por preferir a COLSAISA'));







		$fileName = 'Rec-Caja ' . $pref[0] . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');

		$to = [];

		if (filter_var($cashReceipt['vendedorEmail'], FILTER_VALIDATE_EMAIL)) {
			$to[] = $cashReceipt['vendedorEmail'];
		}
		// correo para copia del recibo de caja
		$cashReceipt['emailCashReceiptsCopy'] = explode(',', $cashReceipt['emailCashReceiptsCopy']);
		foreach ($cashReceipt['emailCashReceiptsCopy'] as $key => $cashReceiptEmail) {
			if (filter_var($cashReceiptEmail, FILTER_VALIDATE_EMAIL)) {
				$to[] = $cashReceiptEmail;
			}
		}


		//  if (filter_var($cashReceipt['ClienteEmail'], FILTER_VALIDATE_EMAIL)) {
		//  	$to[] = $cashReceipt['ClienteEmail'];

		//  }
		//if (config('app.send_emails')) {
		if (sizeof($to) > 0) {
			$email = new SendMailCashReceipt($cashReceipt, $fileName);

			//  if(config('app.environment_test')){
			//  	Mail::to(['andresurdaneta@u2.com.co'])->send($email);
			//  }else{
			//  	Mail::to($to)->send($email);
			//  }

			Mail::to($to)->send($email);
		}
		//}

		return $fileName;
	}

	/**
	 * pdf facturas despachos
	 * @author Santiago Torres
	 */
	public function generatePdfdespatch($pdf, $despatchs)
	{
		$pdf = new despatchPdf('p', 'mm', 'letter');
		$pdf->SetMargins(10, 10, 1);
		$pdf->AliasNbPages();


		foreach (array_keys($despatchs) as $key => $descatchZone) {
			$total_facturas = 0;
			$total_peso = 0;

			$pdf->AddPage();
			$pdf->SetFont('Arial', '', 6);

			$pdf->SetAutoPageBreak(true, 10);

			//header
			$pdf->SetFont('Arial', 'B', 6);
			$pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13, $pdf->getY(), 25, 15);
			$pdf->Image(storage_path() . '/app/public/logo.png', 165,  $pdf->getY(), 40, 10);
			//body

			$pdf->SetFont('Arial', 'B', 6);
			$pdf->ln(1);
			$pdf->setX(60);
			$pdf->SetFillColor(235, 235, 235);

			$pdf->Cell(22, 4, utf8_decode('Fecha'), 1, 0, 'L', true);

			$pdf->Cell(30, 4, utf8_decode('Transportador'), 1, 0, 'L', true);

			$pdf->Cell(30, 4, utf8_decode('Placas'), 1, 0, 'L', true);

			$pdf->Cell(23, 4, utf8_decode('Peso Total'), 1, 0, 'L', true);

			$getyForPeso = $pdf->getY();
			$pdf->ln(4);
			$pdf->setX(60);

			$pdf->Cell(22, 4, utf8_decode($despatchs[$descatchZone][0]["date"]), 1, 0, 'L', false);

			$pdf->Cell(30, 4, utf8_decode($despatchs[$descatchZone][0]["protractor"]), 1, 0, 'L', false);

			$pdf->Cell(30, 4, utf8_decode(''), 1, 0, 'L', false);


			foreach ($despatchs[$descatchZone] as $key => $despatch) {
				$total_facturas += $despatch['tot_brut'];
				$total_peso += floatval($despatch['weight']);


				$pdf->SetDash();

				//header
				// $pdf->SetFont('Arial', 'B', 6);
				// $pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13, $pdf->getY(), 25, 15);
				// $pdf->Image(storage_path() . '/app/public/logo.png', 165,  $pdf->getY(), 40, 10);
				//body

				// $pdf->SetFont('Arial', 'B', 6);
				// $pdf->ln(1);
				// $pdf->setX(45);
				// $pdf->SetFillColor(235, 235, 235);

				// $pdf->Cell(22, 4, utf8_decode('fECHA'), 1, 0, 'L', true);

				// $pdf->Cell(15, 4, utf8_decode('Zona'), 1, 0, 'L', true);

				// $pdf->Cell(30, 4, utf8_decode('Transportador'), 1, 0, 'L', true);

				// $pdf->Cell(30, 4, utf8_decode('Placas'), 1, 0, 'L', true);

				// $pdf->Cell(15, 4, utf8_decode('Peso Total'), 1, 0, 'L', true);

				// $pdf->ln(4);
				// $pdf->setX(45);


				$pdf->ln(3);
				$pdf->setX(10);

				$pdf->ln(8);
				$pdf->Cell(15, 4, utf8_decode('Factura'), 1, 0, 'L', true);
				$pdf->Cell(45, 4, utf8_decode('Cliente'), 1, 0, 'L', true);
				$pdf->Cell(86.6, 4, utf8_decode('Dirección / Ciudad'), 1, 0, 'L', true);
				$pdf->Cell(15, 4, utf8_decode('Teléfono'), 1, 0, 'L', true);
				$pdf->Cell(23, 4, utf8_decode('Contacto'), 1, 0, 'L', true);
				$pdf->Cell(11, 4, utf8_decode('Tot Prods'), 1, 0, 'L', true);
				$pdf->ln(4);
				$pdf->Cell(15, 4, utf8_decode($despatch["invoice"]), 1, 0, 'L', false);

				$client = substr($despatch["client"], 0, 32) . '...';
				$pdf->Cell(45, 4, utf8_decode($client), 1, 0, 'L', false);

				$pdf->Cell(86.6, 4, utf8_decode(str_replace('  ', '', $despatch["Addres_client"])), 1, 0, 'L', false);
				$pdf->Cell(15, 4, utf8_decode($despatch["telephone"]), 1, 0, 'L', false);

				$despatchContact = substr($despatch["contact"], 0, 14) . '...';
				$pdf->Cell(23, 4, utf8_decode($despatchContact), 1, 0, 'L', false);
				$pdf->Cell(11, 4, utf8_decode($despatch["tot_prod"]), 1, 0, 'L', false);


				$pdf->ln(6);
				$pdf->Cell(10, 4, utf8_decode('Zona'), 1, 0, 'L', true);
				$pdf->Cell(15, 4, utf8_decode($despatch["zone"]), 1, 0, 'L', false);

				$pdf->Cell(23, 4, utf8_decode('Valor total Mercancía'), 1, 0, 'L', true);
				$pdf->Cell(77, 4, '$ ' . number_format($despatch['tot_brut'], 0, '', '.'), 1, 0, 'L', false);

				$pdf->Cell(10, 4, utf8_decode('Peso'), 1, 0, 'L', true);
				$pdf->Cell(15, 4, utf8_decode($despatch["weight"]), 1, 0, 'L', false);

				$pdf->Cell(10, 4, utf8_decode('# Guía'), 1, 0, 'L', true);
				// $pdf->Cell(127, 4, '$ ' . number_format($despatch['tot_brut'], 0, '', '.'), 1, 0, 'L', false);
				$pdf->Cell(36, 4, utf8_decode(str_replace('  ', '', $despatch["guide"])), 1, 0, 'L', false);


				$pdf->ln(6);
				$pdf->Cell(23, 4, utf8_decode('Obs Despacho'), 1, 0, 'L', true);
				$pdf->Cell(127, 4, utf8_decode($despatch["obs_Despatach"]), 1, 0, 'L', false);
				$pdf->Cell(10, 4, utf8_decode('firma:'), 1, 0, 'L', true);
				$pdf->Cell(35, 4, utf8_decode(''), 'B', 0, 'L', false);


				// $pdf->ln(8);
				// $pdf->Cell(20, 4, utf8_decode('Código'), 1, 0, 'L', true);
				// $pdf->Cell(90, 4, utf8_decode('Descripción'), 1, 0, 'L', true);
				// $pdf->Cell(36.8, 4, utf8_decode('Línea'), 1, 0, 'L', true);
				// $pdf->Cell(36.8, 4, utf8_decode('Lote'), 1, 0, 'L', true);
				// $pdf->Cell(12, 4, utf8_decode('Cantidad'), 1, 0, 'L', true);

				// $pdf->ln(4);

				// $pdf->SetWidths([20, 90, 36.8, 36.8, 12]);
				// $pdf->SetAligns(['L', 'L', 'L', 'L', 'L']);

				// foreach ($despatch['products'] as $product) {
				// 	$pdf->Row([
				// 		utf8_decode($product['code']),
				// 		utf8_decode($product['description']),
				// 		utf8_decode($product['line']),
				// 		utf8_decode($product['lot']),
				// 		utf8_decode($product['quantity']),
				// 	]);
				// }

				//$pdf->ln(21);
				//footer
				$pdf->SetTextColor(0, 0, 0);
				/* $pdf->setX(127);
				$pdf->Cell(5, 8, '', 0, 0, 'C');
				$pdf->Cell(50, 8, utf8_decode('Firma Cliente'), 'T', 0, 'C'); */
				$pdf->ln(10);

				$pdf->SetDash(4, 1.5);
				$pdf->Line(0, $pdf->getY(), 216, $pdf->getY());
				$pdf->setY($pdf->getY() + 5);
			}
			$pdf->SetDash();

			$pdf->ln(6);
			$pdf->Cell(23, 4, utf8_decode('Valor total Mercancía: '), 1, 0, 'L', true);
			$pdf->Cell(127, 4, '$ ' . number_format($total_facturas, 0, '', '.'), 1, 0, 'L', false);

			$beforPeso = $pdf->getY();
			$pdf->setY($getyForPeso);
			$pdf->ln(4);
			$pdf->setX(142);
			// $pdf->Cell(23, 4, utf8_decode('Peso Total: '), 1, 0, 'L', true);
			$pdf->Cell(23, 4, $total_peso, 1, 0, 'L', false);
			$pdf->setY($beforPeso);
			$pdf->ln(8);
			$pdf->Cell(20, 4,    utf8_decode(''), 1, 0, 'L', true);
			$pdf->Cell(80, 4,    utf8_decode(''), 1, 0, 'L', true);
			$pdf->Cell(30, 4,  utf8_decode(''), 1, 0, 'L', true);
			$pdf->Cell(41.6, 4,  utf8_decode(''), 1, 0, 'L', true);
			$pdf->Cell(24, 4,    utf8_decode(''), 1, 0, 'L', true);

			for ($i = 0; $i <= 5; $i++) {
				$pdf->ln(4);
				$pdf->Cell(20, 4,    utf8_decode(''), 1, 0, 'L', false);
				$pdf->Cell(80, 4,    utf8_decode(''), 1, 0, 'L', false);
				$pdf->Cell(30, 4,  utf8_decode(''), 1, 0, 'L', false);
				$pdf->Cell(41.6, 4,  utf8_decode(''), 1, 0, 'L', false);
				$pdf->Cell(24, 4,    utf8_decode(''), 1, 0, 'L', false);
			}
		}

		//$fileName = $document->vouchertype->name_voucher_type . ' No. ' .$impresion['numero'].'.pdf';
		$fileName = 'bill.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	public function generatePdfOrder($pdf, $documentRepository, $document, $show_Values = true)
	{
		$user = auth()->user();

		switch ($user->subscriber->identification) {
			case '900338016':
				return $this->generatePdfOrderColsaisa($pdf, $documentRepository, $document, $show_Values = true);
			break;

			case '1654654':
				return $this->generatePdfOrderOYT($pdf, $documentRepository, $document, $show_Values = true);
			break;

			case '830048991':
				return $this->generatePdfColrecambios($pdf, $documentRepository, $document, $show_Values = true);
			break;

			default:
				# code...
				break;
		}


	}

	public function generatePdfOrderColsaisa($pdf, $documentRepository, $document, $show_Values = true)
	{
		$impresion = $documentRepository->printDocument($document);
		$pdf = new Pdf('p', 'mm', 'letter');
		$pdf->AliasNbPages();
		$pdf->PDFDocument = $impresion;
		if ($impresion["codigo_modelo"] == 267) {
			$pdf->showValues = false;
			$pdf->AddPage();
			$pdf->SetFont('Arial', '', 6);

			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);
			foreach ($impresion['products'] as  $key => $product) {
				$pdf->Row([
					utf8_decode($key),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),
				]);
			}
		}
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);

		$totalItems = 0;
		$totalQuantity = 0;
		$total_dscto = 0;
		$total_bruto = 0;
		$total_valor_iva = 0;
		$total = 0;
		// dd($impresion['products']);
		if ($show_Values) {
			$pdf->SetWidths([7,20, 58, 9, 21, 20, 21, 22, 18]);
			$pdf->SetAligns(['C', 'L', 'L', 'C', 'C', 'C', 'C', 'C', 'C']);

			foreach ($impresion['products'] as  $key => $product) {
				$totalItems 		+= 		1;
				$totalQuantity 		+= 		$product['quantity'];
				$total_dscto 		+=  	$product['discount_value'];
				$total_bruto 		+=  	$product['total_value_brut'];
				$total_valor_iva	+= 		$product['iva_p'];
				$total				+=		$product['total_value'];
				$line_break 		= strlen($product['description']) >= 55 ? "
				" : '';

				if ($impresion['trm']) {
					$pdf->Row([
						utf8_decode($totalItems),
						utf8_decode($product['code']),
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.'),

						number_format($product['unit_value_before_taxes'], 2, ',', '.') . ' USD',
						number_format($product['discount_value'], 2, ',', '.') . ' USD',
						number_format($product['total_value_brut'], 2, ',', '.') . ' USD',
						number_format($product['iva_p'], 2, ',', '.') . ' USD',
						number_format($product['total_value'], 2, ',', '.') . ' USD',
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				} else {
					$pdf->Row([
						utf8_decode($totalItems),
						utf8_decode($product['code']),
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.'),

						'$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.'),
						'$ ' . number_format($product['discount_value'], 0, '', '.'),
						'$ ' . number_format($product['total_value_brut'], 2, ',', '.'),
						'$ ' . number_format($product['iva_p'], 2, ',', '.'),
						'$ ' . number_format($product['total_value'], 0, '', '.'),
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				}
			}
		} else {
			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);

			foreach ($impresion['products'] as $product) {
				$totalItems += 1;
				$totalQuantity += $product['quantity'];
				$pdf->Row([
					utf8_decode($totalItems),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),

					// '$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.'),
					// '$ ' . number_format($product['total_value_brut'], 0, '', '.'),
					// '$ ' . number_format($product['iva_p'], 0, '', '.'),
					// '$ ' . number_format($product['total_value'], 0, '', '.'),
				]);
			}
		}

		if (!is_null($impresion["branchoffice_change"])) {
			$pdf->setXY(10, $pdf->getY()-3);
		}
		$pdf->SetFont('Arial', '', 5.5);
		$pdf->Cell(27, 4, utf8_decode('Total Items: '.$totalItems), 1, 0, 'C', false);
		$pdf->SetFont('Arial', '', 6);

		$pdf->setXY(10, $pdf->getY()+3);

		if ($pdf->getY() >= 172) {
			$pdf->AddPage();
		}
		$pdf->ln(4);
		if ($impresion['trm']) {
			$pdf->Cell(27, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(58, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(20, 4, utf8_decode('$ ' . number_format($total_dscto) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode('$ ' . number_format($total_bruto) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode('$ ' . number_format($total_valor_iva) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(19, 4, utf8_decode('$ ' . number_format($total) . ' USD'), 1, 0, 'C', false);
		} else {
			$pdf->Cell(27, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(58, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(20, 4, utf8_decode('$ ' . number_format($total_dscto)), 1, 0, 'C', false);
			$pdf->Cell(21, 4, '$ ' . number_format($total_bruto, 2, ',', '.'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, '$ ' . number_format($total_valor_iva, 2, ',', '. '), 1, 0, 'C', false);
			$pdf->Cell(19, 4, utf8_decode('$ ' . number_format($total)), 1, 0, 'C', false);
		}

		// $pdf->Text(10, $pdf->GetY() + 8, 'Total Cantidades: ' . $totalQuantity);
		// $pdf->Text(10, $pdf->GetY() + 8, 'Total Items: ' . $totalItems);
		$pdf->Text(10, $pdf->GetY() + 11, 'Total Peso: ' . number_format($impresion['weight'], 0, '', '.'));

		if ($impresion["user_id_solicit_authorization"] != null) {
			$pdf->Text(35, $pdf->GetY() + 5, utf8_decode('Autorización Elaboración: ' . $impresion["user_id_solicit_authorization"]->name . ' ' . $impresion["user_id_solicit_authorization"]->surname . ' ' . $impresion["date_solicit_authorization"]));
		}

		if ($impresion["status_id_authorized"]) {
			$pdf->Text(35, $pdf->GetY() + 8, utf8_decode('Autorización Cartera: ' . $impresion["PortfolioAuthorization"] . ' ' . $impresion["date_authorized"]));
		}


		if ($show_Values) {
			$pdf->ln(10);

			//   totales
			$pdf->SetFillColor(2, 5, 56, 1);
			$pdf->SetTextColor(255, 255, 255);
			$pdf->setX(10);
			if ($impresion['retention_management'] == '3' || $impresion['retention_management'] == '2') {
				if ($impresion["retenciones_total"]["rete_fuente"]["total"] > 0 && $impresion["retenciones_total"]["rete_ica"]["total"] > 0 && $impresion["retenciones_total"]["rete_iva"]["total"] > 0) {
					$pdf->Cell(79, 4, utf8_decode('Retenciones'), 1, 0, 'C', true);
				}
			}

			$pdf->setX(156);
			$pdf->Cell(0, 4, utf8_decode('Total Documento'), 1, 0, 'L', true);
			$pdf->ln(4);

			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->setX(10);
			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_fuente"]["total"]>0 && $impresion["retenciones_total"]["rete_ica"]["total"]>0 && $impresion["retenciones_total"]["rete_iva"]["total"]>0) {
				$pdf->Cell(19.75, 4, utf8_decode('Retención '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Base') , 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Porcentaje'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Vr. Retención') , 1, 0, 'C', true);
			}
			}*/
			$setY = $pdf->getY();
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', '', 5);
			$pdf->sety($setY);
			$pdf->Cell(26, 3, utf8_decode('Como comprador o beneficiario del servicio, acepto '), 0, 0, 'L', false);
			$pdf->sety($setY + 3);
			$pdf->Cell(26, 3, utf8_decode('de manera expresa el contenido expreso de la siguiente'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('factura. Asi mismo declaro recibido, real y materialmente'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('a satisfaccion los bienes relacionados y/o los servicios prestados.'), 0, 0, 'L', false);

			$pdf->sety($pdf->getY() + 5);
			$pdf->SetFont('Arial', 'B', 5);
			$pdf->Cell(26, 3, utf8_decode('"SOLO SE ACEPTAN DEVOLUCIONES DENTRO DE LAS'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('SIGUIENTES 48 HORAS DE HABER RECIBIDD EL PEDIDO"'), 0, 0, 'L', false);

			$pdf->SetFont('Arial', '', 5);
			$pdf->sety($setY);
			$pdf->setX(80);
			$pdf->Cell(26, 3, utf8_decode('En constacia subcribo el dia _____ mes_____ año _______ '), 0, 0, 'L', false);


			$pdf->SetFillColor(245, 245, 255);
			$pdf->SetTextColor(0, 0, 0);


			$pdf->SetFont('Arial', 'B', 6);
			$pdf->setY($setY);
			$pdf->setX(156);
			$pdf->Cell(26, 4, utf8_decode('Total Bruto'), 1, 0, 'L', true);

			//$pdf->Cell(0, 4, '$ '.number_format($impresion['subtotal_sin_desc'], 0, '', '.') , 1, 0, 'R', true);
			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.'), 1, 0, 'R', true);
			}

			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_fuente"]["total"]>0) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('ReteFuente '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_fuente"]["base_value"], 0, '', '.') , 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_fuente"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_fuente"]["total"], 0, '', '.') , 1, 0, 'C', true);
			}
			}*/


			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);
			$pdf->Cell(26, 4, utf8_decode('Total Iva'), 1, 0, 'L', true);
			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			if ($impresion['iva_obsq'] > 0) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('IVA Obseq.'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 0, '', '.'), 1, 0, 'R', true);
				}


				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_ica"]["total"]>0) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('Reteica '), 1, 0, 'C', true);

				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_ica"]["base_value"], 0, '', '.'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_ica"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_ica"]["total"], 0, '', '.'), 1, 0, 'C', true);
			}
			}*/

			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('Total General'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_iva"]["total"]) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('Reteiva '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_iva"]["base_value"], 0, '', '.'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_iva"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_iva"]["total"], 0, '', '.'), 1, 0, 'C', true);
			}
			}*/


			//$pdf->Cell(26, 4, utf8_decode('Total Retenciones'), 1, 0, 'L', true);

			//$pdf->Cell(0, 4, '$ '.number_format($impresion['total_retention'], 0, '', '.') , 1, 0, 'R', true);
			//$pdf->SetFont('Arial','', 6);
			//$pdf->ln(4);

			if ($impresion['ret_fue_total'] > 0 && $impresion['ret_fue_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);

				$pdf->Cell(26, 4, utf8_decode('Rete FTE'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_iva_total'] > 0 && $impresion['ret_iva_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete IVA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_ica_total'] > 0 && $impresion['ret_ica_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete ICA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 2, '', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}


			// $pdf->setX(156);
			// $pdf->SetFont('Arial', 'B', 6);

			// $pdf->Cell(26, 4, utf8_decode('Flete'), 1, 0, 'L', true);

			// $pdf->Cell(0, 4, ''/*.number_format('', 0, '', '.')  */, 1, 0, 'R', true);
			// $pdf->SetFont('Arial', '', 6);
			// $pdf->ln(4);


			$pdf->setX(80);


			$pdf->Cell(40, 4, utf8_decode('Firma y Sello Identificacion'), 'T', 0, 'C', false);
			if ($impresion["codigo_modelo"] == 267) {
				$pdf->setX(110);
				$pdf->Cell(40, 4, utf8_decode('Firma recibe'), 'T', 0, 'C', false);
			}

			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('TOTAL A PAGAR'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(6);


			$pdf->setFont('Arial', 'B', 6);
			$pdf->setX(80);
			if ($impresion['total_letters']) {
				$pdf->Cell(126, 4, utf8_decode('Monto en letras: ' . $this->valorEnLetras($impresion['total_to_pay'], $impresion["trm"])), 1, 0, 'R', true);
			}
			if ($impresion['operationType_id'] == 5) {
				$pdf->ln(6);
				$pdf->setX(166);
				$pdf->Cell(40, 4, utf8_decode('Medio De Pago : '.$impresion['medio_pago'] ), 1, 0, 'R', true);
				$pdf->ln(4);
				$pdf->setX(166);
				$pdf->Cell(40, 4, utf8_decode('Forma De Pago : '.$impresion['cupo_credito'] ), 1, 0, 'R', true);

			}
			// if ($impresion['operationType_id'] == 5 && $impresion['handle_payment_methods'] != 2 && !is_null($impresion['handle_payment_methods'])) {
			// 	$pdf->ln(6);

			// 	$pdf->SetFillColor(245, 245, 255);
			// 	$pdf->setX(136);
			// 	$pdf->Cell(70, 4, utf8_decode('Formas De Pago '), 1, 0, 'C', true);
			// 	$pdf->ln(4);
			// 	$pdf->SetWidths([50,20]);
			// 	$pdf->SetAligns(['L', 'C']);
			// 	foreach ($impresion['metodos_pago'] as  $key => $medios_pago) {
			// 		$pdf->setX(136);
			// 		$pdf->Row([
			// 			utf8_decode($medios_pago['metodo_pago']),
			// 			'$ ' . number_format($medios_pago['valor'], 0, '', '.')
			// 		]);
			// 	}

			// 	$pdf->ln(4);
			// 	$pdf->setX(136);
			// 	$pdf->Cell(50, 4, utf8_decode('Cambio: '), 1, 0, 'L', false);
			// 	$pdf->Cell(20, 4, '$ ' . number_format($impresion['cambio'], 0, '', '.'), 1, 0, 'C', false);

			// }

			$pdf->ln(8);
			if ($impresion['handle_payment_methods'] == 1) {

				$pdf->SetFillColor(245, 245, 255);
				$pdf->SetTextColor(255, 255, 255);
				$pdf->SetX(10);
				$pdf->Cell(0, 4, utf8_decode('Medios de pago'), 0, 0, 'C', true);
				$pdf->ln(4);

				$pdf->setX(10);
				$pdf->Cell(64, 4, utf8_decode('Forma d4e Pago '), 1, 0, 'C', true);
				$pdf->Cell(63, 4, utf8_decode('Valor'), 1, 0, 'C', true);
				$pdf->Cell(63, 4, utf8_decode('Fecha Vencimiento'), 1, 0, 'C', true);
				$pdf->ln(4);


				$pdf->SetTextColor(0, 0, 0);
				$pdf->SetFont('Arial', '', 6);
				$pdf->SetWidths([64, 63, 63]);
				$pdf->SetAligns(['L', 'L', 'L']);
				foreach ($impresion['medios_pagos'] as $medios_pagos) {
					$pdf->Row([
						utf8_decode($medios_pagos["name_parameter"]),
						utf8_decode($medios_pagos["value"]),
						utf8_decode(''),
					]);
				}
			}
			$pdf->ln(4);
		}

		$fileName = str_replace("/", "-", $document->vouchertype->name_voucher_type . ' No.' . $impresion['numero'] . '.pdf');
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');

		return $fileName;
	}

	public function generatePdfOrderOYT($pdf, $documentRepository, $document, $show_Values = true)
	{
		$impresion = $documentRepository->printDocument($document);
		$pdf = new PdfOyt('p', 'mm', 'letter');
		$pdf->AliasNbPages();
		$pdf->PDFDocument = $impresion;
		if ($impresion["codigo_modelo"] == 267) {
			$pdf->showValues = false;
			$pdf->AddPage();
			$pdf->SetFont('Arial', '', 6);

			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);
			foreach ($impresion['products'] as  $key => $product) {
				$pdf->Row([
					utf8_decode($key),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),
				]);
			}
		}
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);

		$totalItems = 0;
		$totalQuantity = 0;
		$total_dscto = 0;
		$total_bruto = 0;
		$total_valor_iva = 0;
		$total = 0;
		// dd($impresion['products']);
		if ($show_Values) {
			$pdf->SetWidths([7,20, 58, 9, 21, 20, 21, 22, 18]);
			$pdf->SetAligns(['C', 'L', 'L', 'C', 'C', 'C', 'C', 'C', 'C']);

			foreach ($impresion['products'] as  $key => $product) {
				$totalItems 		+= 		1;
				$totalQuantity 		+= 		$product['quantity'];
				$total_dscto 		+=  	$product['discount_value'];
				$total_bruto 		+=  	$product['total_value_brut'];
				$total_valor_iva	+= 		$product['iva_p'];
				$total				+=		$product['total_value'];

				if ($impresion['trm']) {
					$pdf->Row([
						utf8_decode($totalItems),
						utf8_decode($product['code']),
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.'),

						number_format($product['unit_value_before_taxes'], 2, ',', '.') . ' USD',
						number_format($product['discount_value'], 2, ',', '.') . ' USD',
						number_format($product['total_value_brut'], 2, ',', '.') . ' USD',
						number_format($product['iva_p'], 2, ',', '.') . ' USD',
						number_format($product['total_value'], 2, ',', '.') . ' USD',
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				} else {
					$pdf->Row([
						utf8_decode($totalItems),
						utf8_decode($product['code']),
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.'),

						'$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.'),
						'$ ' . number_format($product['discount_value'], 0, '', '.'),
						'$ ' . number_format($product['total_value_brut'], 2, ',', '.'),
						'$ ' . number_format($product['iva_p'], 2, ',', '.'),
						'$ ' . number_format($product['total_value'], 0, '', '.'),
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				}
			}
		} else {
			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);

			foreach ($impresion['products'] as $product) {
				$totalItems += 1;
				$totalQuantity += $product['quantity'];
				$pdf->Row([
					utf8_decode($totalItems),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),

					// '$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.'),
					// '$ ' . number_format($product['total_value_brut'], 0, '', '.'),
					// '$ ' . number_format($product['iva_p'], 0, '', '.'),
					// '$ ' . number_format($product['total_value'], 0, '', '.'),
				]);
			}
		}

		if (!is_null($impresion["branchoffice_change"])) {
			$pdf->setXY(10, $pdf->getY()-3);
		}
		$pdf->SetFont('Arial', '', 5.5);
		$pdf->Cell(27, 4, utf8_decode('Total Items: '.$totalItems), 1, 0, 'C', false);
		$pdf->SetFont('Arial', '', 6);

		$pdf->setXY(10, $pdf->getY()+3);

		if ($pdf->getY() >= 172) {
			$pdf->AddPage();
		}
		$pdf->ln(4);
		if ($impresion['trm']) {
			$pdf->Cell(27, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(58, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(20, 4, utf8_decode('$ ' . number_format($total_dscto) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode('$ ' . number_format($total_bruto) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode('$ ' . number_format($total_valor_iva) . ' USD'), 1, 0, 'C', false);
			$pdf->Cell(19, 4, utf8_decode('$ ' . number_format($total) . ' USD'), 1, 0, 'C', false);
		} else {
			$pdf->Cell(27, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(58, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(21, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(20, 4, utf8_decode('$ ' . number_format($total_dscto)), 1, 0, 'C', false);
			$pdf->Cell(21, 4, '$ ' . number_format($total_bruto, 2, ',', '.'), 1, 0, 'C', false);
			$pdf->Cell(21, 4, '$ ' . number_format($total_valor_iva, 2, ',', '. '), 1, 0, 'C', false);
			$pdf->Cell(19, 4, utf8_decode('$ ' . number_format($total)), 1, 0, 'C', false);
		}

		// $pdf->Text(10, $pdf->GetY() + 11, 'Total Peso: ' . number_format($impresion['weight'], 0, '', '.'));

		if ($impresion["user_id_solicit_authorization"] != null) {
			$pdf->Text(35, $pdf->GetY() + 5, utf8_decode('Autorización Elaboración: ' . $impresion["user_id_solicit_authorization"]->name . ' ' . $impresion["user_id_solicit_authorization"]->surname . ' ' . $impresion["date_solicit_authorization"]));
		}

		if ($impresion["status_id_authorized"]) {
			$pdf->Text(35, $pdf->GetY() + 8, utf8_decode('Autorización Cartera: ' . $impresion["PortfolioAuthorization"] . ' ' . $impresion["date_authorized"]));
		}


		if ($show_Values) {
			$pdf->ln(10);

			//   totales
			$pdf->SetFillColor(255, 204, 0);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->setX(10);
			if ($impresion['retention_management'] == '3' || $impresion['retention_management'] == '2') {
				if ($impresion["retenciones_total"]["rete_fuente"]["total"] > 0 && $impresion["retenciones_total"]["rete_ica"]["total"] > 0 && $impresion["retenciones_total"]["rete_iva"]["total"] > 0) {
					$pdf->Cell(79, 4, utf8_decode('Retenciones'), 1, 0, 'C', true);
				}
			}

			$pdf->setX(156);
			$pdf->Cell(0, 4, utf8_decode('Total Documento'), 1, 0, 'L', true);
			$pdf->ln(4);

			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->setX(10);

			$setY = $pdf->getY();
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', '', 5);
			$pdf->sety($setY);
			$pdf->Cell(26, 3, utf8_decode('Como comprador o beneficiario del servicio, acepto '), 0, 0, 'L', false);
			$pdf->sety($setY + 3);
			$pdf->Cell(26, 3, utf8_decode('de manera expresa el contenido expreso de la siguiente'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('factura. Asi mismo declaro recibido, real y materialmente'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('a satisfaccion los bienes relacionados y/o los servicios prestados.'), 0, 0, 'L', false);

			$pdf->sety($pdf->getY() + 5);
			$pdf->SetFont('Arial', 'B', 5);
			$pdf->Cell(26, 3, utf8_decode('"SOLO SE ACEPTAN DEVOLUCIONES DENTRO DE LAS'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('SIGUIENTES 48 HORAS DE HABER RECIBIDD EL SERVICIO"'), 0, 0, 'L', false);


			$pdf->SetFont('Arial', '', 5);
			$pdf->sety($setY);
			$pdf->setX(80);
			// $pdf->Cell(26, 3, utf8_decode('En constacia subcribo el dia _____ mes_____ año _______ '), 0, 0, 'L', false);


			$pdf->SetFillColor(255, 251, 239);
			$pdf->SetTextColor(0, 0, 0);


			$pdf->SetFont('Arial', 'B', 6);
			$pdf->setY($setY);
			$pdf->setX(156);
			$pdf->Cell(26, 4, utf8_decode('Total Bruto'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.'), 1, 0, 'R', true);
			}

			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);


			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);
			$pdf->Cell(26, 4, utf8_decode('Total Iva'), 1, 0, 'L', true);
			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			if ($impresion['iva_obsq'] > 0) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('IVA Obseq.'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 0, '', '.'), 1, 0, 'R', true);
				}


				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}
			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('Total General'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);


			if ($impresion['ret_fue_total'] > 0 && $impresion['ret_fue_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);

				$pdf->Cell(26, 4, utf8_decode('Rete FTE'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_iva_total'] > 0 && $impresion['ret_iva_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete IVA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_ica_total'] > 0 && $impresion['ret_ica_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete ICA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 2, '', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}



			$pdf->setX(80);

			$pdf->Cell(60, 4, utf8_decode('Firma y Sello Identificacion'), 'T', 0, 'C', false);

			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('TOTAL A PAGAR'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(6);


			$pdf->setFont('Arial', 'B', 6);
			$pdf->setX(80);
			if ($impresion['total_letters']) {
				$pdf->Cell(126, 4, utf8_decode('Monto en letras: ' . $this->valorEnLetras($impresion['total_to_pay'], $impresion["trm"])), 1, 0, 'R', true);
			}
			if ($impresion['operationType_id'] == 5) {
				$pdf->ln(6);

				$pdf->SetFillColor(255, 204, 0);
				$pdf->setX(136);
				$pdf->Cell(70, 4, utf8_decode('Formas De Pago '), 1, 0, 'C', true);
				$pdf->ln(4);
				$pdf->SetWidths([50,20]);
				$pdf->SetAligns(['L', 'C']);
				foreach ($impresion['metodos_pago'] as  $key => $medios_pago) {
					$pdf->setX(136);
					$pdf->Row([
						utf8_decode($medios_pago['metodo_pago']),
						'$ ' . number_format($medios_pago['valor'], 0, '', '.')
					]);
				}

				$pdf->ln(4);
				$pdf->setX(136);
				$pdf->Cell(50, 4, utf8_decode('Cambio: '), 1, 0, 'L', false);
				$pdf->Cell(20, 4, '$ ' . number_format($impresion['cambio'], 0, '', '.'), 1, 0, 'C', false);

			}

			$pdf->ln(8);
			$pdf->ln(4);
		}

		$fileName = str_replace("/", "-", $document->vouchertype->name_voucher_type . ' No.' . $impresion['numero'] . '.pdf');
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');

		return $fileName;
	}


	/*

	retorna el número en letras
	*/
	public function valorEnLetras($x, $trm = false)
	{
		if ($x < 0) {
			$signo = "menos ";
		} else {
			$signo = "";
		}
		$x = abs($x);
		$C1 = $x;

		$G6 = floor($x / (1000000));  // 7 y mas

		$E7 = floor($x / (100000));
		$G7 = $E7 - $G6 * 10;   // 6

		$E8 = floor($x / 1000);
		$G8 = $E8 - $E7 * 100;   // 5 y 4

		$E9 = floor($x / 100);
		$G9 = $E9 - $E8 * 10;  //  3

		$E10 = floor($x);
		$G10 = $E10 - $E9 * 100;  // 2 y 1


		$G11 = round(($x - $E10) * 100, 0);  // Decimales
		//////////////////////

		$H6 = $this->unidades($G6);

		if ($G7 == 1 and $G8 == 0) {
			$H7 = "Cien ";
		} else {
			$H7 = $this->decenas($G7);
		}

		$H8 = $this->unidades($G8);

		if ($G9 == 1 and $G10 == 0) {
			$H9 = "Cien ";
		} else {
			$H9 = $this->decenas($G9);
		}

		$H10 = $this->unidades($G10);

		if ($G11 < 10) {
			$H11 = "0" . $G11;
		} else {
			$H11 = $G11;
		}

		/////////////////////////////
		if ($G6 == 0) {
			$I6 = " ";
		} elseif ($G6 == 1) {
			$I6 = "Millón ";
		} else {
			$I6 = "Millones ";
		}

		if ($G8 == 0 and $G7 == 0) {
			$I8 = " ";
		} else {
			$I8 = "Mil ";
		}

		if ($trm) {
			$I10 = "Dolares";
		} else {
			$I10 = "Pesos";
		}

		$C3 = $signo . $H6 . $I6 . $H7 . $H8 . $I8 . $H9 . $H10 . $I10;

		return $C3; //Retornar el resultado

	}

	/*
	* retorna en letras el valor de la unidad $u
	*/
	public function unidades($u)
	{
		if ($u == 0) {
			$ru = " ";
		} elseif ($u == 1) {
			$ru = "Un ";
		} elseif ($u == 2) {
			$ru = "Dos ";
		} elseif ($u == 3) {
			$ru = "Tres ";
		} elseif ($u == 4) {
			$ru = "Cuatro ";
		} elseif ($u == 5) {
			$ru = "Cinco ";
		} elseif ($u == 6) {
			$ru = "Seis ";
		} elseif ($u == 7) {
			$ru = "Siete ";
		} elseif ($u == 8) {
			$ru = "Ocho ";
		} elseif ($u == 9) {
			$ru = "Nueve ";
		} elseif ($u == 10) {
			$ru = "Diez ";
		} elseif ($u == 11) {
			$ru = "Once ";
		} elseif ($u == 12) {
			$ru = "Doce ";
		} elseif ($u == 13) {
			$ru = "Trece ";
		} elseif ($u == 14) {
			$ru = "Catorce ";
		} elseif ($u == 15) {
			$ru = "Quince ";
		} elseif ($u == 16) {
			$ru = "Diez y seis ";
		} elseif ($u == 17) {
			$ru = "Diez y siete ";
		} elseif ($u == 18) {
			$ru = "Diez y ocho ";
		} elseif ($u == 19) {
			$ru = "Diez y nueve ";
		} elseif ($u == 20) {
			$ru = "Veinte ";
		} elseif ($u == 21) {
			$ru = "Veinte y un ";
		} elseif ($u == 22) {
			$ru = "Veinte y dos ";
		} elseif ($u == 23) {
			$ru = "Veinte y tres ";
		} elseif ($u == 24) {
			$ru = "Veinte y cuatro ";
		} elseif ($u == 25) {
			$ru = "Veinte y cinco ";
		} elseif ($u == 26) {
			$ru = "Veinte y seis ";
		} elseif ($u == 27) {
			$ru = "Veinte y siente ";
		} elseif ($u == 28) {
			$ru = "Veinte y ocho ";
		} elseif ($u == 29) {
			$ru = "Veinte y nueve ";
		} elseif ($u == 30) {
			$ru = "Treinta ";
		} elseif ($u == 31) {
			$ru = "Treinta y un ";
		} elseif ($u == 32) {
			$ru = "Treinta y dos ";
		} elseif ($u == 33) {
			$ru = "Treinta y tres ";
		} elseif ($u == 34) {
			$ru = "Treinta y cuatro ";
		} elseif ($u == 35) {
			$ru = "Treinta y cinco ";
		} elseif ($u == 36) {
			$ru = "Treinta y seis ";
		} elseif ($u == 37) {
			$ru = "Treinta y siete ";
		} elseif ($u == 38) {
			$ru = "Treinta y ocho ";
		} elseif ($u == 39) {
			$ru = "Treinta y nueve ";
		} elseif ($u == 40) {
			$ru = "Cuarenta ";
		} elseif ($u == 41) {
			$ru = "Cuarenta y un ";
		} elseif ($u == 42) {
			$ru = "Cuarenta ydos ";
		} elseif ($u == 43) {
			$ru = "Cuarenta ytres ";
		} elseif ($u == 44) {
			$ru = "Cuarenta ycuatro ";
		} elseif ($u == 45) {
			$ru = "Cuarenta ycinco ";
		} elseif ($u == 46) {
			$ru = "Cuarenta yseis ";
		} elseif ($u == 47) {
			$ru = "Cuarenta ysiete ";
		} elseif ($u == 48) {
			$ru = "Cuarenta yocho ";
		} elseif ($u == 49) {
			$ru = "Cuarenta ynueve ";
		} elseif ($u == 50) {
			$ru = "Cincuenta ";
		} elseif ($u == 51) {
			$ru = "Cincuenta y un ";
		} elseif ($u == 52) {
			$ru = "Cincuenta y dos ";
		} elseif ($u == 53) {
			$ru = "Cincuenta y tres ";
		} elseif ($u == 54) {
			$ru = "Cincuenta y cuatro ";
		} elseif ($u == 55) {
			$ru = "Cincuenta y cinco ";
		} elseif ($u == 56) {
			$ru = "Cincuenta y seis ";
		} elseif ($u == 57) {
			$ru = "Cincuenta y siete ";
		} elseif ($u == 58) {
			$ru = "Cincuenta y ocho ";
		} elseif ($u == 59) {
			$ru = "Cincuenta y nueve ";
		} elseif ($u == 60) {
			$ru = "Sesenta ";
		} elseif ($u == 61) {
			$ru = "Sesenta y un ";
		} elseif ($u == 62) {
			$ru = "Sesenta y dos ";
		} elseif ($u == 63) {
			$ru = "Sesenta y tres ";
		} elseif ($u == 64) {
			$ru = "Sesenta y cuatro ";
		} elseif ($u == 65) {
			$ru = "Sesenta y cinco ";
		} elseif ($u == 66) {
			$ru = "Sesenta y seis ";
		} elseif ($u == 67) {
			$ru = "Sesenta y siete ";
		} elseif ($u == 68) {
			$ru = "Sesenta y ocho ";
		} elseif ($u == 69) {
			$ru = "Sesenta y nueve ";
		} elseif ($u == 70) {
			$ru = "Setenta ";
		} elseif ($u == 71) {
			$ru = "Setenta y un ";
		} elseif ($u == 72) {
			$ru = "Setenta y dos ";
		} elseif ($u == 73) {
			$ru = "Setenta y tres ";
		} elseif ($u == 74) {
			$ru = "Setenta y cuatro ";
		} elseif ($u == 75) {
			$ru = "Setenta y cinco ";
		} elseif ($u == 76) {
			$ru = "Setenta y seis ";
		} elseif ($u == 77) {
			$ru = "Setenta y siete ";
		} elseif ($u == 78) {
			$ru = "Setenta y ocho ";
		} elseif ($u == 79) {
			$ru = "Setenta y nueve ";
		} elseif ($u == 80) {
			$ru = "Ochenta ";
		} elseif ($u == 81) {
			$ru = "Ochenta y un ";
		} elseif ($u == 82) {
			$ru = "Ochenta y dos ";
		} elseif ($u == 83) {
			$ru = "Ochenta y tres ";
		} elseif ($u == 84) {
			$ru = "Ochenta y cuatro ";
		} elseif ($u == 85) {
			$ru = "Ochenta y cinco ";
		} elseif ($u == 86) {
			$ru = "Ochenta y seis ";
		} elseif ($u == 87) {
			$ru = "Ochenta y siete ";
		} elseif ($u == 88) {
			$ru = "Ochenta y ocho ";
		} elseif ($u == 89) {
			$ru = "Ochenta y nueve ";
		} elseif ($u == 90) {
			$ru = "Noventa ";
		} elseif ($u == 91) {
			$ru = "Noventa y un ";
		} elseif ($u == 92) {
			$ru = "Noventa y dos ";
		} elseif ($u == 93) {
			$ru = "Noventa y tres ";
		} elseif ($u == 94) {
			$ru = "Noventa y cuatro ";
		} elseif ($u == 95) {
			$ru = "Noventa y cinco ";
		} elseif ($u == 96) {
			$ru = "Noventa y seis ";
		} elseif ($u == 97) {
			$ru = "Noventa y siete ";
		} elseif ($u == 98) {
			$ru = "Noventa y ocho ";
		} else {
			$ru = "Noventa y nueve ";
		}
		return $ru; //Retornar el resultado
	}


	/*
	* retorna en letras el valor de la decena $d
	*/
	public function decenas($d)
	{
		if ($d == 0) {
			$rd = "";
		} elseif ($d == 1) {
			$rd = "Ciento ";
		} elseif ($d == 2) {
			$rd = "Doscientos ";
		} elseif ($d == 3) {
			$rd = "Trescientos ";
		} elseif ($d == 4) {
			$rd = "Cuatrocientos ";
		} elseif ($d == 5) {
			$rd = "Quinientos ";
		} elseif ($d == 6) {
			$rd = "Seiscientos ";
		} elseif ($d == 7) {
			$rd = "Setecientos ";
		} elseif ($d == 8) {
			$rd = "Ochocientos ";
		} else {
			$rd = "Novecientos ";
		}
		return $rd; //Retornar el resultado
	}



	public function generatePdfpickin($pdf, $pickin)
	{
		$pdf = new PickinPdf('p', 'mm', 'letter');
		foreach (array_keys($pickin) as $key => $branchoffice_warehouse) {
			$pdf->SetTopMargin(24);

			$pdf->AddPage();
			$pdf->SetFont('Arial', '', 6);

			$pdf->SetAutoPageBreak(true, 10);

			$pdf->setY(20);
			$bW = BranchOfficeWarehouse::find($branchoffice_warehouse)->warehouse_description;
			$pdf->Cell(30, 4, utf8_decode('Bodega: ' . $bW), 1, 0, 'L', false);
			$pdf->ln(4);

			$pdf->setY(30);
			$pdf->Cell(36.8, 4, utf8_decode('ubicación'), 1, 0, 'L', false);
			$pdf->Cell(20, 4, utf8_decode('Código'), 1, 0, 'L', false);
			$pdf->Cell(90, 4, utf8_decode('Descripción'), 1, 0, 'L', false);
			$pdf->Cell(12, 4, utf8_decode('Cantidad'), 1, 0, 'L', false);
			$pdf->Cell(36.8, 4, utf8_decode('Línea'), 1, 0, 'L', false);

			$pdf->ln(4);

			$pdf->SetWidths([36.8, 20, 90, 12, 36.8]);
			$pdf->SetAligns(['L', 'L', 'L', 'L', 'L']);

			foreach ($pickin[$branchoffice_warehouse] as $product) {
				$pdf->Row([
					utf8_decode($product['location']),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					utf8_decode($product['quantity']),
					utf8_decode($product['line']),
				]);
			}
		}

		$fileName = 'Pickin.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return $fileName;
	}





	public function generatePdfReceivable($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoices($request->client);


		$pdf = new PdfDocumentsBalance('p', 'mm', 'legal');
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 55);
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				if (count($documents) - 1 > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = $document->prefix ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'vencida') {
						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {
						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}
		}

		$y=$pdf->GetY();
		//msm  dpartamento cartera
		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->SetXY($pdf->GetX(), 60);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln();
		}
		$pdf->SetY($y);
		$pdf->SetXY($pdf->GetX(), $pdf->GetY() - 30);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Estimado Cliente:'), 0, 0, 'L', false);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('A continuación encontrará su estado de cuenta actualizado a la fecha.'), 0, 0, 'L', false);
		$pdf->Ln();
		$pdf->Cell(195.5, 4, utf8_decode('Cualquier inquietud favor comunicarse con nuestro departamento de cartera.'), 0, 0, 'L', false);

		// pinto la informacion de mis facturas
		$pdf->SetXY($pdf->GetX(), $pdf->GetY() + 20);
		$pdf->Ln();
		$pdf->SetWidths([35, 35, 35, 35, 22, 33.5]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode($dataT->status),
					utf8_decode('$ ' . number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		$pdf->ln(15);
		$pdf->SetDrawColor(230, 230, 230);
		$pdf->SetFillColor(245, 245, 255);

		// valores de facturas vencidas y ok
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(162, 4, utf8_decode('Total Cartera Vencida'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalVencidas)), 0, 0, 'C', false);
		$pdf->Ln();
		$pdf->Cell(162, 4, utf8_decode('Total Cartera Normal'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalOk)), 0, 0, 'C', false);
		$pdf->Ln();
		$pdf->Cell(162, 4, utf8_decode('Total Cartera'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalOk + $totalVencidas)), 0, 0, 'C', false);
		$pdf->Ln(30);

		//   espacio de observaciones

		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->SetFont('Arial', '');

		if($pdf->getY() >= 256){
			$pdf->AddPage();
		}

		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {

			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Text(123, $pdf->GetY() + 15, 'Cordialmente:');
				$pdf->Ln();
				$pdf->Image($pathSignature, 120, $pdf->getY() + 15, 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Estado de Cuenta - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	public function generatePdfReceivableCol($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoicesCol($request->client);
		//dd(count($documents));
		$subscriber_id = Auth::user()->subscriber_id;

		$city = Contact::where('contacts.id', $request->client)->join('cities','cities.id','=','contacts.city_id')->join('departments','departments.id','=','cities.department_id')->select('cities.id','cities.city_code','cities.city','departments.id as code_departament','departments.department')->first();
		$credit_quota = Contact::where('contacts.id', $request->client)->join('contacts_customers','contacts_customers.contact_id','=','contacts.id')->select('credit_quota')->first();
		$images = Subscriber::where('id','=',$subscriber_id)->select('logo')->first();

		$pdf = new PdfDocumentsBalanceCol('p', 'mm', 'legal');
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 55);
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->city = $city;
		$pdf->credit_quota = $credit_quota;
		$pdf->logo_cliente = $images;
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				//echo $key;
				if (count($documents) > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = empty($document->prefix) ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'Vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'Vencida') {

						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {

						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}

		}
		//var_dump($dataTable);
		//die();
		$y=$pdf->GetY();
		//msm  dpartamento cartera
		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->SetXY($pdf->GetX(), 60);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln();
		}
		$pdf->SetY($y);
		$pdf->SetXY($pdf->GetX(), $pdf->GetY() - 30);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Estimado Cliente:'), 0, 0, 'L', false);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('A continuación encontrará su estado de cuenta actualizado a la fecha.'), 0, 0, 'L', false);
		$pdf->Ln();
		$pdf->Cell(195.5, 4, utf8_decode('Cualquier inquietud favor comunicarse con nuestro departamento de cartera.'), 0, 0, 'L', false);

		// pinto la informacion de mis facturas
		$pdf->SetXY($pdf->GetX(), $pdf->GetY() + 20);
		$pdf->Ln();
		$pdf->SetWidths([35, 35, 35, 35, 22, 33.5]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode($dataT->status),
					utf8_decode('$ ' . number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		$pdf->ln(15);
		$pdf->SetDrawColor(230, 230, 230);
		$pdf->SetFillColor(245, 245, 255);

		// valores de facturas vencidas y ok
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(162, 4, utf8_decode('Cupo Asignado'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($credit_quota->credit_quota)), 0, 0, 'C', false);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(162, 4, utf8_decode('Total Cartera Vencida'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalVencidas)), 0, 0, 'C', false);
		$pdf->Ln();
		$pdf->Cell(162, 4, utf8_decode('Total Cartera Normal'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalOk)), 0, 0, 'C', false);
		$pdf->Ln();
		$pdf->Cell(162, 4, utf8_decode('Total Cartera'), 0, 0, 'R', false);
		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($totalOk + $totalVencidas)), 0, 0, 'C', false);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(162, 4, utf8_decode('Cupo Disponible'), 0, 0, 'R', false);
		$credit_quota->credit_quota-($totalOk + $totalVencidas)<0?$pdf->SetTextColor(194,8,8):$pdf->SetTextColor(0,0,0);

		$pdf->Cell(33.5, 4, utf8_decode('$ ' . number_format($credit_quota->credit_quota-($totalOk + $totalVencidas))), 0, 0, 'C', false);
		$pdf->Ln(20);

		//   espacio de observaciones

		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->SetFont('Arial', '');

		if($pdf->getY() >= 256){
			$pdf->AddPage();
		}

		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {

			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Text(123, $pdf->GetY() + 15, 'Cordialmente:');
				$pdf->Ln();
				$pdf->Image($pathSignature, 120, $pdf->getY() + 15, 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Estado de Cuenta - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	public function generateNegativeFile($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoices($request->client);


		$pdf = new PdfNegativeFile('p', 'mm', 'legal');
		$pdf->SetTopMargin(47);
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				if (count($documents) - 1 > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = $document->prefix ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'vencida') {
						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {
						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}
		}
		$pdf->setXY(180,$pdf->getY());
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(20, 9,utf8_decode('Bogotá, '.date('Y-m-d')), 0, 0, 'R');


		//msm  dpartamento cartera
		$pdf->SetY($pdf->GetY() +30);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Señor(es)'), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->Cell(195.5, 4, utf8_decode(strtoupper($pdf->contact->name) .' '.strtoupper($pdf->contact->surname)), 0, 0, 'L', false);
		$pdf->Ln(8);
		$pdf->Cell(195.5, 4, utf8_decode('NIT: '.strtoupper($pdf->contact->identification)), 0, 0, 'L', false);
		$pdf->Ln(8);

		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln();
		}

		$pdf->Cell(15, 4, utf8_decode('Asunto: '), 0, 0, 'L', false);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(40, 4, utf8_decode('Aviso Reporte Negativo'), 0, 0, 'L', false);



		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(6);

		$pdf->MultiCell(195.5, 4, utf8_decode('
		El  presente  comunicado  tiene  por  objeto  notificarle  que  será  reportado  próximamente  a  Centrales  de  Riesgo
		(Data-Crédito), de acuerdo  a  ley 1266 (habeas data), debido  a  que  en  varias  oportunidades  le  hemos  solicitado
		la cancelación  de  la  deuda  que  presenta  con  COLSAISA SAS  correspondiente  a  compras de mercancía,  a  la
		fecha presenta una mora mayor a 90 días y cuya obligación se encuentra plenamente aceptada por usted:
		'), 0, 'L', false);
		$pdf->Ln();

		 //encabezado de mi tabla
         $pdf->ln(6);
        $pdf->SetTextColor(255,255,255);
        $pdf->Image('storage/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
        $pdf->SetFont('Arial','B', 10);

        if ($pdf->showValues) {
            $pdf->Cell(37, 8, utf8_decode('N° DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA VTO'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('DIAS'), 0, 0, 'C');
            $pdf->Cell(47, 8, utf8_decode('VALOR'), 0, 0, 'C');
        }


        $pdf->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);



		// pinto la informacion de mis facturas
		$pdf->SetTextColor(1,1,1);
		$pdf->Ln(8);
		$pdf->SetWidths([37, 37, 37, 37, 48]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode( number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		if (count($dataTable) > 0) {
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->SetFillColor(245, 245, 255);
			$pdf->Cell(148, 10, utf8_decode('Total Cartera '), 1, 0, 'C', true);
			$pdf->Cell(48, 10, utf8_decode('$ ' . number_format($totalVencidas+$totalOk)), 1, 0, 'C', true);
		}


		$pdf->SetFont('Arial','', 10);
		$pdf->Ln(20);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Esperando que en esta ocasión tengamos manifiesto de pago por parte de usted antes de    20 días después de la fecha
		de envío   del presente año. Vencido este término procederemos a  reflejar su mora  adquirida con nosotros en  la central
		de riesgos Data-Crédito.'), 0, 'L', false);


		$pdf->Ln(6);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Para  mayor información  puede  dirigirse  a  nuestras   oficinas  ubicadas en la  Cra  124  N  17 - 80  Piso 2  Bogotá  D.C.,
		comunicarse  con  nosotros  a  través  de   nuestro PBX 7452248  ext. 1210, celular  310  2124657  con  el  Departamento
		de  Cartera  o  a  los  correos  cartera@colsaisa.com  o  analista.cartera@olsaisa.com'), 0, 'L', false);


		// $pdf->ln(15);
		// $pdf->SetDrawColor(230, 230, 230);
		// $pdf->SetFillColor(245, 245, 255);



		 //   espacio de observaciones
		 $pdf->Ln(8);
		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->Ln(8);
		$pdf->Cell(148, 10, utf8_decode('Atentamente, '), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(148, 10, utf8_decode('Departamento de cartera y cobro. '), 0, 0, 'L', false);



		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {
			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->Ln();
				$pdf->Image($pathSignature, 8, $pdf->getY() , 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Aviso Reporte Negativo - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	public function generateNegativeFileCol($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoicesCol($request->client);
		$subscriber_id = Auth::user()->subscriber_id;
		$images = Subscriber::where('id','=',$subscriber_id)->select('logo')->first();

		$pdf = new PdfNegativeFileCol('p', 'mm', 'legal');
		$pdf->SetTopMargin(47);
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->logo_cliente = $images;
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				if (count($documents) - 1 > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = $document->prefix ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'vencida') {
						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {
						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}
		}
		$pdf->setXY(180,$pdf->getY());
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(20, 9,utf8_decode('Bogotá, '.date('Y-m-d')), 0, 0, 'R');


		//msm  dpartamento cartera
		$pdf->SetY($pdf->GetY() +30);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Señor(es)'), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->Cell(195.5, 4, utf8_decode(strtoupper($pdf->contact->name) .' '.strtoupper($pdf->contact->surname)), 0, 0, 'L', false);
		$pdf->Ln(8);
		$pdf->Cell(195.5, 4, utf8_decode('NIT: '.strtoupper($pdf->contact->identification)), 0, 0, 'L', false);
		$pdf->Ln(8);

		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln();
		}

		$pdf->Cell(15, 4, utf8_decode('Asunto: '), 0, 0, 'L', false);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(40, 4, utf8_decode('Aviso Reporte Negativo'), 0, 0, 'L', false);



		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(6);

		$pdf->MultiCell(195.5, 4, utf8_decode('
		El  presente  comunicado  tiene  por  objeto  notificarle  que  será  reportado  próximamente  a  Centrales  de  Riesgo
		(Data-Crédito), de acuerdo  a  ley 1266 (habeas data), debido  a  que  en  varias  oportunidades  le  hemos  solicitado
		la cancelación  de  la  deuda  que  presenta  con  COLSAISA SAS  correspondiente  a  compras de mercancía,  a  la
		fecha presenta una mora mayor a 90 días y cuya obligación se encuentra plenamente aceptada por usted:
		'), 0, 'L', false);
		$pdf->Ln();

		 //encabezado de mi tabla
         $pdf->ln(6);
        $pdf->SetTextColor(255,255,255);
        $pdf->Image('storage/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
        $pdf->SetFont('Arial','B', 10);

        if ($pdf->showValues) {
            $pdf->Cell(37, 8, utf8_decode('N° DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA VTO'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('DIAS'), 0, 0, 'C');
            $pdf->Cell(47, 8, utf8_decode('VALOR'), 0, 0, 'C');
        }


        $pdf->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);



		// pinto la informacion de mis facturas
		$pdf->SetTextColor(1,1,1);
		$pdf->Ln(8);
		$pdf->SetWidths([37, 37, 37, 37, 48]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode( number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		if (count($dataTable) > 0) {
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->SetFillColor(245, 245, 255);
			$pdf->Cell(148, 10, utf8_decode('Total Cartera '), 1, 0, 'C', true);
			$pdf->Cell(48, 10, utf8_decode('$ ' . number_format($totalVencidas+$totalOk)), 1, 0, 'C', true);
		}


		$pdf->SetFont('Arial','', 10);
		$pdf->Ln(20);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Esperando que en esta ocasión tengamos manifiesto de pago por parte de usted antes de    20 días después de la fecha
		de envío   del presente año. Vencido este término procederemos a  reflejar su mora  adquirida con nosotros en  la central
		de riesgos Data-Crédito.'), 0, 'L', false);


		$pdf->Ln(6);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Para  mayor información  puede  dirigirse  a  nuestras   oficinas  ubicadas en la  Cra  124  N  17 - 80  Piso 2  Bogotá  D.C.,
		comunicarse  con  nosotros  a  través  de   nuestro PBX 7452248  ext. 1210, celular  310  2124657  con  el  Departamento
		de  Cartera  o  a  los  correos  cartera@colsaisa.com  o  analista.cartera@olsaisa.com'), 0, 'L', false);


		// $pdf->ln(15);
		// $pdf->SetDrawColor(230, 230, 230);
		// $pdf->SetFillColor(245, 245, 255);



		 //   espacio de observaciones
		 $pdf->Ln(8);
		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->Ln(8);
		$pdf->Cell(148, 10, utf8_decode('Atentamente, '), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(148, 10, utf8_decode('Departamento de cartera y cobro. '), 0, 0, 'L', false);



		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {
			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->Ln();
				$pdf->Image($pathSignature, 8, $pdf->getY() , 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Aviso Reporte Negativo - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}


	public function generateFilePrelegal($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoices($request->client);


		$pdf = new PdfNegativeFile('p', 'mm', 'legal');
		$pdf->SetTopMargin(47);
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				if (count($documents) - 1 > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = $document->prefix ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'vencida') {
						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {
						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}
		}
		$pdf->setXY(180,$pdf->getY());
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(20, 9,utf8_decode('Bogotá, '.date('Y-m-d')), 0, 0, 'R');


		//msm  dpartamento cartera
		$pdf->SetY($pdf->GetY() +20);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Señor(es)'), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode(strtoupper($pdf->contact->name) .' '.strtoupper($pdf->contact->surname)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('NIT: '.strtoupper($pdf->contact->identification)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('DIRECCIÓN: '.strtoupper($pdf->contact->address)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('CIUDAD: '.strtoupper($pdf->contact->city->city)), 0, 0, 'L', false);
		$pdf->Ln(8);

		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln(6);
		}


		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(15, 4, utf8_decode('Asunto: '), 0, 0, 'L', false);
		$pdf->Cell(40, 4, utf8_decode('Aviso Pre jurídico'), 0, 0, 'L', false);



		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(10);
		$pdf->Cell(40, 4, utf8_decode('Estimados Señores '), 0, 0, 'L', false);
		$pdf->Ln();
		$pdf->MultiCell(195.5, 4, utf8_decode('
		El presente comunicado tiene por objeto notificarle que fue reportado a Centrales de Riesgo (Data-Crédito), de acuerdo
		a ley 1266 (habeas data), debido  a  que en  varias  oportunidades le hemos solicitado  la  cancelación de la deuda que
		presenta  con  COLSAISA SAS  correspondiente  a compras de  mercancía, a  la fecha presenta una mora mayor a 150
		días y cuya obligación se encuentra plenamente aceptada por ustedes:'), 0, 'L', false);
		$pdf->Ln();

		 //encabezado de mi tabla
         $pdf->ln(5);
        $pdf->SetTextColor(255,255,255);
        $pdf->Image('storage/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
        $pdf->SetFont('Arial','B', 10);

        if ($pdf->showValues) {
            $pdf->Cell(37, 8, utf8_decode('N° DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA VTO'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('DIAS'), 0, 0, 'C');
            $pdf->Cell(47, 8, utf8_decode('VALOR'), 0, 0, 'C');
        }


        $pdf->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);



		// pinto la informacion de mis facturas
		$pdf->SetTextColor(1,1,1);
		$pdf->Ln(8);
		$pdf->SetWidths([37, 37, 37, 37, 48]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode( number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		if (count($dataTable) > 0) {
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->SetFillColor(245, 245, 255);
			$pdf->Cell(148, 10, utf8_decode('Total Cartera '), 1, 0, 'C', true);
			$pdf->Cell(48, 10, utf8_decode('$ ' . number_format($totalVencidas+$totalOk)), 1, 0, 'C', true);
		}


		$pdf->SetFont('Arial','', 10);
		$pdf->Ln(20);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Esperando que en esta ocasión tengamos manifiesto de pago por parte de usted antes de   05 días después de envió
		de esta carta  del presente año. Vencido este término procederemos a entregar su cuenta a nuestros Abogados para
		cobro, ya que los compromisos adquiridos no han sido efectivos.'), 0, 'L', false);


		$pdf->Ln(6);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Para  mayor información puede  dirigirse a nuestras  oficinas ubicadas en  la Cra  124 N  17 - 80  Piso  2  Bogotá D.C.,
		comunicarse  con nosotros  a  través de nuestro PBX 7452248 ext. 1211, celular 315  8615698  con  el  Departamento
		de Cartera al correo,prejuridicos@colsaisa.com.'), 0, 'L', false);

		 //   espacio de observaciones
		 $pdf->Ln(8);
		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->Ln(8);
		$pdf->Cell(148, 10, utf8_decode('Atentamente, '), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(148, 10, utf8_decode('Departamento de cartera y cobro. '), 0, 0, 'L', false);



		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {
			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->Ln();
				$pdf->Image($pathSignature, 8, $pdf->getY() , 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Aviso Pre jurídico - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	public function generateFilePrelegalCol($pdf, $documentRepository, $request, $show_Values = true)
	{

		//obtengo la data a pintar
		$documents =  $documentRepository->getInvoicesCol($request->client);
		$subscriber_id = Auth::user()->subscriber_id;
		$images = Subscriber::where('id','=',$subscriber_id)->select('logo')->first();

		$pdf = new PdfNegativeFileCol('p', 'mm', 'legal');
		$pdf->SetTopMargin(47);
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->logo_cliente = $images;
		$pdf->contact = Contact::where('id', $request->client)->with('city.department')->with('users')->first();
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetAutoPageBreak(true, 70);
		$totalVencidas = 0;
		$totalOk = 0;



		// centralizo los datos que necesito en el array dataTable
		$dataTable = collect();
		if (count($documents) > 0) {
			$today = strtotime(date("Y-m-d"));
			foreach ($documents as $key => $document) {
				if (count($documents) - 1 > $key) {


					$elementDataTable = new stdClass();
					$elementDataTable->document = $document->prefix ? $document->prefix . ' - ' . str_replace('-', '', $document->numeroFactura) : '' . str_replace('-', '', $document->numeroFactura);
					$elementDataTable->issue_date = substr($document->issue_date, 0, 10);
					$elementDataTable->due_date = $document->fechaVencimiento;

					// determino los dias que tiene la factura
					$currentDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
					$issueDate = Carbon::createFromFormat('Y-m-d', date("Y-m-d", strtotime(date($document->issue_date))));

					$days = $currentDate->diffInDays($issueDate);
					$elementDataTable->number_days = $days;
					$elementDataTable->status = $today > strtotime(date($document->fechaVencimiento)) ? 'vencida' : 'Normal';
					$elementDataTable->saldo = $document->saldo + $document->descuentoPP;

					//determino loa sumatoria de facturas vencidas y ok

					if ($elementDataTable->status == 'vencida') {
						$totalVencidas += $elementDataTable->saldo;
					} elseif ($elementDataTable->status == 'Normal') {
						$totalOk += $elementDataTable->saldo;
					}



					$dataTable->push($elementDataTable);
				}
			}
		}
		$pdf->setXY(180,$pdf->getY());
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(20, 9,utf8_decode('Bogotá, '.date('Y-m-d')), 0, 0, 'R');


		//msm  dpartamento cartera
		$pdf->SetY($pdf->GetY() +20);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(195.5, 4, utf8_decode('Señor(es)'), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode(strtoupper($pdf->contact->name) .' '.strtoupper($pdf->contact->surname)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('NIT: '.strtoupper($pdf->contact->identification)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('DIRECCIÓN: '.strtoupper($pdf->contact->address)), 0, 0, 'L', false);
		$pdf->Ln(6);
		$pdf->Cell(195.5, 4, utf8_decode('CIUDAD: '.strtoupper($pdf->contact->city->city)), 0, 0, 'L', false);
		$pdf->Ln(8);

		if($request->legalrepresentative!=''){
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Atte. Sr(a): '.($request->legalrepresentative)), 0, 0, 'L', false);
			$pdf->Ln(6);
		}


		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(15, 4, utf8_decode('Asunto: '), 0, 0, 'L', false);
		$pdf->Cell(40, 4, utf8_decode('Aviso Pre jurídico'), 0, 0, 'L', false);



		$pdf->SetFont('Arial', '', 10);
		$pdf->Ln(10);
		$pdf->Cell(40, 4, utf8_decode('Estimados Señores '), 0, 0, 'L', false);
		$pdf->Ln();
		$pdf->MultiCell(195.5, 4, utf8_decode('
		El presente comunicado tiene por objeto notificarle que fue reportado a Centrales de Riesgo (Data-Crédito), de acuerdo
		a ley 1266 (habeas data), debido  a  que en  varias  oportunidades le hemos solicitado  la  cancelación de la deuda que
		presenta  con  COLSAISA SAS  correspondiente  a compras de  mercancía, a  la fecha presenta una mora mayor a 150
		días y cuya obligación se encuentra plenamente aceptada por ustedes:'), 0, 'L', false);
		$pdf->Ln();

		 //encabezado de mi tabla
         $pdf->ln(5);
        $pdf->SetTextColor(255,255,255);
        $pdf->Image('storage/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
        $pdf->SetFont('Arial','B', 10);

        if ($pdf->showValues) {
            $pdf->Cell(37, 8, utf8_decode('N° DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA DOC'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('FECHA VTO'), 0, 0, 'C');
            $pdf->Cell(37, 8, utf8_decode('DIAS'), 0, 0, 'C');
            $pdf->Cell(47, 8, utf8_decode('VALOR'), 0, 0, 'C');
        }


        $pdf->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);



		// pinto la informacion de mis facturas
		$pdf->SetTextColor(1,1,1);
		$pdf->Ln(8);
		$pdf->SetWidths([37, 37, 37, 37, 48]);
		$pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C']);
		if (count($dataTable) > 0) {
			foreach ($dataTable as $key => $dataT) {

				$pdf->Row([
					utf8_decode($dataT->document),
					utf8_decode($dataT->issue_date),
					utf8_decode($dataT->due_date),
					utf8_decode($dataT->number_days),
					utf8_decode( number_format($dataT->saldo))
				]);
			}
		} else {
			$pdf->Cell(195.5, 4, utf8_decode('No tiene facturas pendientes por pago.'), 0, 0, 'C', false);
		}

		if (count($dataTable) > 0) {
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->SetFillColor(245, 245, 255);
			$pdf->Cell(148, 10, utf8_decode('Total Cartera '), 1, 0, 'C', true);
			$pdf->Cell(48, 10, utf8_decode('$ ' . number_format($totalVencidas+$totalOk)), 1, 0, 'C', true);
		}


		$pdf->SetFont('Arial','', 10);
		$pdf->Ln(20);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Esperando que en esta ocasión tengamos manifiesto de pago por parte de usted antes de   05 días después de envió
		de esta carta  del presente año. Vencido este término procederemos a entregar su cuenta a nuestros Abogados para
		cobro, ya que los compromisos adquiridos no han sido efectivos.'), 0, 'L', false);


		$pdf->Ln(6);
		$pdf->MultiCell(195.5, 4, utf8_decode('
		Para  mayor información puede  dirigirse a nuestras  oficinas ubicadas en  la Cra  124 N  17 - 80  Piso  2  Bogotá D.C.,
		comunicarse  con nosotros  a  través de nuestro PBX 7452248 ext. 1211, celular 315  8615698  con  el  Departamento
		de Cartera al correo,analista.cartera@colsaisa.com.'), 0, 'L', false);

		 //   espacio de observaciones
		 $pdf->Ln(8);
		if ($request->observation != null && $request->observation != '') {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(195.5, 4, utf8_decode('Observación '), 0, 0, 'L', false);
			$pdf->Ln(6);
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->Rect(10, $pdf->getY() - 2, 195.5, 30);
			$pdf->SetFont('Arial', '', 7);
			$pdf->Write(3, utf8_decode($request->observation));
			$pdf->Ln(20);
			$pdf->Write(3, '    ');
			$pdf->Ln();
		}

		$pdf->Ln(8);
		$pdf->Cell(148, 10, utf8_decode('Atentamente, '), 0, 0, 'L', false);
		$pdf->Ln(4);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(148, 10, utf8_decode('Departamento de cartera y cobro. '), 0, 0, 'L', false);



		//  firma de la persona que envia el correo
		if (Auth::user()->signature) {
			$pathSignature = storage_path() . '/app/public/signature/' . Auth::user()->signature;

			if (file_exists($pathSignature)) {
				$pdf->Ln();
				$pdf->Image($pathSignature, 8, $pdf->getY() , 80);
				$pdf->SetFont('Arial', '', 10);
			}
		}






		$fileName = 'Aviso Pre jurídico - ' . $pdf->contact->name . ' ' . $pdf->contact->surname . ' - ' . date('Y-m-d') . '.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		// if (file_exists($ruta)) {
		// 	unlink($ruta) or die("Couldn't delete file");
		// }
		$pdf->Output($ruta, 'F');
		return $fileName;
	}


	public function generateAccountingDocument($accounting_document)
	{
		$pdf = new AccountingDocumentPDF('p', 'mm', 'letter');
		$pdf->PDFDocument = $accounting_document;
        $pdf->SetFont('Arial', '', 6);
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		
		$pdf->ln(6);


		$columns = $pdf->validateColumns();
        $columns_witdh = $pdf->calculateColumns($columns);
		$widths = [$columns_witdh["Cuenta Contable"],26];
		$aligns = ['L', 'L'];

		if ($columns['manage_contact_balances']) {
			array_push($widths, $columns_witdh["Tercero"]);
			array_push($aligns, 'L');
		}
		
		array_push($widths,25,25);
		array_push($aligns,'L','L');

		if ($columns['manage_projects']) {
			array_push($widths, 21);
			array_push($aligns, 'L');
        }
        
        if ($columns['cost_center_1']) {
			array_push($widths, 21);
			array_push($aligns, 'L');
		}
		if ($columns['manage_way_to_pay']) {
			array_push($widths, 21);
			array_push($aligns, 'L');
		}

		$pdf->SetWidths($widths);
		$pdf->SetAligns($aligns);
        $pdf->SetFillColor(0, 0,0);


		
		$total_debit = 0;
		$total_credit = 0;
		foreach ($accounting_document->accounting_transactions as $key => $value) {
			if ($pdf->getY() >= 235) {
				$pdf->AddPage();
				$pdf->setXY(10, 78);
			}
			
			$total_debit += $value->transactions_type == 1 ? $value->operation_value : 0;
			$total_credit += $value->transactions_type == 2 ? $value->operation_value : 0;
	

			$contact = substr($value->contact->identification .' - '.$value->contact->name.' '.$value->contact->surname, 0, 47);
			$contact = strlen($contact) >= 39 ? $contact.'...' : $contact;

			$data = [
				utf8_decode($value->account->code . '-' .$value->account->name),
			];
			
			if (!is_null($value->affectsDocument)) {
				array_push($data, utf8_decode($value->affectsDocument->prefix != null ? trim($value->affectsDocument->prefix) . ' - ' .$value->affectsDocument->consecutive : $value->affectsDocument->consecutive));
			}else{
				array_push($data, utf8_decode(''));
			}

			if ($columns['manage_contact_balances']) {
				array_push($data, utf8_decode($contact));
			}
			array_push($data,
				// utf8_decode(''),
				// utf8_decode($value->concept),
				$value->transactions_type == 'D' ? '$'.number_format($value->operation_value, 2, ',', '.') : '',
				$value->transactions_type == 'C' ? '$'.number_format($value->operation_value, 2, ',', '.') : ''
			);

			if ($columns['manage_projects']) {
				array_push($data, utf8_decode($value->projects != null ? $value->projects->description : '' ));
			}
			if ($columns['cost_center_1']) {
				array_push($data, utf8_decode(''));
			}
			if ($columns["manage_way_to_pay"]) {
				array_push($data, utf8_decode($value->payment != null ? $value->payment->code_parameter .'-'. $value->payment->name_parameter : ''));
			}
			$pdf->Row($data);

			$pdf->setX(10);

			if ($value->observations != $accounting_document->observation && !is_null($value->observations) && $value->observations != '') {
				$pdf->SetFont('Arial', '', 7);
				$pdf->Cell(array_sum($widths), 4, utf8_decode('Detalle: '. $value->observations), 1, 0, 'LRTB', false);
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(8);
			}
		}

		$pdf->SetFillColor(244, 245, 247);
        $pdf->SetDrawColor(230, 230, 230);

		$pdf->ln(4);

		$pdf->setX(array_sum($widths)+10-55);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(20, 4, utf8_decode('Total Debito'), 0, 0, 'R', true);
        $pdf->Cell(35, 4, '$'.number_format($accounting_document->accounting_debit_value, 0, '', '.'), 'TR', 0, 'L', false);
		$pdf->ln(4);
		
		$pdf->setX(array_sum($widths)+10-55);
        $pdf->Cell(20, 4, utf8_decode('Total Credito'), 0, 0, 'R', true);
        $pdf->Cell(35, 4, '$'.number_format($accounting_document->accounting_credit_value, 0, '', '.'), 'RTB', 0, 'L', false);
		$pdf->ln(4);


        $pdf->SetFont('Arial', '', 5.5);

		$pdf->setX(10);
        $pdf->Cell(20, 4, utf8_decode('Usuario Elaboracion: '. $accounting_document->user->name .' '. $accounting_document->user->surname), 0, 0, 'L', false);
		$pdf->ln(4);

		$pdf->setX(10);
		$pdf->Cell(20, 4, utf8_decode('Fecha Elaboracion: '.$accounting_document->created_at), 0, 0, 'L', false);
		$pdf->ln(4);

		// $pdf->setX(160);
        // $pdf->Cell(20, 4, utf8_decode('Diferencia'), 0, 0, 'R', true);
        // $pdf->Cell(20, 4, '$'.number_format($total_credit >= $total_debit ?  $total_credit - $total_debit : $total_debit - $total_credit, 0, '', '.'), 'RB', 0, 'L', false);
        // $pdf->ln(4);

		$fileName = ($accounting_document->prefix != null ? trim($accounting_document->prefix) . '-' .$accounting_document->consecutive : $accounting_document->consecutive).'.pdf';
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return $fileName;
	}

	/**
	 * pdf cuadre de caja
	 * @author Kevin Galindo
	 */
	public function generateSquareCashRegisterPdf($pdf, $documentRepository, $request)
	{
		$dateFrom = $request->dateFrom." ".$request->timeFrom;
		$dateTo = $request->dateTo." ".$request->timeTo;

		$detailDocumentsPaymentMethods = $documentRepository->getDetailDocumentsPaymentMethods($dateFrom, $dateTo, $request->branchoffice_id);
		$squareCashRegister = $documentRepository->getSquareCashRegister($dateFrom, $dateTo, $request->branchoffice_id);

		$squareCashRegister = $squareCashRegister->groupBy('model_description');
		
		$squareCashRegister = $squareCashRegister->map(function ($item, $key) {
			$item = $item->groupBy('document_consecutive');
			return $item;
		});

		$pdf = new SquareCashRegisterPdf('p', 'mm', 'letter');
		$pdf->SetMargins(10, 10, 1);
		$pdf->AliasNbPages();


		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);

		$pdf->SetAutoPageBreak(true, 10);

		//header
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13, $pdf->getY(), 25, 15);
		// $pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13,  $pdf->getY(), 40, 10);

		//body
		// $pdf->ln(5);
		// $pdf->setX(80);
		$pdf->SetFillColor(235, 235, 235);
		
		// Titulo
		$pdf->SetFont('Arial', 'B', 18);
		$pdf->Cell(190, 4, utf8_decode('CUADRE DE CAJA'), 0, 0, 'C', false);		


		$pdf->SetFont('Arial', 'B', 6);
		$pdf->ln(8);
		$pdf->setX(80);
		$pdf->Cell(25, 4, utf8_decode('Fec Desde: '.$request->dateFrom), 0, 0, 'L', false);		
		$pdf->Cell(25, 4, utf8_decode('Hora Desde: '.$request->timeFrom), 0, 0, 'R', false);	
		$pdf->ln(4);
		$pdf->setX(80);
		$pdf->Cell(25, 4, utf8_decode('Fec Hasta: '.$request->dateTo), 0, 0, 'L', false);		
		$pdf->Cell(25, 4, utf8_decode('Hora Hasta: '.$request->timeTo), 0, 0, 'R', false);
		$pdf->ln(5);

		// Tabla del resumen
		$pdf->ln(3);
		$pdf->setX(20);
		$pdf->setY(30);
		

		$pdf->ln(10);
		// $pdf->SetFillColor(255, 251, 239);
		$pdf->Cell(50, 4, utf8_decode('Resumen Formas de Pago'), 'B', 0, 'C', true);		

		$paymentMethodsTotal = 0;
		foreach ($detailDocumentsPaymentMethods as $key => $item) {
			$pdf->ln(4);
			$pdf->Cell(25, 4,    utf8_decode($item->name_parameter), 0, 0, 'L', false);
			$pdf->Cell(25, 4, '$ ' . number_format($item->value, 2, ',', '.'), 0, 0, 'R', false);
			$paymentMethodsTotal+=$item->value;
		}

		$pdf->ln(6);
		$pdf->Cell(25, 4, 'TOTAL INGRESO...', 0, 0, 'L', false);
		$pdf->Cell(25, 4, '$ ' . number_format($paymentMethodsTotal, 2, ',', '.'), 0, 0, 'R', false);

		// Tabla principal
		$pdf->ln(15);
		$pdf->setX(10);
		// $pdf->Image(storage_path() . '/app/public/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
		$pdf->Cell(65, 4, utf8_decode('Tercero'), 'B', 0, 'C', true);
		$pdf->Cell(15, 4, utf8_decode('Doc Ref'), 'B', 0, 'C', true);
		$pdf->Cell(20, 4, utf8_decode('Total'), 'B', 0, 'C', true);
		$pdf->Cell(20, 4, utf8_decode('Bruto'), 'B', 0, 'C', true);
		$pdf->Cell(20, 4, utf8_decode('IVA'), 'B', 0, 'C', true);
		$pdf->Cell(30, 4, utf8_decode('Formas de pago'), 'B', 0, 'C', true);
		$pdf->Cell(15, 4, utf8_decode('Valor'), 'B', 0, 'C', true);

		foreach ($squareCashRegister as $key => $items) {
			$pdf->ln(6);	
			$pdf->setX(10);
			$pdf->Cell(185.5, 4, $key, 'B', 0, 'L', false); 

			foreach ($items as $keyDoc => $documents) {
				
				foreach ($documents as $keyD => $document) {
					$fullNameContact = $document->contact_identification ." - ". $document->contact_name;

					$pdf->ln(4);	
					$pdf->setX(10);

					$validate = false;
					if ($keyD > 0) {
						$validate = $documents[$keyD - 1]->document_id === $documents[$keyD]->document_id;
					}

					if (!$validate) {
						$pdf->Cell(15, 4, utf8_decode($document->document_consecutive), 0, 0, 'C', false);
						$pdf->Cell(50, 4, utf8_decode( strlen($fullNameContact) > 38 ? substr($fullNameContact, 0, 35)."..." : $fullNameContact ), 0, 0, 'L', false);
						$pdf->Cell(15, 4, utf8_decode($document->thread_consecutive), 0, 0, 'C', false);
						$pdf->Cell(20, 4, '$ ' . number_format($document->document_total, 2, ',', '.'), 0, 0, 'R', false);
						$pdf->Cell(20, 4, '$ ' . number_format($document->document_total_brut, 2, ',', '.'), 0, 0, 'R', false);
						$pdf->Cell(20, 4, '$ ' . number_format($document->total_iva, 2, ',', '.'), 0, 0, 'R', false);
					} else {
						$pdf->Cell(140, 4, utf8_decode(''), 0, 0, 'C', false);
					}

					// Metodos de pago
					$pdf->Cell(30, 4, utf8_decode($document->payment_method), 0, 0, 'R', false);
					$pdf->Cell(15, 4, '$ ' . number_format($document->payment_method_value, 2, ',', '.'), 0, 0, 'R', false);
				}
			}
		}

		$existe_directorio = Storage::exists('/public/cuadrecaja');

        if (!$existe_directorio){
            Storage::makeDirectory('/public/cuadrecaja');
        }

		$fileName = uniqid().'.pdf';
		// $fileName = 'test.pdf';
		$ruta = storage_path('/app/public/cuadrecaja/').$fileName;
		

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return 'cuadrecaja/'.$fileName;
	}

	/**
	 * pdf alerta asesores sin activdad
	 * @author Kevin Galindo
	 */
	public function generateAlertEmployeesWithoutActivityPdf($pdf, $data)
	{
		$pdf = new SquareCashRegisterPdf('p', 'mm', 'letter');
		$pdf->SetMargins(10, 10, 1);
		$pdf->AliasNbPages();


		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 6);

		$pdf->SetAutoPageBreak(true, 10);

		//header
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13, $pdf->getY(), 25, 15);
		// $pdf->Image(storage_path() . '/app/public/colsaisa_logo.png', 13,  $pdf->getY(), 40, 10);

		//body
		// $pdf->ln(5);
		// $pdf->setX(80);
		$pdf->SetFillColor(235, 235, 235);
		
		// Titulo
		$pdf->SetFont('Arial', 'B', 18);
		$pdf->Cell(190, 4, utf8_decode('ASESORES SIN ACTIVIDAD'), 0, 0, 'C', false);		


		$pdf->SetFont('Arial', 'B', 6);
		$pdf->ln(8);
		$pdf->setX(65);
		$pdf->Cell(25, 4, utf8_decode('Fec Desde: '.$data['dates']['fromDate']), 0, 0, 'L', false);	
		$pdf->setX(115);
		$pdf->Cell(25, 4, utf8_decode('Fec Hasta: '.$data['dates']['toDate']), 0, 0, 'R', false);	
		$pdf->ln(4);

		// Tabla del resumen
		$pdf->ln(3);
		$pdf->setX(20);
		$pdf->setY(30);
		
		// Tabla principal
		$pdf->ln(15);
		$pdf->setX(10);
		// $pdf->SetFont('Arial', 'B', 10);
		// $pdf->Image(storage_path() . '/app/public/gradient_bar.png', $pdf->getX(), $pdf->getY(), 196, 8);
		$pdf->Cell(70, 4, utf8_decode('Asesor'), 'B', 0, 'C', true);
		$pdf->Cell(40, 4, utf8_decode('Fecha Ult Actividad'), 'B', 0, 'C', true);
		$pdf->Cell(80, 4, utf8_decode('Actividad'), 'B', 0, 'C', true);

		foreach ($data['users'] as $key => $user) {
			$pdf->ln(6);	
			$pdf->setX(10);
			$fullNameContact = $user['contact']['identification'] ." - ". $user['contact']['name']." ".$user['contact']['surname'];

			$pdf->Cell(70, 4, utf8_decode( $fullNameContact ), 0, 0, 'L', false);

			if ($user['lastActivity']['date']) {
				$nameContact = '';

				if ($user['lastActivity']['contact'] !== null) {
					$nameContact = $user['lastActivity']['contact']['name'];
				}

				$pdf->Cell(40, 4, utf8_decode($user['lastActivity']['date']), 0, 0, 'C', false);
				$pdf->Cell(80, 4, utf8_decode($user['lastActivity']['type'].' - '.$nameContact), 0, 0, 'L', false);
			}
		}


		$namePathFile = '/alertemployees';
		$existe_directorio = Storage::exists('/public'.$namePathFile);

        if (!$existe_directorio){
            Storage::makeDirectory('/public'.$namePathFile);
        }

		// $fileName = uniqid().'.pdf';
		// $fileName = 'test.pdf';
		$fileName = 'asesores-sin-actividad-'.date("Y-m-d").'.pdf';
		$ruta = storage_path('/app/public'.$namePathFile.'/').$fileName;
		

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');
		return $namePathFile.'/'.$fileName;
	}


	public function generatePdfColrecambios($pdf, $documentRepository, $document, $show_Values = true)
	{
		$impresion = $documentRepository->printDocument($document);
		$pdf = new PdfColrecambios('p', 'mm', 'letter');
		$pdf->AliasNbPages();
		$pdf->PDFDocument = $impresion;
		if ($impresion["codigo_modelo"] == 267) {
			$pdf->showValues = false;
			$pdf->AddPage();
			$pdf->SetFont('Arial', '', 6);

			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);
			foreach ($impresion['products'] as  $key => $product) {
				$pdf->Row([
					utf8_decode($key),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),
				]);
			}

			$pdf->ln(17);
			$pdf->setX(90);
			$pdf->Cell(40, 4, utf8_decode('Firma recibe'), 'T', 0, 'C', false);
		}
		$pdf->showValues = $show_Values;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 5);
		$pdf->SetAutoPageBreak(true, 70);

		$totalItems = 0;
		$totalQuantity = 0;
		$total_dscto = 0;
		$total_bruto = 0;
		$total_valor_iva = 0;
		$total = 0;
		if ($show_Values) {
			// $pdf->SetWidths([7,20, 58, 9, 21, 20, 21, 22, 18]);
			$pdf->SetWidths([7,25,25,17,53,9,30,30]);
			$pdf->SetAligns(['C', 'L', 'L', 'L','L', 'C', 'C', 'C']);

			foreach ($impresion['products'] as  $key => $product) {
				$totalItems 		+= 		1;
				$totalQuantity 		+= 		$product['quantity'];
				$total_dscto 		+=  	$product['discount_value'];
				$total_bruto 		+=  	$product['total_value_brut'];
				$total_valor_iva	+= 		$product['iva_p'];
				$total				+=		$product['total_value'];
				$line_break 		= strlen($product['description']) >= 55 ? "
				" : ''; 

				if ($impresion['trm']) {
					$pdf->Row([
						utf8_decode($totalItems).$line_break,
						utf8_decode($product['code']).$line_break,
						utf8_decode($product['previous_code_system']).$line_break,
						utf8_decode($product['line']).$line_break,
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.').$line_break,
						number_format($product['unit_value_before_taxes'], 2, ',', '.') . ' USD'.$line_break,
						// number_format($product['discount_value'], 2, ',', '.') . ' USD'.$line_break,
						// number_format($product['total_value_brut'], 2, ',', '.') . ' USD'.$line_break,
						// number_format($product['iva_p'], 2, ',', '.') . ' USD'.$line_break,
						number_format($product['total_value'], 2, ',', '.') . ' USD'.$line_break,
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				} else {
					$pdf->Row([
						utf8_decode($totalItems).$line_break,
						utf8_decode($product['code']).$line_break,
						utf8_decode($product['previous_code_system']).$line_break,
						utf8_decode($product['line']).$line_break,
						utf8_decode($product['description']),
						number_format($product['quantity'], 0, '', '.').$line_break,
						'$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.').$line_break,
						// '$ ' . number_format($product['discount_value'], 0, '', '.').$line_break,
						// '$ ' . number_format($product['total_value_brut'], 2, ',', '.').$line_break,
						// '$ ' . number_format($product['iva_p'], 2, ',', '.').$line_break,
						'$ ' . number_format($product['total_value'], 0, '', '.').$line_break,
					]);
					if (!is_null($impresion["branchoffice_change"])) {
						$pdf->setX(37);
						$pdf->SetFont('Arial', '', 5.5);
						$pdf->Cell(58, 4, utf8_decode($product['bodegadp']), 1, 0, 'LRTB', false);
						$pdf->SetFont('Arial', '', 6);
						$pdf->ln(3);
					}
				}
			}
		} else {
			$pdf->SetWidths([10, 65, 60, 60]);
			$pdf->SetAligns(['L', 'L', 'C']);

			foreach ($impresion['products'] as $product) {
				$totalItems += 1;
				$totalQuantity += $product['quantity'];
				$pdf->Row([
					utf8_decode($totalItems),
					utf8_decode($product['code']),
					utf8_decode($product['description']),
					number_format($product['quantity'], 0, '', '.'),

					// '$ ' . number_format($product['unit_value_before_taxes'], 0, '', '.'),
					// '$ ' . number_format($product['total_value_brut'], 0, '', '.'),
					// '$ ' . number_format($product['iva_p'], 0, '', '.'),
					// '$ ' . number_format($product['total_value'], 0, '', '.'),
				]);
			}
		}

		if (!is_null($impresion["branchoffice_change"])) {
			$pdf->setXY(10, $pdf->getY()-3);
		}
		$pdf->SetFont('Arial', '', 5.5);
		$pdf->Cell(27, 4, utf8_decode('Total Items: '.$totalItems), 1, 0, 'C', false);
		$pdf->SetFont('Arial', '', 6);

		$pdf->setX(10);

		// $pdf->ln(4);
		if ($impresion['trm']) {
			$pdf->Cell(7, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(25, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(25, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(17, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(53, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(30, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(30, 4, utf8_decode('$ ' . number_format($total) . ' USD'), 1, 0, 'C', false);
		} else {
			$pdf->Cell(7, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(25, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(25, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(17, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(53, 4, utf8_decode('Totales...'), 0, 0, 'R', false);
			$pdf->Cell(9, 4, utf8_decode($totalQuantity), 1, 0, 'C', false);
			$pdf->Cell(30, 4, utf8_decode(''), 0, 0, 'C', false);
			$pdf->Cell(30, 4, utf8_decode('$ ' . number_format($total)), 1, 0, 'C', false);
		}

		if ($pdf->getY() >= 172) {
			$pdf->AddPage();
		}

		// $pdf->Text(10, $pdf->GetY() + 8, 'Total Cantidades: ' . $totalQuantity);
		// $pdf->Text(10, $pdf->GetY() + 8, 'Total Items: ' . $totalItems);
		$pdf->Text(10, $pdf->GetY() + 11, 'Total Peso: ' . number_format($impresion['weight'], 0, '', '.'));

		if ($impresion["user_id_solicit_authorization"] != null) {
			$pdf->Text(35, $pdf->GetY() + 5, utf8_decode('Autorización Elaboración: ' . $impresion["user_id_solicit_authorization"]->name . ' ' . $impresion["user_id_solicit_authorization"]->surname . ' ' . $impresion["date_solicit_authorization"]));
		}

		if ($impresion["status_id_authorized"]) {
			$pdf->Text(35, $pdf->GetY() + 8, utf8_decode('Autorización Cartera: ' . $impresion["PortfolioAuthorization"] . ' ' . $impresion["date_authorized"]));
		}


		if ($show_Values) {
			$pdf->ln(10);

			//   totales
			$pdf->SetFillColor(2, 5, 56, 1);
			$pdf->SetTextColor(255, 255, 255);
			$pdf->setX(10);
			if ($impresion['retention_management'] == '3' || $impresion['retention_management'] == '2') {
				if ($impresion["retenciones_total"]["rete_fuente"]["total"] > 0 && $impresion["retenciones_total"]["rete_ica"]["total"] > 0 && $impresion["retenciones_total"]["rete_iva"]["total"] > 0) {
					$pdf->Cell(79, 4, utf8_decode('Retenciones'), 1, 0, 'C', true);
				}
			}

			$pdf->setX(156);
			$pdf->Cell(0, 4, utf8_decode('Total Documento'), 1, 0, 'L', true);
			$pdf->ln(4);

			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetDrawColor(230, 230, 230);
			$pdf->setX(10);
			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_fuente"]["total"]>0 && $impresion["retenciones_total"]["rete_ica"]["total"]>0 && $impresion["retenciones_total"]["rete_iva"]["total"]>0) {
				$pdf->Cell(19.75, 4, utf8_decode('Retención '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Base') , 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Porcentaje'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode('Vr. Retención') , 1, 0, 'C', true);
			}
			}*/
			$setY = $pdf->getY();
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', '', 4);
			$pdf->sety($setY);
			$pdf->Cell(26, 3, utf8_decode('El original de  la  factura  reposa en  poder de  COLRECAMBIOS  S.A.S.  y  constituye un tItulo  valor'), 0, 0, 'L', false);
			$pdf->sety($setY + 3);
			$pdf->Cell(26, 3, utf8_decode('que presta merito  ejecutivo  de  conformidad  con la Ley 1231/08. Esta factura se asimila para  todos'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('sus efectos legales a una letra de cambio, según los artículos 774 del código de comercio  y  5  de la'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('ley  1231/08  y  genera intereses por mora de conformidad con el  articulo 884 del código de comercio.'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('El comprador del bien o beneficiario no podrá alegar falta de presentación o indebida presentación por'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('razón de la persona que  reciba las  mercancias o el  servicio en sus dependencias para efectos de  la'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('aceptación de este título valor  por  lo  cual  cualquier  trabajador que firme la factura, guía  de entrega'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('o remisión, se entiende que tiene facultades para obligar al  comprador  y  por  ende  se  tendrá como'), 0, 0, 'L', false);
			$pdf->sety($pdf->getY() + 3);
			$pdf->Cell(26, 3, utf8_decode('aceptada la factura de venta (articulo 2 Ley 1231/08).'), 0, 0, 'L', false);

			$pdf->sety($pdf->getY() + 5);
			$pdf->SetFont('Arial', 'B', 5);
			$pdf->Cell(26, 3, utf8_decode('Por Favor No practicar Retención en la Fuente **'), 0, 0, 'L', false);
			


			$pdf->SetFont('Arial', '', 5);
			$pdf->sety($setY);
			$pdf->setX(80);
			$pdf->Cell(26, 3, utf8_decode('En constacia subcribo el dia _____ mes_____ año _______ '), 0, 0, 'L', false);


			$pdf->SetFillColor(245, 245, 255);
			$pdf->SetTextColor(0, 0, 0);


			$pdf->SetFont('Arial', 'B', 6);
			$pdf->setY($setY);
			$pdf->setX(156);
			$pdf->Cell(26, 4, utf8_decode('Total Bruto'), 1, 0, 'L', true);

			//$pdf->Cell(0, 4, '$ '.number_format($impresion['subtotal_sin_desc'], 0, '', '.') , 1, 0, 'R', true);
			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_value_brut_docment'], 2, ',', '.'), 1, 0, 'R', true);
			}

			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('Total descuento'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_discounts'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_discounts'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_fuente"]["total"]>0) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('ReteFuente '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_fuente"]["base_value"], 0, '', '.') , 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_fuente"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_fuente"]["total"], 0, '', '.') , 1, 0, 'C', true);
			}
			}*/


			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);
			$pdf->Cell(26, 4, utf8_decode('Total Iva'), 1, 0, 'L', true);
			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_doc'], 2, ',', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(4);

			if ($impresion['iva_obsq'] > 0) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('IVA Obseq.'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['iva_obsq'], 0, '', '.'), 1, 0, 'R', true);
				}


				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_ica"]["total"]>0) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('Reteica '), 1, 0, 'C', true);

				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_ica"]["base_value"], 0, '', '.'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_ica"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_ica"]["total"], 0, '', '.'), 1, 0, 'C', true);
			}
			}*/

			// $pdf->setX(156);
			// $pdf->SetFont('Arial', 'B', 6);

			// $pdf->Cell(26, 4, utf8_decode('Total General'), 1, 0, 'L', true);

			// if ($impresion['trm']) {
			// 	$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			// } else {
			// 	$pdf->Cell(0, 4, '$ ' . number_format($impresion['full_total'], 0, '', '.'), 1, 0, 'R', true);
			// }
			// $pdf->SetFont('Arial', '', 6);
			// $pdf->ln(4);

			/*if ($impresion['retention_management']=='3'|| $impresion['retention_management']=='2' ) {
			if ($impresion["retenciones_total"]["rete_iva"]["total"]) {
				$pdf->setX(10);
				$pdf->Cell(19.75, 4, utf8_decode('Reteiva '), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_iva"]["base_value"], 0, '', '.'), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, utf8_decode($impresion["retenciones_total"]["rete_iva"]["porcentage"]), 1, 0, 'C', true);
				$pdf->Cell(19.75, 4, '$ '.number_format($impresion["retenciones_total"]["rete_iva"]["total"], 0, '', '.'), 1, 0, 'C', true);
			}
			}*/


			//$pdf->Cell(26, 4, utf8_decode('Total Retenciones'), 1, 0, 'L', true);

			//$pdf->Cell(0, 4, '$ '.number_format($impresion['total_retention'], 0, '', '.') , 1, 0, 'R', true);
			//$pdf->SetFont('Arial','', 6);
			//$pdf->ln(4);

			if ($impresion['ret_fue_total'] > 0 && $impresion['ret_fue_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);

				$pdf->Cell(26, 4, utf8_decode('Rete FTE'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_fue_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_iva_total'] > 0 && $impresion['ret_iva_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete IVA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_iva_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}

			if ($impresion['ret_ica_total'] > 0 && $impresion['ret_ica_total'] != null) {
				$pdf->setX(156);
				$pdf->SetFont('Arial', 'B', 6);
				$pdf->Cell(26, 4, utf8_decode('Rete ICA'), 1, 0, 'L', true);

				if ($impresion['trm']) {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 2, '', '.') . ' USD', 1, 0, 'R', true);
				} else {
					$pdf->Cell(0, 4, '$ ' . number_format($impresion['ret_ica_total'], 0, '', '.'), 1, 0, 'R', true);
				}
				$pdf->SetFont('Arial', '', 6);
				$pdf->ln(4);
			}


			// $pdf->setX(156);
			// $pdf->SetFont('Arial', 'B', 6);

			// $pdf->Cell(26, 4, utf8_decode('Flete'), 1, 0, 'L', true);

			// $pdf->Cell(0, 4, ''/*.number_format('', 0, '', '.')  */, 1, 0, 'R', true);
			// $pdf->SetFont('Arial', '', 6);
			// $pdf->ln(4);


			$pdf->setX(90);

			
			$pdf->Cell(40, 4, utf8_decode('Firma y Sello Identificacion'), 'T', 0, 'C', false);
			if ($impresion["codigo_modelo"] == 267) {
				$pdf->setX(110);
				$pdf->Cell(40, 4, utf8_decode('Firma recibe'), 'T', 0, 'C', false);
			}

			$pdf->setX(156);
			$pdf->SetFont('Arial', 'B', 6);

			$pdf->Cell(26, 4, utf8_decode('TOTAL A PAGAR'), 1, 0, 'L', true);

			if ($impresion['trm']) {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 2, ',', '.') . ' USD', 1, 0, 'R', true);
			} else {
				$pdf->Cell(0, 4, '$ ' . number_format($impresion['total_to_pay'], 0, '', '.'), 1, 0, 'R', true);
			}
			$pdf->SetFont('Arial', '', 6);
			$pdf->ln(6);


			$pdf->setFont('Arial', 'B', 6);
			$pdf->setX(80);
			if ($impresion['total_letters']) {
				$pdf->Cell(126, 4, utf8_decode('Monto en letras: ' . $this->valorEnLetras($impresion['total_to_pay'], $impresion["trm"])), 1, 0, 'R', true);
			}
			if ($impresion['operationType_id'] == 5) {
				$pdf->ln(6);
				$pdf->setX(166);
				$pdf->Cell(40, 4, utf8_decode('Medio De Pago : Transferencia'/*.$impresion['medio_pago'] */), 1, 0, 'R', true);
				$pdf->ln(4);
				$pdf->setX(166);
				$pdf->Cell(40, 4, utf8_decode('Forma De Pago : '.$impresion['cupo_credito'] ), 1, 0, 'R', true);

			}
			// if ($impresion['operationType_id'] == 5 && $impresion['handle_payment_methods'] != 2 && !is_null($impresion['handle_payment_methods'])) {
			// 	$pdf->ln(6);

			// 	$pdf->SetFillColor(245, 245, 255);
			// 	$pdf->setX(136);
			// 	$pdf->Cell(70, 4, utf8_decode('Formas De Pago '), 1, 0, 'C', true);
			// 	$pdf->ln(4);
			// 	$pdf->SetWidths([50,20]);
			// 	$pdf->SetAligns(['L', 'C']);
			// 	foreach ($impresion['metodos_pago'] as  $key => $medios_pago) {
			// 		$pdf->setX(136);
			// 		$pdf->Row([
			// 			utf8_decode($medios_pago['metodo_pago']),
			// 			'$ ' . number_format($medios_pago['valor'], 0, '', '.')
			// 		]);
			// 	}

			// 	$pdf->ln(4);
			// 	$pdf->setX(136);
			// 	$pdf->Cell(50, 4, utf8_decode('Cambio: '), 1, 0, 'L', false);
			// 	$pdf->Cell(20, 4, '$ ' . number_format($impresion['cambio'], 0, '', '.'), 1, 0, 'C', false);

			// }

			$pdf->ln(8);
			if ($impresion['handle_payment_methods'] == 1) {

				$pdf->SetFillColor(245, 245, 255);
				$pdf->SetTextColor(255, 255, 255);
				$pdf->SetX(10);
				$pdf->Cell(0, 4, utf8_decode('Medios de pago'), 0, 0, 'C', true);
				$pdf->ln(4);

				$pdf->setX(10);
				$pdf->Cell(64, 4, utf8_decode('Forma d4e Pago '), 1, 0, 'C', true);
				$pdf->Cell(63, 4, utf8_decode('Valor'), 1, 0, 'C', true);
				$pdf->Cell(63, 4, utf8_decode('Fecha Vencimiento'), 1, 0, 'C', true);
				$pdf->ln(4);


				$pdf->SetTextColor(0, 0, 0);
				$pdf->SetFont('Arial', '', 6);
				$pdf->SetWidths([64, 63, 63]);
				$pdf->SetAligns(['L', 'L', 'L']);
				foreach ($impresion['medios_pagos'] as $medios_pagos) {
					$pdf->Row([
						utf8_decode($medios_pagos["name_parameter"]),
						utf8_decode($medios_pagos["value"]),
						utf8_decode(''),
					]);
				}
			}
			$pdf->ln(4);
		}

		$fileName = str_replace("/", "-", $document->vouchertype->name_voucher_type . ' No.' . $impresion['numero'] . '.pdf');
		$ruta = storage_path() . '/app/public/pdf/' . $fileName;

		if (file_exists($ruta)) {
			unlink($ruta) or die("Couldn't delete file");
		}

		$pdf->Output($ruta, 'F');

		return $fileName;
	}
}
