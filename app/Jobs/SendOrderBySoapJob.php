<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\OrdersSyncService;
use App\Entities\Document;
use Illuminate\Support\Facades\Log;

class SendOrderBySoapJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    
    protected $document;
    
    /**
    * Create a new job instance.
    *
    * @return void
    */
    public function __construct($document)
    {        
        $this->document = $document;

        $this->document->documentsInformation = $document->documentsInformation;
    }
    
    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {        
        
        $service = new OrdersSyncService();
        
        $service->buildData($this->document);
        
        $service->sendData();

        Log::info("Order No. ". $this->document->id ." sent successfully");
                
    }
}
