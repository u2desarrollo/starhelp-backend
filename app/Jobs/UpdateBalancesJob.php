<?php

namespace App\Jobs;

use App\Services\BalancesService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateBalancesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $balances_service;
    private $year_month;
    private $document_id;
    private $update;
    public $tries = 10;
    public $user_id;

    /**
     * Create a new job instance.
     *
     * @param $year_month
     * @param null $document_id
     * @param bool $update
     * @param null $user_id
     */
    public function __construct($year_month, $document_id = null, $update = false, $user_id = null)
    {
        $this->balances_service = new BalancesService();

        $this->year_month = $year_month;
        $this->document_id = $document_id;
        $this->update = $update;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->balances_service->updateBalances($this->year_month, $this->document_id, $this->update, $this->user_id);
    }
}
