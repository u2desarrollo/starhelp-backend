<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\CalculatePricesService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class CalculatePricesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $contact_warehouse_id;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($contact_warehouse_id)
    {
       $this->contact_warehouse_id = $contact_warehouse_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $lastDate = DB::table('promotion_prices')->where('contact_warehouse_id', $this->contact_warehouse_id)->max('created_at');

        if($lastDate == null || $lastDate != date('Y-m-d').' 00:00:00'){

            $objCalculatePrices = new CalculatePricesService($this->contact_warehouse_id);            

            $objCalculatePrices->calculatePromotions();
           
        }

    }
}
