<?php

namespace App\Jobs;

use App\ElectronicInvoice\Models\Adjunto;
use App\ElectronicInvoice\Processor;
use App\Entities\DocumentFile;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendElectronicInvoiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $document;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param $document
     */
    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $files = DocumentFile::where('document_id', $this->document->id)->get();

        $processor = new Processor($this->document);
        $adjunto = new Adjunto($this->document);

        foreach ($files as $file){

            $adjunto->setArchivo($file->path);

            $adjunto->setTipo(2);

            $fileNameAsArray = explode('.', $file->name);
            $fileName = '';

            if (count($fileNameAsArray) > 2){
                for ($i = 0; $i < count($fileNameAsArray); $i++){
                    $fileName .= $fileNameAsArray[$i];
                }
            }else{
                $fileName = $fileNameAsArray[0];
            }

            $fileName = preg_replace('([^A-Za-z0-9 ])', '', $fileName);

            $adjunto->setNombre($fileName);

            $adjunto->setFormato($file->extension);

            $adjunto->setEnviar(0);

            $processor->attachPdf($adjunto);

        }

        $file = explode('/', $this->document->invoice_link);

        $name_file = array_pop($file);

        $adjunto->setArchivo('pdf/' . $name_file);

        $adjunto->setTipo(1);

        $adjunto->setNombre('DOCUMENTO ELECTRONICO No ' . $this->document->vouchertype->prefix . '-' . $this->document->consecutive);

        $adjunto->setFormato('pdf');

        $adjunto->setEnviar(1);

        $processor->attachPdf($adjunto);

    }

    public function getNameFile($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

//        // trim
//        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        $textWithOutSpaces = trim($text);

        if (empty($textWithOutSpaces)) {
            return 'Adjunto sin nombre';
        }

        return $text;
    }
}
