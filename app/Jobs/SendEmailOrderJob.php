<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\NotificationOrderEmail;
use Mail;
use App\Entities\Document;
use App\Traits\GeneratePdfTrait;
use App\Services\OrdersSyncService;
use App\Repositories\DocumentRepository;
use App\Entities\Pdf;
use Illuminate\Support\Facades\Log;
use Illuminate\Container\Container;
class SendEmailOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, GeneratePdfTrait;

    protected $document;
    protected $pdf;
    protected $user;
    private $documentRepository;

    /**
    * Create a new job instance.
    *
    * @return void
    */
    public function __construct($document, $user)
    {
        $this->document = $document;
        $this->user = $user;
    }

    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {
        $soapService = new OrdersSyncService();        

        Log::info("Getting order No. ". $this->document->id ." from Siesa");

        $document = Document::withTrashed()->where(['id' => $this->document->id])->first();

        $document_new_id = $soapService->syncSingleOrder($document->toArray());

        if($document_new_id != null){

            Log::info("Generating PDF of the order No. " . $document_new_id);

            $document_new = Document::withTrashed()->where(['id' => $document_new_id])
            ->with(['contact','seller','warehouse','documentsProducts' => function ($query) {
				return $query->with(['product']);
			}])->first();

            $this->documentRepository = new DocumentRepository(new Container);
            $this->pdf = new Pdf();

            $file_name = storage_path() . "/app/public/pdf/" . $this->generatePdfOrder($this->pdf, $this->documentRepository, $document_new);

            Log::info("Pdf generated and saved as " . $file_name);

            $this->callSendEmail($document_new, $file_name);
            
        }

    }

    private function callSendEmail($document_new, $file_name){

        $recipients = [];

        //Validate email warehouse
        /*if ($document_new->warehouse && filter_var($document_new->warehouse->email, FILTER_VALIDATE_EMAIL)){
            $recipients[] = $document_new->warehouse->email; // ------------------------------------------- TODO: Uncomment to go on the air
        }*/

        //Validate email seller
        if ($document_new->seller &&filter_var($document_new->seller->email, FILTER_VALIDATE_EMAIL)){
            $recipients[] = $document_new->seller->email;
        }

        //Validate if exists email user logued
        if(!array_search($this->user->email, $recipients)){
            $recipients[] = $this->user->email;
        }

        Log:info(implode(", ", $recipients));

        if(sizeof($recipients) > 0){

            Log::info("Sending mail with order");

            $email = new NotificationOrderEmail($document_new->toArray(), $file_name);

            Mail::to($recipients)->send($email); // TODO: Change email addresss

            Log::info("Order sent successfully by email");

        }

    }
}
