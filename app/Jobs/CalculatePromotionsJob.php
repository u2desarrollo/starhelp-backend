<?php

namespace App\Jobs;

use App\Repositories\LineRepository;
use App\Repositories\ProductRepository;
use App\Repositories\PromotionRepository;
use App\Services\ProductService;
use Illuminate\Bus\Queueable;
use Illuminate\Container\Container;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CalculatePromotionsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $lineRepository = new LineRepository(new Container());
        $promotionRepository = new PromotionRepository(new Container());
        $productRepository = new ProductRepository(new Container());


        $productService = new ProductService($lineRepository, $promotionRepository, $productRepository);

        $productService->calculatePromotions();
    }
}
