<?php

namespace App\Imports;

use App\Entities\Product;
use App\Entities\Brand;
use App\Entities\Line;
use App\Entities\Subline;
use App\Entities\Categorie;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class ProductsImport implements ToCollection, WithHeadingRow{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows){
        DB::beginTransaction();
        $registros = [];

        foreach ($rows as $key => $row){
            $codigo = (isset($row['codigo']) && strlen(trim($row['codigo'])) > 0 ? trim($row['codigo']) : "");
            $descripcion = (isset($row['descripcion']) && strlen(trim($row['descripcion'])) > 0 ? trim($row['descripcion']) : "");
            $ma = (isset($row['ma']) && strlen(trim($row['ma'])) > 0 ? trim($row['ma']) : null);
            $li = (isset($row['li']) && strlen(trim($row['li'])) > 0 ? trim($row['li']) : null);
            $sl = (isset($row['sl']) && strlen(trim($row['sl'])) > 0 ? trim($row['sl']) : null);
            $subline = (isset($row['sublinea']) && strlen(trim($row['sublinea'])) > 0 ? trim($row['sublinea']) : null);

            $brand_id = 1;
            $line_id  = 1;
            $subline_id  = 1;
            $category_id  = 1;

            if($ma){
                $marca = Brand::where('brand_code', $ma)->first();

                if(!$marca){
                    $marca = Brand::create([
                        'brand_code'  => $ma,
                        'description' => (isset($row['marca']) && strlen(trim($row['marca'])) > 0 ? trim($row['marca']) : ""),
                    ]);

                    if($marca){
                        $brand_id = $marca->id;
                    }

                } else {
                    $brand_id = $marca->id;
                }
            }

            if($li){
                $linea = Line::where('line_code', $li)->first();

                if(!$linea){
                    $linea = Line::create([
                        'subscriber_id'    => 1,
                        'line_code'        => $li,
                        'line_description' => (isset($row['linea']) && strlen(trim($row['linea'])) > 0 ? trim($row['linea']) : ""),
                    ]);

                    if($linea){
                        $line_id = $linea->id;
                    }

                } else {
                    $line_id = $linea->id;
                }

                if($sl){
                    $sublinea = Subline::where('subline_code', $sl)
                    ->where('line_id', $line_id)
                    ->first();

                    if(!$sublinea){
                        $sublinea = Subline::create([
                            'line_id'             => $line_id,
                            'subline_code'        => $sl,
                            'subline_description' => (isset($row['sublinea']) && strlen(trim($row['sublinea'])) > 0 ? trim($row['sublinea']) : ""),
                        ]);

                        if($sublinea){
                            $subline_id = $sublinea->id;
                        }

                    } else {
                        $subline_id = $sublinea->id;
                    }
                }
            }

            if($subline){
                $categoria = Categorie::where('description', $subline)->first();

                if(!$categoria){
                    $categoria = Categorie::create([
                        'categorie_code' => $sl,
                        'description'    => $subline,
                    ]);

                    if($categoria){
                        $category_id = $categoria->id;
                    }

                } else {
                    $category_id = $categoria->id;
                }
            }

            $nuevo = Product::where('code', $codigo)->first();

            $datos = [
                'code'               => $codigo,
                'description'        => $this->slug($descripcion),
                'line_id'            => $line_id,
                'brand_id'           => $brand_id,
                'subline_id'         => $subline_id,
                'category_id'        => $category_id,
                'quantity_available' => rand(pow(10, 2-1), pow(10, 2)-1),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
            ];

            if(!$nuevo){
                $product = Product::create($datos);

                if($product){
                    $registros[count($registros)] = $product->id;
                }

                DB::commit();
            }
        }
    }

    public function slug($text,$strict = false) {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.\s]+~u', '-', $text);

        // trim
        $text = trim($text, '-');
        setlocale(LC_CTYPE, 'en_GB.utf8');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', ' ', $text);
        if (empty($text)) {
            return 'empty_$';
        }
        if ($strict) {
            $text = str_replace(".", " ", $text);
        }
        return mb_strtoupper($text, 'UTF-8');
    }
}
