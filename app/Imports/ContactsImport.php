<?php

namespace App\Imports;

use App\Entities\Contact;
use App\Entities\ContactCustomer;
use App\Entities\City;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class ContactsImport implements ToCollection, WithHeadingRow{
    public function collection(Collection $rows){
        DB::beginTransaction();
        $registros = [];

        foreach ($rows as $key => $row){
            $identification_type = (isset($row['d']) && strlen(trim($row['d'])) > 0 ? 1 : 2);
            $identification      = (isset($row['codigo']) && strlen(trim($row['codigo'])) > 0 ? trim($row['codigo']) : "");
            $check_digit         = (isset($row['d']) && strlen(trim($row['d'])) > 0 ? trim($row['d']) : "");
            $name                = (isset($row['nombre']) && strlen(trim($row['nombre'])) > 0 ? trim($row['nombre']) : "");
            $address             = (isset($row['direccion']) && strlen(trim($row['direccion'])) > 0 ? trim($row['direccion']) : ".");
            $telephone           = (isset($row['telefonos']) && strlen(trim($row['telefonos'])) > 0 ? explode(' ', trim($row['telefonos'])) : []);
            $main_telephone      = (isset($telephone[0]) && strlen(trim($telephone[0])) > 0 ? trim($telephone[0]) : "0");
            $cell_phone          = (isset($telephone[1]) && strlen(trim($telephone[1])) > 0 ? trim($telephone[1]) : "0");
            $city_id             = "148";

            if(isset($row['ciudad'])){
                $ciudad = explode('-', trim($row['ciudad']));

                if(isset($ciudad[0]) && $ciudad[1]){
                    $cities = City::where('city', 'ILIKE', '%' . $ciudad[0] . '%')
                    ->whereHas('department', function ($department) use ($ciudad){
                        $department->where('department', 'ILIKE', '%' . $ciudad[1] . '%');
                    })
                    ->first();

                    $city_id = ($cities ? $cities->id : "148");
                }
            }

            $nuevo = Contact::where('identification', $identification)->first();

            $datos = [
                'subscriber_id'       => 1,
                'identification_type' => $identification_type,
                'identification'      => $identification,
                'check_digit'         => $check_digit,
                'name'                => $this->slug($name),
                'surname'             => ".",
                'address'             => $this->slug($address),
                'main_telephone'      => $main_telephone,
                'cell_phone'          => $cell_phone,
                'city_id'             => $city_id,
                'is_customer'         => 1,
                'is_provider'         => 0,
                'is_employee'         => 0,
                'class_person'        => 8,
                'taxpayer_type'       => 13,
                'tax_regime'          => 11,
                'declarant'           => 0,
                'self_retainer'       => 0,
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s'),
            ];

            if(!$nuevo){
                $contact = Contact::create($datos);

                if($contact){
                    $registros[count($registros)] = $contact->id;

                    $datos_cliente = [
                        'category_id' => 18,
                        'blocked'     => 0,
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s'),
                    ];

                    $contact->customer()->create($datos_cliente);
                }

                DB::commit();
            }

        }

        return $registros;
    }

    public function slug($text,$strict = false) {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.\s]+~u', '-', $text);

        // trim
        $text = trim($text, '-');
        setlocale(LC_CTYPE, 'en_GB.utf8');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', ' ', $text);
        if (empty($text)) {
            return 'empty_$';
        }
        if ($strict) {
            $text = str_replace(".", " ", $text);
        }
        return mb_strtoupper($text, 'UTF-8');
    }


}
