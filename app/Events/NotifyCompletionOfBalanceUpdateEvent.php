<?php

namespace App\Events;

use App\Entities\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

/**
 * Class NotifyCompletionOfBalanceUpdateEvent
 *
 * @package App\Events
 */
class NotifyCompletionOfBalanceUpdateEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $success;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param int $user_id
     * @param $success
     */
    public function __construct($user_id, $success)
    {
        $this->user_id = $user_id;
        $this->success = $success;

        $this->message = "La actualización masiva de saldos a finalizado exitosamente";

        if (!$success) {
            $this->message = "La actualización masiva de saldos a fallado. Por favor intentelo nuevamente";
        }
    }

    /**
     * @return Channel|Channel[]|string[]
     */
    public function broadcastOn()
    {
        return ['update_balances_user_id_' . $this->user_id];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'updated_balances';
    }
}
