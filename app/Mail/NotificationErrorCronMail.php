<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationErrorCronMail extends Mailable
{
    use Queueable, SerializesModels;

    public $text;
    public $title;

    /**
     * Create a new message instance.
     *
     * @param $text
     * @param $title
     */
    public function __construct($text, $title)
    {
        $this->text = $text;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->title)->view('emails.notification_error_cron');
    }
}
