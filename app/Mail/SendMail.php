<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->data['body'] = str_replace("\n", '<br/>', $this->data['body']);
    }

    /**
     * @return SendMail
     */
    public function build()
    {
        $view = $this->view('emails.send-mail')
            ->from($this->data['from'], $this->data['nameFrom'])
            ->cc($this->data['from'], $this->data['nameFrom'])
            ->subject($this->data['subject']);

        if (isset($this->data['filename']) && $this->data['filename'] != '') {
            if (strpos($this->data['filename'], 'Cartera')) {
                $view->attach(storage_path('app/public/') . $this->data['filename']);
            } else if (strpos($this->data['filename'], 'cuadrecaja')) {
                $view->attach(storage_path('app/public/') . $this->data['filename']); 
            } else {
                $view->attach(storage_path('app/public') . $this->data['filename']);
            }

        }

        return $view;
    }
}
