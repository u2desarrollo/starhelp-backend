<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuroraMail extends Mailable
{
    use Queueable, SerializesModels;

    public $messageBody;
    public $subject;
    public $fileName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($messageBody, $subject, $fileName)
    {
        $this->messageBody = $messageBody;
        $this->subject = $subject;
        $this->fileName = $fileName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->view('emails.aurora.send_mail')->subject($this->subject);

        if ($this->fileName) {
            $view->attach(storage_path('app/public').$this->fileName);
        }

        return $view;
    }
}
