<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RetainAuthorizedDocumentsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $documents;
    public $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($documents, $contact)
    {
        $this->documents = $documents;
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.aurora.retain_authorized_documents')
        ->subject('Retención de Pedidos autorizados por Cartera');
    }
}
