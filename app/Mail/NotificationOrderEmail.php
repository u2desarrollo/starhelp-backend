<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationOrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $file_name;
    public $document;
    public $path;

    /**
     * Create a new message instance.
     *
     * @param $document
     * @param $file_name
     */
    public function __construct($document, $file_name)
    {
        $this->file_name = $file_name;
        $this->document = $document;
        $this->path =  'https://colsaisa.starcommerce.co';

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send_order')
            ->from('emails@starcommerce.co', 'Colsaisa - ERP Starcommerce')
            ->subject('Orden de venta - '.$this->document['consecutive'].' Colsaisa')
            ->attach(storage_path('app/public/pdf/') . $this->file_name);
    }
}
