<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailCashReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $file_name;
    public $cash_receipt;
    public $path;
    public $head;
    public $prefix;

    /**
     * Create a new message instance.
     *
     * @param $cash_receipt
     * @param $file_name
     */
    public function __construct($cash_receipt, $file_name)
    {
        $this->file_name = $file_name;
        $this->cash_receipt = $cash_receipt;
        $this->path =  'https://colsaisa.starcommerce.co';
        $this->prefix =explode(' ',$this->cash_receipt['id']);
        $this->head='Recibo de caja '. $this->prefix[0] ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
        return $this->view('emails.send_cash_receipts')
            ->from('emails@starcommerce.co', 'Colsaisa - ERP Starcommerce')
            ->subject( $this->head)
            ->attach(storage_path('app/public/pdf/') . $this->file_name);
    }
}
