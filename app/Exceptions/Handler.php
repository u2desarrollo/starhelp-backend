<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();
            if ($preException instanceof TokenExpiredException) {
                return response()->json(['status'=>'error', 'message' => 'TOKEN_EXPIRED'], 401);
            } else if ($preException instanceof TokenInvalidException) {
                return response()->json(['status'=> 'error', 'message' => 'TOKEN_INVALID'], 401);
            } else if ($preException instanceof TokenBlacklistedException) {
                return response()->json(['status'=>'error', 'message' => 'TOKEN_BLACKLISTED'], 401);
            }
            if ($exception->getMessage() === 'Token not provided') {
                return response()->json([ 'status'=> 'error', 'message' => 'TOKEN_NOT_PROVIDED'], 401);
            }

        }
       // return response()->json([  'error' => 'Se presentó un error al procesar la petición.'], 500);
        return parent::render($request, $exception);
    }
}
