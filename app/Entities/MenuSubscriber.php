<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MenuSubscriber
 * @package App\Entities
 * @version March 6, 2019, 2:01 pm UTC
 *
 * @property \App\Entities\Menu menu
 * @property \App\Entities\Module module
 * @property \App\Entities\Subscriber subscriber
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer module_id
 * @property integer order
 * @property integer menu_id
 * @property integer menu_subscriber_id
 * @property string alternate_description
 * @property integer subscriber_id
 */
class MenuSubscriber extends Model
{

    public $table = 'menu';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'module_id',
        'order',
        'menu_id',
        'menu_subscriber_id',
        'alternate_description',
        'subscriber_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'module_id' => 'integer',
        'order' => 'integer',
        'menu_id' => 'integer',
        'menu_subscriber_id' => 'integer',
        'alternate_description' => 'string',
        'subscriber_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function menu()
    {
        return $this->belongsTo(\App\Entities\Menu::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function module()
    {
        return $this->belongsTo(\App\Entities\Module::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(\App\Entities\Subscriber::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function secondLevel()
    {
        return $this->hasMany(\App\Entities\MenuSubscriber::class, 'menu_subscriber_id')
            ->with('thirdLevel')
            ->with('menu')
            ->orderBy('order', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function thirdLevel()
    {
        return $this->hasMany(\App\Entities\MenuSubscriber::class, 'menu_subscriber_id')
            ->with('menu')
            ->orderBy('order', 'ASC');
    }




}
