<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use SoftDeletes;

    public $table = 'discounts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'parameter',
        'line_id',
        'brand_id',
        'percentage'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function line()
    {
        return $this->belongsTo(\App\Entities\Line::class)->select(['id', 'line_description', 'line_code', 'service', 'inventory_update']);
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function brand()
    {
        return $this->belongsTo(\App\Entities\Brand::class)/* ->select(['id', 'image', 'description']) */;
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'parameter');
    }
}
