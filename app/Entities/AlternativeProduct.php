<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\Product;

class AlternativeProduct extends Model
{
	public $table = 'alternative_products';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'product_id',
		'alias'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'product_id' => 'integer',
		'alias' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	public function product() {
		return $this->belongsTo(Product::class, 'product_alternative_id');
	}
}