<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ContactEmployee
 * @package App\Entities
 * @version January 15, 2019, 9:26 pm UTC
 */
class ContactEmployee extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $touches = ['contact'];

    public $table = 'contacts_employees';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'category_id',
        'cost_center_id',
        'contract_number',
        'percentage_commision',
        'position_id',
        'observation',
        'photo',
        'order_minimum_ammount',
        'seller_zone_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'category_id' => 'integer',
        'cost_center_id' => 'integer',
        'contract_number' => 'string',
        'percentage_commision' => 'float',
        'position_id' => 'integer',
        'observation' => 'string',
        'photo' => 'string'
    ];

    public function getPhotoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/contacts/employees/default.jpg';
        }
        return '/storage/' . $value;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sellerZone()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'seller_zone_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter3()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }
}
