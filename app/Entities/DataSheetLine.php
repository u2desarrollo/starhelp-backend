<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DataSheetLine extends Model
{
    public $table = 'data_sheet_lines';

    public $timestamps = false;

    public $fillable = [
        'line_id',
        'data_sheet_id',
        'required',
        'order'
    ];
}
