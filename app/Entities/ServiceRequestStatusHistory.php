<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequestStatusHistory extends Model
{
    use SoftDeletes;

    public $table = 'service_requests_status_history';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'service_requests_id',
        'service_requests_state_id',
        'user_id',
        'observation',
        'date',
        'document_id',
    ];

    /**
     * 
     * Relacion 1 a 1 con la solicitud de
     * servicio
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function serviceRequest()
    {
        return $this->belongsTo('App\Entities\ServiceRequest', 'service_requests_id');
    }

    /**
     * 
     * Relacion 1 a 1 con el estado de
     * la solicitud del servicio
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function state()
    {
        return $this->belongsTo('App\Entities\ServiceRequestState', 'service_requests_state_id');
    }

    /**
     * 
     * Relacion 1 a 1 con el 
     * documento
     * 
     * @author Kevin Galindo
     */
    public function document()
    {
        return $this->belongsTo('App\Entities\Document', 'document_id');
    }

    /**
     * 
     * Relacion 1 a 1 con el usuario
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }

}
