<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobInteractionDoc extends Model
{
    use SoftDeletes;

	public $table = 'jobs_interactions_docs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'jobs_interactions_id',
        'path_document',
        'name',
        'type'
    ];

    public static $rules = [
        'jobs_interactions_id' => 'required|exists:jobs_interactions,id',
        'path_document'        => 'required|string'
    ];

    // public function getpathDocumentAttribute($value)
    // {
    //     return '/colsaisa/jobstar-interactions/'.$value;
    // }

    /**
     * Get the JobsInteraction that owns the JobsInteractionDoc
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interaction()
    {
        return $this->belongsTo(JobInteraction::class);
    }

}
