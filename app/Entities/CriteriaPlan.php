<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CriteriaPlan
 * @package App\Entities
 * @version April 17, 2019, 8:40 pm UTC
 *
 * @property \App\Entities\CriteriaType criteriaType
 * @property \App\Entities\Paramtable paramtable
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaValues
 * @property \Illuminate\Database\Eloquent\Collection priceListPromotion
 * @property \Illuminate\Database\Eloquent\Collection productPromotion
 * @property \Illuminate\Database\Eloquent\Collection promotionRule
 * @property string code
 * @property string name
 * @property integer param_table_id
 * @property integer criteria_type_id
 */
class CriteriaPlan extends Model
{
    public $table = 'criteria_plans';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'name',
        'param_table_id',
        'criteria_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'name' => 'string',
        'param_table_id' => 'integer',
        'criteria_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function criteriaType()
    {
        return $this->belongsTo(\App\Entities\CriteriaType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paramtable()
    {
        return $this->belongsTo(\App\Entities\Paramtable::class);
    }
}
