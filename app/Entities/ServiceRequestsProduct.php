<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestsProduct extends Model
{

    public $table = 'service_requests_products';

    public $fillable = [
        'service_requests_id',
        'service_requests_state_id',
        'product_id',
        'quantity',
        'unit_value',
    ];

    public function serviceRequest()
    {
        return $this->belongsTo('App\Entities\ServiceRequest', 'service_requests_id');
    }

    public function serviceRequestState()
    {
        return $this->belongsTo('App\Entities\ServiceRequestState', 'service_requests_state_id');
    }

    public function product()
    {
        return $this->belongsTo('\App\Entities\Product');
    }
}
