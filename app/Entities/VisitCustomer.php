<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VisitCustomer
 * @package App\Entities
 * @version June 5, 2019, 5:22 pm UTC
 *
 * @property \App\Entities\Contact contact
 * @property \App\Entities\ContactsWarehouse contactsWarehouse
 * @property \App\Entities\Parameter parameter
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property integer contact_id
 * @property integer contact_warehouse_id
 * @property integer visit_type
 * @property string latitude
 * @property string longitude
 * @property string motive
 * @property boolean test
 * @property integer distance
 * @property string|\Carbon\Carbon checkin_date
 * @property string|\Carbon\Carbon checkout_date
 * @property string observation
 * @property string photo
 */
class VisitCustomer extends Model
{
	public $table = 'visits_customers';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'seller_id',
		'contact_id',
		'contact_warehouse_id',
		'visit_type',
		'latitude_checkin',
		'longitude_checkin',
		'latitude_checkout',
		'longitude_checkout',
		'test',
		'distance',
		'checkin_date',
		'checkout_date',
		'observation',
		'photo',
		'checkout_ok',
		'sales_management',
		'collection',
		'training'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'seller_id' => 'integer',
		'contact_id' => 'integer',
		'contact_warehouse_id' => 'integer',
		'visit_type' => 'integer',
		'latitude_checkin' => 'string',
		'longitude_checkin' => 'string',
		'latitude_checkout' => 'string',
		'longitude_checkout' => 'string',
		'test' => 'boolean',
		'distance' => 'integer',
		'observation' => 'string',
		'photo' => 'string',
		'checkout_ok' => 'string',
		'sales_management' => 'boolean',
		'collection' => 'boolean',
		'training' => 'boolean'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

    protected $dates = [
        'checkin_date',
        'checkout_date'
    ];

    protected $appends = [
       'duration'
    ];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function contact()
	{
		return $this->belongsTo(\App\Entities\Contact::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function contactsWarehouse()
	{
		return $this->belongsTo(ContactWarehouse::class,'contact_warehouse_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function parameter()
	{
		return $this->belongsTo(\App\Entities\Parameter::class);
	}

    public function getDurationAttribute()
    {
        $duration = $this->checkout_date->diffForHumans($this->checkin_date);

        $duration = str_replace('después', '', $duration);
        $duration = str_replace('minuto', 'min', $duration);
        $duration = str_replace('segundo', 'seg', $duration);
        $duration = str_replace('hora', 'hr', $duration);

        return $duration;
	}
}
