<?php

namespace App\Entities;

use Eloquent as Model;

class PaymentWay extends Model
{

    public $table = 'payment_ways';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'document_id',
        'pay_way_id',
        'value',
        'bank',
        'bonus_number',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'document_id'   => 'integer',
        'pay_way_id'    => 'integer',
        'value'         => 'double',
        'bank'          => 'string',
        'bonus_number'  => 'string',
        'user_id'       => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function document()
    {
        return $this->hasMany(\App\Entities\Document::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function parameter()
    {
        return $this->hasMany(\App\Entities\Parameter::class, 'id', 'pay_way_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Entities\User::class);
    }
}
