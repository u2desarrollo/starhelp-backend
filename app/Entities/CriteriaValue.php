<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CriteriaValue
 * @package App\Entities
 * @version April 17, 2019, 8:41 pm UTC
 *
 * @property \App\Entities\CriteriaType criteriaType
 * @property \App\Entities\Parameter parameter
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaMajors
 * @property \Illuminate\Database\Eloquent\Collection priceListPromotion
 * @property \Illuminate\Database\Eloquent\Collection productPromotion
 * @property \Illuminate\Database\Eloquent\Collection promotionRule
 * @property integer relation_id
 * @property integer parameter_id
 * @property integer criteria_type_id
 */
class CriteriaValue extends Model
{
	public $table = 'criteria_values';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'relation_id',
		'parameter_id',
		'criteria_type_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'relation_id' => 'integer',
		'parameter_id' => 'integer',
		'criteria_type_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function criteriaType()
	{
		return $this->belongsTo(\App\Entities\CriteriaType::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function parameter()
	{
		return $this->belongsTo(\App\Entities\Parameter::class);
	}

	/*public function promotionRules(){
		return $this->belongsTo(\App\Entities\PromotionRule::class, 'parameter_id', 'parameters_id');
	}*/
}