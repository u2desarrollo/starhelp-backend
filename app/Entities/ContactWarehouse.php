<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ContactWarehouse
 *
 * @package App\Entities
 */
class ContactWarehouse extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'contacts_warehouses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'contact_id',
        'code',
        'description',
        'address',
        'latitude',
        'length',
        'telephone',
        'telephone2',
        'cellphone',
        'email',
        'observation',
        'state',
        'seller_id',
        'price_list_id',
        'credit_quota',
        'erp_id',
        'pay_condition',
        'send_point',
        'picture',
        'ret_fue_prod_vr_limit',
        'ret_fue_prod_percentage',
        'ret_fue_serv_vr_limit',
        'ret_fue_serv_percentage',
        'ret_iva_prod_vr_limit',
        'ret_iva_prod_percentage',
        'ret_iva_serv_vr_limit',
        'ret_iva_serv_percentage',
        'ret_ica_prod_vr_limit',
        'ret_ica_prod_percentage',
        'ret_ica_serv_vr_limit',
        'ret_ica_serv_percentage',
        'branchoffice_id',
        'creation_date',
        'other_telephones',
        'payment_type_id',
        'discount_percentage',
        'zone_id',
        'zone_despatch_id',
        'city_id',
        'types_negotiation_portfolio',

        'ret_fue_prod_vr_limit_provider',
        'ret_fue_prod_percentage_provider',
        'ret_fue_serv_vr_limit_provider',
        'ret_fue_serv_percentage_provider',
        'ret_iva_prod_vr_limit_provider',
        'ret_iva_prod_percentage_provider',
        'ret_iva_serv_vr_limit_provider',
        'ret_iva_serv_percentage_provider',
        'ret_ica_prod_vr_limit_provider',
        'ret_ica_prod_percentage_provider',
        'ret_ica_serv_vr_limit_provider',
        'ret_ica_serv_percentage_provider',

        'ret_fue_account_id',
        'ret_iva_account_id',
        'ret_ica_account_id',
        'ret_fue_provider_account_id',
        'ret_iva_provider_account_id',
        'ret_ica_provider_account_id',

        'ret_fue_serv_account_id',
        'ret_iva_serv_account_id',
        'ret_ica_serv_account_id',
        'ret_fue_serv_provider_account_id',
        'ret_iva_serv_provider_account_id',
        'ret_ica_serv_provider_account_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'contact_id'    => 'integer',
        'code'          => 'string',
        'description'   => 'string',
        'address'       => 'string',
        'latitude'      => 'string',
        'length'        => 'string',
        'telephone'     => 'string',
        'telephone2'    => 'string',
        'cellphone'     => 'string',
        'email'         => 'string',
        'observation'   => 'string',
        'state'         => 'integer',
        'seller_id'     => 'integer',
        'price_list_id' => 'integer',
        'credit_quota'  => 'double',
        'erp_id'        => 'integer',
        'pay_condition' => 'string',
        'send_point'    => 'string',
        'creation_date'=> 'date',
        'other_telephones'=> 'string',
        'payment_type_id'=> 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesCreate = [
        'contact_id' => 'required|integer',
        'code' => 'required|string|max:5',
        'description' => 'required|string|max:150'

    ];

    public static $rulesUpdate = [
        'code' => 'required|string|max:5',
        'description' => 'required|string|max:150'
    ];

    protected $appends = [
        'description_with_code'
    ];

    public function getDescriptionWithCodeAttribute()
    {
        return $this->code . ' - ' . $this->description;
    }

    public function setCreationDateAttribute($creationDate)
    {
        $this->attributes['creation_date'] = substr($creationDate, 0, 10);
    }

    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class)->select('id', 'name', 'surname', 'cell_phone', 'main_telephone', 'secondary_telephone', 'identification', 'address', 'tradename');
    }


    public function contactt()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'contact_id');
    }
    public function documents()
    {
        return $this->hasMany(\App\Entities\ContactDocument::class);
    }

    public function documentsApp()
    {
        return $this->hasMany(Document::class, 'warehouse_id');
    }

    public function allDocuments()
    {
        return $this->hasMany(Document::class, 'warehouse_id');
    }

    public function seller()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'seller_id')->select('id', 'name', 'surname', 'identification', 'email', 'main_telephone', 'cell_phone');
    }

    public function line()
    {
        return $this->hasOne(\App\Entities\ContactWarehouseLine::class, 'contact_warehouse_id')->withDefault(new ContactWarehouseLine);
    }

    public function cashReceiptsApps()
    {
        return $this->hasMany(CashReceiptsApp::class);
    }

    public function zone()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'zone_id');
    }

    public function zoneDespatch()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'zone_despatch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Entities\City::class, 'city_id');
    }

    public function branchoffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class, 'branchoffice_id');
    }

    public function paymentType()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function typesNegotiationPortfolio_()
    {
        return $this->belongsTo(Parameter::class, 'types_negotiation_portfolio');
    }

    public function retFueAccount()
    {
        return $this->belongsTo(Account::class, 'ret_fue_account_id');
    }

    public function retIvaAccount()
    {
        return $this->belongsTo(Account::class, 'ret_iva_account_id');
    }

    public function retIcaAccount()
    {
        return $this->belongsTo(Account::class, 'ret_ica_account_id');
    }

    public function retFueProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_fue_provider_account_id');
    }

    public function retIvaProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_iva_provider_account_id');
    }

    public function retIcaProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_ica_provider_account_id');
    }

    public function retFueServAccount()
    {
        return $this->belongsTo(Account::class, 'ret_fue_serv_account_id');
    }

    public function retIcaServAccount()
    {
        return $this->belongsTo(Account::class, 'ret_ica_serv_account_id');
    }

    public function retIvaServAccount()
    {
        return $this->belongsTo(Account::class, 'ret_iva_serv_account_id');
    }

    public function retFueServProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_fue_serv_provider_account_id');
    }

    public function retIcaServProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_ica_serv_provider_account_id');
    }

    public function retIvaServProviderAccount()
    {
        return $this->belongsTo(Account::class, 'ret_iva_serv_provider_account_id');
    }

    public function lastSalesProducts()
    {
        return $this->hasMany(SalesContactWarehousesProduct::class, 'contact_warehouse_id');
    }
}
