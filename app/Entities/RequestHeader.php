<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestHeader
 * @package App\Entities
 * @version September 1, 2019, 3:50 am UTC
 *
 * @property \App\Entities\Contact contact
 * @property \App\Entities\ContactsWarehouse contactsWarehouse
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection requestProducts
 * @property integer contact_id
 * @property integer contacts_warehouse_id
 * @property string request_date
 * @property string request_type
 * @property string maintenance_type
 * @property integer units
 * @property string direc_observation
 * @property string customer_observation
 * @property string provider_observation
 */
class RequestHeader extends Model
{
    use SoftDeletes;

    public $table = 'request_headers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'contacts_warehouse_id',
        'request_date',
        'request_type',
        'maintenance_type',
        'units',
        'direc_observation',
        'customer_observation',
        'provider_observation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'contacts_warehouse_id' => 'integer',
        //'request_date' => 'timestamp',
        'request_type' => 'integer',
        'status_request' => 'integer',
        'maintenance_type' => 'integer',
        'units' => 'integer',
        'direc_observation' => 'string',
        'customer_observation' => 'string',
        'provider_observation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'contact_id' => 'required',
        'contacts_warehouse_id' => 'required',
        'request_date' => 'required',
        'request_type' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contactsWarehouse()
    {
        return $this->belongsTo(\App\Entities\ContactWarehouse::class, 'contacts_warehouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function requestProducts()
    {
        return $this->hasMany(\App\Entities\RequestProduct::class);
    }
}
