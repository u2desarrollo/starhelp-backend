<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BannerB2b
 * @package App\Entities
 * @version March 21, 2019, 2:15 pm UTC
 *
 * @property \App\Entities\Line line
 * @property \App\Entities\Parameter parameter
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer line_id
 * @property integer parameter_id
 * @property string big_banner
 * @property string small_banner
 * @property boolean state
 */
class BannerB2b extends Model
{
	use SoftDeletes;

	public $table = 'banners_b2b';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'line_id',
		'parameter_id',
		'name',
		'big_banner',
		'small_banner',
		'since',
		'until',
		'state'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'line_id' => 'integer',
		'parameter_id' => 'integer',
		'name' => 'string',
		'big_banner' => 'string',
		'small_banner' => 'string',
		'since' => 'datetime:Y-m-d',
		'until' => 'datetime:Y-m-d',
		'state' => 'integer'
	];

    public function getBigBannerAttribute($image) {
        if ($image != '' && $image != null) {
            $base_64 = base64_encode(\Storage::disk('public')->get('/' . $image));
            return 'data:image/png;base64,' . $base_64;
        }
    }


    public function getSmallBannerAttribute($image) {
        if ($image != '' && $image != null) {
            $base_64 = base64_encode(\Storage::disk('public')->get('/' . $image));
            return 'data:image/png;base64,' . $base_64;
        }
    }

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		// 'line_id' => 'required|integer',
		// 'parameter_id' => 'required|integer',
		// 'name' => 'required|max:255|string',
		// 'since' => 'required|date',
		// 'until' => 'required|date'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function line()
	{
		return $this->belongsTo(\App\Entities\Line::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function parameter()
	{
		return $this->belongsTo(\App\Entities\Parameter::class);
	}
}
