<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactClerk
 * @package App\Entities
 * @version January 15, 2019, 9:28 pm UTC
 *
 * @property \App\Entities\Contact contact
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer contact_id
 * @property string name
 * @property string surname
 * @property string address
 * @property string main_telephone
 * @property string secondary_telephone
 * @property string fax
 * @property string cell_phone
 * @property string email
 */
class ContactClerk extends Model
{
    use SoftDeletes;

    public $table = 'contacts_clerks';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'identification_type',
        'identification',
        'name',
        'surname',
        'gender',
        'birthdate',
        'city_id',
        'address',
        'stratum',
        'main_telephone',
        'secondary_telephone',
        'cell_phone',
        'email',
        'position',
        'observation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'name' => 'string',
        'surname' => 'string',
        'address' => 'string',
        'gender' => 'string',
        'main_telephone' => 'string',
        'secondary_telephone' => 'string',
        'fax' => 'string',
        'cell_phone' => 'string',
        'email' => 'string',
        'city_id' => 'integer',
        'position' => 'string',
        'observation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesCreate = [

        'contact_id' => 'required',
        'name' => 'required|string|max:60',
        'surname' => 'required|string|max:60',


    ];

    public static $rulesUpdate = [
        'name' => 'required|string|max:60',
        'surname' => 'required|string|max:60',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function city()
    {
        return $this->belongsTo(\App\Entities\City::class)->withDefault(new City());
    }
}
