<?php

namespace App\Entities\Util;

use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class HasManySyncable
 * @package App\Models\Relations
 * @author Jhon García
 */
class HasManySyncable extends HasMany
{

    /**
     * @param $data
     * @param bool $deleting
     * @return array[]
     * @author Jhon García
     */
    public function sync($data, $deleting = true)
    {
        $changes = [
            'created' => [], 'deleted' => [], 'updated' => [],
        ];

        $relatedKeyName = $this->related->getKeyName();

        // Obtenemos cualquiera de los modelos asociados que no están actualmente
        // en la tabla de entidad secundaria. Revisaremos las identificaciones dadas, verificando para ver
        // si existen en la matriz de los actuales, y si no, los insertaremos.
        $current = $this->newQuery()->pluck($relatedKeyName)->all();

        // Separe los datos enviados en "actualización" y "nuevo"
        $updateRows = [];
        $newRows = [];
        foreach ($data as $row) {
            // Determinamos las filas "actualizables" como aquellas cuyo $ relatedKeyName (generalmente 'id') está establecido, no está vacío, y
            // coincide con una fila relacionada en la base de datos.
            if (isset($row[$relatedKeyName]) && !empty($row[$relatedKeyName]) && in_array($row[$relatedKeyName], $current)) {
                $id = $row[$relatedKeyName];
                $updateRows[$id] = $row;
            } else {
                $newRows[] = $row;
            }
        }

        // A continuación, determinaremos las filas en la base de datos que no están en la lista de "actualización".
        // Estas filas se programarán para su eliminación. Una vez más, determinamos en función del nombre clave relacionado (generalmente 'id').
        $updateIds = array_keys($updateRows);
        $deleteIds = [];
        foreach ($current as $currentId) {
            if (!in_array($currentId, $updateIds)) {
                $deleteIds[] = $currentId;
            }
        }

        // Eliminar cualquier fila que no coincida
        if ($deleting && count($deleteIds) > 0) {
            $this->getRelated()->destroy($deleteIds);

            $changes['deleted'] = $this->castKeys($deleteIds);
        }

        // Actualiza las filas actualizables
        foreach ($updateRows as $id => $row) {
            $this->getRelated()->where($relatedKeyName, $id)->update($row);
        }

        $changes['updated'] = $this->castKeys($updateIds);

        // Insertar las nuevas filas
        $newIds = [];
        foreach ($newRows as $row) {
            $newModel = $this->create($row);
            $newIds[] = $newModel->$relatedKeyName;
        }

        $changes['created'][] = $this->castKeys($newIds);

        return $changes;
    }


    /**
     * Convierta las claves dadas en enteros si son numéricos y de lo contrario son cadenas.
     *
     * @param array $keys
     * @return array
     */
    protected function castKeys(array $keys)
    {
        return (array)array_map(function ($v) {
            return $this->castKey($v);
        }, $keys);
    }

    /**
     * Transmita la clave dada a un entero si es numérico.
     *
     * @param mixed $key
     * @return mixed
     */
    protected function castKey($key)
    {
        return is_numeric($key) ? (int)$key : (string)$key;
    }

}
