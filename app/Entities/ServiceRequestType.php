<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequestType extends Model
{
    use SoftDeletes;

    public $table = 'service_requests_types';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name'
    ];

    public function states()
    {
        return $this->hasMany('App\Entities\ServiceRequestState', 'service_requests_types_id');
    }
}
