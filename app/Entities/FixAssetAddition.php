<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixAssetAddition extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'fix_asset_additions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'fix_asset_id',
        'plate_number',
        'description',
        'value',
        'date_addition'
    ];

    protected $casts = [
        'value' => 'float'
    ];

    public function fixAsset()
	{
		return $this->belongsTo(\App\Entities\FixAsset::class);
    }
}
