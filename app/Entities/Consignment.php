<?php

namespace App\Entities;

use Eloquent as Model;

class Consignment extends Model
{

    public $table = 'consignment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'bank_id',
        'consignment_number',
        'photo_consignment',
        'user_id',
        'date_time',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                    => 'integer',
        'bank_id'               => 'integer',
        'consignment_number'    => 'string',
        'photo_consignment'     => 'string',
        'user_id'               => 'int',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'iduser' => 'required',
        'banco' => 'required',
        'numeroConsignacion' => 'required',
        'recibosCaja' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function parameters()
    {
        return $this->hasMany(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Entities\User::class);
    }

    public function cashReceiptsApp()
    {

        return $this->hasMany(CashReceiptsApp::class, 'consignment_id');
    }
}
