<?php

namespace App\Entities;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Log;

class PdfColrecambios extends Fpdf
{
    public function Header()
    {
        $document = $this->PDFDocument;
        $showValues = isset($this->showValues) ? $this->showValues : true;
        //LOGO
        $this->SetTextColor(0,0,0);
        // $this->Image(storage_path() . '/app/public/bex.png',15,13,36);
        // $this->Image(storage_path() . '/app/public/colrecambios_logo.png',16,27,32);
        $this->Image(storage_path() . '/app/public/bex.png',15,13,37);
        // $this->Image(storage_path() . '/app/public/bex.png',150,15,30);

        //Código QR

        $image_path = storage_path().'/app/public/qr/'.$document['id'].'.png';

        $qr = $document['qr'];
        \QrCode::size(150)->format('png')->generate(strval($qr), $image_path);
        $this->Image($image_path,180,15,25);

        $a=$this->getY()+4;
        $this->setXY(55,$a);
        $this->SetFont('Arial','B',6);
        // $this->Cell(20, 10, utf8_decode($document['subs_name'].' '.$document['prefijo'].'-'.$document['numero']), 0, 0, 'L');
        // $this->Cell(20, 10, utf8_decode($document['prefijo'].'-'.$document['numero']), 0, 0, 'L');
        $this->setXY(55,$this->getY()+3);
        // $this->Cell(20, 10, utf8_decode($document['subs_identification'].'-'.$document['subs_check_digit']), 0, 0, 'L');


        $this->SetFont('Arial','B',13);
        //BANNER
        $this->setXY(55,10);
        $this->Cell(20, 10, utf8_decode($document['name_document'].'  '.trim($document['prefijo']).'-'.$document['numero']), 0, 0, 'L');
        $this->SetFont('Arial','B');

        //$s = 4;
        if(!empty($document['document_name'])){
            $this->setXY(55,11);
            $this->Cell(20, $a+3, utf8_decode($document['document_name']), 0, 0, 'L');
            $this->SetFont('Arial','B');
            $s = 4;
        }else{
            $s = 0;
        }


        $this->SetFont('Arial','B',8);
        $this->SetFont('Arial','');

        $this->setXY(55,$a+6+$s);
        $this->SetFont('Arial','B');
        $this->Cell(30, 5, utf8_decode('FECHA EXPEDICIÓN: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(21, 5, utf8_decode(' '.$document['fecha_inicio']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(9.5, 5, utf8_decode('HORA: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(15, 5, utf8_decode(' '.$document['fecha_inicio_hora']), 0, 0, 'L');

        $this->ln(6);
        $this->setX(55);
        $this->SetFont('Arial','B');
        $this->Cell(19, 5, utf8_decode('FEC.VENC:'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(22, 5, utf8_decode($document['fecha_fin']), 0, 0, 'L');



        if (config('app.enable_electronic_billing') && !empty($document['cufe'])) {
            $this->SetTextColor(0, 0, 0);
            $this->SetXY(10, 35);
            $this->Cell(0, 4, utf8_decode('CUFE: ' . $document['cufe']), 0, 0, 'L', false);
            $this->ln(4);
        }

        
        //tabla

        //info emisor
        if ($document['canceled']) {
            $this->setXY(65,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('Documento anulado '), 0, 0, 'R');

            $this->setXY(88,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('fecha anulación'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['canceled_date']), 0, 0, 'L');
            $this->setXY(134,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('usuario anulación '), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['user_canceled']), 0, 0, 'L');
        }

        $this->setX(10);
        $this->SetFont('Arial','B', 7);
        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
        $this->SetFillColor(2, 5, 56,1);
        $this->setY(40);
        $this->Cell(95, 3, utf8_decode('Datos Colrecambios '), 'L', 0, 'L', true);

        $this->setY(44);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode(strtoupper($document['subs_name']).' '), 0, 0, 'R');
        $this->Cell(0, 3, utf8_decode($document['subs_identification'].'-'.$document['subs_check_digit']), 0, 0, 'L');
        $this->SetFont('Arial','', 7);
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Sede '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['sede'] .' - '. $document['bodega']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Actividad Económica '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode('4530'), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);


        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Dirección '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->MultiCell(57, 3, utf8_decode($document['direccion']), 0, 'L', false);
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY());
        $this->Cell(27, 3, utf8_decode('Ciudad '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['city']), 0, 0, '');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Teléfono '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['telefono']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        // $this->setY($this->getY()+3);
        // $this->Cell(27, 3, utf8_decode('Email '), 0, 0, 'R');
        // $this->SetFont('Arial','', 7);
        // $this->Cell(0, 3, utf8_decode($document['email']), 0, 0, 'L');
        // $this->SetFont('Arial','B', 7);

        if ($document['template_contact_type'] != 2) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Vendedor '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['vendedor']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        // $this->setY($this->getY()+3);
        // $this->Cell(27, 3, utf8_decode('Elaboro '), 0, 0, 'R');
        // $this->SetFont('Arial','', 7);
        // $this->Cell(0, 3, utf8_decode($document['elaboro']), 0, 0, 'L');
        // $this->SetFont('Arial','B', 7);

        if ($document["documentoBase"] != null) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Documento Base '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['documentoBase']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }
        if ($document['project'] != null) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Proyecto '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['project']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        if ($document['documento_cruce'] ) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode($document['titulo_cruce'].' '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['documento_cruce']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);

            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Fec Doc Cruce '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['fec_documento_cruce']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        //info Cliente

        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
         $this->SetFillColor(2, 5, 56,1);
        $this->setXY(105,40);


        switch ($document['contact']) {
            case '1':
                    $this->Cell(0, 3, utf8_decode('Datos Cliente '), 0, 0, 'L', true);
                break;
            case '2':
                    $this->Cell(0, 3, utf8_decode('Datos Proveedor '), 0, 0, 'L', true);
                break;
            default:
                    $this->Cell(0, 3, utf8_decode('Datos Cliente '), 0, 0, 'L', true);
                break;
        }

        $this->setXY(105,$this->getY()+4);
        $this->SetTextColor(0,0,0);
        $this->Cell(12, 3, utf8_decode('Nombre '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_nombre']),0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Nit '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_nit']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Sucursal '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_sucursal']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Dirección '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_direccion']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);


        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Teléfono '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_telefono']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Celular '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(70, 3, utf8_decode($document['contacto_celular']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setX(175);
        $this->Cell(12, 3, utf8_decode('Dcto '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_descuento'].'%'), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Email '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_email']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        
        $this->setX(175);
        $this->Cell(12, 3, utf8_decode('Cupo '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(75, 3,number_format($document['contacto_cupo'], 0, '', ','), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Ciudad '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_ciudad'].' / '.$document['contacto_departamento']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setX(175);
        $this->Cell(12, 3, utf8_decode('Plazo '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_plazo']. ' días'), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);


        if ($document['trm']) {
            $this->setXY(105,$this->getY()+3);
            $this->Cell(12, 3, utf8_decode('TRM '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['valor_trm']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        if ($document['template_contact_type'] != 2) {
            $this->setXY(105,$this->getY()+3);
            $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');
        }
        $this->setXY(105,$this->getY()+3);




        // if ($document["documentoBase"] != null) {
        //     // $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');

        //     // $this->ln(4);
        // }else{
        //     // $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');

        //     // $this->ln(4);
        // }


        $this->ln(4);
        $this->SetFillColor(2, 5, 56,1);
		$this->SetTextColor(255,255,255);
        $this->SetX(10);
        $this->Cell(0, 4, utf8_decode($document['observacion_title']), 0, 0, 'L',true);
		$this->ln(4);

		$this->setX(10);
		$this->SetTextColor(0,0,0);
        $this->MultiCell(0,4,utf8_decode($document['observacion1']),'BLR','L');
        //$this->MultiCell(0,4,utf8_decode($document['observacion2']),'BLR','L');

        if ($document["tituloDevol"] != null) {
			$this->ln(4);
			$this->SetFillColor(2, 5, 56,1);
			$this->SetTextColor(255,255,255);
			$this->SetX(10);
			$this->Cell(0, 4, utf8_decode($document['tituloDevol']), 0, 0, 'L',true);
			$this->ln(5);
			$this->setX(10);
			$this->SetTextColor(0,0,0);
			$this->Cell(0, 4, utf8_decode($document['razonDevol']), 'BLR', 0, 'L',false);
			// $pdf->MultiCell(0,4,utf8_decode($document['observacion1']),'BLR','L');
		}


        $this->ln(6);
        $this->SetTextColor(255,255,255);
        $this->Image($document['gradient'], $this->getX(), $this->getY(), 196, 8);
        $this->SetFont('Arial','B', 6);

        if ($showValues) {
            $this->Cell(7, 8, utf8_decode('Ítem'), 0, 0, 'C');
            $this->Cell(25, 8, utf8_decode('Referencia'), 0, 0, 'C');
            $this->Cell(25, 8, utf8_decode('Ref. Alterna'), 0, 0, 'C');
            $this->Cell(17, 8, utf8_decode('Linea'), 0, 0, 'C');
            $this->Cell(53, 8, utf8_decode('Descripción'), 0, 0, 'C');
            $this->Cell(9, 8, utf8_decode('Cantidad'), 0, 0, 'C');
            $this->Cell(30, 8, utf8_decode('Valor Unit'), 0, 0, 'C');
            $this->Cell(30, 8, utf8_decode('Valor Total'), 0, 0, 'C');
        }else{
            $this->Cell(10, 8, utf8_decode('Ítem'), 0, 0, 'C');
            $this->Cell(65, 8, utf8_decode('Código'), 0, 0, 'C');
            $this->Cell(60, 8, utf8_decode('Descripción'), 0, 0, 'C');
            $this->Cell(60, 8, utf8_decode('Cantidad'), 0, 0, 'C');
        }


        $this->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);

        $this->ln(8);
    }

    public function Footer()
    {
        $document = $this->PDFDocument;



        $this->SetFillColor(255, 255, 255);
        $this->SetXY(10,-55);
		$this->SetTextColor(76,76,76);
        $this->SetFont('Arial','B');
        $this->MultiCell(0, 4, utf8_decode(
        'Con el recibo de la presente factura declaro que he verificado y constatado que los productos adquiridos cumplen con losr equisitos de etiquetado exigidos por la Resolución 4983 de 2011 del Mintc. A partir de la adquisidón de los productos que dio lugar a la expedición de esta factura, asum o la total responsabilidad respecto al mantenimiento de las etiquetas en perfecto estado durante el tiempo de la comercializadón de los productos.'), 0, 'C', false);
        $this->SetFont('Arial','B');

        $this->MultiCell(0, 4, utf8_decode('""SOMDOS CONT RIBUYENTES DEL IMPUESTO DE INDUSTRIA Y COMERCIO EN EL MUNICIPIO DE COTA POR FAVOR ABSTENERSE DE PRACTICAR RETENCÓN DE ICA SI SE ENCUENTRA EN OTRO MUNICIPIO""'), 0, 'C', false);
        $this->SetFont('Arial','');

        $this->MultiCell(0, 4, utf8_decode('SI ESTA FACTCURA NO SE CANCELA ANTES DE SU VENCIMIENTO PERDERÁ EL DESCUENTO PARA EL PAGO. POR FAVOR GIRAR CHEQUE ANOMBRE DE COLRECAMBIOS S.AS. CON CRUCE AL PRIMER BENEFICIARIO. CONSIGNACIONES EN NUESTRAS CUENTAS cORRIENTES:'), 0, 'C', false);
        $this->SetFont('Arial','B');
        $this->MultiCell(0, 4, utf8_decode('- BANCOLOMBIA RECAUDO NACIONAL: 033-294 496-48
        - BANCO DE BOGOTA RECAUDO NACIONAL: 920 063 484'), 0, 'C', false);
        $this->SetFont('Arial','');

        $this->SetFillColor(255, 255, 255);

        if ($document['operationType_id'] == 5 || $document['operationType_id'] == 1) {
            $this->MultiCell(0, 4, utf8_decode('Proveedor Tecnológico Facturación Electrónica: The FactoryHKAColombia SAS- NIT: 900390126-6'), 0, 'C', false);
        }

        $this->SetX(60);
        $this->SetFont('Arial','B');
        $this->Cell(23, 5, utf8_decode('FECHA GENERACIÓN: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(14, 5, utf8_decode(' '.$document['fecha_inicio']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(6.5, 5, utf8_decode('HORA: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(8, 5, utf8_decode(' '.$document['fecha_inicio_hora']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(20, 5, utf8_decode('MODELO: '.$document['modelo']), 0, 0, 'L');
        $this->ln(4);

        if ($document['operation_type_code']==205) {
            $this->setX(60);
            $this->SetFont('Arial','B');
            $this->Cell(20, 4, utf8_decode('N°.Resolución:'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['resolucion']), 0, 0, 'L');

            $this->setX(100);
            $this->SetFont('Arial','B');
            $this->Cell(7, 4, utf8_decode('FECHA:'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(0, 4, utf8_decode($document['fecha_inicio']), 0, 0, 'L');

            $this->setX(120);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('Hasta: '), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['fecha_fin']), 0, 0, 'L');

            // $this->setXY(124,$a+14+$s);
            // $this->setXY(120, -54);
            // $this->SetFont('Arial','B');
            // $this->Cell(8, 4, utf8_decode('Id: '), 0, 0, 'R');
            // $this->SetFont('Arial','');
            // $this->Cell(20, 4, utf8_decode($document['id_']), 0, 0, 'L');
        }

        $this->ln(3);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(0, 3, utf8_decode(' NO SE RECIBEN PAGOS EN EFECTIVO'), 0, 0, 'C', false);
        $this->SetFont('Arial', 'B', 5);


    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        }
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            // $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->SetDrawColor(230, 230, 230);
            $this->SetFillColor(245, 245, 255);
            $this->MultiCell($w, 5, $data[$i], 'T', $a, true);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}
