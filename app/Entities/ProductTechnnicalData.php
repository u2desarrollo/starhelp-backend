<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductTechnnicalData
 * @package App\Entities
 * @version March 26, 2019, 5:56 pm UTC
 *
 * @property \App\Entities\Product product
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer product_id
 * @property string link_manufacturer_one
 * @property string link_manufacturer_two
 * @property string weight
 * @property string volume
 * @property string size
 * @property string color
 * @property string year_model
 * @property string series
 * @property string link_data_sheet
 * @property string products_technical_datacol
 * @property string link_video
 * @property integer generic
 * @property integer used
 * @property string invima_registry
 * @property date expiration_date_invima
 * @property string width
 * @property string profile
 * @property string rin
 * @property string load_index
 * @property string velocity_index
 * @property string utqg
 * @property integer runflat
 * @property integer lt
 * @property string code_cum
 * @property string code_secretary_of_health
 * @property string aplication_unit
 * @property string number_aplications
 * @property string pharmacological_indication_code
 * @property string concentration
 * @property string pharmaceutical_form
 * @property string form_unit
 * @property integer number_of_doses
 * @property string ium_code
 */
class ProductTechnnicalData extends Model
{
    use SoftDeletes;

    public $table = 'products_technnical_data';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'link_manufacturer_one',
        'link_manufacturer_two',
        'weight',
        'volume',
        'size',
        'color',
        'year_model',
        'series',
        'link_data_sheet',
        'products_technical_datacol',
        'link_video',
        'generic',
        'used',
        'invima_registry',
        'expiration_date_invima',
        'width',
        'profile',
        'rin',
        'load_index',
        'velocity_index',
        'utqg',
        'runflat',
        'lt',
        'code_cum',
        'code_secretary_of_health',
        'aplication_unit',
        'number_aplications',
        'pharmacological_indication_code',
        'concentration',
        'pharmaceutical_form',
        'form_unit',
        'number_of_doses',
        'ium_code',
        'data_sheet'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'link_manufacturer_one' => 'string',
        'link_manufacturer_two' => 'string',
        'weight' => 'string',
        'volume' => 'string',
        'size' => 'string',
        'color' => 'string',
        'year_model' => 'string',
        'series' => 'string',
        'link_data_sheet' => 'string',
        'products_technical_datacol' => 'string',
        'link_video' => 'string',
        'generic' => 'integer',
        'used' => 'integer',
        'invima_registry' => 'string',
        'expiration_date_invima' => 'datetime:Y-m-d',
        'width' => 'string',
        'profile' => 'string',
        'rin' => 'string',
        'load_index' => 'string',
        'velocity_index' => 'string',
        'utqg' => 'string',
        'runflat' => 'integer',
        'lt' => 'integer',
        'code_cum' => 'string',
        'code_secretary_of_health' => 'string',
        'aplication_unit' => 'string',
        'number_aplications' => 'string',
        'pharmacological_indication_code' => 'string',
        'concentration' => 'string',
        'pharmaceutical_form' => 'string',
        'form_unit' => 'string',
        'number_of_doses' => 'integer',
        'ium_code' => 'string',
        'data_sheet' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Entities\Product::class);
    }
}
