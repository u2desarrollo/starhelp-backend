<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethod
 * @package App\Entities
 * @version August 15, 2019, 7:21 pm UTC
 *
 * @property \App\Entities\Contact contact
 * @property \App\Entities\Paramtable paramtable
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property string code
 * @property string description
 * @property integer paramtable_id
 * @property integer contact_id
 * @property string accounting_account
 * @property integer commission_percentage
 * @property string authorization_number
 * @property float check_number
 * @property date expiration_date
 */
class PaymentMethod extends Model
{
    use SoftDeletes;

    public $table = 'payment_methods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'description',
        'parameter_id',
        'contact_id',
        'accounting_account',
        'commission_percentage',
        'authorization_number',
        'check_number',
        'expiration_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'description' => 'string',
        'parameter_id' => 'integer',
        'contact_id' => 'integer',
        'accounting_account' => 'string',
        'commission_percentage' => 'integer',
        'authorization_number' => 'string',
        'check_number' => 'string',
        'expiration_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }
}
