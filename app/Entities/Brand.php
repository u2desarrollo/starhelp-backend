<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Brand
 *
 * @package App\Entities
 */
class Brand extends Model
{
    use SoftDeletes;

    public $table = 'brands';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'brand_code',
        'description',
        'image',
        'provider_id',
        'maker_id',
        'lock_buy',
        'block_sale',
        'thumbnails',
        'deploy_sales_force_app',
        'deploy_b2c',
        'deploy_b2b',
        'percentage_utility'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                     => 'integer',
        'brand_code'             => 'string',
        'description'            => 'string',
        'image'                  => 'string',
        'provider_id'            => 'integer',
        'maker_id'               => 'integer',
        'lock_buy'               => 'integer',
        'block_sale'             => 'integer',
        'thumbnails'             => 'string',
        'deploy_sales_force_app' => 'integer',
        'deploy_b2c'             => 'integer',
        'deploy_b2b'             => 'integer',
    ];

    public function getImageAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/brands/default.png';
        }
        return '/storage/' . $value;
    }

    public function getThumbnailsAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/brands/thumbnails/default.png';
        }
        return '/storage/brands/thumbnails/' . $value;
    }

    /**
     * @return BelongsTo
     **/
    public function contactsProvider()
    {
        return $this->belongsTo(ContactProvider::class);
    }

    public function contactsMaker()
    {
        return $this->belongsTo(ContactProvider::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id', 'id')->select(['id'])->with(['lines']);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class, 'brand_id');
    }

}
