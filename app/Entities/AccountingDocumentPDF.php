<?php

namespace App\Entities;

use Codedge\Fpdf\Fpdf\Fpdf;

class AccountingDocumentPDF extends Fpdf
{
    public function Header()
    {
        $document = $this->PDFDocument;
        $this->SetFillColor(244, 245, 247);
        $this->SetDrawColor(230, 230, 230);

        $this->setXY(10, 19);
        $this->SetFont('Arial', '', 15);
        $this->Cell(120, 4, utf8_decode($document->vouchertype->name_voucher_type), 0, 0, 'R', false);


        $this->setXY(135, 17);
        $this->SetFont('Arial', '', 6);
        $doc = is_null($document->prefix) ? $document->consecutive :  trim($document->prefix) . ' - ' . $document->consecutive;
        $this->Cell(30, 4, utf8_decode('No. Documento '.$doc), 'TR', 0, 'L', false);
        $this->ln(4);
        // $this->Image(storage_path() . '/app/public/logo.png', 165,  5, 40, 10);
        $this->setX(135);
        $this->Cell(30, 4, utf8_decode('Fecha '.$document->document_date), 'R', 0, 'L', false);
        $this->ln(4);
        if (!is_null($document->affects_prefix_document)) {
            $this->setX(135);
            $this->Cell(30, 4, utf8_decode('No Doc Afecta '.trim($document->affects_prefix_document).' '.$document->affects_number_document), 'RB', 0, 'L', false);
            $this->ln(4);
        }
        

        $this->Image(storage_path() . '/app/public/bex.png',15,23,36);
        $this->Image(storage_path() . '/app/public/colrecambios_logo.png',16,37,32);

        $this->ln(4);
        $this->setXY(60, 32);
        $y_header = $this->getY();
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Tercero: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $contact_name = $document->contact->name.' '.$document->contact->surname;
        $contact_name = strlen($contact_name) > 47 ? substr($contact_name, 0, 47).'...': $contact_name;
        $this->Cell(30, 4, utf8_decode($contact_name), 0, 0, 'L', false);
       

        $this->setXY(60, $this->getY()+4);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('NIT: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($document->contact->identification), 0, 0, 'L', false);

        $this->setXY(60, $this->getY()+4);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Direccion: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->MultiCell(57, 4, utf8_decode($document->contact->address), 0, 'L', false);

        $this->setXY(60, $this->getY());
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Telefono: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($document->contact->cell_phone), 0, 0, 'L', false);

        


        $this->setXY(135, $y_header);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(25, 4, utf8_decode($document->contact->subscriber->name.' NIT: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($document->contact->subscriber->identification), 0, 0, 'L', false);

        $this->setXY(135, $this->getY()+4);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Direccion: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->MultiCell(60, 4, utf8_decode($document->branchoffice->address), 0, 'L', false);

        $this->setXY(135, $this->getY());
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Telefono: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($document->branchoffice->telephone), 0, 0, 'L', false);

        $this->setXY(135, $this->getY()+4);
        $this->SetFont('Arial', 'B', 6);
        $template = is_null($document->template) ?  '' : $document->template->code. '-' .$document->template->description;
        $this->Cell(15, 4, utf8_decode('Modelo: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($template), 0, 0, 'L', false);

        $this->setXY(135, $this->getY()+4);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 4, utf8_decode('Tipo-Comp: '), 0, 0, 'L', false);
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, 4, utf8_decode($document->vouchertype->code_voucher_type. '-' .$document->vouchertype->name_voucher_type), 0, 0, 'L', false);


        $this->ln(10);
        $this->SetFont('Arial','', 8);

        $this->SetTextColor(0,0,0);
        $this->SetX(10);
        $this->Cell(0, 4, utf8_decode('Observación'), 0, 0, 'L',true);
        $this->ln(4);

        $this->SetFont('Arial','', 6);
        $this->setX(10);
        $this->SetTextColor(0,0,0);
        $this->MultiCell(0,4,utf8_decode($document->observation),'BLR','L');
        
        
        $this->ln(6);
        

        $columns = $this->validateColumns();

        $columns_witdh = $this->calculateColumns($columns);
        // $this->Cell(7, 6, utf8_decode('Orden'), 0, 0, 'C', true);
        $this->Cell($columns_witdh["Cuenta Contable"], 6, utf8_decode('Cuenta Contable'), 0, 0, 'C', true);
        $this->Cell(26, 6, utf8_decode('No Doc'), 0, 0, 'C', true);
        if ($columns['manage_contact_balances']) {
            $this->Cell($columns_witdh["Tercero"], 6, utf8_decode('Tercero'), 0, 0, 'C', true);
        }
        // $this->Cell(15, 6, utf8_decode('Referencia'), 0, 0, 'C', true);
        // $this->Cell(15, 6, utf8_decode('Concepto'), 0, 0, 'C', true);
        $this->Cell(25, 6, utf8_decode('Valor Debito'), 0, 0, 'C', true);
        $this->Cell(25, 6, utf8_decode('Valor Credito'), 0, 0, 'C', true);


        if ($columns['manage_projects']) {
            $this->Cell(21, 6, utf8_decode('Proyecto'), 0, 0, 'C', true);
        }
        
        if ($columns['cost_center_1']) {
            $this->Cell(21, 6, utf8_decode('Centro de Costo'), 0, 0, 'C', true);
        }
        
        if ($columns['manage_way_to_pay']) {
            $this->Cell(21, 6, utf8_decode('Formas de Pago'), 0, 0, 'C', true);
        }

        $this->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);

    }

    public function Footer()
    {
        
    }

    public function calculateColumns($columns)
    {
        $baseColums = 5;
        $total_columns = $baseColums;
        foreach ($columns as $key => $column) {
            if($column) $total_columns++;
        }

        $w = [
            'Cuenta Contable' => $this->ColumnWidth($total_columns),
            'Tercero' => $this->ColumnWidth($total_columns),
        ];

        return $w;
        
    }

    public function ColumnWidth($total_columns)
    {
        switch ($total_columns) {
            case 6:
                return 60.5;
            break;
            case 7:
                return 53;
            break;
            case 8:
                return 42.5;
            break;
            case 9:
                return 32;
            break;
            default:
                return 74;
            break;
        }
    }

    public function validateColumns()
    {
        $document = $this->PDFDocument;
        $columns =[
            'manage_projects' => false,
            'manage_contact_balances' => false,
            'cost_center_1' => false,
            'manage_way_to_pay' => false
        ];
        foreach ($document->accounting_transactions as $key => $value) {
            if ($value->account->manage_projects) {
                $columns['manage_projects'] = true;
            }
            if ($value->account->manage_contact_balances) {
                $columns['manage_contact_balances'] = true;
            }
            if ($value->account->cost_center_1 != null || $value->account->cost_center_2 != null || $value->account->cost_center_3 != null) {
                $columns['cost_center_1'] = true;
            }
            if ($value->account->manage_way_to_pay || !is_null($value->payment)) {
                $columns['manage_way_to_pay'] = true;
            }
        }
        return $columns;
    }

    function SetDash($black=null, $white=null)
    {
        if($black!==null)
            $s=sprintf('[%.3F %.3F] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        }
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            // $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->SetDrawColor(230, 230, 230);
            $this->SetFillColor(255, 251, 239);
            $this->MultiCell($w, 5, $data[$i], 'RLTB', $a, false);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}