<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Projects extends Model
{
    // use SoftDeletes;
    public $timestamps = true;
	public $table = 'projects';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];

    protected $fillable=[
        // 'name',
        'description',
        'code',
        'contact_id',
        'type_id',
        'status',
        'observation',
    ];

    /**
     * Get the type that owns the Business
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Parameter::class, 'type_id');
    }

    /**
     * Get the contact that owns the Business
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

}
