<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RelatedProduct
 * @package App\Entities
 * @version March 29, 2019, 4:01 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection bannersB2b
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer product_id
 * @property integer product_related_id
 */
class RelatedProduct extends Model
{
    use SoftDeletes;

    public $table = 'related_products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'product_related_id',
        'erp_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'product_related_id' => 'integer',
        'erp_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_related_id', 'product_id');
    }


}
