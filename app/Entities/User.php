<?php

namespace App\Entities;

use App\Entities\Modelo;
use App\Entities\Subscriber;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'session_id',
        'surname',
        'photo',
        'branchoffice_warehouse_id',
        'module_id',
        'branchoffice_id',
        'subscriber_id',
        'contact_id',
        'contact_warehouse_id',
        'clerk_id',
        'type',
        'password_changed',
        'signature',
        'email_cash_receipts_app',
        'username',
        'area_id',
        'company_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'name'           => 'string',
        'email'          => 'string',
        'password'       => 'string',
        'remember_token' => 'string'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $createRules = [
        'name'                      => 'required|max:30',
        'email'                     => 'required|email|unique:users,email',
        'password'                  => 'required',
        'surname'                   => 'nullable|max:30',
        'branchoffice_warehouse_id' => 'required',
        'module_id'                 => 'required',
        'branchoffice_id'           => 'required',
        'subscriber_id'             => 'required'
    ];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setPasswordChanged($value)
    {
        $this->attributes['password_changed'] = true;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getPhotoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/contacts/employees/default.jpg';
        }

        return '/storage' . $value;
    }

    public function getUsernameAttribute($value)
    {
        if (is_null($value) || trim($value) == 'null') {

            $user_name = '';

            if (!is_null($this->name) && trim($this->name) != 'null') {
                $user_name .= trim($this->name);
            }

            if (!is_null($this->surname) && trim($this->surname) != 'null') {
                $user_name .= '_' . trim($this->surname);
            }

            return $this->removeAccents(str_replace(' ', '_', $user_name));
        }

        return $value;
    }

    public static function updateRules($id)
    {
        return [
            'name'                      => 'required|max:30',
            'email'                     => 'required|email|unique:users,email,' . $id, ',id',
            'surname'                   => 'nullable|max:30',
            'branchoffice_warehouse_id' => 'required',
            'module_id'                 => 'required',
            'branchoffice_id'           => 'required',
            'subscriber_id'             => 'required'
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function branchofficeWarehouse()
    {
        return $this->belongsTo(\App\Entities\BranchOfficeWarehouse::class, 'branchoffice_warehouse_id')->with('branchoffice');
    }

    public function module()
    {
        return $this->belongsTo(\App\Entities\Module::class);
    }

    public function subscribers()
    {
        return $this->belongsToMany(\App\Entities\Subscriber::class)->withTimestamps();
    }

    public function subscriber()
    {
        return $this->belongsTo(Subscriber::class);
    }

    public function modules()
    {
        return $this->belongsToMany(\App\Entities\Module::class)->withTimestamps();
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    public function contactWarehouse()
    {
        return $this->belongsTo(ContactWarehouse::class);
    }

    public function clerk()
    {
        return $this->belongsTo(ContactClerk::class);
    }

    public function templates()
    {
        return $this->belongsToMany(Template::class)->withTimestamps();
    }

    // Inicio bloque de relaciones y metodos utilizados para middleware VerificarPermisos

    /**
     * Relación con Permission
     */
    public function getPermissions()
    {
        return $this->belongsToMany(Permission::class); // OJO - No olvidar importar la clase
    }

    /**
     * Relación con Role
     */
    public function getRoles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Retorna un array con los permisos asociados a los roles de un usuario.
     *
     * @return array
     */
    public function getRolesPermissionsUser()
    {
        return $this->getRoles()
            ->getPermissions() // OJO - Esta relación debe existir en el modelo Role (relación de Role con Permission) y debe ser igual a la que tenemos aquí en getPermissions()
            ->pluck('permission')
            ->values()
            ->toArray();
    }

    /**
     * Retorna una colección con los permisos del usuario autenticado.
     * Metodología:
     *  Se deben obtener los permisos asociados a los roles del usuario
     *  Se deben obtener los permisos asociados directamente al usuario
     *  Los dos arrays anteriores se deben fusionar con valores únicos hacia la colección que será retornada
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getUserPermissions()
    {
        $permissionsRoles = $this->getRolesPermissionsUser();
        $permissionsUser = $this->getPermissions()
            ->pluck('permission')
            ->values()
            ->toArray();

        $permissions = array_unique(array_merge($permissionsRoles, $permissionsUser));
        return collect($permissions)->all();
    }

    public function hasRole(string $role)
    {
        $roles = $this->getRoles;

        return $roles->contains('role', $role);
    }

    // Fin bloque de relaciones y metodos utilizados para middleware VerificarPermisos

    private function removeAccents($string)
    {
        //Ahora reemplazamos las letras
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string);

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string);

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string);

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string);

        return $string;
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
