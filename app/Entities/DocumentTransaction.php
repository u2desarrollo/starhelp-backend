<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentTransaction extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [

        'account_code_puc',
        'contact_code',
        'voucher_type',
        'voucher_number',
        'voucher_prefix',
        'voucher_date',
        'document_number',
        'document_prefix',
        'erp_id',
        'quota_number',
        'document_date',
        'term_days_document',
        'expiration_date_document',
        'year_month',
        'detail',
        'description',
        'cost_center',
        'type_bank_document',
        'number_document',
        'Production_order_number',
        'cod_expense_concept',
        'consignment',
        'identifier_1',
        'identifier_2',
        'identifier_3',
        'document_id',
        'transaction_value',
        'transaction_type',
        'base_value',
        'payment_applied_erp',
        'update_or_create',
        'affects_document_id',
        'origin_document'

    ];
    protected $table = 'documents_transactions';


    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * Relación de muchos a uno con Document
     * @return BelongsTo
     */
    public function affectDocument()
    {
        return $this->belongsTo(Document::class, 'affects_document_id');
    }
}
