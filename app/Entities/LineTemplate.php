<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LineTemplate extends Model{
    public $table = 'line_template';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'template_id',
		'line_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'template_id' => 'integer',
		'line_id' => 'integer'
	];

}
