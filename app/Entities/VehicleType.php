<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class VehicleType proporciona la implementación del modelo base para la tabla vehicle_types.
 * @property array $attributes representa una fila en la tabla de la base de datos.
 * @package App\Entities
 * @author Jhon García
 */
class VehicleType extends Model
{
    protected $table = 'vehicle_types';

    protected $fillable = [
        'code',
        'brand_vehicle_id',
        'class_vehicle_id',
        'vehicle_type',
        'weight',
        'service',
        'power',
        'box_type',
        'cylinder_capacity',
        'passenger_capacity',
        'nationality',
        'fuel',
        'doors',
        'transmission'
    ];

    protected $with = [
        'brandVehicle:id,name_parameter',
        'classVehicle:id,name_parameter'
    ];

    /**
     * Relación de muchos a uno entre la tabla de parámetros de Marcas de vehículo (Parameter)
     * @return BelongsTo
     * @author Jhon García
     */
    public function brandVehicle()
    {
        return $this->belongsTo(Parameter::class, 'brand_vehicle_id');
    }

    /**
     * Relación de muchos a uno entre la tabla de parámetros de Clases de vehículo (Parameter)
     * @return BelongsTo
     * @author Jhon García
     */
    public function classVehicle()
    {
        return $this->belongsTo(Parameter::class, 'class_vehicle_id');
    }
}
