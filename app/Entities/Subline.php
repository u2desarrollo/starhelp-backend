<?php

namespace App\Entities;

use App\Entities\Line;
use Eloquent as Model;
//use App\Entities\Promotion;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subline extends Model {
    use SoftDeletes;

    public $table = 'sublines';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'line_id',
        'subline_code',
        'subline_description',
        'short_description',
        'lock_buy',
        'block_sale',
        'margin_cost',
        'calculates_sale_price',
        'inventories_account',
        'account_cost',
        'sales_account',
        'devol_sales_account'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'line_id' => 'integer',
        'subline_code' => 'string',
        'subline_description' => 'string',
        'short_description' => 'string',
        'lock_buy' => 'integer',
        'block_sale' => 'integer',
        'margin_cost' => 'string',
        'calculates_sale_price' => 'integer',
        'inventories_account' => 'integer',
        'account_cost' => 'integer',
        'sales_account' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static function rulesCreate($id) {
        return [
            'subline_code' =>  Rule::unique('sublines')->where(function ($query) use ($id) {
                return $query->where('line_id', $id)
                            ->whereNull('deleted_at');
            }),
            //'subline_code' => 'required|unique:sublines,subline_code,' . $id . ',line_id|max:15',
        ];
    }

    public static function rulesUpdate($id_line, $id){
        return [
            'subline_code' => Rule::unique('sublines')->where(function ($query) use ($id_line, $id) {
                return $query->where('line_id', $id_line)
                ->where('id','<>',$id)
                ->whereNull('deleted_at');
            }),
            // 'subline_code' => 'required|unique:sublines,subline_code,' . $id_line . ',line_id,' . $id . ',id|max:15',
        ];
    }

     // Cambio de string a uppercase
     public function setSublineDescriptionAttribute($value) {
        $this->attributes['subline_description'] = strtoupper($value);
    }

    // Cambio de string a uppercase
    public function setShortDescriptionAttribute($value) {
        $this->attributes['short_description'] = strtoupper($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function line()
    {
        return $this->belongsTo(Line::class, 'line_id');
    }

    public function product()
    {
        return $this->hasMany(\App\Entities\Product::class, 'subline_id');
    }

    public function promotions() {
    	return $this->hasMany(\App\Entities\Promotion::class, 'subline_id');
    }
    
    public function inventorieAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'inventories_account');
    }

    public function costAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'account_cost');
    }
    
    public function saleAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'sales_account');
    }

    public function devolSaleAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'devol_sales_account');
    }
}
