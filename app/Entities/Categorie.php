<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Categorie
 * @package App\Entities
 * @version March 6, 2019, 9:48 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property \Illuminate\Database\Eloquent\Collection Subcategory
 * @property string categorie_code
 * @property string description
 */
class Categorie extends Model
{
    use SoftDeletes;

    public $table = 'categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'categorie_code',
        'description',
        'line_id',
        'subline_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'categorie_code' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subcategories()
    {
        return $this->hasMany(\App\Entities\Subcategory::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}