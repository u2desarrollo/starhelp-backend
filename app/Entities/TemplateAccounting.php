<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateAccounting extends Model
{
    use SoftDeletes;

    public $table = 'template_accounting';
    
    protected $fillable = [
        'model_id',
        'concept_id',
        'according_id',
        'account_id',
        'departure_square',
        'debit_credit',
        'order_accounting',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    **/
    public function concept()
    {
        return $this->belongsTo(Parameter::class, 'concept_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    **/
    public function according()
    {
        return $this->belongsTo(Parameter::class, 'according_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    **/
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    **/
    public function template()
    {
        return $this->belongsTo(Template::class, 'model_id');
    }

}
