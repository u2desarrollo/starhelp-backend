<?php

namespace App\Entities;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Log;

class PdfNegativeFileCol extends Fpdf
{
    public function Header()
    {
        $contact=$this->contact;
        $showValues = isset($this->showValues) ? $this->showValues : true;
        $images=$this->logo_cliente;
        //LOGO
        $this->SetTextColor(0,0,0);
        $this->Image(substr($images->logo,1),15,24,36);

        
/* 
         $a=$this->getY()+8;
         $this->setXY(65,$a);
         $this->SetFont('Arial','B',10);
         $this->Cell(20, 10, utf8_decode('ESTADO DE CUENTA '), 0, 0, 'L');
         $this->setXY(180,$this->getY());
         $this->SetFont('Arial','B',8);
         $this->Cell(20, 9,utf8_decode('FECHA: '.date('Y-m-d').' HORA:'.date('H:i:s')), 0, 0, 'R');
         $this->setXY(65,$this->getY()+6);
         $this->SetFont('Arial','B',10);
         $this->Cell(20, 10,utf8_decode(strtoupper($contact->name) .' '.strtoupper($contact->surname)), 0, 0, 'L');
        


        // //tabla info emisor
        $this->SetFont('Arial','B', 7);
        $this->setXY(55,35);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Señores:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($contact->name .' '.$contact->surname), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);
        

        $this->setXY(55,$this->getY()+3);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Direccion:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($contact->address), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(55,$this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Ciudad:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($contact->city && $contact->city->department?$contact->city->department->department:''.' - '.$contact->city && $contact->city->city?$contact->city->city:''), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);



        $this->SetFont('Arial','B', 7);
        $this->setXY(150,35);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Nit:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($contact->identification), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);
        

        $this->setXY(150,$this->getY()+3);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Telefonos:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($contact->main_telephone), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(150,$this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Moneda:'), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode('PESOS M/CTE'), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);
        $this->SetDrawColor(2,5,56);
        $this->SetLineWidth(0.7);
        $this->Rect(65,$this->getY()-7,141,11);


        //encabezado de mi tabla
        $this->ln(45);
        $this->SetTextColor(255,255,255);
        $this->Image('storage/gradient_bar.png', $this->getX(), $this->getY(), 196, 8);
        $this->SetFont('Arial','B', 10);

        if ($showValues) {
            $this->Cell(35, 8, utf8_decode('Documento'), 0, 0, 'C');
            $this->Cell(35, 8, utf8_decode('Fecha'), 0, 0, 'C');
            $this->Cell(35, 8, utf8_decode('Vence'), 0, 0, 'C');
            $this->Cell(35, 8, utf8_decode('Num Dias'), 0, 0, 'C');
            $this->Cell(22, 8, utf8_decode('Estado'), 0, 0, 'C');
            $this->Cell(35, 8, utf8_decode('Valor'), 0, 0, 'C');
        }


        $this->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);

        $this->ln(8); */


       
       
    }

    public function Footer()
    {
        //$document = $this->PDFDocument;
        
        // $this->SetTextColor(255,255,255);
        // $this->SetFont('Arial', '', 6);
        // $this->SetFillColor(255, 255, 255);
        // $this->SetXY(10,-55);
        // $this->MultiCell(0, 4, utf8_decode('CUENTA CORRIENTE DAVIVIENDA 457969996299 /CUENTA CORRIENTE ITAU 013384524 /CTA CORRIENTE BANCOLOMBIA 69760999972 CUENTA CORRIENTE BANCO DE OCCIDENTE 234874923; BANCO AV VILLAS CUENTA CORRIENTE 394005383(BOGOTA) 370013575(OTRAS CIUDADES) Esta Factura se asimila en todos sus efectos a una letra de cambio de conformidad con los articulos 621,722,773 Y 774 Codigo de Comercio. Actividad Economica 4530. REGIMEN COMUN. NO SOMOS GRANDES CONTRIBUYENTES.TARIFA ICA 11.4xMil'), 0, 'L', false);
        // $this->MultiCell(0, 4, utf8_decode('FACTURACION POR CONTINGENCIA FE'), 0, 'L', false);
		// $this->SetTextColor(76,76,76);
        // $this->MultiCell(0, 4, utf8_decode('A partir del vencimiento se causaran intereses por mora a la tasa maxima legal autorizada. A la presente factura se aplicarán, en lo pertinente las normas relativas a la Letra de Cambio. Autorizo a la entidad COLSAISA SAS o a quien represente sus derechos u ostente en el futuro la calidad de acreedor a reportar,procesar,solicitar o divulgar a cualquier Entidad que manejeo administre bases de datos con la informacion referente a mi comportamiento comercial'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Bogotá:Principal: Cra 124 No.17-80 Fontibon Tel : 7422588'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Medellín: Cra 47 # 87-27 Tel: 3164739135 – 3008646489; Barranquilla: Calle 63A # 38-21 Esquina Tel: 3188480734;'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Bucaramanga: Calle 16 # 15-33 Barrio Mutualidad, Tel: 3153758109; Cali: Cra 6 # 21-23 San Nicolas, Tel: 3163205200; Ibague: Calle 39A # 6-37 Barrio Restrepo, Tel: 3175780408;'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Villavicencio: Cra 29 # 28-37, El Porvenir, Tel: 3167575281; Cucuta: Calle 1 # 4-44, LOCAL 4, Edificio San Judas Tadeo, Tel: 3183870338'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Duitama: Calle 6 # 18 -79 Barrio Cándido Quintero, Tel: 3156977392; Monteria: Calle 21 N 3-34 Centro Parqueadero CicloEnergia, TEL: 3147926023'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode(''), 0, 'C', false);
        // $this->SetFillColor(255, 255, 255);

        // if (config('app.enable_electronic_billing') && $document['vouchertype']['electronic_bill'] && ($document['operationType_id'] == 5 || $document['operationType_id'] == 1)) {
        //     $this->SetTextColor(0, 0, 0);
        //     $this->SetXY(10, -60);
        //     $this->Cell(0, 4, utf8_decode('CUFE: ' . $document['cufe']), 0, 0, 'C', false);
        //     $this->ln(4);
        // }
        // $this->SetFillColor(2, 5, 56,1);
		// $this->SetTextColor(255,255,255);
        // $this->SetXY(10,-33);
        // $this->Cell(0, 4, utf8_decode('Información adicional'), 0, 0, 'C',true);
        // $this->ln(4);
        // $this->SetFillColor(255, 255, 255);
		// $this->SetTextColor(0,0,0);
        // $this->Cell(0, 4, utf8_decode('Pedido: '.$document['numero'].' /Observación: '.$document['observacion1'].'/Dir entrega: /'), 0, 0, 'L',true);
        // $this->ln(4);
        // $this->Cell(0, 4, utf8_decode('Vendedor: '.$document['vendedor'].' /Elaboro: '.$document['elaboro']), 0, 0, 'L',true);
        // $this->ln(9);
        // $this->Cell(25, 4, utf8_decode('Firma resposable:'), 0, 0, 'L',true);
        // $this->setDrawColor(0,0,0);
        // $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');
        // $this->setX(100);
        // $this->Cell(25, 4, utf8_decode('  Recibido por:'), 0, 0, 'L',true);
        // $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        }
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            // $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->SetDrawColor(230, 230, 230);
            $this->SetFillColor(245, 245, 255);
            $this->MultiCell($w, 5, $data[$i], 'RLB', $a, true);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}
