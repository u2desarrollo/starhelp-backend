<?php

namespace App\Entities;
use Codedge\Fpdf\Fpdf\Fpdf;
use function GuzzleHttp\json_encode;

class PdfSale extends Fpdf{
    public function Header() {

        //LOGO
        $this->SetFont('Arial','B',6);
        $this->SetTextColor(0,0,0);
        //$this->Image(public_path().'/images/logopropartesblanco.png',15,6,40);
        
        //BANNER
        $this->setXY(110,2);
        $this->Cell(20, 5, utf8_decode('Factura Electrónica de Venta'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->setXY(110,5);
        $this->SetFont('Arial','B');
        $this->Cell(20, 5, utf8_decode('Nacional'), 0, 0, 'L');
        $this->setXY(110,8);
        $this->SetFont('Arial','B');
        $this->Cell(20, 5, utf8_decode('SIN VALOR FISCAL'), 0, 0, 'L');
        
        $this->setXY(110,11);
        $this->Cell(12, 5, utf8_decode('NUM.DOC.:'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(20, 5, utf8_decode('0000000000'), 0, 0, 'L');
        
        $this->setXY(110,14);
        $this->SetFont('Arial','B');
        $this->Cell(8, 5, utf8_decode('FECHA:'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(20, 5, utf8_decode('0000000000'), 0, 0, 'L');
        
        $this->setXY(110,17);
        $this->SetFont('Arial','B');
        $this->Cell(12, 5, utf8_decode('FEC.VENC:'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(20, 5, utf8_decode('0000000000'), 0, 0, 'L');

        //Código QR
        //$this->Image(public_path().'/images/qr.png',180,0.5,24);
        
        //Barra inferior de Banner
        $this->setXY(80,26);
        $this->SetFont('Arial','B', 5);
        $this->Cell(20, 4, utf8_decode('N°.Resolución:'), 0, 0, 'R');
        $this->SetFont('Arial','');
        $this->Cell(20, 4, utf8_decode('0000000000'), 0, 0, 'L');

        $this->setXY(105,26);
        $this->SetFont('Arial','B', 5);
        $this->Cell(15, 4, utf8_decode('Prefijo:'), 0, 0, 'R');
        $this->SetFont('Arial','');
        $this->Cell(5, 4, utf8_decode('0'), 0, 0, 'L');

        $this->setXY(130,26);
        $this->SetFont('Arial','B', 5);
        $this->Cell(8, 4, utf8_decode('Consecutivo:'), 0, 0, 'R');
        $this->SetFont('Arial','');
        $this->Cell(8, 4, utf8_decode('0000000000'), 0, 0, 'L');

        $this->setXY(155,26);
        $this->SetFont('Arial','B', 5);
        $this->Cell(7, 4, utf8_decode('FECHA:'), 0, 0, 'R');
        $this->SetFont('Arial','');
        $this->Cell(0, 4, utf8_decode('0000000000'), 0, 0, 'L');

        $this->setXY(180,26);
        $this->SetFont('Arial','B', 5);
        $this->Cell(8, 4, utf8_decode('N°.Resolución:'), 0, 0, 'R');
        $this->SetFont('Arial','');
        $this->Cell(20, 4, utf8_decode('0000000000'), 0, 0, 'L');
        
        //tabla

        //info emisor 

        $this->setX(10);
        $this->SetFont('Arial','B', 6);
        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
        $this->SetFillColor(2, 5, 56,1);
        $this->setY(30);
        $this->Cell(95, 3, utf8_decode('Datos Colsaisa: '), 'L', 0, 'L', true);
        $this->setY(33);
        $this->SetTextColor(0,0,0);
        $this->Cell(95, 3, utf8_decode('Razón Social/Nombre: '), 'L', 0, 'L');
        $this->setY(36);
        $this->Cell(95, 3, utf8_decode('Nit: '), 'L', 0, 'L');
        $this->setY(39);
        $this->Cell(95, 3, utf8_decode('Actividad Económica: '), 'L', 0, 'L');
        $this->setY(42);
        $this->Cell(95, 3, utf8_decode('Dirección: '), 'L', 0, 'L');
        $this->setY(45);
        $this->Cell(95, 3, utf8_decode('Teléfono: '), 'L', 0, 'L');
        $this->setY(48);
        $this->Cell(95, 3, utf8_decode('Email: '), 'L', 0, 'L');
        $this->setY(51);
        $this->Cell(95, 3, utf8_decode('Vendedor: '), 'L', 0, 'L');
        $this->setY(54);
        $this->Cell(95, 3, utf8_decode('Elaboro: '), 'LB', 0, 'L');

        //info Cliente 

        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
        $this->SetFillColor(2, 5, 56,1);
        $this->setXY(105,30);
        $this->Cell(95, 3, utf8_decode('Datos Cliente: '), 'RL', 0, 'L', true);
        $this->setXY(105,33);
        $this->SetTextColor(0,0,0);
        $this->Cell(95, 3, utf8_decode('Razón Social/Nombre: '), 'RL', 0, 'L');
        $this->setXY(105,36);
        $this->Cell(95, 3, utf8_decode('Nit: '), 'RL', 0, 'L');
        $this->setXY(105,39);
        $this->Cell(95, 3, utf8_decode('Dirección: '), 'RL', 0, 'L');
        $this->setXY(105,42);
        $this->Cell(95, 3, utf8_decode('Teléfono: '), 'RL', 0, 'L');
        $this->setXY(105,45);
        $this->Cell(95, 3, utf8_decode('Email: '), 'RL', 0, 'L');
        $this->setXY(105,48);
        $this->Cell(95, 3, utf8_decode(''), 'RL', 0, 'L');
        $this->setXY(105,51);
        $this->Cell(95, 3, utf8_decode(''), 'RL', 0, 'L');
        $this->setXY(105,54);
        $this->Cell(95, 3, utf8_decode(''), 'RLB', 0, 'L');

        
        $this->ln(6);
        
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B', 7);
        
        $this->Cell(12, 5, utf8_decode('Código'), 0, 0, 'C',true);
        $this->Cell(56, 5, utf8_decode('Descripción'), 0, 0, 'C',true);
        $this->Cell(23, 5, utf8_decode('Cantidad'), 0, 0, 'C',true);
        $this->Cell(25, 5, utf8_decode('valor Unit.'), 0, 0, 'C',true);
        $this->Cell(25, 5, utf8_decode('valor Bruto.'), 0, 0, 'C',true);
        $this->Cell(25, 5, utf8_decode('valor Iva.'), 0, 0, 'C',true);
        $this->Cell(25, 5, utf8_decode('Vr. Total.'), 0, 0, 'C',true);
        
        $this->Image(public_path().'/images/Logo-B2B-V.png',204,140,6);
        
        $this->ln(8);
        
    }
    
    public function Footer(){
        $this->SetFillColor(2, 5, 56,1);
		$this->SetTextColor(255,255,255);
        $this->SetXY(10,-30);
        $this->Cell(0, 4, utf8_decode('Información adicional'), 0, 0, 'C',true);
        $this->ln(4);
        $this->SetFillColor(255, 255, 255);
		$this->SetTextColor(0,0,0);
        $this->Cell(0, 4, utf8_decode('Pedido: /Observación: /Dir entrega: /'), 0, 0, 'L',true);
        $this->ln(4);
        $this->Cell(0, 4, utf8_decode('Vendedor: /Elaboro: '), 0, 0, 'L',true);
        $this->ln(4);
        $this->Cell(0, 4, utf8_decode(''), 0, 0, 'L',true);
        $this->ln(4);
        $this->Cell(0, 4, utf8_decode(''), 0, 0, 'L',true);
        $this->ln(4);
        $this->Cell(25, 4, utf8_decode('Firma resposable:'), 0, 0, 'L',true);
        $this->setDrawColor(0,0,0);
        $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');
        $this->setX(100);
        $this->Cell(25, 4, utf8_decode('/ Recibido por:'), 0, 0, 'L',true);
        $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');

    }

    function SetWidths($w){
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a){
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data){
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++){
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        }
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++){
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            // $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->SetDrawColor(230, 230, 230);
            $this->SetFillColor(250, 250, 250);
            $this->MultiCell($w,5,$data[$i],'RLB',$a,true);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h){
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger){
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w,$txt){
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb){
            $c=$s[$i];
            if($c=="\n"){
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax){
                if($sep==-1){
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
