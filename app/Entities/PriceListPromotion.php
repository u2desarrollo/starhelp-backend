<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PriceListPromotion
 * @package App\Entities
 * @version April 17, 2019, 8:40 pm UTC
 *
 * @property \App\Entities\Parameter parameter
 * @property \App\Entities\PromotionGroupRule promotionGroupRule
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaMajors
 * @property \Illuminate\Database\Eloquent\Collection criteriaValues
 * @property \Illuminate\Database\Eloquent\Collection productPromotion
 * @property \Illuminate\Database\Eloquent\Collection promotionRule
 * @property integer parameter_id
 * @property integer promotion_group_rule_id
 */
class PriceListPromotion extends Model
{
    // use SoftDeletes;

    public $table = 'price_list_promotion';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];


    public $fillable = [
        'parameter_id',
        'promotion_group_rule_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parameter_id' => 'integer',
        'promotion_group_rule_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function promotionGroupRule()
    {
        return $this->belongsTo(\App\Entities\PromotionGroupRule::class);
    }
}
