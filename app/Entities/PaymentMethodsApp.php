<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodsApp extends Model
{
    protected $fillable=['cash_receipts_app_id','parameter_id','total_paid','bank','check_number'];
    protected $table='payment_methods_app';


    public function cashReceiptsApp()
    {
        return $this->belongsTo(CashReceiptsApp::class);
    }

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }
}
