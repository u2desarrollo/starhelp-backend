<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactLine extends Model
{
	public $table = 'contact_line';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'contact_id',
		'line_id'
	];
}