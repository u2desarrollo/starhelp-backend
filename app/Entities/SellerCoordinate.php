<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SellerCoordinate
 * @package App\Entities
 * @version June 10, 2019, 2:58 pm UTC
 *
 * @property \App\Entities\Contact contact
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property integer seller_id
 * @property string latitude
 * @property string longitude
 * @property string|\Carbon\Carbon datetime
 */
class SellerCoordinate extends Model
{

	public $table = 'seller_coordinates';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'seller_id',
		'latitude',
		'longitude',
		'datetime'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'seller_id' => 'integer',
		'latitude' => 'string',
		'longitude' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

            'iduser' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function contact()
	{
		return $this->belongsTo(\App\Entities\Contact::class);
	}
}
