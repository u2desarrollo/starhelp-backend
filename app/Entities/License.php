<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class License extends Model
{
    use SoftDeletes;

    public $table = 'licenses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected  $dates = ['deleted_at'];

    public $fillable = [
        'license_code',
        'start_date',
        'end_date',
        'trial_license',
        'maximum_users'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'start_date' => 'datetime:Y-m-d',
        'end_date' => 'datetime:Y-m-d',
        'trial_license' => 'integer',
        'maximum_users' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    /* public static $rules = [

    ]; */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscribers()
    {
        return $this->hasMany(\App\Entities\Subscriber::class);
    }
}
