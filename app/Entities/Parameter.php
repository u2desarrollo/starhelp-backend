<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\Document;
use App\entities\DocumentTransaction;
use App\Entities\Template;
use App\Entities\ParamTable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 *
 */
class Parameter extends Model {

    use SoftDeletes;

    public $table = 'parameters';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'paramtable_id',
        'code_parameter',
        'name_parameter',
        'alphanum_data_first',
        'alphanum_data_second',
        'alphanum_data_third',
        'numeric_data_first',
        'numeric_data_second',
        'numeric_data_third',
        'date_data_first',
        'date_data_second',
        'date_data_third',
        'image',
        'line_id',
        'subline_id',
        'city_id',
        'branchoffice_warehouses_id',
        'accounting_account_id'
    ];
    public function getImageAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paramtable_id' => 'integer',
        'code_parameter' => 'string',
        'name_parameter' => 'string',
        'alphanum_data_first' => 'string',
        'alphanum_data_second' => 'string',
        'alphanum_data_third' => 'string',
        'numeric_data_first' => 'float',
        'numeric_data_second' => 'float',
        'numeric_data_third' => 'float',
        'date_data_first' => 'date',
        'date_data_second' => 'date',
        'date_data_third' => 'date',
        'image' => 'string',
        'line_id'=>'integer',
        'subline_id'=>'integer',
        'city_id'=>'integer',
        'branchoffice_warehouses_id'=>'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static function rulesCreate($id) {
        return [
            'code_parameter' =>  Rule::unique('parameters')->where(function ($query) use ($id) {
                return $query->where('paramtable_id', $id)
                            ->whereNull('deleted_at');
            }),
            //'subline_code' => 'required|unique:sublines,subline_code,' . $id . ',line_id|max:15',
        ];
    }

    public static function rulesUpdate($paramtable_id, $id){
        return [
            'code_parameter' => Rule::unique('parameters')->where(function ($query) use ($paramtable_id, $id) {
                return $query->where('paramtable_id', $paramtable_id)
                ->where('id','<>',$id)
                ->whereNull('deleted_at');
            }),
            // 'subline_code' => 'required|unique:sublines,subline_code,' . $id_line . ',line_id,' . $id . ',id|max:15',
        ];
    }


    // Cambio de string a uppercase
    public function setNameParameterAttribute($value) {
        $this->attributes['name_parameter'] = strtoupper($value);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paramtable()
    {
        return $this->belongsTo(ParamTable::class);
    }
    public function documents(){
        return $this->hasMany(Document::class);
    }

    public function documentTransactions(){
        return $this->hasMany(DocumentTransaction::class);
    }
    public function templates(){
        return $this->hasMany(Template::class);
    }

    public function typesOperations()
    {
         return $this->hasMany(TypesOperation::class);
    }

    public function templatesPays()
    {
        return $this->belongsToMany(Template::class);
    }


    public function payMethodsApp()
    {
         return $this->hasMany(PaymentMethodsApp::class);
    }

    public function priceList()
    {
        return $this->hasMany(Parameter::class,'price_list_id');
    }


    public function line()
    {
        return $this->belongsTo(\App\Entities\Line::class, 'line_id');
    }

    public function sub_line()
    {
        return $this->belongsTo(\App\Entities\Subline::class, 'subline_id');
    }

    public function city()
    {
        return $this->belongsTo(\App\Entities\City::class, 'city_id');
    }

    public function branchoffice_warehouse()
    {
        return $this->belongsTo(\App\Entities\BranchOfficeWarehouse::class, 'branchoffice_warehouses_id');
    }

    public function accountingAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'accounting_account_id');
    }

}
