<?php

namespace App\Entities;

use Eloquent as Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Inventory
 * @package App\Entities
 * @version August 1, 2019, 5:19 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property string inventory_cost
 * @property string iva_operation
 * @property string control_existence_outputs
 * @property string sales_price_control
 * @property string control_price_list
 */
class Inventory extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'inventories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'branchoffice_warehouse_id',
        'product_id',
        'stock',
        'stock_values',
        'balance_values',
        'average_cost',
        'available',
        'location',
        'minimum_stock',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'branchoffice_warehouse_id' => 'integer',
        'product_id' => 'integer',
        'stock' => 'integer',
        'balance_values' => 'integer',
        'average_cost' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return mixed
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return mixed
     */
    public function branchofficeWarehouse()
    {
        return $this->belongsTo(BranchOfficeWarehouse::class);
    }


}
