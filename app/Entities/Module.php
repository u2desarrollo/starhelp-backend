<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Module
 * @package App\Entities
 * @version March 6, 2019, 2:00 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property \Illuminate\Database\Eloquent\Collection MenuSubscriber
 * @property \Illuminate\Database\Eloquent\Collection Menu
 * @property string name
 */
class Module extends Model
{

    public $table = 'modules';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getNameAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function menuSubscribers()
    {
        return $this->hasMany(\App\Entities\MenuSubscriber::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function menus()
    {
        return $this->hasMany(\App\Entities\Menu::class);
    }
}
