<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromotionGroupRule
 * @package App\Entities
 * @version April 17, 2019, 8:35 pm UTC
 *
 * @property \App\Entities\Promotion promotion
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaMajors
 * @property \Illuminate\Database\Eloquent\Collection criteriaValues
 * @property \Illuminate\Database\Eloquent\Collection CustomerPromotion
 * @property \Illuminate\Database\Eloquent\Collection PriceListPromotion
 * @property \Illuminate\Database\Eloquent\Collection ProductPromotion
 * @property \Illuminate\Database\Eloquent\Collection PromotionRule
 * @property integer promotion_id
 * @property integer percentage
 * @property integer erp_id
 * @property boolean state
 */
class PromotionGroupRule extends Model
{

    public $table = 'promotion_group_rules';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'promotion_id',
        'percentage',
        'erp_id',
        'state'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'promotion_id' => 'integer',
        'percentage' => 'integer',
        'erp_id' => 'integer',
        'state' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function promotion()
    {
        return $this->belongsTo(\App\Entities\Promotion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function customerPromotions()
    {
        return $this->hasMany(\App\Entities\CustomerPromotion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function priceListPromotions()
    {
        return $this->hasMany(\App\Entities\PriceListPromotion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productPromotions()
    {
        return $this->hasMany(\App\Entities\ProductPromotion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function promotionRules()
    {
        return $this->hasMany(\App\Entities\PromotionRule::class);
    }
}
