<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Line extends BaseModel {
    use SoftDeletes;

    public $table = 'lines';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'subscriber_id',
        'line_code',
        'line_description',
        'short_description',
        'lock_buy',
        'block_sale',
        'margin_cost',
        'calculates_sale_price',
        'inventories_account',
        'account_cost',
        'sales_account',
        'barcode',
        'brand',
        'sub_brand',
        'line',
        'subline',
        'category',
        'subcategory',
        'lot',
        'serial',
        'type',
        'Photos',
        'owner',
        'maker',
        'registration_invima',
        'exp_date_invima',
        'generic',
        'year',
        'conformity_certificate',
        'exp_date_certified',
        'weight',
        'volume',
        'height',
        'color',
        'old_product_code',
        'new_product_code',
        'commission_portion',
        'mg_of_utility',
        'contact_income',
        'reduction_management',
        'high_price',
        'high_risk',
        'refrigerated',
        'type_rotation',
        'handling_formula',
        'width',
        'outline',
        'rin',
        'utqg',
        'load_index',
        'speed_index',
        'cum_code',
        'sgsss_code',
        'application_unit',
        'number_applications',
        'pharmacy_code',
        'concentration',
        'controlled_sale',
        'belongs_to_pos',
        'purchase_present_packaging',
        'purchase_quantity_present',
        'local_purchase',
        'observation_buy',
        'present_sale_packaging',
        'sale_quantity_packaging',
        'observation_of_sale',
        'channel_point_sale',
        'channel_amazon',
        'channel_b2c',
        'channel_b2b',
        'prod_desc_config',
        'tipe',
        'data_sheet',
        'data_sheet_link',
        'video_link',
        'percentage_utility',
        'line_code_wms',
        'arancel_percentage',
        'devol_sales_account'
    ];

    /**
     *
     * Esta variable "$fieldsLine" fue creada con el fin de
     * tener todos los campos que hacen referencia a la ficha del
     * producto para poder filtrarlos de una manera mas adecuada.
     *
     * @author Kevin Galindo
     * @var array
     */
    public static $fieldsLine = [
        'brand' => [
            'value' => '',
            'label' => 'MARCA'
        ],
        'barcode' => [
            'value' => '',
            'label' => 'CODIGO_DE_BARRAS'
        ],
        'sub_brand' => [
            'value' => '',
            'label' => 'SUBMARCA'
        ],
        'line' => [
            'value' => '',
            'label' => 'LINEA'
        ],
        'subline' => [
            'value' => '',
            'label' => 'SUBLINEA'
        ],
        'category' => [
            'value' => '',
            'label' => 'CATEGORIA'
        ],
        'subcategory' => [
            'value' => '',
            'label' => 'SUBCATEGORIA'
        ],
        'lot' => [
            'value' => '',
            'label' => 'LOTE'
        ],
        'serial' => [
            'value' => '',
            'label' => 'SERIAL'
        ],
        'type' => [
            'value' => '',
            'label' => 'TIPO'
        ],
        'Photos' => [
            'value' => '',
            'label' => 'FOTOS'
        ],
        'owner' => [
            'value' => '',
            'label' => 'PROPIETARIO'
        ],
        'maker' => [
            'value' => '',
            'label' => 'FABRICANTE'
        ],
        'registration_invima' => [
            'value' => '',
            'label' => 'REGISTRO_INVIMA'
        ],
        'exp_date_invima' => [
            'value' => '',
            'label' => 'FEC_VENC_INVIMA'
        ],
        'generic' => [
            'value' => '',
            'label' => 'GENERICO'
        ],
        'year' => [
            'value' => '',
            'label' => 'AÑO'
        ],
        'conformity_certificate' => [
            'value' => '',
            'label' => 'CERT_CONFORMIDAD'
        ],
        'exp_date_certified' => [
            'value' => '',
            'label' => 'FEC_VENC_CERTIFICADO'
        ],
        'weight' => [
            'value' => '',
            'label' => 'PESO'
        ],
        'volume' => [
            'value' => '',
            'label' => 'VOLUMEN'
        ],
        'height' => [
            'value' => '',
            'label' => 'TALLA'
        ],
        'color' => [
            'value' => '',
            'label' => 'COLOR'
        ],
        'old_product_code' => [
            'value' => '',
            'label' => 'COD_PROD_ANTIGUO'
        ],
        'new_product_code' => [
            'value' => '',
            'label' => 'COD_PROD_NUEVO'
        ],
        'commission_portion' => [
            'value' => '',
            'label' => 'PORCION_COMISION'
        ],
        'mg_of_utility' => [
            'value' => '',
            'label' => 'MARGEN_UTILIDAD'
        ],
        'contact_income' => [
            'value' => '',
            'label' => 'INGRESO_PARA_TERCERO'
        ],
        'reduction_management' => [
            'value' => '',
            'label' => 'MANEJO_REDUCCION'
        ],
        'high_price' => [
            'value' => '',
            'label' => 'ALTO_COSTO'
        ],
        'high_risk' => [
            'value' => '',
            'label' => 'ALTO_RIESGO'
        ],
        'refrigerated' => [
            'value' => '',
            'label' => 'REFRIGERADO'
        ],
        'type_rotation' => [
            'value' => '',
            'label' => 'TIPO_ROTACION'
        ],
        'handling_formula' => [
            'value' => '',
            'label' => 'MANEJO_FORMULA'
        ],
        'width' => [
            'value' => '',
            'label' => 'ANCHO'
        ],
        'outline' => [
            'value' => '',
            'label' => 'PERFIL'
        ],
        'rin' => [
            'value' => '',
            'label' => 'RIN'
        ],
        'utqg' => [
            'value' => '',
            'label' => 'UTQG'
        ],
        'load_index' => [
            'value' => '',
            'label' => 'INDICE_CARGA'
        ],
        'speed_index' => [
            'value' => '',
            'label' => 'INDICE'
        ],
        'cum_code' => [
            'value' => '',
            'label' => 'CODIGO_CUM'
        ],
        'sgsss_code' => [
            'value' => '',
            'label' => 'CODIGO_SGSSS'
        ],
        'application_unit' => [
            'value' => '',
            'label' => 'UNIDAD_DE_APLICACION'
        ],
        'number_applications' => [
            'value' => '',
            'label' => 'NUMERO_APLICACION'
        ],
        'pharmacy_code' => [
            'value' => '',
            'label' => 'COD_INDICACION_FARMACIA'
        ],
        'concentration' => [
            'value' => '',
            'label' => 'CONCENTRACION'
        ],
        'controlled_sale' => [
            'value' => '',
            'label' => 'VENTA_CONTROLADA'
        ],
        'belongs_to_pos' => [
            'value' => '',
            'label' => 'PERTENECE_A_POS'
        ],
        'data_sheet' => [
            'value' => '',
            'label' => 'FICHA_TECNICA'
        ],
        'data_sheet_link' => [
            'value' => '',
            'label' => 'LINK_FICHA_TECNICA'
        ],
        'video_link' => [
            'value' => '',
            'label' => 'LINK_VIDEO'
        ],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subscriber_id' => 'integer',
        'line_code' => 'string',
        'line_description' => 'string',
        'short_description' => 'string',
        'lock_buy' => 'integer',
        'block_sale' => 'integer',
        'margin_cost' => 'string',
        'calculates_sale_price' => 'integer',
        'inventories_account' => 'integer',
        'account_cost' => 'integer',
        'sales_account' => 'integer',
        'barcode' => 'integer',
        'brand' => 'integer',
        'sub_brand' => 'integer',
        'line' => 'integer',
        'subline' => 'integer',
        'category' => 'integer',
        'subcategory' => 'integer',
        'lot' => 'integer',
        'serial' => 'integer',
        'type' => 'integer',
        'Photos' => 'integer',
        'owner' => 'integer',
        'maker' => 'integer',
        'registration_invima' => 'integer',
        'exp_date_invima' => 'integer',
        'generic' => 'integer',
        'year' => 'integer',
        'conformity_certificate' => 'integer',
        'exp_date_certified' => 'integer',
        'weight' => 'integer',
        'volume' => 'integer',
        'height' => 'integer',
        'color' => 'integer',
        'old_product_code' => 'integer',
        'new_product_code' => 'integer',
        'commission_portion' => 'integer',
        'mg_of_utility' => 'integer',
        'contact_income' => 'integer',
        'reduction_management' => 'integer',
        'high_price' => 'integer',
        'high_risk' => 'integer',
        'refrigerated' => 'integer',
        'type_rotation' => 'integer',
        'handling_formula' => 'integer',
        'width' => 'integer',
        'outline' => 'integer',
        'rin' => 'integer',
        'utqg' => 'integer',
        'load_index' => 'integer',
        'speed_index' => 'integer',
        'cum_code' => 'integer',
        'sgsss_code' => 'integer',
        'application_unit' => 'integer',
        'number_applications' => 'integer',
        'pharmacy_code' => 'integer',
        'concentration' => 'integer',
        'controlled_sale' => 'integer',
        'belongs_to_pos' => 'integer',
        'purchase_present_packaging' => 'integer',
        'purchase_quantity_present' => 'string',
        'local_purchase' => 'integer',
        'observation_buy' => 'integer',
        'present_sale_packaging' => 'integer',
        'sale_quantity_packaging' => 'string',
        'observation_of_sale' => 'integer',
        'channel_point_sale' => 'integer',
        'channel_amazon' => 'integer',
        'channel_b2c' => 'integer',
        'channel_b2b' => 'integer',
        'prod_desc_config' => 'string',
        'tipe' => 'integer',
        'data_sheet' => 'integer',
        'data_sheet_link' => 'integer',
        'video_link' => 'integer',
        'line_code_wms' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesCreate = [
        'line_code' => "required|unique:lines,line_code,NULL, deleted_at,deleted_at,NULL|max:15",
    ];

    // Cambio de string a uppercase
    public function setLineDescriptionAttribute($value) {
        $this->attributes['line_description'] = strtoupper($value);
    }

    // Cambio de string a uppercase
    public function setShortDescriptionAttribute($value) {
        $this->attributes['short_description'] = strtoupper($value);
    }

    /**
     * @return BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(Subscriber::class);
    }

    /**
     * @return HasMany
     **/
    public function sublines()
    {
        return $this->hasMany(Subline::class);
    }

    /**
     * @return BelongsToMany
     */
	public function contacts()
	{
		return $this->belongsToMany(Contact::class)->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function templates()
	{
		return $this->belongsToMany(Template::class)->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function taxes()
    {
        return $this->belongsToMany(Tax::class, 'lines_taxes', 'line_id', 'tax_id')
            ->where('lines_taxes.deleted_at', null);
    }

    /**
     * @return BelongsToMany
     */
    public function dataSheets()
    {
        return $this->belongsToMany('App\Entities\DataSheet', 'data_sheet_lines', 'line_id', 'data_sheet_id')
                    ->withPivot('required')
                    ->withPivot('order')
                    ->withPivot('id');
    }

    /**
     * @return Util\HasManySyncable
     */
    public function promotions() {
    	return $this->hasMany(Promotion::class, 'line_id');
    }

    public function inventorieAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'inventories_account');
    }

    public function costAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'account_cost');
    }

    public function saleAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'sales_account');
    }

    public function devolSaleAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class, 'devol_sales_account');
    }

}
