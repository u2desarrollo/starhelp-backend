<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SubCategorie
 * @package App\Entities
 * @version March 6, 2019, 9:49 pm UTC
 *
 * @property \App\Entities\Category category
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer categorie_id
 * @property string subcategorie_code
 * @property string description
 */
class SubCategorie extends Model
{
    use SoftDeletes;

    public $table = 'subcategories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'categorie_id',
        'subcategorie_code',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'categorie_id' => 'integer',
        'subcategorie_code' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Entities\Category::class);
    }
}
