<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountSoonPayment extends Model
{
    use SoftDeletes;

    public $table = 'discounts_soon_payment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'line_id',
        'subline_id',
        'days_up',
        'percentage',
        'branchoffice_id',
        'brand_id'
    ];

    public function line()
    {
        return $this->belongsTo(\App\Entities\Line::class)->select(['id', 'line_description', 'line_code', 'service', 'inventory_update']);
    }

    public function subline()
    {
        return $this->belongsTo(\App\Entities\Subline::class);
    }

    public function branchoffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class);
    }

    public function brand()
    {
        return $this->belongsTo(\App\Entities\Brand::class);
    }

}
