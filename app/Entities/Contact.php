<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Contact
 *
 * @package App\Entities
 * @version January 15, 2019, 9:25 pm UTC
 */
class Contact extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'contacts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $appends = [
        'full_name_with_identification',
        'full_name',
    ];

    /**
     * Obtener la identificación y el nombre completo
     * @return string Contiene la identificación y el nombre completo
     */
    public function getFullNameWithIdentificationAttribute()
    {
        return $this->identification . ' - ' . $this->name . (empty($this->surname) ? '' : ' ' . $this->surname);
    }

    /**
     * Obtener y el nombre completo
     * @return string Contiene el nombre completo
     */
    public function getFullNameAttribute()
    {
        return $this->name . (empty($this->surname) ? '' : ' ' . $this->surname);
    }

    public $fillable = [
        'subscriber_id',
        'identification_type',
        'identification',
        'check_digit',
        'name',
        'surname',
        'birthdate',
        'gender',
        'city_id',
        'address',
        'stratum',
        'code_ciiu',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'email_electronic_invoice',
        'web_page',
        'latitude',
        'length',
        'is_customer',
        'is_provider',
        'is_employee',
        'is_other',
        'class_person',
        'taxpayer_type',
        'tax_regime',
        'declarant',
        'self_retainer',
        'observations',
        'erp_id',
        'physical_file_number',
        'pay_days',
        'date_opening',
        'tradename',
        'created_at',
        'ERP_code',
        'fiscal_responsibility_id',
        'country_code',
        'direct_invoice_blocking',
        'address_object'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesCreate = [
        'identification'      => 'required|unique:contacts|max:30',
        'identification_type' => 'required',
        'name'                => 'required|max:150',
        'city_id'             => 'required',
        'class_person'        => 'required',
        'taxpayer_type'       => 'required',
        'tax_regime'          => 'required'
    ];

    public static $rulesClientCreate = [
        'identification_type' => 'required|exists:parameters,id',
        'identification'      => 'required|unique:contacts|max:30',
        'check_digit'         => 'numeric',
        'name'                => 'required|max:150',
        'surname'             => 'nullable|max:150',
        'address'             => 'required|max:200',
        'cell_phone'          => 'required|numeric',
        'city_id'             => 'required|exists:cities,id',
        'email'               => 'email|required|max:150',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                       => 'integer',
        'subscriber_id'            => 'integer',
        'identification_type'      => 'integer',
        'identification'           => 'string',
        'check_digit'              => 'string',
        'name'                     => 'string',
        'surname'                  => 'string',
        'birthdate'                => 'datetime:Y-m-d',
        'gender'                   => 'string',
        'city_id'                  => 'integer',
        'address'                  => 'string',
        'stratum'                  => 'string',
        'code_ciiu'                => 'string',
        'main_telephone'           => 'string',
        'secondary_telephone'      => 'string',
        'fax'                      => 'string',
        'cell_phone'               => 'string',
        'email'                    => 'string',
        'email_electronic_invoice' => 'string',
        'web_page'                 => 'string',
        'latitude'                 => 'string',
        'length'                   => 'string',
        'is_customer'              => 'integer',
        'is_provider'              => 'integer',
        'is_employee'              => 'integer',
        'is_other'                 => 'integer',
        'class_person'             => 'integer',
        'taxpayer_type'            => 'integer',
        'tax_regime'               => 'integer',
        'declarant'                => 'integer',
        'self_retainer'            => 'integer',
        'erp_id'                   => 'integer',
        'created_at'               => 'string'
    ];

    public static function rulesUpdate($id)
    {
        return [
            'identification'      => 'required|unique:contacts,identification,' . $id . ',id|max:15',
            'identification_type' => 'required',
            'name'                => 'required|max:150',
            'city_id'             => 'required',
            'class_person'        => 'required',
            'taxpayer_type'       => 'required',
            'tax_regime'          => 'required'
        ];
    }

    public function toSearchableArray()
    {
        $searchable = collect($this->toArray())->only([
            'id',
            'identification',
            'name',
            'surname',
            'code_ciiu',
            'address',
            'main_telephone',
            'secondary_telephone',
            'fax',
            'cell_phone',
            'email',
            'email_electronic_invoice',
            'web_page',
            'is_customer',
            'is_provider',
            'is_employee'
        ])->toArray();

        $searchable['customer']['category_id'] = $this->customer->category_id;
        $searchable['provider']['category_id'] = $this->provider->category_id;
        $searchable['employee']['category_id'] = $this->employee->category_id;

        return $searchable;
    }

    public static function boot()
    {
        parent::boot();
    }

    // public function setEmailElectronicInvoiceAttribute($value)
    // {
    // 	$this->attributes['email_electronic_invoice'] = json_encode($value);
    // }

    // public function getEmailElectronicInvoiceAttribute($value)
    // {
    //     return json_decode($value);
    // }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setSurnameAttribute($value)
    {
        $this->attributes['surname'] = strtoupper($value);
    }

    public function getAddressObjectAttribute($value)
    {
        if (is_null($value)) {
            return [
                'type_of_road'          => '',
                'way_number'            => '',
                'generating_way_number' => '',
                'plate_number'          => '',
                'complement'            => ''
            ];
        }

        return json_decode($value);
    }

    public function getAddressAttribute($value)
    {
        $address = $value;

        if (empty($address)) {
            if (!empty($this->address_object)) {

                $address_object = (array)$this->address_object;

                if ($address_object['type_of_road'] && $address_object['way_number'] && $address_object['generating_way_number'] && $address_object['plate_number']) {
                    $address = $address_object['type_of_road'] . ' ' . $address_object['way_number'] . ' # ' . $address_object['generating_way_number'] . ' - ' . $address_object['plate_number'] . ' ' . (empty($address_object['complement']) ? '' : $address_object['complement']);
                }
            }
        }

        return strtoupper($address);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = strtoupper($value);
    }

    public function setAddressObjectAttribute($value)
    {
        $this->attributes['address_object'] = json_encode($value);
    }

    /**
     * @return BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(City::class)->withDefault(new City());
    }

    /**
     * @return BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return BelongsTo
     **/
    public function parameter1()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(Subscriber::class)->select('id', 'name', 'identification', 'check_digit');
    }

    public function contactOther()
    {
        return $this->hasOne(ContactOther::class)->withDefault(new ContactOther());
    }

    /**
     * @return BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return BelongsTo
     **/
    public function parameter3()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return HasMany
     **/
    public function clerks()
    {
        return $this->hasMany(ContactClerk::class)->with('city');
    }

    /**
     * @return HasMany
     **/
    public function customer()
    {
        return $this->hasOne(ContactCustomer::class, 'contact_id')->withDefault(new ContactCustomer());
    }

    /**
     * @return HasMany
     **/
    public function documents()
    {
        return $this->hasMany(ContactDocument::class);
    }

    public function documentsProducts()
    {
        return $this->hasMany(DocumentProduct::class, 'technician­_id');
    }

    /**
     * @return HasMany
     **/
    public function employee()
    {
        //return $this->hasOne(\App\Entities\ContactEmployee::class);
        return $this->hasOne(\App\Entities\ContactEmployee::class, 'contact_id')->withDefault(new ContactEmployee());
    }

    /**
     * @return HasMany
     **/
    public function provider()
    {

        //puede ser el problem
        return $this->hasOne(\App\Entities\ContactProvider::class, 'contact_id')->withDefault(new ContactProvider());
    }

    /**
     * @return HasMany
     **/
    public function warehouses()
    {
        return $this->hasMany(\App\Entities\ContactWarehouse::class, 'contact_id');
    }

    public function lines()
    {
        return $this->belongsToMany(\App\Entities\Line::class)->withTimestamps();
    }

    public function users()
    {
        return $this->hasMany(\App\Entities\User::class);
    }

    public function cashReceiptsApp()
    {
        return $this->hasMany(CashReceiptsApp::class);
    }

    public function visits()
    {
        return $this->hasMany(VisitCustomer::class, 'seller_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(\App\Entities\Document::class)->where('vouchertype_id', 4);
    }

    public function invoices()
    {
        return $this->hasMany(\App\Entities\Document::class)->where('vouchertype_id', 2);
    }

    public function ordersForSeller()
    {
        return $this->hasMany(\App\Entities\Document::class, 'seller_id')->where('operationType_id', 8);
    }

    public function invoicesForSeller()
    {
        return $this->hasMany(\App\Entities\Document::class, 'seller_id')->where('operationType_id', 5);
    }

    public function allDocuments()
    {
        return $this->hasMany(Document::class);
    }

    public function returnsForSeller()
    {
        return $this->hasMany(\App\Entities\Document::class, 'seller_id')->where('operationType_id', 1);
    }

    public function identificationt()
    {
        return $this->belongsTo(Parameter::class, 'identification_type');
    }

    public function classPersonp()
    {
        return $this->belongsTo(Parameter::class, 'class_person');
    }

    public function taxPayerTypep()
    {
        return $this->belongsTo(Parameter::class, 'taxpayer_type');
    }

    public function taxRegimep()
    {
        return $this->belongsTo(Parameter::class, 'tax_regime');
    }

    public function warehousesSeller()
    {
        return $this->hasMany(ContactWarehouse::class, 'seller_id');
    }
}
