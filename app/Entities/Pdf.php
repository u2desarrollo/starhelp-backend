<?php

namespace App\Entities;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Log;

class Pdf extends Fpdf
{
    public function Header()
    {
        $document = $this->PDFDocument;
        $showValues = isset($this->showValues) ? $this->showValues : true;
        //LOGO
        $this->SetTextColor(0,0,0);
        $this->Image($document['logo'],15,15,36);

        //Código QR

        $image_path = storage_path().'/app/public/qr/'.$document['id'].'.png';

        $qr = $document['qr'];
        \QrCode::size(150)->format('png')->generate(strval($qr), $image_path);
        $this->Image($image_path,180,15,25);

        $a=$this->getY()+4;
        $this->setXY(55,$a);
        $this->SetFont('Arial','B',6);
        // $this->Cell(20, 10, utf8_decode($document['subs_name'].' '.$document['prefijo'].'-'.$document['numero']), 0, 0, 'L');
        // $this->Cell(20, 10, utf8_decode($document['prefijo'].'-'.$document['numero']), 0, 0, 'L');
        $this->setXY(55,$this->getY()+3);
        // $this->Cell(20, 10, utf8_decode($document['subs_identification'].'-'.$document['subs_check_digit']), 0, 0, 'L');


        $this->SetFont('Arial','B',13);
        //BANNER
        $this->setXY(55,10);
        $this->Cell(20, 10, utf8_decode($document['name_document'].'  '.trim($document['prefijo']).'-'.$document['numero']), 0, 0, 'L');
        $this->SetFont('Arial','B');

        //$s = 4;
        if(!empty($document['document_name'])){
            $this->setXY(55,11);
            $this->Cell(20, $a+3, utf8_decode($document['document_name']), 0, 0, 'L');
            $this->SetFont('Arial','B');
            $s = 4;
        }else{
            $s = 0;
        }


        $this->SetFont('Arial','B',6);
        $this->SetFont('Arial','');

        $this->setXY(55,$a+3+$s);
        $this->SetFont('Arial','B');
        $this->Cell(23, 5, utf8_decode('FECHA GENERACIÓN: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(14, 5, utf8_decode(' '.$document['fecha_inicio']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(6.5, 5, utf8_decode('HORA: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(8, 5, utf8_decode(' '.$document['fecha_inicio_hora']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(20, 5, utf8_decode('MODELO: '.$document['modelo']), 0, 0, 'L');

        $this->setXY(55,$a+6+$s);
        $this->SetFont('Arial','B');
        $this->Cell(23, 5, utf8_decode('FECHA EXPEDICIÓN: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(14, 5, utf8_decode(' '.$document['fecha_inicio']), 0, 0, 'L');
        $this->SetFont('Arial','B');
        $this->Cell(6.5, 5, utf8_decode('HORA: '), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(8, 5, utf8_decode(' '.$document['fecha_inicio_hora']), 0, 0, 'L');

        $this->setXY(106,$a+6+$s);
        $this->SetFont('Arial','B');
        $this->Cell(12, 5, utf8_decode('FEC.VENC:'), 0, 0, 'L');
        $this->SetFont('Arial','');
        $this->Cell(15, 5, utf8_decode($document['fecha_fin']), 0, 0, 'L');



        //Barra inferior de Banner
        if ($document['operation_type_code']==205) {
            $this->setXY(52,$a+10+$s);
            $this->SetFont('Arial','B');
            $this->Cell(20, 4, utf8_decode('N°.Resolución:'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['resolucion']), 0, 0, 'L');

            $this->setXY(49,$a+14+$s);
            $this->SetFont('Arial','B');
            $this->Cell(15, 4, utf8_decode('Prefijo:'), 0, 0, 'R');
            $this->SetFont('Arial','', 5);
            $this->Cell(5, 4, utf8_decode(trim($document['prefijo'])), 0, 0, 'L');

            // $this->setXY(130,$a+14);
            // $this->SetFont('Arial','B',);
            // $this->Cell(8, 4, utf8_decode('Rango autorizado:'), 0, 0, 'R');
            // $this->SetFont('Arial','');
            // $this->Cell(8, 4, utf8_decode($document['numero']), 0, 0, 'L');

            $this->setXY(74,$a+14+$s);//25
            $this->SetFont('Arial','B');
            $this->Cell(7, 4, utf8_decode('FECHA:'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(0, 4, utf8_decode($document['fecha_inicio']), 0, 0, 'L');

            $this->setXY(99,$a+14+$s);//25
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('Hasta: '), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['fecha_fin']), 0, 0, 'L');

            $this->setXY(124,$a+14+$s);//25
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('Id: '), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['id_']), 0, 0, 'L');
        }

        //tabla

        //info emisor
        if ($document['canceled']) {
            $this->setXY(65,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('Documento anulado '), 0, 0, 'R');

            $this->setXY(88,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('fecha anulación'), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['canceled_date']), 0, 0, 'L');
            $this->setXY(134,35);
            $this->SetFont('Arial','B');
            $this->Cell(8, 4, utf8_decode('usuario anulación '), 0, 0, 'R');
            $this->SetFont('Arial','');
            $this->Cell(20, 4, utf8_decode($document['user_canceled']), 0, 0, 'L');
        }

        $this->setX(10);
        $this->SetFont('Arial','B', 7);
        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
        $this->SetFillColor(2, 5, 56,1);
        $this->setY(40);
        $this->Cell(95, 3, utf8_decode('Datos Colsaisa '), 'L', 0, 'L', true);

        $this->setY(44);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode(strtoupper($document['subs_name']).' '), 0, 0, 'R');
        $this->Cell(0, 3, utf8_decode($document['subs_identification'].'-'.$document['subs_check_digit']), 0, 0, 'L');
        $this->SetFont('Arial','', 7);
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->SetTextColor(0,0,0);
        $this->Cell(27, 3, utf8_decode('Sede '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['sede'] .' - '. $document['bodega']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Actividad Económica '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode('4530'), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);


        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Dirección '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['direccion']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Ciudad '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['city']), 0, 0, '');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Teléfono '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['telefono']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Email '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['email']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        if ($document['template_contact_type'] != 2) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Vendedor '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['vendedor']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        $this->setY($this->getY()+3);
        $this->Cell(27, 3, utf8_decode('Elaboro '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['elaboro']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        if ($document["documentoBase"] != null) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Documento Base '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['documentoBase']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        if ($document['documento_cruce'] ) {
            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode($document['titulo_cruce'].' '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['documento_cruce']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);

            $this->setY($this->getY()+3);
            $this->Cell(27, 3, utf8_decode('Fec Doc Cruce '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['fec_documento_cruce']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        //info Cliente

        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(230, 230, 230);
         $this->SetFillColor(2, 5, 56,1);
        $this->setXY(105,40);


        switch ($document['contact']) {
            case '1':
                    $this->Cell(0, 3, utf8_decode('Datos Cliente '), 0, 0, 'L', true);
                break;
            case '2':
                    $this->Cell(0, 3, utf8_decode('Datos Proveedor '), 0, 0, 'L', true);
                break;
            default:
                    $this->Cell(0, 3, utf8_decode('Datos Cliente '), 0, 0, 'L', true);
                break;
        }

        $this->setXY(105,$this->getY()+4);
        $this->SetTextColor(0,0,0);
        $this->Cell(12, 3, utf8_decode('Nombre '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_nombre']),0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Nit '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_nit']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Sucursal '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_sucursal']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Dirección '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_direccion']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);


        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Teléfono '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_telefono']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Email '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_email']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        $this->setXY(105,$this->getY()+3);
        $this->Cell(12, 3, utf8_decode('Ciudad '), 0, 0, 'R');
        $this->SetFont('Arial','', 7);
        $this->Cell(0, 3, utf8_decode($document['contacto_ciudad']), 0, 0, 'L');
        $this->SetFont('Arial','B', 7);

        if ($document['trm']) {
            $this->setXY(105,$this->getY()+3);
            $this->Cell(12, 3, utf8_decode('TRM '), 0, 0, 'R');
            $this->SetFont('Arial','', 7);
            $this->Cell(0, 3, utf8_decode($document['valor_trm']), 0, 0, 'L');
            $this->SetFont('Arial','B', 7);
        }

        if ($document['template_contact_type'] != 2) {
            $this->setXY(105,$this->getY()+3);
            $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');
        }
        $this->setXY(105,$this->getY()+3);




        if ($document["documentoBase"] != null) {
            $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');

            $this->ln(9);
        }else{
            $this->Cell(0, 3, utf8_decode(''), 0, 0, 'L');

            $this->ln(6);
        }


        $this->ln(4);
        $this->SetFillColor(2, 5, 56,1);
		$this->SetTextColor(255,255,255);
        $this->SetX(10);
        $this->Cell(0, 4, utf8_decode($document['observacion_title']), 0, 0, 'L',true);
		$this->ln(4);

		$this->setX(10);
		$this->SetTextColor(0,0,0);
        $this->MultiCell(0,4,utf8_decode($document['observacion1']),'BLR','L');
        //$this->MultiCell(0,4,utf8_decode($document['observacion2']),'BLR','L');

        if ($document["tituloDevol"] != null) {
			$this->ln(4);
			$this->SetFillColor(2, 5, 56,1);
			$this->SetTextColor(255,255,255);
			$this->SetX(10);
			$this->Cell(0, 4, utf8_decode($document['tituloDevol']), 0, 0, 'L',true);
			$this->ln(5);
			$this->setX(10);
			$this->SetTextColor(0,0,0);
			$this->Cell(0, 4, utf8_decode($document['razonDevol']), 'BLR', 0, 'L',false);
			// $pdf->MultiCell(0,4,utf8_decode($document['observacion1']),'BLR','L');
		}


        $this->ln(6);
        $this->SetTextColor(255,255,255);
        $this->Image($document['gradient'], $this->getX(), $this->getY(), 196, 8);
        $this->SetFont('Arial','B', 7);

        if ($showValues) {
            $this->Cell(7, 8, utf8_decode('Ítem'), 0, 0, 'C');
            $this->Cell(20, 8, utf8_decode('Código'), 0, 0, 'C');
            $this->Cell(58, 8, utf8_decode('Descripción'), 0, 0, 'C');
            $this->Cell(9, 8, utf8_decode('Cantidad'), 0, 0, 'C');
            $this->Cell(21, 8, utf8_decode('Valor Unit'), 0, 0, 'C');
            $this->Cell(20, 8, utf8_decode('Descuento'), 0, 0, 'C');
            $this->Cell(21, 8, utf8_decode('Valor Bruto'), 0, 0, 'C');
            $this->Cell(21, 8, utf8_decode('Valor Iva'), 0, 0, 'C');
            $this->Cell(20, 8, utf8_decode('Valor Total'), 0, 0, 'C');
        }else{
            $this->Cell(10, 8, utf8_decode('Ítem'), 0, 0, 'C');
            $this->Cell(65, 8, utf8_decode('Código'), 0, 0, 'C');
            $this->Cell(60, 8, utf8_decode('Descripción'), 0, 0, 'C');
            $this->Cell(60, 8, utf8_decode('Cantidad'), 0, 0, 'C');
        }


        $this->Image(storage_path().'/app/public/Logo-ERP-V.png',207 ,127,10);

        $this->ln(8);
    }

    public function Footer()
    {
        $document = $this->PDFDocument;

        // $this->SetTextColor(255,255,255);
        $this->SetFillColor(255, 255, 255);

        if ($document['operationType_id'] == 5 ) {
            $this->SetXY(10,-58);
            $this->SetFont('Arial','B');
            $this->Cell(0, 4, utf8_decode('SOMOS SUJETOS DE RETENCION DE ICA UNICAMENTE EN LAS CIUDADES DE BOGOTA Y CALI'), 0, 0, 'C', false);
            $this->SetFont('Arial','');
        }

        $this->SetXY(10,-55);
        $this->MultiCell(0, 4, utf8_decode('CUENTA CORRIENTE DAVIVIENDA 457969996299 /CUENTA CORRIENTE ITAU 013384524 /CTA CORRIENTE BANCOLOMBIA 69760999972 CUENTA CORRIENTE BANCO DE OCCIDENTE 234874923; BANCO AV VILLAS CUENTA CORRIENTE 394005383(BOGOTA) 370013575(OTRAS CIUDADES) Esta Factura se asimila en todos sus efectos a una letra de cambio de conformidad con los articulos 621,722,773 Y 774 Codigo de Comercio. Actividad Economica 4530. REGIMEN COMUN. NO SOMOS GRANDES CONTRIBUYENTES.TARIFA ICA 11.4xMil'), 0, 'L', false);
        // $this->MultiCell(0, 4, utf8_decode('FACTURACION POR CONTINGENCIA FE'), 0, 'L', false);
		$this->SetTextColor(76,76,76);
        $this->MultiCell(0, 4, utf8_decode('A partir del vencimiento se causaran intereses por mora a la tasa maxima legal autorizada. A la presente factura se aplicarán, en lo pertinente las normas relativas a la Letra de Cambio. Autorizo a la entidad  COLSAISA SAS  o a quien represente sus derechos u ostente en el futuro la calidad de acreedor a reportar,procesar,solicitar o divulgar a cualquier Entidad que manejeo administre bases de datos con la informacion referente a mi comportamiento comercial'), 0, 'C', false);
        $this->MultiCell(0, 4, utf8_decode('Bogotá:Principal: Cra 124 No.17-80 Fontibon Tel : 7452248'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Medellín: Cra 47 # 87-27 Tel: 3164739135 – 3008646489; Barranquilla: Calle 63A # 38-21 Esquina Tel: 3188480734;'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Cali: Cra 6 # 21-23 San Nicolas, Tel: 3154104280;'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Bucaramanga: Calle 16 # 15-33 Barrio Mutualidad, Tel: 3153758109; Cali: Cra 6 # 21-23 San Nicolas, Tel: 3163205200; Ibague: Calle 39A # 6-37 Barrio Restrepo, Tel: 3175780408;'), 0, 'C', false);
        $this->MultiCell(0, 4, utf8_decode('Cucuta: Calle 1 # 4-44, LOCAL 4, Edificio San Judas Tadeo, Tel: 3183870338'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Villavicencio: Cra 29 # 28-37, El Porvenir, Tel: 3167575281; Cucuta: Calle 1 # 4-44, LOCAL 4, Edificio San Judas Tadeo, Tel: 3183870338'), 0, 'C', false);
        // $this->MultiCell(0, 4, utf8_decode('Duitama: Calle 6 # 18 -79 Barrio Cándido Quintero, Tel: 3156977392; Monteria: Calle 21 N 3-34 Centro Parqueadero CicloEnergia, TEL: 3147926023'), 0, 'C', false);
        $this->MultiCell(0, 4, utf8_decode(''), 0, 'C', false);
        $this->SetFillColor(255, 255, 255);

        if ($document['operationType_id'] == 5 || $document['operationType_id'] == 1) {
            $this->MultiCell(0, 4, utf8_decode('Proveedor Tecnológico Facturación Electrónica: The FactoryHKAColombia SAS- NIT: 900390126-6'), 0, 'C', false);
        }

        if (config('app.enable_electronic_billing') && !empty($document['cufe'])) {
            $this->SetTextColor(0, 0, 0);
            $this->SetXY(10, -60);
            $this->Cell(0, 4, utf8_decode('CUFE: ' . $document['cufe']), 0, 0, 'C', false);
            $this->ln(4);
        }

        // $this->SetFillColor(2, 5, 56,1);
		// $this->SetTextColor(255,255,255);
        // $this->SetXY(10,-33);
        // $this->Cell(0, 4, utf8_decode('Información adicional'), 0, 0, 'C',true);
        // $this->ln(4);
        // $this->SetFillColor(255, 255, 255);
		// $this->SetTextColor(0,0,0);
        // $this->Cell(0, 4, utf8_decode('Pedido: '.$document['numero'].' /Observación: '.$document['observacion1'].'/Dir entrega: /'), 0, 0, 'L',true);
        // $this->ln(4);
        // $this->Cell(0, 4, utf8_decode('Vendedor: '.$document['vendedor'].' /Elaboro: '.$document['elaboro']), 0, 0, 'L',true);
        // $this->ln(9);
        // $this->Cell(25, 4, utf8_decode('Firma resposable:'), 0, 0, 'L',true);
        // $this->setDrawColor(0,0,0);
        // $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');
        // $this->setX(100);
        // $this->Cell(25, 4, utf8_decode('  Recibido por:'), 0, 0, 'L',true);
        // $this->Cell(50, 3, utf8_decode(''), 'B', 0, 'L');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        }
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            // $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->SetDrawColor(230, 230, 230);
            $this->SetFillColor(245, 245, 255);
            $this->MultiCell($w, 5, $data[$i], 'RLB', $a, true);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}
