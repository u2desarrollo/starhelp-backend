<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\BranchOfficeWarehouse;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class BranchOffice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'branchoffices';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'subscriber_id',
        'name',
        'address',
        'telephone',
        'fax',
        'main',
        'city_id',
        'latitude',
        'length',
        'branchoffice_type',
        'code',
        'minimum_order_amount',
        'email'

    ];

    public $visible= [
        'id',
        'name',
        'address',
        'main',
        'city',
        'warehouses',
        'telephone',
        'fax',
        'main',
        'city_id',
        'latitude',
        'length',
        'branchoffice_type',
        'subscriber_id',
        'code',
        'minimum_order_amount',
        'email'


    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subscriber_id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'main' => 'integer',
        'city_id' => 'integer',
        'latitude' => 'string',
        'length' => 'string',
        'branchoffice_type' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Entities\City::class, 'city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(\App\Entities\Subscriber::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function warehouses()
    {
        return $this->hasMany(BranchOfficeWarehouse::class, 'branchoffice_id', 'id');
    }
    public function prices(){
        return $this->hasMany(\App\Entities\Price::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }


    //  tipos de comprobantes 
    public function voucherTypesBranchoffice()
    {
        return $this->hasMany(branchoffice::class,'branchoffice_vouchertype_id');
    }
}
