<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductsHasPromotions extends Model
{
    protected $table = 'products_has_promotions';

    protected $fillable = [
        'promotion_id',
        'branchoffice_id',
        'product_id',
        'paramenter_id'
    ];
}
