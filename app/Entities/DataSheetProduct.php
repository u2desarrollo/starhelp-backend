<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DataSheetProduct extends Model
{
    public $table = 'data_sheet_products';

    public $timestamps = false;

    public $fillable = [
        'product_id',
        'data_sheet_id',
        'value',
    ];
    
    protected $casts = [
        'value' => 'array',
    ];

    public function product()
    {
        return $this->belongsTo('App\Entities\Product');
    }

}