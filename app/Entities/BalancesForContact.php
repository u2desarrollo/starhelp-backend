<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BalancesForContact extends Model
{
    protected $table = 'balances_for_contacts';

    protected $fillable = [
        'year_month',
        'account_id',
        'contact_id',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public static $columns = [
        'year_month',
        'account_id',
        'contact_id',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public function __construct(array $attributes = [])
    {
        $this->attributes['initial_balance_month'] = 0;
        $this->attributes['original_balance'] = 0;

        parent::__construct($attributes);
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

}
