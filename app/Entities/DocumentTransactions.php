<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentTransactions extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'document_transactions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable=[
        // 'document_id',
        // 'branchoffice_id',
        // 'vouchertype_id',
        // 'date_vouchertype',
        // 'number_vouchertype',
        // 'account_id',
        // 'contact_id',
        // 'document_number',
        // 'prefix',
        // 'quota_number',
        // 'type_bank_document',
        // 'bank_document_number',
        // 'cost_center_1',
        // 'cost_center_2',
        // 'cost_center_3',
        // 'business_id',
        // 'concept',
        // 'document_date',
        // 'due_date',
        // 'pay_day',
        // 'seller_id',
        // 'detail',
        // 'automatic',
        // 'observations',
        // 'operation_value',
        // 'transactions_type',
        // 'base_value',
        // 'base_document_id',
        // 'payment_method',
        // 'reference_detail',
        // 'num_fact',
        // 'Pref_fact',
        // 'number_bussiness'

        'year_month',
        'document_id',
        'branchoffice_id',
        'vouchertype_id',
        'account_id',
        'contact_id',
        'quota_number',
        'consecutive',
        'document_date',
        'seller_id',
        'issue_date',
        'pay_day',
        'due_date',
        'concept',
        'detail',
        'refer_detail',
        'observations',
        'type_bank_document',
        'bank_document_number',
        'cost_center_1',
        'cost_center_2',
        'cost_center_3',
        'projects_id',
        'number_business',
        'payment_method',
        'crossing_document_id',
        'commissionable_concept_id',
        // 'crossing_vouchertype_id',
        // 'crossing_prefix',
        // 'crossing_consecutive',
        // 'crossing_due_date_document',
        'order',
        'operation_value',
        'transactions_type',
        'base_value',
        'base_document_id',
        'affects_document_id',
        'origin_document',
        'transaction_reference_id',
        'optional_sequence',
        'fix_asset_id'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function account()
	{
		return $this->belongsTo(\App\Entities\Account::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function fixAsset()
	{
		return $this->belongsTo(\App\Entities\FixAsset::class);
    }
    /**
     * @return mixed
     */
    public function document()
    {
        // return $this->HasMany(\App\Entities\Document::class, 'thread', 'id');
       return $this->belongsTo(Document::class);
    }
    /**
     * @return mixed
     */
    public function affectsDocument()
    {
        // return $this->HasMany(\App\Entities\Document::class, 'thread', 'id');
       return $this->belongsTo(Document::class, 'affects_document_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function projects()
    {
        return $this->belongsTo(\App\Entities\Projects::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function payment()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'payment_method');
    }
     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

    public function vouchertype()
    {
        return $this->belongsTo(VouchersType::class);
    }

    public function transactionReference()
    {
       return $this->belongsTo(BalancesForDocument::class, 'transaction_reference_id');
    }
}
