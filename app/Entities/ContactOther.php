<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactOther extends Model
{
    protected $table = 'contact_others';

    protected $fillable = [
        'contact_id',
        'category_id',
        'observations'
    ];
}
