<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\Promotion;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subbrand
 * @package App\Entities
 * @version March 4, 2019, 7:56 pm UTC
 *
 * @property \App\Entities\Brand brand
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer brand_id
 * @property string subbrand_code
 * @property string description
 * @property string image
 * @property boolean lock_buy
 * @property boolean block_sale
 * @property string data_sheet
 * @property float margin_cost_percentage
 */
class Subbrand extends Model
{
    use SoftDeletes;

    public $table = 'subbrands';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'brand_id',
        'subbrand_code',
        'description',
        'image',
        'lock_buy',
        'block_sale',
        'data_sheet',
        'margin_cost_percentage'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'brand_id' => 'integer',
        'subbrand_code' => 'string',
        'description' => 'string',
        'image' => 'string',
        'lock_buy' => 'integer',
        'block_sale' => 'integer',
        'data_sheet' => 'string',
        'margin_cost_percentage' => 'float'
    ];

    public function getImageAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    public function getDataSheetAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static function rulesCreate($id) {
        return [
            'subbrand_code' =>  Rule::unique('subbrands')->where(function ($query) use ($id) {
                return $query->where('brand_id', $id)
                            ->whereNull('deleted_at');
            }),
            //'subline_code' => 'required|unique:sublines,subline_code,' . $id . ',line_id|max:15',
        ];
    }

    public static function rulesUpdate($brand_id, $id){
        return [
            'subbrand_code' => Rule::unique('subbrands')->where(function ($query) use ($brand_id, $id) {
                return $query->where('brand_id', $brand_id)
                ->where('id','<>',$id)
                ->whereNull('deleted_at');
            }),
            // 'subline_code' => 'required|unique:sublines,subline_code,' . $id_line . ',line_id,' . $id . ',id|max:15',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function brand()
    {
        return $this->belongsTo(\App\Entities\Brand::class);
    }

    public function promotions() {
    	return $this->hasMany(\App\Entities\Promotion::class, 'subbrand_id');
    }
}
