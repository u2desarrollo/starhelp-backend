<?php

namespace App\Entities;

use Eloquent as Model;

/**
 * Class City
 *
 * @package App\Entities
 */
class City extends Model
{

    public $table = 'cities';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'department_id',
        'city'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'department_id' => 'integer',
        'city' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function department()
    {
        return $this->belongsTo(\App\Entities\Department::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function branchoffices()
    {
        return $this->hasMany(\App\Entities\Branchoffice::class, 'city_id', 'id');
    }
}
