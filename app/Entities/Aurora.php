<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aurora extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'aurora';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable=[
        'type',
        'option',
        'frequency',
        'period',
        'hour',
        'active',
        'email',
        'days',
        'last_execution_date'
    ];

    protected $casts = [
        'type',
        'option',
        'frequency',
        'period',
        'Hour',
        'active' => 'integer',
        'email',
        'days',
        'last_execution_date'
    ];

    public function option_()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'option');
    }

}
