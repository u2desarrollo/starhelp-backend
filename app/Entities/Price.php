<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Price
 * @package App\Entities
 * @version April 3, 2019, 7:09 pm UTC
 *
 * @property \App\Entities\Branchoffice branchoffice
 * @property \App\Entities\Parameter parameter
 * @property \App\Entities\Product product
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property integer product_id
 * @property integer price_list_id
 * @property integer branchoffice_id
 * @property float price
 * @property boolean include_iva
 * @property float previous_price
 */
class Price extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'prices';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'price_list_id',
        'branchoffice_id',
        'price',
        'previous_price',
        'floor_price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'price_list_id' => 'integer',
        'branchoffice_id' => 'integer',
        'price' => 'float',
        'previous_price' => 'float',
        'floor_price' => 'integer',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchOffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class,'branchoffice_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class,'price_list_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Entities\Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

}
