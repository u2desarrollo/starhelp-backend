<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\BranchOffice;

/**
 *
 */
class BranchOfficeWarehouse extends Model
{

    public $table = 'branchoffice_warehouses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'branchoffice_id',
        'warehouse_code',
        'warehouse_description',
        'update_inventory',
        'exits_no_exist',
        'negative_balances',
        'warehouse_status',
        'last_date_physical_inventory',
        'number_physical_inventory',
        'code_previous',
        'inventories_account'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'branchoffice_id' => 'integer',
        'warehouse_code' => 'string',
        'warehouse_description' => 'string',
        'update_inventory' => 'integer',
        'exits_no_exist' => 'integer',
        'negative_balances' => 'integer',
        'warehouse_status' => 'integer',
        'last_date_physical_inventory' => 'datetime:Y-m-d',
        'number_physical_inventory' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchoffice()
    {
        return $this->belongsTo(BranchOffice::class, 'branchoffice_id');
    }
}
