<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentPaymentMethod extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'documents_payment_methods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'document_id',
        'payment_method_id',
        'authorization_number',
        'check_number',
        'entity',
        'value',
        'value_change',
        'total_value'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * Relacion muchos a uno a document
     * 
     * @author Kevin Galindo
     */
    public function document()
    {
        return $this->belongsTo('App\Entities\Document');
    }

    /**
     * Relacion muchos a uno a parameters
     * 
     * @author Kevin Galindo
     */
    public function paymentMethod()
    {
        return $this->belongsTo('App\Entities\Parameter', 'payment_method_id');
    }
}
