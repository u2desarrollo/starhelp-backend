<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'accounts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable=[
        'code',
        'name',
        'short_name',
        'description',
        'debit_credit',
        'group',
        'level',
        'account_id_totalize',
        'account_id',
        'category',
        'branchoffice_id',
        'contact_id',
        'manage_contact_balances',
        'is_customer',
        'is_provider',
        'is_employee',
        'is_others',
        'resume_beginning_year',
        'wallet_type',
        'document_description',
        'manage_quota_balances',
        'cost_center_1',
        'cost_center_2',
        'cost_center_3',
        'manage_projects_stage',
        'manage_projects',
        'commissionable_concept',
        'manage_bank_document',
        'manage_way_to_pay',
        'tax_id',
        'Active_account',
        'account_created_at',
        'account_cancellation_date',
        'pevious_account',
        'user_id',
        'results_account',
        'manage_document_balances',
        'manage_base_value',
        'line_id'
    ];

    protected $casts = [
        'results_account' => 'integer',
    ];
    public function setNameAttribute($value)
	{
		$this->attributes['name'] = strtoupper($value);
	}

    public function account_totalize()
    {
		return $this->belongsTo(\App\Entities\Account::class, 'account_id_totalize');
    }

    public function branchoffice()
	{
		return $this->belongsTo(\App\Entities\BranchOffice::class);
    }

    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function line()
    {
        return $this->belongsTo(\App\Entities\Line::class, 'line_id');
    }
    public function BalanceForAccount()
    {
        return $this->hasMany(\App\Entities\BalancesForAccount::class, 'account_id');
    }

    public function parameter_group()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'group');
    }

    public function parameter_level()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'level');
    }

    public function parameter_category()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'category');
    }

    public function parameter_wallet_type()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'wallet_type');
    }

    public function parameter_cost_center_1()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'cost_center_2');
    }

    public function parameter_cost_center_2()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'cost_center_2');
    }

    public function parameter_cost_center_3()
    {
        return $this->belongsTo(\App\Entities\Parameter::class, 'cost_center_2');
    }

    public function tax()
    {
        return $this->belongsTo(\App\Entities\Tax::class, 'tax_account');
    }

    public function user_modification()
    {
        return $this->belongsTo(\App\Entities\User::class, 'user_id_last_modification');
    }

}
