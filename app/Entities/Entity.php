<?php

namespace App\Entities;

use Eloquent as Model;

class Entity extends Model
{

    public $table = 'entities';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'subscribers_id',
        'identification_type',
        'identification',
        'check_digit',
        'name',
        'surname',
        'is_customer',
        'is_provider',
        'is_employee',
        'state',
        'city_id',
        'code_ciiu',
        'commercial_address',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'web_page',
        'birthdate',
        'gender',
        'stratum',
        'point_sale_code',
        'latitude',
        'length',
        'class_person',
        'taxpayer_type',
        'tax_regime',
        'declarant',
        'self_retainer',
        'category_code_customer',
        'seller_code',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority_customer',
        'code_zone_customer',
        'code_price_list_customer',
        'percentage_discount_customer',
        'credit_quota_customer',
        'expiration_date_credit_quota_customer',
        'days_soon_payment_customer_1',
        'percentage_soon_payment_customer_1',
        'days_soon_payment_customer_2',
        'percentage_soon_payment_customer_2',
        'days_soon_payment_customer_3',
        'percentage_soon_payment_customer_3',
        'contract_number_customer',
        'last_purchase_customer',
        'blocked_customer',
        'observation_customer',
        'image_customer',
        'retention_source_customer',
        'retention_iva_customer',
        'retention_ica_customer',
        'code_ica_customer',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'category_code_provider',
        'seller_provider',
        'contact_provider',
        'comments_provider',
        'percentage_commision_provider',
        'contract_number_provider',
        'code_price_list_provider',
        'priority_provider',
        'percentage_discount_provider',
        'credit_quota_provider',
        'expiration_date_credit_quota_provider',
        'days_soon_payment_provider_1',
        'percentage_soon_payment_provider_1',
        'days_soon_payment_provider_2',
        'percentage_soon_payment_provider_2',
        'days_soon_payment_provider_3',
        'percentage_soon_payment_provider_3',
        'replacement_days_provider',
        'electronic_order_provider',
        'update_shopping_list',
        'calculate_sale_prices_provider',
        'last_purchase_provider',
        'blocked_provider',
        'observation_provider',
        'image_provider',
        'retention_source_provider',
        'percentage_retention_source_provider',
        'retention_iva_provider',
        'percentage_retention_iva_provider',
        'retention_ica_provider',
        'code_ica_provider',
        'payment_bank_code',
        'account_type_provider',
        'account_number_provider',
        'code_category_employee',
        'cost_center_employee',
        'contract_number_employee',
        'percentage_commision_employee',
        'position_employee',
        'image_employee',
        'comments_employee'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subscribers_id' => 'integer',
        'identification_type' => 'integer',
        'identification' => 'string',
        'check_digit' => 'string',
        'name' => 'string',
        'surname' => 'string',
        'is_customer' => 'integer',
        'is_provider' => 'integer',
        'is_employee' => 'integer',
        'state' => 'integer',
        'city_id' => 'integer',
        'code_ciiu' => 'string',
        'commercial_address' => 'string',
        'main_telephone' => 'string',
        'secondary_telephone' => 'string',
        'fax' => 'string',
        'cell_phone' => 'string',
        'email' => 'string',
        'web_page' => 'string',
        'birthdate' => 'date',
        'gender' => 'string',
        'stratum' => 'string',
        'point_sale_code' => 'integer',
        'latitude' => 'string',
        'length' => 'string',
        'class_person' => 'integer',
        'taxpayer_type' => 'integer',
        'tax_regime' => 'integer',
        'declarant' => 'integer',
        'self_retainer' => 'integer',
        'category_code_customer' => 'integer',
        'seller_code' => 'integer',
        'electronic_invoice_shipping' => 'integer',
        'b2b_portal_access' => 'integer',
        'priority_customer' => 'integer',
        'code_zone_customer' => 'integer',
        'code_price_list_customer' => 'integer',
        'percentage_discount_customer' => 'float',
        'credit_quota_customer' => 'integer',
        'expiration_date_credit_quota_customer' => 'date',
        'days_soon_payment_customer_1' => 'integer',
        'percentage_soon_payment_customer_1' => 'float',
        'days_soon_payment_customer_2' => 'integer',
        'percentage_soon_payment_customer_2' => 'float',
        'days_soon_payment_customer_3' => 'integer',
        'percentage_soon_payment_customer_3' => 'float',
        'contract_number_customer' => 'string',
        'last_purchase_customer' => 'date',
        'blocked_customer' => 'integer',
        'observation_customer' => 'string',
        'image_customer' => 'string',
        'retention_source_customer' => 'integer',
        'retention_iva_customer' => 'integer',
        'retention_ica_customer' => 'integer',
        'code_ica_customer' => 'integer',
        'date_financial_statement' => 'date',
        'assets' => 'integer',
        'liabilities' => 'integer',
        'heritage' => 'integer',
        'category_code_provider' => 'integer',
        'seller_provider' => 'string',
        'contact_provider' => 'string',
        'comments_provider' => 'string',
        'percentage_commision_provider' => 'float',
        'contract_number_provider' => 'string',
        'code_price_list_provider' => 'integer',
        'priority_provider' => 'string',
        'percentage_discount_provider' => 'float',
        'credit_quota_provider' => 'integer',
        'expiration_date_credit_quota_provider' => 'date',
        'days_soon_payment_provider_1' => 'integer',
        'percentage_soon_payment_provider_1' => 'float',
        'days_soon_payment_provider_2' => 'integer',
        'percentage_soon_payment_provider_2' => 'float',
        'days_soon_payment_provider_3' => 'integer',
        'percentage_soon_payment_provider_3' => 'float',
        'replacement_days_provider' => 'integer',
        'electronic_order_provider' => 'integer',
        'update_shopping_list' => 'integer',
        'calculate_sale_prices_provider' => 'integer',
        'last_purchase_provider' => 'date',
        'blocked_provider' => 'integer',
        'observation_provider' => 'string',
        'image_provider' => 'string',
        'retention_source_provider' => 'integer',
        'percentage_retention_source_provider' => 'float',
        'retention_iva_provider' => 'integer',
        'percentage_retention_iva_provider' => 'float',
        'retention_ica_provider' => 'integer',
        'code_ica_provider' => 'integer',
        'payment_bank_code' => 'integer',
        'account_type_provider' => 'integer',
        'account_number_provider' => 'string',
        'code_category_employee' => 'integer',
        'cost_center_employee' => 'integer',
        'contract_number_employee' => 'string',
        'percentage_commision_employee' => 'float',
        'position_employee' => 'integer',
        'image_employee' => 'string',
        'comments_employee' => 'string'
    ];

    public function getImageCustomerAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    public function getImageProviderAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    public function getImageEmployeeAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter1()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Entities\City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter3()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter4()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter5()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter6()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter7()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter8()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter9()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter10()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(\App\Entities\Subscriber::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter11()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter12()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter13()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    public function warehouses()
    {

        return $this->hasMany(\App\Entities\EntityWareHouse::class);

    }

}
