<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobMember extends BaseModel
{
    use SoftDeletes;

	public $table = 'jobs_members';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'jobs_star_id',
        'user_id',
        'user_inviting_id'
    ];

    public static $rules = [
        'jobs_star_id'      => 'required|exists:jobs_star,id',
        'user_id'           => 'required|exists:users,id',
        'user_inviting_id'  => 'required|exists:users,id'
    ];

    /**
     * Get the JobsStar that owns the JobsMember
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobStar()
    {
        return $this->belongsTo(JobStar::class, 'jobs_star_id');
    }

    /**
     * Get the User that owns the JobsMember
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the User that owns the JobsMember
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userInviting()
    {
        return $this->belongsTo(User::class);
    }

}
