<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'areas';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'active'
    ];    

    public function company()
    {
        return $this->belongsToMany(Company::class);
    }
}
