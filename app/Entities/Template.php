<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Eloquent as Model;

/**
 * Class Template
 *
 * @package App\Entities
 */
class Template extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'templates';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'price_list',
        'method',
        'condition',
        'select_projects',
        'code',
        'description',
        'operationType_id',
        'voucherType_id',
        'consecutive_handling',
        'return_model_id',
        'select_branchoffice',
        'select_warehouse',
        'select_cost_center',
        'select_import',
        'document_name',
        'select_validate_wms',
        'select_send_wms',
        'quantity_decimals',
        'seller_mode',
        'currency',
        'observation_tittle_1',
        'observation_tittle_2',
        'footnotes',
        'user_id',
        'select_conveyor',
        'capt_delivery_address',
        'capt_license',
        'final_function',
        'sale_price_control',
        'stock_control',
        'contact_capture',
        'contact_type',
        'contact_category',
        'contact_id',
        'basic_contact_information',
        'financial_info',
        'button_360',
        'send_contact_email',
        'doc_base_management',
        'voucher_type_id',
        'model_id',
        'see_backorder',
        'days_number',
        'doc_cruce_management',
        'tittle',
        'see_seller_doc',
        'valid_exist',
        'value_control',
        'quantity_control',
        'crossing_document_dates',
        'add_products',
        'price_list_id',
        'add_promotions_button',
        'recalc_iva_less_cost',
        'recalc_iva_base',
        'up_excel_button',
        'down_excel_button',
        'line_id',
        'see_location',
        'modify_prices',
        'see_discount',
        'edit_discount',
        'see_bonus_amount',
        'edit_bonus_amount',
        'see_vrunit_base',
        'see_cant_backorder',
        'backorder_quantity_control',
        'see_physical_cant',
        'edit_physical_cant',
        'see_lots',
        'edit_lots',
        'edit_unit_value',
        'see_applied_promotion',
        'see_seller',
        'edit_seller',
        'hide_values',
        'pay_taxes',
        'see_product_observation',
        'edit_product_observation',
        'hide_value',
        'cents_values',
        'hands_free_operation',
        'discount_foot_bill',
        'retention_management',
        'handle_payment_methods',
        'payment_methods_allowed_id',
        'purchase_difference_management',
        'print',
        'print_template',
        'print_tittle',
        'product_order',
        'total_letters',
        'cash_drawer_opens',
        'control_wallet_and_credit_quota',
        'handling_other_parameters_state',
        'handling_other_parameters_description',
        'handling_other_parameters_param_table_id',
        'branchoffice_id',
        'branchoffice_warehouse_id',
        'TRM_value',
        'minimal_price',
        'edit_value_iva',
        'import_number',
        'branchoffice_vouchertype_id',
        'branchoffice_change',
        'edit_cost',
        'credit_note',
        'edit_retention',
        'accounting_generate',
        'accounting_concept',
        'accounting_according',
        'accounting_account',
        'attach_file',
        'business',
        'cost_center',
        'quantity_zero',
        'handle_payment_methods',
        'view_thread_history',
        'document_save_finish',
        'capture_vehicle_data',
        'technical_capture',
        'vehicle_review',
        'because_he_half_found_out',
        'validates_amount_at_zero',
        'restart_document',
        'capture_commission_percentage',
        'capture_accounting_account',
        'select_date_entry',
        'inherit_products',
        'multiple_documents'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                             => 'integer',
        'code'                           => 'string',
        'operationType_id'               => 'integer',
        'voucherType_id'                 => 'integer',
        'consecutive_handling'           => 'integer',
        'return_model_id'                => 'integer',
        'select_branchoffice'            => 'integer',
        'select_warehouse'               => 'integer',
        'select_import'                  => 'integer',
        'select_validate_wms'            => 'integer',
        'select_send_wms'                => 'integer',
        'document_name'                  => 'integer',
        'select_date_entry'              => 'integer',
        'select_cost_center'             => 'integer',
        'quantity_decimals'              => 'integer',
        'select_projects'                => 'integer',
        'seller_mode'                    => 'integer',
        'currency'                       => 'integer',
        'observation_tittle_1'           => 'string',
        'observation_tittle_2'           => 'string',
        'footnotes'                      => 'string',
        'user_id'                        => 'integer',
        'select_conveyor'                => 'integer',
        'capt_delivery_address'          => 'integer',
        'capt_license'                   => 'integer',
        'final_function'                 => 'string',
        'contact_capture'                => 'integer',
        'contact_type'                   => 'integer',
        'contact_category'               => 'integer',
        'contact_id'                     => 'integer',
        'basic_contact_information'      => 'integer',
        'financial_info'                 => 'integer',
        'button_360'                     => 'integer',
        'send_contact_email'             => 'integer',
        'doc_base_management'            => 'integer',
        'voucher_type_id'                => 'integer',
        'model_id'                       => 'integer',
        'see_backorder'                  => 'integer',
        'see_seller_doc'                 => 'integer',
        'days_number'                    => 'integer',
        'doc_cruce_management'           => 'integer',
        'tittle'                         => 'string',
        'valid_exist'                    => 'integer',
        'value_control'                  => 'integer',
        'quantity_control'               => 'integer',
        'crossing_document_dates'        => 'integer',
        'add_products'                   => 'integer',
        'price_list_id'                  => 'integer',
        'add_promotions_button'          => 'integer',
        'recalc_iva_less_cost'           => 'integer',
        'recalc_iva_base'                => 'integer',
        'up_excel_button'                => 'integer',
        'down_excel_button'              => 'integer',
        'line_id'                        => 'integer',
        'see_location'                   => 'integer',
        'modify_prices'                  => 'integer',
        'see_discount'                   => 'integer',
        'edit_discount'                  => 'integer',
        'see_bonus_amount'               => 'integer',
        'edit_bonus_amount'              => 'integer',
        'see_vrunit_base'                => 'integer',
        'see_cant_backorder'             => 'integer',
        'backorder_quantity_control'     => 'integer',
        'see_physical_cant'              => 'integer',
        'edit_physical_cant'             => 'integer',
        'see_lots'                       => 'integer',
        'edit_lots'                      => 'integer',
        'edit_unit_value'                => 'integer',
        'see_applied_promotion'          => 'integer',
        'see_seller'                     => 'integer',
        'edit_seller'                    => 'integer',
        'hide_values'                    => 'integer',
        'pay_taxes'                      => 'integer',
        'see_product_observation'        => 'integer',
        'edit_product_observation'       => 'integer',
        'hide_value'                     => 'integer',
        'cents_values'                   => 'integer',
        'hands_free_operation'           => 'integer',
        'discount_foot_bill'             => 'integer',
        'retention_management'           => 'integer',
        'handle_payment_methods'         => 'integer',
        'payment_methods_allowed_id'     => 'integer',
        'purchase_difference_management' => 'integer',
        'print'                          => 'integer',
        'print_template'                 => 'integer',
        'print_tittle'                   => 'string',
        'product_order'                  => 'integer',
        'total_letters'                  => 'integer',
        'cash_drawer_opens'              => 'integer',
        'branchoffice_id'                => 'integer',
        'branchoffice_warehouse_id'      => 'integer',
        'TRM_value'                      => 'integer',
        'accounting_concept'             => 'integer',
        'accounting_according'           => 'integer',
        'accounting_account'             => 'integer',
        'business'                       => 'integer',
        'cost_center'                    => 'integer',
        'inherit_products'               => 'integer',
        'multiple_documents'             => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];


    //Relaciones

    public function voucherType()
    {
        return $this->belongsTo(VouchersType::class, 'voucherType_id');
    }

    public function voucherType1()
    {
        return $this->belongsTo(VouchersType::class, 'voucher_type_id');
    }

    public function operationType()
    {
        return $this->belongsTo(TypesOperation::class, 'operationType_id');
    }

    /**
     * @return BelongsTo
     **/
    public function currency()
    {
        return $this->belongsTo(Parameter::class, 'currency');
    }

    /**
     * @return BelongsTo
     **/
    public function priceList()
    {
        return $this->belongsTo(Parameter::class, 'price_list_id');
    }

    /**
     * @return BelongsTo
     **/
    public function minimalPriceParam()
    {
        return $this->belongsTo(Parameter::class, 'minimal_price');
    }

    /**
     * @return BelongsTo
     **/
    public function contactCategory()
    {
        return $this->belongsTo(Parameter::class, 'contact_category');
    }

    /**
     * @return HasMany
     **/
    public function contacts()
    {
        return $this->belongsTo(Contact::class);
    }

    public function lines()
    {
        return $this->belongsToMany(Line::class)->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function modelo()
    {
        return $this->HasOne(Template::class, 'model_id', 'id');
    }

    public function modelo1()
    {
        return $this->HasOne(Template::class, 'return_model_id', 'id');
    }

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class);
    }

    /**
     * @return BelongsTo
     **/
    public function branchOfficeWarehouse()
    {
        return $this->belongsTo(BranchOfficeWarehouse::class, 'branchoffice_warehouse_id');
    }

    /**
     * @return BelongsTo
     **/
    public function branchoffice()
    {
        return $this->belongsTo(BranchOffice::class, 'branchoffice_id');
    }

    public function authorizedUsers()
    {
        return $this->hasMany(TemplateUser::class, 'template_id', 'id');
    }

    public function templateAccountings()
    {
        return $this->hasMany(TemplateAccounting::class, 'model_id', 'id');
    }

    //  sede tipo de comprobante
    public function branchofficeVoucherType()
    {
        return $this->belongsTo(branchoffice::class);
    }

    public function transactions()
    {
        return $this->hasMany(TemplateTransaction::class, 'template_id', 'id');
    }
}
