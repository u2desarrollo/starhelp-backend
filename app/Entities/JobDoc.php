<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobDoc extends Model
{
    use SoftDeletes;

	public $table = 'jobs_docs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'jobs_star_id',
        'path_document',
        'name',
        'type'
    ];

    public static $rules = [
        'jobs_star_id'       => 'required|exists:jobs_star,id',
        'path_document'      => 'required|string'
    ];

    // public function getpathDocumentAttribute($value)
    // {
    //     return '/colsaisa/jobstar/'.$value;
    // }

    /**
     * Get the JobsStar that owns the JobsDoc
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobStar()
    {
        return $this->belongsTo(JobStar::class);
    }

}
