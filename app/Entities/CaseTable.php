<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseTable extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'cases';

    protected $dates = ['deleted_at'];

    protected $fillable=[
        'user_id',
        'title',
        'content',
        'solved',
        'area_id',
        'company_id',
        'priority_id',
        'state_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function priority()
    {
        return $this->belongsTo(Parameter::class, 'priority_id');
    }

    public function state()
    {
        return $this->belongsTo(Parameter::class, 'state_id');
    }

    public function answers()
    {
        return $this->hasMany(CaseAnswer::class, 'case_id');
    }
}
