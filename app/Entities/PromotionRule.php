<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromotionRule
 * @package App\Entities
 * @version April 17, 2019, 8:37 pm UTC
 *
 * @property \App\Entities\Parameter parameter
 * @property \App\Entities\PromotionGroupRule promotionGroupRule
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaMajors
 * @property \Illuminate\Database\Eloquent\Collection criteriaValues
 * @property \Illuminate\Database\Eloquent\Collection priceListPromotion
 * @property \Illuminate\Database\Eloquent\Collection productPromotion
 * @property integer parameters_id
 * @property integer promotion_group_rule_id
 */
class PromotionRule extends Model
{
	// use SoftDeletes;

	public $table = 'promotion_rule';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'parameters_id',
		'promotion_group_rule_id',
		'type'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'parameters_id' => 'integer',
		'promotion_group_rule_id' => 'integer',
		'type' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function parameter()
	{
		return $this->belongsTo(\App\Entities\Parameter::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function promotionGroupRule()
	{
		return $this->belongsTo(\App\Entities\PromotionGroupRule::class, 'promotion_group_rule_id');
	}
}