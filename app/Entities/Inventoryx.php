<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Inventoryx extends Model
{
    public $table = 'inventoriesx';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'branchoffice_warehouse_id',
        'product_id',
        'stock',
        'stock_values',
        'average_cost',
        'available'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'branchoffice_warehouse_id' => 'integer',
        'product_id' => 'integer',
        'stock' => 'integer',
        'stock_values' => 'integer',
        'average_cost' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return mixed
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return mixed
     */
    public function branchofficeWarehouse()
    {
        return $this->belongsTo(BranchOfficeWarehouse::class);
    }
}
