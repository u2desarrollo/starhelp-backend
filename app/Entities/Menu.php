<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Menu
 * @package App\Entities
 * @version March 6, 2019, 2:00 pm UTC
 *
 * @property \App\Entities\Module module
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property \Illuminate\Database\Eloquent\Collection MenuSubscriber
 * @property integer module_id
 * @property integer order
 * @property string description
 * @property string url
 * @property string icon
 * @property integer menu_id
 * @property integer permission_id
 */
class Menu extends Model
{

    public $table = 'menus';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'module_id',
        'order',
        'description',
        'url',
        'url_video_tutorial',
        'level',
        'icon',
        'menu_id',
        'permission_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'module_id' => 'integer',
        'order' => 'integer',
        'description' => 'string',
        'url' => 'string',
        'url_video_tutorial' => 'string',
        'icon' => 'string',
        'menu_id' => 'integer',
        'permission_id' => 'integer'
    ];

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = strtoupper($value);
    }

    public function getDescriptionAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * @return BelongsTo
     **/
    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    /**
     * @return BelongsTo
     **/
    public function secondLevel()
    {
        return $this->hasMany(Menu::class, 'menu_id')
            ->orderBy('order', 'ASC');
    }

    /**
     * @return BelongsTo
     **/
    public function thirdLevel()
    {
        return $this->hasMany(Menu::class, 'menu_id')
            ->orderBy('order', 'ASC');
    }
}
