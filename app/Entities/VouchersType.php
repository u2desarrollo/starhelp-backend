<?php

namespace App\Entities;

use Eloquent as Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 *
 */
class VouchersType extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $table = 'voucherstypes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'branchoffice_id',
        'code_voucher_type',
        'name_voucher_type',
        'short_name',
        'prefix',
        'consecutive_number',
        'electronic_bill',
        'resolution',
        'affectation',
        'state',
        'range_from',
        'range_up',
        'origin_document'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'branchoffice_id' => 'integer',
        'code_voucher_type' => 'string',
        'name_voucher_type' => 'string',
        'short_name' => 'string',
        'prefix' => 'string',
        'consecutive_number' => 'integer',
        'electronic_bill' => 'integer',
        'resolution' => 'string',
        'affectation' => 'string',
        'origin_document' => 'integer',
        'state' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchoffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payReceivables()
    {
        return $this->hasMany(\App\Entities\PayReceivable::class);
    }


    /**
     * @return mixed
     */
    public function template()
    {
        return $this->hasOne(Template::class, 'voucherType_id');
    }
}
