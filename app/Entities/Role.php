<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Permission;
use App\Entities\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
use  SoftDeletes;
    protected $fillable = ['role'];

    /**
     * Relación con Permission
     */
    public function getPermissions()
    {
        return $this->belongsToMany(Permission::class); // OJO - No olvidar importar la clase
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }

}
