<?php

namespace App\Entities;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="EntityWareHouse",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="entity_id",
 *          description="entity_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_warehouse",
 *          description="code_warehouse",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_warehouse",
 *          description="description_warehouse",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="length",
 *          description="length",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telephone",
 *          description="telephone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact",
 *          description="contact",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="observation",
 *          description="observation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="boolean"
 *      )
 * )
 */
class EntityWareHouse extends Model
{

    public $table = 'entity_warehouses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'entity_id',
        'code_warehouse',
        'description_warehouse',
        'address',
        'latitude',
        'length',
        'telephone',
        'email',
        'contact',
        'observation',
        'state'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'entity_id' => 'integer',
        'code_warehouse' => 'string',
        'description_warehouse' => 'string',
        'address' => 'string',
        'latitude' => 'string',
        'length' => 'string',
        'telephone' => 'string',
        'email' => 'string',
        'contact' => 'string',
        'observation' => 'string',
        'state' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function entity()
    {
        return $this->belongsTo(\App\Entities\Entity::class);
    }
}
