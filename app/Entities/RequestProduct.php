<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestProduct
 * @package App\Entities
 * @version September 1, 2019, 3:53 am UTC
 *
 * @property \App\Entities\RequestHeader requestHeader
 * @property \App\Entities\Product product
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection
 * @property integer request_header_id
 * @property integer product_id
 * @property integer quantity
 * @property string serial
 * @property string certificate_expiration_date
 * @property string fault_description
 * @property string photo
 */
class RequestProduct extends Model
{
    use SoftDeletes;

    public $table = 'request_products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'request_header_id',
        'product_id',
        'quantity',
        'serial',
        'certificate_expiration_date',
        'fault_description',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_header_id' => 'integer',
        'product_id' => 'integer',
        'quantity' => 'integer',
        'serial' => 'string',
        'certificate_expiration_date' => 'string',
        'fault_description' => 'string',
        'photo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    // public static $rules = [
    //     'id' => 'required',
    //     'request_header_id' => 'required',
    //     'product_id' => 'required'
    // ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function requestHeader()
    {
        return $this->belongsTo(\App\Entities\RequestHeader::class, 'request_header_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Entities\Product::class, 'product_id');
    }
}
