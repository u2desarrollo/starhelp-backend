<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 *
 */
class ParamTable extends Model {

    use SoftDeletes;

    public $table = 'paramtables';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'subscriber_id',
        'code_table',
        'name_table',
        'desc_code_parameter',
        'desc_name_parameter',
        'desc_alphanum_data_first',
        'desc_alphanum_data_second',
        'desc_alphanum_data_third',
        'desc_numeric_data_first',
        'desc_numeric_data_second',
        'desc_numeric_data_third',
        'desc_date_data_first',
        'desc_date_data_second',
        'desc_date_data_third',
        'observation',
        'protected',
        'desc_image',
        'line',
        'subline',
        'city',
        'branchoffice_warehouses',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subscriber_id' => 'integer',
        'code_table' => 'string',
        'name_table' => 'string',
        'desc_code_parameter' => 'string',
        'desc_name_parameter' => 'string',
        'desc_alphanum_data_first' => 'string',
        'desc_alphanum_data_second' => 'string',
        'desc_alphanum_data_third' => 'string',
        'desc_numeric_data_first' => 'string',
        'desc_numeric_data_second' => 'string',
        'desc_numeric_data_third' => 'string',
        'desc_date_data_first' => 'string',
        'desc_date_data_second' => 'string',
        'desc_date_data_third' => 'string',
        'observation' => 'string',
        'protected' => 'integer',
        'line' => 'integer',
        'subline' => 'integer',
        'city' => 'integer',
        'branchoffice_warehouses' => 'integer',
        'desc_image' => 'string',
    ];

    // Cambio de string a uppercase
    public function setNameTableAttribute($value) {
        $this->attributes['name_table'] = strtoupper($value);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function parameters()
    {
        return $this->hasMany(\App\Entities\Parameter::class);
    }

    #se agrega extraParameters para no toca parameters,  y afectar otra funcionalidad tenida al agregar la columna de relación
    public function extraParameters()
    {
        return $this->hasMany(\App\Entities\Parameter::class,'paramtable_id');
    }
}
