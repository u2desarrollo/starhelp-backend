<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DocumentProductLot extends Model
{
    protected $table = 'documents_products_lots';

    protected $fillable = [
        'documents_products_id',
        'num_lot',	
        'expiration_date'	
    ];
    
    public function documentProduct()
    {
        return $this->hasOne('\App\Entities\DocumentProduct');
    }
}
