<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerPromotion
 * @package App\Entities
 * @version April 17, 2019, 8:38 pm UTC
 *
 * @property \App\Entities\ContactsCustomer contactsCustomer
 * @property \App\Entities\ContactsWarehouse contactsWarehouse
 * @property \App\Entities\PromotionGroupRule promotionGroupRule
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property \Illuminate\Database\Eloquent\Collection contactLine
 * @property \Illuminate\Database\Eloquent\Collection criteriaMajors
 * @property \Illuminate\Database\Eloquent\Collection criteriaValues
 * @property \Illuminate\Database\Eloquent\Collection priceListPromotion
 * @property \Illuminate\Database\Eloquent\Collection productPromotion
 * @property \Illuminate\Database\Eloquent\Collection promotionRule
 * @property integer contact_customer_id
 * @property integer contact_warehouse_id
 * @property integer promotion_group_rule_id
 */
class CustomerPromotion extends Model
{
	use SoftDeletes;

	public $table = 'customer_promotion';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'contact_id',
		'contact_warehouse_id',
		'promotion_group_rule_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'contact_id' => 'integer',
		'contact_warehouse_id' => 'integer',
		'promotion_group_rule_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function contactsCustomer()
	{
		return $this->belongsTo(\App\Entities\ContactsCustomer::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function contactsWarehouse()
	{
		return $this->belongsTo(\App\Entities\ContactsWarehouse::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function promotionGroupRule()
	{
		return $this->belongsTo(\App\Entities\PromotionGroupRule::class);
	}
}