<?php

namespace App\Entities;

use App\Entities\Consignment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class CashReceiptsApp extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $fillable = ['consignment_id', 'creation_date', 'observations', 'contact_id', 'consecutive', 'invoice_link', 'warehouse_id','payment_applied_erp','id_erp'];
    protected $table = 'cash_receipts_app';

    public function consignement()
    {
        return $this->belongsTo(Consignment::class, 'consignment_id');
    }

    public function paymentMethodsApp()
    {
        return $this->hasMany(PaymentMethodsApp::class, 'cash_receipts_app_id');
    }

    public function contributionInvoicesApp()
    {
        return $this->hasMany(ContributionInvoicesApp::class, 'cash_receipts_app_id');
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(ContactWarehouse::class,'warehouse_id');
    }
}
