<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tax
 * @package App\Entities
 * @version September 9, 2019, 8:28 pm UTC
 *
 * @property string code
 * @property string description
 * @property integer percentage
 * @property integer base
 * @property integer minimum_base
 * @property string sales_account
 * @property string sales_return_account
 * @property string purchase_account
 * @property string purchase_return_account
 * @property boolean status
 */
class Tax extends Model
{
    use SoftDeletes;

    public $table = 'taxes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'description',
        'percentage_sale',
        'percentage_purchase',
        'base',
        'minimum_base',
        'sales_account',
        'sales_return_account',
        'purchase_account',
        'purchase_return_account',
        'status',
        'condition',
        'settlement_by_product'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'description' => 'string',
        'percentage' => 'double',
        'base' => 'integer',
        'minimum_base' => 'integer',
        'sales_account' => 'string',
        'sales_return_account' => 'string',
        'purchase_account' => 'string',
        'purchase_return_account' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
