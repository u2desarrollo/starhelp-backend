<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BalancesForDocument extends Model
{
    protected $table = 'balances_for_documents';

    protected $fillable = [
        'year_month',
        'account_id',
        'contact_id',
        'affects_document_id',
        'quota_number',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public static $columns = [
        'year_month',
        'account_id',
        'contact_id',
        'affects_document_id',
        'quota_number',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public function __construct(array $attributes = [])
    {
        $this->attributes['initial_balance_month'] = 0;
        $this->attributes['original_balance'] = 0;

        parent::__construct($attributes);
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }


    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

}
