<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PayReceivable
 * @package App\Entities
 * @version May 30, 2019, 9:42 pm UTC
 *
 * @property \App\Entities\Parameter parameter
 * @property \App\Entities\Receivable receivable
 * @property \App\Entities\Voucherstype voucherstype
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property integer receivable_id
 * @property integer vouchertype_id
 * @property integer voucher_number
 * @property date pay_date
 * @property integer pay_way
 * @property float applied_value
 */
class PayReceivable extends Model
{
   //  use SoftDeletes;

    public $table = 'pay_receivables';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'documents_balance_id',
        'vouchertype_id',
        'voucher_number',
        'pay_date',
        'pay_way',
        'applied_value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'documents_balance_id' => 'integer',
        'vouchertype_id' => 'integer',
        'voucher_number' => 'integer',
        'pay_date' => 'datetime:Y-m-d',
        'pay_way' => 'integer',
        'applied_value' => 'float',
        'debit_value' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class,'pay_way');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receivable()
    {
        return $this->belongsTo(\App\Entities\DocumentsBalance::class,'documents_balance_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function voucherstype()
    {
        return $this->belongsTo(\App\Entities\VouchersType::class,'vouchertype_id');
    }
}
