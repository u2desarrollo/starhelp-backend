<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixAsset extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'fix_assets';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'consecut',
        'plate_number',
        'previous_code',
        'active_group_id',
        'description',
        'long_description',
        'purchase_date',
        'date_regist',
        'year_month_start_caus',
        'year_month_end_caus',
        'year_month_last_caus',
        'months_to_caus',
        'provider_id',
        'invoice_prefix',
        'invoice_number',
        'invoice_document_id', //*
        'purchase_value',
        'iva_value',
        'addition_value',
        'historical_cost_value',
        'salvage_value',
        'previous_caus_value',
        'value_for_causing',
        'monthly_deprec_value',
        'caus_status',
        'record_account_id',
        'account_accumulated_depreciation_id',
        'account_depreciation_expense_id',
        'brand',
        'model',
        'serial_number',
        'engine_number',
        'deeds',
        'quantity',
        'class_id',
        'city_id',
        'branchoffice_id',
        'address',
        'location',
        'cost_center_id',
        'contact_responsible_id',
        'delivery_date',
        'delivery_doc',
        'derecognized_date',
        'contact_sale_id',
        'value_derecognized',
        'derecognized_type_id',
        'derecognized_observation',
        'derecognized_document_id',//*
        'maintenance_provider_id',
        'contract_description',
        'contract_expiration_date',
        'last_maintenance_date',
        'spare_parts',
        'maintenance_notes',
        'policy_number',
        'policy_provider_id',
        'policy_expiration_date',
        'coverages',
        'premium_value',
        'insured_value',
        'policy_observation',
        'date_last_commercial_appraisal',
        'value_last_commercial_appraisal'
    ];

    protected $casts = [
        'purchase_value' => 'float',
        'iva_value' => 'float',
        'addition_value' => 'float',
        'historical_cost_value' => 'float',
        'salvage_value' => 'float',
        'previous_caus_value' => 'float',
        'value_for_causing' => 'float',
        'monthly_deprec_value' => 'float',
        'value_derecognized' => 'float',
        'premium_value' => 'float',
        'insured_value' => 'float',
        'value_last_commercial_appraisal' => 'float',
    ];

    public function activeGroup()
	{
		return $this->belongsTo(\App\Entities\Parameter::class, 'active_group_id');
    }
    
    public function provider()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function invoiceDocument()
    {
        return $this->belongsTo(\App\Entities\Document::class);
    }

    public function recordAccount()
    {
        return $this->belongsTo(\App\Entities\Account::class);
    }

    public function accountAccumulatedDepreciation()
    {
        return $this->belongsTo(\App\Entities\Account::class);
    }

    public function accountDepreciationExpense()
    {
        return $this->belongsTo(\App\Entities\Account::class);
    }

    public function paramClass()
	{
        return $this->belongsTo(\App\Entities\Parameter::class, 'class_id');
    }
    
    public function city()
	{
        return $this->belongsTo(\App\Entities\City::class);
    }
    
    public function branchoffice()
	{
        return $this->belongsTo(\App\Entities\BranchOffice::class);
    }
    
    public function costCenter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    public function contactResponsible()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function contactSale()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function derecognizedType()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    public function derecognizedDocument()
    {
        return $this->belongsTo(\App\Entities\Document::class);
    }

    public function maintenanceProvider()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function policyProvider()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    public function additions()
    {
        return $this->hasMany(FixAssetAddition::class, 'fix_asset_id', 'id');
    }
}
