<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductPromotion extends Model
{
	protected $fillable = [
		'promotions_id',
		'product_id',
		'minimum_amount',
        'discount',
        'discount_value',
		'pay_soon',
		'cash_payment',
    ];

    protected $cast = [
        'discount'=>'integer',
        'discount_value'=>'integer',
	];

	protected $table = 'products_promotions';


	public function Promotion(){
		return $this->belongsTo(Promotion::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
