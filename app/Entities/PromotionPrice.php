<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PromotionPrice extends Model
{
    protected $table = 'promotion_prices';

    protected $fillable = [
        'product_id',
        'contact_warehouse_id',
        'price',
        'percentage'
    ];
}
