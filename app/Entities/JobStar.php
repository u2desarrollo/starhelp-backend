<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobStar extends BaseModel
{
    use SoftDeletes;

	public $table = 'jobs_star';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_creation_id',
        'name',
        'description',
        'state',
        'closing_date',
        'limit_date',
        'alert_type'
    ];

    public static $rules = [
        'name'             => 'required|min:5|max:255',
        'description'      => 'nullable|min:5|max:255',
        'limit_date'       => 'nullable|string',
        'alert_type'       => 'nullable|numeric',
        'files'            => 'nullable'
    ];

    /**
     * Get the user that owns the JobsStar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userCreation()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the files for the JobsStar
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(JobDoc::class, 'jobs_star_id');
    }

    /**
     * Get all of the members for the JobsStar
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(JobMember::class, 'jobs_star_id');
    }

    /**
     * Get all of the members for the JobsStar
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interactions()
    {
        return $this->hasMany(JobInteraction::class, 'jobs_star_id');
    }

}
