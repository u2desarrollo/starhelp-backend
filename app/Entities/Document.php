<?php

namespace App\Entities;

use Carbon\Carbon;
use App\Entities\Receivable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Document
 *
 * @package App\Entities
 * @version April 25, 2019, 3:23 pm UTC
 */
class Document extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'documents';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['document_date', 'deleted_at'];

    public $fillable = [
        'branchoffice_id',
        'vouchertype_id',
        'consecutive',
        'prefix',
        'warehouse_id',
        'document_date',
        'contact_id',
        'inventory_operation',
        'model_code',
        'thread',
        'base_document',
        'erp_id',
        'cufe',
        'cufe_state',
        'document_state',
        'document_last_state',
        'erp_state',
        'erp_last_state',
        'city',
        'zone',
        'observation',
        'observation_two',
        'affects_prefix_document',
        'affects_number_document',
        'issue_date',
        'term_days',
        'due_date',
        'insurance_value',
        'freight_value',
        'other_value',
        'discount_value',
        'in_progress',
        'pay_condition',
        'send_point',
        'buy_order',
        'seller_id',
        'user_id',
        'viewed',
        'sended',
        'branchoffice_warehouse_id',
        'invoice_link',
        'observation_authorized',
        'max_value_authorized',
        'user_id_authorizes',
        'status_id_authorized',
        'date_authorized',
        'total_value_brut',
        'total_value',
        'operationType_id',
        'ret_fue_prod_vr_limit',
        'ret_fue_prod_percentage',
        'ret_fue_prod_total',
        'ret_fue_serv_vr_limit',
        'ret_fue_serv_percentage',
        'ret_fue_serv_total',
        'ret_fue_total',
        'ret_iva_prod_vr_limit',
        'ret_iva_prod_percentage',
        'ret_iva_prod_total',
        'ret_iva_serv_vr_limit',
        'ret_iva_serv_percentage',
        'ret_iva_serv_total',
        'ret_iva_total',
        'ret_ica_prod_vr_limit',
        'ret_ica_prod_percentage',
        'ret_ica_prod_total',
        'ret_ica_serv_vr_limit',
        'ret_ica_serv_percentage',
        'ret_ica_serv_total',
        'ret_ica_total',
        'incorporated_document',
        'closed',
        'handling_other_parameters_value',
        'qr',
        'model_id',
        'software_siigo',
        'affects_date_document',
        'state_backorder',
        'trm_value',
        'total_value_us',
        'total_value_brut_us',
        'discount_value_us',
        'pending_authorization',

        'user_id_solicit_authorization',
        'date_solicit_authorization',
        'from_service_request',
        'import_number',
        'state_authorization_wallet',
        'ref_update_doc',
        'user_id_last_modification',
        'date_last_modification',

        'ret_fue_base_value',
        'returned_document',
        'ret_iva_base_value',
        'ret_ica_base_value',

        'accounting_debit_value',
        'accounting_credit_value',
        'accounting_user_last_modification',
        'accounting_date_last_modification',
        'center_cost_id',
        'projects_id',

        'ret_fue_serv_base_value',
        'ret_iva_serv_base_value',
        'ret_ica_serv_base_value',
        'edit_retention',
        'canceled',
        'canceled_user_id',
        'canceled_date',
        'edit_base_retention',
        'backorder_quantity',
        'backorder_value',
        'backorder_updated',
        'user_id_authorization',
        'number_stickers',
        'motive_retained',
        'state_ending_id',
        'affects_document_id',
        'origin_document',
        'module',
        'error_wms'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                      => 'integer',
        'invoice_link'            => 'string',
        'branchoffice_id'         => 'integer',
        'vouchertype_id'          => 'integer',
        'consecutive'             => 'integer',
        'prefix'                  => 'string',
        'warehouse_id'            => 'integer',
        'document_date'           => 'datetime:Y-m-d',
        'contact_id'              => 'integer',
        'inventory_operation'     => 'string',
        'model_code'              => 'string',
        'thread'                  => 'integer',
        'base_document'           => 'integer',
        'erp_id'                  => 'string',
        'cufe'                    => 'string',
        'cufe_state'              => 'integer',
        'document_state'          => 'integer',
        'document_last_state'     => 'date',
        'erp_state'               => 'string',
        'erp_last_state'          => 'string',
        'city'                    => 'integer',
        'zone'                    => 'string',
        'observation'             => 'string',
        'observation_two'         => 'string',
        'affects_prefix_document' => 'string',
        'affects_number_document' => 'string',
        'issue_date'              => 'date:Y-m-d',
        'term_days'               => 'integer',
        'due_date'                => 'datetime:Y-m-d',
        'insurance_value'         => 'float',
        'freight_value'           => 'float',
        'other_value'             => 'float',
        'discount_value'          => 'float',
        'in_progress'             => 'boolean',
        'pay_condition'           => 'string',
        'send_point'              => 'string',
        'buy_order'               => 'string',
        'seller_id'               => 'integer',
        'user_id'                 => 'integer',
        'viewed'                  => 'boolean',
        'total_value'             => 'float',
        'closed'                  => 'boolean',
        'number_stickers',
        'error_wms'               => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    protected $appends = [
        'facturaVencida',
        'expiration_days',
        'document_days'
    ];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->document_last_state = date('Y-m-d');

        parent::__construct($attributes);
    }


    /******** ACCESSORS ********/

    /**
     * @param $value
     * @return string
     */
    public function getDocumentDateAttribute($value)
    {
        $date = Carbon::parse($value)->format('Y-m-d H:i:s');

        return $date;
    }

    /******** APPENDS ********/

    /**
     * Calcula la factura vencida
     *
     * @return boolean
     */
    public function getFacturaVencidaAttribute()
    {
        $due_date = Carbon::parse($this->due_date);
        $today = Carbon::now();
        return $today > $due_date;
    }

    /**
     * Calcula los dias de vencimiento de una factura
     *
     * @return Integer
     * @author Kevin Galindo
     */
    public function getExpirationDaysAttribute()
    {
        $dueDate = Carbon::parse($this->due_date);
        $today = Carbon::now();

        if ($today > $dueDate)
            return $dueDate->diffInDays($today);

        return 0;
    }

    public function document_transactions()
    {

        return $this->hasMany(\App\Entities\DocumentTransactions::class);

    }

    /**
     * Calcula los dias de vencimiento de una factura
     *
     * @return Integer
     * @author Kevin Galindo
     */
    public function getDocumentDaysAttribute()
    {
        $documentDate = Carbon::parse($this->document_date);
        $today = Carbon::now();

        return $documentDate->diffInDays($today);
    }

    /******** SCOPES ********/

    /**
     * @param $query
     * @param $field
     * @param $value
     * @return mixed
     */
    public function scopeWhenNotEmpty($query, $field, $value)
    {
        return $query->when(!empty($value), function ($sub_query) use ($value, $field) {
            $sub_query->where($field, $value);
        });
    }


    /******** RELATIONSHIPS ********/

    public function projects()
    {
        return $this->belongsTo(\App\Entities\Projects::class, 'projects_id');
    }

    /**
     * @return BelongsTo
     **/
    public function branchoffice()
    {
        return $this->belongsTo(BranchOffice::class);
    }

    /**
     * @return BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * @return BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(Parameter::class, 'document_state')->select(['id', 'name_parameter', 'code_parameter']);
    }

    public function handlingOtherParametersValue()
    {
        return $this->belongsTo(Parameter::class, 'handling_other_parameters_value')->select(['id', 'name_parameter', 'code_parameter']);
    }

    public function stateBackorder()
    {
        return $this->belongsTo(Parameter::class, 'state_backorder')->withDefault(new Parameter());
    }

    public function stateAuthorizationWalletParam()
    {
        return $this->belongsTo(Parameter::class, 'state_authorization_wallet');
    }

    public function stateEnding()
    {
        return $this->belongsTo(Parameter::class, 'state_ending_id');
    }

    /**
     * @return BelongsTo
     **/

    public function parameter1()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return BelongsTo
     **/

    public function vouchertype()
    {
        return $this->belongsTo(VouchersType::class);
    }

    /**
     * @return BelongsTo
     **/
    public function status_authorized()
    {
        return $this->belongsTo(Parameter::class, 'status_id_authorized');
    }

    /**
     * @return BelongsTo
     **/
    public function warehouse()
    {
        return $this->belongsTo(ContactWarehouse::class);
    }

    /**
     * @return BelongsTo
     **/
    public function branchofficeWarehouse()
    {
        return $this->belongsTo(BranchOfficeWarehouse::class);
    }

    /**
     * @return HasOne
     **/
    public function documentsInformation()
    {
        return $this->hasOne(DocumentInformation::class)->withDefault(new DocumentInformation());
    }

    /**
     * @return HasMany
     **/
    public function documentsProducts()
    {
        return $this->hasMany(DocumentProduct::class, 'document_id');
    }

    /**
     * @return Util\HasManySyncable|Builder
     */
    public function totalTaxes()
    {
        return $this->hasMany(DocumentProduct::class, 'document_id')
            ->select('documents_products.document_id', 'dpt.base_value', 'dpt.tax_percentage', 't.description', 't.increase')
            ->selectRaw('CASE WHEN t.base = 1 THEN \'BRUTO\' ELSE \'IVA\' END AS base')
            ->selectRaw('SUM(unit_value) AS total_for_base')
            ->join('documents_products_taxes AS dpt', 'dpt.document_product_id', '=', 'documents_products.id')
            ->join('taxes AS t', 't.id', '=', 'dpt.tax_id')
            ->orderBy('t.increase', 'ASC')
            ->groupBy('documents_products.document_id', 'dpt.base_value', 'dpt.tax_percentage', 't.description', 't.increase', 't.base');
    }

    /**
     * @return mixed
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'thread');
    }

    /**
     * @return mixed
     */
    public function refUpdateDocument()
    {
        return $this->belongsTo(Document::class, 'ref_update_doc');
    }

    /**
     * @return mixed
     */
    public function documentThread()
    {
        return $this->HasMany(\App\Entities\Document::class, 'thread', 'id');
    }

    /**
     * Esta relacion se creo para obtener
     * las facturas de un pedido
     *
     * @return mixed
     * @author Santiago torres | kevin galindo
     */
    public function invoices()
    {
        return $this->hasMany(Document::class, 'thread');
    }

    public function receivable()
    {
        return $this->hasMany(DocumentsBalance::class);
    }

    public function transactions()
    {
        return $this->hasMany(DocumentTransaction::class);
    }

    public function accounting_transactions()
    {
        return $this->hasMany(\App\Entities\DocumentTransactions::class);
    }


    /**
     * Obtiene el vendedor relacionado con el documento
     *
     * @return BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo(Contact::class, 'seller_id');
    }

    /**
     * Obtiene el estado del documento
     *
     * @return BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Parameter::class, 'document_state');
    }

    /**
     * Obtiene el usuario que creo el documento
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function userLastModification()
    {
        return $this->belongsTo(User::class, 'user_id_last_modification');
    }

    public function userSolicitAuthorization()
    {
        return $this->belongsTo(User::class, 'user_id_solicit_authorization');
    }

    public function userAuthorization()
    {
        return $this->belongsTo(User::class, 'user_id_authorization');
    }

    public function userAuthorizesWallet()
    {
        return $this->belongsTo(User::class, 'user_id_authorizes');
    }

    /**
     * Obtiene los productos del documento base
     *
     * @return Util\HasManySyncable
     */
    public function products()
    {
        return $this->hasMany(DocumentProduct::class, 'document_id');
    }

    /**
     * Relación formas de pago con recibos de caja
     */
    public function paymentWay()
    {
        return $this->hasMany(PaymentWay::class);
    }

    public function contributionInvoicesApp()
    {
        return $this->hasMany(ContributionInvoicesApp::class);
    }

    public function priceList()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function operationType()
    {
        return $this->belongsTo(TypesOperation::class, 'operationType_id');
    }

    public function template()
    {
        return $this->belongsTo(Template::class, 'model_id');
    }

    public function files()
    {
        return $this->hasMany('\App\Entities\DocumentFile', 'document_id', 'id');
    }

    /**
     * @return HasMany
     **/
    public function paymentMethods()
    {
        return $this->hasMany('\App\Entities\DocumentPaymentMethod', 'document_id');
    }

    /**
     * Relación de muchos a uno con Document
     *
     * @return BelongsTo
     */
    public function affectDocument()
    {
        return $this->belongsTo(Document::class, 'affects_document_id');
    }

}
