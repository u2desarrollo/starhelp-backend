<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Receivable
 * @package App\Entities
 * @version May 30, 2019, 9:39 pm UTC
 *
 * @property \App\Entities\Document document
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection PayReceivable
 * @property integer invoice_id
 * @property date year_month
 * @property float original_invoice_value
 * @property float invoice_balance
 * @property string accounting_account
 */
class DocumentsBalance extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'documents_balance';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [

        'document_id',
        'year_month',
        'Account_code',
        'contact_code',
        'document_code',
        'invoice_balance',
        'quota_number',
        'erp_id',
        'update_or_create',
        'document_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'document_id' => 'integer',
        'year_month' => 'string',
        'invoice_balance' => 'float',
        'Account_code' => 'string',
        'erp_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function document()
    {
        return $this->belongsTo(\App\Entities\Document::class);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payReceivables()
    {
        return $this->hasMany(\App\Entities\PayReceivable::class);
    }
}
