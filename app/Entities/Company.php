<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'companies';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'urlapp',
        'active'
    ];

    public function areas()
    {
        return $this->belongsToMany(Area::class);
    }

    public function responsibles()
    {
        return $this->belongsToMany('App\Entities\User', 'companies_responsibles', 'company_id', 'user_id');
    }    

}
