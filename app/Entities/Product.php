<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'code',
        'barcode',
        'description',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'line',
        'category_id',
        'subcategory_id',
        'inventory_management',
        'purchase',
        'sale',
        'kit',
        'state',
        'product_id',
        'condition',
        'has_lot',
        'has_serial',
        'type',
        'high_price',
        'products_risk_id',
        'rotation_type',
        'cooled',
        'reusable',
        'production_formula',
        'tariff_position',
        'export_product',
        'pos',
        'lock_buy',
        'lock_buy_observations',
        'lock_sale',
        'lock_sale_observations',
        'percentage_iva_buy',
        'percentage_tax_consumer_buy',
        'percentage_iva_sale',
        'percentage_tax_consumer_sale',
        'balance_units',
        'balance_value',
        'average_cost',
        'start_date_inventory',
        'modify_description_buy',
        'modify_price_buy',
        'observation_buy',
        'presentation_packaging_buy',
        'presentation_quantity_buy',
        'presentation_unit_buy',
        'presentation_assembled_buy',
        'minimum_quantity_buy',
        'local_buy',
        'calculate_price_buy',
        'automatic_reposition',
        'contact_id',
        'point_of_sale_deployment',
        'amazon_deployment',
        'free_market_deployment',
        'modify_description_sale',
        'modify_price_sale',
        'observation_sale',
        'allow_invoices_sales_zeros',
        'presentation_packaging_sale',
        'presentation_quantity_sale',
        'presentation_unit_sale',
        'presentation_assembled_sale',
        'minimum_quantity_sale',
        'commision_percentage',
        'profit_margin_percentage',
        'entry_third_party',
        'controlled_sale',
        'product_of_continuous_use',
        'quantity_available',
        'long_description',
        'erp_id',
        'cost_center',
        'exp_date_certified',
        'conformity_certificate',
        'reduction_management',
        'high_risk',
        'channel_b2c',
        'channel_b2b',
        'user_id',
        'weight',
        'manage_inventory',
        'provider',
        'alternate_code',
        'certificate_of_conformity',
        'old_product_code',
        'previous_code_system',
        'short_description',

        'alternate_code',
        'rotation_classification',
        'supplyer_code',
        // 'units_per_pallet',
        'high',
        'long',
        'width',

        'exclude_sales_report',
        'parameter_status',
        'update_in_app',
        'update_wsm',
        'date_update_wsm'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                           => 'integer',
        'code'                         => 'string',
        'barcode'                      => 'string',
        'description'                  => 'string',
        'long_description'             => 'string',
        'line_id'                      => 'integer',
        'subline_id'                   => 'integer',
        'brand_id'                     => 'integer',
        'subbrand_id'                  => 'integer',
        'category_id'                  => 'integer',
        'subcategory_id'               => 'integer',
        'inventory_management'         => 'integer',
        'purchase'                     => 'integer',
        'sale'                         => 'integer',
        'kit'                          => 'integer',
        'state'                        => 'integer',
        'product_id'                   => 'integer',
        'condition'                    => 'integer',
        'has_lot'                      => 'integer',
        'has_serial'                   => 'integer',
        'high_price'                   => 'integer',
        'products_risk_id'             => 'integer',
        'rotation_type'                => 'integer',
        'cooled'                       => 'integer',
        'reusable'                     => 'integer',
        'production_formula'           => 'string',
        'tariff_position'              => 'string',
        'export_product'               => 'integer',
        'pos'                          => 'integer',
        'lock_buy'                     => 'integer',
        'lock_buy_observations'        => 'string',
        'lock_sale'                    => 'integer',
        'lock_sale_observations'       => 'string',
        'percentage_iva_buy'           => 'float',
        'percentage_tax_consumer_buy'  => 'float',
        'percentage_iva_sale'          => 'float',
        'percentage_tax_consumer_sale' => 'float',
        'balance_units'                => 'integer',
        'balance_value'                => 'integer',
        'average_cost'                 => 'float',
        'start_date_inventory'         => 'datetime:Y-m-d',
        'modify_description_buy'       => 'integer',
        'modify_price_buy'             => 'integer',
        'observation_buy'              => 'string',
        'presentation_packaging_buy'   => 'string',
        'presentation_quantity_buy'    => 'integer',
        'presentation_unit_buy'        => 'string',
        'presentation_assembled_buy'   => 'string',
        'minimum_quantity_buy'         => 'integer',
        'local_buy'                    => 'integer',
        'calculate_price_buy'          => 'integer',
        'automatic_reposition'         => 'integer',
        'contact_id'                   => 'integer',
        'point_of_sale_deployment'     => 'integer',
        'amazon_deployment'            => 'integer',
        'free_market_deployment'       => 'integer',
        'modify_description_sale'      => 'integer',
        'modify_price_sale'            => 'integer',
        'observation_sale'             => 'string',
        'allow_invoices_sales_zeros'   => 'integer',
        'presentation_packaging_sale'  => 'string',
        'presentation_quantity_sale'   => 'integer',
        'presentation_unit_sale'       => 'string',
        'presentation_assembled_sale'  => 'string',
        'minimum_quantity_sale'        => 'integer',
        'commision_percentage'         => 'float',
        'profit_margin_percentage'     => 'float',
        'entry_third_party'            => 'integer',
        'controlled_sale'              => 'integer',
        'product_of_continuous_use'    => 'integer',
        'quantity_available'           => 'double',
        'cost_center'                  => 'integer',
        'exp_date_certified'           => 'datetime:Y-m-d',
        'conformity_certificate'       => 'string',
        'reduction_management'         => 'integer',
        'high_risk'                    => 'integer',
        'channel_b2c'                  => 'integer',
        'channel_b2b'                  => 'integer',
        'update_wsm'                   => 'integer',
        'date_update_wsm'              => 'datetime:Y-m-d',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code'        => 'required|max:50',
        'description' => 'required|max:200',
        'line_id'     => 'required|exists:lines,id'
    ];

    public function __construct(array $attributes = [])
    {
        $this->state = true;

        parent::__construct($attributes);
    }

    /**
     * @return BelongsTo
     **/
    public function brand()
    {
        return $this->belongsTo(Brand::class)/* ->select(['id', 'image', 'description']) */ ;
    }

    /**
     * @return BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(Categorie::class)/* ->select(['id','description']) */ ;
    }

    /**
     * @return BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * @return BelongsTo
     **/
    public function line()
    {
        return $this->belongsTo(Line::class)->select(['id', 'line_description', 'line_code', 'service', 'inventory_update', 'tipe', 'percentage_utility', 'arancel_percentage']);
    }

    /**
     * @return BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(Parameter::class)->select(['id', 'name_parameter']);
    }

    /**
     * @return BelongsTo
     **/
    public function subbrand()
    {
        return $this->belongsTo(Subbrand::class);
    }

    /**
     * @return BelongsTo
     **/
    public function subcategory()
    {
        return $this->belongsTo(SubCategorie::class)->select(['id', 'description']);
    }

    /**
     * @return BelongsTo
     **/
    public function subline()
    {
        return $this->belongsTo(Subline::class)->select(['id', 'subline_description', 'subline_code']);
    }

    /**
     * @return HasMany
     **/
    public function productsAttachments()
    {
        return $this->hasMany(ProductAttachment::class, 'product_id', 'id')->orderBy('main', 'desc');
    }

    /**
     * @return HasOne
     */
    public function productAttachmentMain()
    {
        return $this->hasOne(ProductAttachment::class, 'product_id', 'id')->where('main', true);
    }

    /**
     * @return HasOne
     **/
    public function productsTechnnicalData()
    {
        return $this->hasOne(ProductTechnnicalData::class, 'product_id')->withDefault(new ProductTechnnicalData());
    }

    public function price()
    {
        return $this->hasOne(Price::class)->withDefault(new Price());
    }

    public function minimum_price()
    {
        return $this->hasOne(Price::class)->withDefault(new Price());
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function relatedProducts()
    {
        return $this->hasMany(RelatedProduct::class, 'product_id');
    }

    public function alternativeProducts()
    {
        return $this->hasMany(AlternativeProduct::class, 'product_id');
    }

    public function criteriaValues()
    {
        return $this->hasMany(CriteriaValue::class, 'relation_id');
    }

    public function documentsProducts()
    {
        return $this->hasMany(DocumentProduct::class, 'product_id');
    }

    public function inventory()
    {
        return $this->hasMany(Inventory::class, 'product_id');
    }

    public function priceContact()
    {
        return $this->hasMany(PriceContact::class, 'product_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'specific_product_id');
    }

    public function data_sheets()
    {
        return $this->hasOne('App\Entities\DataSheetProduct');
    }

    public function productsPromotions()
    {
        return $this->hasMany(ProductPromotion::class, 'product_id');
    }

    /**
     * @return mixed
     */
    public function productHasPromotions()
    {
        return $this->hasMany(ProductsHasPromotions::class, 'product_id');
    }
}
