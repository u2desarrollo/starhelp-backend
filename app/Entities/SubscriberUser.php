<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SubscriberUser extends Model
{

    protected $table = 'subscriber_user';

    protected $fillable = ['user_id', 'subscriber_id'];
}
