<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HistoryCashRegisterSquare
 * @package App\Entities
 */
class HistoryCashRegisterSquare extends Model
{
    use SoftDeletes;

    public $table = "history_cash_register_square";

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'branchoffice_id',
        'user_id',
        'dateFrom',
        'dateTo',
        'note'
    ];

    public static $rules = [
        'branchoffice_id' => 'required|integer|exists:branchoffices,id',
        // 'user_id'         => 'required|integer|exists:users,id',
        'dateFrom'        => 'required|date',
        'dateTo'          => 'required|date',
        'note'            => 'nullable|string',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Entities\User::class);
    }

    public function branchoffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class);
    }
}
