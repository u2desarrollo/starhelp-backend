<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
    protected $fillable = ["user_id", "datable_id", "datable_type"];

    public function datable(){
        return $this->morphTo();
    }
}
