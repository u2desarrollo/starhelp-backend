<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ContributionInvoicesApp extends Model
{
    protected $fillable = [
        'cash_receipts_app_id', 'document_id',
        'total_paid', 'value_discount_prompt_payment', 'invoice_days', 'rete_fte',
        'rete_iva',
        'rete_ica'
    ];
    protected $cast = ['created_at' => 'datetime:Y-m-d'];
    protected $table = 'contribution_invoices_app';


    public function cashReceiptsApp()
    {
        return $this->belongsTo(CashReceiptsApp::class);
    }

    public function document()
    {
        return $this->belongsTo(Document::class);
    }
}
