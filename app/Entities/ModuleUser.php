<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ModuleUser extends Model
{
    protected $table = 'module_user';

    protected $fillable = ['user_id', 'module_id'];
}
