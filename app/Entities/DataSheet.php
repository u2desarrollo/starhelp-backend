<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataSheet extends Model
{
    use SoftDeletes;

    public $table = 'data_sheet';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'internal_name',
        'visible_name',
        'type_value',
        'tooltip',
        'paramtable_id'
    ];

    public function lines()
    {
        return $this->belongsToMany('App\Entities\Line', 'data_sheet_lines', 'data_sheet_id', 'line_id')
                    ->withPivot('required');
    }

    public function type_value_name()
    {
        return $this->belongsTo(Parameter::class, 'type_value');
    }
}
