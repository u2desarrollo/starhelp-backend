<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentInformation extends Model
{
    use SoftDeletes;

    public $table = 'documents_informations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'document_id',
        'mac_address',
        'money',
        'trm',
        'branchoffice_dispatch',
        'delivery_establishment',
        'delivery_place',
        'delivery_date',
        'delivery_days',
        'delivery_observation',
        'home_delivery',
        'domiciliary',
        'protractor',
        'guide_number',
        'registration_tag',
        'order_type',
        'cost_center',
        'bussiness_code',
        'price_list',
        'include_tax',
        'additional_contact',
        'additional_branchoffice',
        'additional_warehouse',
        'since',
        'until',
        'accounting_error',
        'accounting_description',
        'incorporated',
        'reversed',
        'modified',
        'anulled',
        'special_bearings',
        'printed',
        'document_name',
        'date_entry',
        'vouchers_type_prefix',
        'vouchers_type_range_from',
        'vouchers_type_range_up',
        'vouchers_type_resolution',
        'vouchers_type_electronic_bill',
        'additional_contact_id',
        'additional_contact_warehouse_id',
        'invoice_filed',

        'commission_percentage_applies',
        'commission_percentage',
        'document_previous_system'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                      => 'integer',
        'document_id'             => 'integer',
        'mac_address'             => 'string',
        'money'                   => 'integer',
        'trm'                     => 'string',
        'branchoffice_dispatch'   => 'integer',
        'delivery_establishment'  => 'integer',
        'delivery_place'          => 'string',
        'delivery_date'           => 'date',
        'delivery_days'           => 'integer',
        'delivery_observation'    => 'string',
        'home_delivery'           => 'boolean',
        'domiciliary'             => 'integer',
        'protractor'              => 'integer',
        'guide_number'            => 'string',
        'registration_tag'        => 'string',
        'order_type'              => 'integer',
        'cost_center'             => 'integer',
        'bussiness_code'          => 'integer',
        'price_list'              => 'integer',
        'include_tax'             => 'boolean',
        'additional_contact'      => 'integer',
        'additional_branchoffice' => 'integer',
        'additional_warehouse'    => 'integer',
        'since'                   => 'date',
        'until'                   => 'date',
        'incorporated'            => 'boolean',
        'reversed'                => 'boolean',
        'modified'                => 'boolean',
        'anulled'                 => 'boolean',
        'special_bearings'        => 'string',
        'printed'                 => 'boolean',
        'invoice_filed'           => 'boolean',
        'accounting_error'        => 'boolean',
        'accounting_description'  => 'string',
        'document_name'           => 'string',
        'date_entry'              => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected $appends = [
        'tracing'
    ];

    public function getTracingAttribute()
    {
        if (!empty($this->protractor) && !empty($this->guide_number) && !empty($this->contactProtractor) && !empty($this->contactProtractor->web_page)) {
            $url = $this->contactProtractor->web_page;

            $url = str_replace('$guia$', $this->guide_number, $url);

            return $url;
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchoffice()
    {
        return $this->belongsTo(\App\Entities\Branchoffice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'protractor');
    }

    public function additional_contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'additional_contact_id');
    }

    public function additional_contact_warehouse()
    {
        return $this->belongsTo(\App\Entities\ContactWarehouse::class, 'additional_contact_warehouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchofficeWarehouse()
    {
        return $this->belongsTo(\App\Entities\BranchofficeWarehouse::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contactsWarehouse()
    {
        return $this->belongsTo(\App\Entities\ContactsWarehouse::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function document()
    {
        return $this->belongsTo(\App\Entities\Document::class, 'document_id');
    }

    public function contactProtractor()
    {
        return $this->belongsTo(\App\Entities\Contact::class, 'protractor')->with('provider');
    }
}
