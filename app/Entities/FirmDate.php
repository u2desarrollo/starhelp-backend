<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FirmDate extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'firm_date';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'firm_date_general',
        'account_id',
        'firm_date_account',
        'observations',
    ];


    public function account()
    {
        return $this->belongsTo(\App\Entities\Account::class);
    }
}
