<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SalesContactWarehousesProduct extends Model
{
    protected $table = 'sales_contact_warehouses_products';

    protected $fillable = [
        'contact_warehouse_id',
        'product_id',
        'last_sale_date',
        'last_sale_days',
        'last_sale_quantity',
        'last_sale_unit_value',
        'date_one',
        'date_two',
        'date_three',
        'days_sale',
        'suggested'
    ];
}
