<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use OwenIt\Auditing\Contracts\Auditable;

class Promotion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $fillable = [
        'promotion_type_id',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'description',
        'start_date',
        'end_date',
        'minimum_gross_value',
        'only_available',
        'discount',
        'pay_soon',
        'cash_payment',
        'gift_type',
        'specific_product_id',
        'image',
        'minimum_amount',
        'branchoffice_id',
        'description2',
        'title'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['state'];

    public function getStateAttribute()
    {
        $current_date = Carbon::now();
        $start_date = Carbon::parse($this->start_date);
        $end_date = Carbon::parse($this->end_date);

        if (($start_date > $current_date) && ($end_date > $current_date)) {
            return 'Proxima';
        } elseif (($start_date <= $current_date) && ($end_date >= $current_date)) {
            return 'Vigente';
        } elseif (($start_date < $current_date) && ($end_date <= $current_date)) {
            return 'Vencida';
        }
    }




    public function productsPromotions()
    {
        return $this->hasMany(ProductPromotion::class);
    }
    public function productsPromotionsFilter()
    {
        return $this->hasMany(ProductPromotion::class, 'promotion_id');
    }
    public function line()
    {
        return $this->belongsTo(Line::class);
    }
    public function subline()
    {
        return $this->belongsTo(Subline::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    public function subbrand()
    {
        return $this->belongsTo(Subbrand::class);
    }

    public function specificProduct()
    {
        return $this->belongsTo(Product::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function branchoffice()
    {
        return $this->belongsTo(BranchOffice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|HasMany|Builder
     */
    public function totalBrutDocumentsProducts()
    {
        return $this->hasMany(DocumentProduct::class)
            ->select('documents_products.promotion_id')
            ->selectRaw('SUM(CASE WHEN d."operationType_id" = 5 THEN documents_products.total_value_brut ELSE documents_products.total_value_brut * -1 END) as total_value_brut')
            ->join('documents AS d', 'd.id', '=', 'documents_products.document_id')
            ->where('d.in_progress',false)
            ->whereRaw('d."operationType_id" IN (1,5)')
            ->groupBy('documents_products.promotion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|HasMany|Builder
     */
    public function totalInventoryCostDocumentsProducts()
    {
        return $this->hasMany(DocumentProduct::class)
            ->select('documents_products.promotion_id')
            ->selectRaw('SUM(CASE WHEN d."operationType_id" = 5 THEN documents_products.inventory_cost_value ELSE documents_products.inventory_cost_value * -1 END) as inventory_cost_value')
            ->join('documents AS d', 'd.id', '=', 'documents_products.document_id')
            ->where('d.in_progress',false)
            ->whereRaw('d."operationType_id" IN (1,5)')
            ->groupBy('documents_products.promotion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|HasMany|Builder
     */
    public function totalQuantityDocumentsProducts()
    {
        return $this->hasMany(DocumentProduct::class)
            ->select('documents_products.promotion_id')
            ->selectRaw('SUM(CASE WHEN d."operationType_id" = 5 THEN documents_products.quantity ELSE documents_products.quantity * -1 END) as quantity')
            ->join('documents AS d', 'd.id', '=', 'documents_products.document_id')
            ->where('d.in_progress',false)
            ->whereRaw('d."operationType_id" IN (1,5)')
            ->groupBy('documents_products.promotion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|HasMany|Builder
     */
    public function totalApplicationsInvoicesDocumentsProducts()
    {
        return $this->hasMany(DocumentProduct::class)
            ->select('documents_products.promotion_id', 'documents_products.document_id')
            ->selectRaw('COUNT(quantity) AS countApplications')
            ->join('documents AS d', 'd.id', '=', 'documents_products.document_id')
            ->where('d.in_progress',false)
            ->whereRaw('d."operationType_id" = 5')
            ->groupBy('documents_products.promotion_id', 'documents_products.document_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|HasMany|Builder
     */
    public function totalApplicationsDevolutionsDocumentsProducts()
    {
        return $this->hasMany(DocumentProduct::class)
            ->select('documents_products.promotion_id', 'documents_products.document_id')
            ->selectRaw('COUNT(quantity) AS countApplications')
            ->join('documents AS d', 'd.id', '=', 'documents_products.document_id')
            ->where('d.in_progress',false)
            ->whereRaw('d."operationType_id" = 1')
            ->groupBy('documents_products.promotion_id', 'documents_products.document_id');
    }
}
