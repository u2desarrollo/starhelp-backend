<?php

namespace App\Entities;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="ContactType",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="contact_id",
 *          description="contact_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="category_code",
 *          description="category_code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="main_telephone",
 *          description="main_telephone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="secondary_telephone",
 *          description="secondary_telephone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fax",
 *          description="fax",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cell_phone",
 *          description="cell_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="web_page",
 *          description="web_page",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birthdate",
 *          description="birthdate",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="gender",
 *          description="gender",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="stratum",
 *          description="stratum",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="point_sale_code",
 *          description="point_sale_code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="length",
 *          description="length",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seller_code",
 *          description="seller_code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="electronic_invoice_shipping",
 *          description="electronic_invoice_shipping",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="b2b_portal_access",
 *          description="b2b_portal_access",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="priority",
 *          description="priority",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_zone",
 *          description="code_zone",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_price_list",
 *          description="code_price_list",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="percentage_discount",
 *          description="percentage_discount",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="credit_quota_customer",
 *          description="credit_quota_customer",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="expiration_date_credit_quota",
 *          description="expiration_date_credit_quota",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="days_soon_payment_1",
 *          description="days_soon_payment_1",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="percentage_soon_payment_1",
 *          description="percentage_soon_payment_1",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="days_soon_payment_2",
 *          description="days_soon_payment_2",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="percentage_soon_payment_2",
 *          description="percentage_soon_payment_2",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="days_soon_payment_3",
 *          description="days_soon_payment_3",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="percentage_soon_payment3",
 *          description="percentage_soon_payment3",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="contract_number",
 *          description="contract_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_purchase",
 *          description="last_purchase",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="blocked",
 *          description="blocked",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="observation",
 *          description="observation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retention_source",
 *          description="retention_source",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="retention_iva",
 *          description="retention_iva",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="retention_ica",
 *          description="retention_ica",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="date_financial_statement",
 *          description="date_financial_statement",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="assets",
 *          description="assets",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="liabilities",
 *          description="liabilities",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="heritage",
 *          description="heritage",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="seller",
 *          description="seller",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact",
 *          description="contact",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="percentage_commision",
 *          description="percentage_commision",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="replacement_days",
 *          description="replacement_days",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="electronic_order",
 *          description="electronic_order",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="update_shopping_list",
 *          description="update_shopping_list",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="calculate_sale_prices",
 *          description="calculate_sale_prices",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="percentage_retention_source",
 *          description="percentage_retention_source",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="percentage_retention_iva",
 *          description="percentage_retention_iva",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="percentage_retention_iva_copy1",
 *          description="percentage_retention_iva_copy1",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="code_ica",
 *          description="code_ica",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_bank_code",
 *          description="payment_bank_code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="account_type",
 *          description="account_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="account_number",
 *          description="account_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cost_center",
 *          description="cost_center",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="position",
 *          description="position",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="boolean"
 *      )
 * )
 */
class ContactType extends Model
{

    public $table = 'contacts_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'contact_id',
        'category_code',
        'address',
        'main_telephone',
        'secondary_telephone',
        'fax',
        'cell_phone',
        'email',
        'web_page',
        'birthdate',
        'gender',
        'stratum',
        'point_sale_code',
        'latitude',
        'length',
        'seller_code',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority',
        'code_zone',
        'code_price_list',
        'percentage_discount',
        'credit_quota_customer',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment3',
        'contract_number',
        'last_purchase',
        'blocked',
        'observation',
        'image',
        'retention_source',
        'retention_iva',
        'retention_ica',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'seller',
        'contact',
        'percentage_commision',
        'replacement_days',
        'electronic_order',
        'update_shopping_list',
        'calculate_sale_prices',
        'percentage_retention_source',
        'percentage_retention_iva',
        'percentage_retention_iva_copy1',
        'code_ica',
        'payment_bank_code',
        'account_type',
        'account_number',
        'cost_center',
        'position',
        'state'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'category_code' => 'integer',
        'address' => 'string',
        'main_telephone' => 'string',
        'secondary_telephone' => 'string',
        'fax' => 'string',
        'cell_phone' => 'string',
        'email' => 'string',
        'web_page' => 'string',
        'birthdate' => 'date',
        'gender' => 'integer',
        'stratum' => 'string',
        'point_sale_code' => 'integer',
        'latitude' => 'string',
        'length' => 'string',
        'seller_code' => 'integer',
        'electronic_invoice_shipping' => 'boolean',
        'b2b_portal_access' => 'boolean',
        'priority' => 'integer',
        'code_zone' => 'integer',
        'code_price_list' => 'integer',
        'percentage_discount' => 'float',
        'credit_quota_customer' => 'integer',
        'expiration_date_credit_quota' => 'date',
        'days_soon_payment_1' => 'integer',
        'percentage_soon_payment_1' => 'float',
        'days_soon_payment_2' => 'integer',
        'percentage_soon_payment_2' => 'float',
        'days_soon_payment_3' => 'integer',
        'percentage_soon_payment3' => 'float',
        'contract_number' => 'string',
        'last_purchase' => 'date',
        'blocked' => 'boolean',
        'observation' => 'string',
        'image' => 'string',
        'retention_source' => 'boolean',
        'retention_iva' => 'boolean',
        'retention_ica' => 'boolean',
        'date_financial_statement' => 'date',
        'assets' => 'integer',
        'liabilities' => 'integer',
        'heritage' => 'integer',
        'seller' => 'string',
        'contact' => 'string',
        'percentage_commision' => 'float',
        'replacement_days' => 'integer',
        'electronic_order' => 'boolean',
        'update_shopping_list' => 'boolean',
        'calculate_sale_prices' => 'boolean',
        'percentage_retention_source' => 'float',
        'percentage_retention_iva' => 'float',
        'percentage_retention_iva_copy1' => 'float',
        'code_ica' => 'integer',
        'payment_bank_code' => 'integer',
        'account_type' => 'integer',
        'account_number' => 'string',
        'cost_center' => 'integer',
        'position' => 'integer',
        'state' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function contactsWarehouses()
    {
        return $this->hasMany(\App\Entities\ContactsWarehouse::class);
    }
}
