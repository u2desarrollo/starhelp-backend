<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequest extends Model
{
    use SoftDeletes;

    public $table = 'service_requests';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'service_requests_types_id',
        'product_id',
        'contact_id',
        'seller_id',
        'date',
        'lot',
        'due_date',
        'observation',
        'unit_value',
        'branchoffice_id',
        'number_batteries',
        'contacts_warehouse_id',
        'actual_state_id',
    ];

    /**
     * 
     * Relacion 1 a 1 con el tipo de solicitud
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function type()
    {
        return $this->belongsTo('App\Entities\ServiceRequestType', 'service_requests_types_id');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * historico de estados.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function statusHistory()
    {
        return $this->hasMany('App\Entities\ServiceRequestStatusHistory', 'service_requests_id');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * registro del cliente
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function contact()
    {
        return $this->belongsTo('\App\Entities\Contact');
    }

    public function contactsWarehouse()
    {
        return $this->belongsTo('\App\Entities\ContactWarehouse', 'contacts_warehouse_id');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * registro del vendedor
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function seller()
    {
        return $this->belongsTo('\App\Entities\Contact');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * registro del cliente
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function product()
    {
        return $this->belongsTo('\App\Entities\Product');
    }

}
