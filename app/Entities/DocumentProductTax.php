<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentProductTax extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
	use SoftDeletes;

	public $table = 'documents_products_taxes';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'document_product_id',
		'tax_id',
		'base_value',
		'tax_percentage',
		'value'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function documentProduct() {
		return $this->belongsTo(DocumentProduct::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tax() {
		return $this->belongsTo(Tax::class);
	}
}
