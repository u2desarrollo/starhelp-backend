<?php

namespace App\Entities;

use Eloquent as Model;
use App\Entities\BranchOffice;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{

    use SoftDeletes;

    public $table = 'subscribers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'license_id',
        'identification',
        'check_digit',
        'identification_type',
        'name',
        'short_name',
        'email',
        'web_page',
        'class_person',
        'taxpayer_type',
        'tax_regimen',
        'id_legal_representative',
        'name_legal_representative',
        'company_type',
        'logo',
        'state',
        'firmDate',
        'vouchertype_id',
        'manager_wms',
        'document_id',
        'last_date_update_balances'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                        => 'integer',
        'license_id'                => 'integer',
        'identification'            => 'string',
        'check_digit'               => 'integer',
        'identification_type'       => 'integer',
        'name'                      => 'string',
        'short_name'                => 'string',
        'email'                     => 'string',
        'web_page'                  => 'string',
        'class_person'              => 'integer',
        'taxpayer_type'             => 'integer',
        'tax_regimen'               => 'integer',
        'id_legal_representative'   => 'string',
        'name_legal_representative' => 'string',
        'company_type'              => 'integer',
        'logo'                      => 'string',
        'state'                     => 'integer',
        'manager_wms'               => 'integer',
        'document_id'               => 'integer'
    ];

    public function getLogoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function license()
    {
        return $this->belongsTo(\App\Entities\License::class);
    }

    public function vouchertype()
    {
        return $this->belongsTo(VouchersType::class);
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function configuration()
    {
        return $this->hasOne(\App\Entities\Configuration::class)->withDefault(new Configuration());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter1()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter3()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter4()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    public function branchOffice()
    {
        return $this->hasOne(BranchOffice::class, 'subscriber_id', 'id')->with('city');
    }

}
