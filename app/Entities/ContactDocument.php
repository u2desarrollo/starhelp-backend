<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ContactDocument
 * @package App\Entities
 * @version January 15, 2019, 9:27 pm UTC
 *
 * @property \App\Entities\Contact contact
 * @property \App\Entities\Parameter parameter
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property integer contact_id
 * @property integer document_type_id
 * @property string path
 * @property string name
 */
class ContactDocument extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'contacts_documents';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'document_type_id',
        'path',
        'name',
        'observation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'document_type_id' => 'integer',
        'path' => 'string',
        'name' => 'string',
        'observation' => 'string',
        'created_at' => 'datetime:Y-m-d'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesCreate = [
        'contact_id' => 'required|integer',
        'document_type_id' => 'required|integer',
        'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,png,jpg,jpeg|max:10240',
        'name' => 'required|string|max:60',
        'observation' => 'max:255'
    ];

    public static $rulesUpdate = [
        'document_type_id' => 'required|integer',
        'path' => 'string',
        'name' => 'required|string|max:60',
        'observation' => 'max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter()
    {
        return $this->hasOne(\App\Entities\Parameter::class, 'id', 'document_type_id');
    }
}
