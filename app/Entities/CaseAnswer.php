<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseAnswer extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'cases_answers';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'case_id',
        'user_id',
        'content',
        'private',
        'solution'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function case()
    {
        return $this->belongsTo(CaseTable::class);
    }
}
