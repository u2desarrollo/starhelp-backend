<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequestState extends Model
{
    use SoftDeletes;

    public $table = 'service_requests_state';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'service_requests_types_id',
        'model_id',
        'service_requests_state_id',
        'name',
        'order',
        'view_customer',
        'view_seller',
        'branchoffice_warehouse_id',
        'join_name'
    ];

    /**
     * 
     * Relacion 1 a 1 con el tipo de solicitud
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function type()
    {
        return $this->belongsTo('App\Entities\ServiceRequestType', 'service_requests_types_id');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * historico de estados.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function statusHistory()
    {
        return $this->hasMany('App\Entities\ServiceRequestStatusHistory');
    }

    /**
     * 
     * Relacion de 1 a muchos con el 
     * historico de estados.
     * 
     * @author Kevin Galindo | Santiago Torres
     */
    public function template()
    {
        return $this->belongsTo('App\Entities\Template', 'model_id');
    }
}
