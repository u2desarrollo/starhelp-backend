<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioShipment extends Model
{

    use SoftDeletes;
    protected $table = 'portfolio_shipments';


    protected $fillable = [
        'expiration_days_from',
        'expiration_days_until',
        'frecuency',
        'time',
        'pre_legal_charge'
    ];
}
