<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BalancesForAccount extends Model
{
    protected $table = 'balances_for_accounts';

    protected $fillable = [
        'year_month',
        'account_id',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public static $columns = [
        'year_month',
        'account_id',
        'original_balance',
        'initial_balance_month',
        'debits_month',
        'credits_month',
        'final_balance',
    ];

    public function __construct(array $attributes = [])
    {
        $this->attributes['initial_balance_month'] = 0;
        $this->attributes['original_balance'] = 0;

        parent::__construct($attributes);
    }

    public function Account()
    {
        return $this->belongsTo(\App\Entities\Account::class);
    }

    public function BalancesForContact()
    {
        return $this->belongsTo(\App\Entities\BalancesForContact::class, 'contact_id');
    }
}
