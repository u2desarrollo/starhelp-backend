<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixAssetMonthlyDepreciation extends Model
{
    use SoftDeletes;
    public $timestamps = true;
	public $table = 'fix_asset_monthly_depreciation';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'fix_asset_id',
        'year_month',
        'value_additions',
        'historical_cost',
        'balance_to_depreciated',
        'depreciate_value_month',
        'accumulated_depreciation_value',
        'months_accumulated',
        'initial_recording',
    ];

    public function fixAsset()
	{
		return $this->belongsTo(\App\Entities\FixAsset::class);
    }
}
