<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentProduct extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'documents_products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'document_id',
        'product_id',
        'lot',
        'serial_number',
        'seller_id',
        'technician',
        'lot_due_date',
        'line_id',
        'subline_id',
        'brand_id',
        'subbrand_id',
        'description',
        'observation',
        'applies_inventory',
        'quantity',
        'bonus_quantity',
        'missing_quantity',
        'unit_list_value',
        'bonus_value',
        'unit_value_before_taxes',
        'inventory_adjustment',
        'discount_value',
        'inventory_value',
        'freight_value',
        'insurance_value',
        'unit_warehouse_balance',
        'value_warehouse_balance',
        'unit_balance',
        'value_balance',
        'state',
		'warehouse_id',
		'erp_id',
		'original_quantity',
        'unit_value_after_taxes',
        'promotion_id',
        'is_benefit',
        'physical_unit',
        'unit_list_value',
        'total_value_brut',
        'total_value',
        'inventory_cost_value',
        'stock_after_operation',
        'stock_value_after_operation',
        'stock_company_after_operation',
        'stock_company_value_after_operation',
        'operationType_id',
        'type_difference',
        'difference_quantity',
        'difference_value',
        'manage_inventory',
        'cost_ref',
        'fixed_cost',
        'product_repeat_number',
        'total_value_us',
        'total_value_brut_us',
        'discount_value_us',
        'branchoffice_warehouse_id',
        'unit_value_list',
        'reconstruction_date',
        'user_reconstruction',
        'stock_before_reconstruction',
        'backorder_quantity',
        'backorder_value',
        'account_id',
        'quantity_wms',
        'lot_wms',
        'base_document_product_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'document_id' => 'integer',
		'product_id' => 'integer',
		'lot' => 'string',
		'serial_number' => 'string',
		'seller_id' => 'integer',
		'technician' => 'integer',
		'lot_due_date' => 'date',
		'line_id' => 'integer',
		'subline_id' => 'integer',
		'brand_id' => 'integer',
		'subbrand_id' => 'integer',
		'description' => 'string',
		'observation' => 'string',
		'applies_inventory' => 'boolean',
		'quantity' => 'integer',
		'bonus_quantity' => 'integer',
		'missing_quantity' => 'integer',
		'unit_list_value' => 'float',
		'bonus_value' => 'float',
		'unit_value' => 'float',
		'inventory_adjustment' => 'boolean',
		'consumption_tax_percentage' => 'float',
		'discount_value' => 'float',
		'inventory_value' => 'float',
		'freight_value' => 'float',
		'insurance_value' => 'float',
		'unit_warehouse_balance' => 'integer',
		'value_warehouse_balance' => 'float',
		'unit_balance' => 'integer',
		'value_balance' => 'float',
        'state' => 'integer',
        'warehouse_id' => 'integer',
        'erp_id' => 'integer',
        'original_quantity' => 'integer',
        'is_benefit' => 'boolean',
        'unit_value_before_taxes'=>'float',
        'quantity_wms'=>'integer',
    ];

	protected $appends = [
	    'discount_value_unit',
	    'discount_value_unit_us',
        'total_value_iva',
        'base_unit_value_before_taxes',
        'base_unit_value',
        'base_quantity',
        'inventory_cost_value_unit'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return BelongsTo
     **/
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    /**
     * @return BelongsTo
     **/
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    /**
     * @return BelongsTo
     **/
    public function line()
    {
        return $this->belongsTo(Line::class);
    }

    /**
     * @return BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return BelongsTo
     **/
    public function parameter()
    {
        return $this->belongsTo(Parameter::class, 'state')->select(['id', 'name_parameter', 'code_parameter']);
    }

    /**
     * @return BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * @return BelongsTo
     **/
    public function subbrand()
    {
        return $this->belongsTo(Subbrand::class);
    }

    /**
     * @return BelongsTo
     **/
    public function subline()
    {
        return $this->belongsTo(Subline::class);
    }

    /**
     * @return BelongsTo
     **/
    public function seller()
    {
        return $this->belongsTo(Contact::class, 'seller_id');
    }

    public function documentProductTaxes()
    {
        return $this->hasMany(DocumentProductTax::class);
    }

	/** Relación documents_products con documents_products_taxes */
	public function documentsProductsTaxes() {
		return $this->hasMany(DocumentProductTax::class);
	}

	/** Relación documents_products con promotions */
	public function promotion() {
		return $this->belongsTo(Promotion::class);
    }

    /**
     * @return BelongsTo
     **/
    public function branchofficeWarehouse()
	{
		return $this->belongsTo(BranchOfficeWarehouse::class);
	}

    /**
     * @return BelongsTo
     */
    public function operationType()
    {
        return $this->belongsTo(TypesOperation::class, 'operationType_id');
    }

    public function baseDocumentProduct()
    {
        return $this->belongsTo(DocumentProduct::class, 'base_document_product_id');
    }

    /**
     *
     */
    public function getDiscountValueUnitAttribute()
    {
        if ($this->discount_value > 0 && $this->quantity > 0) {
            return $this->discount_value / $this->quantity;
        }

        return 0;
    }

    /**
     *
     */
    public function getDiscountValueUnitUsAttribute()
    {
        if ($this->discount_value_us > 0 && $this->quantity > 0) {
            return $this->discount_value_us / $this->original_quantity;
        }

        return 0;
    }

    /**
     *
     */
    public function getInventoryCostValueUnitAttribute()
    {
        if (((int)$this->inventory_cost_value) > 0 && ((int)$this->quantity) > 0) {
            return $this->inventory_cost_value / $this->quantity;
        }

        return 0;
    }

    /**
     *
     */
    public function getTotalValueIvaAttribute()
    {
        $document_product_taxes = $this->documentProductTaxes;

        foreach ($document_product_taxes as $document_product_tax) {
            if ($document_product_tax->tax_id == 1) {
                return $document_product_tax->value;
            }
        }

        return null;
    }

    public function getBaseQuantityAttribute()
    {
        if ($this->id != $this->thread && !empty($this->thread)) {
            foreach ($this->document->documentsProducts as $document_product) {
                foreach ($this->document->document->documentsProducts as $base_document_product) {
                    if ($document_product->product_id == $base_document_product->product_id && $document_product->product_repeat_number == $base_document_product->product_repeat_number) {
                        return $base_document_product->quantity;
                    }
                }
            }
        }else{
            return $this->quantity;
        }
    }

    public function getBaseUnitValue()
    {
        if ($this->id != $this->thread && !empty($this->thread)) {
            foreach ($this->document->documentsProducts as $document_product) {
                foreach ($this->document->document->documentsProducts as $base_document_product) {
                    if ($document_product->product_id == $base_document_product->product_id && $document_product->product_repeat_number == $base_document_product->product_repeat_number) {
                        return round($base_document_product->unit_value_before_taxes);
                    }
                }
            }
        }else{
            return $this->unit_value_before_taxes;
        }
    }

    public function getBaseUnitValueBeforeTaxesAttribute()
    {
        return $this->getBaseUnitValue();
    }

    public function getBaseUnitValueAttribute()
    {
        return $this->getBaseUnitValue();
    }
}
