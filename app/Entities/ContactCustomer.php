<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ContactCustomer
 * @package App\Entities
 * @version January 15, 2019, 9:26 pm UTC
 */
class ContactCustomer extends EloquentModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $touches = ['contact'];

    public $table = 'contacts_customers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];



    public $fillable = [
        'contact_id',
        'category_id',
        'seller_id_1',
        'seller_id_2',
        'callcenter_advisor_id',
        'electronic_invoice_shipping',
        'b2b_portal_access',
        'priority',
        'point_of_sale_id',
        'price_list_id',
        'zone_id',
        'percentage_discount',
        'credit_quota',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment_3',
        'last_purchase',
        'blocked',
        'observation',
        'logo',
        'retention_source',
        'retention_iva',
        'retention_ica',
        'date_financial_statement',
        'assets',
        'liabilities',
        'heritage',
        'code_ica_id',
        'aut_rep_cent_riesgo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'category_id' => 'integer',
        'seller_id_1' => 'integer',
        'seller_id_2' => 'integer',
        'callcenter_advisor_id' => 'integer',
        'electronic_invoice_shipping' => 'integer',
        'b2b_portal_access' => 'integer',
        'priority' => 'integer',
        'point_of_sale_id' => 'integer',
        'price_list_id' => 'integer',
        'zone_id' => 'integer',
        'percentage_discount' => 'float',
        'credit_quota' => 'integer',
        'expiration_date_credit_quota' => 'datetime:Y-m-d',
        'days_soon_payment_1' => 'integer',
        'percentage_soon_payment_1' => 'float',
        'days_soon_payment_2' => 'integer',
        'percentage_soon_payment_2' => 'float',
        'days_soon_payment_3' => 'integer',
        'percentage_soon_payment_3' => 'float',
        'last_purchase' => 'datetime:Y-m-d',
        'blocked' => 'integer',
        'observation' => 'string',
        'logo' => 'string',
        'retention_source' => 'integer',
        'retention_iva' => 'integer',
        'retention_ica' => 'integer',
        'date_financial_statement' => 'datetime:Y-m-d',
        'assets' => 'integer',
        'liabilities' => 'integer',
        'heritage' => 'integer',
        'code_ica_id' => 'integer',
        'aut_rep_cent_riesgo' => 'integer'
    ];

    public function getLogoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/contacts/default.png';
        }
        return '/storage/' . $value;
    }


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter3()
    {
        return $this->belongsTo(\App\Entities\Parameter::class);
    }

}
