<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LineTax extends Model
{
	use SoftDeletes;

	public $table = 'lines_taxes';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'line_id',
		'tax_id',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'line_id' => 'integer',
		'tax_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	public function line() {
		return $this->belongsTo(\App\Entities\Line::class);
	}

	public function tax() {
		return $this->belongsTo(\App\Entities\Tax::class);
	}
}
