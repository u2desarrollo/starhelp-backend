<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactWarehouseLine extends Model
{
	public $table = 'contacts_warehouses_lines';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'contact_warehouse_id',
		'line_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'contact_warehouse_id' => 'integer',
		'line_id' => 'integer'
	];

	public function line(){
        return $this->belongsTo(\App\Entities\Line::class);
    }

}