<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TypesOperation
 * @package App\Entities
 * @version August 5, 2019, 3:03 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection
 * @property string code
 * @property string description
 * @property string oper_inv
 * @property string affectation
 * @property string inventory_cost
 * @property string iva_operation
 * @property string control_existence_outputs
 * @property string sales_price_control
 * @property string control_price_list
 */
class TypesOperation extends Model
{
    use SoftDeletes;

    public $table = 'types_operation';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'description',
        'affectation',
        'inventory_cost',
        'inventory_movement',
        'parameter_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'description' => 'string',
        'affectation' => 'integer',
        'inventory_cost' => 'integer',
        'inventory_movement'=>'integer',
        'parameter_id'=>'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }


}
