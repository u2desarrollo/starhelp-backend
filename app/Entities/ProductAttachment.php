<?php

namespace App\Entities;

use Eloquent as Model;
use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductAttachment
 *
 * @package App\Entities
 */
class ProductAttachment extends BaseModel
{
    use SoftDeletes;

    public $table = 'products_attachments';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'product_id',
        'description',
        'url',
        'type',
        'main'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected $appends = [
        'src',
        'thumb'
    ];

    public function getSrcAttribute()
    {
        return '/storage/products/web/' . $this->url;
    }

    public function getThumbAttribute()
    {
        return '/storage/products/app/' . $this->url;
    }

    public function getMainAttribute($value)
    {
        if (is_null($value)){
            return false;
        }

        return $value;
    }

    /**
     * @return BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Entities\Product::class, 'product_id');
    }
}
