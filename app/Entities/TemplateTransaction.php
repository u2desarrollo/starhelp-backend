<?php

namespace App\Entities;

use App\Entities\Line;
use App\Entities\User;
use Eloquent as Model;
use App\Entities\Contact;
use App\Entities\VouchersType;
use App\Entities\TypesOperation;
use App\Entities\BranchOfficeWarehouse;
use App\Entities\BranchOffice;
use App\Entities\TemplateUser;
use App\Entities\Template;
use App\Entities\Account;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TemplateTransaction extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    public $table = 'template_transactions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'template_id',
        'order',
        'description',
        'account_aux_id',
        'account_root_id',
        'db_cr',
        'cap_contact',
        'default_contact_id',
        'create_contact',
        'default_detail',
        'square_match',
        'doc_bank'
    ];

    //Relaciones
    public function template()
    {
        return $this->belongsTo(Template::class, 'template_id');
    }

    public function accountAux()
    {
        return $this->belongsTo(Account::class, 'account_aux_id');
    }

    public function accountRoot()
    {
        return $this->belongsTo(Account::class, 'account_root_id');
    }

    public function defaultContact()
    {
        return $this->belongsTo(Contact::class, 'default_contact_id');
    }

}
