<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PriceContact
 * @package App\Entities
 * @version August 13, 2019, 5:54 pm UTC
 *
 * @property \App\Entities\Product product
 * @property \App\Entities\Branchoffice branchoffice
 * @property \App\Entities\Contact contact
 * @property \App\Entities\User user
 * @property \Illuminate\Database\Eloquent\Collection branchoffices
 * @property \Illuminate\Database\Eloquent\Collection contactsDocuments
 * @property integer product_id
 * @property integer branchoffice_id
 * @property integer contact_id
 * @property float price
 * @property float previous_price
 * @property integer user_id
 */
class PriceContact extends Model
{
    use SoftDeletes;

    public $table = 'price_contacts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'branchoffice_id',
        'contact_id',
        'price',
        'previous_price',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'branchoffice_id' => 'integer',
        'contact_id' => 'integer',
        'price' => 'float',
        'previous_price' => 'float',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Entities\Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function branchOffice()
    {
        return $this->belongsTo(\App\Entities\BranchOffice::class,'branchoffice_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Entities\User::class);
    }
}
