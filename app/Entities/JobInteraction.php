<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobInteraction extends Model
{
    use SoftDeletes;

	public $table = 'jobs_interactions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'jobs_star_id',
        'user_id',
        'description'
    ];

    public static $rules = [
        'description' => 'required|string'
    ];

    /**
     * Get the JobsStar that owns the JobsInteraction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobStar()
    {
        return $this->belongsTo(JobStar::class, 'jobs_star_id');
    }

    /**
     * Get the User that owns the JobsInteraction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get all of the files for the JobInteraction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(JobInteractionDoc::class, 'jobs_interactions_id');
    }

}
