<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ContactProvider
 * @package App\Entities
 * @version January 15, 2019, 9:27 pm UTC
 */
class ContactProvider extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $touches = ['contact'];

    public $table = 'contacts_providers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'category_id',
        'seller_id',
        'percentage_commision',
        'contract_number',
        'price_list_id',
        'priority',
        'point_of_sale_id',
        'percentage_discount',
        'credit_quota',
        'expiration_date_credit_quota',
        'days_soon_payment_1',
        'percentage_soon_payment_1',
        'days_soon_payment_2',
        'percentage_soon_payment_2',
        'days_soon_payment_3',
        'percentage_soon_payment_3',
        'replacement_days',
        'electronic_order',
        'update_shopping_list',
        'calculate_sale_prices',
        'last_purchase',
        'observation',
        'blocked',
        'logo',
        'retention_source',
        'percentage_retention_source',
        'retention_iva',
        'percentage_retention_iva',
        'retention_ica',
        'percentage_retention_ica',
        'code_ica_id',
        'payment_bank_id',
        'account_type',
        'account_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'category_id' => 'integer',
        'seller_id' => 'integer',
        'percentage_commision' => 'float',
        'contract_number' => 'string',
        'price_list_id' => 'integer',
        'priority' => 'integer',
        'point_of_sale_id' => 'integer',
        'percentage_discount' => 'float',
        'credit_quota' => 'integer',
        'expiration_date_credit_quota' => 'datetime:Y-m-d',
        'days_soon_payment_1' => 'integer',
        'percentage_soon_payment_1' => 'float',
        'days_soon_payment_2' => 'integer',
        'percentage_soon_payment_2' => 'float',
        'days_soon_payment_3' => 'integer',
        'percentage_soon_payment_3' => 'float',
        'replacement_days' => 'integer',
        'electronic_order' => 'integer',
        'update_shopping_list' => 'integer',
        'calculate_sale_prices' => 'integer',
        'last_purchase' => 'datetime:Y-m-d',
        'observation' => 'string',
        'blocked' => 'integer',
        'logo' => 'string',
        'retention_source' => 'integer',
        'percentage_retention_source' => 'float',
        'retention_iva' => 'integer',
        'percentage_retention_iva' => 'float',
        'retention_ica' => 'integer',
        'percentage_retention_ica' => 'float',
        'code_ica_id' => 'integer',
        'payment_bank_id' => 'integer',
        'account_type' => 'integer',
        'account_number' => 'string'
    ];

    public function getLogoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '/storage/contacts/default.png';
        }
        return '/storage/' . $value;
    }


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parameter2()
    {
        return $this->belongsTo(Parameter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(\App\Entities\Contact::class);
    }
}
