<?php

namespace App\Entities;

use Eloquent as Model;

class Configuration extends Model {

    public $table = 'configurations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'subscriber_id',
        'product_digits',
        'seller_management',
        'brand_management',
        'line_management',
        'category_management',
        'line_description',
        'subline_description',
        'category_description',
        'subcategory_description',
        'type_description',
        'recalculate_sales_price',
        'percentage_minimum_variation',
        'rounding_factor',
        'type_voucher_inventory',
        'account_entry',
        'account_exit',
        'consecutive_product_code',
        'consecutive_transfer',
        'consecutive_sales_orders',
        'consecutive_purchase_orders',
        'folios_electronic_invoice',
        'last_date_close',
        'firm_date',
        'base_retention_source_products',
        'percentage_retention_source_products',
        'base_retention_source_services',
        'percentage_retention_source_services',
        'percentage_reteiva',
        'code_activity_ica',
        'code_ciiu',
        'percentage_reteica',
        'bill_to_pay',
        'receivable',
        'account_heritage_utility',
        'account_heritage_lost',
        'account_pyg_result_exercise',
        'monthly_utility_transfer_or_lost',
        'annual_utility_transfer_or_lost',
        'type_voucher_result_exercise',
        'first_logo',
    ];

    public function getFirstLogoAttribute($value)
    {
        if ($value == '' || $value == null) {
            return '';
        }
        return '/storage/' . $value;
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subscriber_id' => 'integer',
        'product_digits' => 'integer',
        'seller_management' => 'integer',
        'brand_management' => 'integer',
        'line_management' => 'integer',
        'category_management' => 'integer',
        'line_description' => 'string',
        'subline_description' => 'string',
        'category_description' => 'string',
        'subcategory_description' => 'string',
        'type_description' => 'string',
        'recalculate_sales_price' => 'integer',
        'percentage_minimum_variation' => 'float',
        'rounding_factor' => 'float',
        'type_voucher_inventory' => 'integer',
        'account_entry' => 'integer',
        'account_exit' => 'integer',
        'consecutive_product_code' => 'integer',
        'consecutive_transfer' => 'integer',
        'consecutive_sales_orders' => 'integer',
        'consecutive_purchase_orders' => 'integer',
        'folios_electronic_invoice' => 'integer',
        'last_date_close' => 'datetime:Y-m-d',
        'firm_date' => 'datetime:Y-m-d',
        'base_retention_source_products' => 'integer',
        'percentage_retention_source_products' => 'float',
        'base_retention_source_services' => 'integer',
        'percentage_retention_source_services' => 'float',
        'percentage_reteiva' => 'float',
        'code_activity_ica' => 'string',
        'code_ciiu' => 'string',
        'percentage_reteica' => 'float',
        'bill_to_pay' => 'integer',
        'receivable' => 'integer',
        'account_heritage_utility' => 'integer',
        'account_heritage_lost' => 'integer',
        'account_pyg_result_exercise' => 'integer',
        'monthly_utility_transfer_or_lost' => 'integer',
        'annual_utility_transfer_or_lost' => 'integer',
        'type_voucher_result_exercise' => 'integer',
        'first_logo' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subscriber()
    {
        return $this->belongsTo(\App\Entities\Subscriber::class);
    }
}
