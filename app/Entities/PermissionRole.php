<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
   protected  $table='permission_role';
}
