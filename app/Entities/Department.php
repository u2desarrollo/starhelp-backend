<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Department extends Model
{

    public $table = 'departments';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'department' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return HasMany
     **/
    public function cities()
    {
        return $this->hasMany(\App\Entities\City::class);
    }
}
