<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';

    protected $fillable = ['role_id', 'user_id'];

    /**
     * Relación con Permission
     */
    public function getPermissions()
    {
        return $this->belongsToMany(Permission::class); // OJO - No olvidar importar la clase
    }

}
