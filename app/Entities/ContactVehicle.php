<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ContactVehicle proporciona la implementación del modelo base para la tabla contact_vehicles.
 * @property array $attributes representa una fila en la tabla de la base de datos.
 * @package App\Entities
 * @author Jhon García
 */
class ContactVehicle extends Model
{
    protected $table = 'contact_vehicles';

    protected $fillable = [
        'vehicle_type_id',
        'contact_id',
        'year',
        'license_plate',
        'serial_number',
        'average_daily_kilometers',
        'width',
        'rin',
        'profile',
        'observations'
    ];

    protected $with = [
        'vehicleType'
    ];

    /**
     * @param $value
     * @return string
     */
    public function getLicensePlateAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * @param $value
     */
    public function setLicensePlateAttribute($value)
    {
        $this->attributes['license_plate'] = strtoupper($value);
    }

    /**
     * Relación de muchos a uno con Tipos de vehículo (VehicleType)
     *
     * @return BelongsTo
     * @author Jhon García
     */
    public function vehicleType()
    {
        return $this->belongsTo(VehicleType::class);
    }

    /**
     * Relación de uno a uno con Terceros (Contact)
     *
     * @return BelongsTo
     * @author Jhon García
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
