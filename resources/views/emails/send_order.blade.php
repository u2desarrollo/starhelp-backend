<html>


<body>
<div>
    <div>
        <h3>Señor(a): <br> {{$document['warehouse']['description']}} </h3>
        <div>
            <p>
                Hemos recibido su pedido. <br>
                Para nosotros fue un gusto atenderlo. <br>

                <strong>Detalle del pedido</strong> <br>
                <strong>Numero&nbsp;&nbsp;&nbsp;</strong><span><a href="https://colsaisa.starcommerce.co/#/b2b/pedidos">{{$document['consecutive']}}</a></span><br>
                <strong>Vr-Total</strong>&nbsp;&nbsp;&nbsp;$ {{number_format($document['total_value'])}}
                <br>
                <span><a href="https://colsaisa.starcommerce.co/#/b2b/inicio" class="btn boton">Ir al Portal</a></span> <br>

            </p>
            <br>
            <p>
                Cordialmente,
            </p>
            <h4>
                {{$document['seller']['name']}} {{$document['seller']['surname']}} <br>
                Asesor Comercial 
            </h4>
            @if($document['user']['signature'])
                @if(file_exists(storage_path('app/public/signature/') .$document['user']['signature']))
                    <img style="max-width: 200px;max-height: 200px;margin:auto;" src="<?php echo $message->embed(storage_path('app/public/signature/') .$document['user']['signature'] ); ?>">
                @endif
            @endif
        </div>
    </div>
</div>
</body>
</html>


