<html>

<head>
    <style>
        .body-email {
            font-family: Arial, Helvetica, sans-serif;
        }
        .text-nowrap{white-space:nowrap}
        .hr-email {
            background: #2f86c2;
            height: 3px;
            width: 1000px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .section-email {
            min-width: 1000px;
            max-width: 1000px;
        }
    </style>
</head>
<body>

@php
    // FECHAS
    $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    $now = \Carbon\Carbon::now();
    $fecha = \Carbon\Carbon::parse(\Carbon\Carbon::now());
    $mes = $meses[($fecha->format('n')) - 1];
    $fechaActual = $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    $horaActual = $now->format('g:i A');

    // NOMBRE USUARIO
    if (isset($contact->name)) {
        $names = explode(' ',$contact->name);
        $surnames = explode(' ',$contact->surname);
        $fullName = ucwords(strtolower($names[0]." ".(isset($surnames[0]) ? $surnames[0] : "")));
    } else {
        $fullName = "...";
    }
@endphp

<div class="body-email">

    {{-- HEADER --}}
    @if(file_exists(storage_path('app/public/') .'signature-colsaisa.png') && isset($message))
        <img 
            style="max-width: 150px;max-height: 150px;margin:auto;" 
            src="<?php echo $message->embed(storage_path('app/public/') .'signature-colsaisa.png'); ?>"
            alt="logo-colsaisa"
        >
    @endif

    

    <div class="hr-email"></div>
    <div class="section-email">
        <span style="float: right;"> Bogotá, {{ $fechaActual }}</span> <br>
        <span> Señores: <b>Colsaisa S.A</b> </span> <br>
        <span> Señor(a): <b>{{ $fullName }}</b> </span> <br>
        <span> Asunto: <b>Retención de Pedidos autorizados por Cartera</b> </span> <br>
        <div>
            <p>
                Reciba usted un cordial saludo. <br>
                Me permito informar que el día {{$fechaActual}} a las {{$horaActual}} se retuvieron los siguientes pedidos por tener mas <br>
                de 5 días de ser autorizados manualmente por el departamento de cartera:
            </p>

            @if (isset($documents))   
                <table cellspacing="15">
                    <thead>
                        <tr class="text-nowrap">
                            <td>
                                <b>Numero-Pedido</b>
                            </td>
                            <td>
                                <b>Cod-Cliente</b>
                            </td>
                            <td>
                                <b>Nombre-Cliente</b>
                            </td>
                            <td>
                                <b>Fec-Autoriz-Cartera</b>
                            </td>
                            <td>
                                <b>Usr-Autoriz-Cartera</b>
                            </td>
                            <td>
                                <b>Sede</b>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($documents as $document)
                            <tr class="text-nowrap">
                                <td>{{$document->consecutive}}</td>
                                <td>{{$document->contact->identification}}</td>
                                <td>{{$document->contact->name}} {{$document->contact->surname}}</td>
                                <td>{{$document->date_authorized}}</td>
                                <td>{{$document->userAuthorizesWallet->contact->name}} {{$document->userAuthorizesWallet->contact->surname}}</td>
                                <td>{{$document->branchoffice->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else 
             <span>...</span>
            @endif

            <p>
                Cordialmente,
            </p>
            
            @if(file_exists(storage_path('app/public/') .'signature-aurora01.png') && isset($message))
                <img 
                 style="max-width: 150px;max-height: 150px;margin:auto;" 
                 src="<?php echo $message->embed(storage_path('app/public/') .'signature-aurora01.png'); ?>"
                 alt="logo-aurora"
                >
            @endif
            
        </div>
    </div>
</div>
</body>
</html>


