<html>

<head>
    <style>
        .body-email {
            font-family: Arial, Helvetica, sans-serif;
        }
        .text-nowrap{white-space:nowrap}
        .hr-email {
            background: #2f86c2;
            height: 3px;
            width: 1000px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .section-email {
            min-width: 1000px;
            max-width: 1000px;
        }
    </style>
</head>
<body>

@php
    // FECHAS
    $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    $now = \Carbon\Carbon::now();
    $fecha = \Carbon\Carbon::parse(\Carbon\Carbon::now());
    $mes = $meses[($fecha->format('n')) - 1];
    $fechaActual = $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    $horaActual = $now->format('g:i A');
@endphp

<div class="body-email">

    {{-- HEADER --}}
    @if(file_exists(storage_path('app/public/') .'signature-colsaisa.png') && isset($message))
        <img 
            style="max-width: 150px;max-height: 150px;margin:auto;" 
            src="<?php echo $message->embed(storage_path('app/public/') .'signature-colsaisa.png'); ?>"
            alt="logo-colsaisa"
        >
    @endif

    

    <div class="hr-email"></div>
    <div class="section-email">
        <span style="float: right;"> Bogotá, {{ $fechaActual }}</span> <br>
        <span> Estimado Cliente. </span> <br>
        <div>
            <p>{{$messageBody}}</p>

            <p>Cordialmente,</p>
            
            @if(file_exists(storage_path('app/public/') .'signature-aurora01.png') && isset($message))
                <img 
                 style="max-width: 150px;max-height: 150px;margin:auto;" 
                 src="<?php echo $message->embed(storage_path('app/public/') .'signature-aurora01.png'); ?>"
                 alt="logo-aurora"
                >
            @endif
        </div>
    </div>
</div>
</body>
</html>


