<?php

return [
    'default' => env('FILESYSTEM_DRIVER', 'local'),
    'cloud'   => env('FILESYSTEM_CLOUD', 's3'),
    'disks'   => [
        'local'    => [
            'driver' => 'local',
            'root'   => storage_path('app')
        ],
        'colsaisa' => [
            'driver'     => 'local',
            'root'       => storage_path('app/colsaisa'),
            'url'        => env('APP_URL') . '/storage',
            'visibility' => 'private'
        ],
        'private'  => [
            'driver'     => 'local',
            'root'       => storage_path('app/private'),
            'url'        => env('APP_URL') . '/storage',
            'visibility' => 'private'
        ],
        'backup'   => [
            'driver'     => 'local',
            'root'       => storage_path('app/backup'),
            'visibility' => 'private'
        ],
        'public'   => [
            'driver'     => 'local',
            'root'       => storage_path('app/public'),
            'url'        => env('APP_URL') . '/storage',
            'visibility' => 'public'
        ],
        's3'       => [
            'driver' => 's3',
            'key'    => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url'    => env('AWS_URL')
        ],
        'sftp'     => [
            'driver'   => 'sftp',
            'host'     => env('SFTP_HOST'),
            'username' => env('SFTP_USER'),
            'password' => env('SFTP_PASSWORD'),
            'port'     => env('SFTP_PORT'),
            'timeout'  => 300
        ],
    ],
];
