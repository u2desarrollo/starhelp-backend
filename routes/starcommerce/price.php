<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * PriceAPIController
     */
    Route::apiResource('price-list', 'PriceAPIController');
    Route::post("import-excel", 'PriceAPIController@importPrices');
    Route::post('/cargar-precion-masivo', 'PriceAPIController@loadPricetoCsv');
    Route::put('price-update', 'PriceAPIController@updatedPriceArray');
    Route::get('price', 'PriceAPIController@getPrice');
    Route::get('testupdatepriceslol/{documentProduct}', 'PriceAPIController@updateprice');
    Route::put('price-update-excel', 'PriceAPIController@updatedPriceArrayExcel');
    Route::post('price-format-excel', 'PriceAPIController@formatPriceArrayExcel');

    /**
     * PriceContactAPIController
     */
    Route::apiResource('price_contacts', 'PriceContactAPIController');
    Route::get('price-contact', 'PriceContactAPIController@getPriceContact');

});
