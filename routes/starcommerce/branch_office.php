<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * BranchOfficeAPIController
     */
    Route::apiResource('sedes', 'BranchOfficeAPIController');
    Route::get('branchs', 'BranchOfficeAPIController@getBranchs');

    /**
     * BranchOfficeWarehouseAPIController
     */
    Route::apiResource('bodegas-sedes', 'BranchOfficeWarehouseAPIController')->only(['index']);
    Route::get('branchs-ware', 'BranchOfficeWarehouseAPIController@getBranchsWare');

});

