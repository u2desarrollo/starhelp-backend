<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * ContactAPIController
     */
    Route::apiResource('terceros', 'ContactAPIController');
    Route::post('terceros/excel', 'ContactAPIController@uploadExcel')->name('terceros.excel');
    Route::post('creacion-basica-tercero', 'ContactAPIController@storeBasicAsCustomer');
    Route::post('/create-new-client', 'ContactAPIController@prospectiveCustomer');
    Route::post('cliente-prospecto', 'ContactAPIController@prospectiveCustomer');
    Route::post('/carga-terceros', 'ContactAPIController@cargaInformacion');
    Route::post('/carga-terceros-masivo', 'ContactAPIController@updateContacts');
    Route::post('/carga-precios', 'ContactAPIController@cargaPrecios');
    Route::get('terceros-por-tipo', 'ContactAPIController@getContactForType');
    Route::get('terceros/id/{identification}', 'ContactAPIController@findForIdentification');
    Route::get('terceros-clientes', 'ContactAPIController@findForEmployeeType');
    Route::get('/check-new-client/{idClient}', 'ContactAPIController@check_new_client');
    Route::get('contacts', 'ContactAPIController@getContacts');
    Route::get('contactos-categoria', 'ContactAPIController@contactsByCategory');
    Route::get('contact-select', 'ContactAPIController@getContactsSelect');
    Route::get('client-debt/{diasMora}', 'ContactAPIController@client_debt');
    Route::get('informe-terceros', 'ContactAPIController@getContactReport');
    Route::get('/users', 'ContactAPIController@getUsers');
    Route::get('obtener-transportadoras', 'ContactAPIController@getConveyors');

    /**
     * ContactWarehouseAPIController
     */
    Route::apiResource('establecimientos', 'ContactWarehouseAPIController');
    Route::post('establecimientos/excel', 'ContactWarehouseAPIController@uploadExcel')->name('establecimientos.excel');
    Route::post('sucursal-contacto/actualizar-retencion', 'ContactWarehouseAPIController@saveConfigurationRetention');
    Route::post('/cargar-porcentaje-retenciones-masivo', 'ContactWarehouseAPIController@loadRetentionMasiveCsv');
    Route::put('actualizar-correo', 'ContactWarehouseAPIController@updateEmail');
    Route::get('establecimientos/array', 'ContactWarehouseAPIController@getByIdArray');
    Route::get('establecimientos/ids', 'ContactWarehouseAPIController@getIdContactsWarehouses');
    Route::get('cwarehouses', 'ContactWarehouseAPIController@getWarehouses');
    Route::get('establecimientos-vendedor', 'ContactWarehouseAPIController@findForEmployeeType');
    Route::get('establecimientos-vendedor/no-paginar', 'ContactWarehouseAPIController@findCustomersNotPaginate');
    Route::get('sucursales', 'ContactWarehouseAPIController@getContactWarehouse');
    Route::get('buscar-correo', 'ContactWarehouseAPIController@searchmail');
    Route::get('lista-proveedores', 'ContactWarehouseAPIController@getProviders');
    Route::get('/reasignacion-vendedor', 'ContactWarehouseAPIController@massReassignSeller');

    /**
     * ContactDocumentAPIController
     */
    Route::apiResource('documentos', 'ContactDocumentAPIController');
    Route::get('documentos-descargar/{id}', 'ContactDocumentAPIController@download');

    /**
     * ContactProviderAPIController
     */
    Route::apiResource('proveedores', 'ContactProviderAPIController', ['only' => 'index']);
    Route::get('lista-transportadores', 'ContactProviderAPIController@getConveyor');

    /**
     * ContactEmployeeAPIController
     */
    Route::get('lista-vendedores', 'ContactEmployeeAPIController@seller');

    /**
     * ContactClerkAPIController
     */
    Route::apiResource('funcionarios', 'ContactClerkAPIController');

    /**
     * ContactVehicleController
     */
    Route::get('vehiculos-terceros', 'ContactVehicleController@index');

});

