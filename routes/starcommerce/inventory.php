<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * InventoryAPIController
     */
    Route::apiResource('inventories', 'InventoryAPIController');
    Route::post('/cargar-ubicaciones-inventario-masivo', 'InventoryAPIController@loadLocationCsv');
    Route::put('actualizar-localizacion', 'InventoryAPIController@updatelocation');
    Route::get('disponibilidad', 'InventoryAPIController@available');
    Route::get('lista-productos-por-sede', 'InventoryAPIController@gProducts');
    Route::get('fin/{documentProduct}', 'InventoryAPIController@fin');
    Route::get('disponibilidad-producto', 'InventoryAPIController@availability');
    Route::get('reportes/sugerido-de-reposicion', 'InventoryAPIController@getReportRepositioningAssistant');
    Route::get('consulta-existencias', 'InventoryAPIController@getExistenceForProductAndBranchofficeWarehouse');
    Route::get('/inventario-sedes-por-producto/{product_id}', 'InventoryAPIController@branchOfficesWarehousesAvailableByProduct');
    Route::get('/diponibilidad-sedes/{product_id}', 'InventoryAPIController@branchOfficesAvailability');
    Route::get('inventario-valorizado', 'InventoryAPIController@generateReport');
    Route::get('/validar-duplicados-inventory', 'InventoryAPIController@searchdupli');

    /**
     * InventoryxController
     */
    Route::get('reconstructor', 'InventoryxController@fixValues');
    Route::get('reconstructor-global', 'InventoryxController@fixGlobalValues');
    Route::get('/reconstructorproductosdocumentos/{document_id}', 'InventoryxController@rebuildDocument');

});
