<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * RequestProductAPIController
     */
    Route::apiResource('request_products', 'RequestProductAPIController');
    Route::post('insert-request', 'RequestProductAPIController@insertData');
    Route::get('request-type', 'RequestProductAPIController@getRequestType');

    /**
     * RequestHeaderAPIController
     */
    Route::apiResource('request_headers', 'RequestHeaderAPIController');

});
