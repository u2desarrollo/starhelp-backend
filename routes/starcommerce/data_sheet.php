<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * DataSheetController
     */
    Route::apiResource('ficha-datos', 'DataSheetController');
    Route::post('ficha-datos-linea', 'DataSheetController@addDataSheetToLine');
    Route::post('ficha-datos/importar', 'DataSheetController@loadNameDataSheetMasive');
    Route::put('ficha-datos-linea/{id}', 'DataSheetController@updateRequiredDataSheetToLine');
    Route::delete('ficha-datos-linea/{id}', 'DataSheetController@removeDataSheetToLine');
    Route::get('ficha-datos-linea/{id}', 'DataSheetController@getDataSheetSelect');
    Route::get('/ficha-datos-tipo-valor', 'DataSheetController@getTypeValueDataSheet');
    Route::get('ficha-datos-listado', 'DataSheetController@listDataSheets');
    Route::get('ficha-datos-verificar-existe', 'DataSheetController@checkExistDataSheet');

    /**
     * DataSheetProductController
     */
    Route::apiResource('ficha-datos-productos', 'DataSheetProductController');

});




