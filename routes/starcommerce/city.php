<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * DepartmentAPIController
     */
    Route::apiResource('departamentos', 'DepartmentAPIController', ['only' => 'index']);

    /**
     * CityAPIController
     */
    Route::apiResource('ciudades', 'CityAPIController', ['only' => 'index']);
    Route::get('ciudades-departamentos', 'CityAPIController@cities');

});
