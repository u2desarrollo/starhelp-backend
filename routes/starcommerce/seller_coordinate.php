<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * SellerCoordinateAPIController
     */
    Route::post('/vendedores/registrar-visita', 'SellerCoordinateAPIController@store');
    Route::get('coordinate-seller', 'SellerCoordinateAPIController@getCoordinateSeller');
    Route::get('line-coordinate', 'SellerCoordinateAPIController@getLineCoordinate');

});


