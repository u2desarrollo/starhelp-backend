<?php

use App\Entities\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

Route::get('/test-concurrency', function () {

    set_time_limit(0);

    try {
        Log::info('Inicia transacción uno');
        DB::beginTransaction();

        $user = User::sharedLock()->find(44);
        Log::info('Obtiene usuario en transacción uno');
        Log::info('Inicia Sleep');
        sleep(60);
        Log::info('Finaliza Sleep');
        $user->remember_token += 7;
        $user->save();
        Log::info('Finaliza transacción uno');


        DB::commit();
    } catch (Exception $exception) {
        Log::error($exception->getMessage());
        DB::rollBack();
    }
});

Route::get('/test-concurrency-two', function () {

    try {
        Log::info(' Inicia transacción dos');
        DB::beginTransaction();

        $user = User::lockForUpdate()->find(44);
        Log::info('Obtiene usuario en transacción dos');
        $user->remember_token -= 3;
        $user->save();
        Log::info('Finaliza transacción dos');

        DB::commit();
    } catch (Exception $exception) {
        Log::error($exception->getMessage());
        DB::rollBack();
    }
});

