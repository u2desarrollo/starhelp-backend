<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * CashReceiptsAppController
     */
    Route::get('reciboCaja/descargar-pdf/{id}', 'CashReceiptsAppController@downloadPdf');
    Route::get('recibos-de-caja', 'CashReceiptsAppController@getCashReceiptsApp');
    Route::get('mio', 'CashReceiptsAppController@mio');

});
