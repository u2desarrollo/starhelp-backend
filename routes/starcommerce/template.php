<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * TemplateAPIController
     */
    Route::apiResource('modelos', 'TemplateAPIController');
    Route::get('obtener-modelo/{id}', 'TemplateAPIController@getModelForId');
    Route::get('modelo-por-codigo/{code}', 'TemplateAPIController@getModelForCode');

    /**
     * TemplateAccountingApiController
     */
    Route::apiResource('modelos-contabilidad', 'TemplateAccountingApiController');

});

