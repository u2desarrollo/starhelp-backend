<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * DiscountAPIController
     */
    Route::post('crear-descuento', 'DiscountAPIController@store');
    Route::put('actualizar-descuento', 'DiscountAPIController@update');
    Route::get('lista-descuentos', 'DiscountAPIController@get');
    Route::get('descuento/{id}', 'DiscountAPIController@show');
    Route::delete('eliminar-descuento/{id}', 'DiscountAPIController@delete');

    /**
     * DiscountSoonPaymentApiController
     */
    Route::post('crear-descuento-pronto-pago', 'DiscountSoonPaymentApiController@store');
    Route::put('actualizar-descuento-pronto-pago', 'DiscountSoonPaymentApiController@update');
    Route::get('lista-descuentos-pronto-pago', 'DiscountSoonPaymentApiController@get');
    Route::get('descuento-pronto-pago/{id}', 'DiscountSoonPaymentApiController@show');
    Route::delete('eliminar-descuento-pronto-pago/{id}', 'DiscountSoonPaymentApiController@delete');

});
