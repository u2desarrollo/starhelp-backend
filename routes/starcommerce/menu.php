<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * ModuleAPIController
     */
    Route::apiResource('modulos', 'ModuleAPIController')->only(['index']);

    /**
     * MenuAPIController
     */
    Route::apiResource('menu', 'MenuAPIController')->only(['index', 'store']);

    Route::get('/menu-video-tutorial', 'MenuAPIController@findAllMenuVideoTutorial');
    Route::post('/menu-video-tutorial', 'MenuAPIController@createMenuVideoTutorial');
    Route::put('/menu-video-tutorial/{id}', 'MenuAPIController@updateMenuVideoTutorial');
    Route::delete('/menu-video-tutorial/{id}', 'MenuAPIController@deleteMenuVideoTutorial');
});
