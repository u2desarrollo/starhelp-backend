<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * ServiceRequestTypeController
     */
    Route::apiResource('solicitudes-de-servicio', 'ServiceRequestApiController');
    Route::post('solicitudes-de-servicio/actualizar-estado', 'ServiceRequestApiController@updateState');
    Route::post('solicitudes-de-servicio/crear', 'ServiceRequestApiController@store');
    Route::post('solicitudes-de-servicio/crear-documento', 'ServiceRequestApiController@createDocument');
    Route::get('lista-tipos-solicitud', 'ServiceRequestTypeController@search');
    Route::get('/estados-por-solicitud-de-servicio', 'ServiceRequestTypeController@getStateByServiceRequestType');

});
