<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * DocumentAPIController
     */
    Route::apiResource('carrito', 'DocumentAPIController');
    Route::post('tets', 'DocumentAPIController@createTransfer');
    Route::post('tets-rot', 'DocumentAPIController@createTransferRot');
    Route::post('finalizar-pedido', 'DocumentAPIController@completeCart');
    Route::post('finalizar-documento', 'DocumentAPIController@finalizeDocument');
    Route::post('finalizar-documento-actualizar', 'DocumentAPIController@finalizeUpdateDocument');
    Route::post('enviar-documento-finalizado/{document}', 'DocumentAPIController@sendFinishedDocument');
    Route::post('actualizar-documento-contable', 'DocumentAPIController@UpdateAccountingDocument');
    Route::post('finalizar-documento-contable', 'DocumentAPIController@finalizeAccountingDocument');
    Route::post('documentos/cerrar/{id}', 'DocumentAPIController@closeDocument');
    Route::post('/recalcular-documento', 'DocumentAPIController@recalculateTotalsDocument');
    Route::post('/documento/actualizar-valores/', 'DocumentAPIController@updateDocumentValues');
    Route::get('/documento/contabilizar/{document}', 'DocumentAPIController@AccountingDocument');
    Route::post('/documento/actualizar-retenciones/', 'DocumentAPIController@updateDocumentValuesRetention');
    Route::post('saldos-iniciales-masivos', 'DocumentAPIController@massiveOpeningBalances');
    Route::post('enviar-correo', 'DocumentAPIController@sendmail');
    Route::post('/cargar-documentos-masivo', 'DocumentAPIController@loadDocumentMasive');
    Route::post('/anular-documento/{id}', 'DocumentAPIController@cancelDocument');
    Route::post('/correccion-documentos', 'DocumentAPIController@correctionDocuments');
    Route::post('/actualizar-doc-cruce', 'DocumentAPIController@updateDocCruce');
    Route::put('/despachos/{id}', 'DocumentAPIController@updateDespatchInvoiceInformation');
    Route::put('/despachos-cambiar-estado/{id}', 'DocumentAPIController@updateDespatchInvoiceStatus');
    Route::put('/despachos-masivo', 'DocumentAPIController@updateDespatchInvoiceStatusMasive');
    Route::put('/pedido/actualizar-estado', 'DocumentAPIController@changeStatusOrder');
    Route::put('/pedido/{id}/retener', 'DocumentAPIController@retainOrder');
    Route::put('/documento/{id}/actualizar-datos-generales','DocumentAPIController@updateGeneralDataDocument');
    Route::put('/borrar-erp','DocumentApiController@clearErp');
    Route::get('tets/validar-estado-factura', 'DocumentAPIController@validateOrderStatus');
    Route::get('lista-tras', 'DocumentAPIController@getTransferEntry');
    Route::get('archivo-plano', 'DocumentAPIController@generateFile');
    Route::get('carrito/{contact}/{warehouse}/{user}', 'DocumentAPIController@checkCart');
    Route::get('pedidos', 'DocumentAPIController@tracking');
    Route::get('pedidos-vendedor', 'DocumentAPIController@sellerOrders');
    Route::get('documents', 'DocumentAPIController@getDocuments');
    Route::get('documentos-inventario', 'DocumentAPIController@createDocumentFromBase');
    Route::get('documentos-inventario/{document_id}', 'DocumentAPIController@viewDocument');
    Route::get('get-documents-wms', 'DocumentAPIController@getDocumentsWms');
    Route::get('documentos-finalizados-pendientes-por-enviar', 'DocumentAPIController@getFinishedDocumentsPendingToSend');
    Route::get('back-order/{thread_id}', 'DocumentAPIController@getDocumentsForThreadId');
    Route::get('/buscar-documento','DocumentAPIController@searchDocument_');
    Route::get('editar-documento/{id}', 'DocumentAPIController@editDocument');
    Route::get('documentos-base', 'DocumentAPIController@baseDocuments');
    Route::get('documentos-retenidos', 'DocumentAPIController@documentsRetained');
    Route::get('reporte-ventas', 'DocumentAPIController@getReportSales');
    Route::get('reporte-ventas-detalle', 'DocumentAPIController@getReportSalesProducts');
    Route::get('detalle-documento/{id}', 'DocumentAPIController@detailDocument');
    Route::get('/lista-despachos', 'DocumentAPIController@despatchInvoiceList');
    Route::get('/lista-pedidos/{status}', 'DocumentAPIController@getListOrder');
    Route::get('/documento/buscar', 'DocumentAPIController@searchDocument');
    Route::get('facturacion-electronica', 'DocumentAPIController@electronicInvoice');
    Route::get('/exceldespachos', 'DocumentAPIController@excelDespatch');
    Route::get('/calcular-disponibilidad', 'DocumentAPIController@calculateAvailability');
    Route::get('/documentos-diponibilidad', 'DocumentAPIController@getDocumentsAbailability');
    Route::get('/documentos-margen-utilidad', 'DocumentAPIController@documentoMargenUtilidad');
    Route::get('/horadocumento', 'DocumentAPIController@search');
    Route::get('/documento/test', 'DocumentAPIController@test');
    Route::get('/descargar-documentos-con-estado', 'DocumentAPIController@downloadDocumentsByState');
    Route::get('/listaay', 'DocumentAPIController@buscrarDP');
    Route::get('/documentos-autorizar', 'DocumentAPIController@getDocumentByauthorize');
    Route::get('/reportes/autorizacion-documentos', 'DocumentAPIController@generateReportAuthorizedDocuments');
    Route::get('/documentos-backorder-faltante', 'DocumentAPIController@documentsMissingBackOrder');
    Route::get('/testt/{id}', 'DocumentAPIController@testt');
    Route::get('/hora-documento-0', 'DocumentAPIController@search');
    Route::get('documentos-por-autorizar', 'DocumentAPIController@getDocumentsToAuthorize');
    Route::get('traslado-a-medellin', 'DocumentAPIController@transferMedellin');
    Route::get('/excelpickin', 'DocumentAPIController@excelPickin');
    Route::get('/excel-etiquetas', 'DocumentAPIController@printLabels');
    Route::get('/test-return/{id}', 'DocumentAPIController@setReturnedDocument');
    Route::get('auditoria-documento/{id}' , 'DocumentAPIController@auditDocumentForId');
    Route::get('/documento-descargar-archivo/{file_id}', 'DocumentAPIController@documentDownloadFile');
    Route::get('update-backorder-document' , 'DocumentAPIController@updateBackOrderAndMissingValueDocument');
    Route::get('documentos-historial' , 'DocumentAPIController@documentsThreadHistoryByDocumentId');
    Route::get('/traslado/{id}','DocumentApiController@getTransfer');

    Route::post('contabilizacion-documento' , 'DocumentAPIController@accountingInventoryOperations');
    Route::post('contabilizacion-documento-cantidad' , 'DocumentAPIController@accountingInventoryOperationsCountDocuments');

    /**
     * DocumentPDFController
     */
    Route::get('pdfdespatch', 'DocumentPDFController@despatch');
    Route::get('pdf/{id}', 'DocumentPDFController@createPdf');
    Route::get('pdfpickin', 'DocumentPDFController@pickin');
    Route::get('pdfReciboCaja', 'DocumentPDFController@generatePDFCashReceipts');
    Route::get('pdfCartera', 'DocumentPDFController@pdfDocumentsBalance');
    Route::get('pdfPrejuridico', 'DocumentPDFController@pdfPreLegal');
    Route::get('pdfReporteNegativo', 'DocumentPDFController@pdfNegativeReport');
    Route::get('pdf-traslado/{id}', 'DocumentPDFController@transfer');
    Route::get('/pdf/{id}','DocumentPDFController@createPdf');
    Route::get('imprimir-pedido/{id}', 'DocumentPDFController@createPdf');
    Route::get('documentos/descargar-pdf/{document}', 'DocumentPDFController@downloadPdf');
    Route::get('/descargar-pdf-contabilidad/{document}', 'DocumentPDFController@downloadAccountingPdf');
    Route::get('traslado/descargar-pdf/{document}', 'DocumentPDFController@downloadPdfTransfer');
    Route::get('pdf-contabilidad/{id}', 'DocumentPDFController@accountingPDf');
    Route::get('cuadre-de-caja', 'DocumentPDFController@quareCashRegisterPdf');

    /**
     * DocumentProductAPIController
     */
    Route::apiResource('items-carrito', 'DocumentProductAPIController');
    Route::post('/documento/cargar-productos-excel/{document_id}', 'DocumentProductAPIController@uploadFileProductsToDocument');
    Route::post('/documento/actualizar-valores-productos/', 'DocumentProductAPIController@updateDocumentProductsValues');
    Route::get('kardex/', 'DocumentProductAPIController@getKardex');
    Route::get('costo-devuluciones-ant', 'DocumentProductAPIController@fixCostDevs');
    Route::get('crear-informe-operaciones', 'DocumentProductAPIController@createOperationsReport');
    Route::get('devoluciones-cero', 'DocumentProductAPIController@fixCost');
    Route::get('/traza-producto-documento/{document_product_id}', 'DocumentProductAPIController@traceDocumentProduct');
    Route::get('/devoluciones-cero','DocumentProductApiController@zeroReturns');
    Route::get('/reporte-negativos', 'DocumentProductApiController@reporNegative');
    Route::get('/costo-cero', 'DocumentProductApiController@fixCost');
    Route::get('document-update-wms', 'DocumentProductAPIController@updateDocumentProductWms');
    Route::get('transfers-wms', 'DocumentAPIController@transferBranchoffice');
    Route::get('movements-wms', 'DocumentAPIController@movementsWms');
    Route::get('/productos-importacion/{id}','DocumentProductAPIController@getProductsImport');

    /**
     * DocumentTransactionApiController
     */
    Route::post('comprobantes-varios', 'DocumentTransactionApiController@store');
    Route::get('comprobantes-documento/{id}', 'DocumentTransactionApiController@getTransactionsFromDocument');
    Route::get('/libro-auxiliar','DocumentTransactionApiController@auxiliaryBook');
    Route::get('/transacciones-cartera/{id}','DocumentTransactionApiController@getTransactionsWallet');
    Route::get('/libro-auxiliar-tercero','DocumentTransactionApiController@auxiliaryBookContact');
    Route::delete('eliminar-partida/{id}', 'DocumentTransactionApiController@destroy');
    Route::get('/reporte-comisiones','DocumentTransactionApiController@reportCommissions');

    /**
     * DocumentsBalanceAPIController
     */
    Route::apiResource('cartera', 'DocumentsBalanceAPIController');
    Route::get('despatch', 'DocumentsBalanceAPIController@despatch');
    Route::get('/total-cartera-cliente', 'DocumentsBalanceAPIController@getTotalWalletByContactId');
    Route::get('/Informe-cartera/{id}', 'DocumentsBalanceAPIController@getReceivableCSV');

    /**
     * DocumentInformationsAPIController
     */
    Route::apiResource('informacion-carrito', 'DocumentInformationsAPIController');


});
