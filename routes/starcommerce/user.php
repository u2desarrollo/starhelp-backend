<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * UserAPIController
     */
    Route::apiResource('usuarios', 'UserAPIController');
    Route::post('cambiar/contrasena', 'UserAPIController@changePassword');
    Route::get('estadisticas-vendedores', 'UserAPIController@sellersStats');
    Route::get('reportes/estadisticas-vendedores', 'UserAPIController@reportSellersStats');
    Route::get('usuarios-username', 'UserAPIController@getUsersUsername');
    Route::post('/usuario-editar-rol', 'UserAPIController@setUserRole');

});

