<?php

use Illuminate\Support\Facades\Route;

/**
 * rutas de activos fijos
 *
 * @author Kevin Galindo
 */
Route::middleware(['jwt.auth'])->group(function () {

    /**
     * FixAssetApiController
     */
    Route::apiResource('fecha-firme', 'FirmDateApiController');
});

