<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * ParameterAPIController
     */
    Route::apiResource('parametros', 'ParameterAPIController');
    Route::get('parametros-por-tabla/{tableparam_id}', 'ParameterAPIController@paramsByTable');
    Route::get('opciones-aurora/{tableparam_id}', 'ParameterAPIController@optionsAurora');
    Route::get('lista-param', 'ParameterAPIController@listParameter');
    Route::get('list-price', 'ParameterAPIController@getPriceList');
    Route::get('category', 'ParameterAPIController@getParameterCategory');
    Route::get('type-payment', 'ParameterAPIController@getTypePayment');
    Route::get('estados-de-documentos', 'ParameterAPIController@documentsStatus');
    Route::get('tipo-negociacion-cartera', 'ParameterAPIController@getTypeNegotiationPortfolio');

    /**
     * ParamTableAPIController
     */
    Route::apiResource('tablas-parametros', 'ParamTableAPIController');
    Route::get('/tabla-parametros/listado', 'ParamTableAPIController@listData');
    Route::get('categoria-cliente/{id}', 'ParamTableAPIController@getContactCategories');
    Route::get('/type-identification', 'ParamTableAPIController@type_identification');

});
