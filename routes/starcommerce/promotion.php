<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * PromotionAPIController
     */
    Route::apiResource('promociones', 'PromotionAPIController');
    Route::get('actualizar-promociones', 'PromotionAPIController@updatePromotions');
    Route::get('promociones-activas', 'PromotionAPIController@activePromotions');
    Route::get('/promocion/imagen/{filename}', 'PromotionAPIController@getImage');
    Route::get('promociones/search/{param}', 'PromotionAPIController@getProductByParam');
    Route::get('promocion/{id}', 'PromotionAPIController@findByIdForViewAndEdit');

});

