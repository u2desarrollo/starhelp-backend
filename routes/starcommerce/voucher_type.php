<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * VouchersTypeAPIController
     */
    Route::apiResource('tipos-de-comprobantes', 'VouchersTypeAPIController');
    Route::get('voucherstype', 'VouchersTypeAPIController@getVouchersType');
    Route::get('tipo-comprobante', 'VouchersTypeAPIController@getVoucherType');
    Route::get('/consecutivo/{vouchertype_id}', 'VouchersTypeAPIController@getConsecutive');
    Route::get('/consecutivo/{code}/{branchoffice_id}', 'VouchersTypeAPIController@getConsecutiveByCode');

});
