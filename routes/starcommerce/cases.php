<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {
    Route::apiResource('casos', 'CaseApiController');
    Route::apiResource('casos/interactuar', 'CaseAnswerApiController');
});

