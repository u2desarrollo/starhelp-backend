<?php

use Illuminate\Support\Facades\Route;

/**
 * rutas de activos fijos
 *
 * @author Kevin Galindo
 */
Route::middleware(['jwt.auth'])->group(function () {

    /**
     * FixAssetApiController
     */
    Route::apiResource('activos-fijos', 'FixAssetApiController');
    Route::apiResource('activos-fijos-adiciones', 'FixAssetAdditionApiController');
    Route::post('activos-fijos-cargar-archivo', 'FixAssetApiController@loadFixedAssetsFile');
    Route::get('activos-fijos-siguiente-consecutivo-sugerido', 'FixAssetApiController@getLastConsecutiveSuggested');
    Route::get('activos-fijos-descargar', 'FixAssetApiController@downloadFixedAssets');
    Route::post('activos-fijos-depreciacion-mensual/{id}', 'FixAssetApiController@createDepreciationMonthly');
    Route::post('activos-fijos-depreciacion-mensual-masivo', 'FixAssetApiController@createMasiveDepreciationMonthly');
    Route::get('activos-fijos-depreciacion-mensual/{id}', 'FixAssetApiController@getDepreciationMonthly');
    Route::post('comprobante-activos-fijos-depreciacion/{yearMonth}', 'FixAssetApiController@voucherAssetsFixedDepreciation');
    Route::get('lista-comprobante-activos-fijos-depreciacion', 'FixAssetApiController@getVoucherAssetsFixedDepreciation');
    Route::post('regenerar-comprobante-activos-fijos-depreciacion/{document_id}', 'FixAssetApiController@regenerateVoucherAssetsFixedDepreciation');
    Route::get('excel-comprobante-activos-fijos/{document_id}', 'FixAssetApiController@regenerateExcelVoucherAssetsFixed');

});

