<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * RoleApiController
     */
    Route::apiResource('rol', 'RoleApiController');
    Route::get('permisos', 'RoleApiController@getPermissions');

});

