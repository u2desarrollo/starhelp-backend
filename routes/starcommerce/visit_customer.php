<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * VisitCustomerAPIController
     */
    Route::apiResource('seguimiento-vendedores', 'VisitCustomerAPIController');
    Route::get('coordinate-marker', 'VisitCustomerAPIController@getMarkerCoordinate');
    Route::get('/seller/{id}', 'VisitCustomerAPIController@listDatesSeller');
    Route::get('/seller/{id}/{date}', 'VisitCustomerAPIController@listCordinatesDateSeller');
    Route::get('/seller-visits/{id}/{date}', 'VisitCustomerAPIController@listVisitsSeller');

});

