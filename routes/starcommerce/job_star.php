<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * JobStarApiController
     */
    Route::apiResource('tareas', 'JobStarApiController');
    Route::post('tareas/{id}/agregar-archivos', 'JobStarApiController@addFilesToJobStar');
    Route::post('tareas/{id}/agregar-integrandes', 'JobStarApiController@addMembers');
    Route::post('tareas/{id}/interaccion', 'JobStarApiController@addInteraction');
    Route::get('tareas-usuario', 'JobStarApiController@getJobsStarByUser');
    Route::delete('tareas/eliminar-archivo/{id}', 'JobStarApiController@deleteFileToJobStar');

    /**
     * JobsDocsApiController
     */
    Route::get('tareas/{id}/archivos/ver', 'JobsDocsApiController@show');
    Route::get('tareas/{id}/archivos/descargar', 'JobsDocsApiController@download');

    /**
     * JobsInteractionsDocsApiController
     */
    Route::get('interaccion/{id}/archivos/ver', 'JobsInteractionsDocsApiController@show');
    Route::get('interaccion/{id}/archivos/descargar', 'JobsInteractionsDocsApiController@download');

});
