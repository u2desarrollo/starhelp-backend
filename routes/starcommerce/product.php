<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * ProductAPIController
     */
    Route::apiResource('productos', 'ProductAPIController');
    Route::apiResource('productos', 'ProductAPIController');
    Route::post('productos/ficha-datos/importar/{line}', 'ProductAPIController@importFile');
    Route::post('calcular-precios/{contact_warehouse_id}', 'ProductAPIController@calculatePriceProductsAndContactWarehouse');
    Route::post('productos/excel', 'ProductAPIController@uploadExcel')->name('productos.excel');
    Route::post('actualizar-disponibilidad', 'ProductAPIController@updateAvailability');
    Route::post('productos/ficha-datos/importar/{line}', 'ProductAPIController@importFile');
    Route::post('/producto-por-codigo', 'ProductAPIController@getProductByCode');
    Route::get('productos/ficha-datos/exportar/{line}', 'ProductAPIController@generateFileImport');
    Route::get('get-products-wms', 'ProductAPIController@getProductsWms');
    Route::get('productos/{id}/editar', 'ProductAPIController@getProductSimple');
    Route::get('relacionados/{id}', 'ProductAPIController@relacionados');
    Route::get('tabla-productos', 'ProductAPIController@listProductsTable');
    Route::get('productos-reposicion', 'ProductAPIController@repositionProducts');
    Route::get('productos-promociones', 'ProductAPIController@getPromotions');
    Route::get('promocion-por-producto', 'ProductAPIController@getPromotionsForProduct');
    Route::get('calcular-promociones/{contact_warehouse_id}', 'ProductAPIController@setPromotionsForContactWarehouse');
    Route::get('calcular-precio/{contact_warehouse_id}/{product_id}', 'ProductAPIController@calculatePriceForProductAndContactWarehouse');
    Route::get('productos/id/{code}', 'ProductAPIController@findForCode');
    Route::get('productos-clasificacion', 'ProductAPIController@productByClasification');
    Route::get('lista-productos', 'ProductAPIController@getProducts');
    Route::get('productos/ficha-datos/exportar/{line}', 'ProductAPIController@generateFileImport');
    Route::get('productos-ficha-datos/{id}', 'ProductAPIController@dataSheetProduct');
    Route::get('productos-tabla', 'ProductAPIController@listProductsTable');
    Route::get('reporte-productos', 'ProductAPIController@generateReport');

    /**
     * LineAPIController
     */
    Route::apiResource('lineas', 'LineAPIController');
    Route::get('linea/{id}/ficha-datos', 'LineAPIController@dataSheetForId');
    Route::get('linea/{id}/ficha-datos', 'LineAPIController@dataSheetForId');
    Route::get('/lista-lineas', 'LineAPIController@linesList');

    /**
     * SubbrandAPIController
     */
    Route::apiResource('submarcas', 'SubbrandAPIController');
    Route::get('lista-submarcas', 'SubbrandAPIController@listSubbrand');
    Route::get('/submarca-por-marca', 'SubbrandAPIController@subBrandByBrandId');

    /**
     * SublineAPIController
     */
    Route::apiResource('sublineas', 'SublineAPIController');
    Route::get('lista-sublineas', 'SublineAPIController@listSubline');
    Route::get('/sublinea-por-linea', 'SublineAPIController@subLineByLineId');

    /**
     * SubCategorieAPIController
     */
    Route::apiResource('subcategorias', 'SubCategorieAPIController');
    Route::get('lista-subcategorias', 'SubCategorieAPIController@listSubcategories');
    Route::get('/subcategorias-por-categoria', 'SubCategorieAPIController@subCategoriesByCategorieId');

    /**
     * BrandAPIController
     */
    Route::apiResource('marcas', 'BrandAPIController');
    Route::get('/lista-marcas', 'BrandAPIController@brandList');

    /**
     * CategorieAPIController
     */
    Route::apiResource('categorias', 'CategorieAPIController');
    Route::get('/categorias-por-sublinea', 'CategorieAPIController@categoriesBySublineId');

    /**
     * LineTaxAPIController
     */
    Route::apiResource('lineas-impuestos', 'LineTaxAPIController');

    /**
     * ProductAttachmentAPIController
     */
    Route::post('imagenes-producto/{product}', 'ProductAttachmentAPIController@storeImages');
    Route::get('imagenes-producto/{product}', 'ProductAttachmentAPIController@getImages');
    Route::put('imagenes-producto/{product_attachment}', 'ProductAttachmentAPIController@setMainToImage');
    Route::delete('imagenes-producto/{product_attachment}', 'ProductAttachmentAPIController@deleteImage');

});
