<?php

use Illuminate\Support\Facades\Route;

Route::post('app/login', 'AuthController@authenticateApp');
Route::get('app-image-product' , 'ProductAPIController@getImageProduct');

Route::middleware(['jwt.auth'])->group(function () {

     Route::get('app-catalogo-productos' , 'ProductAPIController@appProductCatalog');
     Route::get('app-disponibilidad-productos' , 'InventoryAPIController@getAvailabilityAndPricesForProducts');
     Route::get('app-lista-precio-productos' , 'ProductAPIController@appListPriceProductByCustomer');
     Route::get('app-lista-inventario-productos' , 'ProductAPIController@appListInventoryProduct');
     Route::get('app-lista-clientes' , 'ContactAPIController@appListCustomerBySeller');

    Route::get('facturas', 'DocumentAPIController@getInvoices');

    Route::put('/boorrar-erp', 'DocumentAPIController@clearErp');

    // Bancos
    Route::get('bancos', 'ParameterAPIController@getBancos');

    // Formas de pago
    Route::post('formaspago', 'CashReceiptsAppController@store');

    // Registra las coordenadas del vendedor (cada minuto)
    Route::post('vendedores/coordenadas', 'SellerCoordinateAPIController@store');

    // Obtiene la cantidad de pedidos de clientes de un vendedor
    Route::post('vendedores/pedidos-clientes', 'DocumentAPIController@ordersCustomers');

    // ------------ CLIENTES ------------ \\
    // Obtiene las sucursales (bodegas) asociadas a los clientes de un vendedor
    Route::get('clientes', 'ContactAPIController@findForSellerId');
    // Obtiene todos los clientes
    Route::get('todo-clientes', 'ContactAPIController@getClients');

    // ------------ VISITAS DE VENDEDORES ------------ \\
    Route::post('vendedores/registrar-visita', 'SellerCoordinateAPIController@store');
    // Registra la visita de un vendedor
    Route::post('vendedores/checkin', 'VisitCustomerAPIController@store');
    // Registra la salida de un vendedor
    Route::post('vendedores/checkout', 'VisitCustomerAPIController@checkout');

    // ------------ FINALIZAR PEDIDO ------------ \\
     Route::post('finalizar-pedido-app', 'DocumentAppAPIController@finalizeOrderApp');

    // ------------ RECIBOS DE CAJA Y CONSIGNACIONES ------------ \\
    // Recibos de caja
    Route::get('recibocaja', 'DocumentAPIController@getInvoiceReceipt');
    Route::get('reciboscaja', 'DocumentAPIController@getCashReceipts');
    Route::get('reciboscaja-all', 'DocumentAPIController@getAllCashReceipts');
    Route::get('reciboscaja-detalle', 'DocumentAPIController@getItemizedCashReceipts');
    // Consignaciones
    Route::post('consignaciones', 'ConsignmentAPIController@store');
});
