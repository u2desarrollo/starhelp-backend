<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['jwt.auth'])->group(function () {

    /**
     * BalancesForAccountApiController
     */
    Route::get('trial-balance', 'BalancesForAccountApiController@getReportTrialBalance');
    Route::get('/estados-financieros','BalancesForAccountApiController@stateFinanze');
    Route::get('/consulta-terceros-balance/{id}','BalanceController@getBalanceContact');
    Route::get('/consulta-documentos-balance/{id}/{account_id}','BalanceController@getBalanceDocument');
    Route::get('/documentos-balance-cuenta-usuario/{id}/{account_id}','BalanceController@getBalanceDocumentContactAccount');

    /**
     * BalanceController
     */
    Route::post('actualizar-saldos', 'BalanceController@updateBalances');
    Route::get('consulta-cartera', 'BalanceController@searchWallet');

    Route::get('consulta-cuentas', 'BalanceController@searchAccount');
    Route::get('movimientos-cuenta-tercero', 'BalanceController@getMovementsAccountContact');

    /**
     * BalancesForDocumentApiController
     */
    Route::post('create-document-cross-balance','BalancesForDocumentApiController@createDocumentByCrossBalance');
    Route::post('create-transactions-document-cross-balance','BalancesForDocumentApiController@createTransactionsByDocumentCrossBalance');
    Route::post('finalizar-documento-cruce','BalancesForDocumentApiController@finalizeDocumentCrossBalance');
    Route::get('cruce-cuentas-saldo-a-favor-tercero','BalancesForDocumentApiController@fetchFavorBalanceContact');
    Route::get('cruce-cuentas-saldo-pendiente-tercero','BalancesForDocumentApiController@fetchPendingBalanceContact');
    Route::get('sucursales','BalancesForDocumentApiController@getAllContactWarehouse');
    Route::get('varificar-documento-usuario-documento-cruce','BalancesForDocumentApiController@checkDocumentInitiatedByAnotherUser');
    Route::delete('delete-transactions-document-cross-balance','BalancesForDocumentApiController@deleteDocumentTransactionsBySequence');
    Route::get('cuenta-por-defecto-cruce-de-saldos', 'AccountApiController@accountsDefaultCrossBalances');
});
