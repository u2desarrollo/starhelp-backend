<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace'  => 'Auth',
    'middleware' => 'api',
    'prefix'     => 'contrasena'
], function () {

    Route::post('olvido', 'PasswordResetController@create');
    Route::get('buscar/{token}', 'PasswordResetController@find');
    Route::post('restablecer', 'PasswordResetController@reset');
});

Route::post('login', 'AuthController@authenticate');
Route::post('login-wms', 'AuthController@authenticateApp');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('exit/{id?}', 'AuthController@logout');

Route::middleware(['jwt.auth'])->group(function () {

    Route::put('logout', 'AuthController@logout');

    Route::apiResource('licencias', 'LicenseAPIController');

    Route::apiResource('suscriptores', 'SubscriberAPIController');

    Route::apiResource('banners', 'BannerB2bAPIController');
    Route::get('banner-list', 'BannerB2bAPIController@getBanners');

    Route::apiResource('carrito', 'DocumentAPIController');
    Route::get('devoluciones-cero', 'DocumentProductAPIController@fixCost');
    Route::get('carrito/{contact}/{warehouse}/{user}', 'DocumentAPIController@checkCart');
    Route::get('pedidos', 'DocumentAPIController@tracking');
    Route::get('pedidos-vendedor', 'DocumentAPIController@sellerOrders');

    Route::apiResource('items-carrito', 'DocumentProductAPIController');
    Route::apiResource('informacion-carrito', 'DocumentInformationsAPIController');
    Route::post('finalizar-pedido', 'DocumentAPIController@completeCart');
    Route::post('actualizar-disponibilidad', 'ProductAPIController@updateAvailability');
    Route::get('imprimir-pedido/{id}', 'DocumentPDFController@createPdf');

    Route::get('promociones-activas', 'PromotionAPIController@activePromotions');

    Route::get('productos-clasificacion', 'ProductAPIController@productByClasification');


    Route::get('terceros-clientes', 'ContactAPIController@findForEmployeeType');
    Route::get('categoria-cliente/{id}', 'ParamTableAPIController@getContactCategories');
    Route::get('categoria-cliente-prospecto/{id}', 'ContactCustomerAPIController@getContactCategory');

    /* Rutas para visitas de vendedores */
    Route::apiResource('seguimiento-vendedores', 'VisitCustomerAPIController');
    //Route::get('estadisticas-vendedores', 'ContactAPIController@sellersStats');
    Route::get('estadisticas-vendedores', 'UserAPIController@sellersStats');
    Route::get('reportes/estadisticas-vendedores', 'UserAPIController@reportSellersStats');

    #traemos los tipos de documentos de identificaciones - relaciones: parameters -> paramtables
    #para consultar y tabular en formulario cliente prospecto -> select.tipo de identificacion
    Route::group(['prefix' => 'extra-data'], function () {
        Route::get('/type-identification', 'ParamTableAPIController@type_identification');
        // Route::get('/cities');
    });

    #Creamos nuevos clientes desde cliente-prospecto
    Route::post('/create-new-client', 'ContactAPIController@prospectiveCustomer');
    Route::get('/check-new-client/{idClient}', 'ContactAPIController@check_new_client');

    #consultamos los dias en mora, pasamos como parametro los dias
    Route::post('/vendedores/registrar-visita', 'SellerCoordinateAPIController@store');
    Route::get('lista-productos', 'ProductAPIController@getProducts');

    Route::get('branchs', 'BranchOfficeAPIController@getBranchs');
    Route::get('branchs-ware', 'BranchOfficeWarehouseAPIController@getBranchsWare');

    Route::get('banner-list', 'BannerB2bAPIController@getBanners');

    Route::apiResource('types_operations', 'TypesOperationAPIController');

    Route::apiResource('cuentas', 'AccountApiController');
    Route::apiResource('aurora', 'AuroraApiController');
    Route::apiResource('proyectos', 'ProjectsApiController');
    Route::get('/proyectos-activos', 'ProjectsApiController@activeProjects');

    Route::get('cuentas-raiz/{id}', 'AccountApiController@accountsFromRoot');

    Route::apiResource('payment-methods', 'PaymentMethodAPIController');

    Route::resource('historial-cuadre-de-caja', 'HistoryCashRegisterSquareController');

    Route::get('tipos-de-vehiculo', 'VehicleTypeController@index');

});

// rutas destinadas para la seccion de impuestos
Route::apiResource('impuestos', 'TaxAPIController');

Route::apiResource('automatic-portfolio-delivery', 'PortfolioShipmentController');
Route::apiResource('rol','RoleApiController');

//Ruta de prueba para generacion de facturacion electronica
Route::get('facturacion-electronica', 'DocumentAPIController@electronicInvoice');
Route::get('permisos', 'RoleApiController@getPermissions');

Route::get('/exceldespachos', 'DocumentAPIController@excelDespatch');

/**
 *
 * Rutas para las solicitudes de servicio
 *
 * @author Kevin Galindo | Santiago Torres
 */

Route::apiResource('solicitudes-de-servicio', 'ServiceRequestApiController');
//Route::get('solicitudes-de-servicio/descargar', 'ServiceRequestApiController@downloadServiceRequest');
Route::post('solicitudes-de-servicio/actualizar-estado', 'ServiceRequestApiController@updateState');
Route::post('solicitudes-de-servicio/crear', 'ServiceRequestApiController@store');
Route::post('solicitudes-de-servicio/crear-documento', 'ServiceRequestApiController@createDocument');
Route::post('saldos-iniciales-masivos', 'DocumentAPIController@massiveOpeningBalances');
Route::get('tipo-comprobante', 'VouchersTypeAPIController@getVoucherType');

Route::get('lista-tipos-solicitud', 'ServiceRequestTypeController@search');
Route::get('/estados-por-solicitud-de-servicio', 'ServiceRequestTypeController@getStateByServiceRequestType');

Route::get('buscar-correo', 'ContactWarehouseAPIController@searchmail');
Route::post('enviar-correo', 'DocumentAPIController@sendmail');
Route::put('actualizar-correo', 'ContactWarehouseAPIController@updateEmail');
Route::get('lista-proveedores', 'ContactWarehouseAPIController@getProviders');


Route::get('testupdatepriceslol/{documentProduct}', 'PriceAPIController@updateprice');

Route::post('/cargar-precion-masivo', 'PriceAPIController@loadPricetoCsv');
Route::post('/cargar-ubicaciones-inventario-masivo', 'InventoryAPIController@loadLocationCsv');
Route::post('/cargar-porcentaje-retenciones-masivo', 'ContactWarehouseAPIController@loadRetentionMasiveCsv');
Route::post('/cargar-documentos-masivo', 'DocumentAPIController@loadDocumentMasive');

Route::get('/calcular-disponibilidad', 'DocumentAPIController@calculateAvailability');
Route::get('reporte-productos', 'ProductAPIController@generateReport');

Route::get('/inventario-sedes-por-producto/{product_id}', 'InventoryAPIController@branchOfficesWarehousesAvailableByProduct');
Route::get('/diponibilidad-sedes/{product_id}', 'InventoryAPIController@branchOfficesAvailability');
Route::get('/consecutivo/{vouchertype_id}', 'VouchersTypeAPIController@getConsecutive');
Route::get('/consecutivo/{code}/{branchoffice_id}', 'VouchersTypeAPIController@getConsecutiveByCode');
Route::get('/documentos-diponibilidad', 'DocumentAPIController@getDocumentsAbailability');
Route::get('/documentos-margen-utilidad', 'DocumentAPIController@documentoMargenUtilidad');
Route::post('/anular-documento/{id}', 'DocumentAPIController@cancelDocument');

Route::get('inventario-valorizado', 'InventoryAPIController@generateReport');
Route::get('lista-transportadores', 'ContactProviderAPIController@getConveyor');


Route::get('/horadocumento', 'DocumentAPIController@search');

Route::post('/correccion-documentos', 'DocumentAPIController@correctionDocuments');
Route::post('/actualizar-doc-cruce', 'DocumentAPIController@updateDocCruce');
Route::get('/documento/test', 'DocumentAPIController@test');
Route::get('/descargar-documentos-con-estado', 'DocumentAPIController@downloadDocumentsByState');
Route::get('informe-terceros','ContactAPIController@getContactReport');
Route::get('/validar-duplicados-inventory', 'InventoryAPIController@searchdupli');
Route::get('/listaay', 'DocumentAPIController@buscrarDP');
Route::get('/reporte-negativos', 'DocumentProductApiController@reporNegative');

Route::get('/documentos-autorizar', 'DocumentAPIController@getDocumentByauthorize');
Route::get('/reportes/autorizacion-documentos', 'DocumentAPIController@generateReportAuthorizedDocuments');
Route::get('/documentos-backorder-faltante', 'DocumentAPIController@documentsMissingBackOrder');
Route::get('/testt/{id}', 'DocumentAPIController@testt');
Route::get('/traslado/{id}','DocumentApiController@getTransfer');
Route::put('/borrar-erp','DocumentApiController@clearErp');
Route::put('/documento/{id}/actualizar-datos-generales','DocumentAPIController@updateGeneralDataDocument');
Route::put('/prueba','DocumentApiController@aaaaaaaa');

Route::get('/hora-documento-0', 'DocumentAPIController@search');
Route::get('/ruta-prueba', 'DocumentApiController@tets');
Route::get('/users', 'ContactAPIController@getUsers');
Route::post('/usuario-editar-rol', 'UserAPIController@setUserRole');
Route::get('/reconstructorproductosdocumentos/{document_id}', 'InventoryxController@rebuildDocument');

Route::get('documentos-por-autorizar', 'DocumentAPIController@getDocumentsToAuthorize');

Route::get('traslado-a-medellin', 'DocumentAPIController@transferMedellin');
Route::get('/seller/{id}', 'VisitCustomerAPIController@listDatesSeller');
Route::get('/seller/{id}/{date}', 'VisitCustomerAPIController@listCordinatesDateSeller');
Route::get('/seller-visits/{id}/{date}', 'VisitCustomerAPIController@listVisitsSeller');


Route::get('/ultima-ubicacion/{id}', 'VisitCustomerAPIController@currentLocation');


Route::get('/excelpickin', 'DocumentAPIController@excelPickin');
Route::get('/excel-etiquetas', 'DocumentAPIController@printLabels');
Route::get('/costo-cero', 'DocumentProductApiController@fixCost');
Route::get('/reasignacion-vendedor', 'ContactWarehouseAPIController@massReassignSeller');
Route::get('/test-return/{id}', 'DocumentAPIController@setReturnedDocument');
Route::post('/producto-por-codigo', 'ProductAPIController@getProductByCode');

Route::get('/Informe-cartera/{id}', 'DocumentsBalanceAPIController@getReceivableCSV');



Route::get('auditoria-documento/{id}' , 'DocumentAPIController@auditDocumentForId');


Route::get('/documento-descargar-archivo/{file_id}', 'DocumentAPIController@documentDownloadFile');

Route::get('update-backorder-document' , 'DocumentAPIController@updateBackOrderAndMissingValueDocument');

Route::get('email-aurora', function (Request $request) {
    return view('emails.aurora.retain_authorized_documents');
});
